/*
Вход из-под Администратора
Создание новой компании Cypress1
Создание нового города/района/маршрута для компании с одноимённым названием.
Создание пользователя этой компании (оператор)
Прикрепление регистратора к компнаии
Назначение пользователю регистратор
Назначение компнаии стрим-сервера
Выход из-под Администратора

Вход из-под Оператора Cypress
Задача по патрулированию
*/

/*		Test params		*/

var adress		  	  = 'https://localhost'; //'https://guards.su'
var serial_number = '397b607d397b62f9';
var name_phone_Admin ='9876543210';
var name_password_Admin = '123456789';
var name_phone_Cypress =  '987100001';
var name_password_Cypress = '123456789';

var company_name ='Cypress';
/*		Templates functions		*/

function authA() {
	  it('Succses authorisation',function() {
		cy.viewport(1920, 1200)
		cy.clearCookie("SESSION");

		cy.visit(adress);
		cy.contains('Авторизация');
		cy.get('[name=phone]')
			.type(name_phone_Admin);
		cy.get('[name=password]')
			.type(name_password_Admin);
		cy.contains('Войти')
			.parents(".x-button")
			.click('',{force: true});
		cy.wait(1000);
		cy.contains('Карта')
			.parents(".x-button")
			.click('',{force: true});
	});
}

function authO() {
	  it('Succses authorisation',function() {
		cy.viewport(1920, 1200)
		cy.clearCookie("SESSION");

		cy.visit(adress);
		cy.contains('Авторизация');
		cy.get('[name=phone]')
			.type(name_phone_Cypress);
		cy.get('[name=password]')
			.type(name_password_Cypress);
		cy.contains('Войти')
			.parents(".x-button")
			.click('',{force: true});
		cy.wait(1000);
		cy.contains('Карта')
			.parents(".x-button")
			.click('',{force: true});
	});
}

function save(){
	cy.viewport(1920, 1200)
	cy.open
	 cy.contains('Сохранить')
      .parents(".x-button")
      .click(); 	
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})
}

Cypress.Cookies.defaults({
  whitelist: "SESSION"
});

describe('Test Functional of company', function(){
	
	authA();
/*
  it('Go to company tab',function() {
	  
		cy.contains('Управление').parents(".x-button").click('',{force: true});
		
		cy.get('#ext-simplelistitem-10')
    	  .click();
		cy.contains('Добавить новую компанию')
         .parents(".x-button")
         .click();
	});

	it('Add company',function(){
		cy.viewport(1920, 1200)
		cy.get('[placeholder="Название"]')
		  .type(company_name);
		
		cy.get('[placeholder="Выберите тип карты..."]')
		  .click();
		cy.contains('Yandex')
		  .click();
		  
		cy.get('[name=grantStreaming]')
		  .click();  
		save();
		cy.contains(company_name);
	})
*/
	it('Go to user tab',function() {
	    cy.viewport(1920, 1200);
		cy.contains('Управление').parents(".x-button").click('',{force: true});
		
		cy.get('#ext-simplelistitem-1')
    	  .click();
		cy.contains('Добавить нового пользователя')
         .parents(".x-button")
         .click();
	});
	it('Add user',function(){
cy.viewport(1920, 1200)
		cy.contains('Управление')
		  .parents(".x-button")
		  .click();
		cy.contains('Добавить нового пользователя')
      	  .parents(".x-button")
      	  .click();
		cy.get('[placeholder=Фамилия]')
		  .type(lastname);
		cy.get('[placeholder=Имя]')
		  .type(firstname);
		cy.get('[placeholder=Отчество]')
		  .type(secondname);
		cy.get('[name=phone]')
		  .type(name_phone_Cypress);
		cy.get('[name=password]')
		  .type(name_password_Cypresspassword);
		  //name="passportNum"
		cy.get('[name=passportNum]')
		  .type('1234');
		cy.get('[name=passportSeries]')
		  .type('123456');
		cy.get('[name=passportIssue]')
		  .type('blablabla');
		cy.get('[name=driverNum]')
		  .type('1234');
		cy.get('[name=driverSeries]')
		  .type('123456');
		cy.get('[name=driverA]')
		  .parent()
		  .click();

//		cy.get('[name=role]')
//		.click();
//		cy.contains('Патрульный') 																								//<!> патрульный
//		cy.get('#ext-boundlist-1')
//		  .contains('Патрульный')
//		  .click();
//
//		cy.get('[name=company]')
//		  .next().click();
//		cy.get('#ext-boundlist-2')
//		  .contains(company_name)
//		  .click();
//
//		cy.get('[name=device]')
//		  .next().click();
//		cy.get('#ext-boundlist-3')
//		  .contains(serial_number)
//		  .click();
//
//		cy.get('[name=city]')
//		  .next().click();
//		cy.get('#ext-boundlist-4')
//		  .contains('Test_City_For_Cypress')
//		  .click();
//
//		cy.get('[name=district]')
//		  .next().click();
//		cy.get('#ext-boundlist-5')
//		  .contains('Test_District_For_Cypress')
//		  .click();

		save();
		
	})
	
	//it('Add city',function(){})
	//it('Add district',function(){})
	//it('Add route',function(){})
	//it('Add user',function() {
	//  
	//
	//	cy.contains('Управление')
	//	.parents(".x-button")
	//	.click();
	//	cy.get('#ext-simplelistitem-2')
	//	.click();
	//	cy.contains('Добавить нового человека')
	//	.parents(".x-button")
	//	.click();
	//	
	//	cy.get('[placeholder=Фамилия]')
	//	.type(PersonFirstName1);
	//	cy.get('[placeholder=Имя]')
	//	.type(PersonLastName1);
	//	cy.get('[placeholder=Отчество]')
	//	.type(PersonSecondName1);
	//	
	//	cy.get('[name=age]')
	//	.type(nameage);
	//	cy.get('[name=passportNum]')
	//	.type(namepassportNum);
	//	cy.get('[name=passportSeries]')
	//	.type(namepassportSeries);
	//	cy.get('[name=passportIssue]')
	//	.type(namepassportIssue);
	//	
	//	cy.get('[name=type]')
	//	.click('',{force: true});																								// <!> обычный
	//	cy.get('#ext-boundlist-1')
	//	.contains('Обычный')
	//	.click('',{force: true});
	//	
	//	cy.get('[name=company]')
	//	.next().click();
	//	cy.get('#ext-boundlist-2')
	//	.contains(namecompany)
	//	.click();
	//	
	//	cy.contains('Сохранить')
	//	.parents(".x-button")
	//	.click('',{force: true}); 			
	//	// <!>*не могу создать. ругается на права
    //})
	//it('Add task',function(){})
})
