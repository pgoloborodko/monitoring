Cypress.Cookies.defaults({
  whitelist: "SESSION"
});


var adress		  	  = 'https://localhost'; //https://guards.su;

var nameage           = '100';

var namephone		  = '9876543210';
var namepassword	  = '123456789';

var namecompany		  = 'Test_Company_For_Cypress';

var PersonFirstName1  = 'TestSimple3'; 
var PersonFirstName2  = 'TestSearch'; 
var PersonLastName1   = 'HumanFor'; 
var PersonLastName2   = 'HumanFor';
var PersonSecondName1 = 'Cypress'; 
var PersonSecondName2 = 'Cypress'; 

var namepassportNum	  = '1234';
var namepassportSeries= '123456';
var namepassportIssue = 'blablabla';

var carname 		  = 'TestCar';
var carnum 			  = 'р123рр199';


/*тестирование Авторизации*/

describe('Authorisation test', function(){

		it('Succses authorisation',function() {
		cy.viewport(1920, 1200)
		cy.clearCookie("SESSION");

			cy.visit(adress);
			cy.wait(1000);
			cy.contains('Авторизация');

			cy.get('[name=phone]')
			  .type(namephone);

			cy.get('[name=password]')
			  .type(namepassword);
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});

			cy.wait(5000);

			cy.contains('Карта');
		});
		it('Failure authorisation(wrong login)',function() {
			cy.clearCookie("SESSION");
			cy.viewport(1920, 1200)
			cy.visit(adress);

			cy.wait(1000);
			cy.contains('Авторизация');

			cy.get('[name=phone]')
			  .type('9879879879');	//wrong number

			cy.get('[name=password]')
			  .type(namepassword);
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.wait(1000);
			cy.contains('Логин/пароль неверные!')
			cy.contains('OK')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.wait(1000);
			cy.contains('Авторизация');
		});
		it('Failure authorisation(wrong password)',function() {
			cy.clearCookie("SESSION");
			cy.viewport(1920, 1200)
			
			cy.visit(adress);
			cy.contains('Авторизация');
			cy.wait(1000);
			cy.get('[name=phone]')
			  .type(namephone);
			cy.get('[name=password]')
			  .type('000011110');	//wrong password
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.wait(1000);
			cy.contains('Логин/пароль неверные!')
			cy.contains('OK')
			  .parents(".x-button")
			  .click('',{force: true});
		})
		it('Change language and turn it back',function() {
		cy.clearCookie("SESSION");
		cy.viewport(1920, 1200)
			cy.visit(adress);

			cy.contains('Авторизация');

			cy.get('[name=language]')
			  .click('',{force: true});

			cy.wait(1000);

			cy.get('#ext-simplelistitem-2')
			  .click('',{force: true});


			cy.contains('Authorization');

			cy.get('[name=language]')
			  .click();

			cy.wait(1000);

			cy.get('#ext-simplelistitem-1')
			  .click('',{force: true});
		})
		it('Failure authorisation(empty fields)',function() {
		cy.clearCookie("SESSION");
		cy.viewport(1920, 1200)
			cy.visit(adress);
			cy.contains('Авторизация');
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Заполните правильно все поля!');
			cy.contains('OK')
			  .parents(".x-button")
			  .click('',{force: true});
			})
		it('Failure authorisation(empty login field)',function() {
		cy.clearCookie("SESSION");
		cy.viewport(1920, 1200)
			cy.visit(adress);
			cy.contains('Авторизация');
			cy.get('[name=password]')
			  .type(namepassword)
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Заполните правильно все поля!');
			cy.contains('OK').click('',{force: true});
			})
		it('Failure authorisation(empty password fiels)',function() {
		cy.clearCookie("SESSION");
		cy.viewport(1920, 1200)
			cy.visit(adress);
			cy.contains('Авторизация');
			cy.get('[name=phone]')
			  .type(namephone);
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Заполните правильно все поля!');
			cy.contains('OK')
			  .parents(".x-button")
			  .click('',{force: true});
			})
})
 /*тестирование Выхода*/

describe('Exit test', function(){
		it('Succses authorisation',function() {
			cy.clearCookie("SESSION");
			cy.viewport(1920, 1200)
			cy.visit(adress);
			cy.contains('Авторизация');

			cy.get('[name=phone]')
			  .type(namephone);

			cy.get('[name=password]')
			  .type(namepassword);
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});

			cy.wait(1000);

			cy.contains('Выход')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.wait(1000);
			cy.contains('Нет')
			  .click('',{force: true});
			cy.wait(1000);
			cy.contains("Карта");

			cy.contains('Выход')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.wait(1000);
			cy.contains('Да')
			  .parents(".x-button")
			  .click('',{force: true});
	cy.wait(1000);
			})
	})

 /*тестирование вкладок основной страницы*/

describe('Tabs test', function(){
		it('Succses authorisation',function() {
			cy.viewport(1920, 1200)
			cy.clearCookie("SESSION");

			cy.visit(adress);
			cy.contains('Авторизация');

			cy.get('[name=phone]')
			  .type(namephone);

			cy.get('[name=password]')
			  .type(namepassword);
			cy.contains('Войти')
			  .parents(".x-button")
			  .click('',{force: true});

			cy.wait(1000);

			cy.contains('Видео')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Выберите устройство')

			cy.wait(1000);

			cy.contains('Терминал')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Выберите онлайн-терминал')

			cy.wait(1000);

			cy.contains('Задачи')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Добавить новую задачу')

			cy.wait(1000);

			cy.contains('Статистика')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Отчеты по времени работы')

			cy.wait(1000);

			cy.contains('Управление')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Добавить нового пользователя')

			cy.wait(1000);

			cy.contains('Карта')
			  .parents(".x-button")
			  .click('',{force: true});
			cy.contains('Добавить нового пользователя')
		})
	})

 /*тестирование вкладки Задачи */

describe('Test Task tab', function(){
	
	it('Succses authorisation',function() {
	cy.clearCookie("SESSION");
	cy.viewport(1920, 1200)

		cy.visit(adress);
		cy.contains('Авторизация');

		cy.get('[name=phone]')
		  .type(namephone);

		cy.get('[name=password]')
		  .type(namepassword);
		cy.contains('Войти')
		  .parents(".x-button")
		  .click('',{force: true});

		cy.wait(1000);

		cy.contains('Карта');
	});
	it('Test button "Add New Task find TestSimplePerson"',function() {
			cy.viewport(1920, 1200)
			cy.contains('Задачи')
			  .parents(".x-button")
		      .click();
		    //
			cy.contains('Добавить новую задачу')
      		  .parents(".x-button")
      		  .click();
			cy.get('[name=type]')
			  .click();
			cy.get('#ext-simplelistitem-1')
			  .click();
            //
			cy.get('[name=company]')
			  .next().click();
			cy.get('#ext-boundlist-2')
			  .contains('Test_Company_For_Cypress')
			  .click();
			//cy.get('#ext-simplelistitem-16').click('',{force: true});
			//
			cy.get('[name=findPeople]')
			  .next()
			  .click();
			cy.get('#ext-boundlist-3')
			  .contains('TestSimplePerson For Cypress')
			  .click();
			//
			cy.get('[name=city]')
			  .next()
			  .click();
			cy.get('#ext-boundlist-4')
			  .contains('Test_City_For_Cypress')
			  .click();
			//
		    cy.get('[name=district]')
			  .next()
			  .click();
			cy.get('#ext-boundlist-5')
			  .contains('Test_District_For_Cypress')
			  .click();
			//
			cy.get('#ext-grid-3')
			  .contains('Test_Registrator_For_Cypress')
			  .parent()
			  .prev()
			  .prev()
			  .click();
			cy.contains('Сохранить')
			  .parents(".x-button")
		      .click();
	})
})

/*тестирование вкладки Управление */

describe('Test Control tab', function(){

  it('Succses authorisation',function() {
		cy.clearCookie("SESSION");
		cy.viewport(1920, 1200)
		cy.visit(adress);
		cy.contains('Авторизация');

		cy.get('[name=phone]')
		  .type(namephone);

		cy.get('[name=password]')
		  .type(namepassword);
		cy.contains('Войти')
		  .parents(".x-button")
		  .click('',{force: true});

		cy.wait(1000);

		cy.contains('Карта');

	});

  it('Test button "Add New User"',function() {
		cy.viewport(1920, 1200)
		cy.contains('Управление')
		  .parents(".x-button")
		  .click();
		cy.contains('Добавить нового пользователя')
      	  .parents(".x-button")
      	  .click();
		cy.get('[placeholder=Фамилия]')
		  .type('Test');
		cy.get('[placeholder=Имя]')
		  .type('UserFor');
		cy.get('[placeholder=Отчество]')
		  .type('Cypress');
		cy.get('[name=phone]')
		  .type('9872525252');
		cy.get('[name=password]')
		  .type(namepassword);
		  //name="passportNum"
		cy.get('[name=passportNum]')
		  .type('1234');
		cy.get('[name=passportSeries]')
		  .type('123456');
		cy.get('[name=passportIssue]')
		  .type('blablabla');
		cy.get('[name=driverNum]')
		  .type('1234');
		cy.get('[name=driverSeries]')
		  .type('123456');
		cy.get('[name=driverA]')
		  .parent()
		  .click();

		cy.get('[name=role]')
		.click('',{force: true});
		cy.contains('Патрульный') 																								//<!> патрульный
		cy.get('#ext-boundlist-1')
		  .contains('Патрульный')
		  .click('',{force: true});

		cy.get('[name=company]')
		  .next().click();
		cy.get('#ext-boundlist-2')
		  .contains('Test_Company_For_Cypress')
		  .click();

		cy.get('[name=device]')
		  .next().click();
		cy.get('#ext-boundlist-3')
		  .contains('Test_Registrator_For_Cypress')
		  .click();

		cy.get('[name=device]')
		  .next().click();
		cy.get('#ext-boundlist-3')
		  .contains('Test_Registrator_For_Cypress')
		  .click();

		cy.get('[name=city]')
		  .next().click();
		cy.get('#ext-boundlist-4')
		  .contains('Test_City_For_Cypress')
		  .click();

		cy.get('[name=district]')
		  .next().click();
		cy.get('#ext-boundlist-5')
		  .contains('Test_District_For_Cypress')
		  .click();

		cy.contains('Сохранить')
		  .parents(".x-button")
		  .click('',{force: true}); 																							//<!>*не могу создать. ругается на права*/
	})

  it('Succses authorisation',function() {
		cy.clearCookie("SESSION");
		cy.viewport(1920, 1200)
		cy.visit(adress);
		cy.contains('Авторизация');

		cy.get('[name=phone]')
		  .type(namephone);

		cy.get('[name=password]')
		  .type(namepassword);
		cy.contains('Войти')
		  .parents(".x-button")
		  .click('',{force: true});

		cy.wait(1000);

		cy.contains('Карта');

	});

  it('Test button "Add New man"',function() {
	cy.viewport(1920, 1200)
    cy.contains('Управление')
      .parents(".x-button")
      .click();
    cy.get('#ext-simplelistitem-2')
      			  .click();
    cy.contains('Добавить нового Человека')
          .parents(".x-button")
          .click();
    cy.get('[placeholder=Фамилия]')
      .type('Test');
    cy.get('[placeholder=Имя]')
      .type('Human');
    cy.get('[placeholder=Отчество]')
      .type('Cypress');

    cy.get('[name=passportNum]')
      .type('1234');
    cy.get('[name=passportSeries]')
      .type('123456');
    cy.get('[name=passportIssue]')
      .type('blablabla');

	cy.get('[name=company]')
      .next().click();
    cy.get('#ext-boundlist-2')
      .contains('Test_Company_For_Cypress')
      .click();

    cy.get('[name=device]')
      .next().click();
    cy.get('#ext-boundlist-3')
      .contains('Test_Registrator_For_Cypress')
      .click();

    cy.get('[name=device]')
      .next().click();
    cy.get('#ext-boundlist-3')
      .contains('Test_Registrator_For_Cypress')
      .click();

    cy.get('[name=city]')
      .next().click();
    cy.get('#ext-boundlist-4')
      .contains('Test_City_For_Cypress')
      .click();

    cy.get('[name=district]')
      .next().click();
    cy.get('#ext-boundlist-5')
      .contains('Test_District_For_Cypress')
      .click();

    cy.contains('Сохранить')
      .parents(".x-button")
      .click('',{force: true}); 				
uncaught: exception
	  //<!>*не могу создать. ругается на права*/
  })
  /*
  нужно добавить проверку корректности реакции на некорректный ввод данных
*/
})
  
describe('Test Control tab', function(){

/*
 it('Succses authorisation',function() {
		cy.clearCookie("SESSION");

		cy.visit(visit_site);
		
		cy.contains('Авторизация');

		cy.get('[name=phone]')
		  .type(namephone);

		cy.get('[name=password]')
		  .type(namepassword);
		cy.contains('Войти')
		  .parents(".x-button")
		  .click('',{force: true});

		cy.wait(1000);

		cy.contains('Карта');

	});

 it('Test button "Add New man"',function() {
	  

   cy.contains('Управление')
     .parents(".x-button")
     .click();
   cy.get('#ext-simplelistitem-2')
     .click();
   cy.contains('Добавить нового человека')
     .parents(".x-button")
     .click();
	   
   cy.get('[placeholder=Фамилия]')
     .type(PersonFirstName1);
   cy.get('[placeholder=Имя]')
     .type(PersonLastName1);
   cy.get('[placeholder=Отчество]')
     .type(PersonSecondName1);
	  
	cy.get('[name=age]')
     .type(nameage);
   cy.get('[name=passportNum]')
     .type(namepassportNum);
   cy.get('[name=passportSeries]')
     .type(namepassportSeries);
   cy.get('[name=passportIssue]')
     .type(namepassportIssue);

   cy.get('[name=type]')
     .click('',{force: true});																								// <!> обычный
   cy.get('#ext-boundlist-1')
     .contains('Обычный')
     .click('',{force: true});
 
   cy.get('[name=company]')
     .next().click();
   cy.get('#ext-boundlist-2')
     .contains(namecompany)
     .click();

   cy.contains('Сохранить')
     .parents(".x-button")
     .click('',{force: true}); 			
	  // <!>*не могу создать. ругается на права
 })
*/

  it('Succses authorisation',function() {
		cy.clearCookie("SESSION");
cy.viewport(1920, 1200)
		cy.visit(adress);
		
		cy.contains('Авторизация');

		cy.get('[name=phone]')
		  .type(namephone);

		cy.get('[name=password]')
		  .type(namepassword);
		cy.contains('Войти')
		  .parents(".x-button")
		  .click('',{force: true});

		cy.wait(1000);

		cy.contains('Карта');

	});

  it('Test button "Add New car"',function() {
	 cy.viewport(1920, 1200) 
    cy.contains('Управление')
      .parents(".x-button")
      .click();
    cy.get('#ext-simplelistitem-3')
      .click();
    cy.contains('Добавить новый транспорт')
      .parents(".x-button")
      .click();
	cy.wait(1000);
	
	cy.get('[placeholder="Марка, модель"]')
	  .type(carname);
	cy.get('[placeholder="Номерной знак"]')
	  .type(carnum);
	cy.get('#ext-radio-1')
	  .contains('A')
	  .click();
		
	cy.get('[name=type]')
      .next().click();
    cy.get('#ext-boundlist-1')
      .contains('Автомобиль')
      .click();	
	  
	cy.get('[name=company]')
      .next().click();
    cy.get('#ext-boundlist-2')
      .contains('Тест')
      .click();
	  
	cy.get('[name=device]')
      .next().click();
    cy.get('#ext-boundlist-3')
      .contains('#52 sada')
      .click();
	  
	cy.get('[name=driver]')
      .next().click();
    cy.get('#ext-boundlist-4')
      .contains('#42 test1 test1 Тесто')
      .click(); 	
    cy.contains('Сохранить')
      .parents(".x-button")
      .click('',{force: true}); 	  
  })
})
