package com.landau.core.tasks.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.DeviceConfigTest;
import com.landau.core.tasks.entities.Task;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceTaskMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceTaskMessagesTest {
    @Autowired
    private DeviceTaskMessages deviceTaskMessages;

    private static final Task task = new Task();
    private final static com.landau.objects.data.Task taskMessage = new com.landau.objects.data.Task();

    @BeforeClass
    public static void setUp(){
        task.setType(Task.Type.FIND_PROFILE);
    }

    @Test
    public void sendTask() {
        try {
            deviceTaskMessages.sendTask(task, device);

            Mockito.verify(deviceTaskMessages.getCoreDeviceMessages(), Mockito.times(1)).send(serial, taskMessage);
            Assert.assertEquals(task, deviceTaskMessages.getTaskRepo().findByType(task.getType()).get());
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}