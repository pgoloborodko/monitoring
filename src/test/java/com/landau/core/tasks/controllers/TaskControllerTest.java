package com.landau.core.tasks.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.services.ControllerService;
import com.landau.core.config.RestConfig;
import com.landau.core.tasks.messages.DeviceTaskMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.RestConfig.device;
import static com.landau.core.config.RestConfig.task;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class TaskControllerTest {
    private TaskController taskController;

    @Autowired
    private ControllerService controllerService;

    @Autowired
    private DeviceTaskMessages deviceTaskMessages;

    @Before
    public void setUp(){
        taskController = new TaskController(controllerService, deviceTaskMessages);

        when(taskController.getControllerService().verifyDevice(device)).thenReturn(device);
    }

    @Test
    public void sendTask() {
        taskController.sendTask(task, device);

        try {
            verify(taskController.getDeviceTaskMessages(), times(1)).sendTask(task, device);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}