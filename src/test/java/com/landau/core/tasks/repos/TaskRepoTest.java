package com.landau.core.tasks.repos;

import com.landau.core.tasks.entities.Task;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TaskRepoTest implements TaskRepo {
    private Map<Task.Type, Task> taskMap = new HashMap<>();
    @Override
    public List<Task> findAll() {
        return null;
    }

    @Override
    public List<Task> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Task> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Task> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Task task) {

    }

    @Override
    public void deleteAll(Iterable<? extends Task> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Task> S save(S s) {
        taskMap.put(s.getType(), s);
        return s;
    }

    @Override
    public <S extends Task> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Task> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Task> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Task> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Task getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Task> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Task> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Task> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Task> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Task> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Task> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<Task> findOne(Specification<Task> specification) {
        return Optional.empty();
    }

    @Override
    public List<Task> findAll(Specification<Task> specification) {
        return null;
    }

    @Override
    public Page<Task> findAll(Specification<Task> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Task> findAll(Specification<Task> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Task> specification) {
        return 0;
    }

    @Override
    public Optional<Task> findByType(Task.Type type) {
        return Optional.of(taskMap.get(type));
    }
}
