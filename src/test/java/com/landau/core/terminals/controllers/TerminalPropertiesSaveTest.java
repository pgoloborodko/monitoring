package com.landau.core.terminals.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.RestConfig;
import com.landau.core.terminals.entities.TerminalSettings;
import com.landau.core.terminals.messages.TerminalSettingsMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class TerminalPropertiesSaveTest {
    private TerminalPropertiesSave terminalPropertiesSave;

    @Autowired
    private TerminalSettingsMessages terminalSettingsMessages;

    @Before
    public void setUp(){
        terminalPropertiesSave = new TerminalPropertiesSave(terminalSettingsMessages);
    }

    @Test
    public void requestSetTerminalSettings() {
        TerminalSettings terminalSettings = new TerminalSettings();

        terminalPropertiesSave.requestSetTerminalSettings(terminalSettings);

        try {
            Mockito.verify(terminalPropertiesSave.getTerminalSettingsMessages(), Mockito.times(1))
                    .sendTerminalSettings(terminalSettings);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}