package com.landau.core.terminals.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.RestConfig;
import com.landau.core.terminals.messages.TerminalLogMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.RestConfig.terminal;
import static com.landau.core.config.RestConfig.test;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class RequestTerminalLogTest {
    private RequestTerminalLog requestTerminalLog;

    @Autowired
    private TerminalLogMessages terminalLogMessages;

    @Before
    public void setUp(){
        requestTerminalLog = new RequestTerminalLog(terminalLogMessages);
    }

    @Test
    public void requestTerminalLogList() {
        requestTerminalLog.requestTerminalLogList(terminal);

        try {
            Mockito.verify(requestTerminalLog.getTerminalLogMessages(), Mockito.times(1)).requestList(terminal);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void requestTerminalLogFile() {
        requestTerminalLog.requestTerminalLogFile(terminal, test);

        try {
            Mockito.verify(requestTerminalLog.getTerminalLogMessages(), Mockito.times(1)).requestFile(terminal, test);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}