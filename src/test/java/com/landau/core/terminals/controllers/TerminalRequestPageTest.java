package com.landau.core.terminals.controllers;

import com.landau.core.config.RestConfig;
import com.landau.core.terminals.messages.TerminalPageMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class TerminalRequestPageTest {
    private TerminalRequestPage terminalRequestPage;

    @Autowired
    private TerminalPageMessages terminalPageMessages;

    @Before
    public void setUp(){
        terminalRequestPage = new TerminalRequestPage(terminalPageMessages);
    }

    @Test
    public void getPage() {
//        HttpRequestExample httpRequestExample = new HttpRequestExample();
//        httpRequestExample.setUrl(test);
//        terminalRequestPage.getPage(terminal, httpRequestExample);
//
//        String url = "/?" + test;
//
//        Mockito.verify(terminalRequestPage.getTerminalPageMessages(), Mockito.times(1)).requestPage(terminal, url, httpRequestExample);
    }
}