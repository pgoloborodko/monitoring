package com.landau.core.terminals.messages;

import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.props.TerminalProps;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.TerminalConfigTest.serial;
import static com.landau.core.config.TerminalConfigTest.terminal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalPropsMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalPropsMessagesTest {
    @Autowired
    private TerminalPropsMessages terminalPropsMessages;

    private static final TerminalProps terminalPropsMessage = new TerminalProps();
    private static final com.landau.core.terminals.entities.TerminalProps terminalProps
            = new com.landau.core.terminals.entities.TerminalProps();

    @BeforeClass
    public static void setUp(){
        terminalPropsMessage.setSerial(serial);
        terminalProps.setTerminal(terminal);
    }

    @Test
    public void consumeTerminalProps() {
        terminalPropsMessages.consumeTerminalProps(terminalPropsMessage);
//TODO переделать
//        Assert.assertEquals(terminalProps, terminalPropsMessages.getTerminalPropsRepo().findByTerminal_Serial(serial).get());
    }
}