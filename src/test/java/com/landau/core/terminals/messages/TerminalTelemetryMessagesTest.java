package com.landau.core.terminals.messages;

import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.telemetry.TerminalTelemetry;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.TerminalConfigTest.serial;
import static com.landau.core.config.TerminalConfigTest.terminal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalTelemetryMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalTelemetryMessagesTest {
    @Autowired
    private TerminalTelemetryMessages terminalTelemetryMessages;

    private static final TerminalTelemetry terminalTelemetryMessage = new TerminalTelemetry();
    private static final com.landau.core.terminals.entities.TerminalTelemetry terminalTelemetry
            = new com.landau.core.terminals.entities.TerminalTelemetry();

    @Before
    public void setUp(){
        terminalTelemetryMessage.setSerial(serial);
        terminalTelemetry.setTerminal(terminal);
    }

    @Test
    public void consumeTerminalTelemetry() {
        terminalTelemetryMessage.setCpuLoad((byte) 5);
        terminalTelemetry.setCpuLoad((byte)5);

        terminalTelemetryMessages.consumeTerminalTelemetry(terminalTelemetryMessage);

        Assert.assertEquals(terminalTelemetryMessages.getTerminalTelemetryRepo().findByTerminal(terminal).get(), terminalTelemetry);
    }
}