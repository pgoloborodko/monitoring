package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.action.GetLogFile;
import com.landau.objects.action.GetLogList;
import com.landau.objects.data.LogFile;
import com.landau.objects.data.LogList;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;

import static com.landau.core.config.TerminalConfigTest.serial;
import static com.landau.core.config.TerminalConfigTest.terminal;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalLogMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalLogMessagesTest {
    @Autowired
    private TerminalLogMessages terminalLogMessages;

    private final static String file = "testFile";
    private final static LogList logList = new LogList();
    private final static List<LogList.Log> logs = new ArrayList<>();
    private final static LogFile logFile = new LogFile();

    @BeforeClass
    public static void setUp(){
        for (int i = 0; i < 10; i++){
            LogList.Log log = new LogList.Log();
            log.setName(file);
            logs.add(log);
        }

        logList.setLogs(logs);
        logList.setSerial(serial);
        logFile.setSerial(serial);
    }

    @Test
    public void requestList() {
        try {
            terminalLogMessages.requestList(terminal);
            Assert.assertNull(terminalLogMessages.getLogListMessages().get(terminal.getSerial()).getResult());
            verify(terminalLogMessages.getMessages(), times(1)).send(terminal.getSerial(), new GetLogList());
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void requestFile() {
        try {
            terminalLogMessages.requestFile(terminal, file);
            Assert.assertNull(terminalLogMessages.getLogFileMessages().get(terminal.getSerial()).getResult());
            verify(terminalLogMessages.getMessages(), times(1)).send(terminal.getSerial(), new GetLogFile(file));
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void consumeLogList() {
        terminalLogMessages.getLogListMessages().put(logList.getSerial(), new DeferredResult<>());
        terminalLogMessages.consumeLogList(logList);
        Assert.assertEquals(terminalLogMessages.getLogListMessages().size(), 0);

    }

    @Test
    public void consumeLogFile() {
        terminalLogMessages.getLogFileMessages().put(logFile.getSerial(), new DeferredResult<>());
        terminalLogMessages.consumeLogFile(logFile);
        Assert.assertEquals(terminalLogMessages.getLogFileMessages().size(), 0);
    }
}