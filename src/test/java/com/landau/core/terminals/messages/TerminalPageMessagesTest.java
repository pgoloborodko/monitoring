package com.landau.core.terminals.messages;

import com.landau.core.config.HttpRequestExample;
import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.action.GetPage;
import com.landau.objects.common.HttpStatus;
import com.landau.objects.data.Page;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

import static com.landau.core.config.TerminalConfigTest.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalPageMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalPageMessagesTest {
    @Autowired
    private TerminalPageMessages terminalPageMessages;

    private static final HttpServletRequest httpServletRequest = new HttpRequestExample();
    private static final String url = "url";
    private static final Page page = new Page();
    private static final GetPage getPage = new GetPage();


    @BeforeClass
    public static void setUp(){
        page.setStatus(HttpStatus.BAD_REQUEST);
        page.setSerial(serial);
        getPage.setUrl(url);
        getPage.setHeaders(new HashMap<>());
    }

    //TODO переделать
    @Test
    public void requestPage() {
//        terminalPageMessages.requestPage(terminal, url, httpServletRequest);
//        Assert.assertNull(terminalPageMessages.getRequestPageMessages().get(serial).getResult());
//        verify(terminalPageMessages.getMessages(), times(1)).send(getPage);

    }

    //TODO переделать
    @Test
    public void consumePage() {
//        terminalPageMessages.getRequestPageMessages().put(page.getSerial(), new DeferredResult<>());
//        terminalPageMessages.consumePage(page);
//        assertEquals(terminalPageMessages.getRequestPageMessages().size(),0);
    }
}