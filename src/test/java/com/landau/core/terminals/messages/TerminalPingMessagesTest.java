package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.telemetry.Ping;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.TerminalConfigTest.terminal;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalPingMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalPingMessagesTest {
    @Autowired
    private TerminalPingMessages terminalPingMessages;

    private static final Ping ping = new Ping();

    @Before
    public void setUp(){
        ping.setSerial(terminal.getSerial());
    }

    @Test
    public void consumePing() {
        terminalPingMessages.consumePing(ping);
        terminal.setOnline(true);
        try {
            verify(terminalPingMessages.getMessages(), times(1)).send("", terminal);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }

        Assert.assertNotNull(terminalPingMessages.getPings().get(terminal));
    }
}