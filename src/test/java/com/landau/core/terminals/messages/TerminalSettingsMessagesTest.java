package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.action.SetTerminalSettings;
import com.landau.objects.settings.TerminalSettings;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.TerminalConfigTest.serial;
import static com.landau.core.config.TerminalConfigTest.terminal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalSettingsMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalSettingsMessagesTest {
    @Autowired
    private TerminalSettingsMessages terminalSettingsMessages;

    private final static TerminalSettings terminalSettingsMessage = new TerminalSettings();
    private final static com.landau.core.terminals.entities.TerminalSettings terminalSettings
            = new com.landau.core.terminals.entities.TerminalSettings();
    private final static SetTerminalSettings setSettings = new SetTerminalSettings();

    @BeforeClass
    public static void setUp() {
        terminalSettingsMessage.setSerial(serial);
        terminalSettings.setTerminal(terminal);
        terminalSettings.setSerial(serial);
    }

    @Test
    public void sendTerminalSettings() {
        try {
            terminalSettings.setAuthTime(1);
            setSettings.setAuthTime(1);

            terminalSettingsMessages.sendTerminalSettings(terminalSettings);

            Mockito.verify(terminalSettingsMessages.getMessages(), Mockito.times(1)).send(serial, setSettings);

            Assert.assertEquals(terminalSettings, terminalSettingsMessages.getTerminalSettingsRepo().findByTerminal(terminal).get());

        } catch (RepublisherException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void consumeTerminalSettings() {
        terminalSettingsMessage.setAuthTime(1);
        terminalSettings.setAuthTime(1);

        terminalSettingsMessages.consumeTerminalSettings(terminalSettingsMessage);

        Assert.assertEquals(terminalSettings, terminalSettingsMessages.getTerminalSettingsRepo().findByTerminal(terminal).get());

    }
}