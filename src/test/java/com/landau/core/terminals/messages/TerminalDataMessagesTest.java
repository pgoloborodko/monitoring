package com.landau.core.terminals.messages;

import com.landau.core.config.TerminalConfigTest;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceProperties;
import com.landau.objects.data.Devices;
import com.landau.objects.props.DeviceProps;
import com.landau.objects.settings.DeviceSettings;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;

import static com.landau.core.config.TerminalConfigTest.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalDataMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalDataMessagesTest {

    @Autowired
    private TerminalDataMessages terminalDataMessages;

    private final static Devices devices = new Devices();
    private final static Device device = new Device();
    private final static DeviceProperties deviceProperties = new DeviceProperties();
    private final static Devices.Device deviceFromMessage = new Devices.Device();


    @BeforeClass
    public static void setUp(){
        device.setSerial(serial);
        deviceProperties.setDevice(device);

        DeviceSettings deviceSettings = new DeviceSettings();
        deviceSettings.setSerial(serial);
        DeviceProps deviceProps = new DeviceProps();
        deviceProps.setSerial(serial);

        deviceFromMessage.setSerial(serial);
        deviceFromMessage.setSettings(deviceSettings);
        deviceFromMessage.setProps(deviceProps);
        devices.setSerial(serial);
        devices.setDevices(new ArrayList<>());
        devices.getDevices().add(deviceFromMessage);
    }
    //TODO переделать
    @Test
    public void consumeDevices() {
//        terminalDataMessages.consumeDevices(devices);
//
//        Assert.assertEquals(device, terminalDataMessages.getDeviceRepo().findBySerial(serial).get());
//        Assert.assertEquals(deviceProperties, terminalDataMessages.getDevicePropertiesRepo().findByDevice(device).get());
    }
}