package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.entities.TerminalSettings;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TerminalSettingsRepoTest implements TerminalSettingsRepo {
    private Map<String, TerminalSettings> settingsMap = new HashMap<>();


    @Override
    public Optional<TerminalSettings> findByTerminal(Terminal serial) {
        return Optional.of(settingsMap.get(serial.getSerial()));
    }

    @Override
    public List<TerminalSettings> findAll() {
        return null;
    }

    @Override
    public List<TerminalSettings> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TerminalSettings> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalSettings> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TerminalSettings terminalSettings) {

    }

    @Override
    public void deleteAll(Iterable<? extends TerminalSettings> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TerminalSettings> S save(S s) {
        settingsMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends TerminalSettings> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TerminalSettings> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TerminalSettings> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TerminalSettings> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TerminalSettings getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends TerminalSettings> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TerminalSettings> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TerminalSettings> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TerminalSettings> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TerminalSettings> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TerminalSettings> boolean exists(Example<S> example) {
        return false;
    }
}
