package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.entities.TerminalProps;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TerminalPropsRepoTest implements TerminalPropsRepo {
    private Map<String, TerminalProps> propsMap = new HashMap<>();

    @Override
    public Optional<TerminalProps> findByTerminal(Terminal serial) {
        return Optional.of(propsMap.get(serial.getSerial()));
    }

    @Override
    public List<TerminalProps> findAll() {
        return null;
    }

    @Override
    public List<TerminalProps> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TerminalProps> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalProps> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TerminalProps terminalProps) {

    }

    @Override
    public void deleteAll(Iterable<? extends TerminalProps> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TerminalProps> S save(S s) {
        propsMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends TerminalProps> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TerminalProps> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TerminalProps> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TerminalProps> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TerminalProps getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends TerminalProps> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TerminalProps> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TerminalProps> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TerminalProps> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TerminalProps> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TerminalProps> boolean exists(Example<S> example) {
        return false;
    }
}
