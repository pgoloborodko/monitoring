package com.landau.core.terminals.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

public class UserRepoTest implements UserRepo {
    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<User> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void deleteAll(Iterable<? extends User> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends User> S save(S s) {
        return null;
    }

    @Override
    public <S extends User> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends User> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<User> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public User getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends User> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends User> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends User> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends User> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public List<User> findAllByOrderByIdDesc() {
        return null;
    }

    @Override
    public boolean existsPhone(String phone) {
        return false;
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public User findByPhone(String phone) {
        return null;
    }

    @Override
    public Optional<User> findByDevice(Device device) {
        return Optional.empty();
    }

    @Override
    public User findByDeviceAndCompany(Device device, Company company) {
        return null;
    }

    @Override
    public List<User> findAllByCompany(Company company) {
        return null;
    }

    @Override
    public User findByActivationCode(String code) {
        return null;
    }

    @Override
    public Optional<User> findOne(Specification<User> specification) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll(Specification<User> specification) {
        return null;
    }

    @Override
    public Page<User> findAll(Specification<User> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<User> findAll(Specification<User> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<User> specification) {
        return 0;
    }
}
