package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.filters.TerminalsFilter;
import lombok.Data;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@Data
public class TerminalRepoTest implements TerminalRepo {
    private Map<String, Terminal> terminalMap = new HashMap<>();

    @Override
    public Long countAllByOnline(Boolean online) {
        return null;
    }

    @Override
    public Integer updateStatus(Boolean online) {
        return null;
    }

    @Override
    public Optional<Terminal> findBySerial(String serial) {
        return Optional.of(terminalMap.get(serial));
    }

    @Override
    public List<Terminal> findAll() {
        return null;
    }

    @Override
    public List<Terminal> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Terminal> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Terminal> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Terminal terminal) {

    }

    @Override
    public void deleteAll(Iterable<? extends Terminal> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Terminal> S save(S s) {
        terminalMap.put(s.getSerial(), s);
        return s;
    }

    @Override
    public <S extends Terminal> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Terminal> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Terminal> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Terminal> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Terminal getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Terminal> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Terminal> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Terminal> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Terminal> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Terminal> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Terminal> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public List<TerminalsFilter> findAll(Specification<TerminalsFilter> specification) {
        return null;
    }

    @Override
    public Page<TerminalsFilter> findAll(Specification<TerminalsFilter> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalsFilter> findAll(Specification<TerminalsFilter> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<TerminalsFilter> specification) {
        return 0;
    }

    @Override
    public Optional<TerminalsFilter> findOne(Specification<TerminalsFilter> specification) {
        return Optional.empty();
    }
}
