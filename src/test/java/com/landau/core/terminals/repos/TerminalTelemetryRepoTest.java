package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.entities.TerminalTelemetry;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TerminalTelemetryRepoTest implements TerminalTelemetryRepo {
    private Map<String, TerminalTelemetry> telemetryMap = new HashMap<>();

    @Override
    public Optional<TerminalTelemetry> findByTerminal(Terminal serial) {
        return Optional.of(telemetryMap.get(serial.getSerial()));
    }

    @Override
    public List<TerminalTelemetry> findAll() {
        return null;
    }

    @Override
    public List<TerminalTelemetry> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TerminalTelemetry> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalTelemetry> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TerminalTelemetry terminalTelemetry) {

    }

    @Override
    public void deleteAll(Iterable<? extends TerminalTelemetry> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TerminalTelemetry> S save(S s) {
        telemetryMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends TerminalTelemetry> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TerminalTelemetry> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TerminalTelemetry> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TerminalTelemetry> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TerminalTelemetry getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends TerminalTelemetry> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TerminalTelemetry> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TerminalTelemetry> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TerminalTelemetry> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TerminalTelemetry> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TerminalTelemetry> boolean exists(Example<S> example) {
        return false;
    }
}
