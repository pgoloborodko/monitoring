package com.landau.core.stats.messages;

import com.landau.core.config.TerminalConfigTest;
import com.landau.objects.stats.TerminalDevicesStats;
import com.landau.objects.stats.TerminalSyncStats;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.TerminalConfigTest.serial;
import static com.landau.core.config.TerminalConfigTest.terminal;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalStatsMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalStatsMessagesTest {
    @Autowired
    private TerminalStatsMessages terminalStatsMessages;

    private static final TerminalDevicesStats terminalDevicesStatsMessage = new TerminalDevicesStats();
    private static final com.landau.core.stats.entity.TerminalDevicesStats terminalDevicesStats
            = new com.landau.core.stats.entity.TerminalDevicesStats();

    private static final TerminalSyncStats terminalSyncStatsMessage = new TerminalSyncStats();
    private static final com.landau.core.stats.entity.TerminalSyncStats terminalSyncStats
            = new com.landau.core.stats.entity.TerminalSyncStats();


    @BeforeClass
    public static void setUp(){
        terminalDevicesStatsMessage.setSerial(serial);
        terminalDevicesStats.setTerminal(terminal);

        terminalSyncStatsMessage.setSerial(serial);
        terminalSyncStats.setTerminal(terminal);
    }

    @Test
    public void consumeTerminalDeviceStats() {
        terminalStatsMessages.consumeTerminalDeviceStats(terminalDevicesStatsMessage);

        Assert.assertEquals(terminalDevicesStats, terminalStatsMessages.getTerminalDevicesStatsRepo().findByTerminal_Serial(serial).get());
    }

    @Test
    public void consumeTerminalSyncStats() {
        terminalStatsMessages.consumeTerminalSyncStats(terminalSyncStatsMessage);

        Assert.assertEquals(terminalSyncStats, terminalStatsMessages.getTerminalSyncStatsRepo().findByTerminal(terminal).get());
    }
}