package com.landau.core.stats.messages;

import com.landau.core.config.DeviceConfigTest;
import com.landau.core.stats.entity.StatsDeviceBattery;
import com.landau.core.stats.entity.StatsDeviceSpeedDistance;
import com.landau.objects.stats.DeviceBatteryStats;
import com.landau.objects.stats.DeviceSpeedDistanceStats;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceStatsMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceStatsMessagesTest {
    @Autowired
    private DeviceStatsMessages deviceStatsMessages;

    private static final DeviceBatteryStats deviceBatteryStats = new DeviceBatteryStats();
    private static final StatsDeviceBattery statsDeviceBattery = new StatsDeviceBattery();

    private static final DeviceSpeedDistanceStats deviceSpeedDistanceStats = new DeviceSpeedDistanceStats();
    private static final StatsDeviceSpeedDistance statsDeviceSpeedDistance = new StatsDeviceSpeedDistance();

    @BeforeClass
    public static void setUp(){
        deviceBatteryStats.setSerial(serial);
        statsDeviceBattery.setDevice(device);

        deviceSpeedDistanceStats.setSerial(serial);
        statsDeviceSpeedDistance.setDevice(device);
    }

    @Test
    public void consumeDeviceBatteryStats() {
        deviceStatsMessages.consumeDeviceBatteryStats(deviceBatteryStats);

        Assert.assertEquals(statsDeviceBattery, deviceStatsMessages.getStatsDeviceBatteryRepo().findByDevice_Serial(serial).get());
    }

    @Test
    public void deviceSpeedDistanceStats() {
        deviceStatsMessages.consumeDeviceSpeedDistanceStats(deviceSpeedDistanceStats);

        Assert.assertEquals(statsDeviceSpeedDistance, deviceStatsMessages.getStatsDeviceSpeedDistanceRepo().findByDevice_Serial(serial).get());
    }
}