package com.landau.core.stats.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.stats.entity.TerminalStats;
import com.landau.core.stats.filters.StatsTerminalFilter;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class TerminalStatsRepoTest implements TerminalStatsRepo {
    private Map<String, TerminalStats> eventMap = new HashMap<>();

    @Override
    public Optional<TerminalStats> findTopByDateBetweenAndCompany(Date timeStart, Date timeEnd, Company company) {
        return Optional.empty();
    }

    @Override
    public Optional<TerminalStats> findByDateBetweenAndCompany(Date timeStart, Date timeEnd, Company company) {
        return Optional.empty();
    }

    @Override
    public Optional<TerminalStats> findByTerminal_Serial(String serial) {
        return Optional.of(eventMap.get(serial));
    }

    @Override
    public List<TerminalStats> findAll() {
        return null;
    }

    @Override
    public List<TerminalStats> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TerminalStats> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalStats> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TerminalStats terminalStats) {

    }

    @Override
    public void deleteAll(Iterable<? extends TerminalStats> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TerminalStats> S save(S s) {
        eventMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends TerminalStats> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TerminalStats> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TerminalStats> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TerminalStats> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TerminalStats getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends TerminalStats> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TerminalStats> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TerminalStats> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TerminalStats> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TerminalStats> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TerminalStats> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<StatsTerminalFilter> findOne(Specification<StatsTerminalFilter> specification) {
        return Optional.empty();
    }

    @Override
    public List<StatsTerminalFilter> findAll(Specification<StatsTerminalFilter> specification) {
        return null;
    }

    @Override
    public Page<StatsTerminalFilter> findAll(Specification<StatsTerminalFilter> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<StatsTerminalFilter> findAll(Specification<StatsTerminalFilter> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<StatsTerminalFilter> specification) {
        return 0;
    }
}
