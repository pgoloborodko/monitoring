package com.landau.core.stats.repos;

import com.landau.core.stats.entity.StatsStreamServer;
import com.landau.core.stats.filters.StatsStreamServerFilter;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class StatsStreamServerRepoTest implements StatsStreamServerRepo {
    private Map<String, StatsStreamServer> eventMap = new HashMap<>();


    @Override
    public Optional<StatsStreamServer> findByName(String name) {
        return Optional.of(eventMap.get(name));
    }

    @Override
    public List<StatsStreamServer> findAll() {
        return null;
    }

    @Override
    public List<StatsStreamServer> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<StatsStreamServer> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<StatsStreamServer> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(StatsStreamServer statsStreamServer) {

    }

    @Override
    public void deleteAll(Iterable<? extends StatsStreamServer> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends StatsStreamServer> S save(S s) {
        eventMap.put(s.getName(), s);
        return s;
    }

    @Override
    public <S extends StatsStreamServer> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<StatsStreamServer> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends StatsStreamServer> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<StatsStreamServer> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public StatsStreamServer getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends StatsStreamServer> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends StatsStreamServer> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends StatsStreamServer> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends StatsStreamServer> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StatsStreamServer> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends StatsStreamServer> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<StatsStreamServerFilter> findOne(Specification<StatsStreamServerFilter> specification) {
        return Optional.empty();
    }

    @Override
    public List<StatsStreamServerFilter> findAll(Specification<StatsStreamServerFilter> specification) {
        return null;
    }

    @Override
    public Page<StatsStreamServerFilter> findAll(Specification<StatsStreamServerFilter> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<StatsStreamServerFilter> findAll(Specification<StatsStreamServerFilter> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<StatsStreamServerFilter> specification) {
        return 0;
    }
}
