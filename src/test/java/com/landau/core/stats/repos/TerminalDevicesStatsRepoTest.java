package com.landau.core.stats.repos;

import com.landau.core.stats.entity.TerminalDevicesStats;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TerminalDevicesStatsRepoTest implements TerminalDevicesStatsRepo {
    private Map<String, TerminalDevicesStats> eventMap = new HashMap<>();

    @Override
    public Optional<TerminalDevicesStats> findByTerminal_Serial(String serial) {
        return Optional.of(eventMap.get(serial));
    }

    @Override
    public List<TerminalDevicesStats> findAll() {
        return null;
    }

    @Override
    public List<TerminalDevicesStats> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TerminalDevicesStats> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalDevicesStats> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TerminalDevicesStats terminalDevicesStats) {

    }

    @Override
    public void deleteAll(Iterable<? extends TerminalDevicesStats> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TerminalDevicesStats> S save(S s) {
        eventMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends TerminalDevicesStats> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TerminalDevicesStats> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TerminalDevicesStats> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TerminalDevicesStats> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TerminalDevicesStats getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends TerminalDevicesStats> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TerminalDevicesStats> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TerminalDevicesStats> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TerminalDevicesStats> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TerminalDevicesStats> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TerminalDevicesStats> boolean exists(Example<S> example) {
        return false;
    }
}
