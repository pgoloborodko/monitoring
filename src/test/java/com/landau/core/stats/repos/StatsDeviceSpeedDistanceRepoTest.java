package com.landau.core.stats.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceSpeedDistance;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class StatsDeviceSpeedDistanceRepoTest implements StatsDeviceSpeedDistanceRepo {
    private Map<String, StatsDeviceSpeedDistance> eventMap = new HashMap<>();

    @Override
    public Optional<StatsDeviceSpeedDistance> findTopByDeviceOrderByIdDesc(Device device) {
        return Optional.empty();
    }

    @Override
    public Optional<StatsDeviceSpeedDistance> findByDevice_Serial(String serial) {
        return Optional.of(eventMap.get(serial));
    }

    @Override
    public List<StatsDeviceSpeedDistance> findAll() {
        return null;
    }

    @Override
    public List<StatsDeviceSpeedDistance> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<StatsDeviceSpeedDistance> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<StatsDeviceSpeedDistance> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(StatsDeviceSpeedDistance statsDeviceSpeedDistance) {

    }

    @Override
    public void deleteAll(Iterable<? extends StatsDeviceSpeedDistance> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends StatsDeviceSpeedDistance> S save(S s) {
        eventMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<StatsDeviceSpeedDistance> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends StatsDeviceSpeedDistance> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<StatsDeviceSpeedDistance> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public StatsDeviceSpeedDistance getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends StatsDeviceSpeedDistance> boolean exists(Example<S> example) {
        return false;
    }
}