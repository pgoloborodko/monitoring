package com.landau.core.stats.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.filters.StatsDeviceOnlineUptimeFilter;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class StatsDeviceOnlineUptimeRepoTest implements StatsDeviceOnlineUptimeRepo {
    private Map<String, StatsDeviceOnlineUptime> eventMap = new HashMap<>();

    @Override
    public StatsDeviceOnlineUptime findFirstByDeviceAndEndSessionOrderByIdDesc(Device device, Date date) {
        return null;
    }

    @Override
    public StatsDeviceOnlineUptime findFirstByDeviceOrderByIdDesc(Device device) {
        return null;
    }

    @Override
    public StatsDeviceOnlineUptime findFirstByDeviceAndEndSessionNull(Device device) {
        return null;
    }

    @Override
    public Optional<StatsDeviceOnlineUptime> findByDevice_Serial(String serial) {
        return Optional.of(eventMap.get(serial));
    }

    @Override
    public List<StatsDeviceOnlineUptime> findAll() {
        return null;
    }

    @Override
    public List<StatsDeviceOnlineUptime> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<StatsDeviceOnlineUptime> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<StatsDeviceOnlineUptime> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(StatsDeviceOnlineUptime statsDeviceOnlineUptime) {

    }

    @Override
    public void deleteAll(Iterable<? extends StatsDeviceOnlineUptime> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends StatsDeviceOnlineUptime> S save(S s) {
        eventMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<StatsDeviceOnlineUptime> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends StatsDeviceOnlineUptime> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<StatsDeviceOnlineUptime> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public StatsDeviceOnlineUptime getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends StatsDeviceOnlineUptime> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<StatsDeviceOnlineUptime> findOne(Specification<StatsDeviceOnlineUptime> specification) {
        return Optional.empty();
    }

    @Override
    public List<StatsDeviceOnlineUptime> findAll(Specification<StatsDeviceOnlineUptime> specification) {
        return null;
    }

    @Override
    public Page<StatsDeviceOnlineUptime> findAll(Specification<StatsDeviceOnlineUptime> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<StatsDeviceOnlineUptime> findAll(Specification<StatsDeviceOnlineUptime> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<StatsDeviceOnlineUptime> specification) {
        return 0;
    }
}
