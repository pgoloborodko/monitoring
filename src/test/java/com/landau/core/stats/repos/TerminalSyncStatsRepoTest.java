package com.landau.core.stats.repos;

import com.landau.core.stats.entity.TerminalSyncStats;
import com.landau.core.terminals.entities.Terminal;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TerminalSyncStatsRepoTest implements TerminalSyncStatsRepo {
    private Map<String, TerminalSyncStats> eventMap = new HashMap<>();

    @Override
    public Optional<TerminalSyncStats> findByTerminal(Terminal serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }

    @Override
    public List<TerminalSyncStats> findAll() {
        return null;
    }

    @Override
    public List<TerminalSyncStats> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TerminalSyncStats> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TerminalSyncStats> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(TerminalSyncStats terminalSyncStats) {

    }

    @Override
    public void deleteAll(Iterable<? extends TerminalSyncStats> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TerminalSyncStats> S save(S s) {
        eventMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends TerminalSyncStats> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TerminalSyncStats> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TerminalSyncStats> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TerminalSyncStats> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TerminalSyncStats getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends TerminalSyncStats> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TerminalSyncStats> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TerminalSyncStats> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TerminalSyncStats> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TerminalSyncStats> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TerminalSyncStats> boolean exists(Example<S> example) {
        return false;
    }
}
