package com.landau.core.stats.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceBattery;
import com.landau.core.stats.filters.StatsDeviceBatteryFilter;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class StatsDeviceBatteryRepoTest implements StatsDeviceBatteryRepo {
   private Map<String, StatsDeviceBattery> eventMap = new HashMap<>();


   @Override
   public Optional<StatsDeviceBattery> findTopByDeviceOrderByIdDesc(Device device) {
      return Optional.empty();
   }

   @Override
   public List<StatsDeviceBattery> findAllByDeviceAndFromGreaterThanEqualAndToLessThanEqualOrderByIdAsc(Device device, Date from, Date to) {
      return null;
   }

   @Override
   public Optional<StatsDeviceBattery> findByDevice_Serial(String serial) {
      return Optional.of(eventMap.get(serial));
   }

   @Override
   public List<StatsDeviceBattery> findAll() {
      return null;
   }

   @Override
   public List<StatsDeviceBattery> findAll(Sort sort) {
      return null;
   }

   @Override
   public Page<StatsDeviceBattery> findAll(Pageable pageable) {
      return null;
   }

   @Override
   public List<StatsDeviceBattery> findAllById(Iterable<Long> iterable) {
      return null;
   }

   @Override
   public long count() {
      return 0;
   }

   @Override
   public void deleteById(Long aLong) {

   }

   @Override
   public void delete(StatsDeviceBattery statsDeviceBattery) {

   }

   @Override
   public void deleteAll(Iterable<? extends StatsDeviceBattery> iterable) {

   }

   @Override
   public void deleteAll() {

   }

   @Override
   public <S extends StatsDeviceBattery> S save(S s) {
      eventMap.put(s.getDevice().getSerial(), s);
      return s;
   }

   @Override
   public <S extends StatsDeviceBattery> List<S> saveAll(Iterable<S> iterable) {
      return null;
   }

   @Override
   public Optional<StatsDeviceBattery> findById(Long aLong) {
      return Optional.empty();
   }

   @Override
   public boolean existsById(Long aLong) {
      return false;
   }

   @Override
   public void flush() {

   }

   @Override
   public <S extends StatsDeviceBattery> S saveAndFlush(S s) {
      return null;
   }

   @Override
   public void deleteInBatch(Iterable<StatsDeviceBattery> iterable) {

   }

   @Override
   public void deleteAllInBatch() {

   }

   @Override
   public StatsDeviceBattery getOne(Long aLong) {
      return null;
   }

   @Override
   public <S extends StatsDeviceBattery> Optional<S> findOne(Example<S> example) {
      return Optional.empty();
   }

   @Override
   public <S extends StatsDeviceBattery> List<S> findAll(Example<S> example) {
      return null;
   }

   @Override
   public <S extends StatsDeviceBattery> List<S> findAll(Example<S> example, Sort sort) {
      return null;
   }

   @Override
   public <S extends StatsDeviceBattery> Page<S> findAll(Example<S> example, Pageable pageable) {
      return null;
   }

   @Override
   public <S extends StatsDeviceBattery> long count(Example<S> example) {
      return 0;
   }

   @Override
   public <S extends StatsDeviceBattery> boolean exists(Example<S> example) {
      return false;
   }

   @Override
   public Optional<StatsDeviceBatteryFilter> findOne(Specification<StatsDeviceBatteryFilter> specification) {
      return Optional.empty();
   }

   @Override
   public List<StatsDeviceBatteryFilter> findAll(Specification<StatsDeviceBatteryFilter> specification) {
      return null;
   }

   @Override
   public Page<StatsDeviceBatteryFilter> findAll(Specification<StatsDeviceBatteryFilter> specification, Pageable pageable) {
      return null;
   }

   @Override
   public List<StatsDeviceBatteryFilter> findAll(Specification<StatsDeviceBatteryFilter> specification, Sort sort) {
      return null;
   }

   @Override
   public long count(Specification<StatsDeviceBatteryFilter> specification) {
      return 0;
   }
}
