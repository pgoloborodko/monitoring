package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricSettings;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BiometricSettingsRepoTest implements BiometricSettingsRepo {
    private Map<String, BiometricSettings> settingsMap = new HashMap<>();

    @Override
    public List<BiometricSettings> findAll() {
        return null;
    }

    @Override
    public List<BiometricSettings> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<BiometricSettings> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<BiometricSettings> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(BiometricSettings biometricSettings) {

    }

    @Override
    public void deleteAll(Iterable<? extends BiometricSettings> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends BiometricSettings> S save(S s) {
        settingsMap.put(s.getBiometric().getSerial(), s);
        return s;
    }

    @Override
    public <S extends BiometricSettings> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<BiometricSettings> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends BiometricSettings> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<BiometricSettings> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public BiometricSettings getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends BiometricSettings> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends BiometricSettings> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends BiometricSettings> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends BiometricSettings> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends BiometricSettings> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends BiometricSettings> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<BiometricSettings> findByBiometric(Biometric serial) {
        return Optional.of(settingsMap.get(serial.getSerial()));
    }
}
