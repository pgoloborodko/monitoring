package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricProps;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BiometricPropsRepoTest implements BiometricPropsRepo {
    private Map<String, BiometricProps> propsMap = new HashMap<>();

    @Override
    public List<BiometricProps> findAll() {
        return null;
    }

    @Override
    public List<BiometricProps> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<BiometricProps> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<BiometricProps> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(BiometricProps biometricProps) {

    }

    @Override
    public void deleteAll(Iterable<? extends BiometricProps> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends BiometricProps> S save(S s) {
        propsMap.put(s.getBiometric().getSerial(), s);
        return s;
    }

    @Override
    public <S extends BiometricProps> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<BiometricProps> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends BiometricProps> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<BiometricProps> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public BiometricProps getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends BiometricProps> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends BiometricProps> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends BiometricProps> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends BiometricProps> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends BiometricProps> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends BiometricProps> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<BiometricProps> findByBiometric(Biometric serial) {
        return Optional.of(propsMap.get(serial.getSerial()));
    }
}
