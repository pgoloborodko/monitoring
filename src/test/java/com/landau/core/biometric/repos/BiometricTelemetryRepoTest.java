package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricTelemetry;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BiometricTelemetryRepoTest implements BiometricTelemetryRepo {
    private Map<String, BiometricTelemetry> telemetryMap = new HashMap<>();
    @Override
    public List<BiometricTelemetry> findAll() {
        return null;
    }

    @Override
    public List<BiometricTelemetry> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<BiometricTelemetry> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<BiometricTelemetry> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(BiometricTelemetry biometricTelemetry) {

    }

    @Override
    public void deleteAll(Iterable<? extends BiometricTelemetry> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends BiometricTelemetry> S save(S s) {
        telemetryMap.put(s.getBiometric().getSerial(), s);
        return s;
    }

    @Override
    public <S extends BiometricTelemetry> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<BiometricTelemetry> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends BiometricTelemetry> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<BiometricTelemetry> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public BiometricTelemetry getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends BiometricTelemetry> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends BiometricTelemetry> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends BiometricTelemetry> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends BiometricTelemetry> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends BiometricTelemetry> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends BiometricTelemetry> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<BiometricTelemetry> findByBiometric(Biometric serial) {
        return Optional.of(telemetryMap.get(serial.getSerial()));
    }
}
