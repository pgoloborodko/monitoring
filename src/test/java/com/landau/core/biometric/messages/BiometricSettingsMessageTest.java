package com.landau.core.biometric.messages;

import com.landau.core.config.BiometricTestConfig;
import com.landau.objects.settings.BiometricSettings;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.BiometricTestConfig.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BiometricSettingsMessage.class)
@ContextConfiguration(classes = BiometricTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class BiometricSettingsMessageTest {
    @Autowired
    private BiometricSettingsMessage biometricSettingsMessage;

    private final static BiometricSettings settings = new BiometricSettings();

    @BeforeClass
    public static void setUp(){
        settings.setSerial(serial);
    }

//TODO переделать
    @Test
    public void consumeBiometricSettings() {
//        when(biometricSettingsMessage.getBiometricRepo().findBySerial(serial)).thenReturn(Optional.of(biometric));
//
//        biometricSettingsMessage.consumeBiometricSettings(settings);
//        Assert.assertEquals(biometricSettingsMessage.getBiometricSettingsRepo().findByBiometric_Serial(serial).get().getBiometric().getSerial(), serial);
    }
}