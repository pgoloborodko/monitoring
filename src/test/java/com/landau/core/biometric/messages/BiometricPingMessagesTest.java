package com.landau.core.biometric.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.BiometricTestConfig;
import com.landau.objects.telemetry.Ping;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.BiometricTestConfig.biometric;
import static com.landau.core.config.BiometricTestConfig.serial;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BiometricPingMessages.class)
@ContextConfiguration(classes = BiometricTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class BiometricPingMessagesTest {
    @Autowired
    private BiometricPingMessages biometricPingMessages;

    private final static Ping ping = new Ping();

    @BeforeClass
    public static void setUp(){
        ping.setSerial(serial);
    }

    @Test
    public void consumePing() {
        biometric.setOnline(true);
        biometricPingMessages.consumePing(ping);
        Assert.assertEquals(biometricPingMessages.getBiometricRepo().findBySerial(biometric.getSerial()).get(), biometric);
        try {
            verify(biometricPingMessages.getCoreBrowserMessages(), times(1)).send("", biometric);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}