package com.landau.core.biometric.messages;

import com.landau.core.config.BiometricTestConfig;
import com.landau.objects.props.BiometricProps;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.BiometricTestConfig.biometric;
import static com.landau.core.config.BiometricTestConfig.serial;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BiometricPropsMessages.class)
@ContextConfiguration(classes = BiometricTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class BiometricPropsMessagesTest {
    @Autowired
    private BiometricPropsMessages biometricPropsMessages;

    private final static BiometricProps biometricProps = new BiometricProps();

    @BeforeClass
    public static void setUp(){
        biometricProps.setSerial(serial);
    }

    @Test
    public void consumeBiometricProps() {
        biometricPropsMessages.consumeBiometricProps(biometricProps);

        assertEquals(biometricPropsMessages.getBiometricPropsRepo().findByBiometric(biometric).get().getBiometric().getSerial(), serial);
    }
}