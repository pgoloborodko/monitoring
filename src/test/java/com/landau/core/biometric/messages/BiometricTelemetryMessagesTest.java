package com.landau.core.biometric.messages;

import com.landau.core.config.BiometricTestConfig;
import com.landau.objects.telemetry.BiometricTelemetry;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.BiometricTestConfig.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BiometricTelemetryMessages.class)
@ContextConfiguration(classes = BiometricTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class BiometricTelemetryMessagesTest {
    @Autowired
    private BiometricTelemetryMessages biometricTelemetryMessages;

    private static final BiometricTelemetry biometricTelemetry = new BiometricTelemetry();

    @BeforeClass
    public static void setUp(){
        biometricTelemetry.setSerial(serial);
    }

    //TODO переделать
    @Test
    public void consumeBiometricTelemetry() {
//        when(biometricTelemetryMessages.getBiometricRepo().findBySerial(serial)).thenReturn(Optional.of(biometric));
//
//        biometricTelemetryMessages.consumeBiometricTelemetry(biometricTelemetry);
//
//        Assert.assertEquals(biometricTelemetryMessages.getBiometricTelemetryRepo().findByBiometric_Serial(serial).get().getBiometric().getSerial(), serial);
    }
}