package com.landau.core.common;

import com.landau.core.common.messages.CastService;
import com.landau.core.config.CastServiceTestConfig;
import com.landau.core.devices.entities.DeviceProperties;
import com.landau.objects.action.CreateChatMessage;
import com.landau.objects.data.AppList;
import com.landau.objects.data.ChatMessage;
import com.landau.objects.events.biometric.BiometricFaceRecognitionEvent;
import com.landau.objects.events.biometric.BiometricFaceTrackingEvent;
import com.landau.objects.events.device.DeviceButtonEvent;
import com.landau.objects.events.device.DeviceInstallerEvent;
import com.landau.objects.events.device.DeviceStreamEvent;
import com.landau.objects.events.device.DeviceSystemEvent;
import com.landau.objects.events.terminal.TerminalFileEvent;
import com.landau.objects.events.terminal.TerminalSyncEvent;
import com.landau.objects.events.terminal.TerminalSystemEvent;
import com.landau.objects.props.BiometricProps;
import com.landau.objects.props.DeviceProps;
import com.landau.objects.props.TerminalProps;
import com.landau.objects.settings.BiometricSettings;
import com.landau.objects.settings.DeviceSettings;
import com.landau.objects.settings.TerminalSettings;
import com.landau.objects.stats.DeviceBatteryStats;
import com.landau.objects.stats.DeviceSpeedDistanceStats;
import com.landau.objects.stats.TerminalDevicesStats;
import com.landau.objects.stats.TerminalSyncStats;
import com.landau.objects.telemetry.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CastService.class)
@ContextConfiguration(classes = CastServiceTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class CastServiceTest {
    @Autowired
    private CastService castService;

    private static final DeviceSettings deviceSettings = new DeviceSettings();
    private static final TerminalFileEvent terminalFileEvent = new TerminalFileEvent();
    private static final TerminalSyncEvent terminalSyncEvent = new TerminalSyncEvent();
    private static final TerminalSystemEvent terminalSystemEvent = new TerminalSystemEvent();
    private static final DeviceSystemEvent deviceSystemEvent = new DeviceSystemEvent();
    private static final DeviceStreamEvent deviceStreamEvent = new DeviceStreamEvent();
    private static final DeviceButtonEvent deviceButtonEvent = new DeviceButtonEvent();
    private static final DeviceInstallerEvent deviceInstallerEvent = new DeviceInstallerEvent();
    private static final BiometricFaceTrackingEvent biometricFaceTrackingEvent = new BiometricFaceTrackingEvent();
    private static final BiometricFaceRecognitionEvent biometricFaceRecognitionEvent = new BiometricFaceRecognitionEvent();
    private static final AppList.App app = new AppList.App();
    private static final TerminalTelemetry terminalTelemetry = new TerminalTelemetry();
    private static final TerminalSyncStats terminalSyncStats = new TerminalSyncStats();
    private static final TerminalDevicesStats terminalDevicesStats = new TerminalDevicesStats();
    private static final TerminalSettings terminalSettings = new TerminalSettings();
    private static final TerminalProps terminalProps = new TerminalProps();
    private static final WiFiTelemetry wiFiTelemetry = new WiFiTelemetry();
    private static final DeviceTelemetry deviceTelemetry = new DeviceTelemetry();
    private static final Location location = new Location();
    private static final DeviceBatteryStats deviceBatteryStats = new DeviceBatteryStats();
    private static final DeviceSpeedDistanceStats deviceSpeedDistanceStats = new DeviceSpeedDistanceStats();
    private static final DeviceProps deviceProps = new DeviceProps();
    private static final ChatMessage chatMessage = new ChatMessage();
    private static final BiometricTelemetry biometricTelemetry = new BiometricTelemetry();
    private static final BiometricSettings biometricSettings = new BiometricSettings();
    private static final BiometricProps biometricProps = new BiometricProps();
    private static final CreateChatMessage createChatMessage = new CreateChatMessage();
    private static final DeviceProperties deviceProperties = new DeviceProperties();
    private static final com.landau.core.terminals.entities.TerminalSettings entityTerminalSettings = new com.landau.core.terminals.entities.TerminalSettings();
    private static final com.landau.core.chat.entities.ChatMessage entityChatMessage = new com.landau.core.chat.entities.ChatMessage();
    private static final com.landau.core.tasks.entities.Task task = new com.landau.core.tasks.entities.Task();
    private static final String serial = "testSerial";

    @BeforeClass
    public static void setUp(){
        setSerial(serial);
    }


    //TODO переделать
    @Test
    public void castToEntityMediaFile() {
//        Assert.assertEquals(castService.castToEntity(terminalFileEvent).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventTerminalSync() {
        Assert.assertEquals(castService.castToEntity(terminalSyncEvent).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventTerminalSystem() {
        Assert.assertEquals(castService.castToEntity(terminalSystemEvent).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventDeviceSystem() {
        Assert.assertEquals(castService.castToEntity(deviceSystemEvent).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventDeviceStream() {
        Assert.assertEquals(castService.castToEntity(deviceStreamEvent).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventDeviceButton() {
        Assert.assertEquals(castService.castToEntity(deviceButtonEvent).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventDeviceInstaller() {
        Assert.assertEquals(castService.castToEntity(deviceInstallerEvent).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventBiometricRecognitionTracking() {
        Assert.assertEquals(castService.castToEntity(biometricFaceTrackingEvent).getBiometric().getSerial(), serial);
    }

    @Test
    public void testCastToEntityEventBiometricRecognition() {
        Assert.assertEquals(castService.castToEntity(biometricFaceRecognitionEvent).getBiometric().getSerial(), serial);
    }

    @Test
    public void testCastToEntityApp() {
        Assert.assertEquals(castService.castToEntity(app).getName(), serial);
    }

    @Test
    public void testCastToEntityTerminalTelemetry() {
        Assert.assertEquals(castService.castToEntity(terminalTelemetry).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityTerminalSyncStats() {
        Assert.assertEquals(castService.castToEntity(terminalSyncStats).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityTerminalDevicesStats() {
        Assert.assertEquals(castService.castToEntity(terminalDevicesStats).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityTerminalSettings() {
        Assert.assertEquals(castService.castToEntity(terminalSettings).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityTerminalProps() {
        //TODO переделать
//        Assert.assertEquals(castService.castToEntity(terminalProps).getTerminal().getSerial(), serial);
    }

    @Test
    public void testCastToEntityDeviceTelemetry() {
        Assert.assertEquals(castService.castToEntity(deviceTelemetry).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityLocation() {
        Assert.assertEquals(castService.castToEntity(location).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityStatsBattery() {
        Assert.assertEquals(castService.castToEntity(deviceBatteryStats).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityStatsSpeedDistance() {
        Assert.assertEquals(castService.castToEntity(deviceSpeedDistanceStats).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityDevice() {
        Assert.assertEquals(castService.castToEntity(deviceProps).getSerial(), serial);
    }

    @Test
    public void testCastToEntityDeviceProperties() {
        Assert.assertEquals(castService.castToEntity(deviceProps).getSerial(), serial);
    }

    //TODO переделать
    @Test
    public void testCastToEntityChatMessage() {
//        Assert.assertEquals(castService.castToEntity(deviceSettings).getDevice().getSerial(), serial);
    }

    @Test
    public void testCastToEntityBiometricTelemetry() {
        Assert.assertEquals(castService.castToEntity(biometricTelemetry).getBiometric().getSerial(), serial);
    }

    @Test
    public void testCastToEntityBiometricSettings() {
        Assert.assertEquals(castService.castToEntity(biometricSettings).getBiometric().getSerial(), serial);
    }

    @Test
    public void testCastToEntityBiometricProps() {
        Assert.assertEquals(castService.castToEntity(biometricProps).getBiometric().getSerial(), serial);
    }
//
//    @Test
//    public void testCastToEntityCreateChatMessage() {
//    }
//TODO переделать
    @Test
    public void castToMessageSetDeviceSettings() {
//        deviceProperties.setRewrite(true);
//        Assert.assertEquals(castService.castToMessage(deviceProperties).getRewrite(), deviceProperties.getRewrite());
    }

    @Test
    public void testCastToMessageSetTerminalSettings() {
        entityTerminalSettings.setRewrite(true);
        Assert.assertEquals(castService.castToMessage(entityTerminalSettings).getRewrite(), entityTerminalSettings.getRewrite());
    }
//
//    @Test
//    public void testCastToMessageChatMessage() {
//    }

    @Test
    public void testCastToMessageTask() {
        task.setContent("test");
        Assert.assertEquals(castService.castToMessage(task).getContent(), task.getContent());
    }

//    @Test
//    public void testCastToEntityUser() {
//    }

    private static void setSerial(String serial){
        deviceSettings.setSerial(serial);
        terminalFileEvent.setSerial(serial);
        terminalSyncEvent.setSerial(serial);
        terminalSystemEvent.setSerial(serial);
        deviceSystemEvent.setSerial(serial);
        deviceStreamEvent.setSerial(serial);
        deviceButtonEvent.setSerial(serial);
        deviceInstallerEvent.setSerial(serial);
        biometricFaceTrackingEvent.setSerial(serial);
        biometricFaceRecognitionEvent.setSerial(serial);
        app.setName(serial);
        terminalTelemetry.setSerial(serial);
        terminalSyncStats.setSerial(serial);
        terminalDevicesStats.setSerial(serial);
        terminalSettings.setSerial(serial);
        terminalProps.setSerial(serial);
        wiFiTelemetry.setSerial(serial);
        deviceTelemetry.setSerial(serial);
        location.setSerial(serial);
        deviceBatteryStats.setSerial(serial);
        deviceSpeedDistanceStats.setSerial(serial);
        deviceProps.setSerial(serial);
        chatMessage.setSerial(serial);
        biometricTelemetry.setSerial(serial);
        biometricSettings.setSerial(serial);
        biometricProps.setSerial(serial);
        createChatMessage.setSerial(serial);
//        deviceProperties.setDevice(deviceRepo.findBySerial(serial).get());
//        entityTerminalSettings.setTerminal(terminalRepo.findBySerial(serial).get());
//        entityChatMessage.setUser(userRepo.findByDevice(deviceRepo.findBySerial(serial).get()).get());
//        task.setMakerUser(userRepo.findByDevice(deviceRepo.findBySerial(serial).get()).get());
    }
}