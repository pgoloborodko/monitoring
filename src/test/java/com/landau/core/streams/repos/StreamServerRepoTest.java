package com.landau.core.streams.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.streams.entities.StreamServer;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class StreamServerRepoTest implements StreamServerRepo {
    @Override
    public List<StreamServer> findAllByCompanyAndAvailable(Company company, Boolean available) {
        return null;
    }

    @Override
    public List<StreamServer> findAllByCompanyAndUnavailableSinceAfter(Company company, Date date) {
        return null;
    }

    @Override
    public List<StreamServer> findAllByAvailable(Boolean available) {
        return null;
    }

    @Override
    public List<StreamServer> findAllByUnavailableSinceAfter(Date date) {
        return null;
    }

    @Override
    public List<StreamServer> findByStatusAndStateAndCompanyOrderByAllTrafficAsc(boolean b, boolean b1, Company company) {
        return null;
    }

    @Override
    public List<StreamServer> findAllByCompany(Company company) {
        return null;
    }

    @Override
    public Optional<StreamServer> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public List<StreamServer> findAll() {
        return null;
    }

    @Override
    public List<StreamServer> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<StreamServer> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<StreamServer> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(StreamServer streamServer) {

    }

    @Override
    public void deleteAll(Iterable<? extends StreamServer> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends StreamServer> S save(S s) {
        return null;
    }

    @Override
    public <S extends StreamServer> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<StreamServer> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends StreamServer> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<StreamServer> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public StreamServer getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends StreamServer> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends StreamServer> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends StreamServer> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends StreamServer> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StreamServer> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends StreamServer> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<StreamServer> findOne(Specification<StreamServer> specification) {
        return Optional.empty();
    }

    @Override
    public List<StreamServer> findAll(Specification<StreamServer> specification) {
        return null;
    }

    @Override
    public Page<StreamServer> findAll(Specification<StreamServer> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<StreamServer> findAll(Specification<StreamServer> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<StreamServer> specification) {
        return 0;
    }
}
