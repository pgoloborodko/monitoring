package com.landau.core.config;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.apps.repos.AppRepoTest;
import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.biometric.repos.BiometricRepoTest;
import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.chat.repos.ChatRoomRepoTest;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.*;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.terminals.repos.TerminalRepoTest;
import com.landau.core.terminals.repos.UserRepoTest;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.Mockito.mock;

@ContextConfiguration
public class CastServiceTestConfig {
    public static final String serial = "testSerial";

    public Terminal createTerminal(){
        Terminal terminal = new Terminal();
        terminal.setSerial(serial);
        return terminal;
    }

    public Device createDevice(){
        Device device = new Device();
        device.setSerial(serial);
        return device;
    }

    public User createUser(){
        User user = new User();
        user.setDevice(createDevice());
        return user;
    }

    public App createApp(){
        App app = new App();
        app.setName(serial);
        return app;
    }

    public Location createLocation(){
        Location app = new Location();
        app.setDevice(createDevice());
        return app;
    }


    public Biometric createBiometric(){
        Biometric biometric = new Biometric();
        biometric.setSerial(serial);
        return biometric;
    }


    @Bean
    public DeviceRepo deviceRepo(){
        DeviceRepoTest deviceRepoTest = new DeviceRepoTest();
        deviceRepoTest.save(createDevice());
        return deviceRepoTest;
    }

    @Bean
    public TerminalRepo terminalRepo(){
        TerminalRepoTest terminalRepoTest = new TerminalRepoTest();
        terminalRepoTest.save(createTerminal());
        return terminalRepoTest;
    }



    @Bean
    public LocationRepo locationRepo(){
        LocationRepoTest locationRepoTest = new LocationRepoTest();
        locationRepoTest.save(createLocation());
        return locationRepoTest;
    }

    @Bean
    public DeviceModelRepo deviceModelRepo(){
        return new DeviceModelRepoTest();
    }

    @Bean
    public BiometricRepo biometricRepo(){
        BiometricRepoTest biometricRepoTest = new BiometricRepoTest();
        biometricRepoTest.save(createBiometric());
        return biometricRepoTest;
    }

    @Bean
    public AppRepo appRepo(){
        AppRepoTest appRepoTest = new AppRepoTest();
        appRepoTest.save(createApp());
        return appRepoTest;
    }

    @Bean
    public UserRepo userRepo(){
        UserRepoTest userRepoTest = new UserRepoTest();
        userRepoTest.save(createUser());
        return userRepoTest;
    }

    @Bean
    public CastService castService(){
        return new CastService();
    }


    @Bean
    public ChatRoomRepo chatRoomRepo(){
        return new ChatRoomRepoTest();
    }

    @Bean
    public DeviceSettingsRepo deviceSettingsRepo(){
        return mock(DeviceSettingsRepo.class);
    }
}
