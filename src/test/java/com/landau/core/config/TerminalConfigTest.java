package com.landau.core.config;

import com.landau.core.common.messages.CastService;
import com.landau.core.devices.repos.DevicePropertiesRepo;
import com.landau.core.devices.repos.DevicePropertiesRepoTest;
import com.landau.core.events.messages.TerminalEventsMessages;
import com.landau.core.events.repos.*;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.stats.messages.TerminalStatsMessages;
import com.landau.core.stats.repos.*;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.messages.*;
import com.landau.core.terminals.repos.*;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.messages.core.CoreTerminalMessages;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.Mockito.mock;

@ContextConfiguration
@Import(CastServiceTestConfig.class)
public class TerminalConfigTest {
    public static final String serial = "testSerial";
    public static final Terminal terminal = new Terminal();
    static {
        terminal.setSerial(serial);
    }

    @Bean
    public CastService castService(){
        return new CastService();
    }

    @Bean
    public EventTerminalSyncRepo eventTerminalSyncRepo(){
        return new EventTerminalSyncRepoTest();
    }

    @Bean
    public EventTerminalSystemRepo eventTerminalSystemRepo(){
        return new EventTerminalSystemRepoTest();
    }

    @Bean
    public MediaFileRepo mediaFileRepo(){
        return new MediaFileRepoTest();
    }

    @Bean
    public TerminalDevicesStatsRepo terminalDevicesStatsRepo(){
        return new TerminalDevicesStatsRepoTest();
    }

    @Bean
    public TerminalSyncStatsRepo terminalSyncStatsRepo(){
        return new TerminalSyncStatsRepoTest();
    }

    @Bean
    public TerminalStatsRepo terminalStatsRepo(){
        return new TerminalStatsRepoTest();
    }

    @Bean
    public TerminalPropsRepo terminalPropsRepo(){
        return new TerminalPropsRepoTest();
    }

    @Bean
    public TerminalTelemetryRepo terminalTelemetryRepo(){
        return new TerminalTelemetryRepoTest();
    }

    @Bean
    public TerminalSettingsRepo terminalSettingsRepo(){
        return new TerminalSettingsRepoTest();
    }

    @Bean
    public DevicePropertiesRepo devicePropertiesRepo(){
        return new DevicePropertiesRepoTest();
    }

    @Bean
    public CoreTerminalMessages coreTerminalMessages(){
        return mock(CoreTerminalMessages.class);
    }

    @Bean
    public CoreBrowserMessages coreBrowserMessages(){
        return mock(CoreBrowserMessages.class);
    }

    @Bean
    public TerminalEventsMessages terminalEventsMessages(){
        return new TerminalEventsMessages();
    }

    @Bean
    public TerminalStatsMessages terminalStatsMessages(){
        return new TerminalStatsMessages();
    }

    @Bean
    public TerminalDataMessages terminalDataMessages(){
        return new TerminalDataMessages();
    }

    @Bean
    public TerminalLogMessages terminalLogMessages(){
        return new TerminalLogMessages();
    }

//    @Bean
//    public TerminalPageMessages terminalPageMessages(){
//        return new TerminalPageMessages();
//    }

    @Bean
    public TerminalPingMessages terminalPingMessages(){
        return new TerminalPingMessages();
    }

    @Bean
    public TerminalPropsMessages terminalPropsMessages(){
        return new TerminalPropsMessages();
    }

    @Bean
    public TerminalSettingsMessages terminalSettingsMessages(){
        return new TerminalSettingsMessages();
    }

    @Bean
    public TerminalTelemetryMessages terminalTelemetryMessages(){
        return new TerminalTelemetryMessages();
    }

    @Bean
    public UserDevicesSyncRepo userDevicesSyncRepo(){
        return mock(UserDevicesSyncRepo.class);
    }

}
