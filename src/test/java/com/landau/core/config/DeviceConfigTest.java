package com.landau.core.config;

import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.apps.repos.*;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.messages.*;
import com.landau.core.devices.repos.*;
import com.landau.core.events.messages.DeviceEventsMessages;
import com.landau.core.events.repos.*;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.messages.DeviceStatsMessages;
import com.landau.core.stats.repos.*;
import com.landau.core.streams.messages.DeviceStreamMessages;
import com.landau.core.streams.repos.StreamServerRepo;
import com.landau.core.streams.repos.StreamServerRepoTest;
import com.landau.core.streams.services.StreamService;
import com.landau.core.tasks.messages.DeviceTaskMessages;
import com.landau.core.tasks.repos.TaskRepo;
import com.landau.core.tasks.repos.TaskRepoTest;
import com.landau.core.tasks.repos.TaskUserRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.messages.core.CoreDeviceMessages;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.Mockito.mock;

@ContextConfiguration
@Import(BiometricTestConfig.class)
public class DeviceConfigTest {
    public final static Device device = new Device();
    public final static String serial = "testSerial";

    static {
        device.setSerial(serial);
    }

    @Bean
    public DeviceRepo deviceRepo(){
        DeviceRepoTest deviceRepoTest = new DeviceRepoTest();
        device.setId(1L);
        device.setSerial(serial);
        deviceRepoTest.save(device);
        return deviceRepoTest;
    }

    @Bean
    public AppRepo appRepo(){
        return mock(AppRepo.class);
    }

    @Bean
    public AppDeviceCommandRepo appDeviceCommandRepo(){
        DeviceAppCommandRepoTest deviceAppCommandRepoTest = new DeviceAppCommandRepoTest();
        AppDeviceCommand appDeviceCommand = new AppDeviceCommand();
        appDeviceCommand.setDevice(device);
        appDeviceCommand.setComplete(false);
        deviceAppCommandRepoTest.save(appDeviceCommand);
        return deviceAppCommandRepoTest;
    }

    @Bean
    public AppDeviceRepo deviceAppRepo(){
        return new DeviceAppRepoTest();
    }

    @Bean
    public EventDeviceButtonRepo eventDeviceButtonRepo(){
        return new EventDeviceButtonRepoTest();
    }
    @Bean
    public EventDeviceInstallerRepo eventDeviceInstallerRepo(){
        return new EventDeviceInstallerRepoTest();
    }
    @Bean
    public EventDeviceStreamRepo eventDeviceStreamRepo(){
        return new EventDeviceStreamRepoTest();
    }
    @Bean
    public EventDeviceSystemRepo eventDeviceSystemRepo(){
        return new EventDeviceSystemRepoTest();
    }

    @Bean
    public CoreDeviceMessages coreDeviceMessages(){
        return mock(CoreDeviceMessages.class);
    }

    @Bean
    public CoreBrowserMessages coreBrowserMessages(){
        return mock(CoreBrowserMessages.class);
    }

    @Bean
    public DeviceSettingsRepo deviceSettingsRepo(){
        return mock(DeviceSettingsRepo.class);
    }

    @Bean
    public DeviceStreamMessages deviceStreamMessages(){
        return mock(DeviceStreamMessages.class);
    }

    @Bean
    public StreamService streamService(){
        return mock(StreamService.class);
    }

    @Bean
    public DeviceAppMessages deviceAppMessages(){
        return mock(DeviceAppMessages.class);
    }

    @Bean
    public StatsDeviceOnlineUptimeRepo uptimeRepo(){
        StatsDeviceOnlineUptimeRepoTest statsDeviceOnlineUptimeRepoTest = new StatsDeviceOnlineUptimeRepoTest();

        StatsDeviceOnlineUptime uptime = new StatsDeviceOnlineUptime();
        uptime.setDevice(device);
        statsDeviceOnlineUptimeRepoTest.save(uptime);

        return statsDeviceOnlineUptimeRepoTest;
    }

    @Bean
    public StatsDeviceBatteryRepo statsDeviceBatteryRepo(){
        return new StatsDeviceBatteryRepoTest();
    }

    @Bean
    public StatsDeviceSpeedDistanceRepo statsDeviceSpeedDistanceRepo(){
        return new StatsDeviceSpeedDistanceRepoTest();
    }

    @Bean
    public TaskUserRepo taskUserRepo(){
        return mock(TaskUserRepo.class);
    }

    @Bean
    public StatsStreamServerRepo statsStreamServerRepo(){
        return  new StatsStreamServerRepoTest();
    }

    @Bean
    public DevicePropertiesRepo devicePropertiesRepo() {
        DevicePropertiesRepoTest devicePropertiesRepoTest = new DevicePropertiesRepoTest();
//        DeviceProperties deviceProperties = new DeviceProperties();
//        deviceProperties.setDevice(device);
//        devicePropertiesRepoTest.save(deviceProperties);
        return devicePropertiesRepoTest;

    }

    @Bean
    public LocationRepo locationRepo() {
        return new LocationRepoTest();

    }

    @Bean
    public DeviceWifiLogRepo deviceWifiLogRepo() {
        return new DeviceWifiLogRepoTest();

    }

    @Bean
    public DeviceTelemetryRepo deviceTelemetryRepo() {
        return new DeviceTelemetryRepoTest();

    }

    @Bean
    public StreamServerRepo streamServerRepo() {
        return new StreamServerRepoTest();

    }

    @Bean
    public TaskRepo taskRepo() {
        return new TaskRepoTest();

    }

    @Bean
    public DeviceTelemetryMessages deviceTelemetryMessages(){
        return new DeviceTelemetryMessages();
    }

    @Bean
    public DevicePingMessages devicePingMessages(){
        return new DevicePingMessages();
    }

    @Bean
    public DeviceLogMessages deviceLogMessages(){
        return new DeviceLogMessages();
    }

    @Bean
    public DeviceSettingsMessages deviceSettingsMessages(){
        return new DeviceSettingsMessages();
    }

    @Bean
    public DeviceEventsMessages deviceEventsMessages(){
        return new DeviceEventsMessages();
    }

    @Bean
    public DeviceStatsMessages deviceStatsMessages(){
        return new DeviceStatsMessages();
    }

    @Bean
    public DeviceTaskMessages deviceTaskMessages(){
        return new DeviceTaskMessages();
    }

    @Bean
    public DevicePropsMessages devicePropsMessages(){
        return new DevicePropsMessages();
    }
}
