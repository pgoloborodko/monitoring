package com.landau.core.config;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.apps.repos.AppRepoTest;
import com.landau.core.apps.repos.DeviceAppRepoTest;
import com.landau.core.cameras.repos.RemoteCameraRepo;
import com.landau.core.chat.entities.ChatMessage;
import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.chat.messages.DeviceChatMessages;
import com.landau.core.chat.repos.ChatMessageRepo;
import com.landau.core.chat.repos.ChatMessageRepoTest;
import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.chat.repos.ChatRoomRepoTest;
import com.landau.core.common.services.ControllerService;
import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.messages.DeviceLogMessages;
import com.landau.core.devices.messages.DeviceSettingsMessages;
import com.landau.core.devices.repos.DevicePropertiesRepo;
import com.landau.core.devices.repos.DevicePropertiesRepoTest;
import com.landau.core.events.repos.*;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.streams.messages.DeviceStreamMessages;
import com.landau.core.tasks.entities.Task;
import com.landau.core.tasks.messages.DeviceTaskMessages;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.messages.TerminalLogMessages;
import com.landau.core.terminals.messages.TerminalPageMessages;
import com.landau.core.terminals.messages.TerminalSettingsMessages;
import com.landau.core.terminals.repos.TerminalSettingsRepo;
import com.landau.core.terminals.repos.TerminalSettingsRepoTest;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.RoleRepo;
import com.landau.messages.core.CoreTerminalMessages;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;

@Import(DeviceConfigTest.class)
@ContextConfiguration
public class RestConfig {
    public final static Device device = new Device();
    public final static Terminal terminal = new Terminal();
    public static final String test = "testSerial";
    public static final App app = new App();
    public static final AppDeviceCommand appDeviceCommand = new AppDeviceCommand();
    public static final ChatRoom chatRoom = new ChatRoom();
    public static final ChatMessage chatMessage = new ChatMessage();
    public static final User user = new User();
    public static final PageableImpl pageable = new PageableImpl();
    public static final AppDevice appDevice = new AppDevice();
    public static final Company company = new Company();
    public static final Task task = new Task();


    static {
        terminal.setSerial(test);
        terminal.setOnline(true);
        device.setSerial(test);
        device.setOnline(true);
        company.setName(test);
        user.setCompany(company);
        user.setFirstName(test);
        user.setDevice(device);
        user.setId(1L);
        app.setId(1L);
        app.setName(test);

        chatRoom.setCompany(company);
        chatRoom.setMembers(new ArrayList<>());
        chatRoom.setAdmin(user);
        chatRoom.getMembers().add(user);

        chatMessage.setSender(user);
        chatMessage.setChatRoom(chatRoom);

        appDeviceCommand.setApp(app);
        appDeviceCommand.setDevice(device);

        appDevice.setDevice(device);
        task.setMakerUser(user);
    }

    @Bean
    public ControllerService controllerService(){
        return mock(ControllerService.class);
    }

    @Bean
    public RemoteCameraRepo remoteCameraRepo(){
        return mock(RemoteCameraRepo.class);
    }

    @Bean
    public AppRepo appRepo(){
        AppRepoTest appRepoTest = new AppRepoTest();
        appRepoTest.save(app);
        return appRepoTest;
    }

    @Bean
    public AppDeviceRepo deviceAppRepo(){
        DeviceAppRepoTest deviceAppRepoTest = new DeviceAppRepoTest();
        deviceAppRepoTest.save(appDevice);
        return deviceAppRepoTest;
    }

    @Bean
    public TerminalSettingsRepo terminalSettingsRepo(){
        return new TerminalSettingsRepoTest();
    }

    @Bean
    public DevicePropertiesRepo devicePropertiesRepo(){
        return new DevicePropertiesRepoTest();
    }

    @Bean
    public RoleRepo roleRepo(){
        return mock(RoleRepo.class);
    }

    @Bean
    public ModelMapper modelMapper(){
        return mock(ModelMapper.class);
    }

    @Bean
    public EventTerminalSyncRepo eventTerminalSyncRepo(){
        return new EventTerminalSyncRepoTest();
    }

    @Bean
    public EventTerminalSystemRepo eventTerminalSystemRepo(){
        return new EventTerminalSystemRepoTest();
    }

    @Bean
    public MediaFileRepo mediaFileRepo(){
        return new MediaFileRepoTest();
    }

    @Bean
    public ChatMessageRepo chatMessageRepo(){
        ChatMessageRepoTest chatMessageRepoTest = new ChatMessageRepoTest();
        chatMessageRepoTest.save(chatMessage);
        return chatMessageRepoTest;
    }

    @Bean
    public ChatRoomRepo chatRoomRepo(){
        ChatRoomRepoTest chatRoomRepoTest = new ChatRoomRepoTest();
        chatRoomRepoTest.save(chatRoom);
        return chatRoomRepoTest;
    }

    @Bean
    public DeviceChatMessages deviceChatMessages(){
        return mock(DeviceChatMessages.class);
    }

    @Bean
    public DeviceLogMessages deviceLogMessages(){
        return mock(DeviceLogMessages.class);
    }

    @Bean
    public DeviceSettingsMessages deviceSettingsMessages(){
        return mock(DeviceSettingsMessages.class);
    }

    @Bean
    public DeviceStreamMessages deviceStreamMessages(){
        return mock(DeviceStreamMessages.class);
    }

    @Bean
    public DeviceTaskMessages deviceTaskMessages(){
        return mock(DeviceTaskMessages.class);
    }

    @Bean
    public TerminalLogMessages terminalLogMessages(){
        return mock(TerminalLogMessages.class);
    }

    @Bean
    public TerminalSettingsMessages terminalSettingsMessages(){
        return mock(TerminalSettingsMessages.class);
    }

    @Bean
    public TerminalPageMessages terminalPageMessages(){
        return mock(TerminalPageMessages.class);
    }

    @Bean
    public CoreTerminalMessages coreTerminalMessages(){
        return mock(CoreTerminalMessages.class);
    }
//
//    @Bean
//    public UserService userService(){
//        return mock(UserService.class);
//    }
//
//    @Bean
//    public EntityManager entityManager(){
//        return mock(EntityManager.class);
//    }
//
//    @Bean
//    public BCryptPasswordEncoder bCryptPasswordEncoder(){
//        return mock(BCryptPasswordEncoder.class);
//    }
}
