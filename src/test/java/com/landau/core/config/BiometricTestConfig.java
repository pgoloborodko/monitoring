package com.landau.core.config;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.messages.BiometricPingMessages;
import com.landau.core.biometric.messages.BiometricPropsMessages;
import com.landau.core.biometric.messages.BiometricSettingsMessage;
import com.landau.core.biometric.messages.BiometricTelemetryMessages;
import com.landau.core.biometric.repos.*;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.repos.DeviceModelRepo;
import com.landau.core.devices.repos.DeviceModelRepoTest;
import com.landau.core.events.messages.BiometricEventsMessages;
import com.landau.core.events.repos.BiometricRepoTest;
import com.landau.core.events.repos.EventBiometricRecognitionRepo;
import com.landau.core.events.repos.EventBiometricRecognitionRepoTest;
import com.landau.messages.core.CoreBrowserMessages;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;

import static org.mockito.Mockito.mock;

@Import(CastServiceTestConfig.class)
@ContextConfiguration
public class BiometricTestConfig {
    public final static Biometric biometric = new Biometric();
    public static final String serial = "testSerial";

    static {
        biometric.setSerial(serial);
    }

    @Bean
    public EventBiometricRecognitionRepo eventBiometricRecognitionRepo(){
        return new EventBiometricRecognitionRepoTest();
    }

    @Bean
    public BiometricRepo biometricRepo(){
        BiometricRepoTest biometricRepoTest= new BiometricRepoTest();
        biometricRepoTest.save(biometric);
        return biometricRepoTest;
    }

    @Bean
    public BiometricPropsRepo biometricPropsRepo(){
        return new BiometricPropsRepoTest();
    }

    @Bean
    public BiometricSettingsRepo biometricSettingsRepo(){
        return new BiometricSettingsRepoTest();
    }

    @Bean
    public BiometricTelemetryRepo biometricTelemetryRepo(){
        return new BiometricTelemetryRepoTest();
    }
    @Bean
    public DeviceModelRepo deviceModelRepo(){
        return new DeviceModelRepoTest();
    }

    @Bean
    public CoreBrowserMessages coreBrowserMessages(){
        return mock(CoreBrowserMessages.class);
    }

    @Bean
    public CastService castService(){
        return new CastService();
    }

    @Bean
    public BiometricPingMessages biometricPingMessages(){
        return new BiometricPingMessages();
    }

    @Bean
    public BiometricPropsMessages biometricPropsMessages(){
        return new BiometricPropsMessages();
    }

    @Bean
    public BiometricSettingsMessage biometricSettingsMessage(){
        return new BiometricSettingsMessage();
    }

    @Bean
    public BiometricTelemetryMessages biometricTelemetryMessages(){
        return new BiometricTelemetryMessages();
    }

    @Bean
    public BiometricEventsMessages biometricEventsMessages(){
        return new BiometricEventsMessages();
    }
}
