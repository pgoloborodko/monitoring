package com.landau.core.apps.controllers;

import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.apps.repos.AppDeviceCommandRepo;
import com.landau.core.common.services.ControllerService;
import com.landau.core.config.RestConfig;
import com.landau.core.devices.entities.Device;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.RestConfig.*;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class AppInstallerTest {

    private AppInstaller appInstaller;

    @Autowired
    private ControllerService controllerService;

    @Autowired
    private DeviceAppMessages deviceAppMessages;

    @Autowired
    private AppDeviceCommandRepo appDeviceCommandRepo;

    @Before
    public void setUp(){
        appInstaller = new AppInstaller(deviceAppMessages, controllerService);

        when(appInstaller.getControllerService().verifyApp(app)).thenReturn(app);
        when(appInstaller.getControllerService().verifyDevice(device)).thenReturn(device);
        when(appInstaller.getControllerService().createDeviceAPKCommand(device, app, AppDeviceCommand.Type.INSTALL)).thenReturn(appDeviceCommand);
        when(appInstaller.getControllerService().createDeviceAPKCommand(device, app, AppDeviceCommand.Type.UNINSTALL)).thenReturn(appDeviceCommand);
        when(appInstaller.getControllerService().getAppDeviceCommandRepo()).thenReturn(appDeviceCommandRepo);
    }

    @Test
    public void requestInstallAPKBadRequest() {
        device.setOnline(false);
        appInstaller = new AppInstaller(deviceAppMessages, controllerService);
        List<Device> deviceList = new ArrayList<>();
        deviceList.add(device);
        try {
            appInstaller.requestInstallAPK(app, deviceList);
        }catch (ResponseStatusException e){
            Assert.assertEquals(e.getReason(), "400");
        }
    }

    @Test
    public void requestInstallAPK() {
        device.setOnline(true);
        List<Device> deviceList = new ArrayList<>();
        deviceList.add(device);

        appInstaller.requestInstallAPK(app, deviceList);
        Assert.assertEquals(appInstaller.getControllerService().getAppDeviceCommandRepo()
                .findByDevice_Serial(device.getSerial()).get().getDevice().getSerial(), test);
    }

    @Test
    public void requestUninstallAPKBadRequest() {
        device.setOnline(false);

        List<Device> deviceList = new ArrayList<>();
        deviceList.add(device);
        try {
            appInstaller.requestUninstallAPK(app, deviceList);
        }catch (ResponseStatusException e){
            Assert.assertEquals(e.getReason(), "400");
        }
    }

    @Test
    public void requestUninstallAPK() {
        device.setOnline(true);

        List<Device> deviceList = new ArrayList<>();
        deviceList.add(device);
        appInstaller.requestUninstallAPK(app, deviceList);
        Assert.assertEquals(appInstaller.getControllerService().getAppDeviceCommandRepo()
                .findByDevice_Serial(device.getSerial()).get().getDevice().getSerial(), test);
    }
}