package com.landau.core.apps.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.config.RestConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.landau.core.config.RestConfig.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class AppListTest {
    private AppList appList;

    @Autowired
    private AppDeviceRepo appDeviceRepo;

    @Autowired
    private DeviceAppMessages deviceAppMessages;

    @Autowired
    private AppRepo appRepo;

    @Before
    public void setUp(){
        appList = new AppList(appDeviceRepo, deviceAppMessages, appRepo);
        }

    @Test
    public void get() {
        Page<App> appPage = appList.get(pageable);
        List<App> appList = appPage.getContent();
        Assert.assertEquals(appList.get(0).getName(), test);
    }

    @Test
    public void get1() {
        Page<AppDevice> appDevicePage = appList.get(device, pageable);
        List<AppDevice> appDevices = appDevicePage.getContent();
        Assert.assertEquals(appDevices.get(0).getDevice().getSerial(), test);
    }

    @Test
    public void requestInstalledAPKListServerError() {
        device.setOnline(false);
        try {
            appList.requestInstalledAPKList(device);
        } catch (ResponseStatusException e){
            Assert.assertEquals(e.getReason(), "device offline");
        }
    }

    @Test
    public void requestInstalledAPKList() {
        device.setOnline(true);

        appList.requestInstalledAPKList(device);

        try {
            Mockito.verify(appList.getDeviceAppMessages(), Mockito.times(1)).getApkList(device);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}