package com.landau.core.apps.repos;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.devices.entities.Device;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class DeviceAppRepoTest implements AppDeviceRepo {
    private Map<Device, AppDevice> appDeviceMap = new HashMap<>();

    @Override
    public Optional<AppDevice> findByApp(App app) {
        return Optional.empty();
    }


    @Override
    public void deleteAllByDevice(Device device) {
        appDeviceMap.clear();
    }

    @Override
    public Page<AppDevice> findAllByDevice(Device device, Pageable pageable) {
        return new PageImpl<>(new ArrayList<>(appDeviceMap.values()));
    }

    @Override
    public Optional<AppDevice> findByDevice(Device device) {
        return Optional.of(appDeviceMap.get(device));
    }

    @Override
    public List<AppDevice> findAll() {
        return null;
    }

    @Override
    public List<AppDevice> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<AppDevice> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<AppDevice> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(AppDevice AppDevice) {

    }

    @Override
    public void deleteAll(Iterable<? extends AppDevice> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends AppDevice> S save(S s) {
        appDeviceMap.put(s.getDevice(), s);
        return s;
    }

    @Override
    public <S extends AppDevice> List<S> saveAll(Iterable<S> iterable) {
        for (S s: iterable){
            appDeviceMap.put(s.getDevice(), s);
        }
        return null;
    }

    @Override
    public Optional<AppDevice> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends AppDevice> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<AppDevice> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public AppDevice getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends AppDevice> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends AppDevice> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends AppDevice> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends AppDevice> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends AppDevice> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends AppDevice> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<AppDevice> findOne(Specification<AppDevice> specification) {
        return Optional.empty();
    }

    @Override
    public List<AppDevice> findAll(Specification<AppDevice> specification) {
        return null;
    }

    @Override
    public Page<AppDevice> findAll(Specification<AppDevice> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<AppDevice> findAll(Specification<AppDevice> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<AppDevice> specification) {
        return 0;
    }
}
