package com.landau.core.apps.repos;

import com.landau.core.apps.entities.App;
import lombok.Data;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

@Data
public class AppRepoTest implements AppRepo {
    private Map<String, App> appMap = new HashMap<>();

    @Override
    public Optional<App> findByName(String name) {
        return Optional.of(appMap.get(name));
    }

    @Override
    public List<App> findAll() {
        return null;
    }

    @Override
    public List<App> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<App> findAll(Pageable pageable) {
        return new PageImpl<>(new ArrayList<>(appMap.values()));
    }

    @Override
    public List<App> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(App app) {

    }

    @Override
    public void deleteAll(Iterable<? extends App> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends App> S save(S s) {
        appMap.put(s.getName(), s);
        return s;
    }

    @Override
    public <S extends App> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<App> findById(Long aLong) {
        List<App> list = new ArrayList<>(appMap.values());
        return Optional.of(list.get(0));
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends App> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<App> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public App getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends App> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends App> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends App> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends App> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends App> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends App> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<App> findOne(Specification<App> specification) {
        return Optional.empty();
    }

    @Override
    public List<App> findAll(Specification<App> specification) {
        return null;
    }

    @Override
    public Page<App> findAll(Specification<App> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<App> findAll(Specification<App> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<App> specification) {
        return 0;
    }
}
