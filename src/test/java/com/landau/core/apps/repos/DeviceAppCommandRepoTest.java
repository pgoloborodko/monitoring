package com.landau.core.apps.repos;

import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.devices.entities.Device;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.*;

public class DeviceAppCommandRepoTest implements AppDeviceCommandRepo {
    private Map<String, AppDeviceCommand> commandMap = new HashMap<>();

    @Override
    public List<AppDeviceCommand> findAllByDeviceAndComplete(Device device, boolean b) {
        List<AppDeviceCommand> list = new ArrayList<>();
        for (Map.Entry<String, AppDeviceCommand> entry: commandMap.entrySet()){
            if (entry.getValue().getDevice().equals(device) && !entry.getValue().getComplete()){
                list.add(entry.getValue());
            }
        }
        return list;
    }

    @Override
    public Optional<AppDeviceCommand> findByDevice_Serial(String serial) {
        return Optional.of(commandMap.get(serial));
    }

    @Override
    public List<AppDeviceCommand> findAll() {
        return null;
    }

    @Override
    public List<AppDeviceCommand> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<AppDeviceCommand> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<AppDeviceCommand> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(AppDeviceCommand AppDeviceCommand) {

    }

    @Override
    public void deleteAll(Iterable<? extends AppDeviceCommand> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends AppDeviceCommand> S save(S s) {
        commandMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends AppDeviceCommand> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<AppDeviceCommand> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends AppDeviceCommand> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<AppDeviceCommand> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public AppDeviceCommand getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends AppDeviceCommand> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends AppDeviceCommand> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends AppDeviceCommand> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends AppDeviceCommand> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends AppDeviceCommand> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends AppDeviceCommand> boolean exists(Example<S> example) {
        return false;
    }
}
