package com.landau.core.apps.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.config.DeviceConfigTest;
import com.landau.objects.action.GetAppList;
import com.landau.objects.action.InstallApp;
import com.landau.objects.action.UninstallApp;
import com.landau.objects.data.AppList;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.Optional;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceAppMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceAppMessagesTest {

    @Autowired
    private DeviceAppMessages deviceAppMessages;

    private final static AppDeviceCommand deviceAppCommand = new AppDeviceCommand();
    private final static AppList appList = new AppList();
    private final static App app = new App();

    @BeforeClass
    public static void setUp(){
        app.setName(serial);
        app.setUrl(serial);
        app.setAppPackage(serial);
        appList.setSerial(serial);
        deviceAppCommand.setDevice(device);
        deviceAppCommand.setApp(app);
        appList.setApps(new ArrayList<>());
        for (int i = 0; i < 10; i++){
            AppList.App app = new AppList.App();
            app.setName(serial);
            appList.getApps().add(app);
        }
    }


    @Test
    public void consumeApkList() {
        when(deviceAppMessages.getCastService().getAppRepo().findByName(serial)).thenReturn(Optional.of(app));

        deviceAppMessages.consumeApkList(appList);

        Assert.assertEquals(deviceAppMessages.getAppDeviceRepo().findByDevice(device).get().getDevice().getSerial(), serial);
    }

    @Test
    public void installApk() {
        try {
            InstallApp installApp = new InstallApp();
            installApp.setUrl(deviceAppCommand.getApp().getUrl());
            deviceAppMessages.installApk(deviceAppCommand);
            Assert.assertEquals(deviceAppMessages.getAppDeviceCommandRepo().findByDevice_Serial(serial).get().getDevice().getSerial(), serial);
            verify(deviceAppMessages.getMessages(), times(1)).send(serial, installApp);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void uninstallApk() {
        try {
            UninstallApp uninstallApp = new UninstallApp();
            uninstallApp.setAppPackage(deviceAppCommand.getApp().getAppPackage());
            deviceAppMessages.uninstallApk(deviceAppCommand);
            Assert.assertEquals(deviceAppMessages.getAppDeviceCommandRepo().findByDevice_Serial(serial).get().getDevice().getSerial(), serial);
            verify(deviceAppMessages.getMessages(), times(1)).send(serial, uninstallApp);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getApkList() {
        try {
            deviceAppMessages.getApkList(device);
            verify(deviceAppMessages.getMessages(), times(1)).send(device.getSerial(), new GetAppList());
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}