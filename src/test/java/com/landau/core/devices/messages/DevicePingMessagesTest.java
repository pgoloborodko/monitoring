package com.landau.core.devices.messages;

import com.landau.core.config.DeviceConfigTest;
import com.landau.objects.telemetry.Ping;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DevicePingMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DevicePingMessagesTest {
    @Autowired
    private DevicePingMessages devicePingMessages;

    private final static Ping ping = new Ping();

    @BeforeClass
    public static void setUp(){
        ping.setSerial(serial);
    }

    //TODO переделать
    @Test
    public void consumePing() {
//        Republish republish = mock(Republish.class);
//        MessageService messageService = mock(MessageService.class);
//        when(devicePingMessages.getBrowser().getMessageService()).thenReturn(messageService);
//        when(devicePingMessages.getBrowser().getMessageService().getRepublish()).thenReturn(republish);
//        devicePingMessages.consumePing(ping);
//        device.setOnline(true);
//        Assert.assertEquals(devicePingMessages.getDeviceRepo().findBySerial(device.getSerial()).get(), device);
//
//        try {
//            verify(devicePingMessages.getBrowser().getMessageService().getRepublish(), times(2)).produce(ArgumentMatchers.any());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }
}