package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.DeviceConfigTest;
import com.landau.objects.props.DeviceProps;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.serial;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DevicePropsMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DevicePropsMessagesTest {
    @Autowired
    private DevicePropsMessages devicePropsMessages;

    private static final DeviceProps deviceProps = new DeviceProps();

    @BeforeClass
    public static void setUp(){
        deviceProps.setSerial(serial);
    }

    @Test
    public void consumeDeviceProps() {
        try {
            devicePropsMessages.consumeDeviceProps(deviceProps);

            verify(devicePropsMessages.getDeviceAppMessages(), times(1)).installApk(devicePropsMessages.getAppDeviceCommandRepo().findByDevice_Serial(serial).get());
            Assert.assertEquals(true, devicePropsMessages.getAppDeviceCommandRepo()
                    .findByDevice_Serial(serial).get().getComplete());
            Assert.assertEquals(devicePropsMessages.getStatsDeviceOnlineUptimeRepo()
                    .findByDevice_Serial(serial).get().getDevice().getSerial(), serial);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
//        command
//        Assert.assertEquals(devicePropsMessages.getDeviceAppMessages().getAppDeviceCommandRepo().findByDevice_Serial(serial), );
    }
}