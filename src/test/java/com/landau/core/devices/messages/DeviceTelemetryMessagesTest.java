package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.core.Republish;
import com.landau.core.config.DeviceConfigTest;
import com.landau.core.devices.entities.DeviceTelemetry;
import com.landau.core.devices.entities.DeviceWifiLog;
import com.landau.core.locations.entities.Location;
import com.landau.messages.Interfaces.MessageService;
import com.landau.objects.telemetry.WiFiTelemetry;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceTelemetryMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceTelemetryMessagesTest {
    @Autowired
    private DeviceTelemetryMessages deviceTelemetryMessages;

    private final static Location location = new Location();
    private final static com.landau.objects.telemetry.Location deviceLocation = new com.landau.objects.telemetry.Location();
    private final static WiFiTelemetry wiFiTelemetry = new WiFiTelemetry();
    private final static DeviceTelemetry deviceTelemetry = new DeviceTelemetry();
    private final static com.landau.objects.telemetry.DeviceTelemetry deviceTelemetryMessage = new com.landau.objects.telemetry.DeviceTelemetry();
    private final static DeviceWifiLog wifiLog = new DeviceWifiLog();

    @BeforeClass
    public static void setUp(){
        deviceLocation.setSerial(serial);

        deviceTelemetryMessage.setSerial(serial);
        deviceTelemetry.setDevice(device);
        wiFiTelemetry.setSerial(serial);
        location.setDevice(device);
        wifiLog.setDevice(device);
    }

    @Test
    public void consumeDeviceLocation() {
        try {
            Republish republish = mock(Republish.class);
            MessageService messageService = mock(MessageService.class);
            when(deviceTelemetryMessages.getBrowser().getMessageService()).thenReturn(messageService);
            when(deviceTelemetryMessages.getBrowser().getMessageService().getRepublish()).thenReturn(republish);

            deviceTelemetryMessages.consumeDeviceLocation(deviceLocation);
            device.setLocation(location);

            Assert.assertEquals(deviceTelemetryMessages.getLocationRepo().findByDevice_Serial(serial).get(), location);
            Assert.assertEquals(deviceTelemetryMessages.getDeviceRepo().findBySerial(serial).get(), device);

            verify(deviceTelemetryMessages.getBrowser().getMessageService().getRepublish(), times(1)).produce(ArgumentMatchers.any());

        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void consumeWifiObject() {
        deviceTelemetryMessages.consumeWifiObject(wiFiTelemetry);
        Assert.assertEquals(deviceTelemetryMessages.getDeviceWifiLogRepo().findByDevice(device).get(), wifiLog);

    }

    //TODO переделать
    @Test
    public void consumeDeviceTelemetry() {
//        deviceTelemetryMessages.consumeDeviceTelemetry(deviceTelemetryMessage);
//        Assert.assertEquals(deviceTelemetryMessages.getDeviceTelemetryRepo().findByDevice(device).get(), deviceTelemetry);
    }
}