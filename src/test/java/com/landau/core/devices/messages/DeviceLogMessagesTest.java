package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.DeviceConfigTest;
import com.landau.objects.action.GetLogFile;
import com.landau.objects.action.GetLogList;
import com.landau.objects.data.LogFile;
import com.landau.objects.data.LogList;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceLogMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceLogMessagesTest {
    @Autowired
    private DeviceLogMessages deviceLogMessages;

    private final static String file = "testFile";
    private final static LogList logList = new LogList();
    private final static List<LogList.Log> logs = new ArrayList<>();
    private final static LogFile logFile = new LogFile();

    @BeforeClass
    public static void setUp(){
        for (int i = 0; i < 10; i++){
            LogList.Log log = new LogList.Log();
            log.setName(file);
            logs.add(log);
        }
        logList.setLogs(logs);
        logList.setSerial(serial);
        logFile.setSerial(serial);
    }


    @Test
    public void getLogList() {
        try {
            deviceLogMessages.getLogList(device);
            Assert.assertNull(deviceLogMessages.getMessagesStack().get(device.getSerial()).getResult());
            verify(deviceLogMessages.getMessages(), times(1)).send(device.getSerial(), new GetLogList());
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getLogFile() {
        try {
            deviceLogMessages.getLogFile(device, file);
            Assert.assertNull(deviceLogMessages.getMessagesStack().get(device.getSerial()).getResult());
            verify(deviceLogMessages.getMessages(), times(1)).send(device.getSerial(), new GetLogFile(file));
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void consumeLogList() {
        deviceLogMessages.getMessagesStack().put(logList.getSerial(), new DeferredResult<>());
        deviceLogMessages.consumeLogList(logList);
        Assert.assertEquals(deviceLogMessages.getMessagesStack().size(), 0);
    }

    @Test
    public void consumeLogFile() {
        deviceLogMessages.getMessagesStack().put(logFile.getSerial(), new DeferredResult<>());
        deviceLogMessages.consumeLogFile(logFile);
        Assert.assertEquals(deviceLogMessages.getMessagesStack().size(), 0);
    }
}