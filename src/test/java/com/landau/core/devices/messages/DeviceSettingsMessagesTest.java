package com.landau.core.devices.messages;

import com.landau.core.config.DeviceConfigTest;
import com.landau.core.devices.entities.DeviceProperties;
import com.landau.objects.settings.DeviceSettings;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceSettingsMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceSettingsMessagesTest {

    @Autowired
    private DeviceSettingsMessages deviceSettingsMessages;

    private final static DeviceProperties deviceProperties = new DeviceProperties();
    private final static DeviceSettings deviceSettings = new DeviceSettings();

    @BeforeClass
    public static void setUp(){
        deviceProperties.setDevice(device);
        deviceSettings.setSerial(serial);

    }
    //TODO переделать
    @Test
    public void setDeviceSettings() {
//        try {
//            deviceSettingsMessages.setDeviceSettings(deviceProperties);
//            Assert.assertEquals(deviceSettingsMessages.getDevicePropertiesRepo().findByDevice(device).get(), deviceProperties);
//            verify(deviceSettingsMessages.getMessages(), times(1)).send(serial, new SetDeviceSettings());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }

    }
    //TODO переделать
    @Test
    public void consumeDeviceSettings() {
//        deviceSettingsMessages.consumeDeviceSettings(deviceSettings);
//        Assert.assertEquals(deviceSettingsMessages.getDevicePropertiesRepo().findByDevice(device).get(), deviceProperties);
    }
}