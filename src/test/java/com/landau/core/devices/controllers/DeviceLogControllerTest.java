package com.landau.core.devices.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.RestConfig;
import com.landau.core.devices.messages.DeviceLogMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.RestConfig.device;
import static com.landau.core.config.RestConfig.test;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class DeviceLogControllerTest {

    private DeviceLogController deviceLogController;

    @Autowired
    private DeviceLogMessages deviceLogMessages;

    @Before
    public void setUp(){
        deviceLogController = new DeviceLogController(deviceLogMessages);
    }

    @Test
    public void requestDeviceLogList() {
        deviceLogController.requestDeviceLogList(device);

        try {
            Mockito.verify(deviceLogController.getDeviceLogMessages(), Mockito.times(1)).getLogList(device);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void requestDeviceLogFile() {
        deviceLogController.requestDeviceLogFile(device, test);

        try {
            Mockito.verify(deviceLogController.getDeviceLogMessages(), Mockito.times(1)).getLogFile(device, test);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}