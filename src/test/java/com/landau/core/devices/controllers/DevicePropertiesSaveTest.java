package com.landau.core.devices.controllers;

import com.landau.core.config.RestConfig;
import com.landau.core.devices.messages.DeviceSettingsMessages;
import com.landau.core.devices.repos.DevicePropertiesRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class DevicePropertiesSaveTest {
    private DevicePropertiesSave devicePropertiesSave;

    @Autowired
    private DeviceSettingsMessages deviceSettingsMessages;

    @Autowired
    private DevicePropertiesRepo devicePropertiesRepo;

    @Before
    public void setUp(){
        devicePropertiesSave = new DevicePropertiesSave(devicePropertiesRepo, deviceSettingsMessages);
    }

    //TODO переделать
    @Test
    public void setDeviceSettings() {
//        devicePropertiesSave.setDeviceSettings(new DeviceProperties());
//
//        try {
//            Mockito.verify(devicePropertiesSave.getDeviceSettingsMessages(), Mockito.times(1))
//                    .setDeviceSettings(new DeviceProperties());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }
}