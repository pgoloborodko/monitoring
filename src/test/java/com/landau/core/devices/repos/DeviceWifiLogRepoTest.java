package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceWifiLog;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.*;

public class DeviceWifiLogRepoTest implements DeviceWifiLogRepo {
    private Map<String, DeviceWifiLog> wifiLogMap = new HashMap<>();

    @Override
    public Optional<DeviceWifiLog> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public List<DeviceWifiLog> findByAddedDateBetweenAndDevice(Pageable pageable, Date start, Date end, Device device) {
        return null;
    }

    @Override
    public List<DeviceWifiLog> findAllByDeviceAndAddedBetween(Pageable pageable, Device device, Date start, Date end) {
        return null;
    }

    @Override
    public DeviceWifiLog findFirstByDeviceOrderByIdDesc(Device device) {
        return null;
    }

    @Override
    public Optional<DeviceWifiLog> findByDevice(Device device) {
        return Optional.of(wifiLogMap.get(device.getSerial()));
    }

    @Override
    public List<DeviceWifiLog> findAll() {
        return null;
    }

    @Override
    public List<DeviceWifiLog> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<DeviceWifiLog> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<DeviceWifiLog> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(DeviceWifiLog deviceWifiLog) {

    }

    @Override
    public void deleteAll(Iterable<? extends DeviceWifiLog> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends DeviceWifiLog> S save(S s) {
        wifiLogMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends DeviceWifiLog> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends DeviceWifiLog> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<DeviceWifiLog> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public DeviceWifiLog getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends DeviceWifiLog> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends DeviceWifiLog> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends DeviceWifiLog> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends DeviceWifiLog> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DeviceWifiLog> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends DeviceWifiLog> boolean exists(Example<S> example) {
        return false;
    }
}
