package com.landau.core.devices.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.users.entities.User;
import lombok.Data;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Data
public class DeviceRepoTest implements DeviceRepo {
    private Map<String, Device> deviceMap = new HashMap<>();

    @Override
    public List<Device> findByOnline(Boolean online) {
        return null;
    }

    @Override
    public void deleteAllByName(String name) {

    }

    @Override
    public Page<Device> findAllByCompany(Pageable pageable, Company company) {
        return null;
    }

    @Override
    public List<Device> findAllByCompanyAndOnline(Company company, Boolean online) {
        return null;
    }

    @Override
    public List<Device> findAllByCompany(Company company) {
        return null;
    }

    @Override
    public Long countAllByOnline(Boolean online) {
        return null;
    }

    @Override
    public Optional<Device> findByUser(User user) {
        return Optional.empty();
    }

    @Override
    public Integer updateStatus(Boolean online) {
        return null;
    }

    @Override
    public Device findNearest() {
        return null;
    }

    @Override
    public Optional<Device> findBySerial(String serial) {
        return Optional.of(deviceMap.get(serial));
    }

    @Override
    public List<Device> findAll() {
        return null;
    }

    @Override
    public List<Device> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Device> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Device> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Device device) {

    }

    @Override
    public void deleteAll(Iterable<? extends Device> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Device> S save(S s) {
        deviceMap.put(s.getSerial(), s);
        return s;
    }

    @Override
    public <S extends Device> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Device> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Device> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Device> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Device getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Device> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Device> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Device> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Device> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Device> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Device> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<Device> findOne(Specification<Device> specification) {
        return Optional.empty();
    }

    @Override
    public List<Device> findAll(Specification<Device> specification) {
        return null;
    }

    @Override
    public Page<Device> findAll(Specification<Device> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Device> findAll(Specification<Device> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Device> specification) {
        return 0;
    }
}
