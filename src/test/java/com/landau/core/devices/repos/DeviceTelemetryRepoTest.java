package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceTelemetry;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class DeviceTelemetryRepoTest implements DeviceTelemetryRepo {
    private Map<Device, DeviceTelemetry> telemetryMap = new HashMap<>();

    @Override
    public DeviceTelemetry findFirstByDeviceOrderByIdDesc(Device device) {
        return null;
    }

    @Override
    public DeviceTelemetry findSecondByDeviceOrderByIdDesc(Device device) {
        return null;
    }

    @Override
    public List<DeviceTelemetry> findFirst10ByIsChargingAndDeviceOrderByIdDesc(Boolean charging, Device device) {
        return null;
    }

    @Override
    public void deleteAllByDeviceId(Long deviceId) {

    }

    @Override
    public void deleteAllByCreatedAndDevice(Date date, Device device) {

    }

    @Override
    public Optional<DeviceTelemetry> findFirstByDeviceAndCreatedBetweenOrderByIdAsc(Device device, Date from, Date to) {
        return Optional.empty();
    }

    @Override
    public Optional<DeviceTelemetry> findByDevice(Device device) {
        return Optional.of(telemetryMap.get(device));
    }

    @Override
    public List<DeviceTelemetry> findAll() {
        return null;
    }

    @Override
    public List<DeviceTelemetry> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<DeviceTelemetry> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<DeviceTelemetry> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(DeviceTelemetry deviceTelemetry) {

    }

    @Override
    public void deleteAll(Iterable<? extends DeviceTelemetry> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends DeviceTelemetry> S save(S s) {
        telemetryMap.put(s.getDevice(), s);
        return s;
    }

    @Override
    public <S extends DeviceTelemetry> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<DeviceTelemetry> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends DeviceTelemetry> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<DeviceTelemetry> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public DeviceTelemetry getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends DeviceTelemetry> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends DeviceTelemetry> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends DeviceTelemetry> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends DeviceTelemetry> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DeviceTelemetry> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends DeviceTelemetry> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<DeviceTelemetry> findOne(Specification<DeviceTelemetry> specification) {
        return Optional.empty();
    }

    @Override
    public List<DeviceTelemetry> findAll(Specification<DeviceTelemetry> specification) {
        return null;
    }

    @Override
    public Page<DeviceTelemetry> findAll(Specification<DeviceTelemetry> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<DeviceTelemetry> findAll(Specification<DeviceTelemetry> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<DeviceTelemetry> specification) {
        return 0;
    }
}
