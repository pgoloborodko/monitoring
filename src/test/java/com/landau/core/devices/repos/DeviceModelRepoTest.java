package com.landau.core.devices.repos;

import com.landau.core.devices.entities.DeviceModel;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public class DeviceModelRepoTest implements DeviceModelRepo {
    @Override
    public Optional<DeviceModel> findFirstByManufacturerAndModelAndFirmware(String manufacturer, String model, String firmware) {
        return Optional.empty();
    }

    @Override
    public List<DeviceModel> findAll() {
        return null;
    }

    @Override
    public List<DeviceModel> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<DeviceModel> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<DeviceModel> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(DeviceModel deviceModel) {

    }

    @Override
    public void deleteAll(Iterable<? extends DeviceModel> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends DeviceModel> S save(S s) {
        return null;
    }

    @Override
    public <S extends DeviceModel> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<DeviceModel> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends DeviceModel> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<DeviceModel> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public DeviceModel getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends DeviceModel> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends DeviceModel> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends DeviceModel> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends DeviceModel> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DeviceModel> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends DeviceModel> boolean exists(Example<S> example) {
        return false;
    }
}
