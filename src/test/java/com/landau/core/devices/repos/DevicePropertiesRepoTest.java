package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceProperties;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DevicePropertiesRepoTest implements DevicePropertiesRepo{
    private Map<String, DeviceProperties> propertiesMap = new HashMap<>();
    @Override
    public List<DeviceProperties> findAll() {
        return null;
    }

    @Override
    public List<DeviceProperties> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<DeviceProperties> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<DeviceProperties> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(DeviceProperties deviceProperties) {

    }

    @Override
    public void deleteAll(Iterable<? extends DeviceProperties> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends DeviceProperties> S save(S s) {
        propertiesMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends DeviceProperties> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<DeviceProperties> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends DeviceProperties> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<DeviceProperties> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public DeviceProperties getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends DeviceProperties> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends DeviceProperties> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends DeviceProperties> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends DeviceProperties> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DeviceProperties> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends DeviceProperties> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<DeviceProperties> findByDevice(Device serial) {
        return Optional.of(propertiesMap.get(serial.getSerial()));
    }
}
