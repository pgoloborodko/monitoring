package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.*;

public class LocationRepoTest implements LocationRepo {
    private Map<String, Location> locationMap = new HashMap<>();
    @Override
    public List<Location> findByCreatedDateBetweenAndDevice(Pageable pageable, Date start, Date end, Device device) {
        return null;
    }

    @Override
    public List<Location> findAllByDeviceAndCreatedBetween(Pageable pageable, Device device, Date start, Date end) {
        return null;
    }

    @Override
    public Location findFirstByDeviceOrderByIdDesc(Device device) {
        return null;
    }

    @Override
    public List<Location> findAllByCreatedBetweenAndDeviceIn(Date start, Date end, List<Device> device) {
        return null;
    }

    @Override
    public List<Location> findLastLocations(Date start, Date end, List<Device> device) {
        return null;
    }

    @Override
    public List findByCreatedDateBetweenAndDevice(Date start, Date end, Device device) {
        return null;
    }

    @Override
    public List findByCreatedDateBetweenAndUser(Date start, Date end, User user) {
        return null;
    }

    @Override
    public List<Location> findFirst10ByDeviceOrderByIdDesc(Device device) {
        return null;
    }

    @Override
    public List<Location> findByCreatedDateBetweenAndDeviceOrderByIdDesc(Date start, Date end, Device device) {
        return null;
    }

    @Override
    public Optional<Location> findLastByDevice(Device device) {
        return Optional.empty();
    }

    @Override
    public Optional<Location> findByCreated(Date date) {
        return Optional.empty();
    }

    @Override
    public Optional<Location> findLastByUser(User user) {
        return Optional.empty();
    }

    @Override
    public List<Location> findLastByUserAndCreatedBetweenOrderByCreatedDesc(User user, Date created_date, Date created_date2) {
        return null;
    }

    @Override
    public Optional<Location> findByDevice_Serial(String serial) {
        return Optional.of(locationMap.get(serial));
    }

    @Override
    public List<Location> findAll() {
        return null;
    }

    @Override
    public List<Location> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Location> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Location> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Location location) {

    }

    @Override
    public void deleteAll(Iterable<? extends Location> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Location> S save(S s) {
        locationMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends Location> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Location> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Location> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Location> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Location getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Location> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Location> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Location> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Location> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Location> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Location> boolean exists(Example<S> example) {
        return false;
    }
}
