package com.landau.core.controllers.devices;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.services.ControllerService;
import com.landau.core.devices.entities.Device;
import com.landau.core.tasks.controllers.TaskController;
import com.landau.core.tasks.entities.Task;
import com.landau.core.tasks.messages.DeviceTaskMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskController.class)
public class TaskControllerTest {
    @Autowired
    private TaskController taskController;

    @MockBean
    private ControllerService controllerService;

    @MockBean
    private DeviceTaskMessages deviceTaskMessages;

    private Device device;
    private Task task;

    @Before
    public void setUp(){
        taskController.setControllerService(controllerService);
        taskController.setDeviceTaskMessages(deviceTaskMessages);
        device = new Device();
        task = new Task();
    }

    @Test
    public void shouldSendTask() {
        try {
            when(controllerService.verifyDevice(device)).thenReturn(device);
            when(deviceTaskMessages.sendTask(task, device)).thenReturn(task);

            assertEquals(taskController.sendTask(task, device), task);

            verify(controllerService, times(1)).verifyDevice(device);
            verify(deviceTaskMessages, times(1)).sendTask(task, device);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldNotSendTask() {
        when(controllerService.verifyDevice(device)).thenReturn(null);
        try {
            taskController.sendTask(task, device);
        } catch (ResponseStatusException e){
            try {
                verify(deviceTaskMessages, times(0)).sendTask(task, device);
                verify(controllerService, times(1)).verifyDevice(device);
            } catch (RepublisherException f) {
                f.printStackTrace();
            }
            assertEquals("400 BAD_REQUEST \"400\"", e.getMessage());
        }
    }
}