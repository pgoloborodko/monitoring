package com.landau.core.controllers.devices.apk;

import com.landau.core.apps.controllers.AppInstaller;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.common.services.ControllerService;
import com.landau.core.devices.entities.Device;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppInstaller.class)
public class InstallerTest {
    @Autowired
    private AppInstaller installer;

    @MockBean
    private ControllerService controllerService;

    @MockBean
    private DeviceAppMessages deviceAppMessages;

    private Device device;
    private AppDevice deviceApk;
    private AppDeviceCommand deviceApkCommand;

    @Before
    public void setUp(){
        installer.setControllerService(controllerService);
        installer.setDeviceAppMessages(deviceAppMessages);
        device = new Device();
        deviceApk = new AppDevice();
        deviceApkCommand = new AppDeviceCommand();
    }


    @Test
    public void shouldInstallAPK() {
//        when(controllerService.verifyDevice(device)).thenReturn(device);
//        when(controllerService.verifyDeviceAPK(deviceApk)).thenReturn(deviceApk);
//        when(controllerService.createDeviceAPKCommand(device, deviceApk, DeviceAppCommand.Type.INSTALL)).thenReturn(deviceApkCommand);
//
//        assertEquals(installer.requestInstallAPK(device, deviceApk), deviceApkCommand);
//
//        try {
//            verify(deviceAppMessages, times(1)).installApk(deviceApkCommand);
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }

    @Test
    public void shouldUninstallAPK() {
//        when(controllerService.verifyDevice(device)).thenReturn(device);
//        when(controllerService.verifyDeviceAPK(deviceApk)).thenReturn(deviceApk);
//        when(controllerService.createDeviceAPKCommand(device, deviceApk, DeviceAppCommand.Type.UNINSTALL)).thenReturn(deviceApkCommand);
//
//        assertEquals(installer.requestUninstallAPK(device, deviceApk), deviceApkCommand);
//
//        try {
//            verify(deviceAppMessages, times(1)).installApk(deviceApkCommand);
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }

    @Test
    public void shouldNotInstallAPK() {
//        when(controllerService.verifyDevice(device)).thenReturn(null);
//        when(controllerService.verifyDeviceAPK(deviceApk)).thenReturn(deviceApk);
//        when(controllerService.createDeviceAPKCommand(device, deviceApk, DeviceAppCommand.Type.INSTALL)).thenReturn(deviceApkCommand);
//
//        try {
//            installer.requestInstallAPK(device, deviceApk);
//        } catch (ResponseStatusException e){
//            verify(controllerService, times(0)).createDeviceAPKCommand(device, deviceApk, DeviceAppCommand.Type.INSTALL);
//            try {
//                verify(deviceAppMessages, times(0)).installApk(deviceApkCommand);
//            } catch (RepublisherException f) {
//                f.printStackTrace();
//            }
//
//            assertEquals("400 BAD_REQUEST \"400\"", e.getMessage());
//        }
    }

    @Test
    public void shouldNotUninstallAPK() {
//        when(controllerService.verifyDevice(device)).thenReturn(null);
//        when(controllerService.verifyDeviceAPK(deviceApk)).thenReturn(deviceApk);
//        when(controllerService.createDeviceAPKCommand(device, deviceApk, DeviceAppCommand.Type.UNINSTALL)).thenReturn(deviceApkCommand);
//
//        try {
//            installer.requestUninstallAPK(device, deviceApk);
//        } catch (ResponseStatusException e){
//            verify(controllerService, times(0)).createDeviceAPKCommand(device, deviceApk, DeviceAppCommand.Type.UNINSTALL);
//            try {
//                verify(deviceAppMessages, times(0)).installApk(deviceApkCommand);
//            } catch (RepublisherException f) {
//                f.printStackTrace();
//            }
//
//            assertEquals("400 BAD_REQUEST \"400\"", e.getMessage());
//        }
    }
}