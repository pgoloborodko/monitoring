package com.landau.core.controllers;

import java.util.Random;

/**
 * @author Alexey Belov
 */
public class MsisdnGenerator {

    public static final long LOWER_RANGE = 79000000000L; //assign lower range value
    public static final long UPPER_RANGE = 79999999999L; //assign upper range value
    private final Random random = new Random();

    public long getNextMsisdn() {
        return LOWER_RANGE +
                (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
    }
}
