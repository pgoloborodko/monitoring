package com.landau.core.events.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.companies.entities.Company;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BiometricRepoTest implements BiometricRepo {
    private Map<String, Biometric> biometricMap = new HashMap<>();

    @Override
    public Optional<Biometric> findByIdAndCompany(Long id, Company company) {
        return Optional.empty();
    }

    @Override
    public Optional<Biometric> findBySerial(String serial) {
        return Optional.of(biometricMap.get(serial));
    }

    @Override
    public Integer updateStatus(Boolean online) {
        return null;
    }

    @Override
    public List<Biometric> findAll() {
        return null;
    }

    @Override
    public List<Biometric> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Biometric> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Biometric> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Biometric biometric) {

    }

    @Override
    public void deleteAll(Iterable<? extends Biometric> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Biometric> S save(S s) {
        biometricMap.put(s.getSerial(), s);
        return s;
    }

    @Override
    public <S extends Biometric> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Biometric> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Biometric> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Biometric> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Biometric getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Biometric> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Biometric> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Biometric> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Biometric> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Biometric> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Biometric> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<Biometric> findOne(Specification<Biometric> specification) {
        return Optional.empty();
    }

    @Override
    public List<Biometric> findAll(Specification<Biometric> specification) {
        return null;
    }

    @Override
    public Page<Biometric> findAll(Specification<Biometric> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Biometric> findAll(Specification<Biometric> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Biometric> specification) {
        return 0;
    }
}
