package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceSystem;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class EventDeviceSystemRepoTest implements EventDeviceSystemRepo {
    private Map<String, EventDeviceSystem> eventMap = new HashMap<>();

    @Override
    public List<EventDeviceSystem> findAll() {
        return null;
    }

    @Override
    public List<EventDeviceSystem> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventDeviceSystem> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceSystem> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventDeviceSystem eventDeviceSystem) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventDeviceSystem> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventDeviceSystem> S save(S s) {
        eventMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends EventDeviceSystem> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventDeviceSystem> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventDeviceSystem> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventDeviceSystem> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventDeviceSystem getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventDeviceSystem> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventDeviceSystem> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventDeviceSystem> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventDeviceSystem> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventDeviceSystem> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventDeviceSystem> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventDeviceSystem> findOne(Specification<EventDeviceSystem> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventDeviceSystem> findAll(Specification<EventDeviceSystem> specification) {
        return null;
    }

    @Override
    public Page<EventDeviceSystem> findAll(Specification<EventDeviceSystem> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceSystem> findAll(Specification<EventDeviceSystem> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventDeviceSystem> specification) {
        return 0;
    }

    @Override
    public Optional<EventDeviceSystem> findByDevice(Device serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }
}
