package com.landau.core.events.repos;

import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.terminals.entities.Terminal;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class EventTerminalSyncRepoTest implements EventTerminalSyncRepo {
    private Map<String, EventTerminalSync> eventMap = new HashMap<>();

    @Override
    public Optional<EventTerminalSync> findByTerminal(Terminal serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }

    @Override
    public List<EventTerminalSync> findAll() {
        return null;
    }

    @Override
    public List<EventTerminalSync> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventTerminalSync> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventTerminalSync> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventTerminalSync eventTerminalSync) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventTerminalSync> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventTerminalSync> S save(S s) {
        eventMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends EventTerminalSync> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventTerminalSync> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventTerminalSync> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventTerminalSync> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventTerminalSync getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventTerminalSync> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventTerminalSync> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventTerminalSync> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventTerminalSync> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventTerminalSync> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventTerminalSync> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventTerminalSync> findOne(Specification<EventTerminalSync> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventTerminalSync> findAll(Specification<EventTerminalSync> specification) {
        return null;
    }

    @Override
    public Page<EventTerminalSync> findAll(Specification<EventTerminalSync> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventTerminalSync> findAll(Specification<EventTerminalSync> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventTerminalSync> specification) {
        return 0;
    }
}
