package com.landau.core.events.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.events.entities.EventBiometricRecognition;
import lombok.Data;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Data
public class EventBiometricRecognitionRepoTest implements EventBiometricRecognitionRepo {
    private Map<Biometric, EventBiometricRecognition> eventMap = new HashMap<>();

    @Override
    public List<EventBiometricRecognition> findAll() {
        return null;
    }

    @Override
    public List<EventBiometricRecognition> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventBiometricRecognition> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventBiometricRecognition> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventBiometricRecognition eventBiometricRecognition) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventBiometricRecognition> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventBiometricRecognition> S save(S s) {
        eventMap.put(s.getBiometric(), s);
        return s;
    }

    @Override
    public <S extends EventBiometricRecognition> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventBiometricRecognition> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventBiometricRecognition> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventBiometricRecognition> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventBiometricRecognition getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventBiometricRecognition> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventBiometricRecognition> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventBiometricRecognition> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventBiometricRecognition> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventBiometricRecognition> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventBiometricRecognition> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventBiometricRecognition> findOne(Specification<EventBiometricRecognition> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventBiometricRecognition> findAll(Specification<EventBiometricRecognition> specification) {
        return null;
    }

    @Override
    public Page<EventBiometricRecognition> findAll(Specification<EventBiometricRecognition> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventBiometricRecognition> findAll(Specification<EventBiometricRecognition> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventBiometricRecognition> specification) {
        return 0;
    }

    @Override
    public Optional<EventBiometricRecognition> findByBiometric(Biometric biometric) {
        return Optional.of(eventMap.get(biometric));
    }
}
