package com.landau.core.events.repos;

import com.landau.core.events.entities.EventTerminalSystem;
import com.landau.core.terminals.entities.Terminal;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class EventTerminalSystemRepoTest implements EventTerminalSystemRepo {
    private Map<String, EventTerminalSystem> eventMap = new HashMap<>();

    @Override
    public Optional<EventTerminalSystem> findByTerminal(Terminal serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }

    @Override
    public List<EventTerminalSystem> findAll() {
        return null;
    }

    @Override
    public List<EventTerminalSystem> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventTerminalSystem> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventTerminalSystem> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventTerminalSystem eventTerminalSystem) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventTerminalSystem> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventTerminalSystem> S save(S s) {
        eventMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends EventTerminalSystem> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventTerminalSystem> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventTerminalSystem> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventTerminalSystem> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventTerminalSystem getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventTerminalSystem> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventTerminalSystem> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventTerminalSystem> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventTerminalSystem> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventTerminalSystem> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventTerminalSystem> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventTerminalSystem> findOne(Specification<EventTerminalSystem> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventTerminalSystem> findAll(Specification<EventTerminalSystem> specification) {
        return null;
    }

    @Override
    public Page<EventTerminalSystem> findAll(Specification<EventTerminalSystem> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventTerminalSystem> findAll(Specification<EventTerminalSystem> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventTerminalSystem> specification) {
        return 0;
    }
}
