package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceInstaller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class EventDeviceInstallerRepoTest implements EventDeviceInstallerRepo {
    private Map<String, EventDeviceInstaller> eventMap = new HashMap<>();

    @Override
    public List<EventDeviceInstaller> findAll() {
        return null;
    }

    @Override
    public List<EventDeviceInstaller> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventDeviceInstaller> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceInstaller> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventDeviceInstaller eventDeviceInstaller) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventDeviceInstaller> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventDeviceInstaller> S save(S s) {
        eventMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends EventDeviceInstaller> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventDeviceInstaller> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventDeviceInstaller> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventDeviceInstaller> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventDeviceInstaller getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventDeviceInstaller> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventDeviceInstaller> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventDeviceInstaller> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventDeviceInstaller> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventDeviceInstaller> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventDeviceInstaller> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventDeviceInstaller> findOne(Specification<EventDeviceInstaller> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventDeviceInstaller> findAll(Specification<EventDeviceInstaller> specification) {
        return null;
    }

    @Override
    public Page<EventDeviceInstaller> findAll(Specification<EventDeviceInstaller> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceInstaller> findAll(Specification<EventDeviceInstaller> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventDeviceInstaller> specification) {
        return 0;
    }

    @Override
    public Optional<EventDeviceInstaller> findByDevice(Device serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }
}
