package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class MediaFileRepoTest implements MediaFileRepo {
    private Map<String, MediaFile> mediaFileMap = new HashMap<>();
    @Override
    public Optional<MediaFile> findByExternal(Long external) {
        return Optional.empty();
    }

    @Override
    public List<MediaFile> findAllByDeviceAndCreatedBetweenOrderByCreated(Device device, Date created, Date created2) {
        return null;
    }

    @Override
    public List<MediaFile> findAllByDevice(Device device) {
        return null;
    }

    @Override
    public List<MediaFile> findAllByUserAndCreatedBetween(User user, Date created, Date created2) {
        return null;
    }

    @Override
    public Optional<MediaFile> findByTerminal_Serial(String serial) {
        return Optional.of(mediaFileMap.get(serial));
    }

    @Override
    public List<MediaFile> findAll() {
        return null;
    }

    @Override
    public List<MediaFile> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<MediaFile> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<MediaFile> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(MediaFile mediaFile) {

    }

    @Override
    public void deleteAll(Iterable<? extends MediaFile> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends MediaFile> S save(S s) {
        mediaFileMap.put(s.getTerminal().getSerial(), s);
        return s;
    }

    @Override
    public <S extends MediaFile> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<MediaFile> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends MediaFile> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<MediaFile> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public MediaFile getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends MediaFile> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends MediaFile> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends MediaFile> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends MediaFile> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends MediaFile> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends MediaFile> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<MediaFile> findOne(Specification<MediaFile> specification) {
        return Optional.empty();
    }

    @Override
    public List<MediaFile> findAll(Specification<MediaFile> specification) {
        return null;
    }

    @Override
    public Page<MediaFile> findAll(Specification<MediaFile> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<MediaFile> findAll(Specification<MediaFile> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<MediaFile> specification) {
        return 0;
    }
}
