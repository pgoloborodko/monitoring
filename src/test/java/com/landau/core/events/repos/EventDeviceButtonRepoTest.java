package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceButton;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class EventDeviceButtonRepoTest implements EventDeviceButtonRepo {
    private Map<String, EventDeviceButton> eventMap = new HashMap<>();

    @Override
    public List<EventDeviceButton> findAll() {
        return null;
    }

    @Override
    public List<EventDeviceButton> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventDeviceButton> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceButton> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventDeviceButton eventDeviceButton) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventDeviceButton> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventDeviceButton> S save(S s) {
        eventMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends EventDeviceButton> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventDeviceButton> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventDeviceButton> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventDeviceButton> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventDeviceButton getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventDeviceButton> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventDeviceButton> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventDeviceButton> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventDeviceButton> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventDeviceButton> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventDeviceButton> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventDeviceButton> findOne(Specification<EventDeviceButton> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventDeviceButton> findAll(Specification<EventDeviceButton> specification) {
        return null;
    }

    @Override
    public Page<EventDeviceButton> findAll(Specification<EventDeviceButton> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceButton> findAll(Specification<EventDeviceButton> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventDeviceButton> specification) {
        return 0;
    }

    @Override
    public Optional<EventDeviceButton> findByDevice(Device serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }
}
