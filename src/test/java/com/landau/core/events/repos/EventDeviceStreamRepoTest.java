package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class EventDeviceStreamRepoTest implements EventDeviceStreamRepo {
    private Map<String, EventDeviceStream> eventMap = new HashMap<>();

    @Override
    public List<EventDeviceStream> findAll() {
        return null;
    }

    @Override
    public List<EventDeviceStream> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<EventDeviceStream> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceStream> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(EventDeviceStream eventDeviceStream) {

    }

    @Override
    public void deleteAll(Iterable<? extends EventDeviceStream> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends EventDeviceStream> S save(S s) {
        eventMap.put(s.getDevice().getSerial(), s);
        return s;
    }

    @Override
    public <S extends EventDeviceStream> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<EventDeviceStream> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends EventDeviceStream> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<EventDeviceStream> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public EventDeviceStream getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends EventDeviceStream> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends EventDeviceStream> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends EventDeviceStream> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends EventDeviceStream> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends EventDeviceStream> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends EventDeviceStream> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<EventDeviceStream> findOne(Specification<EventDeviceStream> specification) {
        return Optional.empty();
    }

    @Override
    public List<EventDeviceStream> findAll(Specification<EventDeviceStream> specification) {
        return null;
    }

    @Override
    public Page<EventDeviceStream> findAll(Specification<EventDeviceStream> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<EventDeviceStream> findAll(Specification<EventDeviceStream> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<EventDeviceStream> specification) {
        return 0;
    }

    @Override
    public Optional<EventDeviceStream> findByDevice(Device serial) {
        return Optional.of(eventMap.get(serial.getSerial()));
    }

    @Override
    public List<EventDeviceStream> findByUserAndCreatedBetween(User user, Date created, Date created2) {
        return null;
    }
}
