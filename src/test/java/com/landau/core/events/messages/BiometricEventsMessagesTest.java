package com.landau.core.events.messages;

import com.lampa.republish.RepublisherException;
import com.landau.core.config.BiometricTestConfig;
import com.landau.core.events.entities.EventBiometricRecognition;
import com.landau.objects.events.biometric.BiometricFaceRecognitionEvent;
import com.landau.objects.events.biometric.BiometricFaceTrackingEvent;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.BiometricTestConfig.biometric;
import static com.landau.core.config.BiometricTestConfig.serial;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BiometricEventsMessages.class)
@ContextConfiguration(classes = BiometricTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class BiometricEventsMessagesTest {
    @Autowired
    private BiometricEventsMessages biometricEventsMessages;

    private static final BiometricFaceRecognitionEvent recognitionEvent = new BiometricFaceRecognitionEvent();
    private static final BiometricFaceTrackingEvent trackingEvent = new BiometricFaceTrackingEvent();

    private static final EventBiometricRecognition eventEntity = new EventBiometricRecognition();

    @BeforeClass
    public static void setUp(){
        recognitionEvent.setSerial(serial);
        trackingEvent.setSerial(serial);
        eventEntity.setBiometric(biometric);
        eventEntity.setRecognized(false);
    }

    @Test
    public void consumeBiometricFaceRecognitionEvent() {
        eventEntity.setType(EventBiometricRecognition.Type.RECOGNITION);
        biometricEventsMessages.consumeBiometricFaceRecognitionEvent(recognitionEvent);

        Assert.assertEquals(eventEntity, biometricEventsMessages.getEventBiometricRecognitionRepo().findByBiometric(biometric).get());
        try {
            verify(biometricEventsMessages.getCoreBrowserMessages(), times(1))
                    .send("", eventEntity);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void consumeBiometricFaceTrackingEvent() {
        eventEntity.setType(EventBiometricRecognition.Type.TRACKING);
        biometricEventsMessages.consumeBiometricFaceTrackingEvent(trackingEvent);

        Assert.assertEquals(eventEntity, biometricEventsMessages.getEventBiometricRecognitionRepo().findByBiometric(biometric).get());
        try {
            verify(biometricEventsMessages.getCoreBrowserMessages(), times(1))
                    .send("", eventEntity);
        } catch (RepublisherException e) {
            e.printStackTrace();
        }
    }
}