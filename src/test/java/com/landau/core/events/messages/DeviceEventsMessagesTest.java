package com.landau.core.events.messages;

import com.landau.core.config.DeviceConfigTest;
import com.landau.core.events.entities.EventDeviceInstaller;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.events.entities.EventDeviceSystem;
import com.landau.objects.events.device.DeviceButtonEvent;
import com.landau.objects.events.device.DeviceInstallerEvent;
import com.landau.objects.events.device.DeviceStreamEvent;
import com.landau.objects.events.device.DeviceSystemEvent;
import lombok.Data;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.DeviceConfigTest.device;
import static com.landau.core.config.DeviceConfigTest.serial;
import static org.junit.Assert.assertEquals;

@Data
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DeviceEventsMessages.class)
@ContextConfiguration(classes = DeviceConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class DeviceEventsMessagesTest {
    @Autowired
    private DeviceEventsMessages deviceEventsMessages;

    private static final DeviceButtonEvent buttonEvent = new DeviceButtonEvent();
    private static final DeviceInstallerEvent deviceInstallerEvent = new DeviceInstallerEvent();
    private static final EventDeviceInstaller eventDeviceInstaller = new EventDeviceInstaller();

    private static final DeviceStreamEvent deviceStreamEvent = new DeviceStreamEvent();
    private static final EventDeviceStream eventDeviceStream = new EventDeviceStream();

    private static final DeviceSystemEvent deviceSystemEvent = new DeviceSystemEvent();
    private static final EventDeviceSystem eventDeviceSystem = new EventDeviceSystem();

    @BeforeClass
    public static void setUp(){

        buttonEvent.setSerial(serial);

        deviceInstallerEvent.setSerial(serial);
        eventDeviceInstaller.setDevice(device);

        deviceStreamEvent.setSerial(serial);
        eventDeviceStream.setDevice(device);

        deviceSystemEvent.setSerial(serial);
        eventDeviceSystem.setDevice(device);

    }

    @Test
    public void consumeDeviceButtonEventSOS() {
//
//        Republish republish = mock(Republish.class);
//        MessageService messageService = mock(MessageService.class);
//        when(deviceEventsMessages.getBrowserMessages().getMessageService()).thenReturn(messageService);
//        when(deviceEventsMessages.getBrowserMessages().getMessageService().getRepublish()).thenReturn(republish);
//
//        buttonEvent.setButton(DeviceButtonEvent.Button.SOS);
//        deviceEventsMessages.consumeDeviceButtonEvent(buttonEvent);
//
//        assertEquals(deviceEventsMessages.getEventDeviceButtonRepo()
//                .findByDevice(device).get().getDevice().getSerial(), serial);
//        assertEquals(deviceEventsMessages.getEventDeviceButtonRepo()
//                .findByDevice(device).get().getType(), EventDeviceButton.Type.SOS);
//
//        try {
//            verify(deviceEventsMessages.getBrowserMessages().getMessageService().getRepublish(), times(1))
//                    .produce(ArgumentMatchers.any());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }

    }

    //TODO переделать
    @Test
    public void consumeDeviceButtonEventSTREAM() {
//        buttonEvent.setButton(DeviceButtonEvent.Button.STREAM);
//        deviceEventsMessages.consumeDeviceButtonEvent(buttonEvent);
//
//        assertEquals(deviceEventsMessages.getEventDeviceButtonRepo()
//                .findByDevice_Serial(serial).get().getDevice().getSerial(), serial);
//        assertEquals(deviceEventsMessages.getEventDeviceButtonRepo()
//                .findByDevice_Serial(serial).get().getType(), EventDeviceButton.Type.STREAM);
//
//        try {
//            verify(deviceEventsMessages.getDeviceStreamMessages(), times(1))
//                    .startStream(deviceEventsMessages.getEventDeviceButtonRepo().findByDevice_Serial(serial).get().getDevice());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }

    }

    @Test
    public void consumeDeviceInstallerEvent() {
        deviceEventsMessages.consumeDeviceInstallerEvent(deviceInstallerEvent);

        assertEquals(eventDeviceInstaller, deviceEventsMessages.getEventDeviceInstallerRepo().findByDevice(device).get());
    }

    //TODO переделать

    @Test
    public void consumeDeviceStreamEvent() {
//        eventDeviceStream.setResolution(EventDeviceStream.Resolution.SD);
//
//        Republish republish = mock(Republish.class);
//        MessageService messageService = mock(MessageService.class);
//        when(deviceEventsMessages.getBrowserMessages().getMessageService()).thenReturn(messageService);
//        when(deviceEventsMessages.getBrowserMessages().getMessageService().getRepublish()).thenReturn(republish);
//
//        deviceEventsMessages.consumeDeviceStreamEvent(deviceStreamEvent);
//
//        assertEquals(eventDeviceStream, deviceEventsMessages.getEventDeviceStreamRepo().findByDevice_Serial(serial).get());
//
//        try {
//            verify(deviceEventsMessages.getBrowserMessages().getMessageService().getRepublish(), times(1))
//                    .produce(ArgumentMatchers.any());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }

    @Test
    public void consumeDeviceSystemEvent() {
        deviceEventsMessages.consumeDeviceSystemEvent(deviceSystemEvent);

        assertEquals(eventDeviceSystem, deviceEventsMessages.getEventDeviceSystemRepo().findByDevice(device).get());
    }

    //TODO переделать
    @Test
    public void consumeDeviceSystemEventTypeNotNull() {
//        deviceSystemEvent.setType(DeviceSystemEvent.Type.WEAPON_EXTRACTED);
//        eventDeviceSystem.setType(EventDeviceSystem.Type.WEAPON_EXTRACTED);
//
//        Republish republish = mock(Republish.class);
//        MessageService messageService = mock(MessageService.class);
//        when(deviceEventsMessages.getBrowserMessages().getMessageService()).thenReturn(messageService);
//        when(deviceEventsMessages.getBrowserMessages().getMessageService().getRepublish()).thenReturn(republish);
//
//        deviceEventsMessages.consumeDeviceSystemEvent(deviceSystemEvent);
//
//        assertEquals(eventDeviceSystem, deviceEventsMessages.getEventDeviceSystemRepo().findByDevice(device).get());
//
//        try {
//            verify(deviceEventsMessages.getBrowserMessages().getMessageService().getRepublish(), times(1))
//                    .produce(ArgumentMatchers.any());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }
}