package com.landau.core.events.messages;

import com.landau.core.config.TerminalConfigTest;
import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.events.entities.EventTerminalSystem;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.terminals.entities.Terminal;
import com.landau.objects.events.terminal.TerminalFileEvent;
import com.landau.objects.events.terminal.TerminalSyncEvent;
import com.landau.objects.events.terminal.TerminalSystemEvent;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static com.landau.core.config.TerminalConfigTest.serial;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TerminalEventsMessages.class)
@ContextConfiguration(classes = TerminalConfigTest.class, loader = AnnotationConfigContextLoader.class)
public class TerminalEventsMessagesTest {

    @Autowired
    private TerminalEventsMessages terminalEventsMessages;

    private static final Terminal terminal = new Terminal();

    private static final TerminalFileEvent terminalFileEvent = new TerminalFileEvent();
    private static final MediaFile mediaFile = new MediaFile();

    private static final TerminalSyncEvent terminalSyncEvent = new TerminalSyncEvent();
    private static final EventTerminalSync eventTerminalSync = new EventTerminalSync();

    private static final TerminalSystemEvent terminalSystemEvent = new TerminalSystemEvent();
    private static final EventTerminalSystem eventTerminalSystem = new EventTerminalSystem();


    @BeforeClass
    public static void setUp(){
        terminal.setSerial(serial);

        terminalFileEvent.setSerial(serial);
        mediaFile.setTerminal(terminal);
        mediaFile.setDuration(1.0);

        terminalSyncEvent.setSerial(serial);
        eventTerminalSync.setTerminal(terminal);

        terminalSystemEvent.setSerial(serial);
        eventTerminalSystem.setTerminal(terminal);
    }

    //TODO переделать
    @Test
    public void consumeTerminalFileEvent() {
//        terminalEventsMessages.consumeTerminalFileEvent(terminalFileEvent);
//        Assert.assertEquals(terminalEventsMessages.getMediaFileRepo().findByTerminal_Serial(serial).get(), mediaFile);
    }

    @Test
    public void consumeTerminalSyncEvent() {
        terminalEventsMessages.consumeTerminalSyncEvent(terminalSyncEvent);
        Assert.assertEquals(terminalEventsMessages.getEventTerminalSyncRepo().findByTerminal(terminal).get(), eventTerminalSync);
    }

    @Test
    public void consumeTerminalSystemEvent() {
        terminalEventsMessages.consumeTerminalSystemEvent(terminalSystemEvent);
        Assert.assertEquals(terminalEventsMessages.getEventTerminalSystemRepo().findByTerminal(terminal).get(), eventTerminalSystem);
    }
}