package com.landau.core.chat.controllers;

import com.landau.core.chat.entities.ChatMessage;
import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.chat.filters.ChatMessageFilter;
import com.landau.core.chat.filters.ChatRoomFilter;
import com.landau.core.chat.messages.DeviceChatMessages;
import com.landau.core.chat.repos.ChatMessageRepo;
import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.common.messages.CastService;
import com.landau.core.common.services.ControllerService;
import com.landau.core.config.RestConfig;
import com.landau.core.users.repos.UserRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.messages.core.CoreDeviceMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static com.landau.core.config.RestConfig.pageable;
import static com.landau.core.config.RestConfig.test;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestConfig.class, loader = AnnotationConfigContextLoader.class)
public class ChatControllerTest {
    private ChatController chatController;

    @Autowired
    private ControllerService controllerService;

    @Autowired
    private DeviceChatMessages deviceChatMessages;

    @Autowired
    private ChatRoomRepo chatRoomRepo;

    @Autowired
    private ChatMessageRepo chatMessageRepo;

    @Autowired
    private CoreDeviceMessages coreDeviceMessages;

    @Autowired
    private CoreBrowserMessages coreBrowserMessages;

    @Autowired
    private CastService castService;

    @Autowired
    private UserRepo userRepo;

    @Before
    public void setUp(){
        chatController = new ChatController(deviceChatMessages, castService,
                coreDeviceMessages, coreBrowserMessages, chatRoomRepo, chatMessageRepo, userRepo);

       // when(chatController.getControllerService().getChatRoomRepo()).thenReturn(chatRoomRepo);
        //when(chatController.getControllerService().getChatMessageRepo()).thenReturn(chatMessageRepo);
        //when(chatController.getControllerService().getUser()).thenReturn(user);
    }

    @Test
    public void getChats() {
        ChatRoomFilter chatRoomFilter = new ChatRoomFilter();
        List<ChatRoom> chatRooms = chatController.getChats(chatRoomFilter);
        assertEquals(chatRooms.get(0).getAdmin().getFirstName(), test);
    }

    @Test
    public void getMessages() {
        ChatMessageFilter chatMessageFilter = new ChatMessageFilter();
        Page<ChatMessage> chatMessagesPage = chatController.getMessages(chatMessageFilter, pageable);
        List<ChatMessage> chatMessages = chatMessagesPage.getContent();

        assertEquals(chatMessages.get(0).getSender().getFirstName(), test);
    }

    //TODO переделать
    @Test
    public void createChatRoomBadRequest() {
//        chatRoom.setCompany(null);
//        try {
//            chatController.createChatRoom(chatRoom);
//        } catch (ResponseStatusException e){
//            assertEquals(e.getReason(), "не указаны участники чат-комнаты");
//        }
    }

    //TODO переделать
    @Test
    public void createChatRoom() {
//        ChatRoom chatRoomTest;
//        chatRoomTest = chatController.createChatRoom(chatRoom);
//        Assert.assertEquals(chatRoomTest.getCompany(), company);
//        Assert.assertEquals(chatRoomTest.getAdmin().getFirstName(), test);
//        Assert.assertNotNull(chatRoomTest.getId());
//
//        try {
//            verify(chatController.getCoreBrowserMessages(), times(chatRoom.getMembers().size()))
//                    .send(ArgumentMatchers.eq(test), (ChatGroupEvent) ArgumentMatchers.any());
//            verify(chatController.getCoreDeviceMessages(), times(chatRoom.getMembers().size()))
//                    .send(ArgumentMatchers.eq(test), (ChatGroupEvent) ArgumentMatchers.any());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }

    }

    //TODO переделать
    @Test
    public void createMessageWrongChatGroup() {
//        try {
//            chatMessage.getChatRoom().setMembers(new ArrayList<>());
//            chatController.createMessage(chatMessage);
//        } catch (ResponseStatusException e){
//            Assert.assertEquals(e.getReason(), "wrong chatGroup");
//        }
    }

    //TODO переделать
    @Test
    public void createMessage() {
//        ChatMessage chatMessageTest;
//        chatMessageTest = chatController.createMessage(chatMessage);
//        Assert.assertEquals(chatMessageTest.getChatRoom(), chatRoom);
//        Assert.assertEquals(chatMessageTest.getSender().getFirstName(), test);
//        Assert.assertNotNull(chatMessageTest.getId());
//
//        try {
//            verify(chatController.getCoreBrowserMessages(), times(1))
//                    .send(ArgumentMatchers.eq(test), (ChatMessageEvent) ArgumentMatchers.any());
//            verify(chatController.getCoreDeviceMessages(), times(1))
//                    .send(ArgumentMatchers.eq(test), (ChatMessageEvent) ArgumentMatchers.any());
//        } catch (RepublisherException e) {
//            e.printStackTrace();
//        }
    }
}