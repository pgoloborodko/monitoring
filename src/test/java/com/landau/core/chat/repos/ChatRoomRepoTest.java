package com.landau.core.chat.repos;

import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class ChatRoomRepoTest implements ChatRoomRepo {
    private Map<Long, ChatRoom> chatRoomMap = new HashMap<>();
    @Override
    public List<ChatRoom> findAllByLastMessageBeforeAndAdminOrderByLastMessageDesc(Date date, User user) {
        return null;
    }

    @Override
    public List<ChatRoom> findAllByLastMessageBeforeOrderByLastMessageDesc(Date date) {
        return null;
    }

    @Override
    public List<ChatRoom> findAll() {
        return null;
    }

    @Override
    public List<ChatRoom> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ChatRoom> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<ChatRoom> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(ChatRoom chatRoom) {

    }

    @Override
    public void deleteAll(Iterable<? extends ChatRoom> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends ChatRoom> S save(S s) {
        if (s.getId() != null){
            chatRoomMap.put(s.getId(), s);
        }
        else {
            s.setId(1L);

            while (chatRoomMap.containsKey(s.getId())){
                s.setId(s.getId() + 1);
            }

            chatRoomMap.put(s.getId(), s);
        }

        return s;
    }

    @Override
    public <S extends ChatRoom> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<ChatRoom> findById(Long aLong) {
        return Optional.of(chatRoomMap.get(aLong));
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ChatRoom> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<ChatRoom> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ChatRoom getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends ChatRoom> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ChatRoom> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ChatRoom> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ChatRoom> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ChatRoom> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ChatRoom> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<ChatRoom> findOne(Specification<ChatRoom> specification) {
        return Optional.empty();
    }

    @Override
    public List<ChatRoom> findAll(Specification<ChatRoom> specification) {
        return null;
    }

    @Override
    public Page<ChatRoom> findAll(Specification<ChatRoom> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<ChatRoom> findAll(Specification<ChatRoom> specification, Sort sort) {
        return new ArrayList<>(chatRoomMap.values());
    }

    @Override
    public long count(Specification<ChatRoom> specification) {
        return 0;
    }
}
