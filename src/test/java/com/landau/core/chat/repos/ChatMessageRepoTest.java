package com.landau.core.chat.repos;

import com.landau.core.chat.entities.ChatMessage;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class ChatMessageRepoTest implements ChatMessageRepo {
    private Map<Long, ChatMessage> messageMap = new HashMap<>();
    private PageImpl<ChatMessage> pageImpl;

    @Override
    public List<ChatMessage> findAll() {
        return null;
    }

    @Override
    public List<ChatMessage> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ChatMessage> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<ChatMessage> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(ChatMessage chatMessage) {

    }

    @Override
    public void deleteAll(Iterable<? extends ChatMessage> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends ChatMessage> S save(S s) {
        if (s.getId() != null){
            messageMap.put(s.getId(), s);
        }
        else {
            s.setId(1L);

            while (messageMap.containsKey(s.getId())){
                s.setId(s.getId() + 1);
            }

            messageMap.put(s.getId(), s);
        }

        return s;
    }

    @Override
    public <S extends ChatMessage> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<ChatMessage> findById(Long aLong) {
        return Optional.of(messageMap.get(aLong));
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ChatMessage> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<ChatMessage> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ChatMessage getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends ChatMessage> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ChatMessage> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ChatMessage> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ChatMessage> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ChatMessage> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ChatMessage> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<ChatMessage> findOne(Specification<ChatMessage> specification) {
        return Optional.empty();
    }

    @Override
    public List<ChatMessage> findAll(Specification<ChatMessage> specification) {
        return null;
    }

    @Override
    public Page<ChatMessage> findAll(Specification<ChatMessage> specification, Pageable pageable) {
        return new PageImpl<>(new ArrayList<>(messageMap.values()));
    }

    @Override
    public List<ChatMessage> findAll(Specification<ChatMessage> specification, Sort sort) {
        return new ArrayList<>(messageMap.values());
    }

    @Override
    public long count(Specification<ChatMessage> specification) {
        return 0;
    }
}
