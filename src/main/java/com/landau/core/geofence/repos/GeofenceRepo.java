package com.landau.core.geofence.repos;

import com.landau.core.geofence.entities.Geofence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GeofenceRepo extends JpaRepository<Geofence, Long>, JpaSpecificationExecutor<Geofence> {
}
