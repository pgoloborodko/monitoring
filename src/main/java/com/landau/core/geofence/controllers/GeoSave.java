package com.landau.core.geofence.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.dto.PointDTO;
import com.landau.core.geofence.entities.Geofence;
import com.landau.core.geofence.repos.GeofenceRepo;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;

/**
 * Сохранение геозоны
 */
@Api(tags = "Сохранение геозоны", description = "Контроллер может получать данные геозоны, а также создавать новые")
@RestController
@RequestMapping("geofences/save")
public class GeoSave extends Base {
    private GeofenceRepo geofenceRepo;

    @Autowired
    public GeoSave(GeofenceRepo geofenceRepo) {
        this.geofenceRepo = geofenceRepo;
    }

    /**
     * Получение данных геозон
     * @param geofence геозона
     * @return {@link Geofence}
     */
    @ApiOperation(value = "Получение данных геозон", response = Geofence.class)
    @Authority(value = Geofence.class)
    @GetMapping
    public Geofence get(Geofence geofence) {
        return geofence;
    }

    /**
     * Сохранение геозоны
     * @param geofence геозона
     * @return
     */
    @ApiOperation(value = "Сохранение геозоны", response = Geofence.class)
    @Authority(value = Geofence.class)
    @PostMapping
    public Geofence post(@RequestBody Geofence geofence, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        ArrayList<Coordinate> coords = new ArrayList<>();

        for (PointDTO pointDTO: geofence.getPoints()){
            coords.add(pointDTO.getCoordinate());
        }


        GeometryFactory GEOMETRY_FACTORY;
        Polygon polygon;

        try {
             GEOMETRY_FACTORY = new GeometryFactory();
             polygon = GEOMETRY_FACTORY.createPolygon(coords.toArray(new Coordinate[0]));
             polygon.setSRID(32635);

        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "geofence bad format " + e.getMessage());
        }

        geofence.setGeom(polygon);


        return geofenceRepo.save(geofence);
    }
}
