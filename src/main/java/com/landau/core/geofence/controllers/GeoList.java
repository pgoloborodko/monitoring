package com.landau.core.geofence.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.geofence.entities.Geofence;
import com.landau.core.geofence.filters.GeofencesFilter;
import com.landau.core.geofence.repos.GeofenceRepo;
import com.landau.core.geofence.specs.GeofencesSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Получение списка геозон с фильтрацией
 */
@Api(tags = "Список геозон", description = "Контроллер обеспечивает получение списка геозон")
@RestController
@RequestMapping("/geofences")
public class GeoList extends Base {
    private GeofenceRepo geofenceRepo;

    @Autowired
    public GeoList(GeofenceRepo geofenceRepo) {
        this.geofenceRepo = geofenceRepo;
    }

    /**
     * Получение списка геозон по фильтру
     * @return список геозон
     */
    @ApiOperation(value = "Получение списка геозон с фильтрацией", response = Geofence.class)
    @GetMapping
    public Page<Geofence> get(GeofencesFilter geofencesFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            geofencesFilter.setCompany(getUser().getCompany());
        }

        return geofenceRepo.findAll(GeofencesSpec.find(geofencesFilter), pageable);
    }
}
