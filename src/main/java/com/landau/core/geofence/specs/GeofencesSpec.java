package com.landau.core.geofence.specs;

import com.landau.core.geofence.entities.Geofence;
import com.landau.core.geofence.filters.GeofencesFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class GeofencesSpec {
    public static Specification<Geofence> find(GeofencesFilter geofencesFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (geofencesFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), geofencesFilter.getCompany()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
