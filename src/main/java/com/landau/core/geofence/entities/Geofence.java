package com.landau.core.geofence.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.companies.entities.Company;
import com.landau.core.dto.PointDTO;
import com.landau.core.geofence.repos.GeofenceRepo;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * Сущность геозоны
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(GeofenceRepo.class)
@ApiModel(description = "Сущность геозоны")
public class Geofence extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Название геозоны")
    private String name;

    @Column
    @JsonIgnore
    @ApiParam(value="geom", hidden=true)
    private Polygon geom;

    @Column
    @Transient
    @ApiModelProperty(value = "Гео-полигоны геозоны")
    private List<PointDTO> points = new ArrayList<>();

    @JsonProperty("company")
    private void unpackCompany(Long companyId) {
        Company company = new Company();
        company.setId(companyId);
        this.setCompany(company);
    }

    public List<PointDTO> getPoints() {
        if(this.getGeom() != null) {
            Coordinate[] coordinates = getGeom().getCoordinates();

            for (Coordinate coordinate: coordinates){
                this.points.add(new PointDTO(coordinate));
            }
        }

        return points;
    }

    public void setPoints(List<PointDTO> points) {
        this.points = points;
    }
}
