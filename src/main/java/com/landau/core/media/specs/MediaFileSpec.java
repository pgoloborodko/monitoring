package com.landau.core.media.specs;

import com.landau.core.events.filters.EventFilter;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.filters.MediaFileFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class MediaFileSpec {
    public static Specification<MediaFile> find(MediaFileFilter mediaFileFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (mediaFileFilter.getDate() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(mediaFileFilter.getDate());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);


                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (mediaFileFilter.getUser() != null){
                predicates.add(cb.equal(root.get("user"), mediaFileFilter.getUser()));
            }

            if (mediaFileFilter.getCompany() != null){
                predicates.add(cb.equal(root.get("company"), mediaFileFilter.getCompany()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    public static Specification<MediaFile> find(EventFilter eventFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();


            if (eventFilter.getFrom() != null && eventFilter.getTo() != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(eventFilter.getFrom());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(eventFilter.getTo());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (eventFilter.getDevice() != null){
                predicates.add(cb.equal(root.get("device"), eventFilter.getDevice()));
            }

            if (eventFilter.getUser() != null){
                predicates.add(cb.equal(root.get("user"), eventFilter.getUser()));
            }

            if (eventFilter.getCompany() != null){
                predicates.add(cb.equal(root.get("company"), eventFilter.getCompany()));
            }

            if(eventFilter.getGroupByUser()) {
                query.groupBy(root.get("user"));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    public static Specification<MediaFile> findByEvent(EventFilter eventFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (eventFilter.getCreated() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(eventFilter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);


                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (eventFilter.getTerminal() != null){
                predicates.add(cb.equal(root.get("terminal"), eventFilter.getTerminal()));
            }
            if (eventFilter.getDevice() != null){
                predicates.add(cb.equal(root.get("device"), eventFilter.getTerminal()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
