package com.landau.core.media.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.events.repos.EventDeviceStreamRepo;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.filters.MediaFileFilter;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.media.services.PlayListService;
import com.landau.core.media.specs.MediaFileSpec;
import com.landau.core.streams.entities.StreamServer;
import com.landau.core.streams.services.StreamService;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController(value = "MediaPlaylist")
@RequestMapping("/media/playlist")
@CrossOrigin
@Slf4j
@Api(tags = "Медиа плей-лист", description = "Контроллер может получать данные из архива медиа-файлов")
public class Playlist extends Base {
    private StreamService streamService;
    private PlayListService playListService;
    private EventDeviceStreamRepo eventDeviceStreamRepo;
    private MediaFileRepo mediaFileRepo;

    @Autowired
    public Playlist(StreamService streamService, PlayListService playListService, EventDeviceStreamRepo eventDeviceStreamRepo, MediaFileRepo mediaFileRepo) {
        this.streamService = streamService;
        this.playListService = playListService;
        this.eventDeviceStreamRepo = eventDeviceStreamRepo;
        this.mediaFileRepo = mediaFileRepo;
    }

    @ModelAttribute
    private User getRequestUser(@RequestParam("user") Long id) {
        User user = new User();
        user.setId(id);

        return user;
    }


    /**
     * get archive

    @ApiOperation(value = "Получение ссылки на плейлист стрима")
    @DTOAuthority(value = Device.class, required = true)
    @GetMapping("streamArchive")
    public MediaFile getStreamArchive(Device device, @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        // find media files
        Date endDate = new Date(date.getTime() + 86400 * 1000);

        MediaFile mediaFile = new MediaFile();
        mediaFile.setDuration(-1D);
        mediaFile.setDevice(device);
        mediaFile.setCreated(date);
        mediaFile.setUrl(
                findStreamServer().getHlsUrl() +
                        "/stream/" + device.getSerial() +
                        "/playlist_dvr_range-" + date.getTime()/1000 + "-86400.m3u8");

        return mediaFile;
    }*/


//    /**
//     * get archive
//     * @param device device
//     * @param date date
//     * @return playlist.m3u8
//     */
//    @ApiOperation(value = "Получение архивных записей(2)", response = MediaFile.class, responseContainer = "List")
//    @DTOAuthority(value = Device.class, required = true)
//    @GetMapping("archive")
//    public List<MediaFile> getArchive(Device device, @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
//        // find media files
//        Date endDate = new Date(date.getTime() + 86400 * 1000);
//
//        StreamServer streamServer = findStreamServer();
//
//        MediaFile streamMediaFile = new MediaFile();
//        streamMediaFile.setDuration(-1D);
//        streamMediaFile.setDevice(device);
//        streamMediaFile.setCreated(date);
//        streamMediaFile.setUrl(streamServer.getHlsUrl() + "/stream/" + device.getSerial() +
//                        "/playlist_dvr_range-" + date.getTime()/1000 + "-86400.m3u8");
//
//        List<MediaFile> mediaFiles = mediaFileRepo.findAllByDeviceAndCreatedBetweenOrderByCreated(device, date, endDate).stream()
//                .filter(mediaFile -> mediaFile.getDuration() != null) // why not equals? Is it one letter only?
//                .peek(mediaFile -> mediaFile.setUrl(streamServer.getHlsUrl() + "/archive/" + mediaFile.getExternalPath() + "/playlist.m3u8"))
//                .collect(Collectors.toList());
//
//        mediaFiles.add(0, streamMediaFile);
//
//        return mediaFiles;
//    }

    @ApiOperation(value = "Получение архивных записей", response = MediaFile.class, responseContainer = "List")
    @Authority(value = User.class, required = true)
    @GetMapping("archive")
    public List<MediaFile> getArchive(@ModelAttribute User user, MediaFileFilter mediaFileFilter) {

        if (mediaFileFilter.getDate() == null){
            mediaFileFilter.setDate(new Date());
        }

        StreamServer streamServer = findStreamServer();

        List<Device> deviceList = playListService.getDevicesForUserPerDay(user, mediaFileFilter.getDate());
        List<MediaFile> streamMediaFileList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mediaFileFilter.getDate());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        List<MediaFile> mediaFiles = mediaFileRepo.findAll(MediaFileSpec.find(mediaFileFilter))
                .stream()
                .filter(mediaFile -> mediaFile.getDuration() != null) // why not equals? Is it one letter only?
                .peek(mediaFile -> mediaFile.setUrl(streamServer.getHlsArchiveUrl() + mediaFile.getExternalPath() + "/playlist.m3u8"))
                .collect(Collectors.toList());

        for(MediaFile mediaFile : mediaFiles) {
            if(mediaFile.getDevice() != null) {
                if(!deviceList.contains(mediaFile.getDevice())) {
                    deviceList.add(mediaFile.getDevice());
                }
            }
            else {
                log.warn("mediafile {} without device", mediaFile);
            }
        }


        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime(calendar.getTime());
        calendarEnd.add(Calendar.DATE, 1);

        List<EventDeviceStream> events = eventDeviceStreamRepo.findByUserAndCreatedBetween(user, calendar.getTime(), calendarEnd.getTime());

        for(EventDeviceStream eventDeviceStream : events) {
            if(eventDeviceStream.getDevice() != null) {
                if(!deviceList.contains(eventDeviceStream.getDevice())) {
                    deviceList.add(eventDeviceStream.getDevice());
                }
            }
            else {
                log.warn("event device stream {} without device", eventDeviceStream);
            }
        }

        for (Device device : deviceList) {
            MediaFile streamMediaFile = new MediaFile();

            streamMediaFile.setDuration(-1D);
            streamMediaFile.setDevice(device);
            streamMediaFile.setCreated(mediaFileFilter.getDate());
            streamMediaFile.setUrl(streamServer.getHlsStreamUrl() + device.getSerial() +
                            "/playlist_dvr_range-" + calendar.getTime().getTime()/1000 + "-86400.m3u8");

            streamMediaFileList.add(0, streamMediaFile);
        }

        streamMediaFileList.addAll(mediaFiles);

        return streamMediaFileList;
    }

    /**
     *
     * @return
     */
    private StreamServer findStreamServer() {
        StreamServer streamServer = streamService.getServerForStreaming(getUser().getCompany());

        if(streamServer != null) {
            return streamServer;
        }
        else {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "server not found");
        }
    }
}
