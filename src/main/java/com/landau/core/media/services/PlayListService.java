package com.landau.core.media.services;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.events.filters.EventUserDevicesSyncFilter;
import com.landau.core.events.repos.UserDevicesSyncRepo;
import com.landau.core.events.specs.EventUserDevicesSyncSpec;
import com.landau.core.users.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PlayListService {
    private UserDevicesSyncRepo syncRepo;


    @Autowired
    public PlayListService(UserDevicesSyncRepo syncRepo) {
        this.syncRepo = syncRepo;
    }

    /**
     * Возвращает список устройств, полученных пользователем в течение дня
     * @param user объект пользователя
     * @param date дата для поиска
     * @return
     */
    public List<Device> getDevicesForUserPerDay(User user, Date date) {
        EventUserDevicesSyncFilter filter = new EventUserDevicesSyncFilter();
        filter.setUser(user);
        filter.setDate(date);
        filter.setType(EventUserDevicesSyncFilter.Type.STREAM);

        List<UserDevicesSync> listOfEvents = syncRepo.findAll(EventUserDevicesSyncSpec.find(filter));
        List<Device> listOfDevices = new ArrayList<>();

        for (UserDevicesSync event : listOfEvents){
            listOfDevices.add(event.getDevice());
        }

        return listOfDevices;
    }
}
