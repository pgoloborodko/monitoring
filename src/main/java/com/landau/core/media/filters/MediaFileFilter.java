package com.landau.core.media.filters;

import com.landau.core.companies.entities.Company;
import com.landau.core.users.entities.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class MediaFileFilter {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date = new Date();
    private User user;
    private Company company;
}
