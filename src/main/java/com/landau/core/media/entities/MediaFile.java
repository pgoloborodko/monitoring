package com.landau.core.media.entities;

import com.fasterxml.jackson.annotation.*;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.devices.entities.Device;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Сущность Медиа-файла
 */
@Data
@Entity
@Table(indexes = {@Index(columnList="external")})
@JsonInclude
@Repo(MediaFileRepo.class)
@ApiModel(description = "Сущность Медиа-файла")
public class MediaFile extends AbstractBusiness {
    @ManyToOne
    @ApiModelProperty(value = "Устройство медиа-файла")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Device device;

    @ManyToOne
    @ApiModelProperty(value = "Юзер медиа-файла")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @ManyToOne
    @ApiModelProperty(value = "Терминал медиа-файла")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Terminal terminal;

    @Column
    @JsonIgnore
    @ApiModelProperty(value = "Идентификатор записи на терминале")
    private Long external;

    @Column
    @ApiModelProperty(value = "Продолжительность медиа-файла, секунды")
    private Double duration;

    @Column
    @ApiModelProperty(value = "Дата создания медиа-файла")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date created;

    @Column
    @ApiModelProperty(value = "Дата добавления файла в систему")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date added;

    @Column
    @JsonIgnore
    private String externalPath;

    @Transient
    @ApiModelProperty(value = "Url путь до плейлиста файла, VOD")
    private String url;

    @Column
    private Long externalDevice;

    @Column
    private Long externalUser;
}
