package com.landau.core.media.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MediaFileRepo extends JpaRepository<MediaFile, Long>, JpaSpecificationExecutor<MediaFile> {
    Optional<MediaFile> findByExternal(Long external);

    List<MediaFile> findAllByDeviceAndCreatedBetweenOrderByCreated(Device device, Date created, Date created2);

    List<MediaFile> findAllByDevice(Device device);

    List<MediaFile> findAllByUserAndCreatedBetween(User user, Date created, Date created2);

    /**
     *  Метод для тестов
     * @param serial
     * @return
     */
    Optional<MediaFile> findByTerminal_Serial(String serial);
}
