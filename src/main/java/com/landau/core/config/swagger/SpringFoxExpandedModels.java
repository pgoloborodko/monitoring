package com.landau.core.config.swagger;

import com.google.common.base.Optional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ExpandedParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterExpansionContext;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class SpringFoxExpandedModels implements ExpandedParameterBuilderPlugin {
    @Override
    public boolean supports(DocumentationType type) {
        return true;
    }

    @Override
    public void apply(ParameterExpansionContext context) {
        if(context.getDataTypeName().equals("boolean")) {
            context.getParameterBuilder().scalarExample("");
        }
        else if(context.getDataTypeName().equals("long")) {
            context.getParameterBuilder().scalarExample(0L);
        }
        else if(context.getDataTypeName().equals("int")) {
            context.getParameterBuilder().scalarExample(0);
        }
        else if (context.getDataTypeName().equals("byte")){
            context.getParameterBuilder().scalarExample(0);
        }
        else if (context.getDataTypeName().equals("double")){
            context.getParameterBuilder().scalarExample(0.0);
        }
        else if (context.getDataTypeName().equals("float")){
            context.getParameterBuilder().scalarExample(0.0);
        }
        else if(context.getDataTypeName().equals("date-time")) {
            Optional<DateTimeFormat> format = context.findAnnotation(DateTimeFormat.class);

            if(format.isPresent()) {
                context.getParameterBuilder().scalarExample(new SimpleDateFormat(format.get().pattern()).format(new Date()));
            }
        }
    }
}