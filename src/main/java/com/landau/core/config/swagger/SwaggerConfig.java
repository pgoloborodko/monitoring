package com.landau.core.config.swagger;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.vividsolutions.jts.geom.Polygon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.AlternateTypeProvider;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${security.oauth2.client.clientId}")
    private String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    private String clientSecret;

    private List<ResponseMessage> global = newArrayList(
            new ResponseMessageBuilder().code(200).message("ОК").build(),
            new ResponseMessageBuilder().code(201).message("create successful").build(),
            new ResponseMessageBuilder().code(202).message("change successful").build(),
            new ResponseMessageBuilder().code(400).message("Ошибки заполнения полей").build(),
            new ResponseMessageBuilder().code(401).message("Клиент не прошел авторизацию").build(),
            new ResponseMessageBuilder().code(403).message("Клиент не имеет доступа к запрашиваемому ресурсу").build(),
            new ResponseMessageBuilder().code(404).message("Ресурса не существует").build(),
            new ResponseMessageBuilder().code(405).message("Ошибка HTTP-запроса").build(),
            new ResponseMessageBuilder().code(409).message("Не передан идентификатор сущности").build(),
            new ResponseMessageBuilder().code(424).message("Не найдена зависимость").build(),
            new ResponseMessageBuilder().code(500).message("Непредвиденная ошибка сервера").build(),
            new ResponseMessageBuilder().code(503).message("Удаленный ресурс не отвечает").build()
    );

    @Autowired
    TypeResolver typeResolver;

    @Autowired
    EntityManager entityManager;

    @Bean
    AlternateTypeProvider entityRules() {
        final ResolvedType integerType = typeResolver.resolve(Integer.class);
        final ResolvedType voidType = typeResolver.resolve(Void.class);

        List<AlternateTypeRule> rules = new ArrayList<>();
        for(EntityType entity : entityManager.getMetamodel().getEntities()) {
            rules.add(new AlternateTypeRule(typeResolver.resolve(entity.getJavaType()), integerType));
        }
        rules.add(new AlternateTypeRule(typeResolver.resolve(Polygon.class), voidType));

        return new AlternateTypeProvider(rules);
    }

   /* @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(false)
                .defaultModelsExpandDepth(0)
                .defaultModelExpandDepth(0)
                .build();
    }*/

    /**
     * Код для фейковых полей авторизации
     * @return
     */

//    @Primary
//    @Bean
//    public ApiListingScanner addExtraOperations(ApiDescriptionReader apiDescriptionReader, ApiModelReader apiModelReader, DocumentationPluginsManager pluginsManager)
//    {
//        return new FormLoginOperations(apiDescriptionReader, apiModelReader, pluginsManager);
//    }
    /**
     * Правила формирования документации для работы с котроллерами пакета echd
     * @return
     */
    @Bean
    public Docket echdApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("ECHD")
                .ignoredParameterTypes(File.class, Sort.class, Pageable.class, GrantedAuthority.class)
                .select()
                .paths(PathSelectors.ant("/echd/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                //.alternateTypeRules(new AlternateTypeRule(jsonNodeType, stringType))
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета apps
     * @return
     */
    @Bean
    public Docket appsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Apps")
                .ignoredParameterTypes(File.class, Sort.class, Pageable.class, GrantedAuthority.class)
                .select()
                .paths(PathSelectors.ant("/apps/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                //.alternateTypeRules(new AlternateTypeRule(jsonNodeType, stringType))
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета biometric
     * @return
     */
    @Bean
    public Docket biometricApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Biometric")
                .ignoredParameterTypes(File.class)
                .select()
                .paths(PathSelectors.ant("/biometric/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                //.alternateTypeRules(new AlternateTypeRule(jsonNodeType, stringType))
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }
    /**
     * Правила формирования документации для работы с котроллерами пакета biometric
     * @return
     */
    @Bean
    public Docket camerasApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Cameras")
                .ignoredParameterTypes(File.class)
                .select()
                .paths(PathSelectors.ant("/cameras/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                //.alternateTypeRules(new AlternateTypeRule(jsonNodeType, stringType))
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета chat
     * @return
     */
    @Bean
    public Docket chatApi() {
        TypeResolver typeResolver = new TypeResolver();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Chat")
                .ignoredParameterTypes(File.class, Sort.class, Pageable.class)
                .select()
                .paths(PathSelectors.ant("/chat/**"))
                .build()
//                .alternateTypeRules(new AlternateTypeRule(typeResolver.resolve(DeferredResult.class, Object.class), typeResolver.resolve(WildcardType.class))) //Разобраться позже
                .useDefaultResponseMessages(false)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .apiInfo(apiInfo())
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета cities
     * @return
     */
    @Bean
    public Docket citiesApi() {
        TypeResolver typeResolver = new TypeResolver();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Cities")
                .ignoredParameterTypes(File.class)
                .select()
                .paths(PathSelectors.ant("/cities/**"))
                .build()
//                .alternateTypeRules(new AlternateTypeRule(typeResolver.resolve(DeferredResult.class, Object.class), typeResolver.resolve(WildcardType.class))) //Разобраться позже
                .useDefaultResponseMessages(false)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .apiInfo(apiInfo())
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета cities
     * @return
     */
    @Bean
    public Docket companiesApi() {
        TypeResolver typeResolver = new TypeResolver();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Companies")
                .ignoredParameterTypes(File.class)
                .select()
                .paths(PathSelectors.ant("/companies/**"))
                .build()
//                .alternateTypeRules(new AlternateTypeRule(typeResolver.resolve(DeferredResult.class, Object.class), typeResolver.resolve(WildcardType.class))) //Разобраться позже
                .useDefaultResponseMessages(false)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .apiInfo(apiInfo())
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }


//    @Bean                                                                     //Не определяется в сваггере
//    public Docket chartsApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("Charts")
//                .select()
//                .paths(PathSelectors.ant("/charts/userdevice/**"))
//                .build()
//                .useDefaultResponseMessages(false)
//                .apiInfo(apiInfo())
//                .globalResponseMessage(RequestMethod.GET,
//                        newArrayList(new ResponseMessageBuilder()
//                                        .code(500)
//                                        .message("Internal Error")
//                                        .build(),
//                                new ResponseMessageBuilder()
//                                .code(400)
//                                .message("Bad Request")
//                                .build(),
//                                new ResponseMessageBuilder()
//                                        .code(403)
//                                        .message("Forbidden!!!")
//                                        .build()));
//    }

    /**
     * Правила формирования документации для работы с котроллерами пакета devices
     * @return
     */
    @Bean
    public Docket devicesApi() {
        TypeResolver typeResolver = new TypeResolver();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Device")
                .ignoredParameterTypes(DeferredResult.class)
                .select()
                .paths(PathSelectors.ant("/devices/**"))
                .build()
//                .alternateTypeRules(new AlternateTypeRule(typeResolver.resolve(DeferredResult.class, Object.class), typeResolver.resolve(WildcardType.class))) //Разобраться позже
                .useDefaultResponseMessages(false)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .apiInfo(apiInfo())
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета districts
     * @return
     */
    @Bean
    public Docket districtsApi() {
        TypeResolver typeResolver = new TypeResolver();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Districts")
                .ignoredParameterTypes(DeferredResult.class)
                .select()
                .paths(PathSelectors.ant("/districts/**"))
                .build()
//                .alternateTypeRules(new AlternateTypeRule(typeResolver.resolve(DeferredResult.class, Object.class), typeResolver.resolve(WildcardType.class))) //Разобраться позже
                .useDefaultResponseMessages(false)
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .apiInfo(apiInfo())
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }




    /**
     * Правила формирования документации для работы с котроллерами пакета events
     * @return
     */
    @Bean
    public Docket eventsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Events")
                .select()
                .paths(PathSelectors.ant("/events/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета externals
     * @return
     */
    @Bean
    public Docket externalsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Externals")
                .select()
                .paths(PathSelectors.ant("/externals/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета filter
     * @return
     */
    @Bean
    public Docket filterApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Filter")
                .select()
                .paths(PathSelectors.ant("/filter/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета geofence
     * @return
     */
    @Bean
    public Docket geofenceApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Geofence")
                .select()
                .paths(PathSelectors.ant("/geofences/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета geofence
     * @return
     */
    @Bean
    public Docket licenseApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("License")
                .select()
                .paths(PathSelectors.ant("/license/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета locations
     * @return
     */
    @Bean
    public Docket locationsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Locations")
                .select()
                .paths(PathSelectors.ant("/locations/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами
     * @return
     */
    @Bean
    public Docket loginApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Login")
                .select()
                .paths(PathSelectors.ant("/login/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета media
     * @return
     */
    @Bean
    public Docket mediaApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Media")
                .select()
                .paths(PathSelectors.ant("/media/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета profiles
     * @return
     */
    @Bean
    public Docket profilesApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Profiles")
                .select()
                .paths(PathSelectors.ant("/profiles/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета stats
     * @return
     */
    @Bean
    public Docket statsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Stats")
                .select()
                .paths(PathSelectors.ant("/stats/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета streams
     * @return
     */
    @Bean
    public Docket streamsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Streams")
                .select()
                .paths(PathSelectors.ant("/streams/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }


    /**
     * Правила формирования документации для работы с котроллерами пакета tasks
     * @return
     */
    @Bean
    public Docket tasksApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Tasks")
                .ignoredParameterTypes(File.class)
                .select()
                .paths(PathSelectors.ant("/tasks/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета terminals
     * @return
     */
    @Bean
    public Docket terminalsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Terminals")
                .ignoredParameterTypes(DeferredResult.class)
                .select()
                .paths(PathSelectors.ant("/terminals/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами пакета terminals
     * @return
     */
    @Bean
    public Docket transportApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Transport")
                .ignoredParameterTypes(DeferredResult.class)
                .select()
                .paths(PathSelectors.ant("/transport/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }


    /**
     * Правила формирования документации для работы с котроллерами пакета users
     * @return
     */
    @Bean
    public Docket usersApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Users")
                .ignoredParameterTypes(File.class)
                .select()
                .paths(PathSelectors.ant("/users/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

    /**
     * Правила формирования документации для работы с котроллерами
     * @return
     */
    @Bean
    public Docket routesApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Routes")
                .select()
                .paths(PathSelectors.ant("/routes/**"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalResponseMessage(RequestMethod.GET, global)
                .globalResponseMessage(RequestMethod.POST, global);
    }

//    /**
//     * Правила формирования документации для работы с котроллерами
//     * @return
//     */
//    @Bean
//    public Docket queriesApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("Queries")
//                .select()
//                .paths(PathSelectors.ant("/statistics/queries/**"))
//                .build()
//                .useDefaultResponseMessages(false)
//                .apiInfo(apiInfo())
//                .securitySchemes(Arrays.asList(apiKey()))
//                .securityContexts(Arrays.asList(securityContext()))
//                .globalResponseMessage(RequestMethod.GET, global);
//    }

//
//    /**
//     * Правила формирования документации для работы с котроллерами
//     * @return
//     */
//    @Bean
//    public Docket systemApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("System")
//                .ignoredParameterTypes(File.class)
//                .select()
//                .paths(PathSelectors.ant("/system/**"))
//                .build()
//                .useDefaultResponseMessages(false)
//                .apiInfo(apiInfo())
//                .securitySchemes(Arrays.asList(apiKey()))
//                .securityContexts(Arrays.asList(securityContext()))
//                .globalResponseMessage(RequestMethod.GET, global);
//    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Landau Monitoring API",
                "API Documentation for Landau Monitoring",
                "0.2.5",
                "Some terms of some Service",
                new Contact("", "", ""),
                "", "", Collections.emptyList());
    }

    private ApiKey apiKey() {
        return new ApiKey("apiKey", "token", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("apiKey", authorizationScopes));
    }

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
                .clientId(clientId)
                .clientSecret(clientSecret)
                .realm("realm")
                .appName("test-app")
                .scopeSeparator(",")
                .additionalQueryStringParams(null)
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }
}
