package com.landau.core.config.swagger;

import com.fasterxml.classmate.ResolvedType;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.Collections;
import springfox.documentation.schema.Maps;
import springfox.documentation.schema.Types;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.EnumTypeDeterminer;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spi.service.contexts.ParameterContext;
import springfox.documentation.spring.web.plugins.DocumentationPluginsManager;
import springfox.documentation.spring.web.readers.parameter.ExpansionContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1000)
public class SpringFoxDocsModelBuilder implements OperationBuilderPlugin {
    private final SpringFoxDocsModelExpander expander;
    private final EnumTypeDeterminer enumTypeDeterminer;

    @Autowired
    private DocumentationPluginsManager pluginsManager;

    @Autowired
    public SpringFoxDocsModelBuilder(SpringFoxDocsModelExpander expander, EnumTypeDeterminer enumTypeDeterminer) {
        this.expander = expander;
        this.enumTypeDeterminer = enumTypeDeterminer;
    }

    @Override
    public void apply(OperationContext context) {
        Field init = ReflectionUtils.findField(OperationBuilder.class, "parameters");
        init.setAccessible(true);
        try {
            init.set(context.operationBuilder(), new ArrayList<>());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        context.operationBuilder().parameters(context.getGlobalOperationParameters());
        context.operationBuilder().parameters(this.readParameters(context));
    }

    public boolean supports(DocumentationType delimiter) {
        return true;
    }

    private List<Parameter> readParameters(OperationContext context) {
        List<ResolvedMethodParameter> methodParameters = context.getParameters();
        List<Parameter> parameters = Lists.newArrayList();
        Iterator var4 = methodParameters.iterator();

        while(var4.hasNext()) {
            ResolvedMethodParameter methodParameter = (ResolvedMethodParameter)var4.next();
            ResolvedType alternate = context.alternateFor(methodParameter.getParameterType());
            if (!this.shouldIgnore(methodParameter, alternate, context.getIgnorableParameterTypes())) {
                ParameterContext parameterContext = new ParameterContext(methodParameter, new ParameterBuilder(), context.getDocumentationContext(), context.getGenericsNamingStrategy(), context);
                if (this.shouldExpand(methodParameter, alternate)) {
                    parameters.addAll(this.expander.expand(new ExpansionContext("", alternate, context)));
                } else {
                    parameters.add(this.pluginsManager.parameter(parameterContext));
                }
            }
        }

        return FluentIterable.from(parameters).filter(Predicates.not(this.hiddenParams())).toList();
    }

    private Predicate<Parameter> hiddenParams() {
        return input -> input.isHidden();
    }

    private boolean shouldIgnore(ResolvedMethodParameter parameter, ResolvedType resolvedParameterType, Set<Class> ignorableParamTypes) {
        if (ignorableParamTypes.contains(resolvedParameterType.getErasedType())) {
            return true;
        } else {
            return FluentIterable.from(ignorableParamTypes).filter(this.isAnnotation()).filter(this.parameterIsAnnotatedWithIt(parameter)).size() > 0;
        }
    }

    private Predicate<Class> parameterIsAnnotatedWithIt(final ResolvedMethodParameter parameter) {
        return input -> parameter.hasParameterAnnotation(input);
    }

    private Predicate<Class> isAnnotation() {
        return input -> Annotation.class.isAssignableFrom(input);
    }

    private boolean shouldExpand(ResolvedMethodParameter parameter, ResolvedType resolvedParamType) {
        return !parameter.hasParameterAnnotation(RequestBody.class) && !parameter.hasParameterAnnotation(RequestPart.class) && !parameter.hasParameterAnnotation(RequestParam.class) && !parameter.hasParameterAnnotation(PathVariable.class) && !Types.isBaseType(Types.typeNameFor(resolvedParamType.getErasedType())) && !this.enumTypeDeterminer.isEnum(resolvedParamType.getErasedType()) && !Collections.isContainerType(resolvedParamType) && !Maps.isMapType(resolvedParamType);
    }
}