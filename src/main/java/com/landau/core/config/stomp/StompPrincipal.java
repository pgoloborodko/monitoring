package com.landau.core.config.stomp;

import com.landau.core.users.entities.User;

import javax.security.auth.Subject;
import java.security.Principal;

public class StompPrincipal implements Principal {
    private User user;
    private String name;

    public StompPrincipal(String name, User user) {
        this.user = user;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    public User getUser() {
        return user;
    }
}
