package com.landau.core.config.stomp;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface SubscribeEvent {
    String value();
}
