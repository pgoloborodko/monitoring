package com.landau.core.config.stomp;

import com.lampa.republish.RepublisherException;
import com.landau.core.events.services.EventService;
import com.landau.core.users.repos.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class StompChannelInterceptor implements ChannelInterceptor {
    private List<Method> subscribes = new ArrayList<>();
    private List<Method> unsubscribes = new ArrayList<>();

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    private EventService eventService;

    @Autowired
    private UserRepo userRepo;

    @PostConstruct
    public void init(){
        Method[] methods = eventService.getClass().getDeclaredMethods();

        for(Method method : methods) {
            if(method.getAnnotation(SubscribeEvent.class) != null) {
                this.subscribes.add(method);
            }

            if(method.getAnnotation(UnsubscribeEvent.class) != null) {
                this.unsubscribes.add(method);
            }
        }

        log.info("init stomp");
    }


    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        if (StompCommand.CONNECT.equals(accessor.getCommand())) {
            Map<String, Object> sessionAttributes = accessor.getSessionAttributes();

            UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) sessionAttributes.get("user");
            accessor.setUser(new StompPrincipal(accessor.getSessionId(), userRepo.findById((Long) token.getDetails()).get()));
        }

        if (StompCommand.SUBSCRIBE.equals(accessor.getCommand())) {
            String dest = accessor.getDestination();

            for(Method method : this.subscribes) {
                SubscribeEvent subscribeEvent = method.getAnnotation(SubscribeEvent.class);
                String path = subscribeEvent.value();
            }
        }

        if (StompCommand.UNSUBSCRIBE.equals(accessor.getCommand())) {
            accessor = accessor;
            eventService.unsubscribe(accessor);
        }

        if (StompCommand.DISCONNECT.equals(accessor.getCommand())) {
            try {
                eventService.disconnect(accessor);
            } catch (RepublisherException e) {
                log.error("error disconnect queues", e);
            }
        }

        return message;
    }
}
