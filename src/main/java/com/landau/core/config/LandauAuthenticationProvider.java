package com.landau.core.config;

import com.landau.core.cities.entities.City;
import com.landau.core.cities.repos.CityRepo;
import com.landau.core.companies.entities.Company;
import com.landau.core.companies.repos.CompanyRepo;
import com.landau.core.districts.entities.District;
import com.landau.core.districts.repos.DistrictRepo;
import com.landau.core.transport.entities.Transport;
import com.landau.core.transport.repos.TransportRepo;
import com.landau.core.users.entities.Route;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.RouteRepo;
import com.landau.core.users.repos.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Аутентификация пользователя
 */
@Slf4j
@Component
public class LandauAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private CityRepo cityRepo;

    @Autowired
    private DistrictRepo districtRepo;

    @Autowired
    private TransportRepo transportRepo;

    @Autowired
    private RouteRepo routeRepo;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info("Authenticate...");
        Locale l = Locale.getDefault();
        String phone = ((String)authentication.getPrincipal()).replaceAll("\\+| |\\(|\\)|\\-", "");
        String password = (String)authentication.getCredentials();

        if(!phone.matches("7[0-9]{10}")) {
            throw new BadCredentialsException("invalid phone");
        }

        if (password.length() < 4) {
            throw new BadCredentialsException("invalid password");
        }

        User user = null;

        if(userRepo.count() == 0) {
            user = new User();
            user.setFirstName("Админ");
            user.setLastName("Админ");
            user.setSecondName("Админ");
            user.setPhone(phone);
            user.setPassword(bCryptPasswordEncoder.encode(password));
            user.setRole(User.Role.ADMIN);
            user = userRepo.save(user);

            Company company = new Company();
            company.setName("Компания");
            company = companyRepo.save(company);

            City city = new City();
            city.setName("Город");
            city.setCompany(companyRepo.findById(company.getId()).get());
            city.setRegion("77");
            city = cityRepo.save(city);

            District district = new District();
            district.setCity(cityRepo.findById(city.getId()).get());
            district.setCompany(companyRepo.findById(company.getId()).get());
            district.setName("Район");
            districtRepo.save(district);

            Transport transport = new Transport();
            transport.setCompany(companyRepo.findById(company.getId()).get());
            transport.setName("Audi");
            transportRepo.save(transport);

            Route route = new Route();
            route.setCompany(companyRepo.findById(company.getId()).get());
            route.setName("Маршрут");
            routeRepo.save(route);
        }
        else {
            user = userRepo.findByPhone(phone);
        }

        if(user == null) {
            throw new BadCredentialsException("phone not found");
        }
        if(!bCryptPasswordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("password invalid");
        }

        /*if (!user.getActivated()){
            throw new BadCredentialsException(messageSource.getMessage("use.notActivated", null, l));
        }*/

        UsernamePasswordAuthenticationToken res = new UsernamePasswordAuthenticationToken(phone, password, user.getAuthorities());
        res.setDetails(user.getId());
        return res;
    }

    public boolean supports(Class<?> authentication) {
        return true;
    }
}