package com.landau.core.config;

import com.landau.core.users.entities.Session;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.SessionRepo;
import com.landau.core.users.repos.TokenRepo;
import com.landau.core.users.repos.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class SessionFilter implements Filter {
    @Autowired
    TokenRepo tokenRepo;

    @Autowired
    SessionRepo sessionRepo;

    @Autowired
    UserRepo userRepo;

    private Timer timer = new Timer();
    private SessionTimerTask sessionTimerTask = new SessionTimerTask();
    private ConcurrentHashMap<String, Long> timerHashMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Session> sessionHashMap = new ConcurrentHashMap<>();

    @PostConstruct
    private void init(){
        timer.schedule(sessionTimerTask, 0, 5000);
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;

        Cookie cookie = WebUtils.getCookie(httpRequest, "SESSION");

        if(cookie != null) {
            String value = new String(Base64.getDecoder().decode(cookie.getValue()));

            if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof User) {
                User user = (User) SecurityContextHolder.getContext().getAuthentication().getDetails();

                if (!timerHashMap.containsKey(value)) {
                    timerHashMap.put(value, System.currentTimeMillis());

                    Session session = new Session();
                    session.setUser(user);
                    session.setSpringSessionId(value);
                    session.setOnline(true);
                    session.setStartSession(new Date());
                    session.setCompany(user.getCompany());

                    //session = sessionRepo.save(session);

                    sessionHashMap.put(value, session);
                }

                timerHashMap.put(value, System.currentTimeMillis());
            }

            if(sessionHashMap.containsKey(value)) {
                request.setAttribute("session", sessionHashMap.get(value));
            }
        }

        chain.doFilter(request, response);
    }

    private class SessionTimerTask extends TimerTask{
        @Override
        public void run() {
            Iterator<Map.Entry<String, Long>> it = timerHashMap.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, Long> entry = it.next();

                if (System.currentTimeMillis() - entry.getValue() > 300000){
                    timerHashMap.remove(entry.getKey());
                    sessionHashMap.remove(entry.getKey());

                    Optional<Session> optional = sessionRepo.findBySpringSessionIdAndOnlineOrderByIdDesc(entry.getKey(), true);
                    if (optional.isPresent()){
                        Session session = optional.get();
                        session.setOnline(false);
                        session.setEndSession(new Date());
                        sessionRepo.save(session);
                    }
                }
            }
        }
    }
}
