package com.landau.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.landau.core.common.filters.TokenAuthenticationFilter;
import com.landau.core.users.entities.User;
import com.landau.core.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final ObjectMapper objectMapper;

    public SecurityConfig(UserService userService, ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    private LandauAuthenticationProvider authProvider;

    @Autowired
    private TokenAuthenticationFilter tokenFilter;

    public User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getDetails();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(tokenFilter, BasicAuthenticationFilter.class);

        http.csrf().disable();
        http.headers().cacheControl().disable();
        http.headers().frameOptions().sameOrigin();

        http.authorizeRequests()
                .antMatchers("/login/status").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/common/**").permitAll()
                .antMatchers("/app/**").permitAll()
                .antMatchers("/images/**").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/webjars/springfox-swagger-ui/**").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/v2/api-docs**").permitAll()
                .antMatchers("/login/activate/*").permitAll()
                .antMatchers("/apks/**").permitAll()
                .antMatchers("/extjs/**").permitAll()
                .antMatchers("/tests/**").permitAll()
                .antMatchers("/login/restore").anonymous()
                .antMatchers("/login/reg").anonymous()
                .antMatchers("/login/auth").anonymous()
                .antMatchers("/login/registration").anonymous()
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginPage("/")
                .permitAll()
                .loginProcessingUrl("/login/auth")
                .usernameParameter("phone")
                .passwordParameter("password")
                .successHandler(this::loginSuccessHandler)
                .failureHandler(this::loginFailureHandler)

                .and()
                .logout()
                .logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> httpServletResponse.sendRedirect("/"));

        http.sessionManagement()
                .invalidSessionUrl("/");
//                .maximumSessions(1)
//                .sessionRegistry(sessionRegistry())
//                .expiredUrl("/");
    }

    private void loginSuccessHandler(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws IOException {

        response.setStatus(HttpStatus.OK.value());
        response.getWriter().print("[]");
    }

    private void loginFailureHandler(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException e) {

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }

        @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public static ServletListenerRegistrationBean httpSessionEventPublisher() {	//(5)
        return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
    }
}
