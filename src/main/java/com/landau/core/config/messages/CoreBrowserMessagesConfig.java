package com.landau.core.config.messages;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lampa.republish.RepublisherException;
import com.lampa.republish.core.Republish;
import com.lampa.republish.interfaces.IService;
import com.lampa.republish.interfaces.serializers.IDeserializer;
import com.lampa.republish.modules.RabbitModule;
import com.landau.messages.Interfaces.MessageService;
import com.landau.messages.core.CoreBrowserMessages;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.ConnectionFactory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
@Slf4j
@Data
@Configuration
public class CoreBrowserMessagesConfig {
    @Autowired
    private ApplicationContext applicationContext;

    @Value("${broker.host}")
    private String host;

    @Value("${broker.port}")
    private Integer port;

    @Value("${broker.user}")
    private String user;

    @Value("${broker.password}")
    private String password;

    @Value("${broker.heartbeat}")
    private Integer heartbeat;

    private Republish republish;

    private RabbitModule initRabbitModule() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        factory.setUsername(user);
        factory.setPassword(password);
        factory.setRequestedHeartbeat(heartbeat);
        factory.setAutomaticRecoveryEnabled (true);
        factory.setNetworkRecoveryInterval(5000);
        factory.setConnectionTimeout(30);

        RabbitModule.Exchange exchange = new RabbitModule.Exchange();
        exchange.setName("CoreBrowserMessages");
        exchange.setType(BuiltinExchangeType.HEADERS);
        exchange.setAutoDelete(false);
        exchange.setDurable(false);
        exchange.setGroup("browser");

        return new RabbitModule(factory, exchange);
    }

    private Republish initRepublish() throws RepublisherException {
        this.republish = new Republish();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        this.republish.setDeserializer(new IDeserializer() {
            @Override
            public Object deserialize(String value, byte[] bytes) throws Exception {
                return mapper.readValue(bytes, Class.forName(value));
            }
        });

        this.republish.addModule(initRabbitModule());
        this.republish.start();

        return this.republish;
    }

    @EventListener({ContextRefreshedEvent.class})
    public void contextRefreshedEvent(ContextRefreshedEvent e) throws RepublisherException {
        log.warn("contextRefreshEvent: {}", e);
        for(IService iService : applicationContext.getBeansOfType(IService.class).values()) {
            if(iService.getClass().getSimpleName().startsWith("Browser")) {
                republish.addService(iService);
            }
        }
    }

    @Bean
    public CoreBrowserMessages coreBrowserMessages() throws RepublisherException {
        return new CoreBrowserMessages(new MessageService(initRepublish()));
    }
}
