package com.landau.core.config.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.core.Republish;
import com.lampa.republish.interfaces.IService;
import com.lampa.republish.modules.RabbitModule;
import com.landau.messages.Interfaces.MessageService;
import com.landau.messages.core.CoreStreamerMessages;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Slf4j
@Configuration
public class CoreStreamerMessagesConfig {
    @Autowired
    private ApplicationContext applicationContext;

    @Value("${broker.host}")
    private String host;

    @Value("${broker.port}")
    private Integer port;

    @Value("${broker.user}")
    private String user;

    @Value("${broker.password}")
    private String password;

    @Value("${broker.heartbeat}")
    private Integer heartbeat;

    private Republish republish;

    private RabbitModule initRabbitModule() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        factory.setUsername(user);
        factory.setPassword(password);
        factory.setRequestedHeartbeat(heartbeat);
        factory.setAutomaticRecoveryEnabled (true);
        factory.setNetworkRecoveryInterval(5000);
        factory.setConnectionTimeout(30);

        RabbitModule.Exchange exchange = new RabbitModule.Exchange();
        exchange.setName("CoreStreamerMessages");
        exchange.setType(BuiltinExchangeType.DIRECT);
        exchange.setAutoDelete(false);
        exchange.setDurable(true);
        exchange.setGroup("streamer");

        return new RabbitModule(factory, exchange);
    }

    private Republish initRepublish() throws RepublisherException {
        this.republish = new Republish();
        this.republish.addModule(initRabbitModule());
        this.republish.start();

        return this.republish;
    }

    @EventListener({ContextRefreshedEvent.class})
    public void contextRefreshedEvent(ContextRefreshedEvent e) throws RepublisherException {
        log.warn("contextRefreshEvent: {}", e);
        for(IService iService : applicationContext.getBeansOfType(IService.class).values()) {
            if(iService.getClass().getSimpleName().startsWith("Streamer")) {
                republish.addService(iService);
            }
        }
    }

    @Bean
    public CoreStreamerMessages coreStreamerMessages() throws RepublisherException {
        return new CoreStreamerMessages(new MessageService(initRepublish()));
    }
}
