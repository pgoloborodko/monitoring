package com.landau.core.tasks.specs;

import com.landau.core.tasks.entities.Task;
import com.landau.core.tasks.filters.TaskFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class TaskSpec {
    public static Specification<Task> find(TaskFilter tasksFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (tasksFilter.getCreated() != null) {
                Date date = new Date(tasksFilter.getCreated().getTime());
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), date));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), new Date(date.getTime() + 1000 * 60 * 60 * 24)));
            }
            else {
                Date date = new Date();
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), date));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), new Date(date.getTime() + 1000 * 60 * 60 * 24)));
            }

            if (tasksFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), tasksFilter.getCompany()));
            }

            if (tasksFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("city"), tasksFilter.getCity()));
            }

            if (tasksFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("district"), tasksFilter.getDistrict()));
            }

            if (tasksFilter.getType() != null) {
                predicates.add(cb.equal(root.get("type"), tasksFilter.getType()));
            }

            //if (tasksFilter.getCompany() != null) {
            //   predicates.add(cb.equal(root.get("people").get("company"), tasksFilter.getCompany()));
            //}
            query.orderBy(cb.desc(root.get("created")));

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
