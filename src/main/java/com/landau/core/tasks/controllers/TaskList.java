package com.landau.core.tasks.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.tasks.entities.Task;
import com.landau.core.tasks.filters.TaskFilter;
import com.landau.core.tasks.repos.TaskRepo;
import com.landau.core.tasks.specs.TaskSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список задач
 */
@Api(tags = "Список задач", description = "Контроллер для получения списка задач")
@RestController(value = "TasksIndex")
@RequestMapping("/tasks")
public class TaskList extends Base {
    private TaskRepo taskRepo;

    @Autowired
    public TaskList(TaskRepo taskRepo) {
        this.taskRepo = taskRepo;
    }

    /**
     * Получение списка задач
     * @param tasksFilter поля фильтра
     * @return список задач
     */
    @ApiOperation(value = "Получение списка задач по фильтру", response = Task.class)
    @GetMapping
    public Page<Task> get(TaskFilter tasksFilter, Pageable pageable) {
        if (getUser().getRole() != User.Role.ADMIN) {
            tasksFilter.setCompany(getUser().getCompany());
        }

        return taskRepo.findAll(TaskSpec.find(tasksFilter), pageable);
    }
}
