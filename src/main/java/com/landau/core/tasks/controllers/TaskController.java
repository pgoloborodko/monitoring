package com.landau.core.tasks.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.services.ControllerService;
import com.landau.core.devices.entities.Device;
import com.landau.core.tasks.entities.Task;
import com.landau.core.tasks.messages.DeviceTaskMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Api(tags = "Постановка задач",
        description = "Контроллер обеспечивает возможность постановки задач на устройства")
@Data
@RestController("TaskController")
@RequestMapping("/tasks/sendTask")
public class TaskController {

    private ControllerService controllerService;

    private DeviceTaskMessages deviceTaskMessages;

    @Autowired
    public TaskController(ControllerService controllerService, DeviceTaskMessages deviceTaskMessages){
        this.controllerService = controllerService;
        this.deviceTaskMessages = deviceTaskMessages;
    }

    @ApiOperation(value = "")
    @PostMapping
    public Task sendTask(Task task, Device deviceToRequest){
        Device device = controllerService.verifyDevice(deviceToRequest);
        if (device != null){
            try {
                return deviceTaskMessages.sendTask(task, device);
            } catch (RepublisherException e) {
                e.printStackTrace();
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
        }
        return task;
    }
}
