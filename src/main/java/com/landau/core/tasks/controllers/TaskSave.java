package com.landau.core.tasks.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.tasks.entities.Task;
import com.landau.core.tasks.entities.TaskUser;
import com.landau.core.tasks.messages.DeviceTaskMessages;
import com.landau.core.tasks.repos.TaskRepo;
import com.landau.core.tasks.repos.TaskUserRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

/**
 * Сохранение задачи
 */
@Api(tags = "Cохранение задачи", description = "Контроллер может получать данные задачи, а также создавать новые")
@RestController(value = "TasksSave")
@RequestMapping("/tasks/save")
public class TaskSave extends Base {
    private TaskRepo taskRepo;
    private TaskUserRepo taskDeviceRepo;
    private DeviceTaskMessages deviceTaskMessages;

    @Autowired
    public TaskSave(TaskRepo taskRepo, TaskUserRepo taskDeviceRepo, DeviceTaskMessages deviceTaskMessages) {
        this.taskRepo = taskRepo;
        this.taskDeviceRepo = taskDeviceRepo;
        this.deviceTaskMessages = deviceTaskMessages;
    }

    /**
     * Получение данных задачи
     * @param task id задачи
     * @return объект задачи
     */
    @ApiOperation(value = "Получение данных задачи", response = Task.class)
    @Authority(Task.class)
    @GetMapping
    public Task get(Task task) {
        return task;//getTemplate();
    }

    //TODO обсудить контроллеры задач
    /**
     * Сохранение данных задачи
     * @param task объект задачи
     * @param result
     * @return false с ошибкой, либо true с объектом задачи
     */
    @ApiOperation(value = "Сохранение задачи", response = Task.class)
    @Authority(Task.class)
    @PostMapping
    public Task post(Task task, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getAllErrors().toString());
        }

        if(task.getComment() == null) {
            task.setComment("");
        }

        if(task.getType() == Task.Type.WALK_ROUTE) {
            task.setProfile(null);
        }
        else {
            task.setRoute(null);
        }

        task.setMaker(Task.Maker.USER);
        task.setMakerUser(getUser());
        task.setCreated(new Date());

        task = taskRepo.save(task);

        for(TaskUser taskUser :  task.getPerformers()) {
            taskUser.setTask(task);
            taskUser.setDevice(taskUser.getUser().getDevice());
            taskUser.setStatus(TaskUser.Status.OPEN);

            taskDeviceRepo.save(taskUser);
        }

        for(TaskUser taskDevice :  task.getPerformers()) {
            // TODO add send task to device
           /* deviceTaskMessages.sendTask();

            DeviceConnector connector = (DeviceConnector) webSocketServer.find(taskDevice.getDevice());

            if(connector != null) {
                if(task.getType() == Task.Type.WALK_ROUTE) {
                    connector.sendPatrol(task);
                }
                else {
                    connector.sendOrder(task, false);
                }
            }*/
        }

        return task;
    }
}
