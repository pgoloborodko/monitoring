package com.landau.core.tasks.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.biometric.entities.Biometric;
import com.landau.core.cities.entities.City;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.districts.entities.District;
import com.landau.core.profiles.entities.Profile;
import com.landau.core.tasks.repos.TaskRepo;
import com.landau.core.users.entities.Route;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Задачи
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(TaskRepo.class)
@ApiModel(description = "Задачи")
public class Task extends AbstractBusiness {
    /**
     * Тип задачи
     */
    public enum Type {
        /**
         * Найти человека
         */
        FIND_PROFILE,
        /**
         * Задержать человека
         */
        ORDER_PROFILE,
        /**
         * Патруль
         */
        WALK_ROUTE,
        /**
         * Запуск стрима
         */
        START_STREAM,
        /**
         * идентификация человека
         */
        IDENTY_PROFILE
    }

    /**
     * Создатель задачи
     */
    public enum Maker {
        /**
         * Пользователь
         */
        USER,
        /**
         * Система
         */
        SYSTEM,
        /**
         * Биометрический комплекс
         */
        BIOMETRIC
    }

    @Column
    @ApiModelProperty(value = "Тип задачи")
    private Type type;

    @Column
    @ApiModelProperty(value = "Комментарий к задаче")
    private String comment;

    @ManyToOne
    @ApiModelProperty(value = "Человек которого надо найти")
    private Profile profile;

    @OneToMany(cascade = CascadeType.ALL)
    @ApiModelProperty(value = "Получатели задачи")
    private List<TaskUser> performers;

    @Column
    @ApiModelProperty(value = "Тип создателя задачи")
    private Maker maker;

    @ManyToOne
    @ApiModelProperty(value = "Создатель задачи")
    private User makerUser;

    @ManyToOne
    @ApiModelProperty(value = "Биометрический комплекс задачи")
    private Biometric makerBiometric;

    @ManyToOne
    @ApiModelProperty(value = "Город задачи")
    private City city;

    @ManyToOne
    @ApiModelProperty(value = "Район задачи")
    private District district;

    @ManyToOne
    @ApiModelProperty(value = "Маршрут задачи")
    private Route route;

    @Column
    @ApiModelProperty(value = "Дата добавления задачи задачи")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Column
    @ApiModelProperty(value = "содержание задачи")
    private String content;

    @Transient
    private Byte[] image;
}
