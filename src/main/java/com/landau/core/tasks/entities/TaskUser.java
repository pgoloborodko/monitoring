package com.landau.core.tasks.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import com.landau.core.tasks.repos.TaskUserRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Задачи для устройств
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(TaskUserRepo.class)
@ApiModel(description = "Задачи для пользователей")
public class TaskUser extends AbstractId {
    /**
     * Статус задачи
     */
    public enum Status {
        /**
         * Открыта
         */
        OPEN,
        /**
         * Подтверждена
         */
        CONFIRMED,
        /**
         * Отменена
         */
        CANCELED,
        NOT_PERFORMED,
        /**
         * Выполнена
         */
        COMPLETED
    }

    @ManyToOne
    @ApiModelProperty(value = "Устройство")
    private Device device;

    @ManyToOne
    @ApiModelProperty(value = "Пользователь")
    private User user;

    @ManyToOne
    @JsonIgnore
    @ApiModelProperty(value = "Задача")
    private Task task;

    @Column
    @ApiModelProperty(value = "Статус задачи")
    private Status status;
}
