package com.landau.core.tasks.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.tasks.repos.TaskRepo;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.data.Task;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Data
@Slf4j
@Service
public class DeviceTaskMessages implements IService {
    @Autowired
    private TaskRepo taskRepo;

    @Autowired
    private CoreDeviceMessages coreDeviceMessages;

    @Autowired
    private CastService castService;

    /**
     * Метод отвечает за отпправку сообщения типа Task
     * @param task
     * @param device
     * @return
     * @throws RepublisherException
     */
    public com.landau.core.tasks.entities.Task sendTask(com.landau.core.tasks.entities.Task task, Device device) throws RepublisherException {
        coreDeviceMessages.send(device.getSerial(), castService.castToMessage(task));
        log.info("send message: {}", Task.class);
        return taskRepo.save(task);
    }

    /**
     *
     * @param device
     * @param taskObject
     * @param event
     * @return
    @ObjectListener(object = TaskObject.class)
    @ObjectPublisher(listener = BrowserMessages.class, send = Event.class)
    private Device TaskObject(Device device, TaskObject taskObject, Event event) {
        if(taskObject.getType() == TaskObject.Type.CALL || taskObject.getType() == TaskObject.Type.ORDER) {
            Task task = taskRepo.findById(taskObject.getId()).orElse(null);

            if(task != null) {
                TaskDevice taskDevice = taskDeviceRepo.findByTaskAndDevice(task, device);

                if(taskDevice != null) {
                    event.setType(taskObject.getType() == TaskObject.Type.CALL ? Event.Type.CALL : Event.Type.ORDER);
                    event.setCreated(new Date());
                    event.setDevice(device);
                    event.setUser(device.getUser());
                    event.setPeople(task.getFindPeople());

                    if(taskObject.getType() == TaskObject.Type.CALL) {
                        taskDevice.setStatus(taskObject.getResult() ? TaskDevice.Status.CONFIRMED : TaskDevice.Status.CANCELED);
                    }

                    if(taskObject.getType() == TaskObject.Type.ORDER) {
                        taskDevice.setStatus(taskObject.getResult() ? TaskDevice.Status.COMPLETED : TaskDevice.Status.NOT_PERFORMED);
                    }

                    if (taskDevice.getStatus() == TaskDevice.Status.CONFIRMED && taskObject.getType() == TaskObject.Type.CALL) {
                        sendOrder(task.getFindPeople(), new ArrayList<>(Arrays.asList(taskDevice.getDevice())));
                    }

                    taskDeviceRepo.save(taskDevice);
                }
            }

            eventRepo.save(event);
        }

        return device;
    }
     */

    /**
     * отправка задачи регам
     * @param task
    @ObjectListener(object = Task.class)
    private void onTask(Task task) {
        TaskObject taskObject = new TaskObject();
        taskObject.setId(task.getId());

        if(task.getType() == Task.Type.WALK_ROUTE) {
            // create object
            taskObject.setType(TaskObject.Type.PATROL);
            taskObject.setText("Патрулирование\n" + task.getRoute().getName());
        }
        else {
            taskObject.setType(task.getType() == Task.Type.ORDER_PEOPLE ? TaskObject.Type.ORDER : TaskObject.Type.CALL);
            taskObject.setText("Поиск человека " + (task.getFindPeople().getLastName() != null ? task.getFindPeople().getLastName(): "") + "\n" +
                    (task.getFindPeople().getFirstName() != null ? task.getFindPeople().getFirstName(): "") + "\n" +
                    (task.getFindPeople().getSecondName() != null ? task.getFindPeople().getSecondName(): ""));

            try {
                if(task.getFindPeople().getPhotoAsFile().exists()) {
                    taskObject.setImage(FileUtils.readFileToByteArray(task.getFindPeople().getPhotoAsFile()));
                }
            }
            catch (IOException | NullPointerException ex){
                ex.printStackTrace();
            }
        }

        for(TaskDevice taskDevice : task.getPerformers()) {
            messageService.send(taskDevice.getDevice(), taskObject);
        }
    }
     */

    /**
     *
     * @param people
     * @param com.landau.core.devices
    public void sendOrder(People people, List<Device> com.landau.core.devices) {
        List<TaskDevice> taskDevices = new ArrayList<>();

        for(Device device : com.landau.core.devices) {
            TaskDevice taskDevice = new TaskDevice();
            taskDevice.setStatus(TaskDevice.Status.OPEN);
            taskDevice.setDevice(device);

            taskDevices.add(taskDevice);
        }

        Task task = new Task();
        task.setPerformers(taskDevices);
        task.setType(Task.Type.ORDER_PEOPLE);
        messageService.broadcast(null, task);
    }
     */

    /**
     *
     * @param route
     * @param com.landau.core.devices
    public void sendPatrol(Route route, List<Device> com.landau.core.devices) {
        List<TaskDevice> taskDevices = new ArrayList<>();

        for(Device device : com.landau.core.devices) {
            TaskDevice taskDevice = new TaskDevice();
            taskDevice.setStatus(TaskDevice.Status.OPEN);
            taskDevice.setDevice(device);

            taskDevices.add(taskDevice);
        }

        Task task = new Task();
        task.setPerformers(taskDevices);
        task.setType(Task.Type.WALK_ROUTE);
        task.setRoute(route);
        messageService.broadcast(DeviceTaskMessages.class, task);
    }
     */
}
