package com.landau.core.tasks.repos;

import com.landau.core.tasks.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface TaskRepo extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {
    /**
     * Метод для тестов
     * @param type
     * @return
     */
    Optional<Task> findByType(Task.Type type);
}
