package com.landau.core.tasks.repos;

import com.landau.core.tasks.entities.TaskUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskUserRepo extends JpaRepository<TaskUser, Long> {
}
