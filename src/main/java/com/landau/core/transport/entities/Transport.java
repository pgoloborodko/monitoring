package com.landau.core.transport.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.devices.entities.Device;
import com.landau.core.transport.repos.TransportRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * Транспорт
 */
@Data
@Table
@Entity
@JsonInclude
@Repo(TransportRepo.class)
@ApiModel(description = "Транспорт")
public class Transport extends AbstractBusiness {
    /**
     * Тип транспорта
     */
    enum Type {
        /**
         * Машина
         */
        CAR,
        /**
         * Автобус
         */
        BUS,
        /**
         * Мото
         */
        MOTO,
        /**
         * Грузовик
         */
        TRUCK,
        /**
         * Другое
         */
        OTHER,
        /**
         * Фургон
         */
        WAGON
    }

    /**
     * Категория транспорта
     */
    enum Category {
        A, B, C, D
    }

    @Column
    @ApiModelProperty(value = "Название транспорта")
    private String name;

    @Column
    @ApiModelProperty(value = "Регистрационные номера транспорта")
    private String numbers;

    @Column
    @ApiModelProperty(value = "Тип транспорта")
    private Type type;

    @ManyToOne
    @ApiModelProperty(value = "Водитель транспорта")
    private User driver;

    @Column
    @ApiModelProperty(value = "Категория транспорта")
    private Category category;

    @OneToOne
    @ApiModelProperty(value = "Устройство закрепленное за транспортом")
    @NotFound(action = NotFoundAction.IGNORE)
    private Device device;
}
