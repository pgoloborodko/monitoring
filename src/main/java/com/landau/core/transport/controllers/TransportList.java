package com.landau.core.transport.controllers;


import com.landau.core.common.controllers.Base;
import com.landau.core.transport.entities.Transport;
import com.landau.core.transport.filters.TransportFilter;
import com.landau.core.transport.repos.TransportRepo;
import com.landau.core.transport.specs.TransportSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список транспорта
 */
@Api(tags = "Список транспорта", description = "Контроллер для получения списка транспорта")
@RestController("TransportIndex")
@RequestMapping("/transport")
public class TransportList extends Base {
    private TransportRepo transportRepo;

    public TransportList(TransportRepo transportRepo) {
        this.transportRepo = transportRepo;
    }

    /**
     * Получение списка транспорта
     * @param filter поиск по фильтру
     * @return список транспорта
     */
    @ApiOperation(value = "Получение списка транспорта", response = Transport.class)
    @GetMapping
    public Page<Transport> get(TransportFilter filter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            filter.setCompany(getUser().getCompany());
        }

        return transportRepo.findAll(TransportSpec.find(filter), pageable);
    }
}
