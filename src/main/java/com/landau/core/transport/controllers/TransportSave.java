package com.landau.core.transport.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.transport.entities.Transport;
import com.landau.core.transport.repos.TransportRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

/**
 * Сохранение транспорта
 */
@Api(tags = "Сохранение транспорта", description = "Контроллер может получать данные транспорта, а также создавать новые")
@RestController(value = "TransportSave")
@RequestMapping("/transport/save")
public class TransportSave extends Base {
    private TransportRepo transportRepo;

    @Autowired
    public TransportSave(TransportRepo transportRepo) {
        this.transportRepo = transportRepo;
    }

    /**
     * Получение данных транспорта
     * @param transport id транспорта
     * @return объект транспорта
     */
    @ApiOperation(value = "Получение данных транспорта", response = Transport.class)
    @Authority(Transport.class)
    @GetMapping
    public Transport get(Transport transport) {
        return transport;
    }

    /**
     * Сохранение данных транспорта
     * @param transport объект транспорта
     * @param result
     * @return false с ошибкой, либо true с объектом транспорта
     */
    @ApiOperation(value = "Сохранение транспорта", response = Transport.class)
    @Authority(Transport.class)
    @PostMapping
    public Transport post(Transport transport, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getAllErrors().toString());
        }

        Optional<Transport> findOtherTransport = transportRepo.findByDevice(transport.getDevice());
         if(findOtherTransport.isPresent()) {
            if(transport.getId() == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "other transport already have a selected device");
            }
            else if(!transport.getId().equals(findOtherTransport.get().getId())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "other transport already have a selected device");
            }
        }

        return transportRepo.save(transport);//.getDTO(TransportDTO.class);
    }
}
