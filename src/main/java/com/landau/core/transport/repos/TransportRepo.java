package com.landau.core.transport.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.transport.entities.Transport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface TransportRepo extends JpaRepository<Transport, Long>, JpaSpecificationExecutor<Transport> {
    Optional<Transport> findByDevice(Device device);
}