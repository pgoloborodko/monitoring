package com.landau.core.transport.specs;

import com.landau.core.transport.entities.Transport;
import com.landau.core.transport.filters.TransportFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class TransportSpec {
    public static Specification<Transport> find(TransportFilter transportFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (transportFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), transportFilter.getCompany()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
