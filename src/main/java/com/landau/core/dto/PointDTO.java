package com.landau.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vividsolutions.jts.geom.Coordinate;

/**
 * DTO-объект координаты
 */
public class PointDTO {
    @JsonProperty("x")
    Double x;

    @JsonProperty("y")
    Double y;

    /**
     * Координата "х"
     * @return
     */
    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    /**
     * Координата "у"
     * @return
     */
    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public PointDTO(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public PointDTO(Coordinate coordinate) {
        this.x = coordinate.x;
        this.y = coordinate.y;
    }

    @JsonIgnore
    public Coordinate getCoordinate() {
        return new Coordinate(x, y);
    }
}
