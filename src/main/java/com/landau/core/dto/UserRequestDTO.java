package com.landau.core.dto;

import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@ApiModel(description = "DTO-объект для регистрации пользователя")
@Data
public class UserRequestDTO {

    @ApiModelProperty(value = "Идентификатор пользователя")
    private Long id;

    @ApiModelProperty(value = "Телефон пользователя")
    private String phone;

    @ApiModelProperty(value = "Пароль пользователя")
    private String password;

    @ApiModelProperty(value = "Имя пользователя")
    private String firstName;

    @ApiModelProperty(value = "Фамилия пользователя")
    private String lastName;

    @ApiModelProperty(value = "Идентификатор устройства пользователя")
    private Long device;

    @ApiModelProperty(value = "Отчество пользователя")
    private String secondName;

    @ApiModelProperty(value = "Номер паспорта пользователя")
    private String passportNum;

    @ApiModelProperty(value = "Серия паспорта пользователя")
    private String passportSeries;

    @ApiModelProperty(value = "Кем выдан паспорт пользователя")
    private String passportIssue;

    @ApiModelProperty(value = "Номер водительского удостоверения пользователя")
    private String driverNum;

    @ApiModelProperty(value = "Серия водительского удостоверения пользователя")
    private String driverSeries;

    @ApiModelProperty(value = "Категория А да/нет")
    private Boolean driverA = false;

    @ApiModelProperty(value = "Категория B да/нет")
    private Boolean driverB = false;

    @ApiModelProperty(value = "Категория C да/нет")
    private Boolean driverC = false;

    @ApiModelProperty(value = "Категория D да/нет")
    private Boolean driverD = false;

    @ApiModelProperty(value = "Путь к фото пользователя")
    private String photo;

    @ApiModelProperty(value = "Роль пользователя")
    private User.Role role;

    @ApiModelProperty(value = "Идентификатор города пользователя")
    private Long city;

    @ApiModelProperty(value = "Идентификатор района пользователя")
    private Long district;

    @ApiModelProperty(value = "Фото-файл пользователя")
    private MultipartFile photoFile;

    private String photoFileHidden;

    @ApiModelProperty(value = "Идентификатор компании пользователя")
    private Long company;

    @ApiModelProperty(value = "Описание пользователя")
    private String description;

    @ApiModelProperty(value = "Электронная почта пользователя")
    private String email;
}
