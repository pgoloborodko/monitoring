package com.landau.core.dto;

import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
@Api(tags = "DTO-объект списка событий для списка пользователей за месяц")
public class EventsPerMonthDTO {
    @ApiModelProperty(name = "день месяца", example = "0")
    private Integer dayOfMonth;

    @ApiModelProperty(name = "Список пользователей")
    private List<User> users;

    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
