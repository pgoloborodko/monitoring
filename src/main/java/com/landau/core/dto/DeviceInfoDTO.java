package com.landau.core.dto;

import com.landau.core.transport.entities.Transport;

/**
 * DTO-объект для информации об устройстве
 */
public class DeviceInfoDTO {
    private Boolean online;
    private String currentPhotoReference;
    private String fullName;
    private String userPhoto;
    private Transport transport;
    private Integer charge;

    /**
     * Онлайн(да/нет)
     * @return
     */
    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    /**
     * Текущая ссылка на фото
     * @return
     */
    public String getCurrentPhotoReference() {
        return currentPhotoReference;
    }

    public void setCurrentPhotoReference(String currentPhotoReference) {
        this.currentPhotoReference = currentPhotoReference;
    }

    /**
     * Полное имя владельца
     * @return
     */
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Фото пользователя
     * @return
     */
    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    /**
     * Заряд устройства
     * @return
     */
    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

}
