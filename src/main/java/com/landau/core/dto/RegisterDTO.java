package com.landau.core.dto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * DTO-объект регистрации оператора
 */
@Data
@ApiModel(description = "DTO-объект регистрации оператора")
public class RegisterDTO {
    @ApiModelProperty(value = "Телефон", example = "9876543210")
    private String phone;

    @ApiModelProperty(value = "Пароль", example = "123456789")
    private String password;

    @ApiModelProperty(value = "Имя компании", example = "testCompany")
    private String companyName;

    @ApiModelProperty(value = "Массив разрешений лицензии", example = "[0, 2]")
    private byte[] monitoringPropertiesArray;

    /**
     * Телефон оператора
     * @return
     */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Пароль оператора
     * @return
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Название компании
     * @return
     */
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * байтовый массив файла с лицензией
     * @return
     */
    public byte[] getMonitoringPropertiesArray() {
        return monitoringPropertiesArray;
    }

    public void setMonitoringPropertiesArray(byte[] monitoringPropertiesArray) {
        this.monitoringPropertiesArray = monitoringPropertiesArray;
    }
}
