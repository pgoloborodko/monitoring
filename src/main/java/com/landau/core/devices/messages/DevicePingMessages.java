package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.lampa.republish.messages.Message;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.repos.StatsDeviceOnlineUptimeRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.objects.telemetry.Ping;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Data
@Service
public class DevicePingMessages implements IService {
    private static long timeout = 30000;
    private ConcurrentHashMap<Long, Timer> pings = new ConcurrentHashMap<>();

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private CoreBrowserMessages browser;

    @Autowired
    private StatsDeviceOnlineUptimeRepo statsDeviceOnlineUptimeRepo;

    /**
     * wait ping from device
     * @param ping
     */
    @ReConsumer(listen = Ping.class)
    public void consumePing(Ping ping) {
        log.info("consume message: {}", Ping.class);

        Optional<Device> optionalDevice = deviceRepo.findBySerial(ping.getSerial());

        if(optionalDevice.isPresent()) {
            Device device = optionalDevice.get();

            synchronized (device.getSerial().intern()) {
                if (pings.containsKey(device.getId())) {
                    pings.get(device.getId()).cancel();
                }
                else {
                    device.setOnline(true);
                    deviceRepo.save(device);

                    StatsDeviceOnlineUptime uptime = new StatsDeviceOnlineUptime();
                    uptime.setDevice(device);
                    uptime.setStartSession(new Date());
                    statsDeviceOnlineUptimeRepo.save(uptime);

                    if (device.getUser() != null) {
                        TreeMap<String, Object> filter = new TreeMap<>();
                        filter.put("device", device.getId());

                        if (device.getUser().getCompany() != null) {
                            filter.put("company", device.getUser().getCompany().getId());
                        }

                        try {
                            filter.put("user", device.getUser().getId());
                            browser.getMessageService().getRepublish().produce(new Message<>(device.getUser(), filter));

                        } catch(RepublisherException e){
                            e.printStackTrace();
                        }
                    }
                }

                Timer timer = new Timer();
                timer.schedule(new PingTimerTask(device), timeout);
                pings.put(device.getId(), timer);
            }
        }
    }


    private class PingTimerTask extends TimerTask {
        Device device;

        public PingTimerTask(Device device) {
            device.setLastOnline(new Date());
            device = deviceRepo.save(device);
            this.device = device;
        }

        @Override
        public void run() {
            synchronized (device.getSerial().intern()) {
                log.info("disconnect device {}, timer {}", device.getSerial(), this);

                Optional<Device> optionalDevice = deviceRepo.findById(device.getId());

                if (optionalDevice.isPresent()) {
                    Device device = optionalDevice.get();
                    device.setOnline(false);
                    deviceRepo.save(device);

                    pings.remove(device.getId());

                    StatsDeviceOnlineUptime uptime = statsDeviceOnlineUptimeRepo.findFirstByDeviceAndEndSessionNull(device);

                    if(uptime != null) {
                        uptime.setEndSession(new Date());
                        statsDeviceOnlineUptimeRepo.save(uptime);
                    }

                    if (device.getUser() != null) {
                        TreeMap<String, Object> filter = new TreeMap<>();
                        filter.put("device", device.getId());

                        if (device.getUser().getCompany() != null) {
                            filter.put("company", device.getUser().getCompany().getId());
                        }

                        try {
                            filter.put("user", device.getUser().getId());
                            browser.getMessageService().getRepublish().produce(new Message<>(device.getUser(), filter));

                        } catch(RepublisherException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
