package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.apps.repos.AppDeviceCommandRepo;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceModelRepo;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.repos.StatsDeviceOnlineUptimeRepo;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.action.GetAppList;
import com.landau.objects.props.DeviceProps;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Data
@Slf4j
@Service
public class DevicePropsMessages implements IService {
    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private CoreDeviceMessages messages;

    @Autowired
    private DeviceModelRepo deviceModelRepo;

    @Autowired
    private StatsDeviceOnlineUptimeRepo statsDeviceOnlineUptimeRepo;

    @Autowired
    private CastService castService;

    @Autowired
    private AppDeviceCommandRepo appDeviceCommandRepo;

    @Autowired
    private DeviceAppMessages deviceAppMessages;


    /**
     * Метод отвечает за обработку принятого сообщения типа DeviceProps
     * @param deviceProps
     */
    @ReConsumer(listen = DeviceProps.class)
    public void consumeDeviceProps(DeviceProps deviceProps) {
        log.info("consume message: {}", deviceProps.getClass());
        Device device = castService.castToEntity(deviceProps);
        if (device != null){
            deviceRepo.save(device);
            log.info("save: {}", deviceProps.getClass().getSimpleName());

            try {
                deviceAppMessages.getApkList(device);
                log.info("send message: {}", GetAppList.class);

                List<AppDeviceCommand> commands = appDeviceCommandRepo.findAllByDeviceAndComplete(device, false);
                for (AppDeviceCommand command : commands){
                    deviceAppMessages.installApk(command);
                    command.setComplete(true);
                    appDeviceCommandRepo.save(command);
                }

            } catch (RepublisherException e) {
                e.printStackTrace();
            }
        }
        else {
            log.warn("device is: {}" , (Object) null);
        }
    }
}
