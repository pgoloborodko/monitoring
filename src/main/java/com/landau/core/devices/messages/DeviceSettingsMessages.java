package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.devices.repos.DeviceSettingsRepo;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.action.SetDeviceSettings;
import com.landau.objects.settings.DeviceSettings;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Slf4j
@Service
public class DeviceSettingsMessages implements IService {
    @Autowired
    private CoreDeviceMessages messages;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private DeviceSettingsRepo deviceSettingsRepo;

    @Autowired
    private CastService castService;

    /**
     * Метод отвечает за отправку сообщения типа SetDeviceSettings
     * @param deviceSettings
     * @throws RepublisherException
     */
    public void setDeviceSettings(com.landau.core.devices.entities.DeviceSettings deviceSettings) throws RepublisherException {
        messages.send(deviceSettings.getDevice().getSerial(), castService.castToMessage(deviceSettings));
        log.info("send message: {}", SetDeviceSettings.class);

        deviceSettings.setSended(true);
        deviceSettingsRepo.save(deviceSettings);
    }

    //TODO напсиать тест
    /**
     * Метод отвечает за обработку принятого сообщения типа DeviceSettings
     * @param deviceSettingsMessage
     */
    @ReConsumer(listen = DeviceSettings.class, autoDelete = false)
    public void consumeDeviceSettings(DeviceSettings deviceSettingsMessage) throws RepublisherException {
        log.info("consume message: " + deviceSettingsMessage.getClass().getSimpleName());
        Device device = deviceRepo.findBySerial(deviceSettingsMessage.getSerial()).orElse(null);

        if (device != null){
            Optional<com.landau.core.devices.entities.DeviceSettings> optionalDeviceSettings = deviceSettingsRepo.findByDeviceAndSendedIsFalse(device);

            if (optionalDeviceSettings.isPresent()){
                setDeviceSettings(optionalDeviceSettings.get());
            }
            else {
                com.landau.core.devices.entities.DeviceSettings deviceSettings = castService.castToEntity(deviceSettingsMessage);
                deviceSettingsRepo.save(deviceSettings);
                log.info("save: " + deviceSettings.getClass().getSimpleName());
            }
        }
        else {
            log.error("device not fund: {}", deviceSettingsMessage.getSerial());
        }
    }
}
