package com.landau.core.devices.messages;

import com.lampa.republish.interfaces.IService;
import com.lampa.republish.interfaces.hooks.IHook;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.profiles.repos.ProfileRepo;
import com.landau.objects.props.DeviceProps;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Data
@Slf4j
@Service
public class DeviceBiometricMessages implements IService, IHook<DeviceProps> {
    @Value("${server.address}")
    private String siteUrl;


    @Autowired
    private ProfileRepo profileRepo;

    @Autowired
    private DeviceRepo deviceRepo;

/*    @Autowired
    private MessageService messageService;*/

    @Override
    public void postConsume(DeviceProps deviceProps) {
        log.info("update templates and com.landau.core.users for device " + deviceProps.getSerial());
        Optional<Device> optionalDevice = deviceRepo.findBySerial(deviceProps.getSerial());

        if(optionalDevice.isPresent()) {
            //sendNewUsers(optionalDevice.get());
            //sendTemplatesByCompany(optionalDevice.get());
        }
    }

    /**
     *
     * @param device
     * @param identyObject
     * @param event
     * @return

    @ObjectListener(object = TemplateObject.class)
    @ObjectPublisher(listener = DeviceBiometricMessages.class, send = Event.class)
    @ObjectPublisher(listener = BrowserListener.class, send = Event.class)
    private Device IdentyObject(Device device, IdentyObject identyObject, Event event) {
        event.setType(Event.Type.IDENTY);
        event.setCreated(new Date());
        event.setDevice(device);
        event.setPeople(peopleRepo.findById(identyObject.getId()).orElse(null));

        eventRepo.save(event);

        return device;
    }*/

    /**
     * обновление профлия
     * @param people
     * @return

    @ObjectListener(object = People.class)
    private void onPeopleChange(People people) {
        // поиск
        messageService.findByType(Device.class)
                .stream()
                .filter(device -> device.getEntity().getCompany().equals(people.getCompany()))
                .peek(deviceAbstractClient -> deviceAbstractClient.send(people));
    } */


    /**
     *
     * @param people
     */
 /*   public void sendPeoplePhoto(People people) {
        sendPeoplePhoto(people,"Найден человек", false);
    }*/

/*    public void sendPeoplePhoto(People people, String message, boolean simple) {
        // create object
        TaskObject taskObject = new TaskObject();
        //taskObject.setId(task.getId());
        taskObject.setType(TaskObject.Type.PHOTO);

        taskObject.setResult(simple);

        taskObject.setText(message + ". " + people.getLastName() + "\n" +
                people.getFirstName() + "\n" +
                people.getSecondName() + "\n" + people.getDescription());
        try {
            if(people.getPhotoAsFile().exists()) {
                taskObject.setImage(FileUtils.readFileToByteArray(people.getPhotoAsFile()));
            }
        }
        catch (IOException | NullPointerException ex){
            ex.printStackTrace();
        }

        //send(taskObject);
    }*/


    /**
     *
     * @param device
     */
/*    private void sendTemplatesByCompany(Device device) {

        if(device.getCompany() != null) {
            List<People> peoples = peopleRepo.findAllByPhotoTemplateIsNotNullAndCompany(device.getCompany());

            if(peoples.size() > 0) {
                TaskObject taskObject = new TaskObject();
                taskObject.setType(TaskObject.Type.NEW_TEMPLATES);

                for(People people : peoples) {
                    taskObject.addImage(people.getId(), ArrayUtils.toPrimitive(people.getPhotoTemplate()));
                }

                messageService.send(device, taskObject);
            }
        }
    }*/

    /**
     *
     * @return
     */
/*    public void sendNewUsers(Device device) {
        if(device.getCompany() != null) {
            List<People> peoples = peopleRepo.findAllByPhotoTemplateIsNullAndPhotoIsNotNullAndCompany(device.getCompany());

            if(peoples.size() > 0) {
                TaskObject taskObject = new TaskObject();
                taskObject.setType(TaskObject.Type.NEW_IMAGES);

                for(People people : peoples) {
                    try {
                        taskObject.addImage(people.getId(), FileUtils.readFileToByteArray(people.getPhotoAsFile()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                messageService.send(device, taskObject);
            }
        }
    }*/

}
