package com.landau.core.devices.messages;


import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.lampa.republish.messages.Message;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceTelemetry;
import com.landau.core.devices.entities.DeviceWifiLog;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.devices.repos.DeviceTelemetryRepo;
import com.landau.core.devices.repos.DeviceWifiLogRepo;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.objects.telemetry.WiFiTelemetry;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.TreeMap;

@Data
@Service
@Slf4j
public class DeviceTelemetryMessages implements IService {
    @Autowired
    private DeviceTelemetryRepo deviceTelemetryRepo;

    @Autowired
    private LocationRepo locationRepo;

    @Autowired
    private DeviceWifiLogRepo deviceWifiLogRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private CastService castService;

    @Autowired
    private CoreBrowserMessages browser;

    /**
     * метод отвечает за обработку принятого сообщения типа Location
     * @param deviceLocationMessage
     */
    @ReConsumer(listen = com.landau.objects.telemetry.Location.class, autoDelete = false)
    protected void consumeDeviceLocation(com.landau.objects.telemetry.Location deviceLocationMessage) throws RepublisherException {
        log.info("consume message: " + deviceLocationMessage.getClass().getSimpleName());
        Location location = castService.castToEntity(deviceLocationMessage);
        if (location != null) {
            TreeMap<String, Object> filter = new TreeMap<>();
            Device device = location.getDevice();
            filter.put("device", device.getId());

            if(device.getUser() != null) {
                location.setUser(device.getUser());
                filter.put("user", device.getUser().getId());
            }

            device.setLocation(location);

            locationRepo.save(location);
            deviceRepo.save(device);

            browser.getMessageService().getRepublish().produce(new Message<>(location, filter));

            log.info("save: " + location.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", Location.class);
        }
    }

    /**
     * метод отвечает за обработку принятого сообщения типа WiFiTelemetry
     * @param wiFiTelemetryMessage
     */
    @ReConsumer(listen = WiFiTelemetry.class, autoDelete = false)
    public void consumeWifiObject(WiFiTelemetry wiFiTelemetryMessage) {
        log.info("consume message: " + wiFiTelemetryMessage.getClass().getSimpleName());
        DeviceWifiLog wifiLog = castService.castToEntity(wiFiTelemetryMessage);
        if (wifiLog != null){
            deviceWifiLogRepo.save(wifiLog);
            log.info("save: " + wifiLog.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", DeviceWifiLog.class);
        }
    }


    //TODO написать тест
    /**
     * метод отвечает за обработку принятого сообщения типа DeviceTelemetry
     * @param telemetryMessage
     */
    @ReConsumer(listen = com.landau.objects.telemetry.DeviceTelemetry.class, autoDelete = false)
    public void consumeDeviceTelemetry(com.landau.objects.telemetry.DeviceTelemetry telemetryMessage) {
        log.info("consume message: " + telemetryMessage.getClass().getSimpleName());
        DeviceTelemetry deviceTelemetry = castService.castToEntity(telemetryMessage);
        if (deviceTelemetry != null){
            int charge = (int) deviceTelemetry.getDeviceCharge();
            deviceTelemetry.getDevice().setCharge(charge);
            deviceRepo.save(deviceTelemetry.getDevice());
            deviceTelemetryRepo.save(deviceTelemetry);
            log.info("save: " + deviceTelemetry.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", DeviceTelemetry.class);
        }
    }
}
