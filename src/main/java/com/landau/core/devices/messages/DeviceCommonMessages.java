package com.landau.core.devices.messages;

import com.lampa.republish.interfaces.IService;
import com.lampa.republish.interfaces.hooks.IHook;
import com.landau.core.devices.repos.DeviceModelRepo;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.profiles.repos.ProfileRepo;
import com.landau.core.stats.repos.StatsDeviceOnlineUptimeRepo;
import com.landau.objects.base.Serial;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Data
@Slf4j
@Service
public class DeviceCommonMessages implements IService, IHook<Serial> {
    @Value("${server.address}")
    private String siteUrl;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private StatsDeviceOnlineUptimeRepo statsDeviceOnlineUptimeRepo;

    @Autowired
    private ProfileRepo peopleRepo;

    @Autowired
    private DeviceModelRepo deviceModelRepo;



    /*@Autowired
    CoreDeviceMessages messageService;*/



    /**
     *
     * @param device
     * @param radioObject
     * @param chatMessage
     * @return

    @ObjectListener(object = RadioObject.class)
    @ObjectPublisher(listener = BrowserMessages.class, send = ChatMessage.class)
    @ObjectPublisher(listener = DeviceCommonMessages.class, send = ChatMessage.class)
    private Device RadioObject(Device device, RadioObject radioObject, ChatMessage chatMessage) {
        chatMessage.setType(ChatMessage.Type.AUDIO);
        chatMessage.setExtraMessage(ArrayUtils.toObject(radioObject.getBytes()));
        chatMessage.setUser(device.getUser());
        chatMessage.setDate(new Date());
        chatMessage.setDevice(device);

        return device;
    }*/

    /**
     *
     * @param device
     * @param templateObject
     * @param people
     * @return

    @ObjectListener(object = TemplateObject.class)
    @ObjectPublisher(listener = DeviceCommonMessages.class, send = People.class)
    private Device TemplateObject(Device device, TemplateObject templateObject, People people) {
        people = peopleRepo.findById(templateObject.getId()).orElse(null);

        if(people != null) {
            people.setPhotoTemplate(templateObject.getTemplate());
            peopleRepo.save(people);
        }

        return device;
    }*/


/*    @ObjectListener(object = TaskObject.class)
    @ObjectPublisher(listener = DeviceMessages.class)
    private Device RequestStreamObject(Device device, RequestStreamObject requestStreamObject) {
        if(request.getResponseDeviceObjectV1() != null && request.getResponseDeviceObjectV1().getSerial() != null) {

            Optional<Device> optionalDevice = deviceRepo.findBySerial(request.getResponseDeviceObjectV1().getSerial());

            if(optionalDevice.isPresent()) {
                DeviceConnector deviceConnector = (DeviceConnector) webSocketServer.find(optionalDevice.get());

                if (deviceConnector != null) {
                    deviceConnector.streamKeepAlive();
                }
            }
        }

        return device;
    }*/

}
