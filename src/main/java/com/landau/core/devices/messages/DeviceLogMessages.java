package com.landau.core.devices.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.action.GetLogFile;
import com.landau.objects.action.GetLogList;
import com.landau.objects.data.LogFile;
import com.landau.objects.data.LogList;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
@Data
@Component
@Slf4j
public class DeviceLogMessages implements IService {
    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private CoreDeviceMessages messages;

    private HashMap<String, DeferredResult<Object>> messagesStack = new HashMap<>();

    /**
     * get log list from device
     * @param device
     * @return
     * @throws RepublisherException
     */
    public DeferredResult<Object> getLogList(Device device) throws RepublisherException {
        DeferredResult<Object> deferredResult = new DeferredResult<>();
        messagesStack.put(device.getSerial(), deferredResult);
        messages.send(device.getSerial(), new GetLogList());
        log.info("send to Device: {}", GetLogList.class);

        return deferredResult;
    }

    /**
     * wait log list from device
     * @param logList
     */
    @ReConsumer(listen = LogList.class)
    protected void consumeLogList(LogList logList) {
        Optional<Device> optionalDevice = deviceRepo.findBySerial(logList.getSerial());
        log.info("consume: {}", LogList.class);

        if(optionalDevice.isPresent() && messagesStack.containsKey(logList.getSerial())) {
            DeferredResult<Object> deferredResult = messagesStack.get(logList.getSerial());
            deferredResult.setResult(logList.getLogs());

            messagesStack.remove(logList.getSerial());
        }
    }

    /**
     * get log file from device
     * @param device
     * @param file
     * @return
     * @throws RepublisherException
     */
    public DeferredResult<Object> getLogFile(Device device, String file) throws RepublisherException {
        DeferredResult<Object> deferredResult = new DeferredResult<>();

        messagesStack.put(device.getSerial(), deferredResult);
        messages.send(device.getSerial(), new GetLogFile(file));
        log.info("send to Device: {}", GetLogFile.class);

        return deferredResult;
    }



    /**
     * wait log file from device
     * @param logFile
     */
    @ReConsumer(listen = LogFile.class)
    public void consumeLogFile(LogFile logFile) {
        log.info("consume: {}", LogFile.class);
        Optional<Device> optionalDevice = deviceRepo.findBySerial(logFile.getSerial());

        if(optionalDevice.isPresent()) {
            if(messagesStack.containsKey(logFile.getSerial())) {
                DeferredResult<Object> deferredResult = messagesStack.get(logFile.getSerial());

                Byte[] bytes = logFile.getFile();

                if(bytes != null) {
                    ByteArrayInputStream bis = new ByteArrayInputStream(ArrayUtils.toPrimitive(bytes));

                    try (GZIPInputStream in = new GZIPInputStream(bis)){
                        String theString = IOUtils.toString(in);
                        deferredResult.setResult(theString);

                    } catch (IOException e){
                        deferredResult.setErrorResult(e.getMessage());
                    }
                }
                else {
                    deferredResult.setErrorResult("null bytes");
                }

                messagesStack.remove(logFile.getSerial());
            }
        }
    }
}
