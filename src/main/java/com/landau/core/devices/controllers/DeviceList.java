package com.landau.core.devices.controllers;

import com.landau.core.common.ApiPageable;
import com.landau.core.common.controllers.Base;
import com.landau.core.common.services.ControllerService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.filters.DevicesFilter;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.devices.specs.DevicesSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Получение списка регистраторов
 */
@Api(tags = "Список устройств", description = "Контроллер обеспечивает получение списка устройств")
@RestController("DevicesIndex")
@RequestMapping("/devices")
public class DeviceList extends Base {
    private DeviceRepo deviceRepo;
    private ControllerService controllerService;

    @Autowired
    public DeviceList(DeviceRepo deviceRepo, ControllerService controllerService) {
        this.deviceRepo = deviceRepo;
        this.controllerService = controllerService;
    }

    /**
     * Получение списка устройств по фильтру
     * @param devicesFilter поля фильтра
     * @return список устройств
     */
    @ApiPageable
    @ApiOperation(value = "Получение списка устройств по фильтру")
    @GetMapping
    public Page<Device> get(DevicesFilter devicesFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            if (getUser().getCompany().getId() != null){
                devicesFilter.setCompany(getUser().getCompany());
            }
        }

        return deviceRepo.findAll(DevicesSpec.find(devicesFilter), pageable);
    }
}