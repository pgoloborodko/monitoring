package com.landau.core.devices.controllers;

import com.landau.core.devices.entities.DeviceProperties;
import com.landau.core.devices.messages.DeviceSettingsMessages;
import com.landau.core.devices.repos.DevicePropertiesRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * Сохранение разрешений для устройства
 */
@Slf4j
@Data
@Api(tags = "Сохранение разрешений для устройства", description = "Контроллер обеспечивает получение и сохранение данных для устройств")
@RestController("devicePropertiesSave")
@RequestMapping("/devices/properties")
public class DevicePropertiesSave {
    private DevicePropertiesRepo devicePropertiesRepo;
    private DeviceSettingsMessages deviceSettingsMessages;

    @Autowired
    public DevicePropertiesSave(DevicePropertiesRepo devicePropertiesRepo, DeviceSettingsMessages deviceSettingsMessages) {
        this.devicePropertiesRepo = devicePropertiesRepo;
        this.deviceSettingsMessages = deviceSettingsMessages;
    }

    @ApiOperation(value = "Получение детальных данных разрешений для устройства", response = DeviceProperties.class)
    @GetMapping
    public DeviceProperties get(DeviceProperties deviceProperties) {
        return deviceProperties;
    }

    /**
     * Запрос на установку переданных настроек устройства
     * @param deviceProperties настройки устройства для запроса
     * @exception ResponseStatusException INTERNAL_SERVER_ERROR - ошибка на стороне сервера, ошибка отправки сообщения
     * @exception ResponseStatusException BAD_REQUEST - ошибка на стороне клиента, неправильный запрос,
     * возможно, отсутствуют необходимые для работы параметры
     */
    @ApiOperation(value = "Запрос на установку настроек на устройство")
    @PostMapping
    public DeviceProperties setDeviceSettings(@Valid DeviceProperties deviceProperties) {
//        try {
//            deviceSettingsMessages.setDeviceSettings(deviceProperties);
//        } catch (RepublisherException e) {
//            log.error("error set device settings", e);
//            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
//        }

        return deviceProperties;
    }
}
