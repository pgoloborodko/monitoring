package com.landau.core.devices.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.DeviceTelemetry;
import com.landau.core.devices.filters.DevicesTelemetryFilter;
import com.landau.core.devices.repos.DeviceTelemetryRepo;
import com.landau.core.devices.specs.DevicesTelemetrySpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Получение телеметрии с устройств
 */
@Api(tags = "Список телеметрии с устройств", description = "Контроллер обеспечивает получение списка телеметрии с устройств")
@RestController("DevicesTelemetry")
@RequestMapping("/devices/telemetry")
public class DeviceTelemetryController extends Base {
    private DeviceTelemetryRepo deviceTelemetryRepo;

    @Autowired
    public DeviceTelemetryController(DeviceTelemetryRepo deviceTelemetryRepo) {
        this.deviceTelemetryRepo = deviceTelemetryRepo;
    }

    /**
     * Получение телеметрии устройств
     * @param devicesTelemetryFilter поля фильтра
     * @return Page<DeviceTelemetry>
     */
    @ApiOperation(value = "Получение телеметрии устройств")
    @GetMapping
    public Page<DeviceTelemetry> get(DevicesTelemetryFilter devicesTelemetryFilter, Pageable pageable) {

        return deviceTelemetryRepo.findAll(DevicesTelemetrySpec.find(devicesTelemetryFilter), pageable);
    }
}
