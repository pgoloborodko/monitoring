package com.landau.core.devices.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.messages.DeviceLogMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.server.ResponseStatusException;

/**
 * Просмотр списка и получение файла логов регистратора
 */
@Data
@Api(tags = "Логи регистратора", description = "Контроллер обеспечивает просмотр списка лог-файлов и получение лог-файла регистратора")
@RestController("DevicesRequestLog")
@RequestMapping("/devices/log/")
public class DeviceLogController extends Base {
    private DeviceLogMessages deviceLogMessages;

    @Autowired
    public DeviceLogController(DeviceLogMessages deviceLogMessages) {
        this.deviceLogMessages = deviceLogMessages;
    }

    /**
     * Получение списка логов регистратора
     * @param device объект регистратора
     * @return DeferredResult объект с результатом выполнения запроса
     * @throws ResponseStatusException
     */
    @ApiOperation(value = "Получение списка логов регистратора")
    @GetMapping("list")
    @Authority(value = Device.class, required = true)
    public DeferredResult<Object> requestDeviceLogList(Device device) {
        try {
            return deviceLogMessages.getLogList(device);
        } catch (RepublisherException e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
        }
    }

    /**
     * Получение файла логов регистратора
     * @param file название файла
     * @return файл с логом регистратора
     * @throws Exception
     */
    @ApiOperation(value = "Получение файла логов регистратора")
    @GetMapping("file")
    @Authority(value = Device.class, required = true)
    public DeferredResult<Object> requestDeviceLogFile(Device device, @RequestParam("file") String file) {
        try {
            return deviceLogMessages.getLogFile(device, file);
        } catch (RepublisherException e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
        }
    }
}
