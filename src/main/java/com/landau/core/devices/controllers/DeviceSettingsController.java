package com.landau.core.devices.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceSettings;
import com.landau.core.devices.messages.DeviceSettingsMessages;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.devices.repos.DeviceSettingsRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RestController("DeviceSettings")
@RequestMapping("/devices/settings")
@Api(tags = "Настройки устройства", description = "Контроллер служит для взаимодействия с настройками устройства")
public class DeviceSettingsController extends Base {

    @Autowired
    public DeviceSettingsController(DeviceRepo deviceRepo, DeviceSettingsRepo deviceSettingsRepo, DeviceSettingsMessages deviceSettingsMessages){
        this.deviceRepo = deviceRepo;
        this.deviceSettingsMessages = deviceSettingsMessages;
        this.deviceSettingsRepo = deviceSettingsRepo;
    }

    private DeviceRepo deviceRepo;
    private DeviceSettingsMessages deviceSettingsMessages;
    private DeviceSettingsRepo deviceSettingsRepo;

    /**
     * Получение настроек устройства
     * @param device
     * @return
     */
    @GetMapping
    @ApiOperation(value = "Получение настроек устройства", response = DeviceSettings.class)
    public DeviceSettings getDeviceSettings(@RequestParam(name = "id") Device device){
        device = deviceRepo.findById(device.getId()).orElse(null);
        if (device != null){
            DeviceSettings deviceSettings = deviceSettingsRepo.findByDevice(device).orElse(null);
            if (deviceSettings != null){
                return deviceSettings;
            }
            else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "resource not found");
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "device does not exist");
        }
    }

    /**
     * Запрос на установку переданных настроек устройства
     * @param deviceSettings
     * @return
     */
    @PostMapping
    @ApiOperation(value = "Установка настроек на устройство", response = DeviceSettings.class)
    public DeviceSettings setDeviceSettings(DeviceSettings deviceSettings){
        Device device = deviceRepo.findById(deviceSettings.getDevice().getId()).orElse(null);
        if (device != null){
            if (device.getOnline()){
                try {
                    deviceSettingsMessages.setDeviceSettings(deviceSettings);
                } catch (RepublisherException e) {
                    log.error("error set device settings", e);
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
                }
            }
            else {
                deviceSettings.setSended(false);
                deviceSettingsRepo.save(deviceSettings);
                deviceRepo.save(device);
            }

            return deviceSettings;
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "device does not exist");
        }
    }
}
