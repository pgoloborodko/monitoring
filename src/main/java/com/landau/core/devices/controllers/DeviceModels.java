package com.landau.core.devices.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.DeviceModel;
import com.landau.core.devices.repos.DeviceModelRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "Модели устройств", description = "Контроллер может получать данные моделей регистраторов, а также создавать новые")
@RestController("devicesModels")
@RequestMapping("/devices/models")
public class DeviceModels extends Base {
    DeviceModelRepo deviceModelRepo;

    @Autowired
    public DeviceModels(DeviceModelRepo deviceModelRepo) {
        this.deviceModelRepo = deviceModelRepo;
    }

    @Authority(value = DeviceModel.class)
    @ApiOperation(value = "Получение списка моделей устройств", response = DeviceModel.class)
    @GetMapping
    public List<DeviceModel> get() {
        return new ArrayList<>(deviceModelRepo.findAll());
    }


    @ApiOperation(value = "Получение данных модели устройств", response = DeviceModel.class)
    @Authority(value = DeviceModel.class)
    @GetMapping("save")
    public DeviceModel save(DeviceModel deviceModelDTO) {
        return deviceModelDTO;
    }

    @ApiOperation(value = "Сохранение модели устройства", response = DeviceModel.class)
    @Authority(value = DeviceModel.class)
    @PostMapping("save")
    public DeviceModel post(DeviceModel deviceModel, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getAllErrors().toString());
        }

        return deviceModelRepo.save(deviceModel);
    }
}
