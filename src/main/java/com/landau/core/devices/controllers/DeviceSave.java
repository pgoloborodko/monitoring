package com.landau.core.devices.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Изменение и просмотр данных регистратора
 */
@Api(tags = "Изменение и просмотр данных регистратора", description = "Контроллер обеспечивает просмотр и изменение данных регистратора")
@RestController("devicesSave")
@RequestMapping("/devices/save")
public class DeviceSave extends Base {
    /**
     * Получение данных регистратора
     * @param device регистратор
     * @return {@link Device}
     */
    @ApiOperation(value = "Получение детальных данных регистратора", response = Device.class)
    @Authority(value = Device.class, required = true)
    @GetMapping
    public Device get(Device device) {
        return device;
    }

    /**
     * Сохранение данных регистратора
     * @param device регистратор
     * @param result не используется
     * @return {@link Device}
     */
    @ApiOperation(value = "Изменение данных регистратора", response = Device.class)
    @Authority(value = Device.class, required = true)
    @PostMapping
    public Device post(Device device, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getAllErrors().toString());
        }

/*        if(device.getCompany() != null) {
            DeviceConnector deviceConnector = (DeviceConnector) webSocketServer.find(device);

            if(deviceConnector != null) {
                deviceConnector.sendStream();
            }
        }*/

        return deviceRepo.save(device);
    }
}
