package com.landau.core.devices.entities;

import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table
@Entity
@ApiModel(description = "Настройки устройства")
public class DeviceSettings extends AbstractId {
    public enum Resolution{
        SD,
        HD,
        FHD
    }

    public enum StreamInterval{
        sec10,
        sec20,
        sec30
    }

    public enum RotateBitmap{
        degree0,
        degree90,
        degree180,
        degree270
    }

    public enum RecordEncoder{
        CBR,
        VBR
    }

    public enum Duration{
        sec30,
        sec120,
        sec300,
        sec600
    }

    public enum Notification{
        OFF,
        VIBRO,
        VOICE
    }

    public enum DefaultVolume{
        vol30,
        vol50,
        vol80,
        vol90,
        vol100
    }

    @Column
    @ApiModelProperty(value = "Вертикальное отображение")
    private Boolean vertical = true;

    @Column
    @ApiModelProperty(value = "Поворт матрицы")
    private Boolean rotateMatrix = false;

    @Column
    @ApiModelProperty(value = "Параметры стрима: разрешение и частота кадров")
    private Resolution streamResolution = Resolution.HD;

    @Column
    @ApiModelProperty(value = "Длительность стрима по кнопке - 5/15/30/ ручное управление")
    private StreamInterval streamInterval = StreamInterval.sec10;

    @Column
    @ApiModelProperty(value = "Поворот изображения стримера")
    private RotateBitmap rotateBitmapFromStreamer = RotateBitmap.degree180;

    @Column
    @ApiModelProperty(value = "Параметры видеозаписи: разрешение")
    private Resolution recordResolution = Resolution.FHD;

    @Column
    @ApiModelProperty(value = "Битрейт видеозаписи")
    private RecordEncoder recordEncoder= RecordEncoder.CBR;

    @Column
    @ApiModelProperty(value = "Маркер перезаписи")
    private Boolean rewrite = false;

    @Column
    @ApiModelProperty(value = "Поворот изображения записи")
    private RotateBitmap rotateBitmapFromRecorder = RotateBitmap.degree180;

    @Column
    @ApiModelProperty(value = "Автоматическая видеозапись")
    private Boolean record = true;

    @Column
    @ApiModelProperty(value = "Размер видеофрагментов в секундах")
    private Duration duration = Duration.sec30;

    @Column
    @ApiModelProperty(value = "Тип уведомлений")
    private Notification notification = Notification.VOICE;

    @Column
    @ApiModelProperty(value = "Громкость уведомлений - от 0 до 100")
    private DefaultVolume defaultVolume = DefaultVolume.vol80;

    @Column
    @ApiModelProperty(value = "Последняя дата обновления настроек")
    private Date updated;

    @Column
    @ApiModelProperty(value = "url с настройками для работы с CORE")
    private String coreUrl;

    @Column
    @ApiModelProperty(value = "url с настройками для работы с CORE")
    private String terminalUrl;

    @Column
    @ApiModelProperty(value = "url с настройками для подключения к WIFI")
    private String wifiUrl;

    @Column
    @ApiModelProperty(value = "")
    private Boolean based = false;

    @OneToOne
    @ApiModelProperty(value = "")
    private Device device;

    @Column
    @ApiModelProperty(value = "настройки отправлены да/нет")
    private boolean sended;
}
