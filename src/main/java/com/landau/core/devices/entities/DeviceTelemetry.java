package com.landau.core.devices.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.repos.DeviceTelemetryRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Телеметрия устройств
 */
@Entity
@Table(name = "device_telemetry")
@JsonInclude
@Getter
@Setter
@Repo(DeviceTelemetryRepo.class)
@ApiModel(description = "Телеметрия устройств")
public class DeviceTelemetry extends AbstractId {
    /**
     * Тип GSM
     */
    public enum GsmType {
        /**
         * 2G
         */
        TYPE_2G,
        /**
         * 3G
         */
        TYPE_3G,
        /**
         * 4G
         */
        TYPE_4G,
        /**
         * 5G
         */
        TYPE_5G
    }

    @ManyToOne
    @JsonIgnore
    @ApiModelProperty(value = "Утройство")
    private Device device;

    @Column
    @ApiModelProperty(value = "Тип GSM телеметрии утройства")
    private GsmType gsmType;

    @Column
    @ApiModelProperty(value = "Значение сигнала GSM телеметрии утройства")
    private Short gsmSignal;

    @Column
    @ApiModelProperty(value = "Количество лицевых шаблоноа утройства")
    private Integer faceTemplates;

    @Column
    @ApiModelProperty(value = "Пульс носителя утройства")
    private Short watchPulse;

    @Column
    @ApiModelProperty(value = "Общее дисковое пространство")
    private Long allSpace;

    @Column
    @ApiModelProperty(value = "Занятое дисковое пространство")
    private Long holdSpace;

    @Column
    @ApiModelProperty(value = "Свободное дисковое пространство")
    private Long freeSpace;

    @Column
    @ApiModelProperty(value = "Общее дисковое пространство sd-карты")
    private Long allSpaceSd;

    @Column
    @ApiModelProperty(value = "Занятое дисковое пространство sd-карты")
    private Long holdSpaceSd;

    @Column
    @ApiModelProperty(value = "Свободное дисковое пространство sd-карты")
    private Long freeSpaceSd;

    @Column
    @ApiModelProperty(value = "Нагрузка на оперативную память устройства")
    private Byte cpuLoad;

    @Column
    @ApiModelProperty(value = "Нагрузка на видео память устройства")
    private Byte gpuLoad;

    @Column
    @ApiModelProperty(value = "Уровень заряда аккумулятора устройства")
    private Byte watchCharge;

    @Column
    @ApiModelProperty(value = "Уровень заряда аккумулятора часов")
    private Byte deviceCharge;

    @Column
    @ApiModelProperty(value = "Дата телеметрии утройства")
    private Date created;

    @Column
    @ApiModelProperty(value = "Дата создания записи")
    private Date added;

    @Column
    @ApiModelProperty(value = "Подключена внешняя камера да/нет")
    private Boolean isExternalCameraOnline;

    @Column
    @ApiModelProperty(value = "Заряжается да/нет")
    private Boolean isCharging;

    @Column
    @ApiModelProperty(value = "Подключено к Wi-Fi да/нет")
    private Boolean isWifiConnected;

    @Column
    @ApiModelProperty(value = "Подключено к GSM да/нет")
    private Boolean isGsmConnected;
}
