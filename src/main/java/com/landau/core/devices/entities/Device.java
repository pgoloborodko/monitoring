package com.landau.core.devices.entities;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.landau.core.cities.entities.City;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusinessSerial;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.districts.entities.District;
import com.landau.core.locations.entities.Location;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;


/**
 * Устройства
 */
@Table
@Entity
@Getter
@Setter
@EqualsAndHashCode
@Repo(DeviceRepo.class)
@ApiModel(description = "Сущность устройства")
public class Device extends AbstractBusinessSerial {
    /**
     * Тип регистратора
     */
    public enum Type {
        /**
         * Носимый регистратор
         */
        WEAR_REG,
        /**
         * Авторегистратор
         */
        CAR_REG,
        /**
         * Телефон
         */
        PHONE,
        /**
         * Планшет
         */
        TABLET,
        /**
         * Камера
         */
        FIXED_CAMERA,
        /**
         * Биометрический компекс
         */
        BIOMETRIC_COMPLEX,
        PASSING
    }

    @Column
    @ApiModelProperty(value = "Название устройства")
    private String name;

    @ManyToOne
    @ApiModelProperty(value = "Модель устройства")
    private DeviceModel model;

    @Column
    @ApiModelProperty(value = "Версия прошивки")
    private String version;

    @Column
    @ApiModelProperty(value = "Пульс носителя")
    private Integer pulse;

    @Column
    @ApiModelProperty(value = "Уровень заряда устройства")
    private Integer charge;

    @ManyToOne
    @ApiModelProperty(value = "Город устройства", example = "0")
    private City city;

    @ManyToOne
    @ApiModelProperty(value = "Район устройства")
    private District district;

    @Column
    @ApiModelProperty(value = "Серийный номер SIM устройства")
    private String simSerial;

    @Column
    @ApiModelProperty(value = "SIM-номер устройства")
    private String simNumber;

    @Column
    @ApiModelProperty(value = "Тип устройства")
    private Type type;

    @Column
    @ApiModelProperty(value = "Количество устройств")
    private Integer numberOfDevices;

    @ManyToOne
    @ApiModelProperty(value = "Последняя телеметрия устройства")
    private DeviceTelemetry telemetry;

    @ManyToOne
    @ApiModelProperty(value = "Последняя локация устройства")
    private Location location;

    @ManyToOne
    @ApiModelProperty(value = "Пользователь устройства")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @Column
    @ApiModelProperty(value = "Дата добавления устройства", example = "2019-11-18 11:24:35.500")
    private Date created;

    @ManyToOne
    @ApiModelProperty(value = "Настройки устройства")
    private DeviceSettings settings;
}
