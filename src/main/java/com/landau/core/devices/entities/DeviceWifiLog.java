package com.landau.core.devices.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.repos.DeviceWifiLogRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Wifi лог
 */
@Table
@Entity
@JsonInclude
@Repo(DeviceWifiLogRepo.class)
@ApiModel(description = "Wifi лог")
public class DeviceWifiLog extends AbstractId {
    @ManyToOne
    @ApiModelProperty(value = "Устройство WiFi лога")
    private Device device;

    @Column
    @ApiModelProperty(value = "Дата добавления WiFi лога")
    private Date added;

    @Column
    @ApiModelProperty(value = "Mac-адрес устройства")
    private String mac;

    @Column
    @ApiModelProperty(value = "SSID Сети")
    private String ssid;

    @Column
    @ApiModelProperty(value = "Мощность сигнала")
    private Integer signal;

    @Column
    @ApiModelProperty(value = "Частота")
    private Integer frequency;

    @Column
    @ApiModelProperty(value = "Скорость соединения")
    private Integer linkSpeed;

    /**
     * Mac-Адрес устройства
     * @return
     */
    public String getMac() {
        return mac;
    }

    /**
     * Дата добавления
     * @return
     */
    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * SSID устройства
     * @return
     */
    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    /**
     * Уровень сигнала
     * @return
     */
    public Integer getSignal() {
        return signal;
    }

    public void setSignal(Integer signal) {
        this.signal = signal;
    }

    /**
     * Частота сигнала
     * @return
     */
    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    /**
     * Скорость соединения
     * @return
     */
    public Integer getLinkSpeed() {
        return linkSpeed;
    }

    public void setLinkSpeed(Integer linkSpeed) {
        this.linkSpeed = linkSpeed;
    }

    /**
     * Сущность устройства
     * @return
     */
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
