package com.landau.core.devices.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.repos.DevicePropertiesRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Настройки устройства
 */
@Data
@Entity
@JsonInclude
@Repo(DevicePropertiesRepo.class)
@ApiModel(description = "Настройки устройства")
public class DeviceProperties extends AbstractId {

    /**
     * Тип разрешения стрима
     */
    public enum StreamResolutionType {
        FULL_HD,
        HD,
        SD,
        VGA,
        ULTRA_HD;
    }

    /**
     * Тип разрешения записи
     */
    public enum RecordResolution{
        VGA,
        SD,
        HD,
        FULL_HD,
        ULTRA_HD
    }

    /**
     * Тип ИК режима
     */
    public enum IRSensor{
        AUTO,
        ON,
        OFF;
    }

    /**
     * Тип уведомления
     */
    public enum NotificationType{
        OFF,
        VIBRO,
        VIBRO_AND_VOICE;
    }

    @Column
    @ApiModelProperty("Тип разрешения записи")
    private RecordResolution recordResolution;

    @Column
    @ApiModelProperty("Тип уведомления")
    private NotificationType notificationType;

    @ManyToOne
    @ApiModelProperty(value = "Устройство")
    private Device device;

    @Column
    @ApiModelProperty(value = "Перезапись да/нет")
    private Boolean rewrite;

    @Column
    @ApiModelProperty(value = "Тип ИК режима")
    private IRSensor irSensor;

    @Column
    @ApiModelProperty(value = "Динамический стрим да/нет")
    private Boolean dynamicStream;

    @Column
    @ApiModelProperty(value = "Предзапись в секундах")
    private Integer preRecord;

    @Column
    @ApiModelProperty(value = "Постзапись в секундах")
    private Integer postRecord;

    @Column
    @ApiModelProperty(value = "Голосовые уведомления да/нет")
    private Boolean voiceNotification;

    @Column
    @ApiModelProperty(value = "Задержка автовыключения")
    private Integer autoPowerOff;

    @Column
    @ApiModelProperty(value = "Тип видео-разрешения стрима")
    private StreamResolutionType streamResolutionType;

    @Column
    @ApiModelProperty(value = "Дата создания")
    private Date created;

    @Column
    @ApiModelProperty(value = "Оператор устройства")
    private User operator;
}
