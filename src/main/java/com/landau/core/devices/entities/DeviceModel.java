package com.landau.core.devices.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.devices.repos.DeviceModelRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Модель устройства
 */
@Getter
@Setter
@Table
@Entity
@JsonInclude
@Repo(DeviceModelRepo.class)
@ApiModel(description = "Модель устройства")
public class DeviceModel extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Производитель модели")
    private String manufacturer;

    @Column
    @ApiModelProperty(value = "Название модели")
    private String model;

    @Column
    @ApiModelProperty(value = "Прошивка модели")
    private String firmware;

    @Column
    @ApiModelProperty(value = "Емкость модели")
    private Integer batteryCapacity;
}
