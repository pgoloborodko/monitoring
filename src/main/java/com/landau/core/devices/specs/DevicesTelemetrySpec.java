package com.landau.core.devices.specs;

import com.landau.core.devices.entities.DeviceTelemetry;
import com.landau.core.devices.filters.DevicesTelemetryFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class DevicesTelemetrySpec {
    public static Specification<DeviceTelemetry> find(DevicesTelemetryFilter devicesTelemetryFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (devicesTelemetryFilter.getDevice() != null) {
                predicates.add(cb.equal(root.get("device"), devicesTelemetryFilter.getDevice()));
            }

//            if (devicesTelemetryFilter.getTelemetryDate() != null) {
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(devicesTelemetryFilter.getTelemetryDate());
//                calendar.set(Calendar.HOUR_OF_DAY, 0);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//
//                Calendar calendarEnd = (Calendar) calendar.clone();
//                calendarEnd.setTime(devicesTelemetryFilter.getTelemetryDate());
//                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
//                calendarEnd.set(Calendar.MINUTE, 59);
//                calendarEnd.set(Calendar.SECOND, 59);
//
//                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
//                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
//            }


            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
