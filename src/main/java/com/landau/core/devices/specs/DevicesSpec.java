package com.landau.core.devices.specs;


import com.landau.core.devices.entities.Device;
import com.landau.core.devices.filters.DevicesFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class DevicesSpec {
    public static Specification<Device> find(DevicesFilter devicesFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (devicesFilter.getSerial() != null && !devicesFilter.getSerial().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("serial")), "%" + devicesFilter.getSerial().toUpperCase() +"%"));
            }

            if (devicesFilter.getName() != null && !devicesFilter.getName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("name")), "%" + devicesFilter.getName().toUpperCase() +"%"));
            }

            if (devicesFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("city"), devicesFilter.getCity()));
            }

            if (devicesFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("district"), devicesFilter.getDistrict()));
            }

            if (devicesFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), devicesFilter.getCompany()));
            }

            if (devicesFilter.getId() != null) {
                predicates.add(cb.equal(root.get("id"), devicesFilter.getId()));
            }

            if (devicesFilter.isExistsUser()) {
                predicates.add(cb.isNotNull(root.get("user")));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
