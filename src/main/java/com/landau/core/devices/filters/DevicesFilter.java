package com.landau.core.devices.filters;

import com.landau.core.devices.entities.Device;

public class DevicesFilter extends Device {
    private boolean existsUser = false;

    public boolean isExistsUser() {
        return existsUser;
    }

    public void setExistsUser(boolean existsUser) {
        this.existsUser = existsUser;
    }
}
