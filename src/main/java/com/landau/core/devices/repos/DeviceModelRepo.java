package com.landau.core.devices.repos;

import com.landau.core.devices.entities.DeviceModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeviceModelRepo extends JpaRepository<DeviceModel, Long> {
    Optional<DeviceModel> findFirstByManufacturerAndModelAndFirmware(String manufacturer, String model, String firmware);
}
