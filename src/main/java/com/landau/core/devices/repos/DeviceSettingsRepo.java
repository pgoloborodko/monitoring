package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceSettings;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeviceSettingsRepo extends JpaRepository<DeviceSettings, Long> {
    Optional<DeviceSettings> findByDevice(Device device);
    Optional<DeviceSettings> findByDeviceAndSendedIsFalse(Device device);
}
