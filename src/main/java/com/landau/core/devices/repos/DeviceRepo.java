package com.landau.core.devices.repos;

import com.landau.core.common.repos.JpaSerialRepository;
import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface DeviceRepo extends JpaSerialRepository<Device, Long>, JpaSpecificationExecutor<Device> {
    List<Device> findByOnline(Boolean online);
    @Transactional
    void deleteAllByName(String name);

    Page<Device> findAllByCompany(Pageable pageable, Company company);
    List<Device> findAllByCompanyAndOnline(Company company, Boolean online);
    List<Device> findAllByCompany(Company company);
    Long countAllByOnline(Boolean online);

    Optional<Device> findByUser(User user);

    @Modifying
    @Transactional
    @Query("UPDATE Device r SET online = :online")
    Integer updateStatus(@Param("online") Boolean online);

    //    @Query(value = "select device_id from (" +
//            "select min(st_distance(a.point, st_makepoint(50, 50))) min " +
//            "FROM (location_view a " +
//            "JOIN (SELECT location_view.device_id, max(location_view.added) added " +
//            "FROM location_view " +
//            "GROUP BY location_view.device_id) b ON (((a.device_id = b.device_id) AND (a.added = b.added)))) " +
//            ") d inner join ( " +
//            "SELECT a.device_id, st_distance(point, st_makepoint(50, 50)) dist " +
//            "FROM location_view a " +
//            "INNER JOIN ( " +
//            "SELECT device_id, MAX(added) added " +
//            "FROM location_view " +
//            "GROUP BY device_id " +
//            ") b ON a.device_id = b.device_id AND a.added = b.added " +
//            ") c on d.min = c.dist ", nativeQuery = true)
    @Query(value = "select device_id from ( " +
            "select min(st_distance(a.point, st_makepoint(50, 50))) min from location_view a " +
            "inner join ( " +
            "  select device_id, max(added) added from location_view group by device_id " +
            ") b on a.device_id = b.device_id and a.added = b.added " +
            ") d inner join ( " +
            "  select a.device_id, st_distance(point, st_makepoint(50, 50)) dist from location_view a " +
            "  inner join ( " +
            "    select device_id, max(added) added from location_view group by device_id " +
            "  ) b on a.device_id = b.device_id and a.added = b.added " +
            ") c on d.min = c.dist", nativeQuery = true)
    Device findNearest();

}
