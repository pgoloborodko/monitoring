package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceTelemetry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface DeviceTelemetryRepo extends JpaRepository<DeviceTelemetry, Long>, JpaSpecificationExecutor<DeviceTelemetry> {
    DeviceTelemetry findFirstByDeviceOrderByIdDesc(Device device);
    DeviceTelemetry findSecondByDeviceOrderByIdDesc(Device device);
    List<DeviceTelemetry> findFirst10ByIsChargingAndDeviceOrderByIdDesc(Boolean charging, Device device);

    @Transactional
    void deleteAllByDeviceId(Long deviceId);
    @Transactional
    void deleteAllByCreatedAndDevice(Date date, Device device);
    //List<DeviceTelemetry> findFirst10ByChargingAndDeviceOrderByIdDesc(Boolean charging, Device device);

    Optional<DeviceTelemetry> findFirstByDeviceAndCreatedBetweenOrderByIdAsc(Device device, Date from, Date to);

    /**
     * метод для тестов
     * @param device
     * @return
     */
    Optional<DeviceTelemetry> findByDevice(Device device);
}
