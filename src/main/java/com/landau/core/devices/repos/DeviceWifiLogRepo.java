package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceWifiLog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface DeviceWifiLogRepo extends JpaRepository<DeviceWifiLog, Long> {

    @Query("SELECT d from DeviceWifiLog d where d.added >= :from AND d.added <= :to AND device = :device ")
    List<DeviceWifiLog> findByAddedDateBetweenAndDevice(Pageable pageable, @Param("from") Date start, @Param("to") Date end, @Param("device") Device device);


    List<DeviceWifiLog> findAllByDeviceAndAddedBetween(Pageable pageable, @Param("device") Device device, @Param("from") Date start,
                                                       @Param("to") Date end);

    DeviceWifiLog findFirstByDeviceOrderByIdDesc(Device device);

    /**
     *  Метод для тестов
     * @param device
     * @return
     */
    Optional<DeviceWifiLog> findByDevice(Device device);
}
