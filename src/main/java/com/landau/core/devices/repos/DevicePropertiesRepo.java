package com.landau.core.devices.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceProperties;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DevicePropertiesRepo extends JpaRepository<DeviceProperties, Long> {
    Optional<DeviceProperties> findByDevice(Device device);
}
