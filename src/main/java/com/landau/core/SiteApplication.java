package com.landau.core;

import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.terminals.repos.TerminalRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@Slf4j
@SpringBootApplication(scanBasePackages = {"com.landau.core.*"})
public class SiteApplication {
    public static String DIR_AVATARS;
    public static String DIR_APKS;

    @Autowired
    private Environment env;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private BiometricRepo biometricRepo;

    @Value("${app.dir.apks}")
    private Resource apksDir;

    @Value("${app.dir.avatars}")
    private Resource avatarsDir;

    @Value("${app.version}")
    private String version;

    public static void main(String[] args) {
        SpringApplication.run(SiteApplication.class, args);
    }

    @PostConstruct
    public void init() throws IOException {
        DIR_AVATARS = avatarsDir.getFile().getAbsolutePath() + File.separator;
        DIR_APKS = apksDir.getFile().getAbsolutePath() + File.separator;

        terminalRepo.updateStatus(false);
        deviceRepo.updateStatus(false);
        biometricRepo.updateStatus(false);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    PageableHandlerMethodArgumentResolverCustomizer pageableResolverCustomizer() {
        return pageableResolver -> pageableResolver.setOneIndexedParameters(true);
    }

    @Bean
    ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);

        return modelMapper;
    }

    @Bean
    public ServletWebServerFactory servletContainer() {
        String serverPortHttp = env.getProperty("server.port.http");
        if (serverPortHttp == null) return new TomcatServletWebServerFactory();
        TomcatServletWebServerFactory tomcat =
                new TomcatServletWebServerFactory() {

                    @Override
                    protected void postProcessContext(Context context) {
                        SecurityConstraint securityConstraint = new SecurityConstraint();
                        securityConstraint.setUserConstraint("CONFIDENTIAL");
                        SecurityCollection collection = new SecurityCollection();
                        collection.addPattern("/*");
                        securityConstraint.addCollection(collection);
                        context.addConstraint(securityConstraint);
                    }
                };
        tomcat.addAdditionalTomcatConnectors(createHttpConnector(serverPortHttp));
        return tomcat;
    }

    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Value("${server.port}")
    private int serverPortHttps;

    private Connector createHttpConnector(String serverPortHttp) {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        connector.setSecure(false);
        connector.setPort(Integer.parseInt(serverPortHttp));
        connector.setRedirectPort(serverPortHttps);
        return connector;
    }
}

