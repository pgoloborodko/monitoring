package com.landau.core.stats.entity;

import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.stats.repos.TerminalSyncStatsRepo;
import com.landau.core.terminals.entities.Terminal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Repo(TerminalSyncStatsRepo.class)
@Entity
@Table
@ApiModel(description = "Статистика терминала по синхронизации")
public class TerminalSyncStats extends AbstractId {
    @Column
    @ApiModelProperty(value = "Количество синхронизированных файлов")
    private Integer syncFiles;

    @Column
    @ApiModelProperty(value = "Общий объем синхронизированных файлов в мб")
    private Long syncFilesSize;

    @Column
    @ApiModelProperty(value = "Дата сбора статистики терминала")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @ManyToOne
    @ApiModelProperty(value = "сущность терминала")
    private Terminal terminal;
}
