package com.landau.core.stats.entity;

import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.stats.repos.TerminalDevicesStatsRepo;
import com.landau.core.terminals.entities.Terminal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Repo(TerminalDevicesStatsRepo.class)
@Data
@Entity
@Table
@ApiModel(description = "Статистика терминала по устройствам")
public class TerminalDevicesStats extends AbstractId {
    @Column
    @ApiModelProperty(value = "Количество отключений устройств")
    private Byte extracted;

    @Column
    @ApiModelProperty(value = "Количество подключений устройств")
    private Byte inserted;

    @Column
    @ApiModelProperty(value = "Дата сбора статистики терминала")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @ManyToOne
    @ApiModelProperty(value = "сущность терминала")
    private Terminal terminal;
}
