package com.landau.core.stats.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.stats.repos.StatsStreamServerRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * Статистика стрим-сервера
 */
@Data
@Entity
@JsonInclude
@Repo(StatsStreamServerRepo.class)
@ApiModel(description = "Статистика стрим-сервера")
public class StatsStreamServer extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Название стрим-сервера")
    private String name;

    @Column
    @ApiModelProperty(value = "начало сбора статистики стрим-сервера")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startStatistic;

    @Column
    @ApiModelProperty(value = "Нагрузка на оперативную память стрим-сервера")
    private Integer cpuLoad;

    @Column
    @ApiModelProperty(value = "Общее дисковое пространство стрим-сервера")
    private Long allMemory;

    @Column
    @ApiModelProperty(value = "Занятое дисковое пространство стрим-сервера")
    private Long holdMemory;

    @Column
    @ApiModelProperty(value = "Количество стримов")
    private Long streamCount;
}
