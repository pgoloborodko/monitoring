package com.landau.core.stats.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import com.landau.core.stats.repos.StatsDeviceOnlineUptimeRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Статистика онлайна устройств
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(StatsDeviceOnlineUptimeRepo.class)
@ApiModel(description = "Статистика времени онлайна устройств")
public class StatsDeviceOnlineUptime extends AbstractId {
    @ManyToOne
    @ApiModelProperty(value = "Устройство статистики")
    private Device device;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "Начало сессии")
    private Date startSession;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "Конец сессии")
    private Date endSession;
}
