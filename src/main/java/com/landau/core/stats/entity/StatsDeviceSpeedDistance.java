package com.landau.core.stats.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import com.landau.core.stats.repos.StatsDeviceSpeedDistanceRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Статистика изменения скорости
 */
@Entity
@Table
@JsonInclude
@Setter
@Getter
@Repo(StatsDeviceSpeedDistanceRepo.class)
@ApiModel(description = "Статистика изменения скорости")
public class StatsDeviceSpeedDistance extends AbstractId {
    @ManyToOne
    @ApiModelProperty(value = "Устройство статистики")
    private Device device;

    @Column
    @ApiModelProperty(value = "Скорость передвижения устройства")
    private Short speed;

    @Column
    @ApiModelProperty(value = "Расстояние пройденное устройством")
    private Float distance;

    @Column(name="from_date")
    @ApiModelProperty(value = "Начало сбора статистики изменения скорости")
    private Date from;

    @Column(name="to_date")
    @ApiModelProperty(value = "Конец сбора статистики изменения скорости")
    private Date to;
}
