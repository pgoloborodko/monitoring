package com.landau.core.stats.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.stats.repos.TerminalStatsRepo;
import com.landau.core.terminals.entities.Terminal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Статистика терминала
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(TerminalStatsRepo.class)
@ApiModel(description = "Статистика терминала")
public class TerminalStats extends AbstractBusiness {
    @Column
    private Date date;

    @Column
    private Long syncFilesCount;

    @Column
    private Long syncFilesSize;

    @Column
    @ApiModelProperty(value = "Количество синхронизированных файлов в хранилище")
    private Long syncFilesInStorage;

    @Column
    private Long deviceExtractCount;

    @Column
    private Long deviceInsertCount;

    @ManyToOne
    private Terminal terminal;
}
