package com.landau.core.stats.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import com.landau.core.stats.repos.StatsDeviceBatteryRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Статистика батареи
 */
@Entity
@Table
@JsonInclude
@Getter
@Setter
@Repo(StatsDeviceBatteryRepo.class)
@ApiModel(description = "Статистика батареи")
public class StatsDeviceBattery extends AbstractId {
    /**
     * Статус устройства в статистике
     */
    public enum Action {
        /**
         * Заряжается
         */
        CHARGE,
        /**
         * Разряжается
         */
        DISCHARGE
    }

    @ManyToOne
    @ApiModelProperty(value = "Устройство")
    private Device device;

    @Column(name="to_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "Конец сбора статистики батареи")
    private Date to;

    @Column(name="from_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "Начало сбора статистики батареи")
    private Date from;

    @Column
    @ApiModelProperty(value = "Текущий заряд батареи")
    private Byte currentAkbCharge;

    @Column
    @ApiModelProperty(value = "Процент заряда")
    private Byte percent;


    /**
     * Заряд в процентах
     * @return
     */
    public Integer getCapacityFromPercent() {
        if(device.getModel() != null && getPercent() != null && device.getModel().getBatteryCapacity() != null && device.getModel().getBatteryCapacity() != 0) {
            return device.getModel().getBatteryCapacity() * getPercent() / 100;
        }

        return null;
    }

    /**
     * Оставшееся время до полного заряда в секундах
     * @return
     */
    public Integer getChargeTimeLeftInSeconds() {
        if(getPercent() != null && getPercent() != 0) {
            return ((100 - getCurrentAkbCharge()) / getPercent()) * 3600;
        }

        return null;
    }

    /**
     * Оставшееся время до полного разряда в секундах
     * @return
     */
    public Integer getDischargeTimeLeftInSeconds() {
        if(getPercent() != null && getPercent() != 0) {
            return (getCurrentAkbCharge() / getPercent()) * 3600;
        }

        return null;
    }
}
