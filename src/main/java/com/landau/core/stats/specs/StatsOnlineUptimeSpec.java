package com.landau.core.stats.specs;

import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.filters.StatsDeviceOnlineUptimeFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.*;

public class StatsOnlineUptimeSpec {
    public static Specification<StatsDeviceOnlineUptime> find(StatsDeviceOnlineUptimeFilter statsOnlineUptimeFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            // date start
            Date dateStart = Optional.of(statsOnlineUptimeFilter.getStartSession()).orElse(new Date());
            Date dateEnd = Optional.of(statsOnlineUptimeFilter.getEndSession()).orElse(new Date());

            if (dateStart.getTime() > dateEnd.getTime()) {
                dateEnd = new Date(dateStart.getTime());
            }

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(dateStart);
            startCal.set(Calendar.HOUR_OF_DAY, 0);
            startCal.set(Calendar.MINUTE, 0);
            startCal.set(Calendar.SECOND, 0);
            startCal.set(Calendar.MILLISECOND, 0);
            final Date startDate = startCal.getTime();

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(dateEnd);
            endCal.set(Calendar.HOUR_OF_DAY, 23);
            endCal.set(Calendar.MINUTE, 59);
            endCal.set(Calendar.SECOND, 59);
            endCal.set(Calendar.MILLISECOND, 0);
            final Date endDate = endCal.getTime();

            predicates.add(cb.lessThan(root.get("startSession"), endDate));
            predicates.add(cb.greaterThanOrEqualTo(root.get("endSession"), startDate));

            // company
            if (statsOnlineUptimeFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("device").get("company"), statsOnlineUptimeFilter.getCompany()));
            }

            if (statsOnlineUptimeFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("device").get("city"), statsOnlineUptimeFilter.getCity()));
            }

            if (statsOnlineUptimeFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("device").get("district"), statsOnlineUptimeFilter.getDistrict()));
            }

            query.orderBy(cb.asc(root.get("startSession")));

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
