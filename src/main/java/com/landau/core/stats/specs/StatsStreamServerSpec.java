package com.landau.core.stats.specs;

import com.landau.core.stats.filters.StatsStreamServerFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class StatsStreamServerSpec {
    public static Specification<StatsStreamServerFilter> find(StatsStreamServerFilter statsStreamServerFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (statsStreamServerFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), statsStreamServerFilter.getCompany()));
            }

            if (statsStreamServerFilter.getName() != null){
                predicates.add(criteriaBuilder.equal(root.get("name"), statsStreamServerFilter.getName()));

            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
