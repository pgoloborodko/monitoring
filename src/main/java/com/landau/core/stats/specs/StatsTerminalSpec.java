package com.landau.core.stats.specs;

import com.landau.core.stats.filters.StatsTerminalFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class StatsTerminalSpec {public static Specification<StatsTerminalFilter> find(StatsTerminalFilter statsTerminalFilter) {
    return (root, query, cb) -> {
        final Collection<Predicate> predicates = new ArrayList<>();

        // date start
        Date dateStart;

        if (statsTerminalFilter.getDate() != null) {
            dateStart = statsTerminalFilter.getDate();
        }
        else {
            dateStart = new Date();
        }

        dateStart.setHours(0);
        dateStart.setMinutes(0);
        dateStart.setSeconds(0);

        predicates.add(cb.greaterThanOrEqualTo(root.get("date"), dateStart));

        // date end

        Date dateEnd = new Date();
        dateEnd.setHours(23);
        dateEnd.setMinutes(59);
        dateEnd.setSeconds(59);
        predicates.add(cb.lessThanOrEqualTo(root.get("date"), dateEnd));

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    };
}

}
