package com.landau.core.stats.specs;

import com.landau.core.stats.filters.StatsDeviceBatteryFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class StatsBatterySpec {
    public static Specification<StatsDeviceBatteryFilter> find(StatsDeviceBatteryFilter statsBatteryFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (statsBatteryFilter.getDevice() != null) {
                predicates.add(cb.equal(root.get("device"), statsBatteryFilter.getDevice()));
            }

            if (statsBatteryFilter.getCompany() != null){
                predicates.add(cb.equal(root.get("device").get("company"), statsBatteryFilter.getCompany()));
            }

            Date dateFrom = statsBatteryFilter.getFrom();
            dateFrom.setHours(0);
            dateFrom.setMinutes(0);
            dateFrom.setSeconds(0);

            Date dateTo = new Date(dateFrom.getTime() + 24 * 60 * 60 * 1000 - 1000);

            statsBatteryFilter.setTo(dateTo);

            predicates.add(cb.greaterThanOrEqualTo(root.get("from"), dateFrom));
            predicates.add(cb.lessThanOrEqualTo(root.get("to"), dateTo));

            //if (statsBatteryFilter.getId() != null) {
            //    predicates.add(cb.equal(root.get("id"), statsBatteryFilter.getId()));
            //}

            //if (statsBatteryFilter.getAction() != null){
            //    predicates.add(cb.equal(root.get("action"), statsBatteryFilter.getAction()));
            //}

            query.orderBy(cb.asc(root.get("from")));

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
