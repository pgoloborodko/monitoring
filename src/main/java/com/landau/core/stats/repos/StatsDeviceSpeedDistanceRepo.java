package com.landau.core.stats.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceSpeedDistance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StatsDeviceSpeedDistanceRepo extends JpaRepository<StatsDeviceSpeedDistance, Long> {
    Optional<StatsDeviceSpeedDistance> findTopByDeviceOrderByIdDesc(Device device);


    /**
     *  Метод для тестов
     * @param serial
     * @return
     */
    Optional<StatsDeviceSpeedDistance> findByDevice_Serial(String serial);
}