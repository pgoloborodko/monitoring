package com.landau.core.stats.repos;

import com.landau.core.stats.entity.TerminalDevicesStats;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TerminalDevicesStatsRepo extends JpaRepository<TerminalDevicesStats, Long> {
    /**
     * Метод для тестов
     * @param serial
     * @return
     */
    Optional<TerminalDevicesStats> findByTerminal_Serial(String serial);
}
