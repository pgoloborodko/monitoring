package com.landau.core.stats.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.filters.StatsDeviceOnlineUptimeFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.Optional;

public interface StatsDeviceOnlineUptimeRepo extends JpaRepository<StatsDeviceOnlineUptime, Long>, JpaSpecificationExecutor<StatsDeviceOnlineUptime> {
    StatsDeviceOnlineUptime findFirstByDeviceAndEndSessionOrderByIdDesc(Device device, Date date);

    StatsDeviceOnlineUptime findFirstByDeviceOrderByIdDesc(Device device);
    //List<StatsOnlineUptime> findAllByFilter(BiometricFilter filter);

    StatsDeviceOnlineUptime findFirstByDeviceAndEndSessionNull(Device device);


    /**
     *  Метод для тестов
     * @param serial
     * @return
     */
    Optional<StatsDeviceOnlineUptime> findByDevice_Serial(String serial);
}
