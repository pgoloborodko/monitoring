package com.landau.core.stats.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.stats.entity.TerminalStats;
import com.landau.core.stats.filters.StatsTerminalFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.Optional;

public interface TerminalStatsRepo extends JpaRepository<TerminalStats, Long>, JpaSpecificationExecutor<StatsTerminalFilter> {
    //Optional<TerminalStats> findTopByDateAndTerminal(Date date, Terminal terminal);
    Optional<TerminalStats> findTopByDateBetweenAndCompany(Date timeStart, Date timeEnd, Company company);
    Optional<TerminalStats> findByDateBetweenAndCompany(Date timeStart, Date timeEnd, Company company);

    /**
     * Метод для тестов
     * @param serial
     * @return
     */
    Optional<TerminalStats> findByTerminal_Serial(String serial);
}
