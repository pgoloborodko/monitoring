package com.landau.core.stats.repos;

import com.landau.core.stats.entity.TerminalSyncStats;
import com.landau.core.terminals.entities.Terminal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TerminalSyncStatsRepo extends JpaRepository<TerminalSyncStats, Long> {
    /**
     * Метод для тестов
     * @param terminal
     * @return
     */
    Optional<TerminalSyncStats> findByTerminal(Terminal terminal);
}
