package com.landau.core.stats.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceBattery;
import com.landau.core.stats.filters.StatsDeviceBatteryFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface StatsDeviceBatteryRepo extends JpaRepository<StatsDeviceBattery, Long>, JpaSpecificationExecutor<StatsDeviceBatteryFilter> {
   Optional<StatsDeviceBattery> findTopByDeviceOrderByIdDesc(Device device);

   List<StatsDeviceBattery> findAllByDeviceAndFromGreaterThanEqualAndToLessThanEqualOrderByIdAsc(Device device, Date from, Date to);


    /**
     *  Метод для тестов
     * @param serial
     * @return
     */
   Optional<StatsDeviceBattery> findByDevice_Serial(String serial);
}
