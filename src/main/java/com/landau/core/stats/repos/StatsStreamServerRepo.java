package com.landau.core.stats.repos;

import com.landau.core.stats.entity.StatsStreamServer;
import com.landau.core.stats.filters.StatsStreamServerFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface StatsStreamServerRepo extends JpaRepository<StatsStreamServer, Long>,
        JpaSpecificationExecutor<StatsStreamServerFilter> {

    Optional<StatsStreamServer> findByName(String name);
}
