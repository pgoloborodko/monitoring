package com.landau.core.stats.services;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.devices.repos.DeviceTelemetryRepo;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.stats.repos.StatsDeviceBatteryRepo;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Timer;
import java.util.TimerTask;

//@Service
public class TelemetryAnalysisService extends TimerTask {
    @Autowired
    DeviceTelemetryRepo deviceTelemetryRepo;

    @Autowired
    LocationRepo locationRepo;

//    @Autowired
//    WebSocketServer webSocketServer;

    @Autowired
    StatsDeviceBatteryRepo statsBatteryRepo;

    @Autowired
    DeviceRepo deviceRepo;

    private Timer timer = new Timer();

    @PostConstruct
    private void init() {
        timer.schedule(this, 0, 60000); // 1 min
    }

    @Override
    public void run() {

//        List<DeviceConnector> deviceConnectorList = webSocketServer.filterRecipients(DeviceConnector.class);
//
//        for (DeviceConnector deviceConnector : deviceConnectorList) {
//            if (deviceConnector.getDeviceTelemetry() != null){
//                DeviceTelemetry lastTelemetry = deviceConnector.getDeviceTelemetry();
//
//               Optional<StatsBattery> lastStatsBattery = statsBatteryRepo.findTopByDeviceOrderByIdDesc(deviceConnector.getDevice());
//
//               if (lastStatsBattery.isPresent()) {
//                   chargeAkbAnalise(lastTelemetry, deviceConnector.getDevice(), lastStatsBattery.get());
//               }
//               else {
//                   StatsBattery statsBattery = new StatsBattery();
//                   statsBattery.setDevice(deviceConnector.getDevice());
//                   statsBattery.setTo(new Date());
//                   statsBattery.setPercent(0);
//                   statsBatteryRepo.save(statsBattery);
//               }
//            }
//        }
    }

//    public void chargeAkbAnalise(DeviceTelemetry lastDeviceTelemetry, Device device, StatsBattery lastStatsBattery){
//        long time = lastDeviceTelemetry.getTelemetryDate().getTime() - lastStatsBattery.getTo().getTime();
//
//        if (time > 6000) {
//            Date dateTo = new Date();
//            Date dateFrom = new Date(dateTo.getTime() - 6000);
//            Optional<DeviceTelemetry> firstDeviceTelemetry = deviceTelemetryRepo.findFirstByDeviceAndTelemetryDateBetweenOrderByIdAsc(device, dateFrom, dateTo);
//            if (firstDeviceTelemetry.isPresent()){
//                int akbCharge = lastDeviceTelemetry.getAkbDeviceCharge() - firstDeviceTelemetry.get().getAkbDeviceCharge();
//
//                StatsBattery statsBattery = new StatsBattery();
//                statsBattery.setDevice(device);
//                statsBattery.setCurrentAkbCharge(lastDeviceTelemetry.getAkbDeviceCharge());
//                statsBattery.setTo(lastDeviceTelemetry.getTelemetryDate());
//                statsBattery.setFrom(firstDeviceTelemetry.get().getTelemetryDate());
//
//                if (akbCharge >= 0){
//                    statsBattery.setAction(StatsBattery.Action.CHARGE);
//                    statsBattery.setPercent(akbCharge);
//                }
//                else {
//                    statsBattery.setAction(StatsBattery.Action.DISCHARGE);
//                    statsBattery.setPercent(Math.abs(akbCharge));
//                }
//
//                statsBatteryRepo.save(statsBattery);
//            }
//        }
//
//    }

//    public String chargeDeviceAnalise(Device device) {
//        List<DeviceTelemetry> chargingDeicesTelemetries = deviceTelemetryRepo.findFirst10ByIsChargingAndDeviceOrderByIdDesc(true, device);
//
//        long time = (chargingDeicesTelemetries.get(0).getTelemetryDate().getTime() -
//                chargingDeicesTelemetries.get(chargingDeicesTelemetries.size() - 1).getTelemetryDate().getTime()) / 60000;
//
//        int charge = chargingDeicesTelemetries.get(0).getAkbDeviceCharge()
//                - chargingDeicesTelemetries.get(chargingDeicesTelemetries.size() - 1).getAkbDeviceCharge();
//
//        return "Устройство заряжается: " + charge/time + "/мин";
//    }
//
//    public String dischargeDeviceAnalise(Device device){
//        List<DeviceTelemetry> dischargingDevicesTelemetries = deviceTelemetryRepo.findFirst10ByIsChargingAndDeviceOrderByIdDesc(false, device);
//
//        long time = (dischargingDevicesTelemetries.get(0).getTelemetryDate().getTime() -
//                dischargingDevicesTelemetries.get(dischargingDevicesTelemetries.size() - 1).getTelemetryDate().getTime()) / 60000;
//
//        int charge = 100 - dischargingDevicesTelemetries.get(0).getAkbDeviceCharge();
//
//        return "Устройство разяжается: " + charge/time + "/мин";
//    }

    // TODO check
    public String averageSpeedAnalise(Device device){
        /*//List<Location> lastDeviceLocation = locationRepo.findLastByDevice(device);

        Date from = new Date((lastDeviceLocation.get(0).getCreated().getTime() - 600000));
        Date to = lastDeviceLocation.get(0).getCreated();

        List<Location> locations = locationRepo.findByCreatedDateBetweenAndDeviceOrderByIdDesc(from, to, device);
*/
        float speed = 0f;

/*        for (Location location: locations){
            speed = speed + location.getSpeed();
        }

        speed = speed/locations.size();*/

        return "Средняя скорость: " + speed;
    }
}
