package com.landau.core.stats.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.lampa.republish.interfaces.hooks.IHook;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.stats.entity.StatsDeviceBattery;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.entity.StatsDeviceSpeedDistance;
import com.landau.core.stats.repos.StatsDeviceBatteryRepo;
import com.landau.core.stats.repos.StatsDeviceOnlineUptimeRepo;
import com.landau.core.stats.repos.StatsDeviceSpeedDistanceRepo;
import com.landau.objects.base.Serial;
import com.landau.objects.stats.DeviceBatteryStats;
import com.landau.objects.stats.DeviceSpeedDistanceStats;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
@Data
@Slf4j
@Service
public class DeviceStatsMessages implements IService, IHook<Serial> {
    @Autowired
    private StatsDeviceBatteryRepo statsDeviceBatteryRepo;

    @Autowired
    private StatsDeviceSpeedDistanceRepo statsDeviceSpeedDistanceRepo;
    
    @Autowired
    private StatsDeviceOnlineUptimeRepo statsDeviceOnlineUptimeRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private CastService castService;

    @Override
    public void preConsume(Serial deviceSerial) {
        Optional<Device> optionalDevice = deviceRepo.findBySerial(deviceSerial.getSerial());

        if(optionalDevice.isPresent()) {
            StatsDeviceOnlineUptime uptime = statsDeviceOnlineUptimeRepo.findFirstByDeviceOrderByIdDesc(optionalDevice.get());

            if(uptime != null) {
                uptime.setEndSession(new Date());
                statsDeviceOnlineUptimeRepo.save(uptime);
            }
        }
    }

    /**
     * wait DeviceBatteryStats
     * @param deviceBatteryStats
     */
    @ReConsumer(listen = DeviceBatteryStats.class, autoDelete = false)
    public void consumeDeviceBatteryStats(DeviceBatteryStats deviceBatteryStats) {
        log.info("consume message: " + deviceBatteryStats.getClass().getSimpleName());
        StatsDeviceBattery statsBattery = castService.castToEntity(deviceBatteryStats);
        if (statsBattery != null){
            statsDeviceBatteryRepo.save(statsBattery);
            log.info("save: " + statsBattery.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", StatsDeviceBattery.class);
        }
    }

    /**
     * wait DeviceSpeedDistanceStats
     * @param deviceSpeedDistanceStats
     */
    @ReConsumer(listen = DeviceSpeedDistanceStats.class, autoDelete = false)
    public void consumeDeviceSpeedDistanceStats(DeviceSpeedDistanceStats deviceSpeedDistanceStats) {
        log.info("consume message: " + deviceSpeedDistanceStats.getClass().getSimpleName());
        StatsDeviceSpeedDistance statsSpeedDistance = castService.castToEntity(deviceSpeedDistanceStats);
        if (statsSpeedDistance != null){
            statsDeviceSpeedDistanceRepo.save(statsSpeedDistance);
            log.info("save: " + statsSpeedDistance.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", StatsDeviceSpeedDistance.class);
        }
    }
}
