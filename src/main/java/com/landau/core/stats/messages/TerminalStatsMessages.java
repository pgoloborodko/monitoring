package com.landau.core.stats.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.stats.repos.TerminalDevicesStatsRepo;
import com.landau.core.stats.repos.TerminalSyncStatsRepo;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.objects.stats.TerminalDevicesStats;
import com.landau.objects.stats.TerminalSyncStats;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Slf4j
@Service
public class TerminalStatsMessages implements IService {
    @Autowired
    private TerminalDevicesStatsRepo terminalDevicesStatsRepo;

    @Autowired
    private TerminalSyncStatsRepo terminalSyncStatsRepo;

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CastService castService;

    /**
     * метод отвечает за обработку сообщений типа TerminalDevicesStats
     * @param terminalDevicesStatsMessage
     */
    @ReConsumer(listen = TerminalDevicesStats.class)
    public void consumeTerminalDeviceStats(TerminalDevicesStats terminalDevicesStatsMessage){
        log.info("consume message: " + terminalDevicesStatsMessage.getClass().getSimpleName());
        com.landau.core.stats.entity.TerminalDevicesStats terminalDevicesStats = castService.castToEntity(terminalDevicesStatsMessage);
        if (terminalDevicesStats != null){
            terminalDevicesStatsRepo.save(terminalDevicesStats);
            log.info("save: " + terminalDevicesStats.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", TerminalDevicesStats.class);
        }
    }

    /**
     * метод отвечает за обработку сообщений типа TerminalSyncStats
     * @param terminalSyncStatsMessage
     */
    @ReConsumer(listen = TerminalSyncStats.class)
    public void consumeTerminalSyncStats(TerminalSyncStats terminalSyncStatsMessage){
        log.info("consume message: " + terminalSyncStatsMessage.getClass().getSimpleName());
        com.landau.core.stats.entity.TerminalSyncStats terminalSyncStats = castService.castToEntity(terminalSyncStatsMessage);
        if (terminalSyncStats != null){
            terminalSyncStatsRepo.save(terminalSyncStats);
            log.info("save: " + terminalSyncStats.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", TerminalSyncStats.class);
        }
    }
}
