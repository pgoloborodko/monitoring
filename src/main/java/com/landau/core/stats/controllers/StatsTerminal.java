package com.landau.core.stats.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.stats.entity.TerminalStats;
import com.landau.core.stats.filters.StatsTerminalFilter;
import com.landau.core.stats.repos.TerminalStatsRepo;
import com.landau.core.stats.specs.StatsTerminalSpec;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * Статистика терминала
 */
@Api(tags = "Статистика терминала", description = "Контроллер обеспечивает получение статистики терминала за текущий день или выбранный промежуток времени")
@RestController
@RequestMapping("/stats/terminals")
public class StatsTerminal extends Base {
    private TerminalStatsRepo terminalStatsRepo;

    @Autowired
    public StatsTerminal(TerminalStatsRepo terminalStatsRepo) {
        this.terminalStatsRepo = terminalStatsRepo;
    }

    /**
     * Получение статистики работы терминала по фильтру
     * @param statsTerminalFilter фильтрация списка
     * @return
     */
    @ApiOperation(value = "Получение статистики работы терминала по фильтру", response = TerminalStats.class)
    @GetMapping
    public Collection<Values> get(StatsTerminalFilter statsTerminalFilter) {
        HashMap<Terminal, Values> values = new HashMap<>();
        if (getUser().getRole() != User.Role.ADMIN) {
            statsTerminalFilter.setCompany(getUser().getCompany());
        }

        for(TerminalStats terminalStats : terminalStatsRepo.findAll(StatsTerminalSpec.find(statsTerminalFilter))) {
            if(values.containsKey(terminalStats.getTerminal())) {
                values.get(terminalStats.getTerminal()).update(terminalStats);
            }
            else {
                Values value = new Values();
                value.update(terminalStats);
                values.put(terminalStats.getTerminal(), value);
            }
        }

        //saveToSessionLog("GET", servletRequest);

        return values.values();
    }

    @Data
    public class Values {
        private Terminal terminal;
        private Date date;
        private Long syncFilesCount;
        private Long syncFilesSize;
        private Long syncFilesInStorage;
        private Long deviceExtractCount;
        private Long deviceInsertCount;

        public void update(TerminalStats terminalStats) {
            terminal = terminalStats.getTerminal();

            if (terminalStats.getDate() != null){
                date = terminalStats.getDate();
            }
            if (terminalStats.getSyncFilesCount() != null){
                syncFilesCount = terminalStats.getSyncFilesCount();
            }

            if(terminalStats.getSyncFilesSize() != null) {
                syncFilesSize = terminalStats.getSyncFilesSize();
            }

            if (terminalStats.getSyncFilesInStorage() != null){
                syncFilesInStorage = terminalStats.getSyncFilesInStorage();
            }

            if (terminalStats.getDeviceExtractCount() != null){
                deviceExtractCount = terminalStats.getDeviceExtractCount();
            }

            if (terminalStats.getDeviceInsertCount() != null){
                deviceInsertCount = terminalStats.getDeviceInsertCount();
            }
        }
    }
}
