package com.landau.core.stats.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;
import com.landau.core.stats.filters.StatsDeviceOnlineUptimeFilter;
import com.landau.core.stats.repos.StatsDeviceOnlineUptimeRepo;
import com.landau.core.stats.specs.StatsOnlineUptimeSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * Статистика работы устройств за текущий день или выбранный промежуток времени
 */
@Api(tags = "Статистика работы устройств", description = "Контроллер обеспечивает получение статистики работы устройств за текущий день или выбранный промежуток времени")
@RestController("StatsDevices")
@RequestMapping("/stats/devices/uptime")
public class StatsUptimeDevices extends Base {
    private StatsDeviceOnlineUptimeRepo statsDeviceOnlineUptimeRepo;
    private EntityManager entityManager;

    @Autowired
    public StatsUptimeDevices(StatsDeviceOnlineUptimeRepo statsDeviceOnlineUptimeRepo, EntityManager entityManager) {
        this.statsDeviceOnlineUptimeRepo = statsDeviceOnlineUptimeRepo;
        this.entityManager = entityManager;
    }

    /**
     * Получение статистики работы
     * @param statsOnlineUptimeFilter поля фильтра
     * @return список Values
     */
    @ApiOperation(value = "Получение статистики работы", response = StatsDeviceOnlineUptime.class)
    @GetMapping
    public Collection<Values> get(StatsDeviceOnlineUptimeFilter statsOnlineUptimeFilter) {
        HashMap<Device, Values> values = new HashMap<>();

        if (getUser().getRole() != User.Role.ADMIN){
            statsOnlineUptimeFilter.setCompany(getUser().getCompany());
        }

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(statsOnlineUptimeFilter.getStartSession());
        startCal.set(Calendar.HOUR_OF_DAY, 0);
        startCal.set(Calendar.MINUTE, 0);
        startCal.set(Calendar.SECOND, 0);
        startCal.set(Calendar.MILLISECOND, 0);
        final Date startDate = startCal.getTime();

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(statsOnlineUptimeFilter.getEndSession());
        endCal.set(Calendar.HOUR_OF_DAY, 23);
        endCal.set(Calendar.MINUTE, 59);
        endCal.set(Calendar.SECOND, 59);
        endCal.set(Calendar.MILLISECOND, 0);
        final Date endDate = endCal.getTime();

        List<StatsDeviceOnlineUptime> uptimes = statsDeviceOnlineUptimeRepo.findAll(StatsOnlineUptimeSpec.find(statsOnlineUptimeFilter));

        for(StatsDeviceOnlineUptime uptime : uptimes) {
            entityManager.detach(uptime);

            if(values.containsKey(uptime.getDevice())) {
                values.get(uptime.getDevice()).update(uptime);
            }
            else {
                Values value = new Values(startDate, endDate);
                value.update(uptime);
                values.put(uptime.getDevice(), value);
            }
        }

        return values.values();
    }

    /**
     * Объект, содержащий аптайм одного устройства
     */
    @Getter
    @Setter
    public static class Values {
        private Device device;
        private Date startSession;
        private Date endSession;
        private Long uptimeValue = 0L;
        private Integer connectionCount = 0;
        private Date startDate;
        private Date endDate;

        public Values(Date startDate, Date endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public void update(StatsDeviceOnlineUptime uptime) {
            if(uptime.getEndSession().getTime() < uptime.getStartSession().getTime()) {
                return;
            }

            device = uptime.getDevice();

            if(uptime.getStartSession().getTime() < startDate.getTime()) {
                uptime.setStartSession(new Date(this.startDate.getTime()));
            }

            if(uptime.getEndSession().getTime() > endDate.getTime()) {
                uptime.setEndSession(new Date(this.endDate.getTime()));
            }

            if(startSession != null) {
                if(startSession.getTime() > uptime.getStartSession().getTime()) {
                    startSession = uptime.getStartSession();
                }
            }
            else {
                startSession = uptime.getStartSession();
            }

            if(endSession != null && uptime.getEndSession() != null) {
                if(endSession.getTime() < uptime.getEndSession().getTime()) {
                    endSession = uptime.getEndSession();
                }
            }
            else if(uptime.getEndSession() != null) {
                endSession = uptime.getEndSession();
            }

            if(uptime.getEndSession() != null) {
                uptimeValue += uptime.getEndSession().getTime() - uptime.getStartSession().getTime();
            }
            else {
                uptimeValue += new Date().getTime() - uptime.getStartSession().getTime();
            }

            connectionCount++;
        }
    }
}
