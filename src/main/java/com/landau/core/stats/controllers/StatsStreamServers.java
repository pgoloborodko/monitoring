package com.landau.core.stats.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.stats.entity.StatsStreamServer;
import com.landau.core.stats.filters.StatsStreamServerFilter;
import com.landau.core.stats.repos.StatsStreamServerRepo;
import com.landau.core.stats.specs.StatsStreamServerSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Статистика стрим. сервера", description = "Контроллер обеспечивает получение статистики для стрим. сервера")
@RestController("StreamServerStatsIndex")
@RequestMapping("/stats/streamServer")
public class StatsStreamServers extends Base {
    private StatsStreamServerRepo statsStreamServerRepo;

    @Autowired
    public StatsStreamServers(StatsStreamServerRepo statsStreamServerRepo) {
        this.statsStreamServerRepo = statsStreamServerRepo;
    }

    @ApiOperation(value = "Получение статистики стрим-сервера по фильтру", response = StatsStreamServer.class)
    @GetMapping
    public Page<StatsStreamServerFilter> get(StatsStreamServerFilter statsStreamServerFilter, Pageable pageable){
        if (getUser().getRole() != User.Role.ADMIN ){
            statsStreamServerFilter.setCompany(getUser().getCompany());
        }

        return statsStreamServerRepo.findAll(StatsStreamServerSpec.find(statsStreamServerFilter), pageable);
    }
}
