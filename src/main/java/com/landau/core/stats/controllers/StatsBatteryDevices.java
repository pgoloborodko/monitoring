package com.landau.core.stats.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceModel;
import com.landau.core.stats.entity.StatsDeviceBattery;
import com.landau.core.stats.filters.StatsDeviceBatteryFilter;
import com.landau.core.stats.repos.StatsDeviceBatteryRepo;
import com.landau.core.stats.specs.StatsBatterySpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

/**
 * Статистика работы аккумулятора устройства
 */
@Api(tags = "Статистика работы аккумулятора устройства", description = "Контроллер обеспечивает получение статистики работы аккумулятора устройства за текущий день или выбранный промежуток времени")
@RestController("BatteryStatsIndex")
@RequestMapping("/stats/devices/battery")
public class StatsBatteryDevices extends Base {
    private StatsDeviceBatteryRepo statsDeviceBatteryRepo;

    @Autowired
    public StatsBatteryDevices(StatsDeviceBatteryRepo statsDeviceBatteryRepo) {
        this.statsDeviceBatteryRepo = statsDeviceBatteryRepo;
    }

    /**
     * Получение статистики работы аккумуляторов по фильтру
     * @param statsBatteryFilter фильтрация списка
     * @return
     */
    @ApiOperation(value = "Получение статистики работы аккумуляторов по фильтру", response = StatsDeviceBattery.class)
    @GetMapping
    public Collection<Values> get(StatsDeviceBatteryFilter statsBatteryFilter){
        HashMap<Device, Values> values = new HashMap<>();

        if (getUser().getRole() != User.Role.ADMIN) {
            statsBatteryFilter.setCompany(getUser().getCompany());
        }

        for(StatsDeviceBattery battery : statsDeviceBatteryRepo.findAll(StatsBatterySpec.find(statsBatteryFilter))) {
            if(values.containsKey(battery.getDevice())) {
                values.get(battery.getDevice()).update(battery);
            }
            else {
                Values value = new Values();
                value.update(battery);
                values.put(battery.getDevice(), value);
            }
        }

        return values.values();
    }

    /**
     * Детальная информация по статистике для одного устройства
     * @param device устройство
     * @param date дата
     * @return
     */
    @ApiOperation(value = "Детальная информация по статистике для одного устройства", response = StatsDeviceBattery.class)
    @Authority(Device.class)
    @GetMapping("details")
    public List<StatsDeviceBattery> details(@RequestParam("device") Device device,
                                            @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("date") Date date){

        if(device == null || date == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "date or device not set");
        }

        Date dateTo = new Date(date.getTime());
        dateTo.setHours(23);
        dateTo.setMinutes(59);

        return statsDeviceBatteryRepo.findAllByDeviceAndFromGreaterThanEqualAndToLessThanEqualOrderByIdAsc(device, date, dateTo);
    }

    /**
     * Объект, содержащий статистику батарейки одного устройства
     */
    public class Values {
        private Device device;
        private Integer chargePercentAtHour = 0;
        private Integer dischargePercentAtHour = 0;
        private Integer times = 0;

        private Byte lastCharge = 0;
        private List<Byte> chart = new ArrayList<>();

        protected void update(StatsDeviceBattery battery) {
            device = battery.getDevice();
//
//            if(battery.getAction() == StatsBattery.Action.CHARGE) {
//                chargePercentAtHour += battery.getPercent();
//            }
//
//            if(battery.getAction() == StatsBattery.Action.DISCHARGE) {
//                dischargePercentAtHour += battery.getPercent();
//            }

            chart.add(battery.getCurrentAkbCharge());
            lastCharge = battery.getCurrentAkbCharge();
            times += (int) (battery.getTo().getTime() - battery.getFrom().getTime()) / 1000;
        }

        public Device getDevice() {
            return device;
        }

        public Integer getPercentChargeAtHour() {
            return chargePercentAtHour;
        }

        public Integer getPercentDischargeAtHour() {
            return dischargePercentAtHour;
        }

        public Integer getCapacityChargeAtHour() {
            DeviceModel deviceModel = device.getModel();

            if(deviceModel != null) {
                return deviceModel.getBatteryCapacity() * chargePercentAtHour / 100;
            }

            return null;
        }

        public Integer getCapacityDischargeAtHour() {
            DeviceModel deviceModel = device.getModel();

            if(deviceModel != null) {
                return deviceModel.getBatteryCapacity() * dischargePercentAtHour / 100;
            }

            return null;
        }

        public List<Byte> getChart() {
            return chart;
        }

        public Integer getChargeTimeLeftInSeconds() {
            if(lastCharge != null && chargePercentAtHour > 0) {
                return ((100 - lastCharge) / chargePercentAtHour) * times;
            }

            return null;
        }

        public Integer getDischargeTimeLeftInSeconds() {
            if(lastCharge != null && dischargePercentAtHour != 0) {
                return (lastCharge / dischargePercentAtHour) * times;
            }

            return null;
        }

        public Byte getLastCharge() {
            return lastCharge;
        }
    }
}
