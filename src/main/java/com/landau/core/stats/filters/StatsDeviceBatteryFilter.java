package com.landau.core.stats.filters;

import com.landau.core.companies.entities.Company;
import com.landau.core.stats.entity.StatsDeviceBattery;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class StatsDeviceBatteryFilter extends StatsDeviceBattery {
    private Company company;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date from = new Date();

    @Override
    public Date getFrom() {
        return from;
    }

    @Override
    public void setFrom(Date from) {
        this.from = from;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
