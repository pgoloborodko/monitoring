package com.landau.core.stats.filters;

import com.landau.core.cities.entities.City;
import com.landau.core.companies.entities.Company;
import com.landau.core.districts.entities.District;
import com.landau.core.stats.entity.StatsDeviceOnlineUptime;

public class StatsDeviceOnlineUptimeFilter extends StatsDeviceOnlineUptime {
    private Company company;
    private City city;
    private District district;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }
}
