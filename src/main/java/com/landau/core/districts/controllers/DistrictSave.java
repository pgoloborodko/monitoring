package com.landau.core.districts.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.districts.entities.District;
import com.landau.core.districts.repos.DistrictRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Сохранение района
 */
@Api(tags = "Сохранение района", description = "Контроллер может получать данные определенного района, а также создавать и сохранять новые")
@RestController(value = "DistrictsSave")
@RequestMapping("/districts/save")
public class DistrictSave extends Base {
    private DistrictRepo districtRepo;

    @Autowired
    public DistrictSave(DistrictRepo districtRepo) {
        this.districtRepo = districtRepo;
    }

    /**
     * Получение данных района
     * @param district id района
     * @return объект района
     */
    @ApiOperation(value = "Получение данных района", response = District.class)
    @Authority(District.class)
    @GetMapping
    public District get(District district) {
        return district;
    }

    /**
     * Сохранение данных района
     * @param district объект района
     * @param result
     * @return false с ошибкой, либо true с объектом района
     */
    @ApiOperation(value = "Сохранение данных района", response = District.class)
    @Authority(District.class)
    @PostMapping
    public District post(District district, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        if(district.getId() != null && district.getCompany() == null && getUser().getRole() != User.Role.ADMIN) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "not allowed to save");
        }

        if(getUser().getRole() != User.Role.ADMIN && !getUser().getCompany().equals(district.getCity().getCompany())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "not allowed to save to city");
        }

        if(getUser().getRole() != User.Role.ADMIN) {
            district.setCompany(getUser().getCompany());
        }

        return districtRepo.save(district);
    }
}

