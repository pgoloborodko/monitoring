package com.landau.core.districts.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.districts.entities.District;
import com.landau.core.districts.filters.DistrictFilter;
import com.landau.core.districts.repos.DistrictRepo;
import com.landau.core.districts.specs.DistrictSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список районов
 */
@Api(tags = "Список районов", description = "Контроллер отвечает за получение списка районов")
@RestController("DistrictsIndex")
@RequestMapping("/districts")
public class DistrictList extends Base {
    private DistrictRepo districtRepo;

    @Autowired
    public DistrictList(DistrictRepo districtRepo) {
        this.districtRepo = districtRepo;
    }

    /**
     * Получение списка районов
     * @param districtFilter фильтр районов
     * @return список районов
     */
    @ApiOperation(value = "Получение списка районов", response = District.class)
    @GetMapping
    public Page<District> get(Pageable pageable, DistrictFilter districtFilter) {
        if(getUser().getRole() != User.Role.ADMIN) {
            districtFilter.setCompany(getUser().getCompany());
        }

        return districtRepo.findAll(DistrictSpec.find(districtFilter), pageable);
    }
}