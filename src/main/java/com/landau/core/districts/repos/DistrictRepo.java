package com.landau.core.districts.repos;

import com.landau.core.cities.entities.City;
import com.landau.core.districts.entities.District;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DistrictRepo extends JpaRepository<District, Long>, JpaSpecificationExecutor<District> {
    Page<District> findAllByNameContainingAndCity(Pageable pageable, String name, City city);
    Page<District> findAllByNameContaining(Pageable pageable, String name);

}
