package com.landau.core.districts.filters;

import com.landau.core.companies.entities.Company;
import com.landau.core.districts.entities.District;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistrictFilter extends District {
    private Company concreteCompany;
}
