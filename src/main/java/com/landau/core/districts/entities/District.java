package com.landau.core.districts.entities;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.cities.entities.City;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.districts.repos.DistrictRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Район
 */
@Entity
@Table
@JsonInclude
@Repo(DistrictRepo.class)
@ApiModel(description = "Район")
public class District extends AbstractBusiness implements Serializable {
    @Column
    @ApiModelProperty(value = "Название района")
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @ApiModelProperty(value = "Город")
    private City city;

    /**
     * Название района
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Сущность города
     * @return
     */
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
