package com.landau.core.districts.specs;

import com.landau.core.districts.entities.District;
import com.landau.core.districts.filters.DistrictFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class DistrictSpec {
    public static Specification<District> find(DistrictFilter districtFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (districtFilter.getName() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), districtFilter.getName().toUpperCase() +"%"));
            }

            if (districtFilter.getCity() != null) {
                predicates.add(criteriaBuilder.equal(root.get("city"), districtFilter.getCity()));
            }

            if (districtFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("company"), districtFilter.getCompany()), criteriaBuilder.isNull(root.get("company"))));
            }

            if (districtFilter.getConcreteCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), districtFilter.getConcreteCompany()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
