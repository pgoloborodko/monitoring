package com.landau.core.biometric.controllers;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.filters.BiometricFilter;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.biometric.specs.BiometricSpec;
import com.landau.core.common.ApiPageable;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Получение списка биометрических служб
 */
@Api(tags = "Список биометрических служб", description = "Контроллер обеспечивает получение списка биометрических служб")
@RestController(value = "BiometricIndex")
@RequestMapping("/biometric")
public class BiometricList extends Base {
    private BiometricRepo biometricRepo;

    @Autowired
    public BiometricList(BiometricRepo biometricRepo) {
        this.biometricRepo = biometricRepo;
    }

    /**
     * Список биометрических служб
     * @param biometricFilter поля фильтра
     * @return список биометрических служб
     */
    @ApiPageable
    @ApiOperation(value = "Получение списка биометрических служб", response = Biometric.class)
    @GetMapping
    public Page<Biometric> get(BiometricFilter biometricFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            biometricFilter.setCompany(getUser().getCompany());
        }

        return biometricRepo.findAll(BiometricSpec.find(biometricFilter), pageable);
    }
}
