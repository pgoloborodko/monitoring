package com.landau.core.biometric.controllers;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Просмотр и сохранение биометрического комплекса
 */
@Api(tags = "Сохранение биометрического комплекса", description = "Контроллер обеспечивает просмотр и сохранение данных биометрического комплекса")
@RestController
@RequestMapping("/biometric/save")
public class BiometricSave extends Base {
    protected BiometricRepo biometricRepo;

    @Autowired
    public BiometricSave(BiometricRepo biometricRepo) {
        this.biometricRepo = biometricRepo;
    }

    /**
     * Получение данных биометрической службы
     * @param biometric id службы
     * @return объект com.landau.core.biometric.entities.Biometric
     */
    @ApiOperation(value = "Получение данных биометрической службы", response = Biometric.class)
    @Authority(Biometric.class)
    @GetMapping
    public Biometric get(Biometric biometric) {
        return biometric;
    }

    /**
     * Сохранение параметров биометрической службы
     * @param biometric объект биометрии
     * @param bindingResult
     * @return объект биометрии
     */
    @ApiOperation(value = "Изменение параметров биометрической службы", response = Biometric.class)
    @Authority(value = Biometric.class, required = true)
    @PostMapping
    public Biometric post(Biometric biometric, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, bindingResult.getAllErrors().toString()); //This line gives cannot be instantiated error.
        }

        return biometricRepo.save(biometric);
    }
}
