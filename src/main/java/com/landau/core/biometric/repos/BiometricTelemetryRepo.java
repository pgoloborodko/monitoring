package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricTelemetry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BiometricTelemetryRepo extends JpaRepository<BiometricTelemetry, Long> {
    Optional<BiometricTelemetry> findByBiometric(Biometric biometric);
}
