package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.common.repos.JpaSerialRepository;
import com.landau.core.companies.entities.Company;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface BiometricRepo extends JpaSerialRepository<Biometric, Long>, JpaSpecificationExecutor<Biometric> {
    Optional<Biometric> findByIdAndCompany(Long id, Company company);
    Optional<Biometric> findBySerial(String serial);

    @Modifying
    @Transactional
    @Query("UPDATE Biometric r SET online = :online")
    Integer updateStatus(@Param("online") Boolean online);

}