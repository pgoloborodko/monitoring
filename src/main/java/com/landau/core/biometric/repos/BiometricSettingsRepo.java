package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricSettings;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BiometricSettingsRepo extends JpaRepository<BiometricSettings, Long> {
    Optional<BiometricSettings> findByBiometric(Biometric biometric);
}
