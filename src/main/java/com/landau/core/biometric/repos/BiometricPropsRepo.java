package com.landau.core.biometric.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricProps;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BiometricPropsRepo extends JpaRepository<BiometricProps, Long> {
    Optional<BiometricProps> findByBiometric(Biometric biometric);
}
