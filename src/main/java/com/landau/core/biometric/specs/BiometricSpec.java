package com.landau.core.biometric.specs;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.filters.BiometricFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class BiometricSpec {
    public static Specification<Biometric> find(BiometricFilter biometricFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();


            if (biometricFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), biometricFilter.getCompany()));
            }

            if (biometricFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("city"), biometricFilter.getCity()));
            }

            if (biometricFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("district"), biometricFilter.getDistrict()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
