package com.landau.core.biometric.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.biometric.repos.BiometricPropsRepo;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.common.messages.CastService;
import com.landau.objects.props.BiometricProps;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Сервис принимает и обрабатывает сообщения типа props для Biometric
 */
@Data
@Slf4j
@Service
public class BiometricPropsMessages implements IService {
    @Autowired
    private BiometricRepo biometricRepo;

    @Autowired
    private BiometricPropsRepo biometricPropsRepo;

    @Autowired
    private CastService castService;

    /**
     * Получение и обработка сообщения типа BiometricProps
     * @param biometricPropsMessage
     */
    @ReConsumer(listen = BiometricProps.class)
    public void consumeBiometricProps(BiometricProps biometricPropsMessage){
        log.info("consume message: " + biometricPropsMessage.getClass().getSimpleName());
        com.landau.core.biometric.entities.BiometricProps biometricProps = castService.castToEntity(biometricPropsMessage);
        if (biometricProps != null){
            biometricPropsRepo.save(biometricProps);
            log.info("save: {}", biometricProps.getClass().getSimpleName());
        }
        else {
            log.warn("value is null: {}", BiometricProps.class);
        }
    }
}
