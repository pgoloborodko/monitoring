package com.landau.core.biometric.messages;

import com.lampa.republish.interfaces.IService;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.profiles.repos.ProfileRepo;
import com.landau.core.tasks.repos.TaskRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BiometricListener implements IService {
    @Autowired
    BiometricRepo biometricRepo;

    @Autowired
    ProfileRepo peopleRepo;

    @Autowired
    DeviceRepo deviceRepo;

    @Autowired
    TaskRepo taskRepo;

    /*@Autowired
    private MessageService messageService;*/



/*
    public Biometric onResponseMessage(Biometric com.landau.core.biometric, ResponseObject responseObject) {
        if(responseObject.getAction() == RequestObject.Action.info) {
            return onInfo(com.landau.core.biometric, responseObject.getServerData());

        }
        else if(responseObject.getAction() == RequestObject.Action.list) {
            onList(com.landau.core.biometric, responseObject);
        }
        else if(responseObject.getAction() == RequestObject.Action.tracking) {
            return onTracking(com.landau.core.biometric, responseObject);
        }

        return com.landau.core.biometric;
    }
    */

    /**
     *
     * @param com.landau.core.biometric
     * @param serverObject
     * @return

    private Biometric onInfo(Biometric com.landau.core.biometric, ServerObject serverObject) {
        com.landau.core.biometric.setUsers(serverObject.getCount());
        com.landau.core.biometric.setMode(serverObject.getMode());
        com.landau.core.biometric.setVersion(serverObject.getVersion());

        requestUserList(com.landau.core.biometric);

        return com.landau.core.biometric;
    } */

    /**
     *

    private void requestUserList(Biometric com.landau.core.biometric) {
        RequestObject requestObject = new RequestObject();
        requestObject.setAction(RequestObject.Action.list);
        requestObject.setPage(new PageObject());

        messageService.send(com.landau.core.biometric, requestObject);
    }
     */
    /**
     *
     * @param com.landau.core.biometric
     * @param responseObject
     * @return
    private Biometric onList(Biometric com.landau.core.biometric, ResponseObject responseObject) {
        List<UserObject> userObjects = responseObject.getUserList();

        List<People> peoples = peopleRepo.findAllByCompany(com.landau.core.biometric.getCompany());

        //entityManager.detach(peoples);


        Iterator<People> index = peoples.iterator();
        while (index.hasNext()) {
            People people = index.next();

            Iterator<UserObject> indexObj = userObjects.iterator();
            while (indexObj.hasNext()) {
                UserObject userObject = indexObj.next();

                try {
                    if(people.getId().longValue() == Long.valueOf(userObject.getToken())) {
                        if(!userObject.getDescription().equals(people.getUpdated().toString())) {
                            try {
                                userObject.setDescription(people.getUpdated().toString());
                                userObject.setPhoto(ArrayUtils.toObject(FileUtils.readFileToByteArray(people.getPhotoAsFile())));

                                RequestObject requestObject = new RequestObject();
                                requestObject.setAction(RequestObject.Action.update);
                                requestObject.setUser(userObject);

                                messageService.send(com.landau.core.biometric, requestObject);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        index.remove();
                        indexObj.remove();
                    }
                }
                catch (NumberFormatException ex) {
                    //indexObj.remove();
                    log.error("NumberFormatException", ex);
                }
            }
        }

        for(People people : peoples) {
            if(people.getPhotoAsFile().exists()) {
                try {
                    UserObject userObject = new UserObject();
                    userObject.setToken(people.getId().toString());
                    userObject.setDescription(people.getUpdated().toString());
                    userObject.setName(people.getLastName() + " " + people.getFirstName());
                    userObject.setPhoto(ArrayUtils.toObject(FileUtils.readFileToByteArray(people.getPhotoAsFile())));

                    RequestObject requestObject = new RequestObject();
                    requestObject.setAction(RequestObject.Action.create);
                    requestObject.setUser(userObject);

                    messageService.send(com.landau.core.biometric, requestObject);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return com.landau.core.biometric;
    }

    private Biometric onTracking(Biometric com.landau.core.biometric, ResponseObject responseObject) {
        FaceObject faceObject = responseObject.getFaceData();

        if(faceObject.getTracking() && faceObject.getExternal()) {
            People people = peopleRepo.findById(Long.valueOf(faceObject.getCredentialToken())).orElse(null);

            if(people != null && people.getType() == People.Type.CRIMINAL) {

                Query query = new Query();
                query.setType(TaskObject.Type.IDENTY);
                query.setAdded(new Date());
                //query.setDevice(device);
                //query.setCity(device.getCity());
                //query.setDistrict(device.getDistrict());
                query.setCompany(people.getCompany());
                query.setCompany(people.getCompany());
                query.setCursor(people.getId());
                query.setResult((int)(faceObject.getScore() * 100));
                query.setText(people.getLastName() + " " + people.getFirstName() + " " + people.getSecondName());

                queryRepo.save(query);

                for(BrowserConnector browserConnector : webSocketServer.filterRecipients(BrowserConnector.class)) {
                    browserConnector.sendBy(query, people.getCompany(), null, null);
                }


                List<Device> com.landau.core.devices = deviceRepo.findAllByCompanyAndOnline(people.getCompany(), true);

                if(com.landau.core.devices.size() > 0) {
                    Task task = new Task();
                    task.setFindPeople(people);
                    task.setMaker(Task.Maker.BIOMETRIC);
                    task.setMakerBiometric(com.landau.core.biometric);
                    task.setType(Task.Type.FIND_PEOPLE);
                    task.setCompany(people.getCompany());

                    taskRepo.save(task);

                    List<TaskDevice> taskDevices = new ArrayList<>();
                    for(Device device : com.landau.core.devices) {
                        TaskDevice taskDevice = new TaskDevice();
                        taskDevice.setDevice(device);
                        taskDevice.setTask(task);
                        taskDevices.add(taskDevice);
                    }

                    task.setPerformers(taskDevices);
                    taskRepo.save(task);


                    messageService.send(com.landau.core.biometric, requestObject);

                    for(Device device : com.landau.core.devices) {
                        DeviceConnector deviceConnector = (DeviceConnector) webSocketServer.find(device);

                        if(deviceConnector != null) {
                            deviceConnector.sendOrder(task, false);
                        }
                    }
                }
            }
            else if(people != null && people.getType() == People.Type.USUAL) {
                Query query = new Query();
                query.setType(TaskObject.Type.IDENTY);
                query.setAdded(new Date());

                try {
                    query.setResult((int) (faceObject.getScore() * 100)); // score
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                query.setText(people.getLastName() + " " + people.getFirstName() + " " + people.getSecondName());
                queryRepo.save(query);

                List<Device> com.landau.core.devices = deviceRepo.findAllByCompanyAndOnline(people.getCompany(), true);

                for(Device device : com.landau.core.devices) {
                    DeviceConnector deviceConnector = (DeviceConnector) webSocketServer.find(device);

                    if(deviceConnector != null) {
                        deviceConnector.sendPeoplePhoto(people,"Найден человек", true);
                    }
                }
            }
        }

        return com.landau.core.biometric;
    }
     */
}
