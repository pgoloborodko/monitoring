package com.landau.core.biometric.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.biometric.repos.BiometricSettingsRepo;
import com.landau.core.common.messages.CastService;
import com.landau.objects.settings.BiometricSettings;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Сервис принимает и обрабатывает сообщения типа settings для Biometric
 */
@Data
@Slf4j
@Service
public class BiometricSettingsMessage implements IService {
    @Autowired
    private BiometricRepo biometricRepo;

    @Autowired
    private BiometricSettingsRepo biometricSettingsRepo;

    @Autowired
    private CastService castService;

    /**
     * Получение и обработка сообщения типа BiometricSettings
     * @param biometricSettingsMessage
     */
    @ReConsumer(listen = BiometricSettings.class)
    public void consumeBiometricSettings(BiometricSettings biometricSettingsMessage){
        log.info("consume message: " + biometricSettingsMessage.getClass().getSimpleName());
        com.landau.core.biometric.entities.BiometricSettings biometricSettings = castService.castToEntity(biometricSettingsMessage);
        if (biometricSettings != null){
            biometricSettingsRepo.save(biometricSettings);
            log.info("save: {}", biometricSettings.getClass().getSimpleName());
        }
        else {
            log.warn("value is null: {}", BiometricSettings.class);
        }

    }
}
