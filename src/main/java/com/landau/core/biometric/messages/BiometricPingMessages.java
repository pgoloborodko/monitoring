package com.landau.core.biometric.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.objects.telemetry.Ping;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
@Slf4j
@Data
@Service
public class BiometricPingMessages implements IService {
    private static long timeout = 30000;
    private HashMap<Biometric, Timer> pings = new HashMap<>();

    @Autowired
    private BiometricRepo biometricRepo;

    @Autowired
    private CoreBrowserMessages coreBrowserMessages;

    /**
     * wait ping from com.landau.core.biometric
     * @param ping
     */
    @ReConsumer(listen = Ping.class)
    public void consumePing(Ping ping) {
        Optional<Biometric> optionalBiometric = biometricRepo.findBySerial(ping.getSerial());

        if(optionalBiometric.isPresent()) {
            Biometric biometric = optionalBiometric.get();

            if(pings.containsKey(biometric)) {
                pings.get(biometric).cancel();
            }
            else {
                biometric.setOnline(true);
                biometricRepo.save(biometric);

                try {
                    coreBrowserMessages.send("", biometric);
                    log.info("send message: {}", Biometric.class);
                } catch (RepublisherException e) {
                    e.printStackTrace();
                }
            }

            Timer timer = new Timer();
            timer.schedule(new PingTimerTask(biometric), timeout);
            pings.put(biometric, timer);
        }
    }

    private class PingTimerTask extends TimerTask {
        public PingTimerTask(Biometric biometric) {
            this.biometric = biometric;
        }

        private Biometric biometric;
        @Override
        public void run() {
            Optional<Biometric> optionalBiometric = biometricRepo.findById(biometric.getId());

            if(optionalBiometric.isPresent()) {
                Biometric biometric = optionalBiometric.get();
                biometric.setOnline(false);
                biometricRepo.save(biometric);

                pings.remove(biometric);

                try {
                    coreBrowserMessages.send("", biometric);
                    log.info("send message: {}", Biometric.class);
                } catch (RepublisherException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
