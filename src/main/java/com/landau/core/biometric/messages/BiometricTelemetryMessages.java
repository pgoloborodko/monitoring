package com.landau.core.biometric.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.biometric.repos.BiometricTelemetryRepo;
import com.landau.core.common.messages.CastService;
import com.landau.objects.telemetry.BiometricTelemetry;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Сервис принимает и обрабатывает сообщения типа telemetry для Biometric
 */
@Data
@Slf4j
@Service
public class BiometricTelemetryMessages implements IService {
    @Autowired
    private BiometricTelemetryRepo biometricTelemetryRepo;

    @Autowired
    private BiometricRepo biometricRepo;

    @Autowired
    private CastService castService;

    /**
     * Получение и обработка сообщения типа BiometricTelemetry
     * @param biometricTelemetryMessage
     */
    @ReConsumer(listen = BiometricTelemetry.class)
    public void consumeBiometricTelemetry(BiometricTelemetry biometricTelemetryMessage){
        log.info("consume message: " + biometricTelemetryMessage.getClass().getSimpleName());
        com.landau.core.biometric.entities.BiometricTelemetry biometricTelemetry = castService.castToEntity(biometricTelemetryMessage);

        if (biometricTelemetry != null){
            biometricTelemetryRepo.save(biometricTelemetry);
            log.info("save: " + biometricTelemetry.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", BiometricTelemetry.class);
        }
    }
}
