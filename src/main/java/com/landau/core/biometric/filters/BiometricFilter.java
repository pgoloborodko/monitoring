package com.landau.core.biometric.filters;

import com.landau.core.biometric.entities.Biometric;
import io.swagger.annotations.ApiModelProperty;

/**
 * Фильтр для биометрических служб
 */
public class BiometricFilter extends Biometric {
    /**
     * Город
     */
    @ApiModelProperty(value = "Идентификатор города", example = "0")
    private Long city;

    /**
     * Район
     */
    @ApiModelProperty(value = "Идентификатор района", example = "0")
    private Long district;

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public Long getDistrict() {
        return district;
    }

    public void setDistrict(Long district) {
        this.district = district;
    }
}
