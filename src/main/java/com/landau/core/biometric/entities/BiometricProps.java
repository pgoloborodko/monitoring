package com.landau.core.biometric.entities;

import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Table
@Entity
@ApiModel(description = "разрешения для биометрических систем")
public class BiometricProps extends AbstractId {
    @Column
    @ApiModelProperty(value = "версия агента")
    private String agentVersion;

    @Column
    @ApiModelProperty(value = "прошивка")
    private String firmware;

    @Column
    @ApiModelProperty(value = "поставщик")
    private String vendor;

    @ManyToOne
    @ApiModelProperty(value = "биометрическая система")
    private Biometric biometric;
}
