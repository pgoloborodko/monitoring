package com.landau.core.biometric.entities;

import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table
@ApiModel(description = "Телеметрия биометрической системы")
public class BiometricTelemetry extends AbstractId {
    @Column
    @ApiModelProperty(value = "загрузка оперативной памяти")
    private Byte cpuLoad;

    @Column
    @ApiModelProperty(value = "загрузка видеопамяти")
    private Byte gpuLad;

    @ManyToOne
    @ApiModelProperty(value = "загрузка биометрической системы")
    private Biometric biometric;
}
