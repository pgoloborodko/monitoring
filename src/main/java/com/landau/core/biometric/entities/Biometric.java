package com.landau.core.biometric.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusinessSerial;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Биометрические объекты
 */
@Data
@Entity
@Table
@Repo(BiometricRepo.class)
@JsonInclude
@ApiModel(description = "Биометрические системы")
public class Biometric extends AbstractBusinessSerial {
    /**
     * Типы биометрии
     */
    public enum Type {
        /**
         * Проходная
         */
        SECURITY,
        /**
         * Камера
         */
        CAMERA
    }

    @Column
    @ApiModelProperty(value = "Название биометрической службы")
    private String name;


    @Column
    @ApiModelProperty(value = "Версия биометрической службы")
    private String version;

    @Column
    @ApiModelProperty(value = "Количество пользователей")
    private Long users;

    @Column
    @ApiModelProperty(value = "Тип биометрической службы")
    private Type type;
}
