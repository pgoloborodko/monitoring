package com.landau.core.biometric.entities;

import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table
@ApiModel(description = "Настройки биометрических систем")
public class BiometricSettings extends AbstractId {
    public enum Mode {
        MANUAL,
        TRACKING
    }

    @Column
    @ApiModelProperty(value = "режим системы")
    private Mode mode;

    @ManyToOne
    @ApiModelProperty(value = "биометрическая система")
    private Biometric biometric;
}
