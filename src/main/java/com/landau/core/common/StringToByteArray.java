package com.landau.core.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class StringToByteArray extends JsonDeserializer<Byte[]> {

    @Override
    public Byte[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        return ArrayUtils.toObject(Base64.getEncoder().encode(jsonParser.getText().getBytes(StandardCharsets.UTF_8)));
    }
}