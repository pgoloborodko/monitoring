package com.landau.core.common.aspects;

import com.landau.core.users.entities.Session;
import com.landau.core.users.entities.SessionLog;
import com.landau.core.users.repos.SessionLogRepo;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

@Aspect
@Service
public class SessionLogger {
    @Autowired
    private SessionLogRepo sessionLogRepo;

    @Pointcut("within(@org.springframework.stereotype.Controller *)")
    public void controller() {
    }

    //@Before("controller()")
    private void beforeDTOInit(JoinPoint joinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        boolean isGet = false;

        if(signature.getMethod().isAnnotationPresent(GetMapping.class)) {
            isGet = true;
        }
        else if(signature.getMethod().isAnnotationPresent(RequestMapping.class)) {
            RequestMapping requestMapping = signature.getMethod().getAnnotation(RequestMapping.class);

            if(Arrays.asList(requestMapping.method()).contains(RequestMethod.GET)) {
                isGet = true;
            }
        }

        Method method = signature.getMethod();
        saveToSessionLog(isGet, method.getDeclaringClass().getName() + "." + method.getName(), request);
    }

    private void saveToSessionLog(boolean isGet, String desc, ServletRequest request){
        Session session = null;
        if(request.getAttribute("session") != null) {
            session = (Session)request.getAttribute("session");
        }

        SessionLog sessionLog = new SessionLog();
        sessionLog.setDescription(desc);
        sessionLog.setDate(new Date());
        sessionLog.setType(isGet ? SessionLog.Type.GET : SessionLog.Type.POST);
        sessionLog.setSession(session);

        //sessionLogRepo.save(sessionLog);
    }

}
