package com.landau.core.common.aspects.authority;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(Authorities.class)
public @interface Authority {
    Class value();
    boolean required() default false;
    // Class value();
}
