package com.landau.core.common.aspects.authority;

import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Aspect
@Service
@Slf4j
public class Checker {
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepo userRepo;

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void controller() {
    }

    @Before("controller()")
    private void before(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        List<Authority> dtoAuthorities = null;

        if(signature.getMethod().isAnnotationPresent(Authorities.class)) {
            dtoAuthorities = Arrays.asList(signature.getMethod().getAnnotation(Authorities.class).value());
        }
        else if(signature.getMethod().isAnnotationPresent(Authority.class)) {
            dtoAuthorities = Collections.singletonList(signature.getMethod().getAnnotation(Authority.class));
        }

        if(dtoAuthorities != null) {
            if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof Long) {
                Optional<User> optionalUser = userRepo.findById((Long) SecurityContextHolder.getContext().getAuthentication().getDetails());

                if(optionalUser.isPresent()) {
                    User user = optionalUser.get();

                    for(Authority dtoAuthority : dtoAuthorities) {
                        for (Object signatureArg : joinPoint.getArgs()) {
                            if(signatureArg == null) continue;

                            if (signatureArg.getClass() == dtoAuthority.value()) {
                                Repo repo = signatureArg.getClass().getAnnotation(Repo.class);

                                if (repo == null) {
                                    log.warn("error found repo for {}", signatureArg.getClass());
                                    throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "error found repo");
                                }

                                if(!(signatureArg instanceof AbstractId)) {
                                    log.warn("entity not abstractid {}", signatureArg.getClass());
                                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "entity not found");
                                }

                                if(dtoAuthority.required() && ((AbstractId)signatureArg).getId() == null) {
                                    log.warn("entity not exists {}", signatureArg.getClass());
                                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "entity must be exists");
                                }

                                Optional optional = Optional.empty();

                                if(((AbstractId)signatureArg).getId() != null) {
                                    if(signatureArg instanceof AbstractBusiness) {
                                        AbstractBusiness dtoId = (AbstractBusiness) signatureArg;
                                        JpaRepository<AbstractBusiness, Long> bean = (JpaRepository<AbstractBusiness, Long>) applicationContext.getBean(repo.value());

                                        optional = bean.findById(dtoId.getId());
                                    }
                                    else {
                                        AbstractId dtoId = (AbstractId) signatureArg;
                                        JpaRepository<AbstractId, Long> bean = (JpaRepository<AbstractId, Long>) applicationContext.getBean(repo.value());

                                        optional = bean.findById(dtoId.getId());
                                    }
                                }

                                if(optional.isPresent()) {
                                    if(signatureArg instanceof AbstractBusiness) {
                                        AbstractBusiness business = (AbstractBusiness) optional.get();

                                        if (user.getRole() != User.Role.ADMIN && business.getCompany() != null && !business.getCompany().equals(user.getCompany())) {
                                            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
                                        }
                                    }

                                    try {
                                        List<Field> fields = FieldUtils.getAllFieldsList(signatureArg.getClass());
                                        for (Field field : fields) {
                                            if (Modifier.isStatic(field.getModifiers())) {
                                                continue;
                                            }

                                            field.setAccessible(true);

                                            Object signatureFieldValue = field.get(signatureArg);

                                            if (signatureFieldValue == null) {
                                                field.set(signatureArg, field.get(optional.get()));
                                            } else {
                                                if (signature.getMethod().isAnnotationPresent(GetMapping.class)) {
                                                    field.set(signatureArg, field.get(optional.get()));
                                                } else if (signature.getMethod().isAnnotationPresent(RequestMapping.class)) {
                                                    RequestMapping requestMapping = signature.getMethod().getAnnotation(RequestMapping.class);

                                                    if (Arrays.asList(requestMapping.method()).contains(RequestMethod.GET)) {
                                                        field.set(signatureArg, field.get(optional.get()));
                                                    }
                                                }
                                            }
                                        }
                                    } catch (IllegalAccessException e) {
                                        log.error("error mapping entity", e);
                                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "error mapping entity");
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "bad user session");
                }
            }
        }
    }
}
