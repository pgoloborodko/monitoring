package com.landau.core.common.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface JpaSerialRepository<T, ID> extends JpaRepository<T, ID> {
    Optional<T> findBySerial(String var1);
}
