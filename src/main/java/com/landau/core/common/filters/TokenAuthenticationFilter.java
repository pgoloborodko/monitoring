package com.landau.core.common.filters;

import com.landau.core.users.entities.Token;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.TokenRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class TokenAuthenticationFilter implements Filter {
    @Autowired
    TokenRepo tokenRepo;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;


        String value = null;

        //extract token from header
        if(httpRequest.getHeader("token") != null) {
            value = httpRequest.getHeader("token");
        }
        else if(httpRequest.getParameter("token") != null) {
            value = httpRequest.getParameter("token");
        }

        if (value != null) {
            Token token = tokenRepo.findFirstByValue(value);
            if (token != null) {
                final User user = token.getUser();
                final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getPhone(), null, user.getAuthorities());

                authentication.setDetails(user);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        chain.doFilter(request, response);
    }

}