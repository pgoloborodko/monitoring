package com.landau.core.common.filters;

import com.landau.core.users.repos.TokenRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class HeadersFilter extends  OncePerRequestFilter {
    @Autowired
    TokenRepo tokenRepo;

    @Value("${app.version}")
    private String version;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        httpServletResponse.setHeader("App-Version", version); // Proxies.
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}