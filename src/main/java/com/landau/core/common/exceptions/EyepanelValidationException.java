package com.landau.core.common.exceptions;

import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * @author Alexey Belov
 */
public class EyepanelValidationException extends RuntimeException {

    private List<ObjectError> errors;

    public EyepanelValidationException(List<ObjectError> errors) {
        this.errors = errors;
    }

    public List<ObjectError> getErrors() {
        return errors;
    }
}
