package com.landau.core.common.services;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.repos.AppDeviceCommandRepo;
import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.cameras.repos.RemoteCameraRepo;
import com.landau.core.chat.repos.ChatMessageRepo;
import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.events.repos.*;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.users.entities.Role;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.RoleRepo;
import com.landau.core.users.repos.UserRepo;
import lombok.Getter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Service
public class ControllerService {
    @Autowired
    private AppRepo appRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private AppDeviceRepo appDeviceRepo;

    @Autowired
    private AppDeviceCommandRepo appDeviceCommandRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private Environment env;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private EventBiometricRecognitionRepo eventBiometricRecognitionRepo;

    @Autowired
    private EventDeviceButtonRepo eventDeviceButtonRepo;

    @Autowired
    private EventDeviceInstallerRepo eventDeviceInstallerRepo;

    @Autowired
    private EventDeviceStreamRepo eventDeviceStreamRepo;

    @Autowired
    private EventDeviceSystemRepo eventDeviceSystemRepo;

    @Autowired
    private EventTerminalSyncRepo eventTerminalSyncRepo;

    @Autowired
    private EventTerminalSystemRepo eventTerminalSystemRepo;

    @Autowired
    private ChatRoomRepo chatRoomRepo;

    @Autowired
    private MediaFileRepo mediaFileRepo;

    @Autowired
    private ChatMessageRepo chatMessageRepo;

    @Autowired
    private RemoteCameraRepo remoteCameraRepo;

    public User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getDetails();
    }

    @ModelAttribute
    protected Role getRole(@RequestParam(value = "id", required = false) Long role_id) {
        if (role_id == null) {
            return new Role();
        }
        Optional<Role> find = roleRepo.findById(role_id);
        return find.orElse(new Role());
    }

    @ModelAttribute
    protected User getUser(@RequestParam(value = "id", required = false) Long user_id) {
        if (user_id == null) {
            return new User();
        }
        Optional<User> find = userRepo.findById(user_id);
        return find.orElse(new User());
    }

    /**
     * Получение списка ролей
     *
     * @return список ролей (com.landau.core.users.entities.Role)
     */
    @ModelAttribute("roles")
    protected List<Role> getRoles() {
        return roleRepo.findAll();
    }

    public ModelMapper getMapper() {
        return mapper;
    }


    protected <D> D map(Object source, Class<D> destinationType) {
        return this.getMapper().map(source, destinationType);
    }

    public Device verifyDevice(Device device){
        Optional<Device> optionalDevice = deviceRepo.findBySerial(device.getSerial());
        return optionalDevice.orElse(null);
    }

    public AppDevice verifyDeviceAPK(AppDevice deviceApp){
        Optional<AppDevice> optionalDeviceApk = appDeviceRepo.findById(deviceApp.getId());
        return optionalDeviceApk.orElse(null);
    }

    public AppDeviceCommand createDeviceAPKCommand(Device device, App app, AppDeviceCommand.Type type){
        AppDeviceCommand deviceAppCommand = new AppDeviceCommand();
        deviceAppCommand.setCompany(device.getCompany());
        deviceAppCommand.setApp(app);
        deviceAppCommand.setType(type);
        deviceAppCommand.setAdded(new Date());
        deviceAppCommand.setDevice(device);
        deviceAppCommand.setComplete(false);
        return appDeviceCommandRepo.save(deviceAppCommand);
    }

    public Terminal verifyTerminal(Terminal terminal){
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(terminal.getSerial());
        return optionalTerminal.orElse(null);
    }

    public AppDevice verifyDeviceAPK(App app) {
        Optional<AppDevice> optionalDeviceApk = appDeviceRepo.findByApp(app);
        return optionalDeviceApk.orElse(null);
    }

    public App verifyApp(App app) {
        Optional<App> optionalApp = appRepo.findById(app.getId());
        return optionalApp.orElse(null);
    }
}
