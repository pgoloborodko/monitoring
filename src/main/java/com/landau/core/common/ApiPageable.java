package com.landau.core.common;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Страница результатов, которую вы хотите получить (0..N)", example = "0"),
        @ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Количество записей на страницу.", example = "0"),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Критерий сортировки: (asc|desc)"
                + "По умолчанию ASC. " + "Подерживается множество критериев.")})
public @interface ApiPageable {
}