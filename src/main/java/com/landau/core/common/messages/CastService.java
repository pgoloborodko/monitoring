package com.landau.core.common.messages;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.biometric.entities.Biometric;
import com.landau.core.biometric.entities.BiometricProps;
import com.landau.core.biometric.entities.BiometricSettings;
import com.landau.core.biometric.entities.BiometricTelemetry;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.chat.entities.ChatMessage;
import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceModel;
import com.landau.core.devices.entities.DeviceTelemetry;
import com.landau.core.devices.entities.DeviceWifiLog;
import com.landau.core.devices.repos.DeviceModelRepo;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.events.entities.*;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.stats.entity.StatsDeviceBattery;
import com.landau.core.stats.entity.StatsDeviceSpeedDistance;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import com.landau.objects.action.CreateChatMessage;
import com.landau.objects.action.SetDeviceSettings;
import com.landau.objects.action.SetTerminalSettings;
import com.landau.objects.data.AppList;
import com.landau.objects.data.Profile;
import com.landau.objects.events.biometric.BiometricFaceRecognitionEvent;
import com.landau.objects.events.biometric.BiometricFaceTrackingEvent;
import com.landau.objects.events.core.ChatGroupEvent;
import com.landau.objects.events.core.ChatMessageEvent;
import com.landau.objects.events.device.DeviceButtonEvent;
import com.landau.objects.events.device.DeviceInstallerEvent;
import com.landau.objects.events.device.DeviceStreamEvent;
import com.landau.objects.events.device.DeviceSystemEvent;
import com.landau.objects.events.terminal.TerminalFileEvent;
import com.landau.objects.events.terminal.TerminalSyncEvent;
import com.landau.objects.events.terminal.TerminalSystemEvent;
import com.landau.objects.props.DeviceProps;
import com.landau.objects.props.TerminalProps;
import com.landau.objects.settings.DeviceSettings;
import com.landau.objects.settings.TerminalSettings;
import com.landau.objects.stats.DeviceBatteryStats;
import com.landau.objects.stats.DeviceSpeedDistanceStats;
import com.landau.objects.stats.TerminalDevicesStats;
import com.landau.objects.stats.TerminalSyncStats;
import com.landau.objects.telemetry.TerminalTelemetry;
import com.landau.objects.telemetry.WiFiTelemetry;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

/**
 * Сервис отвечает за преобразование сообщений в сущности и подготовку сообщений
 */
@Data
@Service
public class CastService {

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private DeviceModelRepo deviceModelRepo;

    @Autowired
    private BiometricRepo biometricRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private AppRepo appRepo;

    @Autowired
    private ChatRoomRepo chatRoomRepo;

    @Autowired
    private LocationRepo locationRepo;


    /**
     *
     * @param messageToCast
     * @return
     */
    public MediaFile castToEntity(TerminalFileEvent messageToCast) {
        MediaFile mediaFile = null;
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(messageToCast.getSerial());
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getExternalDeviceSerial());

        if (optionalTerminal.isPresent() && optionalDevice.isPresent()){
            mediaFile = new MediaFile();
            if (optionalDevice.get().getCompany() != null){
                mediaFile.setCompany(optionalDevice.get().getCompany());
            }
            if (messageToCast.getDuration() != null){
                mediaFile.setDuration(messageToCast.getDuration().doubleValue());
            }
            if (optionalDevice.get().getUser() != null){
                mediaFile.setUser(optionalDevice.get().getUser());
            }

            mediaFile.setDevice(optionalDevice.get());
            mediaFile.setCreated(messageToCast.getCreated());
            mediaFile.setTerminal(optionalTerminal.get());
            mediaFile.setExternalPath(messageToCast.getExternalPath());
            mediaFile.setAdded(messageToCast.getAdded());
            mediaFile.setExternalDevice(messageToCast.getExternalDevice());
            mediaFile.setExternalUser(messageToCast.getExternalUser());
//            mediaFile.setExternal();
            mediaFile.setUrl(messageToCast.getExternalPath());
        }

        return mediaFile;
    }

    /**
     * в данном методе происходит подготовка сущности EventTerminalSync из сообщения TerminalSyncEvent
     * @param messageToCast
     * @return
     */
    public EventTerminalSync castToEntity(TerminalSyncEvent messageToCast) {
        EventTerminalSync event = null;
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(messageToCast.getSerial());
        Optional<Device> optionalDevice = deviceRepo.findById(messageToCast.getExternalDevice());
        Optional<User> optionalUser = userRepo.findById(messageToCast.getExternalUser());

        if(optionalTerminal.isPresent()) {
            event = new EventTerminalSync();
            event.setTerminal(optionalTerminal.get());
            event.setCreated(messageToCast.getCreated());
            if (optionalDevice.isPresent()){
                event.setDevice(optionalDevice.get());
            }
            if (optionalUser.isPresent()){
                event.setUser(optionalUser.get());
            }
            if (messageToCast.getState() != null){
                event.setType(EventTerminalSync.Type.valueOf(messageToCast.getState().name()));
            }
        }

        return event;
    }

    /**
     * в данном методе происходит подготовка сущности EventTerminalSystem из сообщения TerminalSystemEvent
     * @param messageToCast
     * @return
     */
    public EventTerminalSystem castToEntity(TerminalSystemEvent messageToCast) {
        EventTerminalSystem event = null;
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(messageToCast.getSerial());

        if(optionalTerminal.isPresent()) {
            event = new EventTerminalSystem();
            event.setTerminal(optionalTerminal.get());
            event.setCreated(messageToCast.getCreated());
            event.setInfo(messageToCast.getInfo());
            if (messageToCast.getType() != null){
                event.setType(EventTerminalSystem.Type.valueOf(messageToCast.getType().name()));
            }
        }

        return event;
    }

    /**
     * в данном методе происходит подготовка сущности EventDeviceSystem из сообщения DeviceSystemEvent
     * @param messageToCast
     * @return
     */
    public EventDeviceSystem castToEntity(DeviceSystemEvent messageToCast) {
        EventDeviceSystem event = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            event = new EventDeviceSystem();
            event.setDevice(optionalDevice.get());
            event.setCreated(messageToCast.getCreated());
            event.setInfo(messageToCast.getInfo());
            if (messageToCast.getType() != null){
                event.setType(EventDeviceSystem.Type.valueOf(messageToCast.getType().name()));
            }
        }

        return event;
    }

    /**
     * в данном методе происходит подготовка сущности EventDeviceStream из сообщения DeviceStreamEvent
     * @param messageToCast
     * @return
     */
    public EventDeviceStream castToEntity(DeviceStreamEvent messageToCast) {
        EventDeviceStream event = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            event = new EventDeviceStream();
            event.setDevice(optionalDevice.get());
            event.setCreated(messageToCast.getCreated());
            if (messageToCast.getType() != null){
                event.setType(EventDeviceStream.Type.valueOf(messageToCast.getType().name()));
            }
            if (messageToCast.getResolution() != null){
                event.setResolution(EventDeviceStream.Resolution.valueOf(messageToCast.getResolution().name()));
            }

            if (optionalDevice.get().getUser() != null) {
                event.setUser(optionalDevice.get().getUser());

                if(optionalDevice.get().getUser().getCompany() != null) {
                    event.setCompany(optionalDevice.get().getUser().getCompany());
                }
            }
        }

        return event;
    }


    /**
     * в данном методе происходит подготовка сущности EventDeviceButton из сообщения DeviceButtonEvent
     * @param messageToCast
     * @return
     */
    public EventDeviceButton castToEntity(DeviceButtonEvent messageToCast) {
        EventDeviceButton event = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            event = new EventDeviceButton();
            event.setDevice(optionalDevice.get());
            event.setCreated(messageToCast.getCreated());
            if (optionalDevice.get().getUser() != null){
                event.setUser(optionalDevice.get().getUser());
            }
            if (messageToCast.getButton() != null){
                event.setType(EventDeviceButton.Type.valueOf(messageToCast.getButton().name()));
            }
        }

        return event;
    }

    /**
     * в данном методе происходит подготовка сущности EventDeviceInstaller из сообщения DeviceInstallerEvent
     * @param messageToCast
     * @return
     */
    public EventDeviceInstaller castToEntity(DeviceInstallerEvent messageToCast) {
        EventDeviceInstaller event = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            event = new EventDeviceInstaller();
            event.setCreated(messageToCast.getCreated());
            event.setDevice(optionalDevice.get());
            event.setInfo(messageToCast.getInfo());
            if (messageToCast.getType() != null){
                event.setType(EventDeviceInstaller.Type.valueOf(messageToCast.getType().name()));
            }
        }

        return event;
    }

    /**
     * в данном методе происходит подготовка сущности EventBiometricRecognition из сообщения BiometricFaceTrackingEvent
     * @param messageToCast
     * @return
     */
    public EventBiometricRecognition castToEntity(BiometricFaceTrackingEvent messageToCast) {
        EventBiometricRecognition event = null;
        Optional<Biometric> optionalBiometric = biometricRepo.findBySerial(messageToCast.getSerial());
        if (optionalBiometric.isPresent()){
            event = new EventBiometricRecognition();
            event.setBiometric(optionalBiometric.get());
            event.setCreated(messageToCast.getStarted());
            event.setEnded(messageToCast.getEnded());

            event.setExternalProfile(messageToCast.getExternalProfile());

            event.setFace(messageToCast.getFace());
            event.setRecognized(messageToCast.getRecognized());
            event.setType(EventBiometricRecognition.Type.TRACKING);
            event.setScore(messageToCast.getScore());
        }
        return event;
    }

    /**
     * в данном методе происходит подготовка сущности EventBiometricRecognition из сообщения BiometricFaceRecognitionEvent
     * @param messageToCast
     * @return
     */
    public EventBiometricRecognition castToEntity(BiometricFaceRecognitionEvent messageToCast) {
        EventBiometricRecognition event = null;
        Optional<Biometric> optionalBiometric = biometricRepo.findBySerial(messageToCast.getSerial());
        if (optionalBiometric.isPresent()){
            event = new EventBiometricRecognition();
            event.setBiometric(optionalBiometric.get());
            event.setCreated(messageToCast.getCreated());
            event.setExternalProfile(messageToCast.getExternalProfile());

            event.setFace(messageToCast.getFace());
            event.setRecognized(messageToCast.getRecognized());
            event.setType(EventBiometricRecognition.Type.RECOGNITION);
            event.setScore(messageToCast.getScore());
        }
        return event;
    }
    /**
     * в данном методе происходит подготовка сущности entity.App из сообщения AppList.App
     * @param messageToCast
     * @return
     */
    public App castToEntity(AppList.App messageToCast) {
        App app = null;
        Optional<App> optionalApp = appRepo.findByName(messageToCast.getName());
        if (optionalApp.isPresent()){
            app = optionalApp.get();
        }
        return app;
    }

    /**
     * в данном методе происходит подготовка сущности TerminalTelemetry из сообщения TerminalTelemetry
     * @param messageToCast
     * @return
     */
    public com.landau.core.terminals.entities.TerminalTelemetry castToEntity(TerminalTelemetry messageToCast) {
        com.landau.core.terminals.entities.TerminalTelemetry terminalTelemetry = null;
        Optional<Terminal> terminalOptional = terminalRepo.findBySerial(messageToCast.getSerial());
        if (terminalOptional.isPresent()){
            terminalTelemetry = new com.landau.core.terminals.entities.TerminalTelemetry();

            terminalTelemetry.setTerminal(terminalOptional.get());
            terminalTelemetry.setCpuLoad(messageToCast.getCpuLoad());
            terminalTelemetry.setDeviceConnected(messageToCast.getDeviceConnected());
            terminalTelemetry.setDeviceDisconnected(messageToCast.getDeviceDisconnected());
            terminalTelemetry.setGpuLad(messageToCast.getGpuLad());
        }
        return terminalTelemetry;
    }

    /**
     * в данном методе происходит подготовка сущности TerminalSyncStats из сообщения TerminalSyncStats
     * @param messageToCast
     * @return
     */
    public com.landau.core.stats.entity.TerminalSyncStats castToEntity(TerminalSyncStats messageToCast) {
        com.landau.core.stats.entity.TerminalSyncStats terminalSyncStats = null;
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(messageToCast.getSerial());
        if (optionalTerminal.isPresent()){
            terminalSyncStats = new com.landau.core.stats.entity.TerminalSyncStats();

            if (messageToCast.getDate() != null){
                terminalSyncStats.setDate(messageToCast.getDate());
            }
            terminalSyncStats.setSyncFiles(messageToCast.getSyncFiles());
            terminalSyncStats.setSyncFilesSize(messageToCast.getSyncFilesSize());
            terminalSyncStats.setTerminal(optionalTerminal.get());
        }
        return terminalSyncStats;
    }

    /**
     * в данном методе происходит подготовка сущности TerminalDevicesStats из сообщения TerminalDevicesStats
     * @param messageToCast
     * @return
     */
    public com.landau.core.stats.entity.TerminalDevicesStats castToEntity(TerminalDevicesStats messageToCast) {
        com.landau.core.stats.entity.TerminalDevicesStats terminalDevicesStats = null;
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(messageToCast.getSerial());
        if (optionalTerminal.isPresent()){
            terminalDevicesStats = new com.landau.core.stats.entity.TerminalDevicesStats();
            if (messageToCast.getDate() != null){
                terminalDevicesStats.setDate(messageToCast.getDate());
            }
            terminalDevicesStats.setExtracted(messageToCast.getExtracted());
            terminalDevicesStats.setInserted(messageToCast.getInserted());
            terminalDevicesStats.setTerminal(optionalTerminal.get());
        }
        return terminalDevicesStats;
    }

    /**
     * в данном методе происходит подготовка сущности TerminalSettings из сообщения TerminalSettings
     * @param messageToCast
     * @return
     */
    public com.landau.core.terminals.entities.TerminalSettings castToEntity(TerminalSettings messageToCast){
        com.landau.core.terminals.entities.TerminalSettings terminalSettings = null;
        Optional<Terminal> terminalOptional = terminalRepo.findBySerial(messageToCast.getSerial());
        if (terminalOptional.isPresent()){
            terminalSettings = new com.landau.core.terminals.entities.TerminalSettings();
            terminalSettings.setTerminal(terminalOptional.get());
            terminalSettings.setAuthTime(messageToCast.getAuthTime());
            terminalSettings.setBlockCount(messageToCast.getBlockCount());
            terminalSettings.setBlockTimer(messageToCast.getBlockTime());
            terminalSettings.setExternalPath(messageToCast.getExternalPath());
            terminalSettings.setFileStorageTime(messageToCast.getFileStorageTime());
            terminalSettings.setMinFreeSpace(messageToCast.getMinFreeSpace());
            terminalSettings.setOperationTime(messageToCast.getOperationTime());
            terminalSettings.setRewrite(messageToCast.getRewrite());
            terminalSettings.setSavePath(terminalSettings.getSavePath());
        }
        else {
            return null;
        }
        return terminalSettings;
    }

    /**
     * в данном методе происходит подготовка сущности TerminalProps из сообщения TerminalProps
     * @param messageToCast
     * @return
     */
    public Terminal castToEntity(TerminalProps messageToCast){
        Terminal terminal = terminalRepo.findBySerial(messageToCast.getSerial()).orElse(new Terminal());
        terminal.setOnline(true);

        if(terminal.getId() == null) {
            terminal.setName(messageToCast.getSerial());
        }

        terminal.setSerial(messageToCast.getSerial());
        terminal.setAgentVersion(messageToCast.getAgentVersion());
        terminal.setType(messageToCast.getType());
        terminal.setCapacity(messageToCast.getCapacity());

        return terminal;
    }

    /**
     * в данном методе происходит подготовка сущности WifiLog из сообщения WiFiTelemetry
     * @param messageToCast
     * @return
     */
    public DeviceWifiLog castToEntity(WiFiTelemetry messageToCast){
        DeviceWifiLog wifiLog = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            wifiLog = new DeviceWifiLog();
            wifiLog.setDevice(optionalDevice.get());
            wifiLog.setAdded(new Date());
            wifiLog.setMac(messageToCast.getMac());
            wifiLog.setSsid(messageToCast.getSsid());
            wifiLog.setFrequency(messageToCast.getFrequency());
            wifiLog.setLinkSpeed(messageToCast.getLinkSpeed());
            wifiLog.setSignal(messageToCast.getSignal());
        }

        return wifiLog;
    }

    /**
     * в данном методе происходит подготовка сущности DeviceTelemetry из сообщения DeviceTelemetry
     * @param messageToCast
     * @return
     */
    public DeviceTelemetry castToEntity(com.landau.objects.telemetry.DeviceTelemetry messageToCast){
        DeviceTelemetry deviceTelemetry = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            deviceTelemetry = new DeviceTelemetry();
            deviceTelemetry.setDevice(optionalDevice.get());
            if(messageToCast.getGsmType() != null) {
                deviceTelemetry.setGsmType(DeviceTelemetry.GsmType.valueOf(messageToCast.getGsmType().name()));
            }

            deviceTelemetry.setGsmSignal(messageToCast.getGsmSignal());
            // telemetry.setFaceTemplates(telemetry.getFaceTemplates());
            deviceTelemetry.setWatchPulse(messageToCast.getPulse());

            deviceTelemetry.setAllSpace(messageToCast.getAllSpace());
            deviceTelemetry.setHoldSpace(messageToCast.getHoldSpace());

            deviceTelemetry.setAllSpaceSd(messageToCast.getAllSpaceSd());
            deviceTelemetry.setHoldSpaceSd(messageToCast.getHoldSpaceSd());

            deviceTelemetry.setCpuLoad(messageToCast.getCpuLoad());
            deviceTelemetry.setGpuLoad(messageToCast.getGpuLoad());
            deviceTelemetry.setWatchCharge(messageToCast.getWatchCharge());
            deviceTelemetry.setDeviceCharge(messageToCast.getDeviceCharge());

            deviceTelemetry.setCreated(messageToCast.getCreated());
            deviceTelemetry.setAdded(new Date());

            deviceTelemetry.setIsExternalCameraOnline(messageToCast.getIsExternalCameraConnected());
            deviceTelemetry.setIsCharging(messageToCast.getIsCharging());
            deviceTelemetry.setIsWifiConnected(messageToCast.getIsWifiConnected());
            deviceTelemetry.setIsGsmConnected(messageToCast.getIsGsmConnected());
        }

        return deviceTelemetry;
    }

    /**
     * в данном методе происходит подготовка сущности Location из сообщения Location
     * @param messageToCast
     * @return
     */
    public Location castToEntity(com.landau.objects.telemetry.Location messageToCast){
        Location location = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            Device device = optionalDevice.get();
            location = new Location();
            location.setDevice(device);
            location.setLatitude(messageToCast.getLatitude());
            location.setLongitude(messageToCast.getLongitude());
            location.setSpeed(messageToCast.getSpeed());
            location.setAccuracy(messageToCast.getAccuracy());
            location.setBearing(messageToCast.getBearing());
            location.setCreated(messageToCast.getCreated());
            if (messageToCast.getType() != null){
                location.setType(Location.Type.valueOf(messageToCast.getType().name()));
            }
            location.setAltitude(messageToCast.getAltitude());

            device.setLocation(location);

            locationRepo.save(location);
            deviceRepo.save(device);
        }
        return location;
    }

    /**
     * в данном методе происходит подготовка сущности StatsBattery из сообщения DeviceBatteryStats
     * @param messageToCast
     * @return
     */
    public StatsDeviceBattery castToEntity(DeviceBatteryStats messageToCast){
        StatsDeviceBattery statsBattery = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            statsBattery = new StatsDeviceBattery();
            statsBattery.setDevice(optionalDevice.get());
            statsBattery.setFrom(messageToCast.getFrom());
            statsBattery.setTo(messageToCast.getTo());
            statsBattery.setPercent(messageToCast.getLoad());
            statsBattery.setCurrentAkbCharge(messageToCast.getCurrent());
        }
        return statsBattery;
    }

    /**
     * в данном методе происходит подготовка сущности StatsSpeedDistance из сообщения DeviceSpeedDistanceStats
     * @param messageToCast
     * @return
     */
    public StatsDeviceSpeedDistance castToEntity(DeviceSpeedDistanceStats messageToCast){
        StatsDeviceSpeedDistance statsSpeedDistance = null;
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        if(optionalDevice.isPresent()) {
            statsSpeedDistance = new StatsDeviceSpeedDistance();
            statsSpeedDistance.setDevice(optionalDevice.get());
            statsSpeedDistance.setDistance(messageToCast.getDistance());
            statsSpeedDistance.setSpeed(messageToCast.getSpeed());
            statsSpeedDistance.setFrom(messageToCast.getFrom());
            statsSpeedDistance.setTo(messageToCast.getTo());
        }
        return statsSpeedDistance;
    }

    /**
     * в данном методе происходит подготовка сущности Device из сообщения DeviceProps
     * @param messageToCast
     * @return
     */
    public Device castToEntity(DeviceProps messageToCast){
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());

        Device device = optionalDevice.orElse(new Device());

        if(optionalDevice.isPresent()) {
            device.setVersion(messageToCast.getAgentVersion());
            device.setSimNumber(messageToCast.getSimNumber());
            device.setSimSerial(messageToCast.getSimSerial());
        }
        else {
            device.setSerial(messageToCast.getSerial());
            device.setName(messageToCast.getSerial());
            device.setPulse(0);
            device.setCharge(0);
            device.setCreated(new Date());
        }

        device.setVersion(messageToCast.getAgentVersion());
        device.setSimNumber(messageToCast.getSimNumber());
        device.setSimSerial(messageToCast.getSimSerial());

        if(messageToCast.getManufacturer() != null && messageToCast.getModel() != null && messageToCast.getFirmware() != null) {
            Optional<DeviceModel> optionalDeviceModel = deviceModelRepo.findFirstByManufacturerAndModelAndFirmware(
                    messageToCast.getManufacturer(),
                    messageToCast.getModel(),
                    messageToCast.getFirmware()
            );

            if(optionalDeviceModel.isPresent()) {
                device.setModel(optionalDeviceModel.get());
            }
            else {
                DeviceModel deviceModel = new DeviceModel();
                deviceModel.setBatteryCapacity(0);
                deviceModel.setManufacturer(messageToCast.getManufacturer());
                deviceModel.setFirmware(messageToCast.getFirmware());
                deviceModel.setModel(messageToCast.getModel());

                device.setModel(deviceModelRepo.save(deviceModel));
            }
        }

        return device;
    }

    /**
     * в данном методе происходит подготовка сущности DeviceSettings из сообщения DeviceSettings
     * @param messageToCast
     * @return
     */
    public com.landau.core.devices.entities.DeviceSettings castToEntity(DeviceSettings messageToCast){
        com.landau.core.devices.entities.DeviceSettings entity = new com.landau.core.devices.entities.DeviceSettings();

        if (messageToCast.getStreamResolution() != null){
            entity.setStreamResolution(com.landau.core.devices.entities.DeviceSettings.Resolution.valueOf(messageToCast.getStreamResolution().name()));
        }
        if (messageToCast.getRecordResolution() != null){
            entity.setRecordResolution(com.landau.core.devices.entities.DeviceSettings.Resolution.valueOf(messageToCast.getRecordResolution().name()));
        }
        if (messageToCast.getNotification() != null){
            entity.setNotification(com.landau.core.devices.entities.DeviceSettings.Notification.valueOf(messageToCast.getNotification().name()));
        }
        if (messageToCast.getDefaultVolume() != null){
            entity.setDefaultVolume(com.landau.core.devices.entities.DeviceSettings.DefaultVolume.valueOf(messageToCast.getDefaultVolume().name()));
        }
        if (messageToCast.getDuration() != null){
            entity.setDuration(com.landau.core.devices.entities.DeviceSettings.Duration.valueOf(messageToCast.getDuration().name()));
        }
        if (messageToCast.getRecordEncoder() != null){
            entity.setRecordEncoder(com.landau.core.devices.entities.DeviceSettings.RecordEncoder.valueOf(messageToCast.getRecordEncoder().name()));
        }
        if (messageToCast.getRotateBitmapFromRecorder() != null){
            entity.setRotateBitmapFromRecorder(com.landau.core.devices.entities.DeviceSettings.RotateBitmap.valueOf(messageToCast.getRotateBitmapFromRecorder().name()));
        }
        if (messageToCast.getRotateBitmapFromStreamer() != null){
            entity.setRotateBitmapFromStreamer(com.landau.core.devices.entities.DeviceSettings.RotateBitmap.valueOf(messageToCast.getRotateBitmapFromStreamer().name()));
        }
        if (messageToCast.getStreamInterval() != null){
            entity.setStreamInterval(com.landau.core.devices.entities.DeviceSettings.StreamInterval.valueOf(messageToCast.getStreamInterval().name()));
        }

        entity.setRecord(messageToCast.getRecord());
        entity.setVertical(messageToCast.getVertical());
        entity.setRotateMatrix(messageToCast.getRotateMatrix());
        entity.setUpdated(new Date());
        entity.setRewrite(messageToCast.getRewrite());
        entity.setCoreUrl(messageToCast.getCoreUrl());
        entity.setTerminalUrl(messageToCast.getTerminalUrl());
        entity.setWifiUrl(messageToCast.getWifiUrl());

        return entity;
    }

    /**
     * в данном методе происходит подготовка сущности ChatMessage из сообщения ChatMessage
     * @param messageToCast
     * @return
     */
    public ChatMessage castToEntity(com.landau.objects.data.ChatMessage messageToCast){
        ChatMessage chatMessage = new ChatMessage();
        Optional<Device> optionalDevice = deviceRepo.findBySerial(messageToCast.getSerial());
        if (optionalDevice.isPresent()){
            Optional<User> optionalUser = userRepo.findByDevice(optionalDevice.get());
            optionalUser.ifPresent(chatMessage::setSender);
        }
        if (messageToCast.getType() != null){
            chatMessage.setType(ChatMessage.Type.valueOf(messageToCast.getType().name()));
        }
        if (messageToCast.getBody() != null){
            chatMessage.setBody(messageToCast.getBody());
        }
        if (messageToCast.getCreated() != null){
            chatMessage.setCreated(messageToCast.getCreated());
        }
        return chatMessage;
    }


    /**
     * в данном методе происходит подготовка сущности BiometricTelemetry из сообщения BiometricTelemetry
     * @param messageToCast
     * @return
     */
    public BiometricTelemetry castToEntity(com.landau.objects.telemetry.BiometricTelemetry messageToCast) {
        BiometricTelemetry biometricTelemetry = null;
        Optional<Biometric> optionalBiometric = biometricRepo.findBySerial(messageToCast.getSerial());

        if (optionalBiometric.isPresent()){
            biometricTelemetry = new BiometricTelemetry();
            biometricTelemetry.setBiometric(optionalBiometric.get());
            biometricTelemetry.setCpuLoad(messageToCast.getCpuLoad());
            biometricTelemetry.setGpuLad(messageToCast.getGpuLad());
        }

        return biometricTelemetry;
    }

    /**
     * в данном методе происходит подготовка сущности BiometricSettings из сообщения BiometricSettings
     * @param messageToCast
     * @return
     */
    public BiometricSettings castToEntity(com.landau.objects.settings.BiometricSettings messageToCast) {
        BiometricSettings biometricSettings = null;
        Optional<Biometric> optionalBiometric = biometricRepo.findBySerial(messageToCast.getSerial());
        if (optionalBiometric.isPresent()){
            biometricSettings = new BiometricSettings();
            if (messageToCast.getMode() != null){
                biometricSettings.setMode(BiometricSettings.Mode.valueOf(messageToCast.getMode().name()));
            }
            biometricSettings.setBiometric(optionalBiometric.get());
        }
        return biometricSettings;
    }

    /**
     * в данном методе происходит подготовка сущности BiometricProps из сообщения BiometricProps
     * @param messageToCast
     * @return
     */
    public BiometricProps castToEntity(com.landau.objects.props.BiometricProps messageToCast){
        BiometricProps biometricProps = null;
        Optional<Biometric> biometricOptional = biometricRepo.findBySerial(messageToCast.getSerial());
        if (biometricOptional.isPresent()){
            biometricProps = new BiometricProps();
            biometricProps.setAgentVersion(messageToCast.getAgentVersion());
            biometricProps.setBiometric(biometricOptional.get());
            biometricProps.setFirmware(messageToCast.getFirmware());
            biometricProps.setVendor(messageToCast.getVendor());
        }
        return biometricProps;
    }

    /**
     * в данном методе происходит подготовка сущности ChatMessage из сообщения CreateChatMessage
     * @param messageToConsume
     * @return
     */
    public ChatMessage castToEntity(CreateChatMessage messageToConsume, User sender, User destination) {
        ChatMessage chatMessage = new ChatMessage();

        chatMessage.setSender(sender);
        chatMessage.setDestination(destination);

        chatMessage.setBody(messageToConsume.getBody());
        chatMessage.setCreated(messageToConsume.getCreated());

        if (messageToConsume.getType() != null){
            chatMessage.setType(ChatMessage.Type.valueOf(messageToConsume.getType().name()));
        }

        if (chatRoomRepo.findById(messageToConsume.getGroup()).isPresent()){
            chatMessage.setChatRoom(chatRoomRepo.findById(messageToConsume.getGroup()).get());
        }


        return chatMessage;
    }

    /**
     * в данном методе происходит подготовка сообщения SetDeviceSettings для дальнейшей отправки
     * @param entityToCast
     * @return
     */
    public SetDeviceSettings castToMessage(com.landau.core.devices.entities.DeviceSettings entityToCast){
        SetDeviceSettings setDeviceSettings = new SetDeviceSettings();

        if (entityToCast.getStreamResolution() != null){
            setDeviceSettings.setStreamResolution(SetDeviceSettings.Resolution.valueOf(entityToCast.getStreamResolution().name()));
        }
        if (entityToCast.getRecordResolution() != null){
            setDeviceSettings.setRecordResolution(SetDeviceSettings.Resolution.valueOf(entityToCast.getRecordResolution().name()));
        }
        if (entityToCast.getNotification() != null){
            setDeviceSettings.setNotification(SetDeviceSettings.Notification.valueOf(entityToCast.getNotification().name()));
        }
        if (entityToCast.getDefaultVolume() != null){
            setDeviceSettings.setDefaultVolume(SetDeviceSettings.DefaultVolume.valueOf(entityToCast.getDefaultVolume().name()));
        }
        if (entityToCast.getDuration() != null){
            setDeviceSettings.setDuration(SetDeviceSettings.Duration.valueOf(entityToCast.getDuration().name()));
        }
        if (entityToCast.getRecordEncoder() != null){
            setDeviceSettings.setRecordEncoder(SetDeviceSettings.RecordEncoder.valueOf(entityToCast.getRecordEncoder().name()));
        }
        if (entityToCast.getRotateBitmapFromRecorder() != null){
            setDeviceSettings.setRotateBitmapFromRecorder(SetDeviceSettings.RotateBitmap.valueOf(entityToCast.getRotateBitmapFromRecorder().name()));
        }
        if (entityToCast.getRotateBitmapFromStreamer() != null){
            setDeviceSettings.setRotateBitmapFromStreamer(SetDeviceSettings.RotateBitmap.valueOf(entityToCast.getRotateBitmapFromStreamer().name()));
        }
        if (entityToCast.getStreamInterval() != null){
            setDeviceSettings.setStreamInterval(SetDeviceSettings.StreamInterval.valueOf(entityToCast.getStreamInterval().name()));
        }

        setDeviceSettings.setSerial(entityToCast.getDevice().getSerial());
        setDeviceSettings.setRecord(entityToCast.getRecord());
        setDeviceSettings.setVertical(entityToCast.getVertical());
        setDeviceSettings.setRotateMatrix(entityToCast.getRotateMatrix());
        setDeviceSettings.setUpdated(entityToCast.getUpdated());
        setDeviceSettings.setRewrite(entityToCast.getRewrite());
        setDeviceSettings.setCoreUrl(entityToCast.getCoreUrl());
        setDeviceSettings.setTerminalUrl(entityToCast.getTerminalUrl());
        setDeviceSettings.setWifiUrl(entityToCast.getWifiUrl());
        return setDeviceSettings;
    }


    /**
     * в данном методе происходит подготовка сообщения SetTerminalSettings для дальнейшей отправки
     * @param entityToCast
     * @return
     */
    public SetTerminalSettings castToMessage(com.landau.core.terminals.entities.TerminalSettings entityToCast){
        SetTerminalSettings setTerminalSettings = new SetTerminalSettings();

        setTerminalSettings.setAuthTime(entityToCast.getAuthTime());
        setTerminalSettings.setBlockCount(entityToCast.getBlockCount());
        setTerminalSettings.setBlockTime(entityToCast.getBlockTimer());
        setTerminalSettings.setExternalPath(entityToCast.getExternalPath());
        setTerminalSettings.setFileStorageTime(entityToCast.getFileStorageTime());
        setTerminalSettings.setMinFreeSpace(entityToCast.getMinFreeSpace());
        setTerminalSettings.setOperationTime(entityToCast.getOperationTime());
        setTerminalSettings.setRewrite(entityToCast.getRewrite());
        setTerminalSettings.setSavePath(entityToCast.getSavePath());

        return setTerminalSettings;
    }

    /**
     * в данном методе происходит подготовка сообщения ChatMessage для дальнейшей отправки
     * @param entityToCast
     * @return
     */
    public com.landau.objects.data.ChatMessage castToMessage(ChatMessage entityToCast){
        com.landau.objects.data.ChatMessage chatMessage = new com.landau.objects.data.ChatMessage();
        if (entityToCast.getType() != null){
            chatMessage.setType(com.landau.objects.data.ChatMessage.Type.valueOf(entityToCast.getType().name()));
        }
        if (entityToCast.getBody() != null){
            chatMessage.setBody(entityToCast.getBody());
        }
        if (entityToCast.getCreated() != null){
            chatMessage.setCreated(entityToCast.getCreated());
        }
        if (entityToCast.getChatRoom() != null){
            chatMessage.setGroup(entityToCast.getChatRoom().getId());
        }
        if (entityToCast.getSender() != null){
            chatMessage.setSender(entityToCast.getSender().getId());
            chatMessage.setSenderData(entityToCast.getSender().getLastName() + " "
                    + entityToCast.getSender().getFirstName() + " " + entityToCast.getSender().getLastName());
        }

        if (entityToCast.getDestination() != null)
        chatMessage.setSerial(entityToCast.getDestination().getDevice().getSerial());

        return chatMessage;
    }

    /**
     * в данном методе происходит подготовка сообщения ChatMessageEvent для дальнейшей отправки
     * @param entityToCast
     * @return
     */
    public ChatMessageEvent castToEvent(ChatMessage entityToCast){
        ChatMessageEvent chatMessageEvent = new ChatMessageEvent();
        chatMessageEvent.setCreated(entityToCast.getCreated());
        chatMessageEvent.setMessage(castToMessage(entityToCast));

        return chatMessageEvent;
    }

    /**
     * в данном методе происходит подготовка сообщения ChatMessageEvent для дальнейшей отправки
     * @param entityToCast
     * @return
     */
    public ChatGroupEvent castToEvent(ChatRoom entityToCast){
        ChatGroupEvent chatGroupEvent = new ChatGroupEvent();
        chatGroupEvent.setCreated(new Date());
        chatGroupEvent.setGroup((entityToCast.getId().intValue()));

        if (entityToCast.getMembers() != null){
            Integer[] members = new Integer[entityToCast.getMembers().size()];
            for (int i = 0; i < entityToCast.getMembers().size(); i++){
                members[i] = entityToCast.getMembers().get(i).getId().intValue();
            }

            chatGroupEvent.setMembers(members);
        }

        return chatGroupEvent;
    }

    /**
     * в данном методе происходит подготовка сообщения Task для дальнейшей отправки
     * @param entityToCast
     * @return
     */
    public com.landau.objects.data.Task castToMessage(com.landau.core.tasks.entities.Task entityToCast){
        com.landau.objects.data.Task taskMessage = new com.landau.objects.data.Task();

        if (entityToCast.getImage() != null){
            taskMessage.setImage(entityToCast.getImage());
        }
        if (entityToCast.getContent() != null){
            taskMessage.setContent(entityToCast.getContent());
        }

        return taskMessage;
    }

    public User castToEntity(Profile message) {
        //TODO добавить castToEntity Profile
        return null;
    }

}
