package com.landau.core.common.entities;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class AbstractLocation extends AbstractId {
    @Column
    @ApiModelProperty(value = "Широта")
    Double latitude;

    @Column
    @ApiModelProperty(value = "Долгота")
    Double longitude;

    @Column
    @ApiModelProperty(value = "Дата")
    private Date created;

    @Column
    @ApiModelProperty(value = "Высота", dataType = "double")
    private Double altitude;

    @Column
    @ApiModelProperty(value = "Азимут")
    private Float bearing;

    @Column
    @ApiModelProperty(value = "Погрешность")
    private Float accuracy;
}
