package com.landau.core.common.entities;

import com.landau.core.companies.entities.Company;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public abstract class AbstractBusiness extends AbstractId {
    @ManyToOne
    @ApiModelProperty(value = "Компания")
    private Company company;

    /**
     * Компания
     * @return
     */
    public final Company getCompany() {
        return company;
    }

    public final void setCompany(Company company) {
        this.company = company;
    }
}
