package com.landau.core.common.entities;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public abstract class AbstractBusinessSerial extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Серийный номер устройства")
    private String serial;

    @Column
    @ApiModelProperty(value = "Статус устройства (онлайн/оффлайн)")
    private Boolean online;

    @Column
    @ApiModelProperty(value = "Дата последнего 'онлайна' устройства")
    private Date lastOnline;

    final public String getSerial() {
        return serial;
    }

    final public void setSerial(String serial) {
        this.serial = serial;
    }

    final public Boolean getOnline() {
        return online;
    }

    final public void setOnline(Boolean online) {
        this.online = online;
    }

    final public Date getLastOnline() {
        return lastOnline;
    }

    final public void setLastOnline(Date lastOnline) {
        this.lastOnline = lastOnline;
    }
}
