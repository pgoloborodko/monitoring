package com.landau.core.common;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageProcessor {
    public static final int MAX_SIZE = 800;

    public BufferedImage resizeImage(BufferedImage bufferedImage) {
        BufferedImage scaledImage = null;
        int iw = bufferedImage.getWidth();
        int ih = bufferedImage.getHeight();
        //масштабируем меньшую сторону
        if (iw < ih && iw > MAX_SIZE) {
            float scale= (float) MAX_SIZE / iw;
//            scaledImage = bufferedImage.getScaledInstance(MAX_SIZE, (int)(ih * scale), Image.SCALE_SMOOTH);
            scaledImage = scaleImage(bufferedImage, MAX_SIZE, (int)(ih * scale));
        } else if (ih < iw && ih > MAX_SIZE) {
            float scale= (float) MAX_SIZE / ih;
            scaledImage = scaleImage(bufferedImage, (int)(iw * scale), MAX_SIZE);
        }
        if (scaledImage != null) {
            bufferedImage = scaledImage;//((ToolkitImage) scaledImage).getBufferedImage();
        }
        //обрезаем большую сторону до maxSize
        if (bufferedImage.getWidth() > MAX_SIZE) {
            bufferedImage = bufferedImage.getSubimage(0, 0, MAX_SIZE, bufferedImage.getHeight());
        } else if (bufferedImage.getHeight() > MAX_SIZE) {
            bufferedImage = bufferedImage.getSubimage(0, 0, bufferedImage.getWidth(), MAX_SIZE);
        }
        bufferedImage = drawFields(bufferedImage);
        return bufferedImage;
    }

    private BufferedImage scaleImage(BufferedImage bufferedImage, int width, int height) {
//            scaledImage = bufferedImage.getScaledInstance((int)(iw * scale), MAX_SIZE, Image.SCALE_SMOOTH);
        BufferedImage scaledImage = new BufferedImage(width, height, bufferedImage.getType());
        Graphics2D g = scaledImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, width, height, 0, 0, bufferedImage.getWidth(),
                bufferedImage.getHeight(), null);
        g.dispose();
        return scaledImage;
    }

    //добавляем черные поля по краям
    private BufferedImage drawFields(BufferedImage bufferedImage) {
        int shiftX = 0, shiftY = 0;
        if (bufferedImage.getWidth() < MAX_SIZE) {
            shiftX = (MAX_SIZE - bufferedImage.getWidth()) / 2;
        }
        if (bufferedImage.getHeight() < MAX_SIZE) {
            shiftY = (MAX_SIZE - bufferedImage.getHeight()) / 2;
        }
        if (shiftX == 0 && shiftY == 0) return bufferedImage;
        BufferedImage image = new BufferedImage(MAX_SIZE, MAX_SIZE, bufferedImage.getType());
        Graphics g = image.getGraphics();
        g.setColor(Color.black);
        g.drawRect(0, 0, MAX_SIZE, MAX_SIZE);
        g.drawImage(bufferedImage, shiftX, shiftY, null);
        return image;
    }

}
