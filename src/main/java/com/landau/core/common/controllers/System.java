package com.landau.core.common.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class System extends Base {
    @Value("${app.version}")
    private String version;

    @GetMapping
    public String get() {
        return "{\"version\":\"" + version + "\"}";
    }
}
