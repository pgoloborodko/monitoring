package com.landau.core.common.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/")
public class MainPage extends Base {
    @Value("${server.index}")
    private Resource indexFile;

    @GetMapping
    public ResponseEntity get() throws IOException {
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setContentLength(indexFile.contentLength());
        return new ResponseEntity<>(indexFile, httpHeaders, HttpStatus.OK);
    }
}
