package com.landau.core.common.controllers;

import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.users.entities.Role;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.RoleRepo;
import com.landau.core.users.repos.UserRepo;
import com.landau.core.users.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

/**
 * Базовый набор опций для контроллеров
 */
@Slf4j
public abstract class Base {
    @Autowired
    protected UserService userService;

    @Autowired
    protected RoleRepo roleRepo;

    @Autowired
    protected UserRepo userRepo;

    @Autowired
    protected Environment env;

    @Autowired
    protected ModelMapper mapper;

    @Autowired
    protected DeviceRepo deviceRepo;

    protected User getUser() {
        return userRepo.findById((Long) SecurityContextHolder.getContext().getAuthentication().getDetails()).get();
    }



    @ModelAttribute
    protected Role getRole(@RequestParam(value = "id", required = false) Long role_id) {
        if (role_id == null) {
            return new Role();
        }
        Optional<Role> find = roleRepo.findById(role_id);
        return find.orElse(new Role());
    }

    @ModelAttribute
    protected User getUser(@RequestParam(value = "id", required = false) Long user_id) {
        if (user_id == null) {
            return new User();
        }
        Optional<User> find = userRepo.findById(user_id);
        return find.orElse(new User());
    }

    /**
     * Получение списка ролей
     *
     * @return список ролей (com.landau.core.users.entities.Role)
     */
    @ModelAttribute("roles")
    protected List<Role> getRoles() {
        return roleRepo.findAll();
    }

    public ModelMapper getMapper() {
        return mapper;
    }


    protected <D> D map(Object source, Class<D> destinationType) {
        return this.getMapper().map(source, destinationType);
    }
}
