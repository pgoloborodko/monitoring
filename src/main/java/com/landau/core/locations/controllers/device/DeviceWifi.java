package com.landau.core.locations.controllers.device;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceWifiLog;
import com.landau.core.devices.repos.DeviceWifiLogRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Получение данных Wi-Fi соединения
 */
@Api(tags = "Wi-Fi соединение", description = "Контроллер обеспечивает получение данных wi-fi соединения устройства")
@RestController("devicesWifi")
@RequestMapping("locations/devices/wifi")
public class DeviceWifi extends Base {
    private DeviceWifiLogRepo deviceWifiLogRepo;

    public DeviceWifi(DeviceWifiLogRepo deviceWifiLogRepo) {
        this.deviceWifiLogRepo = deviceWifiLogRepo;
    }

    /**
     * Получение последней wi-fi (mac) локации регистратора
     *
     * @param device регистратор (идентификатор)
     * @return {@link DeviceWifiLog}
     */
    @ApiOperation(value = "Получение последней wi-fi (mac) локации регистратора", response = DeviceWifiLog.class)
    @Authority(Device.class)
    @GetMapping("one")
    public DeviceWifiLog one(Device device) {

        return deviceWifiLogRepo.findFirstByDeviceOrderByIdDesc(device);
    }
}
