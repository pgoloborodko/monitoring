package com.landau.core.locations.controllers.camera;

import com.landau.core.locations.entities.CameraLocation;
import com.landau.core.locations.filters.CameraLocationFilter;
import com.landau.core.locations.repos.CameraLocationRepo;
import com.landau.core.locations.specs.CameraLocationSpec;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController("CameraLocation")
@RequestMapping("/locations/camera")
@Api(tags = "Сохранение/Изменение местоположения камеры",
        description = "контроллер обеспечивает сохранение и обновление данных о местоположении камеры")
public class CameraLocationController {
    private CameraLocationRepo cameraLocationRepo;

    @Autowired
    public CameraLocationController(CameraLocationRepo cameraLocationRepo){
        this.cameraLocationRepo = cameraLocationRepo;
    }

    @ApiOperation(value = "Сохранение/Изменение данных о местоположении камеры", response = CameraLocation.class)
    @PostMapping("/save")
    public CameraLocation saveCameraLocation(@RequestBody CameraLocation cameraLocation){
        if (cameraLocation.getLatitude() != null && cameraLocation.getLongitude() != null){
            Coordinate coordinate = new Coordinate(cameraLocation.getLatitude(), cameraLocation.getLongitude());
            cameraLocation.setGeom(new GeometryFactory().createPoint(coordinate));
            return cameraLocationRepo.save(cameraLocation);
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "wrong coordinates");
    }

    @ApiOperation(value = "Список записей о местоположении камер", response = CameraLocation.class)
    @GetMapping("/list")
    public Page<CameraLocation> getCameraLocationsList(CameraLocationFilter cameraLocationFilter, Pageable pageable){
        return cameraLocationRepo.findAll(CameraLocationSpec.find(cameraLocationFilter), pageable);
    }
}
