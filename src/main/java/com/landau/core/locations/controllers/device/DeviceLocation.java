package com.landau.core.locations.controllers.device;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.devices.entities.Device;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.*;

@Api(tags = "Список локаций для устройства", description = "Контроллер обеспечивает получение списка локаций устройств")
@RestController
@RequestMapping("/locations/device")
public class DeviceLocation {
    @Autowired
    LocationRepo locationRepo;

    @GetMapping("date")
    @Authority(Device.class)
    public List<Location> byDate(@RequestParam(value = "device") Device device, @RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        Date end = c.getTime();

        List<Object[]> points = locationRepo.findByCreatedDateBetweenAndDevice(date, end, device);
        List track = new ArrayList();
        for (Object[] point: points) {
            Location l = new Location();
            l.setId(((BigInteger)point[0]).longValue());
            l.setLatitude((Double)point[2]);
            l.setLongitude((Double)point[3]);
            l.setCreated((Date)point[4]);
            track.add(l);
        }
        return track;
    }

    @GetMapping("last")
    @Authority(Device.class)
    public Optional<Location> byLast(@RequestParam(value = "device") Device device) {
        return locationRepo.findLastByDevice(device);
    }
}
