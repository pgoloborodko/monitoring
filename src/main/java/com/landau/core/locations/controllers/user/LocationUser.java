package com.landau.core.locations.controllers.user;


import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.locations.services.LocationsService;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Получение списка локаций регистраторов
 */
@Api(tags = "Список локаций пользователя", description = "Контроллер обеспечивает получение списка локаций пользователя")
@RestController()
@RequestMapping("/locations/user")
public class LocationUser extends Base {
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    @Autowired
    private LocationRepo locationRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private LocationsService locationsService;


    /**
     * Получение последней локации регистратора
     *
     * @param device регистратор (идентификатор)
     * @return {@link Location}
     */
    @ApiOperation(value = "Получение последней локации регистратора")
    @GetMapping("one")
    @Authority(Device.class)
    public Location location(Device device) {
        return locationRepo.findFirstByDeviceOrderByIdDesc(device);
    }

    /**
     * Получение последней локации для нескольких регистраторов
     *
     * @param devices список регистраторов (идентификаторы)
     * @return List<{@link Location}>
     */
    @ApiOperation(value = "Получение последней локации для нескольких регистраторов")
    @GetMapping("oneList")
    @Authority(value = Device.class)
    public List<Location> location(
            @RequestParam(value = "device") List<Device> devices,
            @RequestParam(value = "date") @DateTimeFormat(pattern = DATE_PATTERN) Date start) {
        List<Location> locations = locationsService.getLocations(devices, start, getUser());

        return locations;
    }

    /**
     * Получение локаций регистратора по дате за 1 день
     *
     * @param user пользователь
     * @param date дата вида yyyy/MM/dd
     * @return List<{@link Location}>
     */
    @ApiOperation(value = "Получение локаций пользователя по дате за 1 день")
    @GetMapping("date")
    @Authority(value = User.class, required = true)
    public List<Location> date(@RequestParam(value = "user") User user,
                               @RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {

            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, 1);
            Date end = c.getTime();

        List<Object[]> points = locationRepo.findByCreatedDateBetweenAndUser(date, end, user);
        List track = new ArrayList();

        for (Object[] point: points) {
            Location l = new Location();
            l.setId(((BigInteger)point[0]).longValue());
            l.setLatitude((Double)point[1]);
            l.setLongitude((Double)point[2]);
            l.setCreated((Date)point[3]);
            l.setSpeed((Short) point[4]);
            l.setBearing((Float) point[5]);
            track.add(l);
        }

        return track;
    }

    @GetMapping("last")
    @Authority(value = User.class, required = true)
    public Location byLast(@RequestParam(value = "user") User user) {
        Date date = new Date();
        date.setSeconds(0);
        date.setHours(0);
        date.setMinutes(0);

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        Date end = c.getTime();

        List<Location> locations =  locationRepo.findLastByUserAndCreatedBetweenOrderByCreatedDesc(user, date, end);

        if(locations.size() == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "location not found");
        }
        else {
            return locations.get(0);
        }
    }

    @ApiOperation(value = "Получение локаций пользователя по дате за 1 день для ЕЦХД")
    @Authority(value = User.class, required = true)
    @GetMapping("date/sdc")
    public String toSDC(@RequestParam(value = "user") User user, @RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        List<Location> locations = this.date(user, date);
        List<String> sdcLocations = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("HHmmss.000");

        StringBuilder builder = new StringBuilder();

        for(Location location : locations) {
            builder.append("imei:0000," +
                    "tracker," + dateFormat.format(location.getCreated()) +",00000000000,F," +
                    dateFormat2.format(location.getCreated()) + "," +
                    (location.getType() == Location.Type.GSM ? "V" : "A") + "," +
                    location.getLatitude() + ",N," +
                    location.getLongitude() + ",E," +
                    location.getSpeed() + "," +
                    location.getBearing() + ";\n");
        }

        return builder.toString();
    }
}
