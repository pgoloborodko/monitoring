package com.landau.core.locations.specs;

import com.landau.core.locations.entities.CameraLocation;
import com.landau.core.locations.filters.CameraLocationFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class CameraLocationSpec {
    public static Specification<CameraLocation> find(CameraLocationFilter cameraLocationFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (cameraLocationFilter.getCreated() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(cameraLocationFilter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(cameraLocationFilter.getCreated());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (cameraLocationFilter.getRemoteCamera() != null){
                predicates.add(criteriaBuilder.equal(root.get("remoteCamera"), cameraLocationFilter.getRemoteCamera()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
