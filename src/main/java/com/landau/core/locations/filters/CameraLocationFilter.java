package com.landau.core.locations.filters;

import com.landau.core.cameras.entities.RemoteCamera;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(description = "Фильтр поиска местоположения камеры")
public class CameraLocationFilter {
    @ApiModelProperty(value = "Дата создания записи о местоположении камеры в БД")
    private Date created;

    @ApiModelProperty(value = "Сущность записи о камере в БД")
    private RemoteCamera remoteCamera;
}
