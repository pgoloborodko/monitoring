package com.landau.core.locations.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.landau.core.cameras.entities.RemoteCamera;
import com.landau.core.common.entities.AbstractLocation;
import com.vividsolutions.jts.geom.Point;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Entity
@Table
@ApiModel(description = "Сущность локации камеры")
public class CameraLocation extends AbstractLocation {
    @ManyToOne
    @ApiModelProperty(value = "Сущность камеры")
    private RemoteCamera remoteCamera;

    @JsonIgnore
    @Transient
    @ApiParam(value = "", hidden = true)
    private Point geom;
}
