package com.landau.core.locations.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractLocation;
import com.landau.core.devices.entities.Device;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Локация
 */
@Entity
@Table(name = "location", indexes = {@Index(columnList = "device_id", name = "location_device_index")})
@JsonInclude
@Getter
@Setter
@Repo(LocationRepo.class)
@ApiModel(description = "Сущность локации устройства")
public class Location extends AbstractLocation {

    /**
     * Тип соединения
     */
    public enum Type {
        /**
         * Gsm
         */
        GSM,
        /**
         * GPS
         */
        GPS
    }

    @Column
    @ApiModelProperty(value = "Скорость")
    private Short speed;

    @Column
    @ApiModelProperty(value = "Тип соединения")
    private Type type;

    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @ApiModelProperty(value = "Устройство")
    private Device device;

    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @ApiModelProperty(value = "Пользователь")
    private User user;

    @Column
    @ApiModelProperty(value = "Пульс")
    private Integer pulse;


    //@Column(name = "point", nullable = false, columnDefinition = "geometry(Point, 4326)")
    //private Point point;



}
