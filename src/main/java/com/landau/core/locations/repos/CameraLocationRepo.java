package com.landau.core.locations.repos;

import com.landau.core.locations.entities.CameraLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CameraLocationRepo extends JpaRepository<CameraLocation, Long>, JpaSpecificationExecutor<CameraLocation> {
}
