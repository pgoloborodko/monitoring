package com.landau.core.locations.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.locations.entities.Location;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface LocationRepo extends JpaRepository<Location, Long> {
    @Query("select d from Location d where d.created >= :from AND d.created <= :to AND device = :device ")
    List<Location> findByCreatedDateBetweenAndDevice(Pageable pageable, @Param("from") Date start, @Param("to") Date end, @Param("device") Device device);


    List<Location> findAllByDeviceAndCreatedBetween(Pageable pageable, @Param("device") Device device, @Param("from") Date start,
                                               @Param("to") Date end);

    Location findFirstByDeviceOrderByIdDesc(Device device);

    List<Location> findAllByCreatedBetweenAndDeviceIn(Date start, Date end, List<Device> device);
    @Query(value = "select l.* from (select device_id, max(created) from location where created > :start and created < " +
            ":end and device_id in (:device) group by device_id) t JOIN location l ON l.device_id = t.device_id and t" +
            ".max = l" +
            ".created;",
            nativeQuery = true)
    List<Location> findLastLocations(@Param("start") Date start, @Param("end") Date end,
                                     @Param("device") List<Device> device);

    @Query(value = "select id, device_id, latitude, longitude, created from Location a inner join\n" +
            "(select st_m((result.points).geom) as m from (\n" +
            "select st_dumppoints(simplified_path.simplifiedLine) as points from (\n" +
            "select st_simplify(st_makeline(path.point), 0.0001) as simplifiedLine from (\n" +
            "select st_setsrid(st_makepointm(latitude, longitude, id), 4326) as point from\n" +
            "location where device_id=:device and created >= :from and created <= :to\n" +
            ") as path\n" +
            ") simplified_path\n" +
            ") result) b on a.id = b.m", nativeQuery = true)
    List findByCreatedDateBetweenAndDevice(@Param("from") Date start, @Param("to") Date end, @Param("device") Device device);

    @Query(value = "select id, latitude, longitude, created, speed, bearing from Location a inner join\n" +
            "(select st_m((result.points).geom) as m from (\n" +
            "select st_dumppoints(simplified_path.simplifiedLine) as points from (\n" +
            "select st_simplify(st_makeline(path.point), 0.0001) as simplifiedLine from (\n" +
            "select st_setsrid(st_makepointm(latitude, longitude, id), 4326) as point from\n" +
            "location where user_id =:user and created >= :from and created <= :to\n" +
            ") as path\n" +
            ") simplified_path\n" +
            ") result) b on a.id = b.m", nativeQuery = true)
    List findByCreatedDateBetweenAndUser(@Param("from") Date start, @Param("to") Date end, @Param("user") User user);

    List<Location> findFirst10ByDeviceOrderByIdDesc(Device device);
    @Query("select d from Location d where d.created >= :from AND d.created <= :to AND device = :device ")
    List<Location> findByCreatedDateBetweenAndDeviceOrderByIdDesc(@Param("from")Date start, @Param("to")Date end, @Param("device")Device device);
    Optional<Location> findLastByDevice(Device device);


    Optional<Location> findByCreated(Date date);

    Optional<Location> findLastByUser(User user);

    List<Location> findLastByUserAndCreatedBetweenOrderByCreatedDesc(User user, Date created_date, Date created_date2);

    /**
     *  Метод для тестов
     * @param serial
     * @return
     */
    Optional<Location> findByDevice_Serial(String serial);
}
