package com.landau.core.locations.services;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.users.entities.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Belov
 */
@Service
@Slf4j
public class LocationsService {

    private LocationRepo locationRepo;
    private DeviceRepo deviceRepo;

    @Autowired
    public void setDeviceRepo(DeviceRepo deviceRepo) {
        this.deviceRepo = deviceRepo;
    }
    @Autowired
    public void setLocationRepo(LocationRepo locationRepo) {
        this.locationRepo = locationRepo;
    }
    public List<Location> getLocations(List<Device> devices, Date start, User user) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(start);
        startCal.set(Calendar.HOUR_OF_DAY, 0);
        startCal.set(Calendar.MINUTE, 0);
        startCal.set(Calendar.SECOND, 0);
        startCal.set(Calendar.MILLISECOND, 0);
        final Date startDate = startCal.getTime();
        startCal.add(Calendar.DATE, 1);
        final Date endDate = startCal.getTime();
        //сначала отфильтруем доступные юзеру девайсы
        return locationRepo.findLastLocations(startDate, endDate, devices);
        //потом запросим все положения девайсов в этот день и сгруппируем по ид девайса
/*        final List<Location> result = new ArrayList<>();
        final Map<Long, List<Location>> locationsByDeviceId = locationRepo
                .findAllByAddedBetweenAndDeviceIn(startDate, endDate, filteredDevices)
                .stream()
                .collect(Collectors.groupingBy(l -> l.getDevice().getId()));
        ;
        //для каждого девайса находим последнее местоположение
        for (List<Location> deviceLocations : locationsByDeviceId.values()) {
            deviceLocations
                    .parallelStream()
                    .max(Comparator.comparing(Location::getId)).ifPresent(result::add);
        }
        return result;*/
    }
}
