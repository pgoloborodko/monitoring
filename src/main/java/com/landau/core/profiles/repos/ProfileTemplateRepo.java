package com.landau.core.profiles.repos;

import com.landau.core.profiles.entities.Profile;
import com.landau.core.profiles.entities.ProfileTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileTemplateRepo extends JpaRepository<ProfileTemplate, Long> {
    List<ProfileTemplate> findAllByProfile(Profile profile);

    void deleteAllByProfile(Profile profile);
}