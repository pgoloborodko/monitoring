package com.landau.core.profiles.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.profiles.entities.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProfileRepo extends JpaRepository<Profile, Long>, JpaSpecificationExecutor<Profile> {
    List<Profile> findAllByCompany(Company company);
}