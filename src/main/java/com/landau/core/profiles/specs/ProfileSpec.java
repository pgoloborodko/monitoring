package com.landau.core.profiles.specs;

import com.landau.core.profiles.entities.Profile;
import com.landau.core.profiles.filters.ProfileFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class ProfileSpec {
    public static Specification<Profile> find(ProfileFilter profileFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (profileFilter.getFirstName() != null && !profileFilter.getFirstName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("firstName")), profileFilter.getFirstName().toUpperCase() +"%"));
            }

            if (profileFilter.getSecondName() != null && !profileFilter.getSecondName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("secondName")), profileFilter.getSecondName().toUpperCase() +"%"));
            }

            if (profileFilter.getLastName() != null && !profileFilter.getLastName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("lastName")), profileFilter.getLastName().toUpperCase() +"%"));
            }

            if (profileFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), profileFilter.getCompany()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
