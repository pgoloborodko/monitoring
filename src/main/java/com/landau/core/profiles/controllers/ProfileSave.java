package com.landau.core.profiles.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.profiles.entities.Profile;
import com.landau.core.profiles.repos.ProfileRepo;
import com.landau.core.profiles.repos.ProfileTemplateRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

/**
 * Сохранение людей
 */
@Api(tags = "Сохранение людей", description = "Контроллер может получать данные человека, а также создавать новые")
@RestController
@RequestMapping("/profiles/save")
public class ProfileSave extends Base {
    private ProfileRepo profileRepo;
    private ProfileTemplateRepo profileTemplateRepo;

    @Autowired
    public ProfileSave(ProfileRepo profileRepo, ProfileTemplateRepo profileTemplateRepo) {
        this.profileRepo = profileRepo;
        this.profileTemplateRepo = profileTemplateRepo;
    }

//
//    @Autowired
//    private WebSocketServer webSocketServer;


    /**
     * Получение данных о человеке
     * @param profile id человека
     * @return объект com.landau.core.profiles.entities.People
     */
    @ApiOperation(value = "Получение данных о человеке", response = Profile.class)
    @GetMapping
    @Authority(Profile.class)
    public Profile get(Profile profile) {
        return profile;
    }

    /**
     * Добавление, изменение данных человека
     * @param profile объект человека
     * @param bindingResult
     * @return false с указанием ошибки, либо объект человека
     */
    @ApiOperation(value = "Сохранение данных человека", response = Profile.class)
    @Authority(Profile.class)
    @PostMapping
    public Profile post(Profile profile, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.getAllErrors().toString());
        }

        if(profile.getId() != null) {
            profileTemplateRepo.deleteAllByProfile(profile);
        }
        if(getUser().getRole() != User.Role.ADMIN) {
            profile.setCompany(getUser().getCompany());
        }

        profile.setUpdated(new Date());

        profileRepo.save(profile);

        //TODO отправлять профили на рег
   /*     if(people.getPhotoAsFile() != null) {
            List<DeviceConnector> connectedDevices = webSocketServer.filterRecipients(DeviceConnector.class);

            if(connectedDevices.size() > 0) {
                connectedDevices.get(0).sendNewUsers(people.getCompany());
            }

            List<BiometricConnector> biometricConnectors = webSocketServer.filterRecipients(BiometricConnector.class);

            for(BiometricConnector connector :biometricConnectors ) {
                connector.sendUpdate();
            }
        }*/

        return profile;
    }
}
