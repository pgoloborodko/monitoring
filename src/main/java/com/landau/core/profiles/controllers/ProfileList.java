package com.landau.core.profiles.controllers;

import com.landau.core.common.ApiPageable;
import com.landau.core.common.controllers.Base;
import com.landau.core.profiles.entities.Profile;
import com.landau.core.profiles.filters.ProfileFilter;
import com.landau.core.profiles.repos.ProfileRepo;
import com.landau.core.profiles.specs.ProfileSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список людей
 */
@Api(tags = "Список людей", description = "Контроллер обеспечивает получение списка людей")
@RestController
@RequestMapping("/profiles")
public class ProfileList extends Base {
    private ProfileRepo profileRepo;

    @Autowired
    public ProfileList(ProfileRepo profileRepo) {
        this.profileRepo = profileRepo;
    }

    /**
     * Получение списка людей по фильтру
     * @param profileFilter поля фильтра
     * @return список людей
     */
    @ApiPageable
    @ApiOperation(value = "Получение списка людей по фильтру", response = Profile.class)
    @GetMapping
    public Page<Profile> get(ProfileFilter profileFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            profileFilter.setCompany(getUser().getCompany());
        }

        return profileRepo.findAll(ProfileSpec.find(profileFilter), pageable);
    }
}
