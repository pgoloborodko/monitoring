package com.landau.core.profiles.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.profiles.repos.ProfileTemplateRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table
@JsonInclude
@Repo(ProfileTemplateRepo.class)
@ApiModel(description = "дескриптор")
public class ProfileTemplate extends AbstractId {
    public enum Type {
        IIT
    }

    @Column(length=65535)
    @JsonIgnore
    @ApiModelProperty(value = "Фото-шаблон человека")
    private Byte[] photoTemplate;

    @Column
    private Type type;

    @ManyToOne
    private Profile profile;
}
