package com.landau.core.profiles.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.SiteApplication;
import com.landau.core.common.ImageProcessor;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.profiles.repos.ProfileRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Профили людей
 */
@Getter
@Setter
@Entity
@Table
@JsonInclude
@Repo(ProfileRepo.class)
@ApiModel(description = "Профили людей")
public class Profile extends AbstractBusiness {
    /**
     * Тип человека
     */
    public enum Type {
        /**
         * Приступник
         */
        CRIMINAL,
        /**
         * Обычный человек
         */
        USUAL,
    }

    @Column
    @ApiModelProperty(value = "Имя человека")
    private String firstName;

    @Column
    @ApiModelProperty(value = "Фамилия человека")
    private String lastName;

    @Column
    @ApiModelProperty(value = "Отчество человека")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String secondName;

    @Column
    @ApiModelProperty(value = "Номер паспорта человека")
    private String passportNum;

    @Column
    @ApiModelProperty(value = "Серия паспорта человека")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String passportSeries;

    @Column
    @ApiModelProperty(value = "Кем выдан паспорт человека")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String passportIssue;

    @Column
    @ApiModelProperty(value = "Путь к фото человека")
    private String photo;

    @Column
    @ApiModelProperty(value = "Тип человека")
    private Type type;

    @Transient
    @JsonIgnore
    @ApiModelProperty(value = "Фото-файл человека")
    private MultipartFile photoFile;

    @Column
    @ApiModelProperty(value = "Описание человека")
    private String description;

    @Column
    @ApiModelProperty(value = "Возраст человека")
    private Integer age;

    @Column
    @ApiModelProperty(value = "Дата создания записи")
    private Date added;

    @Column
    @ApiModelProperty(value = "Дата обновления записи")
    private Date updated;

    @OneToMany
    @ApiModelProperty(value = "Дескрипторы профиля")
    private List<ProfileTemplate> templates;

    private Integer getTemplatesCount() {
        return templates.size();
    }

    /**
     * Отчечтво пользователя
     * @return
     */
    public String getSecondName() {
        if(secondName == null ) {
            return "";
        }

        return secondName;
    }

    /**
     * Описание человека
     * @return
     */
    public String getDescription() {
        if(description == null ) {
            return "";
        }

        return description;
    }

    /**
     * Файл фото
     * @return
     */
    @JsonIgnore
    public MultipartFile getPhotoFile() {
        return photoFile;
    }

    /**
     * Фото как файл
     * @return
     */
    public File getPhotoAsFile() {
        return new File(SiteApplication.DIR_AVATARS + getPhoto());
    }

    public void setPhotoFile(MultipartFile photoFile) {
        this.photoFile = photoFile;
        try {
            if (photoFile == null || photoFile.isEmpty()) return;

            String newPath = UUID.randomUUID().toString() + ".jpeg";

            try {
                BufferedImage bufferedImage = ImageIO.read(photoFile.getInputStream());
                ImageProcessor ip = new ImageProcessor();
                bufferedImage = ip.resizeImage(bufferedImage);
                ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
                ImageWriteParam param = writer.getDefaultWriteParam();
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
                param.setCompressionQuality(1.0F); // Highest quality

                File outputFile = new File(SiteApplication.DIR_AVATARS + newPath);

                writer.setOutput(new FileImageOutputStream(outputFile));
                writer.write(bufferedImage);

                setPhoto(newPath);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        catch (NullPointerException | IllegalArgumentException ex2) {
            ex2.printStackTrace();
        }
    }
}
