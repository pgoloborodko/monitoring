package com.landau.core.events.filters;

import com.landau.core.users.entities.User;
import lombok.Data;

import java.util.Date;

@Data
public class EventUserDevicesSyncFilter {
    public enum Type {
        ARCHIVE,
        STREAM,
        ALL
    }

    private User user;
    private Date date;
    private Type type = Type.ALL;
}
