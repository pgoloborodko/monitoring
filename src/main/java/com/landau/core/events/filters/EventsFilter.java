package com.landau.core.events.filters;

import com.landau.core.events.entities.AbstractEvent;
import lombok.Data;

@Data
public class EventsFilter extends AbstractEvent {
    private boolean orderByCreatedAsc = false;
    private boolean orderByCreatedDesc = false;
}
