package com.landau.core.events.filters;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.*;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.users.entities.User;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class EventMonthFilter {
    public enum Module {
        DEVICE,
        BIOMETRIC,
        TERMINAL
    }

    public enum DeviceEventType{
        BUTTON,
        INSTALLER,
        STREAM,
        SYSTEM
    }

    public enum BiometricEventType{
        RECOGNITION,
        TRACKING
    }

    public enum TerminalEventType{
        FILE,
        SYNC,
        SYSTEM
    }


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date day = new Date();
    private Date from;
    private Date to;
    private Date created;

    private Module module;
    private DeviceEventType deviceEventType;
    private BiometricEventType biometricEventType;
    private TerminalEventType terminalEventType;

    private EventBiometricRecognition.Type biometricRecognitionEventType;
    private EventDeviceButton.Type deviceButtonEventType;
    private EventDeviceInstaller.Type deviceInstallerEventType;
    private EventDeviceStream.Type deviceStreamEventType;
    private EventDeviceStream.Resolution deviceStreamEventResolution;
    private EventDeviceSystem.Type deviceSystemEventType;
    private EventTerminalSync.Type terminalSyncEventType;
    private EventTerminalSystem.Type terminalSystemEventType;

    private Biometric biometric;
    private Terminal terminal;
    private User user;
    private Device device;

    private Boolean recognized;
    private Double duration;
}
