package com.landau.core.events.filters;

import com.landau.core.events.entities.EventTerminalSync;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EventTerminalSyncFilter extends EventTerminalSync {
    private List<EventTerminalSync.Type> types = new ArrayList<>();
    private boolean orderByCreatedAsc = false;
    private boolean orderByCreatedDesc = false;
}
