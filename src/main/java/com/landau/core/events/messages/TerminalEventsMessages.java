package com.landau.core.events.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.events.entities.EventTerminalSystem;
import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.events.repos.EventTerminalSyncRepo;
import com.landau.core.events.repos.EventTerminalSystemRepo;
import com.landau.core.events.repos.UserDevicesSyncRepo;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.objects.events.terminal.TerminalFileEvent;
import com.landau.objects.events.terminal.TerminalSyncEvent;
import com.landau.objects.events.terminal.TerminalSystemEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
@Slf4j
public class TerminalEventsMessages implements IService {
    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CastService castService;

    @Autowired
    private EventTerminalSyncRepo eventTerminalSyncRepo;

    @Autowired
    private EventTerminalSystemRepo eventTerminalSystemRepo;

    @Autowired
    private MediaFileRepo mediaFileRepo;

    @Autowired
    private UserDevicesSyncRepo userDevicesSyncRepo;

    /**
     * Получение и обработка сообщения типа TerminalFileEvent
     * @param event
     */
    @ReConsumer(listen = TerminalFileEvent.class, autoDelete = false)
    protected void consumeTerminalFileEvent(TerminalFileEvent event) {
        log.info("consume message: {}", TerminalFileEvent.class);
        MediaFile mediaFile = castService.castToEntity(event);

        if (mediaFile != null){
            mediaFileRepo.save(mediaFile);
            log.info("save: {}", MediaFile.class);
        }
    }

    /**
     * Получение и обработка сообщения типа TerminalSyncEvent
     * @param event
     */
    @ReConsumer(listen = TerminalSyncEvent.class, autoDelete = false)
    protected void consumeTerminalSyncEvent(TerminalSyncEvent event) {
        log.info("consume message: {}", TerminalSyncEvent.class);
        EventTerminalSync eventTerminalSync = castService.castToEntity(event);

        if (eventTerminalSync != null) {
            eventTerminalSyncRepo.save(eventTerminalSync);
            log.info("save: {}", EventTerminalSync.class);

            if(eventTerminalSync.getType() == EventTerminalSync.Type.EXTRACTED) {
                UserDevicesSync sync = new UserDevicesSync();
                sync.setDevice(eventTerminalSync.getDevice());
                sync.setExtracted(eventTerminalSync.getCreated());
                sync.setUser(eventTerminalSync.getUser());

                userDevicesSyncRepo.save(sync);
            }
            else if(eventTerminalSync.getType() == EventTerminalSync.Type.INSERTED) {
                List<UserDevicesSync> syncs = userDevicesSyncRepo.findAllByDeviceAndUserAndInsertedIsNull(
                        eventTerminalSync.getDevice(), eventTerminalSync.getUser());

                for(UserDevicesSync sync : syncs) {
                    sync.setInserted(eventTerminalSync.getCreated());
                    userDevicesSyncRepo.save(sync);
                }
            }
        }
    }

    /**
     * Получение и обработка сообщения типа TerminalFileEvent
     * @param event
     */
    @ReConsumer(listen = TerminalSystemEvent.class, autoDelete = false)
    protected void consumeTerminalSystemEvent(TerminalSystemEvent event) {
        log.info("consume message: {}", TerminalSystemEvent.class);
        EventTerminalSystem eventTerminalSystem = castService.castToEntity(event);

        if (eventTerminalSystem != null){
            eventTerminalSystemRepo.save(eventTerminalSystem);
            log.info("save: {}", EventTerminalSync.class);
        }
    }

}
