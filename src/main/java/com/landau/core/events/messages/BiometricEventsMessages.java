package com.landau.core.events.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.landau.core.common.messages.CastService;
import com.landau.core.events.entities.EventBiometricRecognition;
import com.landau.core.events.repos.EventBiometricRecognitionRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.objects.events.biometric.BiometricFaceRecognitionEvent;
import com.landau.objects.events.biometric.BiometricFaceTrackingEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Сервис принимает и обрабатывает сообщения типа Event для Biometric
 */
@Data
@Service
@Slf4j
public class BiometricEventsMessages {

    @Autowired
    private EventBiometricRecognitionRepo eventBiometricRecognitionRepo;

    @Autowired
    private CastService castService;

    @Autowired
    private CoreBrowserMessages coreBrowserMessages;

    /**
     * Получение и обработка сообщения типа BiometricFaceRecognitionEvent
     * @param event
     */
    @ReConsumer(listen = BiometricFaceRecognitionEvent.class, autoDelete = false)
    public void consumeBiometricFaceRecognitionEvent(BiometricFaceRecognitionEvent event) {
        log.info("consume message: {}",  BiometricFaceRecognitionEvent.class);
        EventBiometricRecognition eventEntity = castService.castToEntity(event);

        if (eventEntity != null){
            eventBiometricRecognitionRepo.save(eventEntity);
            log.info("save: {}",  EventBiometricRecognition.class);

            try {
                coreBrowserMessages.send("", eventEntity);
            } catch (RepublisherException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Получение и обработка сообщения типа BiometricFaceRecognitionEvent
     * @param event
     */
    @ReConsumer(listen = BiometricFaceTrackingEvent.class)
    public void consumeBiometricFaceTrackingEvent(BiometricFaceTrackingEvent event) {
        log.info("consume message: {}",  BiometricFaceTrackingEvent.class);
        EventBiometricRecognition eventEntity = castService.castToEntity(event);

        if (eventEntity != null){
            eventBiometricRecognitionRepo.save(eventEntity);
            log.info("save: {}",  EventBiometricRecognition.class);

            try {
                coreBrowserMessages.send("", eventEntity);
            } catch (RepublisherException e) {
                e.printStackTrace();
            }
        }
    }

}
