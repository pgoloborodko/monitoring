package com.landau.core.events.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.lampa.republish.messages.Message;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.events.entities.EventDeviceButton;
import com.landau.core.events.entities.EventDeviceInstaller;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.events.entities.EventDeviceSystem;
import com.landau.core.events.repos.EventDeviceButtonRepo;
import com.landau.core.events.repos.EventDeviceInstallerRepo;
import com.landau.core.events.repos.EventDeviceStreamRepo;
import com.landau.core.events.repos.EventDeviceSystemRepo;
import com.landau.core.streams.messages.DeviceStreamMessages;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.objects.events.device.DeviceButtonEvent;
import com.landau.objects.events.device.DeviceInstallerEvent;
import com.landau.objects.events.device.DeviceStreamEvent;
import com.landau.objects.events.device.DeviceSystemEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.TreeMap;

@Data
@Service
@Slf4j
public class DeviceEventsMessages implements IService {
    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private CastService castService;

    @Autowired
    private EventDeviceButtonRepo eventDeviceButtonRepo;

    @Autowired
    private EventDeviceInstallerRepo eventDeviceInstallerRepo;

    @Autowired
    private EventDeviceStreamRepo eventDeviceStreamRepo;

    @Autowired
    private EventDeviceSystemRepo eventDeviceSystemRepo;

    @Autowired
    private DeviceStreamMessages deviceStreamMessages;

    @Autowired
    private CoreBrowserMessages browser;

    /**
     * Получение и обработка сообщения типа DeviceButtonEvent
     * @param event
     */
    @ReConsumer(listen = DeviceButtonEvent.class, autoDelete = false)
    public void consumeDeviceButtonEvent(DeviceButtonEvent event) {
        log.info("consume message: {}", DeviceButtonEvent.class);
        EventDeviceButton eventDeviceButton = castService.castToEntity(event);

        if(eventDeviceButton != null) {
            eventDeviceButtonRepo.save(eventDeviceButton);
            log.info("consume message: {}", EventDeviceButton.class);

            if (eventDeviceButton.getType().equals(EventDeviceButton.Type.STREAM)) {
                try {
                    deviceStreamMessages.startStream(eventDeviceButton.getDevice(), event.getTimeout());
                } catch (RepublisherException e) {
                    // TOFO
                    e.printStackTrace();
                }
            }

            TreeMap<String, Object> filter = new TreeMap<>();
            filter.put("device", eventDeviceButton.getDevice().getId());
            if(eventDeviceButton.getUser() != null) {
                filter.put("user", eventDeviceButton.getUser().getId());
            }

            if(eventDeviceButton.getDevice().getCompany() != null) {
                filter.put("company", eventDeviceButton.getDevice().getCompany().getId());
            }

            try {
                log.info("send message: {}", EventDeviceButton.class);
                browser.getMessageService().getRepublish().produce(new Message<>(eventDeviceButton, filter));
            } catch (RepublisherException e) {
                log.error("error send message to broker", e);
            }
        }
    }

    /**
     * Получение и обработка сообщения типа DeviceInstallerEvent
     * @param event
     */
    @ReConsumer(listen = DeviceInstallerEvent.class, autoDelete = false)
    public void consumeDeviceInstallerEvent(DeviceInstallerEvent event) {
        log.info("consume message: {}", DeviceInstallerEvent.class);
        EventDeviceInstaller eventDeviceInstaller = castService.castToEntity(event);

        if (eventDeviceInstaller != null){
            eventDeviceInstallerRepo.save(eventDeviceInstaller);
            log.info("save: {}", EventDeviceInstaller.class);
        }
    }

    /**
     * Получение и обработка сообщения типа DeviceStreamEvent
     * @param event
     */
    @ReConsumer(listen = DeviceStreamEvent.class, autoDelete = false)
    public void consumeDeviceStreamEvent(DeviceStreamEvent event) {
        log.info("consume message: {}", DeviceStreamEvent.class);
        EventDeviceStream eventDeviceStream = castService.castToEntity(event);

        if (eventDeviceStream != null) {
            eventDeviceStreamRepo.save(eventDeviceStream);
            log.info("save: {}", EventDeviceStream.class);

            TreeMap<String, Object> filter = new TreeMap<>();
            filter.put("device", eventDeviceStream.getDevice().getId());

            if(eventDeviceStream.getUser() != null) {
                filter.put("user", eventDeviceStream.getUser().getId());
            }

            if(eventDeviceStream.getDevice().getCompany() != null) {
                filter.put("company", eventDeviceStream.getDevice().getCompany().getId());
            }

            try {
                log.info("send message: {}", EventDeviceStream.class);
                browser.getMessageService().getRepublish().produce(new Message<>(eventDeviceStream, filter));
            } catch (RepublisherException e) {
                log.error("error send message to broker", e);
            }
        }
        else {
            log.warn("error cast DeviceStreamEvent");
        }
    }

    /**
     * Получение и обработка сообщения типа DeviceSystemEvent
     * @param event
     */
    @ReConsumer(listen = DeviceSystemEvent.class, autoDelete = false)
    public void consumeDeviceSystemEvent(DeviceSystemEvent event) {
        log.info("consume message: {}", DeviceSystemEvent.class);
        EventDeviceSystem eventDeviceSystem = castService.castToEntity(event);

        if (eventDeviceSystem != null){
            eventDeviceSystemRepo.save(eventDeviceSystem);
            log.info("save: {}", EventDeviceSystem.class);

            if (eventDeviceSystem.getType() != null){
                if (eventDeviceSystem.getType().equals(EventDeviceSystem.Type.WEAPON_EXTRACTED) ||
                        eventDeviceSystem.getType().equals(EventDeviceSystem.Type.WEAPON_INSERTED)){
                    //TODO FIX!!!!
                    /*try {
                        browserMessages.getMessageService().getRepublish()
                                .produce(new Message<>(eventDeviceSystem, new CompanyFilter(eventDeviceSystem.getDevice().getCompany())));

                        log.info("send message: {}", EventDeviceSystem.class);
                    } catch (RepublisherException e) {
                        e.printStackTrace();
                    }*/
                }
            }
            else {
                log.warn("null value: {}", EventDeviceSystem.Type.class);
            }
        }
    }
}
