package com.landau.core.events.pojo;

import com.landau.core.events.entities.EventDeviceButton;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BrowserEvent {
    public enum EventType {
        SOS,
        BUTTON,
        PTT
    }
    private EventType type;
    private EventDeviceButton event;
}
