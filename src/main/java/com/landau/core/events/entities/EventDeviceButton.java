package com.landau.core.events.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
@ApiModel(description = "событие нажатия кнопки на устройстве")
public class EventDeviceButton extends AbstractDeviceEvent {
    public enum Type {
        PTT,
        SOS,
        RECOGNITION,
        SETTINGS,
        STREAM
    }

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;
}
