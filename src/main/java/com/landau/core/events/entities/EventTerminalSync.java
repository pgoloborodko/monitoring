package com.landau.core.events.entities;

import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.devices.entities.Device;
import com.landau.core.events.repos.EventTerminalSyncRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Table
@Entity
@Repo(EventTerminalSyncRepo.class)
@ApiModel(description = "событие синхронизации на терминале")
public class EventTerminalSync extends AbstractTerminalEvent {
    public enum Type {
        INSERTED,
        CHARGE,
        SYNC,
        OK,
        EXTRACTED,
        ERROR
    }

    @ManyToOne
    @ApiModelProperty(value = "сущность устройства")
    private Device device;

    @ManyToOne
    @ApiModelProperty(value = "сущность пользователя")
    private User user;

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;

}
