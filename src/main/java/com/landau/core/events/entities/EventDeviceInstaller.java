package com.landau.core.events.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
@ApiModel(description = "события установки/удаления на устройство")
public class EventDeviceInstaller extends AbstractDeviceEvent {
    public enum Type {
        DOWNLOAD,
        AUTO_INSTALL,
        MANUAL_INSTALL,
        INSTALLED,
        AUTO_UNINSTALL,
        MANUAL_UNINSTALL,
        UNINSTALLED,
        ERROR
    }

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;

    @Column
    @ApiModelProperty(value = "тип события")
    private String info;

}
