package com.landau.core.events.entities;

import com.landau.core.terminals.entities.Terminal;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class AbstractTerminalEvent extends AbstractEvent {
    @ManyToOne
    @ApiModelProperty(value = "Терминал")
    private Terminal terminal;
}
