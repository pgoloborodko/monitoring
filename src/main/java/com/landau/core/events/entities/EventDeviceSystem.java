package com.landau.core.events.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
@ApiModel(description = "системное событие на устройстве")
public class EventDeviceSystem extends AbstractDeviceEvent {
    public enum Type{
        LOW_CHARGE,
        POWER_CONNECTED,
        POWER_DISCONNECTED,
        WEAPON_EXTRACTED,
        WEAPON_INSERTED,
        ERROR
    }

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;

    @Column
    @ApiModelProperty(value = "инфо")
    private String info;
}
