package com.landau.core.events.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.landau.core.devices.entities.Device;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class AbstractDeviceEvent extends AbstractEvent {
    @ManyToOne
    @JsonIgnore
    @ApiModelProperty(value = "Пользователь")
    private User user;

    @ManyToOne
    @JsonIgnore
    @ApiModelProperty(value = "Устройства")
    private Device device;

    public String getFio() {
        if(user != null) {
            return user.getFirstName() + " " + user.getLastName() + " " + user.getSecondName();
        }
        return "";
    }
}
