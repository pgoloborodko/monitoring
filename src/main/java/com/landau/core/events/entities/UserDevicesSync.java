package com.landau.core.events.entities;

import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import com.landau.core.events.repos.UserDevicesSyncRepo;
import com.landau.core.users.entities.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table
@Setter
@Getter
@Repo(UserDevicesSyncRepo.class)
public class UserDevicesSync extends AbstractId {
    @ManyToOne
    private User user;

    @ManyToOne
    private Device device;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date extracted;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inserted;
}
