package com.landau.core.events.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
@ApiModel(description = "событие стрима на устройстве")
public class EventDeviceStream extends AbstractDeviceEvent {
    public enum Resolution{
        VGA,
        SD,
        HD,
        FULL_HD,
        ULTRA_HD
    }
    public enum Type{
        START,
        STOP
    }

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;

    @Column
    @ApiModelProperty(value = "тип разрешения")
    private Resolution resolution;
}
