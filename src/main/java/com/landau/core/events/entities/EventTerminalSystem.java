package com.landau.core.events.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@Data
@ApiModel(description = "системное событие терминала")
public class EventTerminalSystem extends AbstractTerminalEvent {
    public enum Type{
        RUNTIME_ERROR,
        DISK_FULL
    }

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;

    @Column
    @ApiModelProperty(value = "информация")
    private String info;
}
