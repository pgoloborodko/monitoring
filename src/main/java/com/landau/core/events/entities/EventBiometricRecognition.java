package com.landau.core.events.entities;

import com.landau.core.biometric.entities.Biometric;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table
@ApiModel(description = "событие биометрической системы")
public class EventBiometricRecognition extends AbstractBiometricEvent {
    public enum Type{
        RECOGNITION,
        TRACKING
    }

    @Column
    @ApiModelProperty(value = "тип события")
    private Type type;

    @ManyToOne
    @ApiModelProperty(value = "биометрическая система")
    private Biometric biometric;

    @Column
    @ApiModelProperty(value = "внешний профиль")
    private Long externalProfile;

    @Column
    @ApiModelProperty(value = "процент распознования")
    private Float score;

    @Column
    @ApiModelProperty(value = "дата создания")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date created;

    @Column
    @ApiModelProperty(value = "дата окончания")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date ended;

    @Column
    @ApiModelProperty(value = "опознан да/нет")
    private Boolean recognized;

    @Transient
    private Byte[] face;
}
