package com.landau.core.events.services;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.core.Consumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.config.stomp.StompPrincipal;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.events.pojo.BrowserEvent;
import com.landau.core.locations.entities.Location;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import com.landau.messages.core.CoreBrowserMessages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Controller
public class EventService {
    @Autowired
    private CoreBrowserMessages messages;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private UserRepo userRepo;

    private List<Sub> subs = new ArrayList<>();

    @PostConstruct
    private void init() {

    }

    /**
     * consume locations
     * @param headerAccessor
     * @throws RepublisherException
     */
    @SubscribeMapping("/user/users/status")
    private void subscribeUserStatus(SimpMessageHeaderAccessor headerAccessor) throws RepublisherException {
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();

        TreeMap<String, Object> filter = new TreeMap<>();

        // check access
        if(principal.getUser().getRole() != User.Role.ADMIN) {
            if(principal.getUser().getCompany() != null) {
                filter.put("company", principal.getUser().getCompany().getId());
            }
            else {
                return;
            }
        }

        IService service = new IService() {
            private void consumeStatus(User user) {
                log.info("consume user status " + user);
                template.convertAndSendToUser(principal.getName(), "/users/status", user);
            }
        };

        Consumer consumer = new Consumer(User.class, "", true, true,
                null, filter, "consumeStatus", service,
                "browser", null, true, false);

        subs.add(new Sub(principal.getName(), consumer, service));
        messages.getMessageService().getRepublish().addConsumer(consumer);
    }


    /**
     * consume locations
     * @param headerAccessor
     * @param userId
     * @throws RepublisherException
     */
    @SubscribeMapping("/user/users/status/{userId}")
    private void subscribeUserStatus(SimpMessageHeaderAccessor headerAccessor, @DestinationVariable final Integer userId) throws RepublisherException {
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();

        // check need user exists
        Optional<User> optionalUser = userRepo.findById(Long.valueOf(userId));
        if(!optionalUser.isPresent()) {
            return;
        }

        // check access
        if(principal.getUser().getRole() != User.Role.ADMIN) {
            if(!optionalUser.get().getCompany().equals(principal.getUser().getCompany())) {
                return;
            }
        }

        // headerAccessor.getSessionId()
        IService service = new IService() {
            private void consumeStatus(User user) {
                log.info("consume user status " + user);
                template.convertAndSendToUser(principal.getName(), "/users/status/" + userId, user);
            }
        };

        TreeMap<String, Object> filter = new TreeMap<>();
        filter.put("user", userId);

        Consumer consumer = new Consumer(User.class, "", true, true,
                null, filter, "consumeStatus", service,
                "browser", null, true, false);

        subs.add(new Sub(principal.getName(), consumer, service));
        messages.getMessageService().getRepublish().addConsumer(consumer);
    }

    /**
         * consume locations
         * @param headerAccessor
         * @param userId
         * @throws RepublisherException
         */
    @SubscribeMapping("/user/users/location/{userId}")
    private void subscribeUserLocations(SimpMessageHeaderAccessor headerAccessor, @DestinationVariable final Integer userId) throws RepublisherException {
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();

        // check need user exists
        Optional<User> optionalUser = userRepo.findById(Long.valueOf(userId));
        if(!optionalUser.isPresent()) {
            return;
        }

        // check access
        if(principal.getUser().getRole() != User.Role.ADMIN) {
            if(!optionalUser.get().getCompany().equals(principal.getUser().getCompany())) {
                return;
            }
        }

        IService service = new IService() {
            private void consumeLocation(Location location) {
                log.info("consume location " + location);
                template.convertAndSendToUser(principal.getName(), "/users/locations/" + userId, location);
            }
        };

        TreeMap<String, Object> filter = new TreeMap<>();
        filter.put("user", userId);

        Consumer consumer = new Consumer(Location.class, "", true, true,
                null, filter, "consumeLocation", service,
                "browser", null, true, false);

        subs.add(new Sub(principal.getName(), consumer, service));
        messages.getMessageService().getRepublish().addConsumer(consumer);
    }

    /**
     *
     * @param headerAccessor
     * @param userId
     * @throws RepublisherException
     */
    @SubscribeMapping("/user/users/streams/start/{userId}")
    private void subscribeStreamsStart(SimpMessageHeaderAccessor headerAccessor, @DestinationVariable Long userId) throws RepublisherException {
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();

        // check need user exists
        Optional<User> optionalUser = userRepo.findById(Long.valueOf(userId));
        if(!optionalUser.isPresent()) {
            return;
        }

        // check access
        if(principal.getUser().getRole() != User.Role.ADMIN) {
            if(!optionalUser.get().getCompany().equals(principal.getUser().getCompany())) {
                return;
            }
        }

        IService service = new IService() {
            private void consumeStartStream(EventDeviceStream deviceStream) {
                log.info("consume start stream " + deviceStream);
                template.convertAndSendToUser(principal.getName(),"/users/streams/start/" + userId, deviceStream);
            }
        };

        TreeMap<String, Object> filter = new TreeMap<>();
        filter.put("user", userId);

        Consumer consumer = new Consumer(EventDeviceStream.class, "", true, true,
                null, filter, "consumeStartStream", service,
                "browser", null, true, false);

        subs.add(new Sub(principal.getName(), consumer, service));
        messages.getMessageService().getRepublish().addConsumer(consumer);
    }

    @SubscribeMapping("/user/users/streams/stop/{user}")
    private void subscribeStreamsStop(SimpMessageHeaderAccessor headerAccessor, @DestinationVariable Long device) {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) headerAccessor.getUser();
        token = token;
        //template.convertAndSendToUser(headerAccessor.getSessionId(),"/streams/stop/" + device, token.getPrincipal(), headerAccessor.getMessageHeaders());
    }

/*    @SubscribeMapping("/user/devices")
    private void subscribeDevices(SimpMessageHeaderAccessor headerAccessor) {
        //final UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) headerAccessor.getUser();
        //token = token;
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();
        headerAccessor = headerAccessor;

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                messagingTemplate.convertAndSendToUser(principal.getName(), "/devices", principal.getName());
            }
        }, 0, 1000);
    }*/

    @SubscribeMapping("/user/users/events")
    private void subscribeEvents(SimpMessageHeaderAccessor headerAccessor) throws RepublisherException {
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();

        TreeMap<String, Object> filter = new TreeMap<>();
        // check access
        if(principal.getUser().getRole() != User.Role.ADMIN) {
            if(principal.getUser().getCompany() != null) {
                filter.put("company", principal.getUser().getCompany().getId());
            }
            else {
                return;
            }
        }

        IService service = new IService() {
            private void consumeEvent(BrowserEvent browserEvent) {
                log.info("consume event " + browserEvent);
                template.convertAndSend("/users/events", browserEvent);
            }
        };

        Consumer consumer = new Consumer(BrowserEvent.class, "", true, true,
                null, filter, "consumeEvent", service,
                "browser", null, true, false);

        subs.add(new Sub(principal.getName(), consumer, service));
        messages.getMessageService().getRepublish().addConsumer(consumer);
    }

    @SubscribeMapping("/user/users/events/{user}")
    private void subscribeUserEvents(SimpMessageHeaderAccessor headerAccessor, @DestinationVariable Long userId) throws RepublisherException {
        StompPrincipal principal = (StompPrincipal) headerAccessor.getUser();
        TreeMap<String, Object> filter = new TreeMap<>();

        Optional<User> optionalUser = userRepo.findById(Long.valueOf(userId));

        if(!optionalUser.isPresent()) {
            return;
        }

        // check access
        if(principal.getUser().getRole() != User.Role.ADMIN) {
            if(!optionalUser.get().getCompany().equals(principal.getUser().getCompany())) {
                return;
            }
        }

        filter.put("user", optionalUser.get().getId());

        IService service = new IService() {
            private void consumeUserEvent(BrowserEvent browserEvent) {
                log.info("consume user event " + browserEvent);
                template.convertAndSendToUser(headerAccessor.getSessionId(),"/users/events/" + userId, browserEvent);
            }
        };

        Consumer consumer = new Consumer(BrowserEvent.class, "", true, true,
                null, filter, "consumeUserEvent", service,
                "browser", null, true, false);

        subs.add(new Sub(principal.getName(), consumer, service));
        messages.getMessageService().getRepublish().addConsumer(consumer);
    }

    @SubscribeMapping("/user/users/devices")
    private void subscribeDevices(SimpMessageHeaderAccessor headerAccessor) {


        template.convertAndSendToUser(headerAccessor.getSessionId(), "/devices", "123");
        //template.convertAndSendToUser(headerAccessor.getSessionId(),"/events", token.getPrincipal(), headerAccessor.getMessageHeaders());
    }

    @SubscribeMapping("/users/events/sos")
    private void subscribeEventsSos(SimpMessageHeaderAccessor headerAccessor) {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) headerAccessor.getUser();
        token = token;
        //template.convertAndSendToUser(headerAccessor.getSessionId(),"/events/sos", token.getPrincipal(), headerAccessor.getMessageHeaders());
    }

    @SubscribeMapping("/user/users/chat/coreBrowserMessages")
    private void subscribeChatMessages(SimpMessageHeaderAccessor headerAccessor) {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) headerAccessor.getUser();
        token = token;
    }

    @SubscribeMapping("/user/users/chat/rooms")
    private void subscribeChatRooms(SimpMessageHeaderAccessor headerAccessor) {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) headerAccessor.getUser();
        token = token;
        //template.convertAndSendToUser(headerAccessor.getSessionId(),"/chat/rooms", token.getPrincipal(), headerAccessor.getMessageHeaders());
    }

    public void disconnect(StompHeaderAccessor accessor) throws RepublisherException {

        for(Iterator<Sub> it = subs.iterator(); it.hasNext();) {
            Sub sub = it.next();

            if(sub.sessionId.equals(accessor.getSessionId())) {
                messages.getMessageService().getRepublish().removeConsumer(sub.consumer);
                messages.getMessageService().getRepublish().removeService(sub.iService);

                it.remove();
            }
        }
    }

    public void unsubscribe(StompHeaderAccessor accessor) {
    }


/*    @Autowired
    EventRepo eventRepo;

    @Autowired
    UserRepo userRepo;

    public List<EventsPerMonthDTO> getListPerMonth(List<User> users, List<String> eventTypesNames, Date date){
        List<Event.Type> eventTypes = new ArrayList<>();
        List<EventsPerMonthDTO> eventsPerMonthDTOList = new ArrayList<>();
        Map<Integer, EventsPerMonthDTO> mapPerMonth = new HashMap<>();

        if (date == null){
            date = new Date();
        }

        if (eventTypesNames != null){
            for (String eventName: eventTypesNames){
                Event.Type type = Event.Type.valueOf(eventName);
                eventTypes.add(type);
            }
        }
        else {
            eventTypes.add(Event.Type.SOS);
        }

        Calendar calendar = Calendar.getInstance();
        for (Event event : eventRepo.findAll(EventsSpec.findForMonth(users, eventTypes, date))){
            calendar.setTime(event.getCreated());
            if (!mapPerMonth.containsKey(calendar.get(Calendar.DAY_OF_MONTH))){
                EventsPerMonthDTO eventsPerMonthDTO = new EventsPerMonthDTO();

                eventsPerMonthDTO.setEvents(new ArrayList<>());
                eventsPerMonthDTO.setUsers(new ArrayList<>());
                eventsPerMonthDTO.setDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));

                eventsPerMonthDTO.getEvents().add(event.getType());
                eventsPerMonthDTO.getUsers().add(event.getUser());

                mapPerMonth.put(calendar.get(Calendar.DAY_OF_MONTH), eventsPerMonthDTO);
            }
            else {
                EventsPerMonthDTO eventsPerMonthDTO = mapPerMonth.get(calendar.get(Calendar.DAY_OF_MONTH));

                if (!eventsPerMonthDTO.getUsers().contains(event.getUser())){
                    eventsPerMonthDTO.getUsers().add(event.getUser());
                }
                if (!eventsPerMonthDTO.getEvents().contains(event.getType())){
                    eventsPerMonthDTO.getEvents().add(event.getType());
                }
            }
        }

        for (Map.Entry<Integer, EventsPerMonthDTO> item : mapPerMonth.entrySet() ){
            eventsPerMonthDTOList.add(item.getValue());
        }

        return eventsPerMonthDTOList;
    }*/

    private static class Sub {
        private String sessionId;
        private Consumer consumer;
        private IService iService;

        public Sub(String sessionId, Consumer consumer, IService iService) {
            this.sessionId = sessionId;
            this.consumer = consumer;
            this.iService = iService;
        }
    }
}
