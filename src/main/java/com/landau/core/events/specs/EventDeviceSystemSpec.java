package com.landau.core.events.specs;

import com.landau.core.events.entities.EventDeviceSystem;
import com.landau.core.events.filters.EventFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class EventDeviceSystemSpec {
    public static Specification<EventDeviceSystem> find(EventFilter eventFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (eventFilter.getDay() != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(eventFilter.getDay());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (eventFilter.getCreated() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(eventFilter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);


                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (eventFilter.getDevice() != null){
                predicates.add(cb.equal(root.get("device"), eventFilter.getDevice()));
            }

            if (eventFilter.getDeviceSystemEventType() != null){
                predicates.add(cb.equal(root.get("type"), eventFilter.getDeviceSystemEventType()));
            }

            if (eventFilter.getCompany() != null){
                predicates.add(cb.equal(root.get("company"), eventFilter.getCompany()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
