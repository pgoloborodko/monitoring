package com.landau.core.events.specs;

import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.events.filters.EventTerminalSyncFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class EventTerminalSyncSpec {
    public static Specification<EventTerminalSync> find(EventTerminalSyncFilter filter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (filter.getCreated() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(filter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);


                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (filter.getTerminal() != null){
                predicates.add(cb.equal(root.get("terminal"), filter.getTerminal()));
            }

            if (filter.getType() != null){
                predicates.add(cb.equal(root.get("type"), filter.getType()));
            }

            if (filter.getUser() != null){
                predicates.add(cb.equal(root.get("user"), filter.getUser()));
            }

            if (filter.getDevice() != null){
                predicates.add(cb.equal(root.get("device"), filter.getDevice()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
