package com.landau.core.events.specs;

public class EventsSpec {
/*    public static Specification<AbstractEvent> find(EventsFilter eventsFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (eventsFilter.getCreated() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(eventsFilter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);


                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            if (eventsFilter.getType() != null){
                predicates.add(cb.equal(root.get("type"), eventsFilter.getType()));
            }

            if (eventsFilter.getDevice() != null) {
                predicates.add(cb.equal(root.get("device"), eventsFilter.getDevice()));
            }

            if (eventsFilter.getUser() != null){
                predicates.add(cb.equal(root.get("user"), eventsFilter.getUser()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    public static Specification<Event> findForMonth(List<User> users, List<Event.Type> eventTypes, Date date) {
        return (root, query, cb) -> {
            final Collection<Predicate> datePredicates = new ArrayList<>();
            final Collection<Predicate> userPredicates = new ArrayList<>();
            final Collection<Predicate> finalPredicates = new ArrayList<>();
            final Collection<Predicate> eventPredicates = new ArrayList<>();


            Calendar calendar = Calendar.getInstance();
            Calendar calendarEnd = Calendar.getInstance();

            calendar.setTime(date);
            calendarEnd.setTime(date);

            int numberOfDays = calendar.getActualMaximum(Calendar.DATE);

            for (int i = 1; i <= numberOfDays; i++){
                calendar.set(Calendar.DAY_OF_MONTH, i);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                calendarEnd.set(Calendar.DAY_OF_MONTH, i);
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                Predicate datePredicate = cb.and(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()),
                        cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
                datePredicates.add(datePredicate);
            }

            if (users.size() != 0){
                for (User user : users){
                    userPredicates.add(cb.equal(root.get("user"), user));
                }

            }

            for (Event.Type type : eventTypes){
                eventPredicates.add(cb.equal(root.get("type"), type));
            }

//            for (Predicate userPredicate : userPredicates){
//                for (Predicate datePredicate : datePredicates){
//                    for (Predicate eventPredicate : eventPredicates){
//                        Predicate finalPredicate = cb.and(userPredicate, datePredicate, eventPredicate);
//                        finalPredicates.add(finalPredicate);
//                    }
//                }
//            }

            if (userPredicates.size() != 0){
                for (Predicate userPredicate : userPredicates){
                    for (Predicate datePredicate : datePredicates){
                        for (Predicate eventPredicate : eventPredicates){
                            Predicate finalPredicate = cb.and(userPredicate, datePredicate, eventPredicate);
                            finalPredicates.add(finalPredicate);
                        }
                    }
                }
            }
            else {
                for (Predicate datePredicate : datePredicates){
                    for (Predicate eventPredicate : eventPredicates){
                        Predicate finalPredicate = cb.and(datePredicate, eventPredicate);
                        finalPredicates.add(finalPredicate);
                    }
                }
            }

            return cb.or(finalPredicates.toArray(new Predicate[finalPredicates.size()]));
        };
    }*/
}
