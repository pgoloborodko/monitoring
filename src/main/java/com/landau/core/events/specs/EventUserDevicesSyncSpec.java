package com.landau.core.events.specs;

import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.events.filters.EventUserDevicesSyncFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class EventUserDevicesSyncSpec {
    public static Specification<UserDevicesSync> find(EventUserDevicesSyncFilter filter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (filter.getDate() != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(filter.getDate());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(calendar.getTime());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("extracted"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("extracted"), calendarEnd.getTime()));
            }

            if (filter.getType() != null) {
                if(filter.getType() == EventUserDevicesSyncFilter.Type.ALL || filter.getType() == EventUserDevicesSyncFilter.Type.ARCHIVE) {
                    predicates.add(cb.isNotNull(root.get("inserted")));
                }

                if(filter.getType() == EventUserDevicesSyncFilter.Type.ALL || filter.getType() == EventUserDevicesSyncFilter.Type.STREAM) {
                    predicates.add(cb.isNotNull(root.get("extracted")));
                }
            }

            if (filter.getUser() != null){
                predicates.add(cb.equal(root.get("user"), filter.getUser()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
