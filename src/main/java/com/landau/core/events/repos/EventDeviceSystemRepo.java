package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventDeviceSystemRepo extends JpaRepository<EventDeviceSystem, Long>, JpaSpecificationExecutor<EventDeviceSystem> {
    Optional<EventDeviceSystem> findByDevice(Device device);
}
