package com.landau.core.events.repos;

import com.landau.core.events.entities.EventTerminalSystem;
import com.landau.core.terminals.entities.Terminal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventTerminalSystemRepo extends JpaRepository<EventTerminalSystem, Long>, JpaSpecificationExecutor<EventTerminalSystem> {

    /**
     *  Метод для тестов
     * @param terminal
     * @return
     */
    Optional<EventTerminalSystem> findByTerminal(Terminal terminal);
}
