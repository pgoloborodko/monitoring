package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface EventDeviceStreamRepo extends JpaRepository<EventDeviceStream, Long>, JpaSpecificationExecutor<EventDeviceStream> {
    Optional<EventDeviceStream> findByDevice(Device device);

    List<EventDeviceStream> findByUserAndCreatedBetween(User user, Date created, Date created2);
}
