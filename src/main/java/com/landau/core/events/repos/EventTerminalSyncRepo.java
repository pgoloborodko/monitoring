package com.landau.core.events.repos;

import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.terminals.entities.Terminal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventTerminalSyncRepo extends JpaRepository<EventTerminalSync, Long>, JpaSpecificationExecutor<EventTerminalSync> {

    /**
     *  Метод для тестов
     * @param terminal
     * @return
     */
    Optional<EventTerminalSync> findByTerminal(Terminal terminal);
}
