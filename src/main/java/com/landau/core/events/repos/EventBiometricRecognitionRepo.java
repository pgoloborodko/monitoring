package com.landau.core.events.repos;

import com.landau.core.biometric.entities.Biometric;
import com.landau.core.events.entities.EventBiometricRecognition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventBiometricRecognitionRepo extends JpaRepository<EventBiometricRecognition, Long>,
        JpaSpecificationExecutor<EventBiometricRecognition> {
    Optional<EventBiometricRecognition> findByBiometric(Biometric biometric);
}
