package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceInstaller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventDeviceInstallerRepo extends JpaRepository<EventDeviceInstaller, Long>, JpaSpecificationExecutor<EventDeviceInstaller> {
    Optional<EventDeviceInstaller> findByDevice(Device device);
}
