package com.landau.core.events.repos;

import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.EventDeviceButton;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventDeviceButtonRepo extends JpaRepository<EventDeviceButton, Long>, JpaSpecificationExecutor<EventDeviceButton> {
    Optional<EventDeviceButton> findByDevice(Device device);
}
