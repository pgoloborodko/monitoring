package com.landau.core.events.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface UserDevicesSyncRepo extends JpaRepository<UserDevicesSync, Long> {
    List<UserDevicesSync> findAllByDeviceAndUserAndInsertedIsNull(Device device, User user);

    List<UserDevicesSync> findAllByInsertedIsNullAndUserNotNull();
    List<UserDevicesSync> findAllByInsertedIsNotNullAndExtractedBetweenOrInsertedBetween(Date extracted, Date extracted2, Date inserted, Date inserted2);

    Page<UserDevicesSync> findAllByUser_CompanyOrderByExtractedDescInsertedDesc(Company company, Pageable pageable);
    Page<UserDevicesSync> findAllByOrderByExtractedDescInsertedDesc(Pageable pageable);

    List<UserDevicesSync> findAll(Specification<UserDevicesSync> userDevicesSyncSpecification);

    List<UserDevicesSync> findAllByInsertedIsNullAndUser_Company(Company company);
}
