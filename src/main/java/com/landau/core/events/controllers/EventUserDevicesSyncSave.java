package com.landau.core.events.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.events.repos.UserDevicesSyncRepo;
import com.landau.messages.core.CoreBrowserMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Api(tags = "Сохранение/Изменение выдачи получения устройств")
@RestController("EventSave")
@RequestMapping("/events/users/sync/save")
public class EventUserDevicesSyncSave extends Base {
    private UserDevicesSyncRepo syncRepo;
    private CoreBrowserMessages messages;

    @Autowired
    public EventUserDevicesSyncSave(UserDevicesSyncRepo syncRepo, CoreBrowserMessages messages) {
        this.syncRepo = syncRepo;
        this.messages = messages;
    }

    @ApiOperation(value = "Получение данных события", response = EventTerminalSync.class)
    @Authority(value = UserDevicesSync.class)
    @GetMapping
    public UserDevicesSync get(UserDevicesSync sync) {
        return sync;
    }

    @ApiOperation(value = "Сохранение/Изменение выдачи получения устройств")
    @Authority(value = UserDevicesSync.class)
    @PostMapping
    public UserDevicesSync post(UserDevicesSync sync, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        if(sync.getInserted() == null) {
            sync.getUser().setDevice(sync.getDevice());
            sync.getDevice().setUser(sync.getUser());
            userRepo.save(sync.getUser());
            deviceRepo.save(sync.getDevice());
        }

        //this.messages.getMessageService().send(new Message<>(new Update()));
        return syncRepo.save(sync);
    }

    private static class Update {

    }
}
