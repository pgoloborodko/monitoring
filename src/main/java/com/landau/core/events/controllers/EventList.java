package com.landau.core.events.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.events.entities.*;
import com.landau.core.events.filters.EventFilter;
import com.landau.core.events.repos.EventDeviceButtonRepo;
import com.landau.core.events.repos.EventDeviceStreamRepo;
import com.landau.core.events.repos.EventDeviceSystemRepo;
import com.landau.core.events.services.EventService;
import com.landau.core.events.specs.EventDeviceButtonSpec;
import com.landau.core.events.specs.EventDeviceStreamSpec;
import com.landau.core.events.specs.EventDeviceSystemSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;


/**
 * Получение списка событий
 */
@Api(tags = "Список событий", description = "Контроллер обеспечивает получение списка событий")
@RestController("EventsIndex")
@RequestMapping("/events")
public class EventList extends Base {
    @Autowired
    private EventService eventService;

    @Autowired
    private EventDeviceButtonRepo eventDeviceButtonRepo;

    @Autowired
    private EventDeviceStreamRepo eventDeviceStreamRepo;

    @Autowired
    private EventDeviceSystemRepo eventDeviceSystemRepo;

    @GetMapping("/buttons/day")
    @Authority(value = User.class, required = true)
    public List<EventDeviceButton> buttonsEvent(@RequestParam("user") User user, @DateTimeFormat(pattern = "yyyy-MM-dd") Date day) {
        EventFilter eventFilter = new EventFilter();
        eventFilter.setUser(user);

        if(day != null) {
            eventFilter.setDay(day);
        }

        return eventDeviceButtonRepo.findAll(EventDeviceButtonSpec.find(eventFilter));
    }

    @GetMapping("buttons")
    @Authority(value = User.class, required = true)
    public Page<EventDeviceButton> buttonsEvent(Pageable pageable, @DateTimeFormat(pattern = "yyyy-MM-dd") Date day) {
        EventFilter eventFilter = new EventFilter();

        if(getUser().getRole() != User.Role.ADMIN) {
            eventFilter.setCompany(getUser().getCompany());
        }

        if(day != null) {
            eventFilter.setDay(day);
        }

        return eventDeviceButtonRepo.findAll(EventDeviceButtonSpec.find(eventFilter), pageable);
    }

    @GetMapping("streams")
    @Authority(value = User.class, required = true)
    public Page<EventDeviceStream> systemEvents(Pageable pageable, @DateTimeFormat(pattern = "yyyy-MM-dd") Date day) {
        EventFilter eventFilter = new EventFilter();

        if(getUser().getRole() != User.Role.ADMIN) {
            eventFilter.setCompany(getUser().getCompany());
        }

        if(day != null) {
            eventFilter.setDay(day);
        }

        return eventDeviceStreamRepo.findAll(EventDeviceStreamSpec.find(eventFilter), pageable);
    }

    @GetMapping("system")
    @Authority(value = User.class, required = true)
    public Page<EventDeviceSystem> systemStreams(Pageable pageable, @DateTimeFormat(pattern = "yyyy-MM-dd") Date day) {
        EventFilter eventFilter = new EventFilter();

        if(getUser().getRole() != User.Role.ADMIN) {
            eventFilter.setCompany(getUser().getCompany());
        }

        if(day != null) {
            eventFilter.setDay(day);
        }

        return eventDeviceSystemRepo.findAll(EventDeviceSystemSpec.find(eventFilter), pageable);
    }


    /**
     * Получение списка событий
     * @param eventFilter фильтрация списка
     * @param pageable настройки пагинации
     * @return
     */
/*
    @ApiOperation(value = "Получение списка событий пользователя")
    @GetMapping
    public Page<AbstractEvent> get(EventFilter eventFilter, Pageable pageable) {
        switch (eventFilter.getModule()) {
            case DEVICE:
                if (eventFilter.getDeviceEventType().equals(EventFilter.DeviceEventType.BUTTON)) {
                    Page<EventDeviceButton> page = controllerService.getEventDeviceButtonRepo()
                            .findAll(EventDeviceButtonSpec.find(eventFilter), pageable);

                    events = page.map(this::convertToIEvent);
                }
                else if (eventFilter.getDeviceEventType().equals(EventFilter.DeviceEventType.INSTALLER)) {
                    Page<EventDeviceInstaller> page = controllerService.getEventDeviceInstallerRepo()
                            .findAll(EventDeviceInstallerSpec.find(eventFilter), pageable);

                    events = page.map(this::convertToIEvent);
                }
                else if (eventFilter.getDeviceEventType().equals(EventFilter.DeviceEventType.STREAM)) {
                    Page<EventDeviceStream> page = controllerService.getEventDeviceStreamRepo()
                            .findAll(EventDeviceStreamSpec.find(eventFilter), pageable);

                    events = page.map(this::convertToIEvent);
                }
                else if (eventFilter.getDeviceEventType().equals(EventFilter.DeviceEventType.SYSTEM)) {
                    Page<EventDeviceSystem> page = controllerService.getEventDeviceSystemRepo()
                            .findAll(EventDeviceSystemSpec.find(eventFilter), pageable);

                    events = page.map(this::convertToIEvent);
                }

                break;
            case BIOMETRIC:
                Page<EventBiometricRecognition> page = controllerService.getEventBiometricRecognitionRepo()
                        .findAll(EventBiometricRecognitionSpec.find(eventFilter), pageable);


                break;
            case TERMINAL:
                if (controllerService.getUser().getRole() != User.Role.ADMIN){
                    eventFilter.getTerminal().setCompany(controllerService.getUser().getCompany());
                }

                if (eventFilter.getTerminalEventType().equals(EventFilter.TerminalEventType.SYNC)){
                    Page<EventTerminalSync> page = controllerService.getEventTerminalSyncRepo()
                            .findAll(EventTerminalSyncSpec.find(eventFilter), pageable);

                    events = page.map(this::convertToIEvent);
                }
                else if (eventFilter.getTerminalEventType().equals(EventFilter.TerminalEventType.SYSTEM)){
                    Page<EventTerminalSystem> page = controllerService.getEventTerminalSystemRepo()
                            .findAll(EventTerminalSystemSpec.find(eventFilter), pageable);

                    events = page.get().collect()
                }
                else if (eventFilter.getTerminalEventType().equals(EventFilter.TerminalEventType.FILE)){
                    Page<MediaFile> page = controllerService.getMediaFileRepo()
                            .findAll(MediaFileSpec.findByEvent(eventFilter), pageable);

                    events = page.map(this::convertToIEvent);
                }

                break;

            default:
                Page<EventDeviceSystem> page = controllerService.getEventDeviceSystemRepo()
                        .findAll(EventDeviceSystemSpec.find(eventFilter), pageable);
        }
    }
*/


    private AbstractEvent convertToIEvent(EventTerminalSystem eventTerminalSystem) {
        return eventTerminalSystem;
    }

    private AbstractEvent convertToIEvent(EventTerminalSync eventTerminalSync) {
        return eventTerminalSync;
    }

    private AbstractEvent convertToIEvent(EventDeviceSystem eventDeviceSystem) {
        return eventDeviceSystem;
    }

    private AbstractEvent convertToIEvent(EventDeviceStream eventDeviceStream) {
        return eventDeviceStream;
    }

    private AbstractEvent convertToIEvent(EventBiometricRecognition  eventBiometricRecognition){
        return eventBiometricRecognition;
    }

    private AbstractEvent convertToIEvent(EventDeviceButton eventDeviceButton){
        return eventDeviceButton;
    }

    private AbstractEvent convertToIEvent(EventDeviceInstaller eventDeviceInstaller){
        return eventDeviceInstaller;
    }


//
//    /**
//     * Получение списка событий
//     * @param eventsFilter фильтрация списка
//     * @param pageable настройки пагинации
//     * @return
//     */
//    @ApiOperation(value = "Получение списка событий")
//    @GetMapping("/find")
//    public Page<Event> get(EventsFilter eventsFilter, Pageable pageable) {
//        return eventRepo.findAll(EventsSpec.find(eventsFilter), pageable);
//    }

//
//    @ApiOperation(value = "Получение списка событий за месяц, с возможностью фильтрации по нескольким событиям для нескольких пользователей ")
//    @GetMapping("/findForMonth")
//    public List<EventsPerMonthDTO> get(@RequestParam(required = false) List<Long> com.landau.core.users,
//                                       @RequestParam(required = false) List<String> events,
//                                       @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
//
////        return eventRepo.findAll(EventsSpec.find(eventsFilter), pageable);
//        return eventService.getListPerMonth(com.landau.core.users, events, date);
//    }

}
