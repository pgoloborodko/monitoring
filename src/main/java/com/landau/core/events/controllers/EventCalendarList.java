package com.landau.core.events.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.events.entities.EventDeviceButton;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.events.repos.EventDeviceButtonRepo;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Api(tags = "Список событий для календаря", description = "Контроллер обеспечивает получение списка событий для календаря")
@RestController("EventCalendarList")
@RequestMapping("/events/calendar")
public class EventCalendarList extends Base {
    @Autowired
    private EventDeviceButtonRepo eventDeviceButtonRepo;

    @Autowired
    private MediaFileRepo mediaFileRepo;

    @Autowired
    EntityManager em;

    @GetMapping
    public List<JpaRecord> events(@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date startDate,
            @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date endDate) {

        // streams
        Query query = em.createNativeQuery("SELECT date_trunc('day', created) as day, count(*) \n" +
                "from event_device_stream \n" +
                "where (created between ? and ?) and type=?  \n" +
                ((getUser().getRole() != User.Role.ADMIN) ? " and company_id = ? " : "") +
                "group by 1");
        query.setParameter(1, startDate, TemporalType.DATE);
        query.setParameter(2, endDate, TemporalType.DATE);
        query.setParameter(3, EventDeviceStream.Type.START.ordinal());

        if(getUser().getRole() != User.Role.ADMIN) {
            query.setParameter(4, getUser().getCompany().getId());
        }

        List<JpaRecord> records = new ArrayList<>();
        for(Object[] obj: (List<Object[]>) query.getResultList()) {
            JpaRecord record = new JpaRecord((Timestamp) obj[0], (BigInteger) obj[1]);
            record.addType(JpaRecord.Type.STREAM);
            records.add(record);
        }

        // sos
        query = em.createNativeQuery("SELECT date_trunc('day', created) as day, count(*) \n" +
                "from event_device_button \n" +
                "where (created between ? and ?) and type=?  \n" +
                ((getUser().getRole() != User.Role.ADMIN) ? " and company_id = ? " : "") +
                "group by 1");
        query.setParameter(1, startDate, TemporalType.DATE);
        query.setParameter(2, endDate, TemporalType.DATE);
        query.setParameter(3, EventDeviceButton.Type.SOS.ordinal());

        if(getUser().getRole() != User.Role.ADMIN) {
            query.setParameter(4, getUser().getCompany().getId());
        }

        for(Object[] obj: (List<Object[]>) query.getResultList()) {
            JpaRecord record = new JpaRecord((Timestamp) obj[0], (BigInteger) obj[1]);
            record.addType(JpaRecord.Type.SOS);
            if(records.contains(record)) {
                records.get(records.indexOf(record)).addType(JpaRecord.Type.SOS);
            }
            else {
                records.add(record);
            }
        }

        // archive
        query = em.createNativeQuery("SELECT date_trunc('day', created) as day, count(*) \n" +
                "from media_file \n" +
                "where (created between ? and ?) \n" +
                ((getUser().getRole() != User.Role.ADMIN) ? " and company_id = ? " : "") +
                "group by 1");
        query.setParameter(1, startDate, TemporalType.DATE);
        query.setParameter(2, endDate, TemporalType.DATE);

        if(getUser().getRole() != User.Role.ADMIN) {
            query.setParameter(3, getUser().getCompany().getId());
        }

        for(Object[] obj: (List<Object[]>) query.getResultList()) {
            JpaRecord record = new JpaRecord((Timestamp) obj[0], (BigInteger) obj[1]);
            record.addType(JpaRecord.Type.ARCHIVE);
            if(records.contains(record)) {
                records.get(records.indexOf(record)).addType(JpaRecord.Type.ARCHIVE);
            }
            else {
                records.add(record);
            }
        }

/*        List<EventDeviceButton> events = eventDeviceButtonRepo.findAll(EventDeviceButtonSpec.find(eventFilter));
        List<CalendarRecord> records = new ArrayList<>();
        List<Integer> dayOfYear = new ArrayList<>();

        for(EventDeviceButton event : events) {
            Calendar calendarDate = Calendar.getInstance();
            calendarDate.setTime( event.getCreated());
            calendarDate.set(Calendar.HOUR_OF_DAY, getOffsetHours());
            calendarDate.set(Calendar.MINUTE, 0);
            calendarDate.set(Calendar.SECOND, 0);
            calendarDate.set(Calendar.MILLISECOND, 0);
            calendarDate.setTimeZone(TimeZone.getDefault());

            if(!dayOfYear.contains(calendarDate.get(Calendar.DAY_OF_YEAR))) {
                dayOfYear.add(calendarDate.get(Calendar.DAY_OF_YEAR));
                Calendar calendarDateEnd = (Calendar) calendarDate.clone();
                calendarDateEnd.setTime(calendarDate.getTime());
                calendarDateEnd.add(Calendar.DAY_OF_YEAR, 1);

                records.add(new CalendarRecord(event.getId(), calendar, calendarDate.getTime(), calendarDateEnd.getTime(), "СОС"));
            }
       }*/

        return records;
    }


    @Getter
    private static class JpaRecord {
        public enum Type {
            STREAM,
            SOS,
            ARCHIVE
        }

        public JpaRecord(Timestamp timestamp, BigInteger count) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(timestamp);
            this.day = calendar.get(Calendar.DAY_OF_MONTH);
            this.count = count;
        }

        private Integer day;
        private List<Type> types = new ArrayList<>();
        private BigInteger count;

        public void addType(Type type) {
            types.add(type);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            JpaRecord jpaRecord = (JpaRecord) o;
            return Objects.equals(day, jpaRecord.day);
        }

        @Override
        public int hashCode() {
            return Objects.hash(day);
        }
    }

    public static int getOffsetHours() {
        return (int) TimeUnit.MILLISECONDS.toHours(TimeZone.getDefault().getOffset(System.currentTimeMillis()));
    }
}
