package com.landau.core.cities.specs;

import com.landau.core.cities.entities.City;
import com.landau.core.cities.filters.CityFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class CitySpec {
    public static Specification<City> find(CityFilter cityFilter) {
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (cityFilter.getName() != null && !cityFilter.getName().isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")),cityFilter.getName().toUpperCase() +"%"));
            }

            if (cityFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("company"), cityFilter.getCompany()), criteriaBuilder.isNull(root.get("company"))));
            }

            if (cityFilter.getConcreteCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), cityFilter.getConcreteCompany()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
