package com.landau.core.cities.entities;

import com.landau.core.cities.repos.CityRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Город
 */
@Data
@Entity
@Table
@Repo(CityRepo.class)
@ApiModel(description = "Город")
public class City extends AbstractBusiness implements Serializable {
    @Column
    @ApiModelProperty(value = "Название города")
    private String name;

    @Column
    @ApiModelProperty(value = "Название региона")
    private String region;
}
