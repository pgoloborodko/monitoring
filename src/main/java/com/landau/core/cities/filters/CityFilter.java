package com.landau.core.cities.filters;

import com.landau.core.cities.entities.City;
import com.landau.core.companies.entities.Company;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityFilter extends City {
    private Company concreteCompany;
}
