package com.landau.core.cities.controllers;

import com.landau.core.cities.entities.City;
import com.landau.core.cities.repos.CityRepo;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Сохранение города
 */
@Api(tags = "Сохранение города", description = "Контроллер можкт получать данные города, а также создавать новый")
@RestController(value = "CitiesSave")
@RequestMapping("/cities/save")
public class CitySave extends Base {
    private CityRepo cityRepo;

    @Autowired
    public CitySave(CityRepo cityRepo) {
        this.cityRepo = cityRepo;
    }

    /**
     * Получение данных города
     * @param city id города
     * @return объект города
     */
    @ApiOperation(value = "Получение данных города", response = City.class)
    @Authority(City.class)
    @GetMapping
    public City get(City city) {
        return city;
    }

    /**
     * Сохранение данных города
     * @param city объект города
     * @param result
     * @return false с ошибкой, либо true с объектом города
     */
    @ApiOperation(value = "Сохранение данных города", response = City.class)
    @Authority(City.class)
    @PostMapping
    public City post(City city, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        if(city.getId() != null && city.getCompany() == null && getUser().getRole() != User.Role.ADMIN) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "not allowed to save");
        }

        if(getUser().getRole() != User.Role.ADMIN) {
            city.setCompany(getUser().getCompany());
        }

        return cityRepo.save(city);
    }
}

