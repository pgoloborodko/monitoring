package com.landau.core.cities.controllers;

import com.landau.core.cities.entities.City;
import com.landau.core.cities.filters.CityFilter;
import com.landau.core.cities.repos.CityRepo;
import com.landau.core.cities.specs.CitySpec;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список городов
 */
@RestController("CitiesIndex")
@RequestMapping("/cities")
@Api(tags="Список городов", description = "Получение списка городов по фильтру")
public class CityList extends Base {
    private CityRepo cityRepo;

    @Autowired
    public CityList(CityRepo cityRepo) {
        this.cityRepo = cityRepo;
    }

    /**
     * Получение списка городов по фильтру
     * @return список городов
     */
    @ApiOperation(value = "Получение списка городов по фильтру", response = City.class)
    @GetMapping
    public Page<City> get(Pageable pageable, CityFilter cityFilter) {
        if(getUser().getRole() != User.Role.ADMIN) {
            cityFilter.setCompany(getUser().getCompany());
        }

        return cityRepo.findAll(CitySpec.find(cityFilter), pageable);
    }
}