package com.landau.core.cities.repos;

import com.landau.core.cities.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CityRepo extends JpaRepository<City, Long>, JpaSpecificationExecutor<City> {
}
