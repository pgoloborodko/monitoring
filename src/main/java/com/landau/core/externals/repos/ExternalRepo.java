package com.landau.core.externals.repos;

import com.landau.core.externals.entities.External;
import com.landau.core.externals.filters.ExternalFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;



public interface ExternalRepo extends JpaRepository<External, Long>, JpaSpecificationExecutor<ExternalFilter> {

}
