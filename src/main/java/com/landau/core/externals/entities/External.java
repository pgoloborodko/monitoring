package com.landau.core.externals.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.externals.repos.ExternalRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Сущность внешней базы данных
 */
@Getter
@Setter
@Entity
@JsonInclude
@Repo(ExternalRepo.class)
@ApiModel(description = "Сущность внешней базы данных")
public class External extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Название внешней БД")
    private String name;

    @Column
    @ApiModelProperty(value = "url внешней БД")
    private String address;

    @Column
    @ApiModelProperty(value = "Логин к внешней БД")
    private String login;

    @Column
    @ApiModelProperty(value = "Пароль от внешней БД")
    private String password;

    @Column
    @ApiModelProperty(value = "Порт подлючения внешней БД")
    private Integer port;
}
