package com.landau.core.externals.specs;

import com.landau.core.externals.filters.ExternalFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class ExternalSpec {
    public static Specification<ExternalFilter> find(ExternalFilter externalDBFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (externalDBFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), externalDBFilter.getCompany()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
