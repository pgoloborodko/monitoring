package com.landau.core.externals.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.externals.entities.External;
import com.landau.core.externals.repos.ExternalRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Изменение и просмотр данных внешних БД
 */
@Api(tags = "Сохранение данных внешних БД", description = "Контроллер может получить данные внешней БД, а также создать и сохранить новую")
@RestController("ExternalDBSave")
@RequestMapping("/externals/save")
public class ExternalSave extends Base {
    private ExternalRepo externalDBRepo;

    @Autowired
    public ExternalSave(ExternalRepo externalDBRepo) {
        this.externalDBRepo = externalDBRepo;
    }

    /**
     * Получение данных внешней БД
     * @param external внешняя БД
     * @return {@link External}
     */
    @ApiOperation(value = "Получение данных внешней БД", response = External.class)
    @Authority(External.class)
    @GetMapping
    public External get(External external) {
        return external;
    }

    /**
     * Сохранение данных внешней БД
     * @param external внешняя БД
     * @param result не используется
     * @return {@link External}
     */
    @ApiOperation(value = "Сохранение данных внешней БД", response = External.class)
    @Authority(External.class)
    @PostMapping
    public External post(External external, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        if(getUser().getRole() != User.Role.ADMIN) {
            external.setCompany(getUser().getCompany());
        }

        return externalDBRepo.save(external);
    }
}
