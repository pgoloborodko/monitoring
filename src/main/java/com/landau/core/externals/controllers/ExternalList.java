package com.landau.core.externals.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.externals.entities.External;
import com.landau.core.externals.filters.ExternalFilter;
import com.landau.core.externals.repos.ExternalRepo;
import com.landau.core.externals.specs.ExternalSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Получение списка внешних БД
 */
@Api(tags = "Список внешних БД", description = "Контроллер для получения списка внешних БД")
@RestController("ExternalDBIndex")
@RequestMapping("/externals")
public class ExternalList extends Base {
    private ExternalRepo externalRepo;

    @Autowired
    public ExternalList(ExternalRepo externalRepo) {
        this.externalRepo = externalRepo;
    }

    /**
     * Получение списка внешних БД по фильтру
     * @param externalDBFilter поля фильтра
     * @return список устройств
     */
    @ApiOperation(value = "Получение списка внешних БД по фильтру", response = External.class)
    @GetMapping
    public Page<ExternalFilter> get(ExternalFilter externalDBFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            externalDBFilter.setCompany(getUser().getCompany());
        }

        return externalRepo.findAll(ExternalSpec.find(externalDBFilter), pageable);
    }
}
