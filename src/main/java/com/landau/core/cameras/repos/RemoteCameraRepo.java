package com.landau.core.cameras.repos;

import com.landau.core.cameras.entities.RemoteCamera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface RemoteCameraRepo extends JpaRepository<RemoteCamera, Long>, JpaSpecificationExecutor<RemoteCamera> {
    Optional<RemoteCamera> findBySettingId(String settingId);
    Optional<RemoteCamera> findByRtsp(String rtsp);
}
