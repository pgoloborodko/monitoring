package com.landau.core.cameras.specs;

import com.landau.core.cameras.entities.RemoteCamera;
import com.landau.core.cameras.filters.RemoteCameraFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class RemoteCameraSpec {
    public static Specification<RemoteCamera> find(RemoteCameraFilter remoteCameraFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (remoteCameraFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), remoteCameraFilter.getCompany()));
            }

            if (remoteCameraFilter.getId() != null) {
                predicates.add(cb.equal(root.get("id"), remoteCameraFilter.getId()));
            }

            if (remoteCameraFilter.getName() != null){
                predicates.add(cb.equal(root.get("name"), remoteCameraFilter.getName()));
            }

            if (remoteCameraFilter.getRtsp() != null){
                predicates.add(cb.equal(root.get("rtsp"), remoteCameraFilter.getRtsp()));
            }

            if(remoteCameraFilter.getCameraLocation() != null) {
                if (remoteCameraFilter.getCameraLocation().getLongitude() != null){
                    predicates.add(cb.equal(root.get("cameraLocation").get("longitude"), remoteCameraFilter.getCameraLocation().getLongitude()));
                }

                if (remoteCameraFilter.getCameraLocation().getLatitude() != null){
                    predicates.add(cb.equal(root.get("cameraLocation").get("latitude"), remoteCameraFilter.getCameraLocation().getLatitude()));
                }
            }

            if (remoteCameraFilter.getState() != null){
                predicates.add(cb.equal(root.get("state"), remoteCameraFilter.getState()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
