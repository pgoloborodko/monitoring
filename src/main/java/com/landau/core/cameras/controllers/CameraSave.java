package com.landau.core.cameras.controllers;

import com.landau.core.cameras.dto.CameraInfoDTO;
import com.landau.core.cameras.dto.CameraSettingDTO;
import com.landau.core.cameras.entities.RemoteCamera;
import com.landau.core.cameras.repos.RemoteCameraRepo;
import com.landau.core.cameras.services.CameraService;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Slf4j
@RequestMapping("/cameras")
@RestController("SaveCameraController")
@Api(tags = "Сохранение/Изменение камер",
        description = "Контроллер обеспечивает сохранение и обновление данных удаленных камер")
public class CameraSave extends Base {
    private CameraService cameraService;
    private RemoteCameraRepo remoteCameraRepo;

    @Autowired
    public CameraSave(CameraService cameraService, RemoteCameraRepo remoteCameraRepo) {
        this.cameraService = cameraService;
        this.remoteCameraRepo = remoteCameraRepo;
    }

    @ApiOperation(value = "Сохранение/Изменение камеры в системе", response = RemoteCamera.class)
    @Authority(value = RemoteCamera.class)
    @PostMapping("/save")
    public RemoteCamera saveCamera(RemoteCamera remoteCamera, CameraSettingDTO cameraSettingDTO){
        if (getUser().getRole() != User.Role.ADMIN){
            remoteCamera.setCompany(getUser().getCompany());
        }

        if (remoteCamera.getIp() != null
                && remoteCamera.getLogin() != null
                && remoteCamera.getPassword() != null){
            return cameraService.prepareONVIF(remoteCamera, cameraSettingDTO);
        } else if (remoteCamera.getRtsp() != null){
            return cameraService.prepareRTSP(remoteCamera, cameraSettingDTO);
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "wrong authorization data");
    }


    @ApiOperation(value = "Просмотр камеры в системе", response = RemoteCamera.class)
    @Authority(value = RemoteCamera.class)
    @GetMapping("/getInfo")
    public CameraInfoDTO getRemoteCamera(@RequestParam("cameraId") RemoteCamera remoteCamera){
        Optional<RemoteCamera> optionalRemoteCamera = remoteCameraRepo.findById(remoteCamera.getId());

        if (optionalRemoteCamera.isPresent()){
            CameraInfoDTO cameraInfoDTO = new CameraInfoDTO();

            cameraInfoDTO.setRemoteCamera(optionalRemoteCamera.get());
            cameraInfoDTO.setCameraSetting(cameraService.requestGetToNimble(optionalRemoteCamera.get()));

            return cameraInfoDTO;
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "404");
        }
    }
}
