package com.landau.core.cameras.controllers;

import com.landau.core.cameras.entities.RemoteCamera;
import com.landau.core.cameras.filters.RemoteCameraFilter;
import com.landau.core.cameras.repos.RemoteCameraRepo;
import com.landau.core.cameras.specs.RemoteCameraSpec;
import com.landau.core.common.ApiPageable;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController("CameraList")
@Api(tags = "Список камер",
        description = "Контроллер обеспечивает выдачу списка камер по фильтру с пагинацией")
@RequestMapping("/cameras")
public class CameraList extends Base {
    @Autowired
    private RemoteCameraRepo remoteCameraRepo;

    @ApiPageable
    @GetMapping
    @ApiOperation(value = "Получение списка камер по фильтру с пагинацией", response = RemoteCamera.class)
    public Page<RemoteCamera> getAllCameras(RemoteCameraFilter remoteCameraFilter, Pageable pageable){
        if (getUser().getRole() != User.Role.ADMIN){
            if (getUser().getCompany().getId() != null){
                remoteCameraFilter.setCompany(getUser().getCompany());
            }
        }
        return remoteCameraRepo.findAll(RemoteCameraSpec.find(remoteCameraFilter), pageable);
    }
}
