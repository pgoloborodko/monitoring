package com.landau.core.cameras.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.landau.core.cameras.repos.RemoteCameraRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.locations.entities.CameraLocation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table
@Repo(RemoteCameraRepo.class)
@ApiModel(description = "Сущность камеры")
public class RemoteCamera extends AbstractBusiness {
    public enum State{
        OFFLINE,
        ONLINE,
        ERROR
    }

    @Column
    @ApiModelProperty(value = "имя камеры")
    private String name;

    @Column
    @ApiModelProperty(value = "ip-адрес камеры")
    private String ip;

    @Column
    @ApiModelProperty(value = "логин для авторизации в интерфейсе камеры")
    private String login;

    @Column
    @ApiModelProperty(value = "пароль для авторизации в интерфейсе камеры")
    private String password;

    @Column
    @ApiModelProperty(value = "rtsp-ссылка")
    private String rtsp;

    @JsonIgnore
    @ManyToOne
    @ApiModelProperty(value = "Местополодение камеры")
    private CameraLocation cameraLocation;

    @Column
    @ApiModelProperty(value = "Состояние камеры")
    private State state;

    @Column
    @ApiModelProperty(value = "Дата создания записи")
    private Date created;

    @Column
    @ApiModelProperty(value = "Идентификатор камеры в NimbleStreamer")
    private String settingId;
}
