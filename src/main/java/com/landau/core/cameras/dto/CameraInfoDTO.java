package com.landau.core.cameras.dto;

import com.landau.core.cameras.entities.RemoteCamera;
import lombok.Data;

@Data
public class CameraInfoDTO {
    private RemoteCamera remoteCamera;
    private CameraSetting cameraSetting;
}
