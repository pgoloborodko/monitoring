package com.landau.core.cameras.dto;

import lombok.Data;

@Data
public class NimbleResponseDTO {
    private String status;

    private CameraSetting setting;
}
