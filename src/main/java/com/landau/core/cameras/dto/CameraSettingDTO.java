package com.landau.core.cameras.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CameraSettingDTO {
    public enum RTSP_MODE{
        TCP_PREFERRED,
        UDP_PREFERRED,
        TCP_ONLY,
        UDP_ONLY,
        RTSP_OVER_HTTP
    }

    @ApiModelProperty(value = "Список резервных URL-адресов входящего потока RTSP для переключения в случае недоступности основного URL-адреса.")
    private String[] fallback_urls;

    @ApiModelProperty(value = "Имя приложения для исходящих потоков.")
    private String application;

    @ApiModelProperty(value = "Имя потока для исходящих потоков.")
    private String stream;

    @ApiModelProperty(value = "Является ли данная настройка в данный момент деактивированной.")
    private Boolean paused;

    @ApiModelProperty(value = "Может быть «TCP_PREFERRED», «UDP_PREFERRED», «TCP_ONLY», «UDP_ONLY» или «RTSP_OVER_HTTP». По умолчанию «TCP_PREFERRED»")
    private RTSP_MODE rtsp_mode;

    @ApiModelProperty(value = "Должно ли это обновление немедленно применяться соответствующим сервером Nimble, сбрасывая все соответствующие потоки.")
    private Boolean apply_immediately;

    @ApiModelProperty(value = "Время ожидания (в секундах) Начиная с того момента, когда никто не запрашивает указанный поток, сервер продолжает извлекать этот поток в течение интервала времени ожидания, а затем отключает его.")
    private Long idle_time;

    @ApiModelProperty(value = "Указывает, пытается ли сервер извлечь какой-либо поток, принадлежащий указанному приложению, из указанного источника в случае его запроса.")
    private Boolean dynamic_stream;

    @ApiModelProperty(value = "Порт прокси HTTP. Может быть установлен, только если rtsp_mode - RTSP_OVER_HTTP.")
    private Boolean http_proxy;

    @ApiModelProperty(value = "Описание изменений в настройках")
    private String description;
}
