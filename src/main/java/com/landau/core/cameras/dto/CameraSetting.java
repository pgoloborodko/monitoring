package com.landau.core.cameras.dto;

import lombok.Data;

@Data
public class CameraSetting {
    private String id ;
    private String url;
    private String[] fallback_urls;
    private String application;
    private String stream;
    private boolean paused;
    private String rtsp_mode;
    private long idle_time;
    private boolean dynamic_stream;
    private String description;
}
