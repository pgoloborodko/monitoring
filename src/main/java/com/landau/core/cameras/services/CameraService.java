package com.landau.core.cameras.services;

import be.teletask.onvif.OnvifManager;
import be.teletask.onvif.listeners.OnvifResponseListener;
import be.teletask.onvif.models.OnvifDevice;
import be.teletask.onvif.responses.OnvifResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.landau.core.cameras.dto.CameraSetting;
import com.landau.core.cameras.dto.CameraSettingDTO;
import com.landau.core.cameras.dto.NimbleResponseDTO;
import com.landau.core.cameras.entities.RemoteCamera;
import com.landau.core.cameras.repos.RemoteCameraRepo;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class CameraService implements OnvifResponseListener {
    @Value("${streamer.serverId}")
    private String serverId;

    @Value("${streamer.clientId}")
    private String clientId;

    @Value("${streamer.apiKey}")
    private String apiKey;

    private RemoteCameraRepo remoteCameraRepo;

    @Autowired
    public CameraService(RemoteCameraRepo remoteCameraRepo){
        this.remoteCameraRepo = remoteCameraRepo;
    }

    /**
     * Функция отвечает за работу взаимодействие пользователя с Nimble PULL API в том случае,
     * если значение параметра rtsp запроса RemoteCamera не рано null.
     * Если значение параметра id запроса RemoteCamera позволяет получить из БД запись с данными камеры,
     * выполняется запрос на обновление записи в Nimble, а также обновление записи в БД.
     * Если значение параметра id запроса RemoteCamera не позволяет получить из БД запись с данными камеры,
     * однако при этом значение параметра settingId позволяет получить из БД запись с данными камеры -
     * выполняется запрос на обновление записи в Nimble, а также обновление полученной записи в БД.
     * Если значение параметра id запроса RemoteCamera не позволяет получить из БД запись с данными камеры,
     * и значение параметра settingId не позволяет получить из БД запись с данными камеры,
     * однако значение settingId не равно null -
     * выполняется запрос на обновление записи в Nimble, а также создание записи в БД.
     * Если значение параметра id запроса RemoteCamera не позволяет получить из БД запись с данными камеры,
     * и значение параметра settingId равно null -
     * выполняется запрос на создание записи в Nimble, а также создание записи в БД.
     * @param remoteCamera тело запроса с параметрами RemoteCamera
     * @param cameraSettingDTO тело запроса с параметрами CameraSettingDTO
     *  @return RemoteCamera
     * @exception ResponseStatusException
     */
    public RemoteCamera prepareRTSP(RemoteCamera remoteCamera, CameraSettingDTO cameraSettingDTO) {
        if (remoteCamera.getId() != null && remoteCameraRepo.findById(remoteCamera.getId()).isPresent() && remoteCamera.getSettingId() != null){
            return requestUpdateToNimble(remoteCamera, cameraSettingDTO);
        }
        else if (remoteCamera.getId() == null && remoteCamera.getSettingId() != null){
            Optional<RemoteCamera> optionalRemoteCamera = remoteCameraRepo.findBySettingId(remoteCamera.getSettingId());

            RemoteCamera tempCamera;
            if (optionalRemoteCamera.isPresent()){
                tempCamera = optionalRemoteCamera.get();
            }
            else {
                tempCamera = new RemoteCamera();
            }

            tempCamera.setName(remoteCamera.getName());
            tempCamera.setRtsp(remoteCamera.getRtsp());
            tempCamera.setCompany(remoteCamera.getCompany());
            tempCamera.setCameraLocation(remoteCamera.getCameraLocation());
            tempCamera.setCreated(remoteCamera.getCreated());
            tempCamera.setSettingId(remoteCamera.getSettingId());
            tempCamera.setState(remoteCamera.getState());

            return requestUpdateToNimble(tempCamera, cameraSettingDTO);
        }
        else if (remoteCamera.getId() == null && remoteCamera.getSettingId() == null){
            return requestCreateToNimble(remoteCamera, cameraSettingDTO);
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "wrong settings");
        }

    }

    /**
     * Функция отвечает за взаимодействие с камерой по средствам onvif протокола.
     * С помощью параметров авторизации осуществляется подключение к устройству по средствам onvif протокола,
     * далее выполняется попытка генерации rtsp url.
     * В случае успешной генерации rtsp url выполняется запрос на поиск в бд записи, атрибут rtsp которой идентичен сгенерированному.
     * Если такая запись существует параметр id запроса remoteCamera становится равен атрибуту id полученной записи.
     * Выполняется переход к взаимодействию с Nimble.
     * В случае ошибки генерации rtsp url функция возвращает ошибку типа ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error while getting rtsp")
     * @param remoteCamera тело запроса с параметрами RemoteCamera
     * @param cameraSettingDTO тело запроса с параметрами CameraSettingDTO
     * @return RemoteCamera
     * @exception ResponseStatusException
     */
    public RemoteCamera prepareONVIF(RemoteCamera remoteCamera, CameraSettingDTO cameraSettingDTO){
        OnvifManager onvifManager = new OnvifManager();
        onvifManager.setOnvifResponseListener(this);

        OnvifDevice onvifDevice = new OnvifDevice(remoteCamera.getIp(), remoteCamera.getLogin(), remoteCamera.getPassword());
        onvifManager.getDeviceInformation(onvifDevice, (onvifDevice13, onvifDeviceInformation) ->
                remoteCamera.setName(onvifDeviceInformation.getModel() + "_" + onvifDeviceInformation.getSerialNumber()));

        onvifManager.getMediaProfiles(onvifDevice, (onvifDevice12, list) ->
                onvifManager.getMediaStreamURI(onvifDevice12, list.get(0), (onvifDevice1, onvifMediaProfile, s) ->
                        remoteCamera.setRtsp(s)));

        if (remoteCamera.getRtsp() == null){
            for (int i = 0;remoteCamera.getRtsp() == null && i < 10; i++){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (remoteCamera.getRtsp() != null){
            if (remoteCamera.getName() != null && !remoteCamera.getName().isEmpty()){
                cameraSettingDTO.setStream(remoteCamera.getName().split("_")[1]);
            }
            else {
                cameraSettingDTO.setStream(UUID.randomUUID().toString());
            }

            try {
                URI uri = new URI(remoteCamera.getRtsp());
                String rtspUrl = uri.getScheme() + "://" + onvifDevice.getUsername() + ":" + onvifDevice.getPassword() +
                        "@" + uri.getAuthority() + uri.getPath() + "?" + uri.getQuery();
                remoteCamera.setRtsp(rtspUrl);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            Optional<RemoteCamera> optionalRemoteCamera = remoteCameraRepo.findByRtsp(remoteCamera.getRtsp());

            if (optionalRemoteCamera.isPresent()){
                remoteCamera.setId(optionalRemoteCamera.get().getId());
            }

            return prepareRTSP(remoteCamera, cameraSettingDTO);
        }
        else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error while getting rtsp");
        }
    }

    private Map<String, String> getAuthority(String onvif){
        Map<String, String> authority = new HashMap<>();
        try {
            URI uri = new URI(onvif);
            if (uri.getUserInfo() != null){
                String[] userPassword = uri.getUserInfo().split(":");
                authority.put("user", userPassword[0]);
                authority.put("password", userPassword[1]);
            }
            else {
                log.warn("userInfo is empty");
            }
            authority.put("host", uri.getHost());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return authority;
    }

    /**
     * Функция отправляет запрос типа UPDATE с помощью средств Nimble PULL API, а также обновляет/создает запись в БД.
     * @param remoteCamera тело запроса с параметрами RemoteCamera
     * @param cameraSettingDTO тело запроса с параметрами CameraSettingDTO
     * @return RemoteCamera
     * @exception ResponseStatusException
     */
    private RemoteCamera requestUpdateToNimble(RemoteCamera remoteCamera, CameraSettingDTO cameraSettingDTO){
        String updateUrl = "https://api.wmspanel.com/v1/server/" + serverId + "/rtsp/live_pull/" + remoteCamera.getSettingId()
                + "/?client_id=" + clientId + "&api_key=" + apiKey;

        Map<String, Object> jsonMap = new HashMap<>();
        if (remoteCamera.getRtsp() != null){
            jsonMap.put("url", remoteCamera.getRtsp());
        }
        if (cameraSettingDTO.getFallback_urls() != null){
            jsonMap.put("fallback_urls", cameraSettingDTO.getFallback_urls());
        }
        if (cameraSettingDTO.getApplication() != null){
            jsonMap.put("application", cameraSettingDTO.getApplication());
        }
        if (cameraSettingDTO.getStream() != null){
            jsonMap.put("stream", cameraSettingDTO.getStream());
        }
        if (cameraSettingDTO.getPaused() != null){
            jsonMap.put("paused", cameraSettingDTO.getPaused());
        }
        if (cameraSettingDTO.getRtsp_mode() != null){
            jsonMap.put("rtsp_mode", cameraSettingDTO.getRtsp_mode().name());
        }
        if (cameraSettingDTO.getApply_immediately() != null){
            jsonMap.put("apply_immediately", cameraSettingDTO.getApply_immediately());
        }
        if (cameraSettingDTO.getIdle_time() != null){
            jsonMap.put("idle_time", cameraSettingDTO.getIdle_time());
        }
        if (cameraSettingDTO.getDynamic_stream() != null){
            jsonMap.put("dynamic_stream", cameraSettingDTO.getDynamic_stream());
        }
        if (cameraSettingDTO.getRtsp_mode() != null && cameraSettingDTO.getRtsp_mode() == CameraSettingDTO.RTSP_MODE.RTSP_OVER_HTTP){
            jsonMap.put("http_proxy", true);
        }
        else if (cameraSettingDTO.getRtsp_mode() != null){
            jsonMap.put("http_proxy", false);
        }
        if (cameraSettingDTO.getDescription() != null){
            jsonMap.put("description", cameraSettingDTO.getDescription());
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = null;
        try {
            request = new HttpEntity<>(new ObjectMapper().writeValueAsString(jsonMap), headers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<NimbleResponseDTO> response = restTemplate.exchange(updateUrl, HttpMethod.PUT, request, NimbleResponseDTO.class);
        
        if (response.getBody() != null){
            if (response.getBody().getStatus().equals("Ok")){
                remoteCamera.setSettingId(response.getBody().getSetting().getId());
            }
            else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "nimble: " + response.getBody().getStatus());
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Nimble response body is empty");
        }
        
        return remoteCameraRepo.save(remoteCamera);
    }

    /**
     * Функция отправляет запрос типа CREATE с помощью средств Nimble PULL API, а также обновляет/создает запись в БД.
     * @param remoteCamera тело запроса с параметрами RemoteCamera
     * @param cameraSettingDTO тело запроса с параметрами CameraSettingDTO
     * @return RemoteCamera
     * @exception ResponseStatusException
     */
    private RemoteCamera requestCreateToNimble(RemoteCamera remoteCamera, CameraSettingDTO cameraSettingDTO){
        String createUrl = "https://api.wmspanel.com/v1/server/" + serverId + "/rtsp/live_pull?client_id="
                + clientId + "&api_key=" + apiKey;

        Map<String, Object> jsonMap = new HashMap<>();
        if (remoteCamera.getRtsp() != null){
            jsonMap.put("url", remoteCamera.getRtsp());
        }
        if (cameraSettingDTO.getFallback_urls() != null){
            jsonMap.put("fallback_urls", cameraSettingDTO.getFallback_urls());
        }
        if (cameraSettingDTO.getApplication() != null){
            jsonMap.put("application", cameraSettingDTO.getApplication());
        }
        if (cameraSettingDTO.getStream() != null){
            jsonMap.put("stream", cameraSettingDTO.getStream());
        }
        if (cameraSettingDTO.getPaused() != null){
            jsonMap.put("paused", cameraSettingDTO.getPaused());
        }
        if (cameraSettingDTO.getRtsp_mode() != null){
            jsonMap.put("rtsp_mode", cameraSettingDTO.getRtsp_mode().name());
        }
        if (cameraSettingDTO.getIdle_time() != null){
            jsonMap.put("idle_time", cameraSettingDTO.getIdle_time());
        }
        if (cameraSettingDTO.getDynamic_stream() != null){
            jsonMap.put("dynamic_stream", cameraSettingDTO.getDynamic_stream());
        }
        if (cameraSettingDTO.getRtsp_mode() != null && cameraSettingDTO.getRtsp_mode() == CameraSettingDTO.RTSP_MODE.RTSP_OVER_HTTP){
            jsonMap.put("http_proxy", true);
        }
        else if (cameraSettingDTO.getRtsp_mode() != null){
            jsonMap.put("http_proxy", false);
        }
        if (cameraSettingDTO.getDescription() != null){
            jsonMap.put("description", cameraSettingDTO.getDescription());
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = null;
        try {
            request = new HttpEntity<>(new ObjectMapper().writeValueAsString(jsonMap), headers);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<NimbleResponseDTO> response = restTemplate.postForEntity(createUrl, request, NimbleResponseDTO.class);

        if (response.getBody() != null){
            if (response.getBody().getStatus().equals("Ok")){
                remoteCamera.setSettingId(response.getBody().getSetting().getId());
            }
            else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "nimble: " + response.getBody().getStatus());
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Nimble response body is empty");
        }

        return remoteCameraRepo.save(remoteCamera);
    }


    /**
     * Функция отправляет запрос типа GET с помощью средств Nimble PULL API
     * Возвращает контейнер данных типа CameraSetting для дальнейшего ответа сервера на пользовательский запрос
     * @param remoteCamera тело запроса с параметрами RemoteCamera
     * @return CameraSetting.class
     * @exception ResponseStatusException
     */
    public CameraSetting requestGetToNimble(RemoteCamera remoteCamera){
        String getUrl = "https://api.wmspanel.com/v1/server/" + serverId + "/rtsp/live_pull/" + remoteCamera.getSettingId()
                + "/?client_id=" + clientId + "&api_key=" + apiKey;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<NimbleResponseDTO> response = restTemplate.getForEntity(getUrl, NimbleResponseDTO.class);

        if (response.getBody() != null){
            if (response.getBody().getStatus().equals("Ok")){
                return response.getBody().getSetting();
            }
            else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "nimble: " + response.getBody().getStatus());
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Nimble response body is empty");
        }

    }

    @Override
    public void onResponse(@NotNull OnvifDevice onvifDevice, @NotNull OnvifResponse onvifResponse) {

    }

    @Override
    public void onError(@NotNull OnvifDevice onvifDevice, int i, String s) {

    }
}
