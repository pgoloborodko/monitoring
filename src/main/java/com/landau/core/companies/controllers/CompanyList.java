package com.landau.core.companies.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.companies.entities.Company;
import com.landau.core.companies.filters.CompanyFilter;
import com.landau.core.companies.repos.CompanyRepo;
import com.landau.core.companies.specs.CompanySpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список компаний
 */
@Api(tags = "Список компаний", description = "Контроллер отвечает за получение списка компаний")
@RestController("CompanyIndex")
@RequestMapping("/companies")
public class CompanyList extends Base {
    private CompanyRepo companyRepo;

    @Autowired
    public CompanyList(CompanyRepo companyRepo) {
        this.companyRepo = companyRepo;
    }

    /**
     * Получение списка компаний
     * @param companyFilter фильтр компании
     * @return список компаний
     */
    @ApiOperation(value = "Получение списка компаний", response = Company.class)
    @GetMapping
    public Page<Company> get(Pageable pageable, CompanyFilter companyFilter) {
        if(getUser().getRole() != User.Role.ADMIN) {
            companyFilter.setId(getUser().getCompany().getId());
        }

        return companyRepo.findAll(CompanySpec.find(companyFilter), pageable);
    }
}
