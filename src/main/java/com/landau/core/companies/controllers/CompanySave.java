package com.landau.core.companies.controllers;

import com.keymus.license.Signature;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.common.exceptions.EyepanelValidationException;
import com.landau.core.companies.entities.Company;
import com.landau.core.companies.repos.CompanyRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

/**
 * Сохранение компании
 */
@Api(tags = "Сохранение компании", description = "Контроллер может получать данные компании, а также создавать и сохранять новые")
@RestController(value = "CompanySave")
@RequestMapping("/companies/save")
@Slf4j
public class CompanySave extends Base {
    private CompanyRepo companyRepo;

    @Value("${server.license.public}")
    private Resource publicLicenseKey;

    @Autowired
    public CompanySave(CompanyRepo companyRepo) {
        this.companyRepo = companyRepo;
    }

    /**
     * Получение данных компании
     *
     * @param company id компании
     * @return объект компании
     */
    @ApiOperation(value = "Получение данных компании", response = Company.class)
    @Authority(Company.class)
    @GetMapping
    public Company get(Company company) {
        return company;
    }

    /**
     * Сохранение данных компании
     *
     * @param company объект компании
     * @param result BindingResult
     * @return false с ошибкой, либо true с объектом компании
     */
    @ApiOperation(value = "Сохранение данных компании", response = Company.class)
    @Authority(Company.class)
    @PostMapping
    public Company post(@Valid Company company,
                        BindingResult result) throws EyepanelValidationException, IOException, IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if (result.hasErrors() /*|| getUser().getRole() != User.Role.ADMIN*/) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }
        MultipartFile sign = company.getLicenseFile();
        if (sign != null && !sign.isEmpty()) {
            Signature signature = Signature.readSignature(sign.getInputStream());
            if (!signature.checkSign(publicLicenseKey.getInputStream())){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "signature not verified");
            }
            if (company.getSign() != null){
                signature.concatSign(company.getSign());
            }
            company.setSign(signature);
        }
        final Company save = companyRepo.save(company);


        return save;
    }
}
