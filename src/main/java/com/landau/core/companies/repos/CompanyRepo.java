package com.landau.core.companies.repos;

import com.landau.core.companies.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CompanyRepo extends JpaRepository<Company, Long>, JpaSpecificationExecutor<Company> {
}
