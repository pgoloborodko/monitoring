package com.landau.core.companies.specs;

import com.landau.core.companies.entities.Company;
import com.landau.core.companies.filters.CompanyFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class CompanySpec {
    public static Specification<Company> find(CompanyFilter companyFilter) {
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (companyFilter.getName() != null && !companyFilter.getName().isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")),companyFilter.getName().toUpperCase() +"%"));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
