package com.landau.core.companies.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.keymus.license.Signature;
import com.landau.core.SiteApplication;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.companies.repos.CompanyRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.StringJoiner;
import java.util.UUID;

/**
 * Компания
 */
@Entity
@Table
@Slf4j
@Repo(CompanyRepo.class)
@ApiModel(description = "Модель компании")
public class Company extends AbstractId implements Serializable {
    /**
     * Тип карты
     */
    enum MapType {
        /**
         * Тип карты "Яндекс"
         */
        YANDEX,
        /**
         * Тип карты "Метро"
         */
        METRO,
        /**
         * Тип карты "Google"
         */
        GOOGLE
    }

    @Column
    @ApiModelProperty(value = "Название компании")
    private String name;

    @Transient
    @JsonIgnore
    @ApiModelProperty(value = "Файл логотипа компании")
    private MultipartFile logoFile;

    @Column
    @ApiModelProperty(value = "Логотип компании")
    private String logo;

    @Column
    @ApiModelProperty(value = "Цвет")
    private String color;

    @Column
    @ApiModelProperty(value = "Стрим на компанию")
    private Boolean grantStreaming;

    @Column
    private Boolean simpleIdenty = false;

    @Column
    @ApiModelProperty(value = "Периодический стрим")
    private Boolean periodic;

    @Column
    @ApiModelProperty(value = "Тип карты")
    private MapType map;

    @Transient
    @JsonIgnore
    @ApiModelProperty(value = "Файл лицензии")
    private MultipartFile licenseFile;

    @JsonIgnore
    @ApiModelProperty(value = "Байтовый массив файла лицензии")
    private byte[] sign;

    @ManyToOne
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true) // otherwise first ref as POJO, others as id
    @ApiModelProperty(value = "Родительская компания")
    private Company parent;

    /**
     * Родительская компания
     * @return
     */
    public Company getParent() {
        return parent;
    }
    public void setParent(Company parent) {
        this.parent = parent;
    }

    /**
     * Название компании
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Разрешение на стрим
     * @return
     */
    public Boolean getGrantStreaming() {
        return grantStreaming;
    }

    public void setGrantStreaming(Boolean grantStreaming) {
        this.grantStreaming = grantStreaming;
    }

    /**
     * Путь к файлу логотипа
     * @return
     */
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * Файл лицензии
     * @return
     */
    public MultipartFile getLicenseFile() {
        return licenseFile;
    }

    public void setLicenseFile(MultipartFile licenseFile) {
        this.licenseFile = licenseFile;
    }

    /**
     * Файл логотипа
     * @return
     */
    public MultipartFile getLogoFile() {
        return logoFile;
    }

    public void setLogoFile(MultipartFile logoFile) {
        this.logoFile = logoFile;
        try {
            if (logoFile == null || logoFile.isEmpty()) return;
            String newPath = UUID.randomUUID().toString() + ".png";
            try {
                BufferedImage bufferedImage = ImageIO.read(logoFile.getInputStream());
                //ImageProcessor ip = new ImageProcessor();
                //bufferedImage = ip.resizeImage(bufferedImage);
                ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();
                ImageWriteParam param = writer.getDefaultWriteParam();
                //param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
                //param.setCompressionQuality(1.0F); // Highest quality
                File outputFile = new File(SiteApplication.DIR_AVATARS + newPath);
                writer.setOutput(new FileImageOutputStream(outputFile));
                writer.write(bufferedImage);
                setLogo(newPath);
            } catch (IOException ex) {
                log.error("Error saving logo", ex);
            }
        } catch (NullPointerException | IllegalArgumentException ex2) {
            log.error("Error saving logo", ex2);
        }
    }

    /**
     * Цвет
     * @return
     */
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     */
    public Boolean getSimpleIdenty() {
        return simpleIdenty;
    }

    public void setSimpleIdenty(Boolean simpleIdenty) {
        this.simpleIdenty = simpleIdenty;
    }

    /**
     *
     * @return
     */
    public Boolean getPeriodic() {
        return periodic;
    }

    public void setPeriodic(Boolean periodic) {
        this.periodic = periodic;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Company.class.getSimpleName() + "[", "]")
                .add("id=" + getId())
                .add("name='" + name + "'")
                .add("color='" + color + "'")
                .toString();
    }

    public MapType getMap() {
        return map;
    }

    public void setMap(MapType map) {
        this.map = map;
    }


    /**
     * Подпись с разрешениями
     * @return
     */
    public Signature getSign() {
        try {
            return Signature.readSignature(new ByteArrayInputStream(sign));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setSign(Signature sign) {
        try {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            sign.writeSignature(byteStream);
            this.sign = byteStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}