package com.landau.core.filter.specs;

import com.landau.core.filter.filters.FilterValuesFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class FilterValuesSpec {
    public static Specification<FilterValuesFilter> find(FilterValuesFilter filterValuesFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (filterValuesFilter.getId() != null) {
                predicates.add(cb.equal(root.get("id"), filterValuesFilter.getId()));
            }

            if (filterValuesFilter.getName() != null) {
                predicates.add(cb.equal(root.get("name"), filterValuesFilter.getName()));
            }

            if (filterValuesFilter.getFilter() != null) {
                predicates.add(cb.equal(root.get("parent"), filterValuesFilter.getFilter()));
            }

            if (filterValuesFilter.getParent() != null) {
                predicates.add(cb.equal(root.get("parent"), filterValuesFilter.getParent()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
