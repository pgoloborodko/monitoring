package com.landau.core.filter.specs;

import com.landau.core.filter.filters.FiltersFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class FiltersSpec {
    public static Specification<FiltersFilter> find(FiltersFilter filtersFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (filtersFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), filtersFilter.getCompany()));
            }

            if (filtersFilter.getId() != null) {
                predicates.add(cb.equal(root.get("id"), filtersFilter.getId()));
            }

            if (filtersFilter.getName() != null) {
                predicates.add(cb.equal(root.get("name"), filtersFilter.getName()));
            }

            if (filtersFilter.getParent() != null) {
                predicates.add(cb.equal(root.get("parent"), filtersFilter.getParent()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
