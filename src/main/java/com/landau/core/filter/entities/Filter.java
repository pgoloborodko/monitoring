package com.landau.core.filter.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.filter.repos.FilterRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Фильтр
 */
@Getter
@Setter
@Entity
@Table
@Repo(FilterRepo.class)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@ApiModel(description = "Фильтр")
public class Filter extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Название фильтра")
    private String name;

    @ManyToOne
    @ApiModelProperty(value = "Родительский фильтр")
    private Filter parent;

    @OneToMany(mappedBy = "filter")
    @ApiModelProperty(value = "Список значений фильтра фильтра")
    private List<FilterValue> values;
}
