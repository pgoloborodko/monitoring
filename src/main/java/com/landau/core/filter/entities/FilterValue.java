package com.landau.core.filter.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.filter.repos.FilterValueRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Значения фильтра
 */
@Getter
@Setter
@Entity
@Table
@Repo(FilterValueRepo.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@ApiModel(description = "Значения фильтр")
public class FilterValue extends AbstractId {
    @Column
    @ApiModelProperty(value = "Название значения фильтра")
    private String name;

    @ManyToOne
    @ApiModelProperty(value = "Фильтр")
    private Filter filter;

    @ManyToOne
    @ApiModelProperty(value = "Родительское значения фильтра")
    private FilterValue parent;
}
