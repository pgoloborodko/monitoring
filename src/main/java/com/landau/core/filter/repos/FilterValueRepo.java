package com.landau.core.filter.repos;

import com.landau.core.filter.entities.FilterValue;
import com.landau.core.filter.filters.FilterValuesFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FilterValueRepo extends JpaRepository<FilterValue, Long>, JpaSpecificationExecutor<FilterValuesFilter> {
}
