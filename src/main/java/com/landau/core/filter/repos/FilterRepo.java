package com.landau.core.filter.repos;

import com.landau.core.filter.entities.Filter;
import com.landau.core.filter.filters.FiltersFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FilterRepo extends JpaRepository<Filter, Long>, JpaSpecificationExecutor<FiltersFilter> {
}
