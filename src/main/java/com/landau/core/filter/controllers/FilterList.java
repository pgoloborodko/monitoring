package com.landau.core.filter.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.filter.entities.Filter;
import com.landau.core.filter.filters.FiltersFilter;
import com.landau.core.filter.repos.FilterRepo;
import com.landau.core.filter.specs.FiltersSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Список фильтров", description = "Контроллер для получения списка фильтров")
@RestController("FilterIndex")
@RequestMapping("/filter")
public class FilterList extends Base {
    @Autowired
    private FilterRepo filterRepo;

    @ApiOperation(value = "Получение списка фильтров", response = Filter.class)
    @GetMapping
    public Page<FiltersFilter> get(FiltersFilter filtersFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            filtersFilter.setCompany(getUser().getCompany());
        }

        return filterRepo.findAll(FiltersSpec.find(filtersFilter), pageable);
    }

}
