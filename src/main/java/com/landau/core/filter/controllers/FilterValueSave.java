package com.landau.core.filter.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.filter.entities.FilterValue;
import com.landau.core.filter.repos.FilterValueRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@Api(tags = "Сохранение значений фильтра", description = "Контроллер может получать данные значений фильтра, а также создавать новые")
@RestController(value = "FilterValueSave")
@RequestMapping("/filter/values/save")
public class FilterValueSave extends Base {
    @Autowired
    private FilterValueRepo filterValueRepo;

    @ApiOperation(value = "Получение данных значений фильтра", response = FilterValue.class)
    @Authority(FilterValue.class)
    @GetMapping
    public FilterValue get(FilterValue filterValue) {
        return filterValue;
    }

    @ApiOperation(value = "Сохранение значений фильтра", response = FilterValue.class)
    @Authority(FilterValue.class)
    @PostMapping
    public @Valid FilterValue post(FilterValue filterValue, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        return filterValueRepo.save(filterValue);
    }
}
