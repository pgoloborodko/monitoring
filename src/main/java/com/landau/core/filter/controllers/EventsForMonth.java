package com.landau.core.filter.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.events.services.EventService;
import com.landau.core.users.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/filter/month")
public class EventsForMonth extends Base {
    @Autowired
    EventService eventService;

    @ModelAttribute
    private List<User> getUsersById(@RequestParam(value = "user", required = false) List<Long> ids) {
        List<User> users = new ArrayList<>();

        if(ids != null) {
            for(Long id: ids) {
                User user = new User();
                user.setId(id);
                users.add(user);
            }
        }

        return users;
    }


/*    @ApiOperation(value = "Получение списка событий за месяц, с возможностью фильтрации по нескольким событиям для нескольких пользователей ")
    @Authority(value = List.class)
    public List<EventsPerMonthDTO> get(List<User> users,
            @RequestParam(value = "event", required = false) List<String> events,
            @RequestParam(value = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return eventService.getListPerMonth(users, events, date);
    }*/
}
