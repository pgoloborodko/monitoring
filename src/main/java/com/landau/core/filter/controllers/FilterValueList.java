package com.landau.core.filter.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.filter.entities.FilterValue;
import com.landau.core.filter.filters.FilterValuesFilter;
import com.landau.core.filter.repos.FilterValueRepo;
import com.landau.core.filter.specs.FilterValuesSpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Список значений фильтра", description = "Контроллер для получения списка значений фильтров")
@RestController("FilterValueIndex")
@RequestMapping("/filter/values")
public class FilterValueList extends Base {
    @Autowired
    FilterValueRepo filterValueRepo;

    @ApiOperation(value = "Получение списка значений фильтра", response = FilterValue.class)
    @GetMapping
    public Page<FilterValuesFilter> get(FilterValuesFilter filterValuesFilter, Pageable pageable) {
        return filterValueRepo.findAll(FilterValuesSpec.find(filterValuesFilter), pageable);
    }
}
