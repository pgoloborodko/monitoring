package com.landau.core.filter.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.events.entities.EventDeviceStream;
import com.landau.core.events.entities.EventTerminalSync;
import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.events.filters.EventFilter;
import com.landau.core.events.repos.EventDeviceStreamRepo;
import com.landau.core.events.repos.UserDevicesSyncRepo;
import com.landau.core.events.specs.EventDeviceStreamSpec;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.filters.MediaFileFilter;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.media.specs.MediaFileSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Фильтр пользователей")
@RequestMapping("/filter/day")
public class UsersForDay extends Base {
    @Autowired
    private UserDevicesSyncRepo syncRepo;

    @Autowired
    private EntityManager em;

    @Autowired
    private EventDeviceStreamRepo streamRepo;

    @Autowired
    private MediaFileRepo fileRepo;

    private enum Type {
        STREAM,
        ARCHIVE,
        MULTI
    }

    @Getter
    @Setter
    private static class Record {
        private Type type;
        private User user;

        Record(Type type, User user) {
            this.type = type;
            this.user = user;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof Record) {
                Record record = (Record) obj;

                return (record.getUser() != null && record.getType() == this.getType() && record.getUser().equals(this.getUser()));
            }

            return false;
        }

        @Override
        public int hashCode() {
            return user.hashCode();
        }
    }

    private List<EventTerminalSync.Type> eventTypes = new ArrayList<EventTerminalSync.Type>() {{
        add(EventTerminalSync.Type.EXTRACTED);
        add(EventTerminalSync.Type.INSERTED);
    }};

    @GetMapping
    public List<Record> get(@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("date") Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Calendar calendarEnd = (Calendar) calendar.clone();
        calendarEnd.setTime(date);
        calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
        calendarEnd.set(Calendar.MINUTE, 59);
        calendarEnd.set(Calendar.SECOND, 59);

        // find streams
        List<UserDevicesSync> streams;

        if(getUser().getRole() != User.Role.ADMIN) {
            streams = syncRepo.findAllByInsertedIsNullAndUser_Company(getUser().getCompany());
        }
        else {
            streams = syncRepo.findAllByInsertedIsNullAndUserNotNull();
        }

        List<User> streamUsers = streams.stream().map(UserDevicesSync::getUser).collect(Collectors.toList());


        // find archive streams (events) and mediafiles exists
        EventFilter filter = new EventFilter();
        filter.setDay(date);

        if(getUser().getRole() != User.Role.ADMIN) {
            filter.setCompany(getUser().getCompany());
        }

        MediaFileFilter mediaFileFilter = new MediaFileFilter();
        mediaFileFilter.setDate(date);

        if(getUser().getRole() != User.Role.ADMIN) {
            mediaFileFilter.setCompany(getUser().getCompany());
        }
        //filter1.setGroupByUser(true);
        List<EventDeviceStream> events = streamRepo.findAll(EventDeviceStreamSpec.find(filter));
        List<MediaFile> files = fileRepo.findAll(MediaFileSpec.find(mediaFileFilter));

        List<Record> records = new ArrayList<>();

        //
        for(User user : streamUsers) {
            Record record = new Record(Type.STREAM, user);

            if(!records.contains(record)) {
                records.add(record);
            }
        }

        //
        for(EventDeviceStream event : events) {
            if(event.getUser() != null) {
                Record record = new Record(Type.ARCHIVE, event.getUser());

                if(!records.contains(record)) {
                    records.add(record);
                }
            }
        }

        //
        for(MediaFile mediaFile : files) {
            if(mediaFile.getUser() != null) {
                Record record = new Record(Type.ARCHIVE, mediaFile.getUser());

                if (!records.contains(record)) {
                    records.add(record);
                }
            }
        }

        return records;
    }
}
