package com.landau.core.filter.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.filter.entities.Filter;
import com.landau.core.filter.repos.FilterRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Api(tags = "Сохранение фильтра", description = "Контроллер может получать данные фильтра, а также создавать новые")
@RestController(value = "FilterSave")
@RequestMapping("/filter/save")
public class FilterSave extends Base {
    @Autowired
    FilterRepo filterRepo;

    @ApiOperation(value = "Получение данных фильтра", response = Filter.class)
    @Authority(Filter.class)
    @GetMapping
    public Filter get(Filter filter) {
        return filter;
    }

    @ApiOperation(value = "Сохранение фильтра", response = Filter.class)
    @Authority(Filter.class)
    @PostMapping
    public @Valid Filter post(@Valid Filter filter, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        if(getUser().getRole() != User.Role.ADMIN) {
            filter.setCompany(getUser().getCompany());
        }

        return filterRepo.save(filter);
    }
}
