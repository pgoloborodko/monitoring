package com.landau.core.license.services;

import com.landau.core.license.dto.LicenseDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LicenseService {
    @Value("${license.smart}")
    Integer smart;

    @Value("${license.smartWatch}")
    Integer smartWatch;

    @Value("${license.chiefSmart}")
    Integer chiefSmart;

    @Value("${license.terminalLocalArchive}")
    Integer terminalLocalArchive;

    @Value("${license.terminalDevices}")
    Integer terminalDevices;

    @Value("${license.terminalIntegrationAgent}")
    Integer terminalIntegrationAgent;

    @Value("${license.monitoring}")
    Integer monitoring;

    @Value("${license.investigationsAndArchive}")
    Integer investigationsAndArchive;

    @Value("${license.core}")
    Integer core;

    @Value("${license.transcoder}")
    Integer transcoder;

    public List<LicenseDTO> prepareInfo() {
        List<LicenseDTO> infoDTOS = new ArrayList<>();

        infoDTOS.add(new LicenseDTO("Мобильный агент для смарт-регистраторов", smart, "SLDMA10v1"));
        infoDTOS.add(new LicenseDTO("Мобильный агент для смарт-часов", smartWatch, "SLDMA20v1"));
        infoDTOS.add(new LicenseDTO("Мобильный руководитель для планшетов", chiefSmart, "SLDMA30v1"));

        infoDTOS.add(new LicenseDTO("Терминал. Локальный архив", terminalLocalArchive, "SLDTA10v1"));
        infoDTOS.add(new LicenseDTO("Терминал. Управление мобильными устройствами", terminalDevices,  "SLDTA20v1"));
        infoDTOS.add(new LicenseDTO("Терминал. Интеграционный агент для терминалов", terminalIntegrationAgent, "SLDTA30v1"));

        infoDTOS.add(new LicenseDTO("Облачные решения. Оперативный мониторинг", monitoring, "SLDCA10v1"));
        infoDTOS.add(new LicenseDTO("Облачные решения. Расследования и архив", investigationsAndArchive, "SLDСA30v1"));

        infoDTOS.add(new LicenseDTO("Ядро. Платформа предоставления и обеспечения сервисов", core, "SLDCR10v1"));
        infoDTOS.add(new LicenseDTO("Транскодер", transcoder, "SLDCR30v1"));

        return infoDTOS;
    }
}
