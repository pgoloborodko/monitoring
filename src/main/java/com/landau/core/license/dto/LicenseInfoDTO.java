package com.landau.core.license.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DTO-контейнер данных для передачи списка лицензий")
public class LicenseInfoDTO {
    @ApiModelProperty(value = "Мобильный агент для смартрегистратов.")
    Integer smart;

    @ApiModelProperty(value = "Мобильный агент для смартчасов.")
    Integer smartWatch;

    @ApiModelProperty(value = "Мобильный руководитель для планшетов.")
    Integer chiefSmart;

    @ApiModelProperty(value = "Терминал. Локальный архив.")
    Integer terminalLocalArchive;

    @ApiModelProperty(value = "Терминал. Управление мобильными устройствами.")
    Integer terminalDevices;

    @ApiModelProperty(value = "Терминал. Интеграционный агент.")
    Integer terminalIntegrationAgent;

    @ApiModelProperty(value = "Облачные решения. Оперативный мониторинг.")
    Integer monitoring;

    @ApiModelProperty(value = "Облачные решения. Расследования и архив.")
    Integer investigationsAndArchive;

    @ApiModelProperty(value = "Платформа предоставления и обеспечения сервисов (Ядро).")
    Integer core;

    @ApiModelProperty(value = "Транскодер (перекодировщик потоков).")
    Integer transcoder;
}
