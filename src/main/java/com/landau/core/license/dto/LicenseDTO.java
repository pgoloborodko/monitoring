package com.landau.core.license.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DTO-контейнер данных для передачи списка лицензий")
public class LicenseDTO {
    @ApiModelProperty(value = "Наименование лицензии")
    private String name;
    @ApiModelProperty(value = "Количество лицензий")
    private Integer count;
    @ApiModelProperty(value = "Состояние лицензии")
    private Boolean state;
    @ApiModelProperty(value = "Артикул лицензии")
    private String article;

    public LicenseDTO(String name, Integer count, String article) {
        this.name = name;
        this.count = count;
        this.state = count > 0;
        this.article = article;
    }
}
