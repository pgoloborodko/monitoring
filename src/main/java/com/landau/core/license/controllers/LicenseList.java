package com.landau.core.license.controllers;

import com.landau.core.license.dto.LicenseDTO;
import com.landau.core.license.services.LicenseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("LicenseList")
@RequestMapping("/license")
@Api(tags = "Список лицензий",
        description = "Контроллер обеспечивает выдачу списка лицензий")
public class LicenseList {
    @Autowired
    private LicenseService licenseService;

    @GetMapping("/list")
    @ApiOperation(value = "Получение списка лицензий")
    public List<LicenseDTO> getInfo() {
        return licenseService.prepareInfo();
    }
}
