package com.landau.core.apps.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.services.ControllerService;
import com.landau.core.devices.entities.Device;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.ServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Установка приложения
 */
@Data
@Api(tags = "Установка приложения", description = "Контроллер обеспечивает установку приложения на устройства")
@RestController
@RequestMapping("/apps/installer")
public class AppInstaller {
    private DeviceAppMessages deviceAppMessages;
    private ControllerService controllerService;

    @Autowired
    public AppInstaller(DeviceAppMessages deviceAppMessages, ControllerService controllerService) {
        this.deviceAppMessages = deviceAppMessages;
        this.controllerService = controllerService;
    }

    /**
     * Добавление нового приложения
     * @param device сущность приложения
     * @param deviceApk сущность приложения
     * @return boolean
     */
    @ApiOperation(value = "Просмотр APK приложения", responseContainer = "List", response = AppDevice.class)
    @Authority(value = Device.class)
    @Authority(value = AppDevice.class)
    @GetMapping
    public boolean get(Device device, AppDevice deviceApk, ServletRequest servletRequest) {

        return true;
        //return deviceApkLogRepo.findAllByDeviceApkOrderByIdAsc(servletRequest.getAttribute(deviceApkDTO.getClass()));
    }

    /**
     * Запрос на установку переданного приложения на переданное устройство
     * @param devices список устройств для установки
     * @param app приложение для инсталляции
     * @exception ResponseStatusException INTERNAL_SERVER_ERROR - ошибка на стороне сервера, ошибка отправки сообщения
     * @exception ResponseStatusException BAD_REQUEST - ошибка на стороне клиента, неправильный запрос,
     * возможно, отсутствуют необходимые для работы параметры
     */
    @Authority(value = App.class, required = true)
    @Authority(value = Device.class, required = true)
    @ApiOperation(value = "Запрос на установку приложения на устройство", response = AppDeviceCommand.class, responseContainer = "List")
    @GetMapping("/install")
    public List<AppDeviceCommand> requestInstallAPK(App app, List<Device> devices) {
        List<AppDeviceCommand> deviceApkCommandList = new ArrayList<>();

        for (Device device: devices){
            device = controllerService.verifyDevice(device);
            app = controllerService.verifyApp(app);

            AppDeviceCommand deviceAppCommand = controllerService.createDeviceAPKCommand(device, app, AppDeviceCommand.Type.INSTALL);

            if (app != null && device.getOnline()){
                try {
                    deviceAppMessages.installApk(deviceAppCommand);
                    deviceApkCommandList.add(deviceAppCommand);
                    deviceAppCommand.setComplete(true);
                    controllerService.getAppDeviceCommandRepo().save(deviceAppCommand);
                } catch (RepublisherException e) {
                    controllerService.getAppDeviceCommandRepo().save(deviceAppCommand);
                    e.printStackTrace();
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
                }
            }
            else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
            }
        }

        return deviceApkCommandList;
    }

    /**
     * Запрос на удаление переданного приложения с переданного устройства
     * @param devices список устройств для удаления
     * @param app приложение для удаления
     * @exception ResponseStatusException INTERNAL_SERVER_ERROR - ошибка на стороне сервера, ошибка отправки сообщения
     * @exception ResponseStatusException BAD_REQUEST - ошибка на стороне клиента, неправильный запрос,
     * возможно, отсутствуют необходимые для работы параметры
     */
    @Authority(value = App.class, required = true)
    @Authority(value = Device.class, required = true)
    @ApiOperation(value = "Запрос на удаление приложения с устройство", response = AppDeviceCommand.class)
    @GetMapping("/uninstall")
    public List<AppDeviceCommand> requestUninstallAPK(App app, List<Device> devices) {
        List<AppDeviceCommand> deviceApkCommandList = new ArrayList<>();

        for (Device device: devices){
            device = controllerService.verifyDevice(device);
            app = controllerService.verifyApp(app);

            AppDeviceCommand deviceAppCommand = controllerService.createDeviceAPKCommand(device, app, AppDeviceCommand.Type.UNINSTALL);

            if (app != null && device.getOnline()){
                try {
                    deviceAppMessages.uninstallApk(deviceAppCommand);
                    deviceApkCommandList.add(deviceAppCommand);
                    deviceAppCommand.setComplete(true);
                    controllerService.getAppDeviceCommandRepo().save(deviceAppCommand);
                } catch (RepublisherException e) {
                    controllerService.getAppDeviceCommandRepo().save(deviceAppCommand);
                    e.printStackTrace();
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
                }
            }
            else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
            }
        }

        return deviceApkCommandList;
    }

}
