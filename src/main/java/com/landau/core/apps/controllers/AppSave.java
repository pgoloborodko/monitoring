package com.landau.core.apps.controllers;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Data;
import net.dongliu.apk.parser.ApkFile;
import net.dongliu.apk.parser.bean.ApkMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

/**
 * Установка приложения
 */
@Data
@Api(tags = "Сохранение приложения", description = "Контроллер обеспечивает сохранение приложения")
@RestController("devicesApkSave")
@RequestMapping("/apps/save")
public class AppSave extends Base {
    private AppDeviceRepo appDeviceRepo;
    private AppRepo appRepo;

    @Autowired
    public AppSave(AppDeviceRepo appDeviceRepo, AppRepo appRepo) {
        this.appDeviceRepo = appDeviceRepo;
        this.appRepo = appRepo;
    }

    /**
     * Просмотр приложения
     * @param id идентификатор сущности приложения
     * @return AppDevice
     */
    @ApiOperation(value = "Просмотр приложения в системе", response = AppDevice.class)
    @Authority(value = AppDevice.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Доступ запрещен")
    })
    @GetMapping
    public AppDevice get(@RequestParam Long id) {
        AppDevice appDevice = appDeviceRepo.findById(id).orElse(null);
        if (appDevice != null){
            return appDevice;
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Установка APK на сутройство
     * @param file файл приложения
     * @return App
     */
    @ApiOperation(value = "Сохранение приложения в системе", response = App.class)
    @PostMapping
    public App save(MultipartFile file) {
        App appToSave = createAppFromFile(file);
        Optional<App> optionalApp = appRepo.findByName(appToSave.getName());

        optionalApp.ifPresent(app -> appToSave.setId(app.getId()));

        return appRepo.save(appToSave);
    }

    private App createAppFromFile(@RequestParam MultipartFile multipartFile){
        App app = null;

        if (multipartFile != null && !multipartFile.isEmpty()){
            app = new App();
            app.setAppFile(multipartFile);

            try (ApkFile apkFile = new ApkFile(new File(app.getApkPath()))){
                ApkMeta apkMeta = apkFile.getApkMeta();
                app.setAppPackage(apkMeta.getPackageName());
                app.setCompany(getUser().getCompany());
                app.setName(apkMeta.getName());
                app.setVersion(apkMeta.getVersionName());
                app.setCreated(new Date());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return app;
    }
}
