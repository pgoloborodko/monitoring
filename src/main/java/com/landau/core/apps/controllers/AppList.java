package com.landau.core.apps.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.messages.DeviceAppMessages;
import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.common.ApiPageable;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.devices.entities.Device;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Получение списка приложений
 */
@Slf4j
@Data
@Api(tags = "Список приложений", description = "Контроллер обеспечивает получение списка приложений")
@RestController
@RequestMapping("/apps")
public class AppList {
    private AppDeviceRepo appDeviceRepo;
    private DeviceAppMessages deviceAppMessages;
    private AppRepo appRepo;

    @Autowired
    public AppList(AppDeviceRepo appDeviceRepo, DeviceAppMessages deviceAppMessages, AppRepo appRepo) {
        this.appDeviceRepo = appDeviceRepo;
        this.deviceAppMessages = deviceAppMessages;
        this.appRepo = appRepo;
    }

    /**
     * Получение списка приложений
     * @param pageable настройки пагинации
     * @return
     */
    @ApiPageable
    @ApiOperation(value = "Получение списка приложений", response = App.class)
    @GetMapping
    public Page<App> get(Pageable pageable) {
        return appRepo.findAll(pageable);
    }

    /**
     * Получение списка приложений для устройства
     * @param device идентификатор устройства
     * @param pageable настройки пагинации
     * @return
     */
    @ApiPageable
    @ApiOperation(value = "Получение списка приложений для устройства", response = AppDevice.class)
    @GetMapping("/device")
    @Authority(value = Device.class, required = true)
    public Page<AppDevice> get(Device device, Pageable pageable) {
        return appDeviceRepo.findAllByDevice(device, pageable);
    }


    /**
     * Запрос списка приложений устройства
     * @param device объект устройства
     * @return
     */
    @Authority(value = Device.class, required = true)
    @ApiOperation(value = "Запрос на устройство для получения списка приложений")
    @GetMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    @Authority(value = Device.class, required = true)
    public void requestInstalledAPKList(Device device) {
        if (device.getOnline()){
            try {
                deviceAppMessages.getApkList(device);
            }
            catch (RepublisherException e) {
                log.error("error get apk list", e);
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            log.error("device offline");
            throw new ResponseStatusException(HttpStatus.GONE, "device offline");
        }
    }
}