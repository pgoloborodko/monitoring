package com.landau.core.apps.entities;

import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Сущность приложения APK
 */
@Data
@Entity
@Table
@Repo(AppDeviceRepo.class)
@ApiModel(description = "Сущность приложения для устройства")
public class AppDevice extends AbstractId {

    @ManyToOne
    @ApiModelProperty(value = "Приложение")
    private App app;

    @ManyToOne
    @ApiModelProperty(value = "Устройство")
    private Device device;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "Дата добавления APK")
    private Date created;
}
