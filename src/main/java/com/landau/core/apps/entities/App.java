package com.landau.core.apps.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.landau.core.SiteApplication;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.companies.entities.Company;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * Приложение в системе
 */
@Data
@Entity
@ApiModel(description = "Сущность приложения")
public class App extends AbstractId {
    @Transient
    @JsonIgnore
    @ApiModelProperty(value = "Файл App")
    private MultipartFile appFile;

    @Column
    @ApiModelProperty(value = "Локальный путь до файла")
    private String apkPath;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(value = "Дата добавления App")
    private Date created;

    @Column
    @ApiModelProperty(value = "url App")
    private String url;

    @Column
    @ApiModelProperty(value = "имя App")
    private String name;

    @Column
    @ApiModelProperty(value = "название пакета APP")
    private String appPackage;

    @Column
    @ApiModelProperty(value = "версия пакета APP")
    private String version;

    @ManyToOne
    private Company company;

    public void setAppFile(MultipartFile appFile) {
        this.appFile = appFile;

        try {
            if (appFile == null || appFile.isEmpty()) return;

            String newPath = UUID.randomUUID().toString() + ".apk";

            try {
                File outputFile = new File(SiteApplication.DIR_APKS + newPath);
                appFile.transferTo(outputFile);

                setUrl(newPath);
                setApkPath(outputFile.getPath());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        catch (NullPointerException | IllegalArgumentException ex2) {
            ex2.printStackTrace();
        }
    }
}
