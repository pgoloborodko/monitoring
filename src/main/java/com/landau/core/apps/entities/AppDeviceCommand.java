package com.landau.core.apps.entities;

import com.landau.core.apps.repos.AppDeviceCommandRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.devices.entities.Device;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Команда установки
 */
@Data
@Table
@Entity
@Repo(AppDeviceCommandRepo.class)
@ApiModel(description = "Команда на установку/удаление приложения")
public class AppDeviceCommand extends AbstractBusiness {
    /**
     * Тип действия с APK
     */
    public enum Type {
        /**
         * Установка приложения
         */
        INSTALL,
        /**
         * Удаление приложения
         */
        UNINSTALL
    }

    @ApiModelProperty(value = "Приложение")
    @ManyToOne
    private App app;

    @ApiModelProperty(value = "Устройство")
    @ManyToOne
    private Device device;

    @ApiModelProperty(value = "Дата добавления")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column
    private Date added;

    @Column
    @ApiModelProperty(value = "Тип команды")
    private Type type;

    @Column
    @ApiModelProperty(value = "Статус команды")
    private Boolean complete;
}