package com.landau.core.apps.specs;


import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.filters.AppDeviceFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class AppDeviceSpec {
    public static Specification<AppDevice> find(AppDeviceFilter appDeviceFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            // TODO доделать
            if (appDeviceFilter.getDevice() != null) {

                predicates.add(cb.equal(root.get("device"), appDeviceFilter.getDevice()));

                if (appDeviceFilter.getDevice().getCompany() != null){
                    predicates.add(cb.equal(root.get("device").get("company"), appDeviceFilter.getDevice().getCompany()));
                }
            }

            if (appDeviceFilter.getApp() != null){
                predicates.add(cb.equal(root.get("app"), appDeviceFilter.getApp()));
            }

            if (appDeviceFilter.getCreated() != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(appDeviceFilter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(appDeviceFilter.getCreated());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
