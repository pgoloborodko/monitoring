package com.landau.core.apps.specs;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.filters.AppFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class AppSpec {
    public static Specification<App> find(AppFilter appFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (appFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), appFilter.getCompany()));
            }


            if (appFilter.getApkPath() != null){
                predicates.add(cb.equal(root.get("apkPath"), appFilter.getApkPath()));
            }

            if (appFilter.getUrl() != null){
                predicates.add(cb.equal(root.get("url"), appFilter.getUrl()));
            }

            if (appFilter.getCreated() != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(appFilter.getCreated());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                Calendar calendarEnd = (Calendar) calendar.clone();
                calendarEnd.setTime(appFilter.getCreated());
                calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
                calendarEnd.set(Calendar.MINUTE, 59);
                calendarEnd.set(Calendar.SECOND, 59);

                predicates.add(cb.greaterThanOrEqualTo(root.get("created"), calendar.getTime()));
                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendarEnd.getTime()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
