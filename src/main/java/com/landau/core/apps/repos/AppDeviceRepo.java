package com.landau.core.apps.repos;

import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.devices.entities.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AppDeviceRepo extends JpaRepository<AppDevice, Long>, JpaSpecificationExecutor<AppDevice> {
    Optional<AppDevice> findByApp(App app);
    void deleteAllByDevice(Device device);
    Page<AppDevice> findAllByDevice(Device device, Pageable pageable);
    Optional<AppDevice> findByDevice(Device device);
}
