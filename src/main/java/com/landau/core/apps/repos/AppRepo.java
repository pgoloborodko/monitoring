package com.landau.core.apps.repos;

import com.landau.core.apps.entities.App;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AppRepo extends JpaRepository<App, Long>, JpaSpecificationExecutor<App> {
    Optional<App> findByName(String name);
}
