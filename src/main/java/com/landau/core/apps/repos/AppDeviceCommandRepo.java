package com.landau.core.apps.repos;

import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.devices.entities.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AppDeviceCommandRepo extends JpaRepository<AppDeviceCommand, Long> {
    List<AppDeviceCommand> findAllByDeviceAndComplete(Device device, boolean b);

    /**
     * Метод для тестов
     * @param serial
     * @return
     */
    Optional<AppDeviceCommand> findByDevice_Serial(String serial);
}
