package com.landau.core.apps.messages;


import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.apps.entities.App;
import com.landau.core.apps.entities.AppDevice;
import com.landau.core.apps.entities.AppDeviceCommand;
import com.landau.core.apps.repos.AppDeviceCommandRepo;
import com.landau.core.apps.repos.AppDeviceRepo;
import com.landau.core.apps.repos.AppRepo;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.action.GetAppList;
import com.landau.objects.action.InstallApp;
import com.landau.objects.action.UninstallApp;
import com.landau.objects.data.AppList;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Сервис предназначен для работы с App устройства
 */
@Data
@Slf4j
@Service
public class DeviceAppMessages implements IService {
    @Value("${server.address}")
    private String siteUrl;

    @Autowired
    private CoreDeviceMessages messages;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private AppDeviceRepo appDeviceRepo;

    @Autowired
    private AppDeviceCommandRepo appDeviceCommandRepo;

    @Autowired
    private CastService castService;

    @Autowired
    private AppRepo appRepo;


    /**
     * wait apk list from device
     * @param appList
     */
    @ReConsumer(listen = AppList.class)
    protected void consumeApkList(AppList appList) {
        log.info("consume message: {}", AppList.class);
        Optional<Device> optionalDevice = deviceRepo.findBySerial(appList.getSerial());

        if(optionalDevice.isPresent()) {
            appDeviceRepo.deleteAllByDevice(optionalDevice.get());

            if(appList.getApps() != null && appList.getApps().size() > 0) {

                List<AppDevice> deviceApps = new ArrayList<>();

                for (AppList.App appFromMessage: appList.getApps()){
                    App app = castService.castToEntity(appFromMessage);
                    if (app != null){
                        AppDevice deviceApp = new AppDevice();
                        deviceApp.setApp(app);
                        deviceApp.setDevice(optionalDevice.get());
                        deviceApp.setCreated(new Date());
                        deviceApps.add(deviceApp);
                    }
                    else {
                        log.warn("null value: {}", AppList.App.class);
                    }

                }

                appDeviceRepo.saveAll(deviceApps);
                log.info("update deviceApps");
            }
        }
    }

    /**
     * request for install apk
     * @param deviceAppCommand
     * @throws RepublisherException
     */
    public void installApk(AppDeviceCommand deviceAppCommand) throws RepublisherException {
        InstallApp installApp = new InstallApp();
        installApp.setUrl(deviceAppCommand.getApp().getUrl());
        //TODO разобраться с login-password для InstallApp
//        installApp.setLogin("login");
//        installApp.setPassword("password");

        messages.send(appDeviceCommandRepo.save(deviceAppCommand).getDevice().getSerial(), installApp);
        log.info("send to device: {}", InstallApp.class);
    }

    /**
     * request for uninstall apk
     * @param deviceApkCommand
     * @throws RepublisherException
     */
    public void uninstallApk(AppDeviceCommand deviceApkCommand) throws RepublisherException {
        UninstallApp uninstallApp = new UninstallApp();
        uninstallApp.setAppPackage(deviceApkCommand.getApp().getAppPackage());

        messages.send(appDeviceCommandRepo.save(deviceApkCommand).getDevice().getSerial(), uninstallApp);
        log.info("send to device: {}", UninstallApp.class);
    }

    /**
     * request for get apk list
     * @param device
     * @throws RepublisherException
     */
    public void getApkList(Device device) throws RepublisherException {
        messages.send(device.getSerial(), new GetAppList());
        log.info("send message: {}", GetAppList.class);
    }
}
