package com.landau.core.streams.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.streams.entities.StreamServer;
import com.landau.core.streams.repos.StreamServerRepo;
import com.landau.core.streams.services.StreamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

/**
 * Сохранение стрим-сервера
 */
@Api(tags = "Изменение/сохранение стрим.сервера", description = "Контроллер может получать данные стрим.сервера, а также изменять имеющиеся данные стрим. сервера, создавать новые")
@RestController(value = "StreamServersSave")
@RequestMapping("/streams/servers/save")
public class StreamServerSave extends Base {
    private StreamServerRepo streamServerRepo;
    private StreamService streamService;

    @Autowired
    public StreamServerSave(StreamServerRepo streamServerRepo, StreamService streamService) {
        this.streamServerRepo = streamServerRepo;
        this.streamService = streamService;
    }

    @ApiOperation(value = "Получение данных стрим. серверов", response = StreamServer.class)
    @Authority(StreamServer.class)
    @GetMapping
    public StreamServer get(StreamServer streamServer) {
        return streamServer;
    }

    /**
     * Сохранение стрим-сервера
     * @param streamServer сущность стрим-сервера
     * @param result
     * @return
     */
    @ApiOperation(value = "Изменение/сохранение стрим. сервера", response = StreamServer.class)
    @Authority(StreamServer.class)
    @PostMapping
    public StreamServer post(StreamServer streamServer, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        StreamServer streamServerInfo = null;

        try {
            streamServerInfo = streamService.getServerInfo(streamServer.getHttpUrl(), streamServer.getToken());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        if(streamServerInfo.getStatus()) {
            streamServer.setStatus(true);
            streamServer.setAvailable(true);
            streamServer.setAllSpace(streamServerInfo.getAllSpace());
            streamServer.setAllTraffic(streamServerInfo.getAllTraffic());
            streamServer.setHoldSpace(streamServerInfo.getHoldSpace());
            streamServer.setConnections(streamServerInfo.getConnections());
            streamServer.setCpuLoad(streamServerInfo.getCpuLoad());
            streamServer.setIncomingStream(streamServerInfo.getIncomingStream());
            streamServer.setOutgoingStream(streamServerInfo.getOutgoingStream());
        }
        else {
            streamServer.setStatus(false);

            streamServer.setAllSpace(0L);
            streamServer.setAllTraffic(0L);
            streamServer.setHoldSpace(0L);
            streamServer.setConnections(0L);
            streamServer.setCpuLoad(0D);
            streamServer.setIncomingStream(0L);
            streamServer.setOutgoingStream(0L);
        }

        streamServer.setUpdated(new Date());

        return streamServerRepo.save(streamServer);
    }

}
