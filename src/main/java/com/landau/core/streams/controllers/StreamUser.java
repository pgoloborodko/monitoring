package com.landau.core.streams.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.streams.messages.DeviceStreamMessages;
import com.landau.core.streams.services.StreamService;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * Запуск и поддержание стриминга на регистраторе
 */
@Api(tags = "Запуск и поддержание стриминга пользователя", description = "Контроллер отвечает за запуск и поддержание стриминга пользователя")
@CrossOrigin
@Data
@RestController
@RequestMapping("/streams/user")
public class StreamUser {
    private DeviceStreamMessages deviceStreamMessages;
    private DeviceRepo deviceRepo;
    private StreamService streamService;

    @Autowired
    public StreamUser(DeviceStreamMessages deviceStreamMessages, DeviceRepo deviceRepo, StreamService streamService) {
        this.deviceStreamMessages = deviceStreamMessages;
        this.deviceRepo = deviceRepo;
        this.streamService = streamService;
    }

    /**
     * Запуск и поддержание стримминга на регистраторе
     * @param user
     * @exception ResponseStatusException INTERNAL_SERVER_ERROR - ошибка на стороне сервера, ошибка отправки сообщения
     * @exception ResponseStatusException BAD_REQUEST - ошибка на стороне клиента, неправильный запрос,
     * возможно, отсутствуют необходимые для работы параметры
     * @return
     */
    @ApiOperation(value = "Запуск и поддержание стримминга на регистраторе по акку юзера", response = String.class)
    @Authority(value = User.class, required = true)
    @GetMapping("keepalive")
    public String keepAlive(@RequestParam("user") User user) {
        if (user.getDevice() != null) {
            try {
                deviceStreamMessages.startStream(user.getDevice());
                return streamService.getSldpStreamUrl(user.getDevice());
            } catch (RepublisherException e) {
                e.printStackTrace();
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error send stream request");
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "device for user not found");
        }
    }
}
