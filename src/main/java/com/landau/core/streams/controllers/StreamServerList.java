package com.landau.core.streams.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.streams.entities.StreamServer;
import com.landau.core.streams.filters.StreamServerFilter;
import com.landau.core.streams.repos.StreamServerRepo;
import com.landau.core.streams.specs.StreamServerSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список стрим серверов
 */
@Api(tags = "Список стрим-серверов", description = "Контроллер для получения списка стрим. серверов")
@RestController("StreamServersIndex")
@RequestMapping("/streams/servers")
public class StreamServerList extends Base {
    private StreamServerRepo streamServerRepo;

    @Autowired
    public StreamServerList(StreamServerRepo streamServerRepo) {
        this.streamServerRepo = streamServerRepo;
    }

    /**
     * Получение списка стрим серверов
     * @return список стрим серверов
     */
    @ApiOperation(value = "Получение списка стрим. серверов по фильтру", response = StreamServer.class)
    @GetMapping
    public Page<StreamServer> get(StreamServerFilter filter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            filter.setCompany(getUser().getCompany());
        }

        return streamServerRepo.findAll(StreamServerSpec.find(filter), pageable);
    }
}