package com.landau.core.streams.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.interfaces.IService;
import com.landau.core.devices.entities.Device;
import com.landau.core.streams.services.StreamService;
import com.landau.core.tasks.entities.TaskUser;
import com.landau.core.tasks.repos.TaskUserRepo;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.action.GetStream;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Data
@Service
@Slf4j
public class DeviceStreamMessages implements IService {
    @Value("${server.address}")
    private String siteUrl;

    @Autowired
    private CoreDeviceMessages messages;

    @Autowired
    private TaskUserRepo taskUserRepo;

    @Autowired
    private StreamService streamService;


/*    @ObjectListener(object = TaskObject.class)
    @ObjectPublisher(listener = BrowserMessages.class, send = Event.class)
    private void onTaskObject(Device device, TaskObject taskObject, Event event) {
        if(taskObject.getType() == TaskObject.Type.START_STREAM) {
            event.setType(Event.Type.START_STREAM);
            event.setCreated(new Date());
            event.setDevice(device);
            event.setUser(device.getUser());

            eventRepo.save(event);

            // find task
            Task task = taskRepo.findById(taskObject.getId()).orElse(null);

            if(task != null) {
                TaskDevice taskDevice = taskDeviceRepo.findByTaskAndDevice(task, device);
                taskDevice.setStatus(TaskDevice.Status.COMPLETED);
                taskDeviceRepo.save(taskDevice);
            }
        }
    }


    @ObjectListener(object = Task.class)
    private void onTask(Task task) {
        if(task.getType() == Task.Type.START_STREAM) {
            TaskObject taskObject = new TaskObject();
            taskObject.setId(task.getId());

            taskObject.setType(TaskObject.Type.START_STREAM);
            taskObject.setText(task.getComment());

            for(TaskDevice taskDevice : task.getPerformers()) {
                messageService.send(taskDevice.getDevice(), taskObject);
            }
        }
    }


    @ObjectListener(object = Event.class)
    private void onEvent(Event event) {
        if(event.getType() == Event.Type.CALL_STREAM) {
            if(messageService.findByEntity(event.getDevice())) {
                startStream(event.getDevice());
            }
        }
    }*/

    /**
     * send stream keep alive
     * @param device
     */
    public void startStream(Device device) throws RepublisherException {
        startStream(device, 30);
    }

    public void startStream(Device device, Integer timeout) throws RepublisherException {
        TaskUser taskDevice = new TaskUser();
        taskDevice.setStatus(TaskUser.Status.OPEN);
        taskDevice.setDevice(device);

        taskUserRepo.save(taskDevice);
        log.info("save: {}", TaskUser.class);


        String streamServerUrl = streamService.getRtmpStreamUrl(device);

        if(streamServerUrl != null) {
            GetStream getStream = new GetStream();
            getStream.setUrl(streamServerUrl);
            getStream.setTimeout(timeout);

            messages.send(device.getSerial(), getStream);
            log.info("send message: {}", GetStream.class);
        }
        else {
            log.warn("stream server not found: {}", GetStream.class);
        }
    }
}
