package com.landau.core.streams.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.streams.services.StreamService;
import com.landau.core.users.entities.Token;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.TokenRepo;
import com.landau.core.users.repos.UserRepo;
import com.landau.messages.core.CoreStreamerMessages;
import com.landau.objects.action.CheckAuthToken;
import com.landau.objects.action.GetPlaylist;
import com.landau.objects.action.GetStreamUrl;
import com.landau.objects.data.AuthToken;
import com.landau.objects.data.Playlist;
import com.landau.objects.data.StreamUrl;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
@Service
@Slf4j
public class StreamerMessages implements IService {
    @Autowired
    private CoreStreamerMessages messages;

    @Autowired
    private TokenRepo tokenRepo;

    @Autowired
    private MediaFileRepo mediaFileRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private StreamService streamService;

    private DeviceStreamMessages deviceStreamMessages;

    @ReConsumer(listen = CheckAuthToken.class)
    public void consumeCheckAuthToken(CheckAuthToken checkAuthToken) throws RepublisherException {
        log.info("consume message: {}", CheckAuthToken.class);

        Optional<Token> optionalToken = tokenRepo.findByValue(checkAuthToken.getToken());
        AuthToken authToken = new AuthToken();
        authToken.setExternal(checkAuthToken.getExternal());
        authToken.setToken(checkAuthToken.getToken());

        if (optionalToken.isPresent()) {
            authToken.setAccess(AuthToken.Access.ALLOW);
        }
        else {
            authToken.setAccess(AuthToken.Access.DENY);
        }

        messages.send(checkAuthToken.getSerial(), authToken);
    }

    @ReConsumer(listen = GetPlaylist.class)
    public void consumeGetPlaylist(GetPlaylist getPlaylist) throws RepublisherException {
        log.info("consume message: {}", GetPlaylist.class);

        Playlist playlist = new Playlist();
        List<Playlist.PlaylistFileUrl> playlistFileUrls = new ArrayList<>();

        Optional<Token> optionalToken = tokenRepo.findByValue(getPlaylist.getToken());
        Optional<User> optionalUser = userRepo.findById(getPlaylist.getUser());

        if(optionalToken.isPresent() && optionalUser.isPresent()) {
            if(optionalToken.get().getUser().getRole() == User.Role.ADMIN || optionalToken.get().getUser().getCompany().equals(optionalUser.get().getCompany())) {
                List<MediaFile> mediaFiles = mediaFileRepo.findAllByUserAndCreatedBetween(optionalUser.get(), getPlaylist.getFrom(), getPlaylist.getTo());

                for(MediaFile mediaFile : mediaFiles) {
                    if(mediaFile.getDuration() != null) {
                        Playlist.PlaylistFileUrl playlistFileUrl = new Playlist.PlaylistFileUrl();
                        playlistFileUrl.setUrl(mediaFile.getUrl());
                        playlistFileUrl.setFrom(mediaFile.getCreated());
                        playlistFileUrl.setTo(new Date((long) (mediaFile.getCreated().getTime() + mediaFile.getDuration() * 1000)));

                        playlistFileUrls.add(playlistFileUrl);
                    }
                }
            }
        }

        playlist.setExternal(getPlaylist.getExternal());
        playlist.setFiles(playlistFileUrls);

        messages.send(getPlaylist.getSerial(), playlist);
    }

    @ReConsumer(listen = GetStreamUrl.class)
    public void consumeGetStreamUrl(GetStreamUrl getStreamUrl) throws RepublisherException {
        Optional<Token> optionalToken = tokenRepo.findByValue(getStreamUrl.getToken());
        Optional<User> optionalUser = userRepo.findById(getStreamUrl.getUser());

        StreamUrl streamUrl = new StreamUrl();
        streamUrl.setExternal(getStreamUrl.getExternal());

        if(optionalToken.isPresent() && optionalUser.isPresent()) {
            if(optionalToken.get().getUser().getRole() == User.Role.ADMIN || optionalToken.get().getUser().getCompany().equals(optionalUser.get().getCompany())) {
                User user = optionalUser.get();

                if (user.getDevice() != null) {
                    try {
                        deviceStreamMessages.startStream(user.getDevice());
                        streamUrl.setUrl(streamService.getSldpStreamUrl(user.getDevice()));
                    } catch (RepublisherException e) {
                        log.error("error send stream to device", e);
                    }
                }
                else {
                    log.error("user not found {}", user.getId());
                }
            }
        }
        HttpClientErrorException gg
                ;

        messages.send(getStreamUrl.getSerial(), streamUrl);
    }
}
