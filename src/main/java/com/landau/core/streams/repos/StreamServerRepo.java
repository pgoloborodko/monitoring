package com.landau.core.streams.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.streams.entities.StreamServer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;
import java.util.Optional;


public interface StreamServerRepo extends JpaRepository<StreamServer, Long>, JpaSpecificationExecutor<StreamServer> {
/*
    @Query("select a from StreamServer where a.unavailableSince >= :unavailableSince and a.company = : company")
    List<StreamServer> findAllByCompanyAndUnavailableSinceAfter(@Param("company")Company company, @Param("unavailableSince") Date date);

 */
/*

    List<StreamServer> findAllByCompanyAndAvailableAAndUnavailableSinceAfter(Company company, Boolean available, Date date);

 */
    List<StreamServer> findAllByCompanyAndAvailable(Company company, Boolean available);
    List<StreamServer> findAllByCompanyAndUnavailableSinceAfter(Company company, Date date);

    List<StreamServer> findAllByAvailable( Boolean available);

    List<StreamServer> findAllByUnavailableSinceAfter(Date date);

    List<StreamServer> findByStatusAndStateAndCompanyOrderByAllTrafficAsc(boolean b, boolean b1, Company company);

    List<StreamServer> findAllByCompany(Company company);

    //List<StreamServer> findByStatusAndStateAndCompanyOrderByAllTrafficAscConnectionsAsc(Boolean status, Boolean state, Company company);

    /**
     * Метод для тестов
     * @param name
     * @return
     */
    Optional<StreamServer> findByName(String name);
}
