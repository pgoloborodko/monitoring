package com.landau.core.streams.specs;

import com.landau.core.streams.entities.StreamServer;
import com.landau.core.streams.filters.StreamServerFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class StreamServerSpec {
    public static Specification<StreamServer> find(StreamServerFilter streamServerFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (streamServerFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), streamServerFilter.getCompany()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
