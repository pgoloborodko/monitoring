package com.landau.core.streams.services;

import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.stats.entity.StatsStreamServer;
import com.landau.core.stats.repos.StatsStreamServerRepo;
import com.landau.core.streams.entities.StreamServer;
import com.landau.core.streams.repos.StreamServerRepo;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class StreamService {
    private StreamServerRepo streamServerRepo;
    private StatsStreamServerRepo statsStreamServerRepo;

    @Autowired
    public StreamService(StreamServerRepo streamServerRepo, StatsStreamServerRepo statsStreamServerRepo) {
        this.streamServerRepo = streamServerRepo;
        this.statsStreamServerRepo = statsStreamServerRepo;
    }

    /**
     * update stream servers
     * @return most perspective stream server
     */
    public StreamServer getServerForStreaming(Company company) {
//        for (StreamServer streamServer : streamServerRepo.findAllByCompany(company)){
//            streamServer = getServerInfo(streamServer);
//        }
//        List<StreamServer> streamServerList = streamServerRepo.findByStatusAndStateAndCompanyOrderByAllTrafficAsc(true, true, company);

        List<StreamServer> unavailableStreamServersAfter10Min = null;
        List<StreamServer> availableStreamServers;

        if(company == null) {
            availableStreamServers= streamServerRepo.findAllByAvailable(true);
        }
        else {
            availableStreamServers= streamServerRepo.findAllByCompanyAndAvailable(company, true);
        }

        try {
            if(company == null) {
                unavailableStreamServersAfter10Min = streamServerRepo.findAllByUnavailableSinceAfter(new SimpleDateFormat("mm").parse("10"));
            }
            else {
                unavailableStreamServersAfter10Min = streamServerRepo.findAllByCompanyAndUnavailableSinceAfter(company, new SimpleDateFormat("mm").parse("10"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (unavailableStreamServersAfter10Min != null){
            availableStreamServers.addAll(unavailableStreamServersAfter10Min);
        }
        StreamServer streamServer = null;


        for (StreamServer x: availableStreamServers){
            try {
                if (x.getHttpUrl() != null){
                    streamServer = getServerInfo(x);

                    if ((x.getMaxDevices() == 0 || x.getConnections() < streamServer.getMaxDevices())
                            && (x.getMaxCPU() == 0.0 || x.getCpuLoad() < streamServer.getMaxCPU())
                            && (x.getMaxOutgoingStream() == 0 || x.getOutgoingStream() < streamServer.getMaxOutgoingStream())){

                        x.setAvailable(true);
                        x.setUnavailableSince(null);
                        streamServer = x;
                    }
                    else {
                        x.setAvailable(false);
                        x.setUnavailableSince(new SimpleDateFormat("mm").parse("00"));
                    }
                    streamServerRepo.save(x);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /*
        if(streamServerList.size() == 0) {
            return null;
        }

        return streamServerList.get(0);

         */
        return streamServer;
    }

    /**
     *
     * @param device
     * @return
     * @throws JSONException
     */
    public JSONArray getStreamTimeLine(Device device, Date date) throws JSONException {
        StreamServer streamServer = getServerForStreaming(device.getCompany());

        if(streamServer == null) {
            return null;
        }

        Double salt = Math.random();
        String str2hash = salt + "/" + streamServer.getToken();
        byte[] md5raw = DigestUtils.md5(str2hash);
        byte[] bytesEncoded = Base64.encodeBase64(md5raw);
        String base64hash = new String(bytesEncoded);

        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(streamServer.getHttpUrl() + "/manage/dvr_status/stream/" + device.getSerial() + "?salt=" + salt + "&hash=" + base64hash + "&server=true&timeline=true").build(false);
        URI uri = uriComponents.toUri();
        RestTemplate restTemplate = new RestTemplate();
        String objectString = restTemplate.getForObject(uri, String.class);

        JSONArray array = new JSONArray(objectString);

        if(array.length() > 0) {
            JSONObject dvrResult = array.getJSONObject(0);
            return dvrResult.getJSONArray("timeline");
        }
        else {
            return null;
        }
    }

    /**
     * get information from stream server
     * @param url server url
     * @param token server token
     * @return StreamServer object if success or null if error
     */
    public StreamServer getServerInfo(String url, String token) throws Exception {
        StreamServer streamServer = new StreamServer();

        Double salt = Math.random();
        String str2hash = salt + "/" + token;
        byte[] md5raw = DigestUtils.md5(str2hash);
        byte[] bytesEncoded = Base64.encodeBase64(md5raw);
        String base64hash = new String(bytesEncoded);

        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url + "/manage/server_status?salt=" + salt + "&hash=" + base64hash + "&server=true").build(false);
        URI uri = uriComponents.toUri();
        RestTemplate restTemplate = new RestTemplate();
        String objectString = restTemplate.getForObject(uri, String.class);

        UriComponents uriDvrComponents = UriComponentsBuilder.fromHttpUrl(url + "/manage/dvr_status?salt=" + salt + "&hash=" + base64hash + "&server=true").build(false);
        URI dvrUri = uriDvrComponents.toUri();
        String arrayDvrResult = restTemplate.getForObject(dvrUri, String.class);

        JSONObject result = new JSONObject(objectString);
        JSONArray dvrResult = new JSONArray(arrayDvrResult);

        streamServer.setConnections(result.getLong("Connections"));
        streamServer.setOutgoingStream(result.getLong("OutRate"));
        streamServer.setCpuLoad(result.getJSONObject("SysInfo").getDouble("scl"));
        streamServer.setHoldSpace(result.getJSONObject("SysInfo").getLong("tpms") - result.getJSONObject("SysInfo").getLong("fpms"));
        streamServer.setAllSpace(result.getJSONObject("SysInfo").getLong("tpms"));


/*
        Long holdSpace = 0L;

        for (int i = 0; i < dvrResult.length(); i++){
            holdSpace += dvrResult.getJSONObject(i).getLong("size");
        }

        if(dvrResult.length() > 0) {
            Long allSpace = holdSpace + dvrResult.getJSONObject(0).getLong("space_available");
            streamServer.setAllSpace(allSpace);
            streamServer.setHoldSpace(holdSpace);
        }

 */

        streamServer.setStatus(true);
        return streamServer;
    }

    /**
     * get and update streamServer
     * @param streamServer is com.landau.core.entity from beans.StreamServer
     * @return updated streamServer
     */
    public StreamServer getServerInfo(StreamServer streamServer) {
        StreamServer streamServer1 = null;

        try {
            streamServer1 = getServerInfo(streamServer.getHttpUrl(), streamServer.getToken());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        if (streamServer1 != null){

            streamServer.setHoldSpace(streamServer1.getHoldSpace());
            streamServer.setAllSpace(streamServer1.getAllSpace());
            streamServer.setAllTraffic(streamServer1.getAllTraffic());
            streamServer.setConnections(streamServer1.getConnections());
            streamServer.setCpuLoad(streamServer1.getCpuLoad());
            streamServer.setStatus(true);
        }
        else {
            streamServer.setStatus(false);
        }
        streamServer.setUpdated(new Date());
        return streamServerRepo.save(streamServer);
    }

    public void startStatsGathering(StreamServer streamServer){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Optional<StreamServer> optional = streamServerRepo.findById(streamServer.getId());

                if (optional.isPresent()){
                    if (optional.get().getStatus()){

                        StatsStreamServer statsStreamServer = new StatsStreamServer();
                        statsStreamServer.setStartStatistic(new Date());
                        statsStreamServer.setName(streamServer.getName());
                        statsStreamServer.setCpuLoad(streamServer.getCpuLoad().intValue());
                        statsStreamServer.setAllMemory(streamServer.getAllSpace());
                        statsStreamServer.setHoldMemory(streamServer.getHoldSpace());
                        statsStreamServer.setStreamCount(streamServer.getConnections());
                        statsStreamServer.setCompany(streamServer.getCompany());

                        statsStreamServerRepo.save(statsStreamServer);
                    }
                }
            }
        }, 600000);
    }

    public String getRtmpStreamUrl(Device device) {
        StreamServer streamServer = getServerForStreaming(device.getCompany());

        if(streamServer != null) {
            return streamServer.getRtmpUrl() + device.getSerial();
        }

        return null;
    }

    public String getSldpStreamUrl(Device device) {
        StreamServer streamServer = getServerForStreaming(device.getCompany());

        if(streamServer != null) {
            return streamServer.getSldpUrl() + device.getSerial();
        }

        return null;
    }
}
