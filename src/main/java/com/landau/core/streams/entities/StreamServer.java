package com.landau.core.streams.entities;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.streams.repos.StreamServerRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Стрим сервера
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(StreamServerRepo.class)
@ApiModel(description = "Стрим-сервер")
public class StreamServer extends AbstractBusiness {
    public enum Type {
        ORIGIN, // only for streaming
        EDGE // only for archive
    }

    @Column
    @ApiModelProperty(value = "название сервера")
    private String name;// название сервера

    @Column
    @ApiModelProperty(value = "url http сервера")
    private String httpUrl;// url http сервера

    @Column
    @ApiModelProperty(value = "url sldp сервера")
    private String sldpUrl;// url sldp сервера

    @Column
    @ApiModelProperty(value = "url rtmp сервера")
    private String rtmpUrl;//url rtmp сервера

    @Column
    @ApiModelProperty(value = "url hls стрим архива")
    private String hlsStreamUrl;//url hls сервера

    @Column
    @ApiModelProperty(value = "url hls raw сервера")
    private String hlsArchiveUrl;//url hls сервера

    @Column
    @ApiModelProperty(value = "токен сервера")
    private String token;//пароль

    @Column
    @ApiModelProperty(value = "количество подключенных устройств")
    private Long connections;//количество подключенных устройств

    @Column
    @ApiModelProperty(value = "общее дисковое пространство")
    private Long allSpace;//общее дисковое место

    @Column
    @ApiModelProperty(value = "занятое дисковое пространство")
    private Long holdSpace;//занятое дисковое место

    @Column
    @ApiModelProperty(value = "общий объем передаваемых данных")
    private Long allTraffic;//общий объем передаваемых данных

    @Column
    @ApiModelProperty(value = "статус вкл/выкл")
    private Boolean state;//статус(вкл/выкл)

    @Column
    @ApiModelProperty(value = "дата обновления сервера")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")                                                                    //указатель для springfox
    private Date updated;

    @Column
    @ApiModelProperty(value = "статус сервера онлайн/офлайн")
    private Boolean status = false;

    @Column
    @ApiModelProperty(value = "Величина входящего потока в Мбит")
    private Long incomingStream; //Величина входящего потока в Мбит

    @Column
    @ApiModelProperty(value = "Величина исходящего потока в Мбит")
    private Long outgoingStream; //Величина исходящего потока в Мбит

    @Column
    @ApiModelProperty(value = "максимальная нагрузка на сервер")
    private Double maxCPU; //максимальная нагрузка на сервер

    @Column
    @ApiModelProperty(value = "максимальный входящий поток в Мбит")
    private Long maxIncomingStream; //максимальный входящий поток в Мбит

    @Column
    @ApiModelProperty(value = "максимальный исходящий поток в Мбит")
    private Long maxOutgoingStream; //максимальный исходящий поток в Мбит

    @Column
    @ApiModelProperty(value = "максимальное количество устройств")
    private Long maxDevices; //максимальное количество устройств

    @Column
    @ApiModelProperty(value = "нагрузка на оперативную память стрим-сервера")
    private Double cpuLoad; //нагрузка на CPU

    @Column
    @ApiModelProperty(value = "доступен/недоступен")
    private Boolean available; //доступен/недоступен по загрузке

    @ApiModelProperty(value = "недоступен с...")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")                                                                    //указатель для springfox
    private Date unavailableSince; // недоступен с...
}
