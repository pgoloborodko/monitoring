package com.landau.core.users.filters;

import com.landau.core.locations.entities.Location;
import com.landau.core.users.entities.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class UsersFilter extends User {
    private String fio;
    private Boolean online = false;
    private Integer page = 1;
    private List<Location> locations = new ArrayList<>();

    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
    private Date startTime = new Date(System.currentTimeMillis() - 3600 * 1000);
    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
    private Date endTime = new Date(System.currentTimeMillis() + 3600 * 1000);
}
