package com.landau.core.users.specs;

import com.landau.core.users.entities.SessionLog;
import com.landau.core.users.filters.SessionLogFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class SessionLogSpec {
    public static Specification<SessionLog> find(SessionLogFilter sessionLogFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (sessionLogFilter.getSession() != null) {
                predicates.add(criteriaBuilder.equal(root.get("session"), sessionLogFilter.getSession()));
            }
            if (sessionLogFilter.getType() != null) {
                predicates.add(criteriaBuilder.equal(root.get("type"), sessionLogFilter.getType()));
            }
            if (sessionLogFilter.getDate() != null) {
                Date date = sessionLogFilter.getDate();
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);

                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("date"), date));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("date"), new Date(date.getTime() + 24 * 60 * 60 * 1000 - 1000)));
            } else {
                Date date = new Date();
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);

                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("date"), date));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("date"), new Date(date.getTime() + 24 * 60 * 60 * 1000 - 1000)));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
