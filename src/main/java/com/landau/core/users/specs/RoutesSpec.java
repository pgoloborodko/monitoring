package com.landau.core.users.specs;

import com.landau.core.users.entities.Route;
import com.landau.core.users.filters.RoutesFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class RoutesSpec {
    public static Specification<Route> find(RoutesFilter routesFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (routesFilter.getName() != null && !routesFilter.getName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("name")), routesFilter.getName().toUpperCase() +"%"));
            }

            if (routesFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), routesFilter.getCompany()));
            }

            if (routesFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("city"), routesFilter.getCity()));
            }

            if (routesFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("district"), routesFilter.getDistrict()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
