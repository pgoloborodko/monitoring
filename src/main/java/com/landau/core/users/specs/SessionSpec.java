package com.landau.core.users.specs;

import com.landau.core.users.entities.Session;
import com.landau.core.users.filters.SessionFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class SessionSpec {
    public static Specification<Session> find(SessionFilter sessionFilter){
        return (root, query, criteriaBuilder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (sessionFilter.getCompany() != null) {
                predicates.add(criteriaBuilder.equal(root.get("company"), sessionFilter.getCompany()));
            }

            if (sessionFilter.getUser() != null){
                predicates.add(criteriaBuilder.equal(root.get("user"), sessionFilter.getUser()));
            }

            if (sessionFilter.getOnline() != null){
                predicates.add(criteriaBuilder.equal(root.get("isOnline"), sessionFilter.getOnline()));
            }

            Date dateFrom = sessionFilter.getStartSession();
            dateFrom.setHours(0);
            dateFrom.setMinutes(0);
            dateFrom.setSeconds(0);

            Date dateTo = new Date(dateFrom.getTime() + 24 * 60 * 60 * 1000 - 1000);

            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("startSession"), dateFrom));
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("startSession"), dateTo));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
