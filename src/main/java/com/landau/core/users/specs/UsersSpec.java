package com.landau.core.users.specs;

import com.landau.core.devices.entities.Device;
import com.landau.core.users.entities.User;
import com.landau.core.users.filters.UsersFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class UsersSpec {
    public static Specification<User> find(UsersFilter usersFilter) {
        return find(usersFilter, false);
    }

    public static Specification<User> find(UsersFilter usersFilter, boolean isMap) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (usersFilter.getFirstName() != null && !usersFilter.getFirstName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("firstName")), usersFilter.getFirstName().toUpperCase() +"%"));
            }

            if (usersFilter.getSecondName() != null && !usersFilter.getSecondName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("secondName")), usersFilter.getSecondName().toUpperCase() +"%"));
            }

            if (usersFilter.getLastName() != null && !usersFilter.getLastName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("lastName")), usersFilter.getLastName().toUpperCase() +"%"));
            }

            if (usersFilter.getPhone() != null && !usersFilter.getPhone().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("phone")), "%" + usersFilter.getPhone().toUpperCase() +"%"));
            }

            if(usersFilter.getOnline()) {
                Join<Device, User> userDevice = root.join("device");
                predicates.add(cb.equal(userDevice.get("state"), 1));
            }

            if(usersFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), usersFilter.getCompany()));
            }

            if (usersFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("city"), usersFilter.getCity()));
            }

            if (usersFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("district"), usersFilter.getDistrict()));
            }

            if(usersFilter.getRole() != null) {
                predicates.add(cb.equal(root.get("role"), usersFilter.getRole()));
            }

            query.groupBy(root.get("id"));

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}

