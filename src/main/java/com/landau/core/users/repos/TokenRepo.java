package com.landau.core.users.repos;

import com.landau.core.users.entities.Token;
import com.landau.core.users.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TokenRepo extends JpaRepository<Token, Long> {
    Token findFirstByValue(String value);
    Page<Token> findByUser(User user, Pageable pageable);

    Optional<Token> findByValue(String token);
}
