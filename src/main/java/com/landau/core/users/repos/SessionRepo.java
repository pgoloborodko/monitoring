package com.landau.core.users.repos;

import com.landau.core.users.entities.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface SessionRepo extends JpaRepository<Session, Long>, JpaSpecificationExecutor<Session> {
    Optional<Session> findBySpringSessionIdAndOnlineOrderByIdDesc(String springSessionId, Boolean isOnline);
    Optional<Session> findBySpringSessionId(String springSessionId);
    void deleteAllBySpringSessionId(String springSessionId);
}
