package com.landau.core.users.repos;

import com.landau.core.users.entities.SessionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SessionLogRepo extends JpaRepository<SessionLog, Long>, JpaSpecificationExecutor<SessionLog> {
}
