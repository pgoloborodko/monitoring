package com.landau.core.users.repos;

import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    List<User> findAllByOrderByIdDesc();

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM #{#entityName} u WHERE u.phone = ?1")//entityName? не существует, можно добавить com.landau.core.entity User
    boolean existsPhone(String phone);

    Optional<User> findByEmail(String email);
    User findByPhone(String phone);
    Optional<User> findByDevice(Device device);

    User findByDeviceAndCompany(Device device, Company company);

    List<User> findAllByCompany(Company company);

    User findByActivationCode(String code);
}
