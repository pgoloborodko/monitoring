package com.landau.core.users.repos;

import com.landau.core.users.entities.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RouteRepo extends JpaRepository<Route, Long>, JpaSpecificationExecutor<Route> {
}
