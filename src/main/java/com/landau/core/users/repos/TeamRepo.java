package com.landau.core.users.repos;

import com.landau.core.users.entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TeamRepo extends JpaRepository<Team, Long>, JpaSpecificationExecutor<Team> {
}
