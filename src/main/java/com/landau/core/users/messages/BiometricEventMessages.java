package com.landau.core.users.messages;

import com.lampa.republish.interfaces.IService;
import com.landau.core.biometric.repos.BiometricRepo;
import com.landau.core.common.messages.CastService;
import com.landau.messages.core.CoreBrowserMessages;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Data
@Service
public class BiometricEventMessages implements IService {
    @Autowired
    private CoreBrowserMessages messages;

    @Autowired
    private BiometricRepo biometricRepo;


    @Autowired
    private CastService castService;

//    @ReConsumer(listen = BiometricEvent.class)
//    private void consumeBiometricEvent(BiometricEvent biometricEventMessage){
//        log.info("consume message: " + biometricEventMessage.getClass().getSimpleName());
//        Event event = castService.castToEntity(biometricEventMessage);
//        eventRepo.save(event);
//        com.landau.core.coreBrowserMessages.sendBiometricEvent(biometricEventMessage);
//        log.info("send to browser" + biometricEventMessage.getClass().getSimpleName());
//    }
}
