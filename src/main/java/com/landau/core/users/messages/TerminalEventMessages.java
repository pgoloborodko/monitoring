package com.landau.core.users.messages;

import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.messages.core.CoreBrowserMessages;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Data
@Service
public class TerminalEventMessages implements IService {
    @Autowired
    private CoreBrowserMessages messages;

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CastService castService;

//    @ReConsumer(listen = TerminalEvent.class)
//    private void consumeTerminalEvent(TerminalEvent terminalEventMessage){
//        log.info("consume message: " + terminalEventMessage.getClass().getSimpleName());
//        Event event = castService.castToEntity(terminalEventMessage);
//        eventRepo.save(event);
//        com.landau.core.coreBrowserMessages.sendTerminalEvent(terminalEventMessage);
//        log.info("send to browser: " + terminalEventMessage.getClass().getSimpleName());
//    }
}
