package com.landau.core.users.messages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@MessageListener(group = "browser")
@Order(1)
@Slf4j
public class BrowserMessages2 {
//    @Autowired
//    private MessageService messageService;

//    /**
//     * Сообщение об изменении устройства
//     * @param device
//     * @return
//     */
//    @ObjectListener(object = Device.class)
//    public void onDeviceChange(Device device) {
//        messageService.findByType(User.class)
//                .stream()
//                .filter(user -> user.getEntity().getCompany().equals(device.getCompany()))
//                .peek(user -> user.send(device));
//    }
//
//    /**
//     * сообщение о событии
//     * @param event
//     */
//    @ObjectListener(object = Event.class)
//    public void onEvent(Event event) {
//        messageService.findByType(User.class)
//                .stream()
//                .filter(user -> user.getEntity().getCompany().equals(event.getUser().getCompany()))
//                .peek(user -> user.send(event));
//    }
//
//    /**
//     * сообщение о событии
//     * @param chatMessage
//     */
//    @ObjectListener(object = ChatMessage.class)
//    public void onChatMessage(ChatMessage chatMessage) {
//        messageService.findByType(User.class)
//                .stream()
//                .filter(user -> user.getEntity().getCompany().equals(chatMessage.getUser().getCompany()))
//                .peek(user -> user.send(chatMessage));
//    }
//
//    /**
//     * сообщение о телеметрии
//     * @param deviceTelemetry
//     */
//    @ObjectListener(object = DeviceTelemetry.class)
//    public void onDeviceTelemetry(DeviceTelemetry deviceTelemetry) {
//        messageService.findByType(User.class)
//                .stream()
//                .filter(user -> user.getEntity().getCompany().equals(deviceTelemetry.getDevice().getCompany()))
//                .peek(user -> user.send(deviceTelemetry));
//    }
//
//    /**
//     * сообщение о событии
//     * @param wifiLog
//     */
//    @ObjectListener(object = WifiLog.class)
//    public void onWifiLog(WifiLog wifiLog) {
//        messageService.findByType(User.class)
//                .stream()
//                .filter(user -> user.getEntity().getCompany().equals(wifiLog.getDevice().getCompany()))
//                .peek(user -> user.send(wifiLog));
//    }
}
