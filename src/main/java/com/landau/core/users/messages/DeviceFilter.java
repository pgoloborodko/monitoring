package com.landau.core.users.messages;

import com.lampa.republish.interfaces.IParam;
import com.landau.core.devices.entities.Device;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.TreeMap;

@Data
@AllArgsConstructor
public class DeviceFilter implements IParam {
    private Device device;

    @Override
    public TreeMap<String, Object> getFilter() {
        TreeMap<String, Object> map = new TreeMap<>();
        map.put("device", device.getId());
        return map;
    }
}
