package com.landau.core.users.controllers.session;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.Session;
import com.landau.core.users.repos.SessionRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.Date;

/**
 * Закрытие сессии
 */
@Api(tags = "Закрытие сессии", description = "Контроллер может закрыть выбранную сессию")
@RestController
@RequestMapping("/sessions/close")
@Slf4j
public class CloseSession extends Base {
    private SessionRepo sessionRepo;
    private EntityManager em;

    @Autowired
    public CloseSession(SessionRepo sessionRepo, EntityManager em) {
        this.sessionRepo = sessionRepo;
        this.em = em;
    }

    /**
     * Закрытие сессии
     * @param session сущность сессии
     * @return
     */
    @ApiOperation(value = "Закрытие переданной сессии")
    @Authority(Session.class)
    @PostMapping
    public Session post(Session session){
        try {
            em.createNativeQuery("DELETE FROM SPRING_SESSION WHERE SESSION_ID = '" + session.getSpringSessionId() + "'").getResultList();
        } catch (Exception e){
            log.error("error delete session id", e);
        }

        session.setEndSession(new Date());
        session.setOnline(false);
        session = sessionRepo.save(session);

        return session;
    }
}
