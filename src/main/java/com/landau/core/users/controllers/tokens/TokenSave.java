package com.landau.core.users.controllers.tokens;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.companies.entities.Company;
import com.landau.core.users.entities.Token;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.TokenRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Сохранение пользователя
 */
@Api(tags = "Сохранение токена пользователя", description = "Контроллер обеспечивает сохранение токена для пользователя")
@RestController
@RequestMapping("/users/tokens/save")
public class TokenSave extends Base {
    private TokenRepo tokenRepo;

    @Autowired
    public TokenSave(TokenRepo tokenRepo) {
        this.tokenRepo = tokenRepo;
    }

    /**
     * Получение данных токена
     * @param token id токена
     * @return объект токена
     */
    @ApiOperation(value = "Получение данных токена")
    @Authority(Token.class)
    @GetMapping
    public Token get(Token token) {
        Company company = token.getUser().getCompany();

        if(company != null) {
            if(getUser().getRole() != User.Role.ADMIN && !company.equals(getUser().getCompany())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
        }

        return token;
    }

    /**
     * Сохранение данных токена
     * @param token объект токена
     * @param result BindingResult
     * @return false с ошибкой, либо true с объектом токена
     */
    @ApiOperation(value = "Сохранение данных токена")
    @Authority(Token.class)
    @PostMapping
    public Token post(Token token, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getAllErrors().toString());
        }

        if (token.getId() == null) token.generateValue();

        token.setUser(getUser());

        return tokenRepo.save(token);
    }
}
