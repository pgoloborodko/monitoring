package com.landau.core.users.controllers.tokens;

import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.Token;
import com.landau.core.users.repos.TokenRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список токенов пользователя
 */
@Api(tags = "Список токенов пользователя", description = "Контроллер показывает список токенов")
@RestController("Users")
@RequestMapping("/users/tokens")
public class TokenList extends Base {
    private TokenRepo tokenRepo;

    @Autowired
    public TokenList(TokenRepo tokenRepo) {
        this.tokenRepo = tokenRepo;
    }

    /**
     * Получение списка токенов пользователя по фильтру
     * @return список токенов
     */
    @ApiOperation(value = "Получение списка токенов пользователя по фильтру", response = Token.class)
    @GetMapping
    public Page<Token> get(Pageable pageable) {
        return tokenRepo.findByUser(getUser(), pageable);
    }
}
