package com.landau.core.users.controllers.session;

import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.SessionLog;
import com.landau.core.users.filters.SessionLogFilter;
import com.landau.core.users.repos.SessionLogRepo;
import com.landau.core.users.specs.SessionLogSpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Лог сессии
 */
@Api(tags = "Лог сессии", description = "Контроллер для получения списка логов")
@RestController
@RequestMapping("/sessions/log")
public class SessionLogController extends Base {
    private SessionLogRepo sessionLogRepo;

    @Autowired
    public SessionLogController(SessionLogRepo sessionLogRepo) {
        this.sessionLogRepo = sessionLogRepo;
    }

    /**
     * Получение лога сесии по фильтру
     * @param sessionLogFilter фильтрация списка
     * @param pageable настройки пагинации
     * @return
     */
    @ApiOperation(value = "Получение списка логов сесии по фильтру", response = SessionLogController.class)
    @GetMapping
    public Page<SessionLog> get(SessionLogFilter sessionLogFilter, Pageable pageable){
        return sessionLogRepo.findAll(SessionLogSpec.find(sessionLogFilter), pageable);
    }
}
