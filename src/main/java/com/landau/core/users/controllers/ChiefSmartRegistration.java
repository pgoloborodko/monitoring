package com.landau.core.users.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.dto.UserRequestDTO;
import com.landau.core.users.repos.UserRepo;
import com.landau.core.users.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Api(tags = "Регистрация пользователя из мобильного приложения", description = "Контроллер отвечает за реистрацию из мобильного приложения ChiefSmart")
@RestController("ChiefSmartRegistration")
@RequestMapping("/login")
public class ChiefSmartRegistration extends Base {
    @Autowired
    UserRepo userRepo;

    @Autowired
    UserService userService;

    @ApiOperation(value = "Регистрация пользователя по вложенным даным", response = UserRequestDTO.class, responseContainer = "List")
    @PostMapping("registration")
    public String getModel(UserRequestDTO userRequestDTO){
        String pin = generatePin();
        userRequestDTO.setPassword(pin);

        if (!userService.addUser(userRequestDTO)){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "com.landau.core.users already exists");
        }

        return String.format("Пользователь сохранен, но не активирован, пожалуйста, проверьте ваш почтовый ящик и пройдите активацию. " +
                "Ваш логин: %s " +
                "Ваш пароль: %s", userRequestDTO.getPhone(), pin);
    }

    @ApiOperation(value = "Активация польователя по ссылке")
    @GetMapping("activate/{code}")
    public String activate(@PathVariable String code){
        boolean isActivated = userService.activateUser(code);
        String message;
        if (isActivated){
            message = "User successfully activated";
        }
        else {
            message = "Activation code is not found";
        }

        return message;
    }


    private String generatePin(){
        List<Integer> listOfPin= randomIntsGenerator();
        String pin = "";
        for (Integer numFromPin: listOfPin){
            pin += numFromPin;
        }
        return pin;
    }

    private List<Integer> randomIntsGenerator() {
        return new Random()
                .ints(4, 1, 10)
                .boxed()
                .collect(Collectors.toList());
    }
}
