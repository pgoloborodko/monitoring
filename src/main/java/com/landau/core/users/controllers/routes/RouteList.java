package com.landau.core.users.controllers.routes;

import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.Route;
import com.landau.core.users.entities.User;
import com.landau.core.users.filters.RoutesFilter;
import com.landau.core.users.repos.RouteRepo;
import com.landau.core.users.specs.RoutesSpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список маршрутов
 */
@Api(tags = "Список маршрутов", description = "Контроллер обеспечивает получение списка маршрутов")
@RestController
@RequestMapping("/routes")
public class RouteList extends Base {
    private RouteRepo routeRepo;

    @Autowired
    public RouteList(RouteRepo routeRepo) {
        this.routeRepo = routeRepo;
    }

    /**
     * Получение списка маршрутов по фильтру
     * @param routesFilter поля фильтра
     * @return список маршрутов
     */
    @ApiOperation(value = "Получение списка маршрутов по фильтру", response = Route.class)
    @GetMapping
    public Page<Route> get(RoutesFilter routesFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            routesFilter.setCompany(getUser().getCompany());
        }


        return routeRepo.findAll(RoutesSpec.find(routesFilter), pageable);
    }
}