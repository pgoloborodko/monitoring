package com.landau.core.users.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import com.landau.core.users.filters.UsersFilter;
import com.landau.core.users.specs.UsersSpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список пользователей
 */
@Api(tags = "Список пользователей", description = "Контроллер обеспечивает получение списка пользователей")
@RestController
@RequestMapping("/users")
public class UserList extends Base {

    /**
     * Получение списка пользователей по фильтру
     * @param usersFilter поля фильтра
     * @return список пользователей
     */
    @ApiOperation(value = "Получение списка пользователей по фильтру", response = User.class)
    @GetMapping
    public Page<User> get(UsersFilter usersFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            usersFilter.setCompany(getUser().getCompany());
        }

        return userRepo.findAll(UsersSpec.find(usersFilter), pageable);
    }
}
