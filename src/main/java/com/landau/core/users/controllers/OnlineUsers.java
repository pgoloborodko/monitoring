package com.landau.core.users.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.events.entities.UserDevicesSync;
import com.landau.core.events.repos.UserDevicesSyncRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Онлайн пользователи")
@RequestMapping("/users/online")
public class OnlineUsers extends Base {
    @Autowired
    private UserDevicesSyncRepo syncRepo;

    @GetMapping
    public List<User> get() {
        // find streams
        List<UserDevicesSync> streams;

        if(getUser().getRole() != User.Role.ADMIN) {
            streams = syncRepo.findAllByInsertedIsNullAndUser_Company(getUser().getCompany());
        }
        else {
            streams = syncRepo.findAllByInsertedIsNullAndUserNotNull();
        }

        List<User> streamUsers = streams.stream().map(UserDevicesSync::getUser).collect(Collectors.toList());
        List<User> records = new ArrayList<>();

        //
        for(User user : streamUsers) {
            if(!records.contains(user)) {
                records.add(user);
            }
        }

        return records;
    }
}
