package com.landau.core.users.controllers.session;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.Session;
import com.landau.core.users.entities.User;
import com.landau.core.users.filters.SessionFilter;
import com.landau.core.users.filters.SessionLogFilter;
import com.landau.core.users.repos.SessionLogRepo;
import com.landau.core.users.repos.SessionRepo;
import com.landau.core.users.specs.SessionLogSpec;
import com.landau.core.users.specs.SessionSpec;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

/**
 * Список сессии
 */
@Api(tags = "Список сессий", description = "Контроллер для получения списка сессий")
@RestController("SessionIndex")
@RequestMapping("/users/sessions")
public class SessionList extends Base {
    private SessionRepo sessionRepo;
    private SessionLogRepo sessionLogRepo;

    @Autowired
    public SessionList(SessionRepo sessionRepo, SessionLogRepo sessionLogRepo) {
        this.sessionRepo = sessionRepo;
        this.sessionLogRepo = sessionLogRepo;
    }

    /**
     * Просмотр списка сессий
     * @param sessionFilter фильтр отображения спсика
     * @param pageable настройки пагинации
     * @return список сессий изи базы
     */
    @ApiOperation(value = "Просмотр списка сессий по фильтру", response = Session.class)
    @GetMapping
    public Page<Session> get(SessionFilter sessionFilter, Pageable pageable, final ServletRequest request){
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        Cookie cookie = WebUtils.getCookie(httpRequest, "SESSION");
        String value = new String(Base64.getDecoder().decode(cookie.getValue()));


        if(getUser().getRole() != User.Role.ADMIN) {
            sessionFilter.setCompany(getUser().getCompany());
        }

        if (sessionFilter.getUser() == null){
            sessionFilter.setUser(getUser());
        }

        if (sessionFilter.getCompany() == null){
            sessionFilter.setCompany(getUser().getCompany());
        }

        if (sessionFilter.getStartSession() == null){
            sessionFilter.setStartSession(new Date());
        }

        Page<Session> sessionFilters = sessionRepo.findAll(SessionSpec.find(sessionFilter), pageable);

        for(Session session : sessionFilters) {
            if (session.getSpringSessionId().equals(value)){
                session.setCurrent(true);
            }
        }

        return sessionFilters;
    }

    /**
     * Просмотр одной сессии
     * @param session идентификатор сессии для просмотра
     * @return запись из базы
     */
    @ApiOperation(value = "Просмотр переданной сессии", response = Session.class)
    @Authority(Session.class)
    @RequestMapping(value = "/getSession", method = RequestMethod.GET)
    public Session get(Session session, SessionLogFilter sessionLogFilter, final ServletRequest request){
        if(getUser().getRole() != User.Role.ADMIN) {
            session.setCompany(getUser().getCompany());
        }

        //
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        Cookie cookie = WebUtils.getCookie(httpRequest, "SESSION");
        String value = new String(Base64.getDecoder().decode(cookie.getValue()));

        Optional<Session> optional = sessionRepo.findById(session.getId());

        if (optional.isPresent()){
            session = optional.get();
            if (session.getSpringSessionId().equals(value)){
                session.setCurrent(true);
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "session not found");
        }

        session.setSessionLogs(sessionLogRepo.findAll(SessionLogSpec.find(sessionLogFilter)));

        return session;
    }
}
