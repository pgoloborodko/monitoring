package com.landau.core.users.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.common.exceptions.EyepanelValidationException;
import com.landau.core.dto.UserRequestDTO;
import com.landau.core.users.entities.Role;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

/**
 * Сохранение пользователя
 */
@Api(tags = "Сохранение пользователя", description = "Контроллер может получать данные пользователя, а также сохранять новых")
@RestController
@RequestMapping("/users/save")
public class UserSave extends Base {
    /**
     * Получение списка ролей
     * @return список ролей
     */
    @ModelAttribute("roles")
    public List<Role> getRoles() {
        return roleRepo.findAll();
    }

    /**
     * Получение пользователя
     * @param user id пользователя
     * @return объект пользователя
     */
    @ApiOperation(value = "Получение данных пользователя", response = User.class)
    @Authority(User.class)
    @GetMapping
    public User get(User user) {
        return user;
    }

    /**
     * Сохранение пользователя
     * @param userRequestDTO объект пользователя
     * @param bindingResult BindingResult
     * @return false с указанием ошибки, либо объект пользователя
     */
    @ApiOperation(value = "Сохранение нового пользователя", response = User.class)
    @PostMapping
    public User post(@Valid UserRequestDTO userRequestDTO,
                     BindingResult bindingResult)  throws EyepanelValidationException {
        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.getAllErrors().toString());
        }

        //если неадмин хочет изменить админа
        final User currentUser = getUser();
        if(userRequestDTO.getRole() == User.Role.ADMIN && currentUser.getRole() != User.Role.ADMIN) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        if(currentUser.getRole() != User.Role.ADMIN) {
            userRequestDTO.setCompany(currentUser.getCompany().getId());
        }

        return userService.save(userRequestDTO);
    }
}
