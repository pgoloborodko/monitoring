package com.landau.core.users.controllers.routes;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.Route;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.RouteRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * Сохранение маршрута
 */
@Api(tags = "Сохранение маршрута", description = "Контроллер может получать данные маршрута, а также создавать новые")
@RestController(value = "RoutesSave")
@RequestMapping("/routes/save")
public class RouteSave extends Base {
    private RouteRepo routeRepo;

    @Autowired
    public RouteSave(RouteRepo routeRepo) {
        this.routeRepo = routeRepo;
    }

    /**
     * Получение данных о маршруте
     * @param route id маршрута
     * @return объект com.landau.core.users.entities.Route
     */
    @ApiOperation(value = "Получение данных о маршруте", response = Route.class)
    @Authority(Route.class)
    @GetMapping
    public Route get(Route route) {
        return route;
    }

    /**
     * Добавление, изменение данных маршрута
     * @param route объект маршрута
     * @param result BindingResult
     * @return false с указанием ошибки, либо объект маршрута
     */
    @ApiOperation(value = "Добавление, изменение данных маршрута", response = Route.class)
    @Authority(Route.class)
    @PostMapping
    public @Valid Route post(@Valid Route route, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  result.getAllErrors().toString());
        }

        if(getUser().getRole() != User.Role.ADMIN) {
            route.setCompany(getUser().getCompany());
        }

        return routeRepo.save(route);
    }
}

