package com.landau.core.users.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.server.ResponseStatusException;

@Api(tags = "Статус авторизации", description = "Контроллер обеспечивает получение статуса авторизации")
@RestController
@RequestMapping("/login/status")
public class UserStatus extends Base {

    /**
     * Получение статуса авторизации
     * @return false, либо объект User
     */
    @ApiOperation(value = "Получение статуса авторизации")
    @GetMapping
    public User get() throws HttpServerErrorException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Object details = auth.getDetails();
        if(details instanceof Long) {
            return userRepo.findById((Long) details).get();
        }
        else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }
}
