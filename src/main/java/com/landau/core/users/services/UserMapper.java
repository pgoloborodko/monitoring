package com.landau.core.users.services;

import com.landau.core.cities.repos.CityRepo;
import com.landau.core.companies.repos.CompanyRepo;
import com.landau.core.districts.repos.DistrictRepo;
import com.landau.core.dto.UserRequestDTO;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Alexey Belov
 */
@Component
public class UserMapper {
    private UserRepo userRepo;
    private CityRepo cityRepo;
    private DistrictRepo districtRepo;
    private CompanyRepo companyRepo;
    @Autowired
    public void setCompanyRepo(CompanyRepo companyRepo) {
        this.companyRepo = companyRepo;
    }
    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    @Autowired
    public void setCityRepo(CityRepo cityRepo) {
        this.cityRepo = cityRepo;
    }
    @Autowired
    public void setDistrictRepo(DistrictRepo districtRepo) {
        this.districtRepo = districtRepo;
    }

    public User fromDto(UserRequestDTO userRequestDTO) {
        User user = null;

        if(userRequestDTO.getId() == null) {
            user = new User();
        }
        else {
            user = userRepo.findById(userRequestDTO.getId()).orElse(null);
        }
        user.setPassword(userRequestDTO.getPassword());
        user.setPhone(userRequestDTO.getPhone());
        user.setRole(userRequestDTO.getRole());
        if (userRequestDTO.getCity() != null ) {
            user.setCity(cityRepo.findById(userRequestDTO.getCity()).orElse(null));
        }
        if (userRequestDTO.getCompany() != null) {
            user.setCompany(companyRepo.findById(userRequestDTO.getCompany()).orElse(null));
        }
        user.setDescription(userRequestDTO.getDescription());

        if (userRequestDTO.getDistrict() != null) {
            user.setDistrict(districtRepo.findById(userRequestDTO.getDistrict()).orElse(null));
        }
        user.setDriverA(userRequestDTO.getDriverA());
        user.setDriverB(userRequestDTO.getDriverB());
        user.setDriverC(userRequestDTO.getDriverC());
        user.setDriverD(userRequestDTO.getDriverD());
        user.setDriverNum(userRequestDTO.getDriverNum());
        user.setDriverSeries(userRequestDTO.getDriverSeries());
        user.setSecondName(userRequestDTO.getSecondName());
        user.setPhotoFileHidden(userRequestDTO.getPhotoFileHidden());
        user.setPhotoFile(userRequestDTO.getPhotoFile());
        user.setPassportSeries(userRequestDTO.getPassportSeries());
        user.setPassportNum(userRequestDTO.getPassportNum());
        user.setPassportIssue(userRequestDTO.getPassportIssue());
        user.setLastName(userRequestDTO.getLastName());
        user.setFirstName(userRequestDTO.getFirstName());

        return user;
    }

}
