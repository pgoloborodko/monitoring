package com.landau.core.users.services;

import com.landau.core.cities.repos.CityRepo;
import com.landau.core.common.exceptions.PhoneExistsException;
import com.landau.core.common.exceptions.UserNotExists;
import com.landau.core.companies.repos.CompanyRepo;
import com.landau.core.districts.repos.DistrictRepo;
import com.landau.core.dto.UserRequestDTO;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import com.landau.core.users.services.mailSender.MailSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private CityRepo cityRepo;

    @Autowired
    private DistrictRepo districtRepo;

    @Autowired
    private MailSender mailSender;


    /**
     * save user to db
     *
     * @param userRequestDto
     * @return
     * @throws PhoneExistsException
     */
    @Transactional
    public User save(UserRequestDTO userRequestDto) {
//        entityManager.clear();
        if (userRequestDto != null && userRequestDto.getPhone() != null) {
            String phone = preparePhone(userRequestDto.getPhone());
            if (userRepo.findByPhone(phone) != null && userRequestDto.getId() == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400 - user with current phone exists");
            }
            else {
                userRequestDto.setPhone(phone);
            }
        }
        if (userRequestDto.getRole() != User.Role.SHADOW) {
            if (userRequestDto.getId() != null) {
//                entityManager.detach(user);

//                final User oldUser = userRepo.findOne(user.getId());
                if (StringUtils.isBlank(userRequestDto.getPassword())) {
                    final Optional<User> oldUser = userRepo.findById(userRequestDto.getId());
                    userRequestDto.setPassword(oldUser.get().getPassword());
                }
                else {
                    userRequestDto.setPassword(bCryptPasswordEncoder.encode(userRequestDto.getPassword()));
                }
            } else {
                log.info("Resetting password for user: {}:<{}>", userRequestDto.getPhone(), userRequestDto.getPassword());
                userRequestDto.setPassword(bCryptPasswordEncoder.encode(userRequestDto.getPassword()));
            }
        }
        // TODO check
        /*if (userRequestDto.getDevice() != null) {
            final Optional<Device> device = deviceRepo.findById(userRequestDto.getDevice());
            Optional<User> optionalUser = userRepo.findByDevice(device.get());
            if (optionalUser.isPresent()){
                if (!optionalUser.get().getId().equals(userRequestDto.getId())) {
                    optionalUser.get().setDevice(null);
                    userRepo.save(optionalUser.get());
                }
            }

        }*/

        return userRepo.save(userMapper.fromDto(userRequestDto));
    }

    public String preparePhone(String phone) {
        return phone.replaceAll("\\+| |\\(|\\)|\\-", "");
    }

    /**
     * @param user
     */
    public void delete(User user) {
        userRepo.delete(user);
    }

    /**
     * auth user
     *
     * @param user
     * @return
     * @throws UserNotExists
     */
    public boolean auth(User user) {
        User getUser = userRepo.findByPhone(user.getPhone());
        System.out.println("is auth: " + getUser.getPassword());
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(getUser, getUser.getPassword(), getUser.getAuthorities());
        usernamePasswordAuthenticationToken.setDetails(getUser);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        return true;
    }

    public boolean addUser(UserRequestDTO userRequestDTO){
        User user;
        if (userRepo.findByEmail(userRequestDTO.getEmail()).isPresent()){
            return false;
        }
        else {
            user = new User();
        }

        user = new User();

        user.setPassword(bCryptPasswordEncoder.encode(userRequestDTO.getPassword()));
        user.setEmail(userRequestDTO.getEmail());
        if (userRequestDTO.getPhone() != null) {
            user.setPhone(preparePhone(userRequestDTO.getPhone()));
        }
        user.setRole(User.Role.OPERATOR);

        if (userRequestDTO.getCompany() != null){
            user.setCompany(companyRepo.findById(userRequestDTO.getCompany()).orElse(null));
        }

        if (userRequestDTO.getCity() != null){
            user.setCity(cityRepo.findById(userRequestDTO.getCity()).orElse(null));
        }

        if (userRequestDTO.getDistrict() != null){
            user.setDistrict(districtRepo.findById(userRequestDTO.getDistrict()).orElse(null));
        }

        user.setPhoto(userRequestDTO.getPhoto());
        user.setDescription(userRequestDTO.getDescription());
        user.setDriverA(userRequestDTO.getDriverA());
        user.setDriverB(userRequestDTO.getDriverB());
        user.setDriverC(userRequestDTO.getDriverC());
        user.setDriverD(userRequestDTO.getDriverD());
        user.setDriverNum(userRequestDTO.getDriverNum());
        user.setDriverSeries(userRequestDTO.getDriverSeries());
        user.setFirstName(userRequestDTO.getFirstName());
        user.setLastName(userRequestDTO.getLastName());
        user.setPassportIssue(userRequestDTO.getPassportIssue());
        user.setPassportNum(userRequestDTO.getPassportNum());
        user.setPassportSeries(userRequestDTO.getPassportSeries());
        user.setPhotoFile(userRequestDTO.getPhotoFile());
        user.setSecondName(userRequestDTO.getSecondName());
        //user.setActivated(false);
        user.setActivationCode(UUID.randomUUID().toString());

        if (!StringUtils.isEmpty(user.getEmail())){
            String message = String.format(
                    "Здравствуйте, %s! \n" +
                            "Добро пожаловать в ChiefSmart. Пожалуйста, пройдите по следующей ссылке, чтобы активировать ваш аккаунт: http://192.168.88.208/login/activate/%s",
                    user.getFirstName(),
                    user.getActivationCode()
            );

            mailSender.send(user.getEmail(), "Activation code", message);
        }

        userRepo.save(user);

        return true;
    }

    public boolean activateUser(String code) {
        User user = userRepo.findByActivationCode(code);

        if (user == null){
            return false;
        }
        //user.setActivated(true);
        user.setActivationCode(null);

        userRepo.save(user);

        return true;
    }
}
