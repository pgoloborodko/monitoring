package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.users.repos.SessionRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


/**
 * Сущность сессии
 */
@Data
@Entity
@JsonInclude
@Repo(SessionRepo.class)
@ApiModel(description = "Модель сессии")
public class Session extends AbstractBusiness {
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "Дата начала сессии")
    private Date startSession;

    @Column
    @ApiModelProperty(value = "Дата конца сессии")
    private Date endSession;

    @ManyToOne
    @ApiModelProperty(value = "Пользователь сессии")
    private User user;

    @Column
    @ApiModelProperty(value = "Выполняется да/нет")
    private Boolean online;

    @Column
    @ApiModelProperty(value = "Идентификатор спринг-сессии")
    private String springSessionId;

    @Transient
    @ApiModelProperty(value = "Текущая да/нет")
    private Boolean current;

    @Transient
    @ApiModelProperty(value = "Список логов сессии")
    private List<SessionLog> sessionLogs;
}
