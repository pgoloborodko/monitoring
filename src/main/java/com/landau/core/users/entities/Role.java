package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Роли пользователей
 */
@Data
@Entity
@Table
@JsonInclude
@ApiModel(description = "Роли пользователей")
public class Role extends AbstractId {
    @Column
    @ApiModelProperty(value = "Название роли")
    private String name;

    @Column
    @ApiModelProperty(value = "Система роли")
    private Integer system;

    @Column
    @ApiModelProperty(value = "Название системы")
    private String systemName;

    @ManyToMany(cascade = CascadeType.ALL)
    @ApiModelProperty(value = "Список пользователей")
    private List<User> users;
}
