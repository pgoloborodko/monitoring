package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.users.repos.TeamRepo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table
@JsonInclude
@Repo(TeamRepo.class)
@ApiModel(description = "Команды")
public class Team extends AbstractBusiness {
    @OneToMany
    private List<User> users;
}
