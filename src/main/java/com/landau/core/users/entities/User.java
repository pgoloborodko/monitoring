package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.SiteApplication;
import com.landau.core.cities.entities.City;
import com.landau.core.common.ImageProcessor;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.devices.entities.Device;
import com.landau.core.districts.entities.District;
import com.landau.core.users.repos.UserRepo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.binary.Base64;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Пользователи системы
 */
@Setter
@Getter
@Entity
@Table(name = "UserTable")
@Repo(UserRepo.class)
@JsonInclude
public class User extends AbstractBusiness implements UserDetails {
    /**
     * Роль пользователя
     */
    public enum Role {
        /**
         * Администратор
         */
        ADMIN,
        /**
         * Оператор
         */
        OPERATOR,
        /**
         * Наблюдатель
         */
        VIEWER,
        /**
         * Патрульный
         */
        PATROL,
        /**
         *
         */
        CHECKPOINT,
        /**
         *
         */
        SHADOW
    }
    @Column
    @ApiModelProperty(value = "Телефон пользователя")
    private String phone;

    @Column
    @ApiModelProperty(value = "Электронная почта пользователя")
    private String email;

    @Column
    @JsonIgnore
    @ApiModelProperty(value = "Пароль пользователя")
    private String password;

    @Column
    @ApiModelProperty(value = "Имя пользователя")
    private String firstName;

    @Column
    @ApiModelProperty(value = "Фамилия пользователя")
    private String lastName;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Отчество пользователя")
    private String secondName;

    @Column
    @ApiModelProperty(value = "Номер паспорта пользователя")
    private String passportNum;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Серия паспорта пользователя")
    private String passportSeries;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Кем выдан паспорт пользователя")
    private String passportIssue;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Номер водительского удостоверения пользователя")
    private String driverNum;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Серия водительского удостоверения пользователя")
    private String driverSeries;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Категория А да/нет")
    private Boolean driverA = false;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Категория B да/нет")
    private Boolean driverB = false;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Категория C да/нет")
    private Boolean driverC = false;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Категория D да/нет")
    private Boolean driverD = false;

    @Column
    @ApiModelProperty(value = "Путь к фото пользователя")
    private String photo;

    @Column
    @ApiModelProperty(value = "Роль пользователя")
    private Role role;

    @ManyToOne
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Город пользователя")
    private City city;

    @ManyToOne
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Район пользователя")
    private District district;

    @Transient
    @JsonIgnore
    @ApiModelProperty(value = "Фото-файл пользователя")
    private MultipartFile photoFile;

    @Transient
    @JsonIgnore
    private String photoFileHidden;

    @Column
    @ApiModelProperty(value = "Описание пользователя")
    private String description;

    @Column
    private String activationCode;

    @Column
    @ApiModelProperty(value = "Пользователь активирован (да/нет)")
    private Boolean isActivated;

    @ManyToOne
    private Device device;

    private Boolean getState() {
        if(device != null) {
            return device.getOnline();
        }

        return false;
    }

    /**
     * Фото-файл
     * @return
     */
    @JsonIgnore
    public File getPhotoAsFile() {
        return new File(SiteApplication.DIR_AVATARS + getPhoto());
    }

    @JsonIgnore
    public void setPhotoFile(MultipartFile photoFile) {
        this.photoFile = photoFile;
        try {
            if (photoFile == null || photoFile.isEmpty()) return;

            String newPath = UUID.randomUUID().toString() + ".jpeg";

            try {
                BufferedImage bufferedImage = ImageIO.read(photoFile.getInputStream());
                ImageProcessor ip = new ImageProcessor();
                bufferedImage = ip.resizeImage(bufferedImage);
                ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
                ImageWriteParam param = writer.getDefaultWriteParam();
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
                param.setCompressionQuality(1.0F); // Highest quality

                File outputFile = new File(SiteApplication.DIR_AVATARS + newPath);

                writer.setOutput(new FileImageOutputStream(outputFile));
                writer.write(bufferedImage);

                setPhoto(newPath);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        catch (NullPointerException ex2) {
            ex2.printStackTrace();
        }
        catch (IllegalArgumentException ex2) {
            ex2.printStackTrace();
        }
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authList = new ArrayList<>();
        authList.add( new SimpleGrantedAuthority("ROLE_ADMIN"));
        return authList;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return null;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return false;
    }

    public void setPhotoFileHidden(String photoFileHidden) {
        this.photoFileHidden = photoFileHidden;

        try {
            if (photoFileHidden == null || photoFileHidden.isEmpty()) return;

            String newPath = UUID.randomUUID().toString() + ".jpeg";

            try {
                byte[] data = Base64.decodeBase64(photoFileHidden);

                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(data));
                ImageProcessor ip = new ImageProcessor();
                bufferedImage = ip.resizeImage(bufferedImage);
                ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
                ImageWriteParam param = writer.getDefaultWriteParam();
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
                param.setCompressionQuality(1.0F); // Highest quality

                File outputFile = new File(SiteApplication.DIR_AVATARS + newPath);

                writer.setOutput(new FileImageOutputStream(outputFile));
                writer.write(bufferedImage);

                setPhoto(newPath);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        catch (NullPointerException | IllegalArgumentException ex2) {
            ex2.printStackTrace();
        }
    }
}
