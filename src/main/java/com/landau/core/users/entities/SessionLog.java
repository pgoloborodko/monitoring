package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.users.repos.SessionLogRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Лог сессии
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(SessionLogRepo.class)
@ApiModel(description = "Лог сессии")
public class SessionLog extends AbstractId {
    /**
     * Тип запроса
     */
    public enum Type{
        /**
         * GET-запрос
         */
        GET,
        /**
         * POST-запрос
         */
        POST
    }

    @Column
    @ApiModelProperty(value = "Описание лога сессии")
    private String description;

    @Column
    @ApiModelProperty(value = "Дата лога сессии")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @Column
    @ApiModelProperty(value = "Тип REST-запроса в логе")
    private Type type;

    @ManyToOne
    @JsonIgnore
    @ApiModelProperty(value = "Сессия")
    private Session session;
}
