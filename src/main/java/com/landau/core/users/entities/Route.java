package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.cities.entities.City;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.districts.entities.District;
import com.landau.core.users.repos.RouteRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Маршруты
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(RouteRepo.class)
@ApiModel(description = "Маршруты")
public class Route extends AbstractBusiness {
    @Column
    @ApiModelProperty(value = "Название маршрута")
    private String name;

    @Column
    @ApiModelProperty(value = "Координаты маршрута")
    private String coords;

    @ManyToOne
    @ApiModelProperty(value = "Город маршрута")
    private City city;

    @ManyToOne
    @ApiModelProperty(value = "Район маршрута")
    private District district;
}
