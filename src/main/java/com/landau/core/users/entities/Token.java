package com.landau.core.users.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.users.repos.TokenRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/**
 * Токены API
 */
@Data
@Entity
@Table(indexes = {@Index(columnList="value", unique = true)})
@JsonInclude
@Repo(TokenRepo.class)
@ApiModel(description = "Токены API")
public class Token extends AbstractId {
    @OneToOne
    @ApiModelProperty(value = "Пользователь токена API")
    private User user;

    @Column
    @ApiParam(hidden = true)
    @ApiModelProperty(value = "Значение токена API")
    private String value;

    @Column
    @ApiModelProperty(value = "Название токена API")
    private String name;

    @Column
    @ApiModelProperty(value = "Активен/неактивен")
    private Boolean status;


    public void generateValue() {
        value = UUID.randomUUID().toString();
    }
}
