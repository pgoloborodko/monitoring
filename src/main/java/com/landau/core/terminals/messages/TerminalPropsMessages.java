package com.landau.core.terminals.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.objects.props.TerminalProps;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Slf4j
@Service
public class TerminalPropsMessages implements IService {
    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CastService castService;

    /**
     * метод отвечает за обработку сообщений типа TerminalProps
     * @param terminalPropsMessage
     */
    @ReConsumer(listen = TerminalProps.class)
    protected void consumeTerminalProps(TerminalProps terminalPropsMessage){
        log.info("consume message: {}", TerminalProps.class);
        Terminal terminal = castService.castToEntity(terminalPropsMessage);
        if (terminal != null){
            castService.getTerminalRepo().save(terminal);
            log.info("save: {}", Terminal.class);
        }
        else {
            log.warn("null value: {}", TerminalProps.class);
        }
    }
}
