package com.landau.core.terminals.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.entities.DeviceSettings;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.devices.repos.DeviceSettingsRepo;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.objects.data.Devices;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Slf4j
@Data
@Service
public class TerminalDataMessages implements IService {
    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private DeviceSettingsRepo deviceSettingsRepo;

    @Autowired
    private CastService castService;

    @ReConsumer(listen = Devices.class)
    public void consumeDevices(Devices devices){
        log.info("consume message: {}", devices.getClass().getSimpleName());
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(devices.getSerial());

        if (optionalTerminal.isPresent()){
            for (Devices.Device messageDevice: devices.getDevices()){
                Device device = castService.castToEntity(messageDevice.getProps());
                deviceRepo.save(device);
                log.info("save: {}", device.getClass().getSimpleName());

                DeviceSettings deviceSettings = castService.castToEntity(messageDevice.getSettings());

                deviceSettingsRepo.save(deviceSettings);
                log.info("save: {}", deviceSettings.getClass().getSimpleName());
            }
        }
    }

}
