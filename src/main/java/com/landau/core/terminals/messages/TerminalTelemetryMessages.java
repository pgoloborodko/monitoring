package com.landau.core.terminals.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.terminals.repos.TerminalTelemetryRepo;
import com.landau.objects.telemetry.TerminalTelemetry;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Slf4j
@Service
public class TerminalTelemetryMessages implements IService {
    @Autowired
    private TerminalTelemetryRepo terminalTelemetryRepo;

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CastService castService;

    /**
     * метод отвечает за обработку сообщений типа TerminalTelemetry
     * @param terminalTelemetryMessage
     */
    @ReConsumer(listen = TerminalTelemetry.class)
    public void consumeTerminalTelemetry(TerminalTelemetry terminalTelemetryMessage){
        log.info("consume message: " + terminalTelemetryMessage.getClass().getSimpleName());
        com.landau.core.terminals.entities.TerminalTelemetry terminalTelemetry = castService.castToEntity(terminalTelemetryMessage);
        if (terminalTelemetry != null){
            terminalTelemetryRepo.save(terminalTelemetry);
            log.info("save: " + terminalTelemetry.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", TerminalTelemetry.class);
        }
    }
}
