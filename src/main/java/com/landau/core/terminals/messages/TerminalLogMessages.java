package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.messages.core.CoreTerminalMessages;
import com.landau.objects.action.GetLogFile;
import com.landau.objects.action.GetLogList;
import com.landau.objects.data.LogFile;
import com.landau.objects.data.LogList;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
@Slf4j
@Data
@Service
public class TerminalLogMessages implements IService {
    @Autowired
    private CoreTerminalMessages messages;

    @Autowired
    private TerminalRepo terminalRepo;

    private HashMap<String, DeferredResult<Object>> logListMessages = new HashMap<>();
    private HashMap<String, DeferredResult<Object>> logFileMessages = new HashMap<>();

    /**
     * Метод отвечает за отправку сообщения типа GetLogList
     * @param terminal
     * @return
     * @throws RepublisherException
     */
    public DeferredResult<Object> requestList(Terminal terminal) throws RepublisherException {
        DeferredResult<Object> deferredResult = new DeferredResult<>();

        logListMessages.put(terminal.getSerial(), deferredResult);

        messages.send(terminal.getSerial(), new GetLogList());
        log.info("send message: {}", GetLogList.class);

        return deferredResult;
    }

    /**
     * Метод отвечает за отправку сообщения типа GetLogFile
     * @param terminal
     * @param file
     * @return
     * @throws RepublisherException
     */
    public DeferredResult<Object> requestFile(Terminal terminal, String file) throws RepublisherException {
        DeferredResult<Object> deferredResult = new DeferredResult<>();

        logFileMessages.put(terminal.getSerial(), deferredResult);

        messages.send(terminal.getSerial(), new GetLogFile(file));
        log.info("send message: {}", GetLogList.class);

        return deferredResult;
    }

    /**
     * Метод отвечает за обработку сообщения типа LogList
     * @param logList
     */
    @ReConsumer(listen = LogList.class)
    public void consumeLogList(LogList logList){
        log.info("consume message: {}", LogList.class);
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(logList.getSerial());
        if (optionalTerminal.isPresent()){
            DeferredResult<Object> deferredResult = logListMessages.get(logList.getSerial());
            deferredResult.setResult(logList.getLogs());
            logListMessages.remove(logList.getSerial());
        }
    }

    /**
     * Метод отвечает за обработку сообщения типа LogFile
     * @param logFile
     */
    @ReConsumer(listen = LogFile.class)
    public void consumeLogFile(LogFile logFile){
        log.info("consume message: {}", LogFile.class);
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(logFile.getSerial());

        if (optionalTerminal.isPresent()){
            DeferredResult<Object> deferredResult = logFileMessages.get(logFile.getSerial());
            byte[] bytes = ArrayUtils.toPrimitive(logFile.getFile());

            if (bytes != null){
                ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
                try (GZIPInputStream in = new GZIPInputStream(bis)){
                    String theString = IOUtils.toString(in);
                    deferredResult.setResult(theString);
                } catch (IOException e){
                    deferredResult.setErrorResult(e.getMessage());
                }
            }
            logFileMessages.remove(logFile.getSerial());
        }
    }
}
