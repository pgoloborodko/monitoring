package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.terminals.repos.TerminalSettingsRepo;
import com.landau.messages.core.CoreTerminalMessages;
import com.landau.objects.action.SetTerminalSettings;
import com.landau.objects.settings.TerminalSettings;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Slf4j
@Service
public class TerminalSettingsMessages implements IService {
    @Autowired
    private TerminalSettingsRepo terminalSettingsRepo;

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CoreTerminalMessages messages;

    @Autowired
    private CastService castService;

    /**
     * метод отвечает за отправку сообщений типа TerminalSettings
     * @param settings
     * @throws RepublisherException
     */
    public void sendTerminalSettings(com.landau.core.terminals.entities.TerminalSettings settings) throws RepublisherException {
        SetTerminalSettings setTerminalSettings = castService.castToMessage(settings);
        terminalSettingsRepo.save(settings);
        messages.send(settings.getSerial(), setTerminalSettings);
        log.info("send message: {}", SetTerminalSettings.class);
    }

    /**
     * метод отвечает за обработку сообщений типа TerminalSettings
     * @param terminalSettingsMessage
     */
    @ReConsumer(listen = TerminalSettings.class)
    public void consumeTerminalSettings(TerminalSettings terminalSettingsMessage){
        log.info("consume message: " + terminalSettingsMessage.getClass().getSimpleName());
        com.landau.core.terminals.entities.TerminalSettings terminalSettings = castService.castToEntity(terminalSettingsMessage);
        if (terminalSettings != null){
            terminalSettingsRepo.save(terminalSettings);
            log.info("save: " + terminalSettings.getClass().getSimpleName());
        }
        else {
            log.warn("null value: {}", TerminalSettings.class);
        }
    }
}
