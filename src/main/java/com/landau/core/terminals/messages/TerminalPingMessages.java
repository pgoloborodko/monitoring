package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.objects.data.Page;
import com.landau.objects.telemetry.Ping;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
@Slf4j
@Data
@Service
public class TerminalPingMessages implements IService {
    private static long timeout = 30000;
    private HashMap<Terminal, Timer> pings = new HashMap<>();

    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private CoreBrowserMessages messages;

    /**
     * wait ping from terminal
     * @param ping
     */
    @ReConsumer(listen = Ping.class)
    public void consumePing(Ping ping) {
        log.info("consume message: {}", Page.class);
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(ping.getSerial());

        if(optionalTerminal.isPresent()) {
            Terminal terminal = optionalTerminal.get();

            if(pings.containsKey(terminal)) {
                pings.get(terminal).cancel();
            }
            else {
                terminal.setOnline(true);
                terminalRepo.save(terminal);

                try {
                    messages.send("", terminal);
                    log.info("send message: {}", Terminal.class);
                } catch (RepublisherException e) {
                    e.printStackTrace();
                }
            }

            Timer timer = new Timer();
            timer.schedule(new PingTimerTask(terminal), timeout);
            pings.put(terminal, timer);
        }
    }

    private class PingTimerTask extends TimerTask {
        private Terminal terminal;

        public PingTimerTask(Terminal terminal) {
            this.terminal = terminal;
        }

        @Override
        public void run() {
            Optional<Terminal> optionalTerminal = terminalRepo.findById(terminal.getId());

            if(optionalTerminal.isPresent()) {
                Terminal terminal = optionalTerminal.get();
                terminal.setOnline(false);
                terminalRepo.save(terminal);

                pings.remove(terminal);
                try {
                    messages.send("", terminal);
                    log.info("send message: {}", Terminal.class);
                } catch (RepublisherException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
