package com.landau.core.terminals.messages;

import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.common.messages.CastService;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import com.landau.objects.data.Profile;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Data
@Service
public class TerminalProfileMessages implements IService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private CastService castService;

    @ReConsumer(listen = Profile.class)
    public void consumeProfile(Profile message){
        User user = castService.castToEntity(message);
        //TODO добавить обработку Profile
    }
}
