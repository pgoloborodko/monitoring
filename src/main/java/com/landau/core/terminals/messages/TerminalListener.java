package com.landau.core.terminals.messages;

import com.lampa.republish.interfaces.IService;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.media.repos.MediaFileRepo;
import com.landau.core.stats.repos.TerminalStatsRepo;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.users.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;

@Component
@Repo(TerminalRepo.class)
public class TerminalListener implements IService {
    @Autowired
    private TerminalRepo terminalRepo;

    @Autowired
    private MediaFileRepo mediaFileRepo;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private TerminalStatsRepo terminalStatsRepo;

    @Autowired
    private UserRepo userRepo;

    /*@Autowired
    private MessageService messageService;*/

    private HashMap<Integer, DeferredResult<Object>> messagesStack = new HashMap<>();



/*    @ObjectListener(object = ResponseInfoObjectV1.class)
    public Terminal ResponseInfoObjectV1(Terminal terminal, ResponseInfoObjectV1 responseInfoObjectV1) {
        terminal.setExtractedDevices(responseInfoObjectV1.getExtractedDevices());
        terminal.setSyncDevices(responseInfoObjectV1.getSyncDevices());
        terminal.setOkDevices(responseInfoObjectV1.getOkDevices());
        terminal.setChargeDevices(responseInfoObjectV1.getChargeDevices());
        terminal.setHoldSpace(responseInfoObjectV1.getHoldSpace());
        terminal.setAllSpace(responseInfoObjectV1.getAllSpace());
        terminal.setFreeSpace(responseInfoObjectV1.getFreeSpace());

        return terminal;
    }*/

 /*   @ObjectListener(object = ResponseTerminalLogListObjectV1.class)
    public Terminal ResponseTerminalLogListObjectV1(Terminal terminal, ResponseTerminalLogListObjectV1 responseTerminalLogListObjectV1) {
        DeferredResult<Object> deferredResult = messagesStack.get(responseTerminalLogListObjectV1.getId());
        deferredResult.setResult(responseTerminalLogListObjectV1.getLogList());
        messagesStack.remove(responseTerminalLogListObjectV1.getId());

        return terminal;
    }*/

/*    @ObjectListener(object = ResponseTerminalLogFileObjectV1.class)
    public Terminal ResponseTerminalLogFileObjectV1(Terminal terminal, ResponseTerminalLogFileObjectV1 responseTerminalLogFileObjectV1) {
        byte[] byteArrayInputStream = responseTerminalLogFileObjectV1.getLogFileByteArray();
        ByteArrayInputStream bis = new ByteArrayInputStream(byteArrayInputStream);

        DeferredResult<Object> deferredResult = messagesStack.get(responseTerminalLogFileObjectV1.getId());

        try (GZIPInputStream in = new GZIPInputStream(bis)){
            String theString = IOUtils.toString(in);
            deferredResult.setResult(theString);

        } catch (IOException e){
            deferredResult.setErrorResult(e.getMessage());
        }

        messagesStack.remove(responseTerminalLogFileObjectV1.getId());

        return terminal;
    }

    @ObjectListener(object = ResponseMediaFileV1.class)
    public Terminal ResponseMediaFileV1(Terminal terminal, ResponseMediaFileV1 response) {
        Optional<MediaFile> external = mediaFileRepo.findByExternal(response.getExternal());
        Optional<Device> optionalDevice = deviceRepo.findBySerial(response.getDeviceSerial());

        MediaFile mediaFile = external.orElseGet(MediaFile::new);
        mediaFile.setExternal(response.getExternal());
        mediaFile.setExternalPath(response.getExternalPath());
        mediaFile.setCreated(response.getCreated());
        mediaFile.setDuration((double) response.getDuration());

        mediaFile.setTerminal(terminal);

        if(optionalDevice.isPresent()) {
            mediaFile.setDevice(optionalDevice.get());
        }
        else {
            Device device = new Device();
            device.setSerial(response.getDeviceSerial());
            device.setName(response.getDeviceSerial());

            mediaFile.setDevice(deviceRepo.save(device));
        }

        mediaFileRepo.save(mediaFile);

        return terminal;
    }

    @ObjectListener(object = ResponseStatsObjectV1.class)
    public Terminal ResponseStatsObjectV1(Terminal terminal, ResponseStatsObjectV1 responseStatsObjectV1) {
        Date currentDateStart = new Date();
        currentDateStart.setMinutes(0);
        currentDateStart.setSeconds(0);

        Date currentDateEnd = new Date(currentDateStart.getTime());
        currentDateEnd.setMinutes(59);
        currentDateEnd.setSeconds(59);

        TerminalStats stats = null;
        Optional<TerminalStats> optionalStats = terminalStatsRepo.findByDateBetweenAndCompany(currentDateStart, currentDateEnd, terminal.getCompany());
        stats = optionalStats.orElse(new TerminalStats());

        stats.setDate(new Date());
        stats.setDeviceExtractCount(responseStatsObjectV1.getDeviceExtractCount());
        stats.setDeviceInsertCount(responseStatsObjectV1.getDeviceInsertCount());
        stats.setSyncFilesCount(responseStatsObjectV1.getSyncFilesCount());
        stats.setSyncFilesInStorage(responseStatsObjectV1.getSyncFilesInStorage());
        stats.setSyncFilesSize(responseStatsObjectV1.getSyncFilesSize());
        stats.setCompany(terminal.getCompany());

        terminalStatsRepo.save(stats);

        return terminal;
    }

    @ObjectListener(object = ResponseDeviceEventObjectV2.class)
    public Terminal ResponseDeviceEventObjectV2(Terminal terminal, ResponseDeviceEventObjectV2 responseDeviceEventObjectV2) {
        Event event = new Event();

        if (responseDeviceEventObjectV2.getType() != null){
            event.setType(Event.Type.valueOf(responseDeviceEventObjectV2.getType().name()));
        }

        Optional<User> optionalUser = userRepo.findById(responseDeviceEventObjectV2.getUserId());
        Optional<Device> optionalDevice = deviceRepo.findBySerial(responseDeviceEventObjectV2.getDeviceSerial());

        optionalUser.ifPresent(event::setUser);
        optionalDevice.ifPresent(event::setDevice);
        event.setCreated(responseDeviceEventObjectV2.getEventDate());
        event.setAdded(new Date());

        eventRepo.save(event);

        return terminal;
    }

    public DeferredResult<Object> requestPage(Terminal terminal, String url, HttpServletRequest request) {
        DeferredResult<Object> deferredResult = new DeferredResult<>();

        RequestPageObjectV1 requestPageObjectV1 = new RequestPageObjectV1();
        requestPageObjectV1.setUrl(url);
        requestPageObjectV1.setId(requestPageObjectV1.hashCode());

        HttpHeaders httpHeaders = new HttpHeaders();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            httpHeaders.set(key, value);
        }

        requestPageObjectV1.setHeaders(httpHeaders);

        if(request.getMethod().toLowerCase().equals("post")) {
            requestPageObjectV1.setMethod(HttpMethod.POST);
            requestPageObjectV1.setParameters(request.getParameterMap());
        }

        messagesStack.put(requestPageObjectV1.getId(), deferredResult);

        messageService.send(terminal, requestPageObjectV1);

        return deferredResult;
    }

    public DeferredResult<Object> requestList(Terminal terminal) {
        DeferredResult<Object> deferredResult = new DeferredResult<>();

        RequestTerminalLogListObjectV1 requestTerminalLogListObjectV1 = new RequestTerminalLogListObjectV1();
        requestTerminalLogListObjectV1.setId(requestTerminalLogListObjectV1.hashCode());

        messagesStack.put(requestTerminalLogListObjectV1.getId(), deferredResult);

        messageService.send(terminal, requestTerminalLogListObjectV1);

        return deferredResult;
    }

    public DeferredResult<Object> requestFile(Terminal terminal, String file) {
        DeferredResult<Object> deferredResult = new DeferredResult<>();

        RequestTerminalLogFileObjectV1 requestTerminalLogFileObjectV1 = new RequestTerminalLogFileObjectV1();

        requestTerminalLogFileObjectV1.setId(requestTerminalLogFileObjectV1.hashCode());

        requestTerminalLogFileObjectV1.setFileName(file);

        messagesStack.put(requestTerminalLogFileObjectV1.getId(), deferredResult);

        messageService.send(terminal, requestTerminalLogFileObjectV1);

        return deferredResult;
    }*/
}
