package com.landau.core.terminals.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.messages.core.CoreTerminalMessages;
import com.landau.objects.action.GetPage;
import com.landau.objects.common.HttpMethod;
import com.landau.objects.data.Page;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@Data
@Service
public class TerminalPageMessages implements IService {
    private CoreTerminalMessages messages;
    private TerminalRepo terminalRepo;

    private HashMap<Integer, DeferredResult<Object>> requestPageMessages = new HashMap<>();

    @Autowired
    public TerminalPageMessages(CoreTerminalMessages messages, TerminalRepo terminalRepo) {
        this.messages = messages;
        this.terminalRepo = terminalRepo;
    }

    /**
     * запрос локального http ресурса
     * @param terminal
     * @param url
     * @param request
     * @return
     */
    public DeferredResult<Object> requestPage(Terminal terminal, String url, HttpServletRequest request) throws RepublisherException {
        HashMap<String, List<String>> headers;
        GetPage getPage = new GetPage();
        getPage.setUrl(url);

        HttpHeaders httpHeaders = new HttpHeaders();
        Enumeration headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            httpHeaders.set(key, value);
        }

        Iterator<Map.Entry<String, List<String>>> setIterator = httpHeaders.entrySet().iterator();

        headers = new HashMap<>();

        while(setIterator.hasNext()){
            Map.Entry<String, List<String>> entry = setIterator.next();
            headers.put(entry.getKey(), entry.getValue());
        }

        getPage.setHeaders(headers);

        if(request.getMethod().toLowerCase().equals("post")) {
            getPage.setHttpMethod(HttpMethod.POST);
            Map<String, String[]> requestParameters = request.getParameterMap();
            Map<String, List<String>> parameters = new HashMap<>();

            for (Map.Entry<String, String[]> param: requestParameters.entrySet()){
                parameters.put(param.getKey(), new ArrayList<>(Arrays.asList(param.getValue())));
            }

            getPage.setParameters(parameters);
        }
        else {
            getPage.setHttpMethod(HttpMethod.GET);
        }

        DeferredResult<Object> deferredResult = new DeferredResult<>();
        getPage.setExternal(deferredResult.hashCode());
        requestPageMessages.put(deferredResult.hashCode(), deferredResult);

        messages.send(terminal.getSerial(), getPage);
        log.info("send message: {}", GetPage.class);

        return deferredResult;
    }

    /**
     * Метод отвечает за обработку сообщения типа Page
     * @param page
     */
    @ReConsumer(listen = Page.class)
    public void consumePage(Page page){
        log.info("consume message: {}", Page.class);
        Optional<Terminal> optionalTerminal = terminalRepo.findBySerial(page.getSerial());
        if (optionalTerminal.isPresent()) {
            DeferredResult<Object> deferredResult = requestPageMessages.get(page.getExternal());
            HttpHeaders headers = new HttpHeaders();
            if (page.getHeaders() != null){
                headers.putAll(page.getHeaders());
            }

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(ArrayUtils.toPrimitive(page.getBody()), headers, HttpStatus.valueOf(page.getStatus().name()));
            deferredResult.setResult(responseEntity);
            requestPageMessages.remove(page.getSerial());
        }
    }
}
