package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.entities.TerminalTelemetry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TerminalTelemetryRepo extends JpaRepository<TerminalTelemetry, Long> {

    Optional<TerminalTelemetry> findByTerminal(Terminal terminal);
}
