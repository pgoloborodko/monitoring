package com.landau.core.terminals.repos;

import com.landau.core.common.repos.JpaSerialRepository;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.filters.TerminalsFilter;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface TerminalRepo extends JpaSerialRepository<Terminal, Long>, JpaSpecificationExecutor<TerminalsFilter> {
    Long countAllByOnline(Boolean online);

    @Modifying
    @Transactional
    @Query("UPDATE Terminal t SET online = :online")
    Integer updateStatus(@Param("online") Boolean online);
}
