package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.entities.TerminalSettings;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TerminalSettingsRepo extends JpaRepository<TerminalSettings, Long> {

    /**
     * Метод для тестов
     * @param terminal
     * @return
     */
    Optional<TerminalSettings> findByTerminal(Terminal terminal);
}
