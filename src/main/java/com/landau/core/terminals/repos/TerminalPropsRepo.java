package com.landau.core.terminals.repos;

import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.entities.TerminalProps;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TerminalPropsRepo extends JpaRepository<TerminalProps, Long> {

    /**
     * Метод для тестов
     * @param terminal
     * @return
     */
    Optional<TerminalProps> findByTerminal(Terminal terminal);
}
