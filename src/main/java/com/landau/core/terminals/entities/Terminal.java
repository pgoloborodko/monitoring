package com.landau.core.terminals.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.landau.core.cities.entities.City;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusinessSerial;
import com.landau.core.districts.entities.District;
import com.landau.core.terminals.repos.TerminalRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Терминалы
 */
@Data
@Entity
@Table
@JsonInclude
@Repo(TerminalRepo.class)
@ApiModel(description = "Терминал")
public class Terminal extends AbstractBusinessSerial {
    @Column
    @ApiModelProperty(value = "Название терминала")
    private String name;

    @ManyToOne
    @ApiModelProperty(value = "Город терминала")
    private City city;

    @ManyToOne
    @ApiModelProperty(value = "Район терминала")
    private District district;

    @Column
    @ApiModelProperty(value = "Общее дисковое пространство терминала")
    private Long allSpace;

    @Column
    @ApiModelProperty(value = "Свободное дисковое пространство терминала")
    private Long freeSpace;

    @Column
    @ApiModelProperty(value = "Занятое дисковое пространство терминала")
    private Long holdSpace;

    @Column
    @ApiModelProperty(value = "Синхронизирующиеся устройства")
    private Integer syncDevices;

    @Column
    @ApiModelProperty(value = "Заряжающиеся устройства")
    private Integer chargeDevices;

    @Column
    @ApiModelProperty(value = "Изъятые устройства")
    private Integer extractedDevices;

    @Column
    @ApiModelProperty(value = "Количество готовых к выдаче устройств")
    private Integer okDevices;

    @Column
    @ApiModelProperty(value = "тип")
    private String type;

    @Column
    @ApiModelProperty(value = "версия агента")
    private String agentVersion;

    @Column
    @ApiModelProperty(value = "количество регистраторов")
    private Byte capacity;

    @Column
    @ApiModelProperty(value = "свет блокировки")
    private Byte blockCount;

    @Column
    @ApiModelProperty(value = "таймер блокировки")
    private Integer blockTimer;

    @Column
    @ApiModelProperty(value = "время на аутентификацию")
    private Integer authTime;

    @Column
    @ApiModelProperty(value = "время на операцию")
    private Integer operationTime;

    @Column
    @ApiModelProperty(value = "путь для сохранения файла")
    private String[] savePath;

    @Column
    @ApiModelProperty(value = "путь к внешнему хранилищу")
    private String externalPath;

    @Column
    @ApiModelProperty(value = "перезапись да/нет")
    private Boolean rewrite;

    @Column
    @ApiModelProperty(value = "минимальное свободное место")
    private Long minFreeSpace;

    @Column
    @ApiModelProperty(value = "время на сохранение файла")
    private Integer fileStorageTime;
}
