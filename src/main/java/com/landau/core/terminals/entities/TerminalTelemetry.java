package com.landau.core.terminals.entities;

import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table
@ApiModel(description = "Телеметрия терминала")
public class TerminalTelemetry extends AbstractId {
    @Column
    @ApiModelProperty(value = "количество подключенных устройств")
    private Byte deviceConnected;

    @Column
    @ApiModelProperty(value = "количество отключенных устройств")
    private Byte deviceDisconnected;

    @Column
    @ApiModelProperty(value = "загрузка оперативной памяти")
    private Byte cpuLoad;

    @Column
    @ApiModelProperty(value = "загрузка видеопамяти")
    private Byte gpuLad;

    @ManyToOne
    @ApiModelProperty(value = "сущность терминала")
    private Terminal terminal;
}
