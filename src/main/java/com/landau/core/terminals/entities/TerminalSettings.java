package com.landau.core.terminals.entities;

import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusinessSerial;
import com.landau.core.terminals.repos.TerminalSettingsRepo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
@Data
@ApiModel(description = "Настройки терминала")
@Repo(TerminalSettingsRepo.class)
public class TerminalSettings extends AbstractBusinessSerial {
    @Column
    @ApiModelProperty(value = "отсчет до блокировки")
    private Byte blockCount;

    @Column
    @ApiModelProperty(value = "таймер блокировки")
    private Integer blockTimer;

    @Column
    @ApiModelProperty(value = "вреям на аутентификацию")
    private Integer authTime;

    @Column
    @ApiModelProperty(value = "время на операцию")
    private Integer operationTime;

    @Column
    @ApiModelProperty(value = "путь сохранения файла")
    private String[] savePath;

    @Column
    @ApiModelProperty(value = "руть сохранения файла во внешнее хранилище")
    private String externalPath;

    @Column
    @ApiModelProperty(value = "перезапись вкл/выкл")
    private Boolean rewrite;

    @Column
    @ApiModelProperty(value = "минимальное свободное место")
    private Long minFreeSpace;

    @Column
    @ApiModelProperty(value = "время на сохранение файла")
    private Integer fileStorageTime;

    @ManyToOne
    @ApiModelProperty(value = "сущность терминала")
    private Terminal terminal;
}
