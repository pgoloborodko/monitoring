package com.landau.core.terminals.entities;

import com.landau.core.common.entities.AbstractId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Table
@Entity
@ApiModel(description = "Разрешения терминала")
public class TerminalProps extends AbstractId {
    @Column
    private String type;

    @Column
    @ApiModelProperty(value = "версия агента")
    private String agentVersion;

    @Column
    @ApiModelProperty(value = "количество регистраторов")
    private Byte capacity;

    @ManyToOne
    @ApiModelProperty(value = "сущность терминала")
    private Terminal terminal;
}
