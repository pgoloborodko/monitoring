package com.landau.core.terminals.specs;

import com.landau.core.terminals.filters.TerminalsFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;

public class TerminalsSpec {
    public static Specification<TerminalsFilter> find(TerminalsFilter terminalsFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (terminalsFilter.getSerial() != null && !terminalsFilter.getSerial().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("serial")), "%" + terminalsFilter.getSerial().toUpperCase() +"%"));
            }

            if (terminalsFilter.getName() != null && !terminalsFilter.getName().isEmpty()) {
                predicates.add(cb.like(cb.upper(root.get("name")), "%" + terminalsFilter.getName().toUpperCase() +"%"));
            }

            if (terminalsFilter.getCompany() != null) {
                predicates.add(cb.equal(root.get("company"), terminalsFilter.getCompany()));
            }

            if (terminalsFilter.getCity() != null) {
                predicates.add(cb.equal(root.get("city"), terminalsFilter.getCity()));
            }

            if (terminalsFilter.getDistrict() != null) {
                predicates.add(cb.equal(root.get("district"), terminalsFilter.getDistrict()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
