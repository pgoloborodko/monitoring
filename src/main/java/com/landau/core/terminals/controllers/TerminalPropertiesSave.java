package com.landau.core.terminals.controllers;


import com.lampa.republish.RepublisherException;
import com.landau.core.common.ApiPageable;
import com.landau.core.terminals.entities.TerminalSettings;
import com.landau.core.terminals.messages.TerminalSettingsMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
@Slf4j
@Data
@Api(tags = "Настройки Терминалов", description = "Контроллер для работы с настройками терминалов")
@RestController
@RequestMapping("/terminals/settings")
public class TerminalPropertiesSave {
    private TerminalSettingsMessages terminalSettingsMessages;

    @Autowired
    public TerminalPropertiesSave(TerminalSettingsMessages terminalSettingsMessages) {
        this.terminalSettingsMessages = terminalSettingsMessages;
    }

    /**
     * Запрос на установку переданных настроек терминала
     * @param terminalSettings настройки устройства для запроса
     * @exception ResponseStatusException INTERNAL_SERVER_ERROR - ошибка на стороне сервера, ошибка отправки сообщения
     * @exception ResponseStatusException BAD_REQUEST - ошибка на стороне клиента, неправильный запрос,
     * возможно, отсутствуют необходимые для работы параметры
     */

    @ApiPageable
    @ApiOperation(value = "Запрос на установку настроек на терминал")
    @PostMapping("/setSettings")
    public void requestSetTerminalSettings(TerminalSettings terminalSettings){
        try {
            terminalSettingsMessages.sendTerminalSettings(terminalSettings);
        }
        catch (RepublisherException e) {
            log.error("error send terminal settings", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
        }
    }
}
