package com.landau.core.terminals.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.filters.TerminalsFilter;
import com.landau.core.terminals.repos.TerminalRepo;
import com.landau.core.terminals.specs.TerminalsSpec;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Список терминалов
 */
@Api(tags = "Список Терминалов", description = "Контроллер для получения списка терминалов")
@RestController("TerminalsIndex")
@RequestMapping("/terminals")
public class TerminalList extends Base {
    private TerminalRepo terminalRepo;

    @Autowired
    public TerminalList(TerminalRepo terminalRepo) {
        this.terminalRepo = terminalRepo;
    }

    /**
     * Получение списка терминалов
     * @param terminalsFilter поля фильтра
     * @return список терминалов
     */
    @ApiOperation(value = "Получение списка терминалов по фильтру", response = Terminal.class)
    @GetMapping
    public Page<TerminalsFilter> get(TerminalsFilter terminalsFilter, Pageable pageable) {
        if(getUser().getRole() != User.Role.ADMIN) {
            terminalsFilter.setCompany(getUser().getCompany());
        }

        return terminalRepo.findAll(TerminalsSpec.find(terminalsFilter), pageable);
    }
}