package com.landau.core.terminals.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.messages.TerminalLogMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.server.ResponseStatusException;

/**
 * Просмотр списка и получения файла логов терминала
 */
@Slf4j
@Data
@Api(tags = "Логи терминала", description = "Контроллер обеспечивает просмотр списка и получение лог-файлов терминала")
@RestController("TerminalsRequestLog")
public class RequestTerminalLog {
    private TerminalLogMessages terminalLogMessages;

    @Autowired
    public RequestTerminalLog(TerminalLogMessages terminalLogMessages) {
        this.terminalLogMessages = terminalLogMessages;
    }

    /**
     * Получение списка логов терминала
     * @param terminal объект терминала
     * @return DeferredResult объект с результатом выполнения запроса
     * @throws ResponseStatusException
     */
    @ApiOperation(value = "Получение списка логов терминала")
    @Authority(value = Terminal.class, required = true)
    @GetMapping("terminals/log/list")
    public DeferredResult<Object> requestTerminalLogList(Terminal terminal) {
        if(terminal.getOnline()) {
            try {
                return terminalLogMessages.requestList(terminal);
            } catch (RepublisherException e) {
                log.error("error request list from terminal", e);
                throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }


    /**
     * Получение файла с логом терминала
     * @param terminal объект терминала
     * @param file название файла
     * @return DeferredResult объект с результатом выполнения запроса
     * @throws ResponseStatusException
     */
    @ApiOperation(value = "Получение лог-файла терминала")
    @Authority(value = Terminal.class, required = true)
    @GetMapping("terminals/log/file")
    public DeferredResult<Object> requestTerminalLogFile(Terminal terminal, @RequestParam("file") String file) {
        if(terminal.getOnline()) {
            try {
                return terminalLogMessages.requestFile(terminal, file);
            } catch (RepublisherException e) {
                log.error("error request file log from terminal", e);
                throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
            }
        }
        else {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}
