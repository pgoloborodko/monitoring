package com.landau.core.terminals.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.repos.TerminalRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * Сохранение терминала
 */
@Api(tags = "Сохранение терминала", description = "Контроллер может получать данные терминала, а также создавать новые")
@RestController("TerminalsSave")
@RequestMapping("/terminals/save")
public class TerminalSave extends Base {
    private TerminalRepo terminalRepo;

    public TerminalSave(TerminalRepo terminalRepo) {
        this.terminalRepo = terminalRepo;
    }

    /**
     * Получение данных терминала
     * @param terminal id терминала
     * @return ResourceNotFoundException если терминал не найден, либо объект терминала
     */
    @ApiOperation(value = "Получение данных терминала", response = Terminal.class)
    @Authority(value = Terminal.class, required = true)
    @GetMapping
    public Terminal get(Terminal terminal) {
        return terminal;
    }

    /**
     * Сохранение данных терминала
     * @param terminal объект терминала
     * @param result
     * @return false с ошибкой, либо true с объектом терминала
     */
    @ApiOperation(value = "Изменение терминала", response = Terminal.class)
    @Authority(value = Terminal.class, required = true)
    @PostMapping
    public Terminal post(Terminal terminal, BindingResult result) {
        if(result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getAllErrors().toString());
        }

        return terminalRepo.save(terminal);
    }
}
