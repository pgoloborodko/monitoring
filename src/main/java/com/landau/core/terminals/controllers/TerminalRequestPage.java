package com.landau.core.terminals.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.terminals.entities.Terminal;
import com.landau.core.terminals.messages.TerminalPageMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Удаленный доступ к терминалу
 */
@Data
@Api(tags = "Удаленный доступ к терминалу", description = "Контроллер обеспечивает удаленный доступ к терминалу")
@RestController("TerminalsRequest")
@RequestMapping("/terminals/request/{terminal}/**")
public class TerminalRequestPage {
    private TerminalPageMessages terminalPageMessages;

    @Autowired
    public TerminalRequestPage(TerminalPageMessages terminalPageMessages) {
        this.terminalPageMessages = terminalPageMessages;
    }

    /**
     * Получение удаленного доступа к терминалу
     * @param terminal объект терминала для доступа
     * @param request http-request
     * @return DeferredResult объект с результатом выполнения запроса
     * @throws ResponseStatusException
     */
    @ApiOperation(value = "Получение удаленного доступа к терминалу", response = Terminal.class)
    @Authority(value = Terminal.class, required = true)
    @RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
    public DeferredResult<Object> getPage(Terminal terminal, HttpServletRequest request) throws RepublisherException {
        if(terminal.getOnline()) {
            String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
            String bestMatchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);

            AntPathMatcher apm = new AntPathMatcher();
            String url = apm.extractPathWithinPattern(bestMatchPattern, path);

            if(url.equals("")) {
                url = "/";
            }

            url += "?" + request.getQueryString();

            return terminalPageMessages.requestPage(terminal, url, request);
        }
        else {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}
