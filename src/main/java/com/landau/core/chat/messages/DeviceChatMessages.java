package com.landau.core.chat.messages;

import com.lampa.republish.RepublisherException;
import com.lampa.republish.annotations.ReConsumer;
import com.lampa.republish.interfaces.IService;
import com.landau.core.chat.entities.ChatMessage;
import com.landau.core.chat.repos.ChatMessageRepo;
import com.landau.core.common.messages.CastService;
import com.landau.core.companies.entities.Company;
import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.users.entities.User;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.action.CreateChatMessage;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.List;

@Data
@Slf4j
@Service
public class DeviceChatMessages implements IService {
    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private ChatMessageRepo chatMessageRepo;

    @Autowired
    private CoreDeviceMessages messages;

    @Autowired
    private CastService castService;

    private HashMap<Long, DeferredResult<Object>> chatMessagesResults = new HashMap<>();

    /**
     * Метод отвечает за обработку принятого сообщения типа CreateChatMessage
     * @param messageToConsume
     */
    //TODO написать тест
    @ReConsumer(listen = CreateChatMessage.class)
    public void consumeCreateChatMessage(CreateChatMessage messageToConsume){
        log.info("consume message: {}", messageToConsume.getClass().getSimpleName());
        Device device = deviceRepo.findBySerial(messageToConsume.getSerial()).orElse(null);

        if (device != null){
            if (messageToConsume.getType().equals(CreateChatMessage.Type.AUDIO)){
                if (messageToConsume.getGroup() == 0 && device.getCompany() != null){
                    List<User> userList = castService.getUserRepo().findAllByCompany(device.getCompany());

                    for (User user: userList){
                        if (user.getDevice() != null && device.getUser() != null && !user.getId().equals(device.getUser().getId())){

                            messageToConsume.setSerial(user.getDevice().getSerial());

                            try {
                                createAndSendChatMessage(messageToConsume, device.getUser(), user);
                            } catch (RepublisherException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            log.warn("null value: {}", Device.class);
                        }
                    }
                }
                else if (messageToConsume.getGroup() == 0 && device.getCompany() == null){
                    log.warn("null value: {}", Company.class);
                }
            }
//            else {
//                try {
//                    createAndSendChatMessage(messageToConsume, device.getUser(), device.getUser());
//                } catch (RepublisherException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }


    /**
     * Метод создает и отправляет сообщение типа ChatMessage
     * @param messageToConsume
     * @param sender
     * @param destination
     * @throws RepublisherException
     */
    private void createAndSendChatMessage(CreateChatMessage messageToConsume, User sender, User destination) throws RepublisherException {
        ChatMessage entityChatMessage = castService.castToEntity(messageToConsume, sender, destination);
        chatMessageRepo.save(entityChatMessage);
        log.info("save message: {}", ChatMessage.class);

        com.landau.objects.data.ChatMessage messageToSend = castService.castToMessage(entityChatMessage);
        messages.send(messageToSend);
        log.info("send message: {}", com.landau.objects.data.ChatMessage.class);
    }
}
