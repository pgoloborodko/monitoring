package com.landau.core.chat.controllers;

import com.lampa.republish.RepublisherException;
import com.landau.core.chat.entities.ChatMessage;
import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.chat.filters.ChatMessageFilter;
import com.landau.core.chat.filters.ChatRoomFilter;
import com.landau.core.chat.messages.DeviceChatMessages;
import com.landau.core.chat.repos.ChatMessageRepo;
import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.chat.specs.ChatMessageSpec;
import com.landau.core.chat.specs.ChatRoomSpec;
import com.landau.core.common.controllers.Base;
import com.landau.core.common.messages.CastService;
import com.landau.core.users.entities.User;
import com.landau.core.users.repos.UserRepo;
import com.landau.messages.core.CoreBrowserMessages;
import com.landau.messages.core.CoreDeviceMessages;
import com.landau.objects.events.core.ChatGroupEvent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.*;
import java.util.List;
import java.util.Optional;

@Api(tags = "Чат с устройством", description = "Контроллер обеспечивает чат для общения с устройством")
@Data
@Slf4j
@RestController("ChatController")
@RequestMapping("/chat")
public class ChatController extends Base {
    private DeviceChatMessages deviceChatMessages;
    private CastService castService;
    private CoreDeviceMessages coreDeviceMessages;
    private CoreBrowserMessages coreBrowserMessages;
    private ChatRoomRepo chatRoomRepo;
    private ChatMessageRepo chatMessageRepo;
    private UserRepo userRepo;

    @Autowired
    public ChatController(DeviceChatMessages deviceChatMessages, CastService castService, CoreDeviceMessages coreDeviceMessages, CoreBrowserMessages coreBrowserMessages, ChatRoomRepo chatRoomRepo, ChatMessageRepo chatMessageRepo, UserRepo userRepo) {
        this.deviceChatMessages = deviceChatMessages;
        this.castService = castService;
        this.coreDeviceMessages = coreDeviceMessages;
        this.coreBrowserMessages = coreBrowserMessages;
        this.chatRoomRepo = chatRoomRepo;
        this.chatMessageRepo = chatMessageRepo;
        this.userRepo = userRepo;
    }


    /**
     * Получение списка чат-комнат
     * @param chatRoomFilter
     * @return
     */
    @ApiOperation(value = "Получение списка чат-комнат", response = ChatRoom.class)
    @GetMapping("/groups")
    public List<ChatRoom> getChats(ChatRoomFilter chatRoomFilter){
        return chatRoomRepo.findAll(ChatRoomSpec.find(chatRoomFilter), Sort.by(Sort.Direction.DESC, "lastMessage"));
    }

    /**
     * Получение списка сообщений внутри чат-комнаты
     * @param chatMessageFilter
     * @return
     */
    @ApiOperation(value = "Получение списка сообщений внутри чат-комнаты", response = ChatMessage.class)
    @GetMapping("/messages")
    public Page<ChatMessage> getMessages(ChatMessageFilter chatMessageFilter, Pageable pageable) {
        return chatMessageRepo.findAll(ChatMessageSpec.find(chatMessageFilter), pageable);
    }

    /**
     * Создание чат-комнаты
     * @param chatRoom
     * @return
     */
    @ApiOperation(value = "Создание чат-комнаты", response = ChatRoom.class)
    @PostMapping("/groups/save")
    public ChatRoom createChatRoom(ChatRoom chatRoom){
        if (getUser().getCompany() != null){
            for (User member : chatRoom.getMembers()){
                if (member.getId() != null){
                    Optional<User> user = userRepo.findById(member.getId());
                    if (user.isPresent()){
                        chatRoom.getMembers().remove(member);
                        member = user.get();
                        chatRoom.getMembers().add(member);
                    }
                }
                else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "участника не существует");
                }
                if (!member.getCompany().equals(getUser().getCompany())){
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "компания участника чата не соответствует компании администратора");
                }
            }

        }
        else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "500");
        }


        ChatRoom chatRoomEntity = chatRoomRepo.save(chatRoom);
        ChatGroupEvent chatGroupEvent = castService.castToEvent(chatRoomEntity);

        for (User member : chatRoom.getMembers()){
            try {
                coreDeviceMessages.send(member.getDevice().getSerial(), chatGroupEvent);
                coreBrowserMessages.send(member.getDevice().getSerial(), chatGroupEvent);
            } catch (RepublisherException e) {
                e.printStackTrace();
            }
        }

        return chatRoomEntity;
    }

    /**
     * Создание сообщения
     * @param chatMessage
     * @return
     */
    @ApiOperation(value = "Создание сообщения", response = ChatMessage.class)
    @PostMapping("/messages/save")
    public ChatMessage createMessage(ChatMessage entityChatMessage) {
        //chatMessage.getChatRoom() == nul
      /*  if (!chatMessage.getChatRoom().getMembers().contains(chatMessage.getSender())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "wrong chatGroup");
        }*/
        entityChatMessage.setSender(getUser());
        if (entityChatMessage.getDestination() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "empty destination");
        }

        try {
            entityChatMessage = chatMessageRepo.save(entityChatMessage);
            com.landau.objects.data.ChatMessage messageToSend = castService.castToMessage(entityChatMessage);


            coreDeviceMessages.send(messageToSend);
            //coreBrowserMessages.send(chatMessage.getSender().getDevice().getSerial(), chatMessageEvent);
        } catch (RepublisherException e) {
            log.error("error send message", e);
        }

        return entityChatMessage;
    }
}
