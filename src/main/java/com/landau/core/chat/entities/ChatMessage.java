package com.landau.core.chat.entities;

import com.landau.core.chat.repos.ChatMessageRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractId;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

/**
 * Сообщение чата
 */
@Data
@Entity
@Table
@ApiModel(description = "сообщение чата")
@Repo(ChatMessageRepo.class)
public class ChatMessage extends AbstractId {
    public enum Type{
        TEXT,
        AUDIO,
        PHOTO,
        VIDEO
    }

    @Column
    @ApiModelProperty(value = "тип сообщения")
    private Type type;

    @ManyToOne
    @ApiModelProperty(value = "чат-комната сообщения")
    private ChatRoom chatRoom;

    @Column
    @ApiModelProperty(value = "тело сообщения")
    private Byte[] body;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(value = "дата-время сообщения")
    private Date created;

    @ManyToOne
    @ApiModelProperty(value = "создатель сообщения")
    private User sender;

    @ManyToOne
    @ApiModelProperty(value = "адресат сообщения")
    private User destination;

    public void setBodyEncode(String bodyEncode) {
        this.setBody(ArrayUtils.toObject(Base64.getEncoder().encode(bodyEncode.getBytes(StandardCharsets.UTF_8))));
    }

    public void setBody64(String body) throws UnsupportedEncodingException {
        byte[] decodedString = Base64.getDecoder().decode(body.getBytes("UTF-8"));
        this.setBody(ArrayUtils.toObject(decodedString));
    }
}
