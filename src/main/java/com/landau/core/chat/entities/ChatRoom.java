package com.landau.core.chat.entities;

import com.landau.core.chat.repos.ChatRoomRepo;
import com.landau.core.common.aspects.authority.Repo;
import com.landau.core.common.entities.AbstractBusiness;
import com.landau.core.users.entities.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table
@ApiModel(description = "Чат-комната")
@Repo(ChatRoomRepo.class)
public class ChatRoom extends AbstractBusiness {

    @ManyToOne
    @ApiModelProperty(value = "администратор чата")
    private User admin;

    @OneToMany
    @ApiModelProperty(value = "список участников")
    private List<User> members;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(value = "дата последнего сообщения")
    private Date lastMessage;
}
