package com.landau.core.chat.repos;

import com.landau.core.chat.entities.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ChatMessageRepo extends JpaRepository<ChatMessage, Long>, JpaSpecificationExecutor<ChatMessage> {
}
