package com.landau.core.chat.repos;

import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

public interface ChatRoomRepo extends JpaRepository<ChatRoom, Long>, JpaSpecificationExecutor<ChatRoom> {
    List<ChatRoom> findAllByLastMessageBeforeAndAdminOrderByLastMessageDesc(Date date, User user);
    List<ChatRoom> findAllByLastMessageBeforeOrderByLastMessageDesc(Date date);
}
