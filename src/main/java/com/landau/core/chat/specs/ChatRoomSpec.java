package com.landau.core.chat.specs;

import com.landau.core.chat.entities.ChatRoom;
import com.landau.core.chat.filters.ChatRoomFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class ChatRoomSpec {
    public static Specification<ChatRoom> find(ChatRoomFilter chatRoomFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (chatRoomFilter.getLastMessage() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(chatRoomFilter.getLastMessage());

                predicates.add(cb.lessThanOrEqualTo(root.get("lastMessage"), calendar.getTime()));
            }

            if (chatRoomFilter.getCompany() != null){
                predicates.add(cb.equal(root.get("company"), chatRoomFilter.getCompany()));
            }

            if (chatRoomFilter.getAdmin() != null){
                predicates.add(cb.equal(root.get("admin"), chatRoomFilter.getAdmin()));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
