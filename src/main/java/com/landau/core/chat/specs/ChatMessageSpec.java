package com.landau.core.chat.specs;

import com.landau.core.chat.entities.ChatMessage;
import com.landau.core.chat.filters.ChatMessageFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class ChatMessageSpec {
    public static Specification<ChatMessage> find(ChatMessageFilter chatMessageFilter) {
        return (root, query, cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (chatMessageFilter.getCreated() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(chatMessageFilter.getCreated());

                predicates.add(cb.lessThanOrEqualTo(root.get("created"), calendar.getTime()));
            }

            if (chatMessageFilter.getChatRoom() != null){
                predicates.add(cb.equal(root.get("chatRoom"), chatMessageFilter.getChatRoom()));
            }

            if (chatMessageFilter.getSender() != null){
                predicates.add(cb.equal(root.get("user"), chatMessageFilter.getSender()));
            }
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
