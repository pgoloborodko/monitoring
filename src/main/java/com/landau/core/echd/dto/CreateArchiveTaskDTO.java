package com.landau.core.echd.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.UUID;

@Data
@ApiModel(description = "DTO-контейнер данных для отвта на запрос CreateArchiveTask")
public class CreateArchiveTaskDTO {
    @ApiModelProperty(value = "идентификатор камеры", example = "1")
    private String cameraId;

    @ApiModelProperty(value = "дата и время начала архива")
    @DateTimeFormat(pattern = "YYYY-MM-DDThh:mm:ssZ")
    private Date from;

    @ApiModelProperty(value = "дата и время окончания архива")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date to;

    @ApiModelProperty(value = "идентификатор созданного задания", example = "1250b5ca-3e74-40cb-9f54-374dfd5050e5")
    private UUID archiveTaskId;

    @ApiModelProperty(value = "сообщение с информацией об ошибке", example = "Exception")
    private String errorMessage;

    @ApiModelProperty(value = "статус результата выполнения задачи", example = "Created")
    private String state;
}
