package com.landau.core.echd.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ApiModel(description = "тело запроса CreateArchiveTask")
public class CreateArchiveTaskRequestBody {
    @ApiModelProperty(value = "идентификатор камеры", example = "1")
    private String cameraId;

    @ApiModelProperty(value = "дата и время начала архива")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date from;

    @ApiModelProperty(value = "дата и время окончания архива")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date to;
}
