package com.landau.core.echd.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.UUID;

@Data
@ApiModel(description = "DTO-контейнер данных для отвта на запрос GetArchiveTaskStatus")
public class RemoveArchiveDTO {
    @ApiModelProperty(value = "идентификатор созданного задания", example = "1250b5ca-3e74-40cb-9f54-374dfd5050e5")
    private UUID archiveTaskId;

    @ApiModelProperty(value = "сообщение с информацией об ошибке", example = "Exception")
    private String errorMessage;

    @ApiModelProperty(value = "признак успешного удаления архива ", example = "true")
    private Boolean success;
}
