package com.landau.core.echd.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "DTO-контейнер данных для передачи списка промежутков архивных записей для устройства")
public class ArchiveRangesDTO {
    @ApiModelProperty(value = "идентификатор устройства")
    Long cameraId;

    @ApiModelProperty(value = "Список временных промежутков продолжительности архивных записей устройства")
    List<Range> ranges;

    @Data
    @ApiModel(description = "Объект временного промежутка продолжыительности архивной записи")
    public static class Range{
        @ApiModelProperty(value = "начало записи")
        Long from;

        @ApiModelProperty(value = "конец записи")
        Long to;
    }
}
