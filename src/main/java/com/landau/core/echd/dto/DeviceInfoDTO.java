package com.landau.core.echd.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DTO-контейнер данных устройства")
public class DeviceInfoDTO {
    @ApiModelProperty(value = "Версия прошивки")
    String firmware_version;

    @ApiModelProperty(value = "Вендор")
    String vendor;

    @ApiModelProperty(value = "Модель устройства")
    String model;

    @ApiModelProperty(value = "Серийный номер устройства")
    String serial_number;

    @ApiModelProperty(value = "Статус PTZ")
    String ptz_status;
}
