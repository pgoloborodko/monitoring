package com.landau.core.echd.services;

import com.landau.core.devices.entities.Device;
import com.landau.core.devices.repos.DeviceRepo;
import com.landau.core.echd.dto.*;
import com.landau.core.echd.entities.ArchiveTask;
import com.landau.core.echd.repos.ArchiveTaskRepo;
import com.landau.core.media.entities.MediaFile;
import com.landau.core.media.repos.MediaFileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ECHDService {
    @Value("${server.address}")
    private String host;

    @Value("${server.port}")
    private String port;

    @Autowired
    private DeviceRepo deviceRepo;

    @Autowired
    private MediaFileRepo mediaFileRepo;

    @Autowired
    private ArchiveTaskRepo archiveTaskRepo;

    /**
     * Метод заполняет DTO объект данных для передачи ответа на запрос от ецхд getDeviceInfo()
     * @param id идентификатор устройства, из которого получаем данные
     * @return DeviceInfoDTO
     */
    public DeviceInfoDTO fillDeviceInfoDTO(Long id){
        Device device = deviceRepo.findById(id).orElse(null);

        if (device != null){
            DeviceInfoDTO deviceInfoDTO = new DeviceInfoDTO();
            deviceInfoDTO.setFirmware_version(device.getVersion());
            deviceInfoDTO.setModel(device.getModel().getModel());
            deviceInfoDTO.setSerial_number(device.getSerial());
            deviceInfoDTO.setVendor(device.getModel().getManufacturer());
            deviceInfoDTO.setPtz_status("Not Supported");

            return deviceInfoDTO;
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
    }


    /**
     * Находит список файлов для устройства, идентификатор которого передали по запросу getArchiveRanges()
     * Формирует из промежутки из данных каждого файла
     * Из полученных промежутков формирует ответ для ецхд
     * @param id идентификатор устройства, из которого получаем данные
     * @return ArchiveRangesDTO
     */
    public ArchiveRangesDTO fillArchiveRangesDTO(Long id){
        Device device = deviceRepo.findById(id).orElse(null);

        if (device != null){
            List<MediaFile> files = mediaFileRepo.findAllByDevice(device);
            if (files != null && !files.isEmpty()){
                ArchiveRangesDTO archiveRangesDTO = new ArchiveRangesDTO();
                archiveRangesDTO.setRanges(new ArrayList<>());

                for (MediaFile file : files){
                    ArchiveRangesDTO.Range range = new ArchiveRangesDTO.Range();

                    range.setFrom(file.getCreated().getTime());
                    range.setTo((long) (file.getCreated().getTime() + file.getDuration() * 1000));
                    archiveRangesDTO.getRanges().add(range);
                }

                return archiveRangesDTO;
            }
            else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "404");

        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
    }

    /**
     * Метод создает сущность ArchiveTask(задание на выгрузку архива), заполняет ее поля и сохраняет в БД
     * Формирует ответ ЕЦХД
     * @param requestBody тело запроса
     * @return CreateArchiveTaskDTO
     */
    public CreateArchiveTaskDTO fillCreateArchiveTaskDTO(CreateArchiveTaskRequestBody requestBody) {
        Device device = deviceRepo.findById(Long.parseLong(requestBody.getCameraId())).orElse(null);

        if (device != null){
            ArchiveTask archiveTask = new ArchiveTask();
            CreateArchiveTaskDTO createArchiveTaskDTO = new CreateArchiveTaskDTO();
            archiveTask.setDevice(device);
            archiveTask.setFrom(requestBody.getFrom());
            archiveTask.setTo(requestBody.getTo());
            archiveTask.setState(ArchiveTask.State.Created);
            archiveTaskRepo.save(archiveTask);

            createArchiveTaskDTO.setCameraId(requestBody.getCameraId());
            createArchiveTaskDTO.setFrom(requestBody.getFrom());
            createArchiveTaskDTO.setTo(requestBody.getTo());
            createArchiveTaskDTO.setArchiveTaskId(archiveTask.getArchiveTaskId());
            createArchiveTaskDTO.setErrorMessage(archiveTask.getErrorMessage());
            createArchiveTaskDTO.setState(archiveTask.getState().name());

            prepareArchive(archiveTask);

            return createArchiveTaskDTO;
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
    }

    /**
     * Готовит файл архива
     * @param archiveTask
     */
    public void prepareArchive(ArchiveTask archiveTask){
        //TODO заглушка
        File dir = new File(getClass().getClassLoader().getResource("static/common").getFile());
        File[] files = dir.listFiles();
        if (files != null && files.length != 0){
            String filename = "";

            for (File file : files){
                if (file.getName().contains(".mp4")){
                    filename = file.getName();
                    return;
                }
            }
            archiveTask.setUrl("https://" + host + ":" + port + "/common/" + filename);
            archiveTask.setPercents((byte)100);
            archiveTask.setState(ArchiveTask.State.ReadyForDownload);
            archiveTaskRepo.save(archiveTask);
        }
    }

    /**
     * Метод формирует ответ на запрос от ЕЦХД вида DownloadArchiveFile
     * @param id идентификатор устройсва
     * @param from дата и время начала архива
     * @param to дата и время окончания архива
     * @return ResponseEntity с файлом архива
     */
    public ResponseEntity<InputStreamResource> prepareArchive(Long id, Date from, Date to) throws FileNotFoundException {
        //TODO заглушка
        InputStreamResource in = null;
        File dir = new File(getClass().getClassLoader().getResource("static/common").getFile());
        File[] files = dir.listFiles();
        File archive = null;
        if (files != null && files.length != 0){
            for (File file : files){
                if (file.getName().contains(".mp4")){
                    in = new InputStreamResource(new FileInputStream(file.getAbsolutePath()));
                    archive = file;
                    break;
                }
            }
        }
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + archive.getName())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(archive.length())
                .body(in);
    }



    /**
     * Метод формирует контейнер данных GetArchiveTaskStatusDTO из сущности ArchiveTask,
     * которую получает из БД по переданному UUID задачи на выгрузку архива.
     * Полученный DTO отправляется в качестве ответа ЕЦХД
     * @param archiveTaskId UUID задачи на выгрузку архива
     * @return CreateArchiveTaskDTO
     */
    public GetArchiveTaskStatusDTO fillCreateArchiveTaskDTO(UUID archiveTaskId) {
        ArchiveTask archiveTask = archiveTaskRepo.findByArchiveTaskId(archiveTaskId).orElse(null);

        if (archiveTask != null){
            GetArchiveTaskStatusDTO getArchiveTaskStatusDTO = new GetArchiveTaskStatusDTO();

            getArchiveTaskStatusDTO.setPercents(archiveTask.getPercents().toString());
            getArchiveTaskStatusDTO.setUrl(archiveTask.getUrl());
            getArchiveTaskStatusDTO.setCameraId(archiveTask.getDevice().getId().toString());
            getArchiveTaskStatusDTO.setFrom(archiveTask.getFrom());
            getArchiveTaskStatusDTO.setTo(archiveTask.getTo());
            getArchiveTaskStatusDTO.setArchiveTaskId(archiveTask.getArchiveTaskId());
            getArchiveTaskStatusDTO.setErrorMessage(archiveTask.getErrorMessage());
            getArchiveTaskStatusDTO.setState(archiveTask.getState().name());

            return getArchiveTaskStatusDTO;
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
    }

    /**
     * Метод формирует контейнер данных GetArchiveTaskStatusDTO из сущности ArchiveTask,
     * которую получает из БД по переданному UUID задачи на выгрузку архива.
     * Полученный DTO отправляется в качестве ответа ЕЦХД
     * @param archiveTaskId UUID задачи на выгрузку архива
     * @return CreateArchiveTaskDTO
     */
    public RemoveArchiveDTO fillRemoveArchiveDTO(UUID archiveTaskId) {
        ArchiveTask archiveTask = archiveTaskRepo.findByArchiveTaskId(archiveTaskId).orElse(null);

        if (archiveTask != null){
            RemoveArchiveDTO archiveTaskStatusDTO = new RemoveArchiveDTO();

            archiveTaskStatusDTO.setArchiveTaskId(archiveTask.getArchiveTaskId());
            archiveTaskStatusDTO.setErrorMessage(archiveTask.getErrorMessage());
            boolean success = false;
            try {
                success = deleteArchive(archiveTask.getUrl());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (success){
                archiveTaskStatusDTO.setSuccess(true);
                archiveTaskRepo.delete(archiveTask);
            }
            else {
                archiveTaskStatusDTO.setSuccess(false);
                archiveTaskStatusDTO.setErrorMessage("error");
            }

            return archiveTaskStatusDTO;
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "400");
    }

    /**
     * Удаляет архив
     * @param url ссылка на архив
     */
    public Boolean deleteArchive(String url) throws IOException {
        //TODO заглушка
        File dir = new File(getClass().getClassLoader().getResource("static/common").getFile());
        File[] files = dir.listFiles();
        String[] pieces = url.split("/");

        if (files != null && files.length != 0){
            String filename = pieces[pieces.length - 1];

            for (File file : files){
                if (file.getName().equals(filename)){
                    file.delete();
                    if (file.exists()){
                        file.getCanonicalFile().delete();
                    }
                }
            }
        }
        return false;
    }

}
