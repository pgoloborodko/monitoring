package com.landau.core.echd.repos;

import com.landau.core.echd.entities.ArchiveTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ArchiveTaskRepo extends JpaRepository<ArchiveTask, Long> {
    Optional<ArchiveTask> findByArchiveTaskId(UUID archiveTaskId);
}
