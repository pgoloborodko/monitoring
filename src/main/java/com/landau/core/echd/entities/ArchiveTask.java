package com.landau.core.echd.entities;

import com.landau.core.common.entities.AbstractId;
import com.landau.core.devices.entities.Device;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;


@Data
@Entity
@Table
@ApiModel(description = "")
public class ArchiveTask extends AbstractId {
    public enum State{
        Created,
        Error,
        Working,
        ReadyForDownload
    }

    @ManyToOne
    @ApiModelProperty(value = "Устройство")
    private Device device;

    @Column(name = "startArchive")
    @ApiModelProperty(value = "дата и время начала архива")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date from;

    @Column(name = "endArchive")
    @ApiModelProperty(value = "дата и время окончания архива")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date to;

//    @Column
//
//    @org.hibernate.annotations.Type(type="pg-uuid")
    @ApiModelProperty(value = "идентификатор созданного задания на выгрузку архива")
    @Column(columnDefinition = "uuid", updatable = false)
    private UUID archiveTaskId = UUID.randomUUID();

    @Column
    @ApiModelProperty(value = "статус результата выполнения задачи")
    private State state;

    @Column(columnDefinition = "TEXT")
    @ApiModelProperty(value = "сообщение с информацией об ошибке.")
    private String errorMessage;

    @Column
    @ApiModelProperty(value = "процент выполнения задания на выгрузку архива")
    private Byte percents;

    @Column(columnDefinition = "TEXT")
    @ApiModelProperty(value = "ссылка на сформированный архив")
    private String url;
}
