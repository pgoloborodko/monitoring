package com.landau.core.echd.controllers;

import com.landau.core.common.controllers.Base;
import com.landau.core.echd.dto.*;
import com.landau.core.echd.services.ECHDService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.UUID;

@Api(tags = "ЕЦХД АПИ ", description = "Контроллер обеспечивает выполнение мтодовов взаимодействия с ЕЦХД")
@RestController("ECHDController")
@RequestMapping("/echd")
public class ECHDController extends Base {
    @Autowired
    private ECHDService echdService;

    @GetMapping("/getdeviceinfo")
    @ApiOperation(value = "Изменение данных регистратора", response = DeviceInfoDTO.class)
    public DeviceInfoDTO getDeviceInfo(@RequestParam Long id){
        return echdService.fillDeviceInfoDTO(id);
    }

    @GetMapping("/getarchiveranges")
    @ApiOperation(value = "Диапазоны доступных архивных записей", response = ArchiveRangesDTO.class)
    public ArchiveRangesDTO getArchiveRanges(@RequestParam Long id){
        return echdService.fillArchiveRangesDTO(id);
    }

    @PostMapping(value = "/createarchivetask", consumes = "application/json", produces = "application/json")
    @ApiOperation(value = "Создание задания на выгрузку архива", response = CreateArchiveTaskDTO.class)
    public CreateArchiveTaskDTO createArchiveTask(@RequestBody CreateArchiveTaskRequestBody requestBody){
        return echdService.fillCreateArchiveTaskDTO(requestBody);
    }

    @GetMapping("/getarchivetaskstatus")
    @ApiOperation(value = "Получение статуса готовности задания на выгрузку архива", response = GetArchiveTaskStatusDTO.class)
    public GetArchiveTaskStatusDTO getArchiveTaskStatus(@RequestParam UUID archivetaskid){
        return echdService.fillCreateArchiveTaskDTO(archivetaskid);
    }

    @DeleteMapping("/removearchive")
    @ApiOperation(value = "Удаление архива", response = GetArchiveTaskStatusDTO.class)
    public RemoveArchiveDTO removeArchive(@RequestParam UUID archivetaskid){
        return echdService.fillRemoveArchiveDTO(archivetaskid);
    }

    @GetMapping(value = "/downloadarchivefile", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ApiOperation(value = "Выгрузка архивов")
    public ResponseEntity<InputStreamResource> downloadArchiveFile(@RequestParam Long id,
                                                                   @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date from,
                                                                   @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date to) {
        try {
            return echdService.prepareArchive(id, from, to);
        } catch (FileNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "404");
        }
    }
}
