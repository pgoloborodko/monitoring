package com.landau.core.echd.controllers;

import com.landau.core.common.aspects.authority.Authority;
import com.landau.core.common.controllers.Base;
import com.landau.core.locations.entities.Location;
import com.landau.core.locations.repos.LocationRepo;
import com.landau.core.users.entities.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Получение списка локаций регистраторов
 */
@Api(tags = "Список локаций пользователя для ецхд", description = "Контроллер обеспечивает получение списка локаций пользователя для ецхд")
@RestController
@RequestMapping("/echd")
public class LocationUserECHD extends Base {
    @Autowired
    private LocationRepo locationRepo;

    /**
     * Получение локаций регистратора по дате за 1 день
     *
     * @param user пользователь
     * @param date дата вида yyyy/MM/dd
     * @return List<{@link Location}>
     */
    private List<Location> date(User user, Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        Date end = c.getTime();

        List<Object[]> points = locationRepo.findByCreatedDateBetweenAndUser(date, end, user);
        List track = new ArrayList();

        for (Object[] point: points) {
            Location l = new Location();
            l.setId(((BigInteger)point[0]).longValue());
            l.setLatitude((Double)point[1]);
            l.setLongitude((Double)point[2]);
            l.setCreated((Date)point[3]);
            l.setSpeed((Short) point[4]);
            l.setBearing((Float) point[5]);
            track.add(l);
        }

        return track;
    }


    @ApiOperation(value = "Получение локаций пользователя по дате за 1 день для ЕЦХД")
    @Authority(value = User.class, required = true)
    @GetMapping("date")
    public String toSDC(@RequestParam(value = "user") User user, @RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        List<Location> locations = this.date(user, date);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("HHmmss.000");

        StringBuilder builder = new StringBuilder();

        for(Location location : locations) {
            builder.append("imei:" + location.getDevice().getSerial() +"," +
                    "tracker," + dateFormat.format(location.getCreated()) +",00000000000,F," +
                    dateFormat2.format(location.getCreated()) + "," +
                    (location.getType() == Location.Type.GSM ? "V" : "A") + "," +
                    location.getLatitude() + ",N," +
                    location.getLongitude() + ",E," +
                    location.getSpeed() + "," +
                    location.getBearing() + ";\n");
        }

        return builder.toString();
    }
}
