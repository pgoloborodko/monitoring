function test(view, cb, data, cmd) {
    fetchStyle("/extjs/modern/theme-material/resources/theme-material-all-debug.css");
    fetchStyle("/css/bootstrap-grid.css");
    fetchStyle("/css/ext.css?v=2");

    cb = cb || function () { };
    data = data || {};
    cmd = cmd || false;

    data.width = '100%';
    data.height = '100%';

    fetchScript("/extjs/ext-modern-all-debug.js?v=2").then(function () {
        let promises = [];

        promises.push(fetchScript("/extjs/modern/theme-material/theme-material-debug.js"));
        promises.push(fetchScript("/app/locale/lang.js"));

        if(cmd) {
            promises.push(fetchScript("/extjs/packages/charts/modern/charts-debug.js"));
            promises.push(fetchScript("/extjs/cmd.js"));
            promises.push(fetchScript("/extjs/exporter.js"));
        }

        Promise.all(promises).then(function () {
            Ext.application({
                appFolder: '/app',
                requires: [
                    "Landau.common.ViewController",
                    "Landau.common.FormPanel",
                    "Landau.common.Component",
                    "Landau.common.Messages",
                    'Landau.common.Sorter',
                    'Landau.common.ProxyServer',
                    'Landau.common.Radiocolumn'
                ],
                name: 'Landau',
                launch: function() {
                    console.log(data);

                    let item = Ext.create(view, data);

                    Ext.Viewport.add({ xtype: 'container', padding: 0, items: [item] });

                    cb(item);
                }
            });
        });
    });
}




let fetchStyle = function(url) {
    let element = document.createElement("link");
    element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", url);
    document.getElementsByTagName("head")[0].appendChild(element)
};

let fetchScript = function(src) {
    return new Promise(function(resolve, reject) {
        let script = document.createElement('script');
        script.src = src;

        script.onload = () => resolve(script);
        script.onerror = () => reject(new Error(`Ошибка загрузки скрипта ${src}`));

        document.head.append(script);
    });
};


function form(id, cb) {
    let form = document.getElementById(id);

    let params = new URLSearchParams(location.hash.substring(1));
    for(let pair of params.entries()) {
        form.elements[pair[0]].value = pair[1];
    }

    form.addEventListener("submit", function (e) {
        e.preventDefault();
        submitForm();
    }, false);

    function submitForm(e) {
        let formData = new FormData(form);
        let data = {};

        for(let pair of formData.entries()) {
            data[pair[0]] = pair[1];
        }

        location.hash = new URLSearchParams(formData).toString();
        cb(data);
    }

    setTimeout(function () {
        if(Array.from(params).length > 0) {
            submitForm();
        }
    }, 100);
}