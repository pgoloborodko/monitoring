var MetroMap = function(id) {
	this.isReady = false;
	this.callbackClick = function() {};
	this.markerStationWrapper = null;
    this.metroWrapper = null;
    this.markers = [];

    if (typeof id === 'string') {
        return this._init(document.getElementById(id));
    }
    else {
        return this._init(id);
	}
}

var regCount = {
	'station_088': 3,
	'station_089': 2,
	'station_090': 5
}


MetroMap.prototype._init = function(wrapper) {

	var link = document.createElement('link');
	link.href = '/js/metro-map/styles.css';
	link.type = 'text/css';
	link.rel = 'stylesheet';
	document.getElementsByTagName('head')[0].appendChild(link);
  
	
	var stationWrapper = document.createElement("div");
	wrapper.appendChild(stationWrapper);
	stationWrapper.classList.add('station-wrapper');
	
	stationWrapper.addEventListener("click", function(ev) {
		if(ev.target === stationWrapper) {
			stationWrapper.classList.remove('active');
			this.markerStationWrapper.classList.remove('active');

			this.removeMarkers();
		}
	}.bind(this), false);
		
	this.markerStationWrapper = document.createElement("div");
	wrapper.appendChild(this.markerStationWrapper);
	this.markerStationWrapper.classList.add('marker-station-wrapper');

	var ajax = new XMLHttpRequest();
	ajax.open("GET", "/js/metro-map/metro-map.svg", true);
	ajax.send();
	ajax.onload = function(e) {
		this.metroWrapper = document.createElement("div");
		wrapper.appendChild(this.metroWrapper);
		this.metroWrapper.classList.add('metro-wrapper');


		this.metroWrapper.innerHTML = ajax.responseText;

		Object.keys(regCount).map(item => {
			document.getElementById(item).innerHTML = regCount[item];
		});

		function ex(event) {
			var target = event.target;

			if(target.getAttribute("data-station") !== undefined) {
				this.callbackClick(target.getAttribute("data-station"));
				stationWrapper.classList.add('active');
				this.markerStationWrapper.classList.add('active');

				var ajax = new XMLHttpRequest();
				ajax.open("GET", "/js/metro-map/stations/" + target.getAttribute("data-station") + ".svg", true);
				ajax.send();
				ajax.onload = function(e) {
					stationWrapper.innerHTML = ajax.responseText;

					this.readyCb.apply(this);
					isReady = true;
				}.bind(this)
			}
		}

		this.metroWrapper.querySelectorAll("use").forEach(function(element) {
			element.addEventListener("click", ex.bind(this), false);
		}.bind(this));

		this.metroWrapper.querySelectorAll("#stname text").forEach(function(element) {
			element.addEventListener("click", ex.bind(this), false);
		}.bind(this));
	}.bind(this);
}

MetroMap.prototype.ready = function(callback) {
    if(this.isReady) {
        callback.apply(this);
    }

	this.readyCb = callback;
}

MetroMap.prototype.setMarkerClick = function(callback) {
	this.callbackClick = callback.bind(this);
}

MetroMap.prototype.addMarker = function(mac, name, image) {
	var marker = document.createElement("div");
	this.markerStationWrapper.appendChild(marker);
	marker.classList.add('marker-wrapper');
	
	marker.innerHTML = "<img src='" + image + "'><div class='map-marker'>" + name + "</div>";
	
	var pos = MetroMap.find(mac);
			
	if(pos != null) {
		marker.style.cssText = "top: " + pos.y + "px; left: " + pos.x + "px";
	}
	else {
		marker.style.cssText = "top: 0; left: 0;";
	}

    this.markers.push(marker);

	return {
		setMac: function(mac) {
			var pos = MetroMap.find(mac);
			
			if(pos != null) {
				marker.style.cssText = "top: " + pos.y + "px; left: " + pos.x + "px";
			}
			else {
				marker.style.cssText = "top: 0; left: 0;";
			}
		},
		remove: function() {
			this.markerStationWrapper.removeChild(marker);
		}.bind(this),
		setClick: function(cb) {
			marker.addEventListener("click", function() {
				cb();
			}.bind(this));
		},
        setIcon: function (image) {
            marker.innerHTML = "<img src='" + image + "'><div class='map-marker'>" + name + "</div>";
        },
        setText: function (text) {
            name = text;
        }
	}
}

MetroMap.prototype.removeMarkers = function() {
    for(var i=0 ; i < this.markers.length; i++) {
        this.markers[i].remove();
    }

    this.markers = [];
}

MetroMap.prototype.updateStationCount = function(station, count) {

}

MetroMap.prototype.getStationCount = function(station) {
    var station = this.metroWrapper.querySelectorAll("[data-station='" +station +"']")[0];
    console.log(station);



}

MetroMap.station = {};
MetroMap.station["089"] = {
    "58:69:6c:e7:fe:e0": {x: 175,y: 130},
    "58:69:6c:e7:fe:c7": {x: 550,y: 170},
    "58:69:6c:e7:ff:62": {x: 730,y: 180},
    "58:69:6c:e7:ff:17": {x: 110,y: 100},
    "64:6e:ea:c2:16:89": {x: 210,y: 200},


    "58:69:6c:e7:c7:e5": {x: 290,y: 150}, //
    "06:69:6c:e7:c7:e8": {x: 290,y: 150},
    "0a:69:6c:e7:c7:e8": {x: 290,y: 150},
    "12:69:6c:e7:c7:e8": {x: 290,y: 150},
    "0e:69:6c:e7:c7:e9": {x: 290,y: 150},
    "12:69:6c:e7:c7:e9": {x: 290,y: 150},


    "58:69:6c:e7:c8:d0": {x: 775,y: 90}, //
    "06:69:6c:e7:c8d3": {x: 775,y: 90},
    "0a:69:6c:e7:c8d3": {x: 775,y: 90},
    "12:69:6c:e7:c8d3": {x: 775,y: 90},
    "0e:69:6c:e7:c8d4": {x: 775,y: 90},
    "12:69:6c:e7:c8d4": {x: 775,y: 90},

    "58:69:6c:e7:fd:aa": {x: 350,y: 100}, //
    "06:69:6c:e7:fd:ad": {x: 350,y: 100},
    "0a:69:6c:e7:fd:ad": {x: 350,y: 100},
    "12:69:6c:e7:fd:ad": {x: 350,y: 100},
    "0e:69:6c:e7:fd:ae": {x: 350,y: 100},
    "12:69:6c:e7:fd:ae": {x: 350,y: 100},

    "58:69:6c:e7:fd:d2": {x: 615,y: 130}, //
    "06:69:6c:e7:fd:d5": {x: 615,y: 130},
    "0a:69:6c:e7:fd:d5": {x: 615,y: 130},
    "12:69:6c:e7:fd:d5": {x: 615,y: 130},
    "0e:69:6c:e7:fd:d6": {x: 615,y: 130},
    "12:69:6c:e7:fd:d6": {x: 615,y: 130},


    "58:69:6c:e7:fe:27": {x: 430,y: 170}, //
    "06:69:6c:e7:fe:2a": {x: 430,y: 170},
    "0a:69:6c:e7:fe:2a": {x: 430,y: 170},
    "12:69:6c:e7:fe:2a": {x: 430,y: 170},
    "0e:69:6c:e7:fe:2b": {x: 430,y: 170},
    "12:69:6c:e7:fe:2b": {x: 430,y: 170},


    "58:69:6c:e7:fe:4f": {x: 45,y: 140},//
    "06:69:6c:e7:fe:52": {x: 45,y: 140},
    "0a:69:6c:e7:fe:52": {x: 45,y: 140},
    "12:69:6c:e7:fe:52": {x: 45,y: 140},
    "0e:69:6c:e7:fe:53": {x: 45,y: 140},
    "12:69:6c:e7:fe:53": {x: 45,y: 140},

};

MetroMap.find = function(mac) {
	for(var station in MetroMap.station) {
		for(var macAdress in MetroMap.station[station]) {
			if(macAdress === mac) {
				return MetroMap.station[station][macAdress];
			}
		}
	}
	
	return null;
}