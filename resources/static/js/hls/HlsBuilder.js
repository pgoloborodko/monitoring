window.HlsBuilder = function (origin, date, id) {
    this.origin = origin;
    this.date = new Date(date.getTime());
    this.date.setHours(0);
    this.date.setMinutes(0);
    this.date.setSeconds(0);

    this.id = id;
};

HlsBuilder.prototype.formatDate = function (date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
};

HlsBuilder.prototype.load = function (callback) {
    let request = new Request(this.origin + "/media/playlist/archive?date=" + this.formatDate(this.date) + "&user=" + this.id);

    fetch(request).then(response => {
        if (response.status === 200) {
            return response.json();
        } else {
            throw new Error('Something went wrong on api server!');
        }
    }).then(mediaFiles => {
        console.log("load media", mediaFiles);

        // load stream archive
        this.loadStreamArchive(mediaFiles).then(function (streamPlaylist) {
            this.loadArchive(mediaFiles).then(function (archivePlaylist) {
                let playlist = this.mergePlaylist(streamPlaylist.flat(), archivePlaylist.flat());

                callback(playlist, this.getM3U8(playlist));
            }.bind(this));
        }.bind(this));
    }).catch(error => {
        console.error(error);
    });
};

/**
 * load stream playlist
 */
HlsBuilder.prototype.loadStreamArchive = function (mediaFiles) {
    let promises = [];

    for(let i=0; i < mediaFiles.length; i++) {
        if(mediaFiles[i].duration < 0) {
            promises.push(new Promise((resolve) => {
                let hls = new Hls();
                hls.on(Hls.Events.LEVEL_LOADED, function(event, level) {
                    let playlist = [];

                    for(let f=0; f < level.details.fragments.length; f++) {
                        playlist.push({
                            start: level.details.fragments[f].programDateTime,
                            duration: level.details.fragments[f].duration * 1000,
                            chunk: level.details.fragments[f].url + "#" + level.details.fragments[f].rawProgramDateTime,
                            dis: f === 0 || level.details.fragments[f].tagList.length === 3,
                            stream: true,
                            device: mediaFiles[i].device
                        });
                    }

                    resolve(playlist);
                });

                hls.on(Hls.Events.ERROR, function(event, level) {
                    resolve([]);
                });

                hls.loadSource(mediaFiles[i].url);
            }));
        }
    }

    return Promise.all(promises);
};

/**
 * load archive playlist
 * @param mediaFiles
 * @returns {Promise<unknown[]>}
 */
HlsBuilder.prototype.loadArchive = function (mediaFiles) {
    let promises = [];

    for(let i=0; i < mediaFiles.length; i++) {
        if(mediaFiles[i].duration > 0) {
            promises.push(new Promise((resolve) => {
                let hls = new Hls();
                hls.on(Hls.Events.LEVEL_LOADED, function(event, level) {
                    let created = new Date(mediaFiles[i].created).getTime();
                    let playlist = [];

                    for(let f=0; f < level.details.fragments.length; f++) {
                        playlist.push({
                            start: created,
                            duration: level.details.fragments[f].duration * 1000,
                            chunk: level.details.fragments[f].url,
                            dis: f === 0 || level.details.fragments[f].tagList.length === 3,
                            stream: false,
                            device: mediaFiles[i].device
                        });

                        created += level.details.fragments[f].duration * 1000;
                    }

                    resolve(playlist);
                });

                hls.on(Hls.Events.ERROR, function(event, level) {
                    resolve([]);
                });

                hls.loadSource(mediaFiles[i].url);
            }));
        }
    }

    return Promise.all(promises);
};

/**
 * build archive and stream playlists
 * @param streamPlaylist
 * @param archivePlaylist
 */
HlsBuilder.prototype.mergePlaylist = function (streamPlaylist, archivePlaylist) {
    let from = 0;
    let i=0;

    console.log(streamPlaylist);
    console.log(archivePlaylist);

    while(true) {
        let isIntersection = false;
        let start = 0;
        let isInserted = false;

        while(true) {
            if(streamPlaylist[from] === undefined) {
                break;
            }

            if(archivePlaylist[i] === undefined) {
                break;
            }

            if(this.intersection(archivePlaylist[i], streamPlaylist[from])) {
                if(!isIntersection) {
                    start = from;
                    isIntersection = true;
                }

                from++;
            }
            else if(isIntersection) {
                isIntersection = false;

                streamPlaylist.splice(start, from - start, archivePlaylist[i]);
                archivePlaylist.splice(i, 1);

                from -= from - start - 1;

                isInserted = true;
                break;
            }
            else {
                if(from > archivePlaylist.length) {
                    break;
                }

                from++;
            }
        }

        i++;

        if(i >= archivePlaylist.length) {
            streamPlaylist = streamPlaylist.concat(archivePlaylist);
            streamPlaylist.sort((a, b) =>a.start - b.start);
            break;
        }
    }


    let playlistTime = 0;
    for(let i=0; i < streamPlaylist.length; i++) {
        streamPlaylist[i].time = playlistTime;
        playlistTime += streamPlaylist[i].duration;
    }

    return streamPlaylist;
};

/**
 *
 * @param a
 * @param b
 * @returns {boolean}
 */
HlsBuilder.prototype.intersection = function (a, b) {
    let result = Math.max(a.start, b.start) < Math.min(a.start + a.duration, b.start + b.duration);
    //С†console.log(a, b, result);
    return result;
};

/**
 *
 */
HlsBuilder.prototype.getNoDataForDay = function() {
    let result = "#EXTM3U\n" +
        "#EXT-X-VERSION:3\n" +
        "#EXT-X-TARGETDURATION:3600\n" +
        "#EXT-X-MEDIA-SEQUENCE:0\n" +
        "#EXT-X-STREAM-INF:BANDWIDTH=5361731,CODECS=\"avc1.100.41\",RESOLUTION=720x480\n";

    result += this.createPause(86400).join("\n") + "\n";
    result += "#EXT-X-ENDLIST" + "\n";

    return result;
};

/**
 *
 */
HlsBuilder.prototype.getM3U8 = function (playlist) {
    if(playlist.length === 0) {
        return this.getNoDataForDay();
    }

    let result = "#EXTM3U\n" +
        "#EXT-X-VERSION:3\n" +
        "#EXT-X-TARGETDURATION:3600\n" +
        "#EXT-X-MEDIA-SEQUENCE:0\n";

    let resultChunks = [];
    resultChunks = resultChunks.concat(this.createPause((playlist[0].start - this.date.getTime()) / 1000));
    let lastMediaFile = playlist[0];

    for(let i=0; i < playlist.length; i++) {
        if(playlist[i].duration == null) continue;

        if(i !== 0) {
            let prevMediaFile = playlist[i - 1];

            if (prevMediaFile.duration != null) {
                if(prevMediaFile.duration < 0) {
                    if(i - 2 >= 0) {
                        prevMediaFile = playlist[i - 2];
                    }
                }

                let endOfPrevMediaFile = prevMediaFile.start + prevMediaFile.duration;
                let pauseIntervalInSeconds = (playlist[i].start - endOfPrevMediaFile) / 1000;

                if (pauseIntervalInSeconds > 5) {
                    resultChunks = resultChunks.concat(this.createPause(pauseIntervalInSeconds));
                }
            }
        }

        if(playlist[i].dis) {
            resultChunks.push("\n#EXT-X-DISCONTINUITY");
        }

        resultChunks.push("#EXTINF:" + playlist[i].duration/1000 + ",");
        resultChunks.push("#EXT-X-PROGRAM-DATE-TIME:" + new Date(playlist[i].start).toISOString()); // date
        resultChunks.push(playlist[i].chunk);

        lastMediaFile = playlist[i];
    }

    let endDate = new Date(this.date.getTime() + 86400 * 1000);
    resultChunks = resultChunks.concat(this.createPause((endDate.getTime() - lastMediaFile.start - lastMediaFile.duration) / 1000));

    resultChunks.push("#EXT-X-ENDLIST");

    let res = result + resultChunks.join("\n");
    return res;
};

HlsBuilder.prototype.createPause = function(seconds, interval) {
    let chunks = [];

    //if(seconds < 1) {
        return chunks;
    //}

    let chunkSeconds = interval || 3600;

    console.log(chunkSeconds);

    let needChunks = Math.floor(seconds/chunkSeconds);
    let noData = location.origin + "/common/no-data-" + chunkSeconds + ".ts";

    for(let i=0; i < needChunks; i++) {
        chunks.push("\n#EXT-X-DISCONTINUITY\n" + "#EXTINF:" + chunkSeconds + ",\n" + noData);
    }

    let lost = seconds - needChunks * chunkSeconds;
    if(lost > 0) {
        let times = 0;

        if(lost >= 1800) {
            times = 1800;
        }
        else if(lost >= 900) {
            times = 900;
        }
        else if(lost >= 450) {
            times = 450;
        }
        else if(lost >= 150) {
            times = 150;
        }
        else if(lost >= 60) {
            times = 60;
        }
        else if(lost >= 20) {
            times = 20;
        }
        else if(lost >= 5) {
            times = 5;
        }
        else {
            times = 1;
        }

        chunks = chunks.concat(this.createPause(lost, times));
    }

    return chunks;
};