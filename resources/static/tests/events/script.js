let sock = new SockJS('/stomp');
let client = Stomp.over(sock);
let events = document.getElementById("events");
let select = document.getElementById("select");
let input = document.getElementById("input");

function log(value, data) {
    data = data || "{}";

    console.log(value, new Date().getTime(), JSON.parse(data));
    events.innerHTML += "<div>" + value+ ":" + new Date().getTime()+ ":" + data + "</div>";
}

function error(value, err) {
    console.error(value, err);
    events.innerHTML += "<div>" + value + ": " + JSON.stringify(err) + "</div>";
}

client.connect({}, function() {
    log("connected");

}, function(err) {
    error("STOMP error", err);
});

document.getElementById("subscribe").addEventListener("click", function () {
    let value = select.value.replace("{userId}", input.value);

    log("subscribe " + value);

    try {
        client.subscribe(value, function (frame) {
            log("message from " + value, frame.body);
        });
    }
    catch (e) {
        error("error subscribe " + value, e);
    }
}, false);