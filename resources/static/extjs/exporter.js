
if (!Ext.exporter) {
    Ext.exporter = {};
}
if (!Ext.exporter.data) {
    Ext.exporter.data = {};
}
if (!Ext.exporter.excel) {
    Ext.exporter.excel = {};
}
if (!Ext.exporter.file) {
    Ext.exporter.file = {};
}
if (!Ext.exporter.file.excel) {
    Ext.exporter.file.excel = {};
}
if (!Ext.exporter.file.html) {
    Ext.exporter.file.html = {};
}
if (!Ext.exporter.file.ooxml) {
    Ext.exporter.file.ooxml = {};
}
if (!Ext.exporter.file.ooxml.excel) {
    Ext.exporter.file.ooxml.excel = {};
}
if (!Ext.exporter.file.ooxml.theme) {
    Ext.exporter.file.ooxml.theme = {};
}
if (!Ext.exporter.file.zip) {
    Ext.exporter.file.zip = {};
}
if (!Ext.exporter.text) {
    Ext.exporter.text = {};
}
if (!Ext.pivot) {
    Ext.pivot = {};
}
if (!Ext.pivot.axis) {
    Ext.pivot.axis = {};
}
if (!Ext.pivot.cell) {
    Ext.pivot.cell = {};
}
if (!Ext.pivot.d3) {
    Ext.pivot.d3 = {};
}
if (!Ext.pivot.dimension) {
    Ext.pivot.dimension = {};
}
if (!Ext.pivot.feature) {
    Ext.pivot.feature = {};
}
if (!Ext.pivot.filter) {
    Ext.pivot.filter = {};
}
if (!Ext.pivot.matrix) {
    Ext.pivot.matrix = {};
}
if (!Ext.pivot.plugin) {
    Ext.pivot.plugin = {};
}
if (!Ext.pivot.plugin.configurator) {
    Ext.pivot.plugin.configurator = {};
}
if (!Ext.pivot.plugin.configurator.model) {
    Ext.pivot.plugin.configurator.model = {};
}
if (!Ext.pivot.plugin.configurator.store) {
    Ext.pivot.plugin.configurator.store = {};
}
if (!Ext.pivot.plugin.rangeeditor) {
    Ext.pivot.plugin.rangeeditor = {};
}
if (!Ext.pivot.result) {
    Ext.pivot.result = {};
}
if (!Ext.pivot.update) {
    Ext.pivot.update = {};
}

var Mz = Mz || {};
if (!Mz.aggregate) {
    Mz.aggregate = {};
}
if (!Mz.aggregate.axis) {
    Mz.aggregate.axis = {};
}
if (!Mz.aggregate.dimension) {
    Mz.aggregate.dimension = {};
}
if (!Mz.aggregate.filter) {
    Mz.aggregate.filter = {};
}
if (!Mz.aggregate.matrix) {
    Mz.aggregate.matrix = {};
}
if (!Mz.pivot) {
    Mz.pivot = {};
}
if (!Mz.pivot.plugin) {
    Mz.pivot.plugin = {};
}

Ext.define('Ext.overrides.exporter.util.Format', {
    override: 'Ext.util.Format',
    decToHex: function(a, d) {
        var c = '',
            b;
        for (b = 0; b < d; b++) {
            c += String.fromCharCode(a & 255);
            a = a >>> 8
        }
        return c
    }
});

(Ext.cmd.derive('Ext.exporter.data.Base', Ext.Base, {
    config: {
        idPrefix: 'id',
        id: null,
        autoGenerateId: !0,
        autoGenerateKey: {
            $value: !1,
            merge: function(a, b) {
                return a ? Ext.concat(a, b || null) : !1
            }
        }
    },
    internalCols: null,
    clearPropertiesOnDestroy: !1,
    constructor: function(b) {
        var a = this;
        a.internalCols = [];
        a.initConfig(b);
        if (!a._id) {
            a.setId(null)
        }
        return a.callParent([b])
    },
    destroy: function() {
        this.destroyCollections();
        this.callParent();
        this.internalCols = null
    },
    destroyCollections: function() {
        var d = this.internalCols,
            f = d.length,
            b, c, e, a;
        for (b = 0; b < f; b++) {
            a = d[b];
            e = a.length;
            for (c = 0; c < e; c++) {
                a.items[c].destroy()
            }
            a.destroy()
        }
        d.length = 0
    },
    clearCollections: function(a) {
        var c, d, b;
        a = a ? Ext.Array.from(a) : this.internalCols;
        d = a.length;
        for (c = d - 1; c >= 0; c--) {
            b = a[c];
            if (b) {
                b.destroy()
            }
            Ext.Array.remove(this.internalCols, b)
        }
    },
    applyId: function(b) {
        var a;
        if (!b && this._autoGenerateId) {
            a = this._idPrefix + (++Ext.idSeed)
        } else {
            a = b
        }
        return a
    },
    checkCollection: function(c, a, d) {
        var b;
        if (c) {
            b = this.constructCollection(d);
            b.add(c)
        }
        if (a) {
            Ext.Array.remove(this.internalCols, a);
            Ext.destroy(a.items, a)
        }
        return b
    },
    constructCollection: function(d) {
        var c = Ext.ClassManager.get(d),
            b = {
                decoder: this.getCollectionDecoder(c)
            },
            a;
        if (typeof c.prototype.getKey === 'function') {
            b.keyFn = this.getCollectionItemKey
        }
        a = new Ext.util.Collection(b);
        this.internalCols.push(a);
        return a
    },
    getCollectionDecoder: function(a) {
        return function(b) {
            return (b && b.isInstance) ? b : new a(b || {})
        }
    },
    getCollectionItemKey: function(a) {
        return a.getKey ? a.getKey() : (a._id || a.getId())
    },
    getKey: function() {
        var a = this.getAutoGenerateKey(),
            c = '',
            e = this.getConfig(),
            d, b;
        if (!Ext.isArray(a) || !a.length) {
            return this.getId()
        }
        d = a.length;
        for (b = 0; b < d; b++) {
            c += this.serializeKeyValue(e[a[b]])
        }
        return c
    },
    serializeKeyValue: function(a) {
        var c = '',
            d, e, b;
        if (a === null) {
            c = '_'
        } else if (Ext.isDate(a)) {
            c = a.getTime()
        } else if (Ext.isArray(a)) {
            e = a.length;
            for (b = 0; b < e; b++) {
                c += this.serializeKeyValue(a[b])
            }
        } else if (typeof a === 'object') {
            if (a.isInstance) {
                c = a.getKey ? a.getKey() : '_'
            } else {
                d = Ext.Object.getAllKeys(a);
                d = Ext.Array.sort(d);
                e = d.length;
                for (b = 0; b < e; b++) {
                    c += this.serializeKeyValue(a[d[b]])
                }
            }
        } else {
            c = a
        }
        return c
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.data, 'Base'], 0));
(Ext.cmd.derive('Ext.exporter.data.Cell', Ext.exporter.data.Base, {
    config: {
        style: null,
        value: null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.data, 'Cell'], 0));
(Ext.cmd.derive('Ext.exporter.data.Row', Ext.exporter.data.Base, {
    config: {
        cells: null
    },
    destroy: function() {
        this.clearCollections()
    },
    applyCells: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.data.Cell')
    },
    addCell: function(a) {
        if (!this._cells) {
            this.setCells([])
        }
        return this._cells.add(a || {})
    },
    getCell: function(a) {
        return this._cells ? this._cells.get(a) : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.data, 'Row'], 0));
(Ext.cmd.derive('Ext.exporter.data.Group', Ext.exporter.data.Base, {
    config: {
        text: null,
        rows: null,
        summaries: null,
        summary: null,
        groups: null
    },
    applyRows: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.data.Row')
    },
    addRow: function(a) {
        if (!this._rows) {
            this.setRows([])
        }
        return this._rows.add(a || {})
    },
    getRow: function(a) {
        return this._rows ? this._rows.get(a) : null
    },
    applyGroups: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.data.Group')
    },
    addGroup: function(a) {
        if (!this._groups) {
            this.setGroups([])
        }
        return this._groups.add(a || {})
    },
    getGroup: function(a) {
        return this._groups ? this._groups.get(a) : null
    },
    applySummaries: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.data.Row')
    },
    applySummary: function(a) {
        if (a) {
            this.addSummary(a)
        }
        return null
    },
    addSummary: function(a) {
        if (!this._summaries) {
            this.setSummaries([])
        }
        return this._summaries.add(a || {})
    },
    getSummary: function(a) {
        return this._summaries ? this._summaries.get(a) : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.data, 'Group'], 0));
(Ext.cmd.derive('Ext.exporter.data.Column', Ext.exporter.data.Base, {
    config: {
        table: null,
        text: null,
        style: null,
        width: null,
        mergeAcross: null,
        mergeDown: null,
        level: 0,
        index: null,
        columns: null
    },
    destroy: function() {
        this.setTable(null);
        Ext.exporter.data.Base.prototype.destroy.call(this)
    },
    updateTable: function(d) {
        var b = this.getColumns(),
            a, c;
        if (b) {
            c = b.length;
            for (a = 0; a < c; a++) {
                b.getAt(a).setTable(d)
            }
        }
    },
    applyColumns: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.data.Column')
    },
    updateColumns: function(c, b) {
        var a = this;
        if (b) {
            b.un({
                add: a.onColumnAdd,
                remove: a.onColumnRemove,
                scope: a
            });
            Ext.destroy(b.items, b)
        }
        if (c) {
            c.on({
                add: a.onColumnAdd,
                remove: a.onColumnRemove,
                scope: a
            });
            a.onColumnAdd(c, {
                items: c.getRange()
            })
        }
    },
    sync: function(c, h) {
        var a = this,
            g = a.getColumnCount() - 1,
            d = a.getColumns(),
            b, f, e;
        a.setLevel(c);
        if (d) {
            f = d.length;
            for (b = 0; b < f; b++) {
                d.items[b].sync(c + 1, h)
            }
            a.setMergeDown(null)
        } else {
            e = h - c;
            a.setMergeDown(e > 0 ? e : null)
        }
        a.setMergeAcross(g > 0 ? g : null)
    },
    onColumnAdd: function(g, e) {
        var c = e.items,
            f = c.length,
            b = this.getTable(),
            a, d;
        for (a = 0; a < f; a++) {
            d = c[a];
            d.setTable(b)
        }
        if (b) {
            b.syncColumns()
        }
    },
    onColumnRemove: function(c, b) {
        var a = this.getTable();
        Ext.destroy(b.items);
        if (a) {
            a.syncColumns()
        }
    },
    getColumnCount: function(a) {
        var d = 0,
            b;
        if (!a) {
            a = this.getColumns();
            if (!a) {
                return 1
            }
        }
        for (var c = 0; c < a.length; c++) {
            b = a.getAt(c).getColumns();
            if (!b) {
                d += 1
            } else {
                d += this.getColumnCount(b)
            }
        }
        return d
    },
    addColumn: function(a) {
        if (!this.getColumns()) {
            this.setColumns([])
        }
        return this.getColumns().add(a || {})
    },
    getColumn: function(a) {
        return this.getColumns().get(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.data, 'Column'], 0));
(Ext.cmd.derive('Ext.exporter.data.Table', Ext.exporter.data.Group, {
    isDataTable: !0,
    config: {
        columns: null
    },
    autoGenerateId: !1,
    applyColumns: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.data.Column')
    },
    updateColumns: function(c, b) {
        var a = this;
        if (b) {
            b.un({
                add: a.onColumnAdd,
                remove: a.onColumnRemove,
                scope: a
            });
            Ext.destroy(b.items, b)
        }
        if (c) {
            c.on({
                add: a.onColumnAdd,
                remove: a.onColumnRemove,
                scope: a
            });
            a.onColumnAdd(c, {
                items: c.getRange()
            });
            a.syncColumns()
        }
    },
    syncColumns: function() {
        var d = this.getColumns(),
            j = this.getColDepth(d, -1),
            h = {},
            a, b, e, k, i, c, g, f;
        if (!d) {
            return
        }
        e = d.length;
        for (a = 0; a < e; a++) {
            d.items[a].sync(0, j)
        }
        this.getColumnLevels(d, j, h);
        i = Ext.Object.getKeys(h);
        e = i.length;
        for (a = 0; a < e; a++) {
            c = h[i[a]];
            k = c.length;
            for (b = 0; b < k; b++) {
                if (b === 0) {
                    f = 1
                } else if (c[b - 1]) {
                    g = c[b - 1].getConfig();
                    f += (g.mergeAcross ? g.mergeAcross + 1 : 1)
                } else {
                    f++
                }
                if (c[b]) {
                    c[b].setIndex(f)
                }
            }
        }
    },
    getLeveledColumns: function() {
        var b = this.getColumns(),
            c = this.getColDepth(b, -1),
            a = {};
        this.getColumnLevels(b, c, a, !0);
        return a
    },
    getBottomColumns: function() {
        var b = this.getLeveledColumns(),
            a, c;
        a = Ext.Object.getKeys(b);
        c = a.length;
        return c ? b[a[a.length - 1]] : []
    },
    getColumnLevels: function(f, j, b, i) {
        var c, d, e, k, a, g, h;
        if (!f) {
            return
        }
        k = f.length;
        for (d = 0; d < k; d++) {
            c = f.items[d];
            g = c.getLevel();
            h = c.getColumns();
            a = 's' + g;
            b[a] = b[a] || [];
            b[a].push(c);
            if (!h) {
                for (e = g + 1; e <= j; e++) {
                    a = 's' + e;
                    b[a] = b[a] || [];
                    b[a].push(i ? c : null)
                }
            } else {
                this.getColumnLevels(h, j, b, i)
            }
        }
    },
    onColumnAdd: function(f, d) {
        var b = d.items,
            e = b.length,
            a, c;
        for (a = 0; a < e; a++) {
            c = b[a];
            c.setTable(this)
        }
        this.syncColumns()
    },
    onColumnRemove: function(b, a) {
        Ext.destroy(a.items);
        this.syncColumns()
    },
    getColumnCount: function() {
        var b = this._columns,
            d = 0,
            a, c;
        if (b) {
            c = b.length;
            for (a = 0; a < c; a++) {
                d += b.items[a].getColumnCount()
            }
        }
        return d
    },
    getColDepth: function(a, d) {
        var c = 0,
            e;
        if (!a) {
            return d
        }
        e = a.length;
        for (var b = 0; b < e; b++) {
            c = Math.max(c, this.getColDepth(a.items[b]._columns, d + 1))
        }
        return c
    },
    addColumn: function(a) {
        if (!this._columns) {
            this.setColumns([])
        }
        return this._columns.add(a || {})
    },
    getColumn: function(a) {
        return this._columns ? this._columns.get(a) : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.data, 'Table'], 0));
(Ext.cmd.derive('Ext.exporter.file.Base', Ext.exporter.data.Base, {
    tpl: null,
    destroy: function() {
        this.tpl = null;
        Ext.exporter.data.Base.prototype.destroy.call(this)
    },
    render: function() {
        var a = this,
            b = a.processRenderData(a.getRenderData());
        return a.tpl ? Ext.XTemplate.getTpl(a, 'tpl').apply(b) : ''
    },
    processRenderData: function(a) {
        return a
    },
    getRenderData: function() {
        var a = this.getConfig();
        a.self = this;
        return a
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file, 'Base'], 0));
(Ext.cmd.derive('Ext.exporter.file.Style', Ext.exporter.file.Base, {
    config: {
        name: null,
        alignment: null,
        font: null,
        interior: null,
        format: null,
        borders: null,
        checks: {
            alignment: {
                horizontal: ['Automatic', 'Left', 'Center', 'Right', 'Justify'],
                readingOrder: ['LeftToRight', 'RightToLeft', 'Context'],
                vertical: ['Automatic', 'Top', 'Bottom', 'Center']
            },
            font: {
                bold: [!0, !1],
                italic: [!0, !1],
                strikeThrough: [!0, !1],
                underline: ['None', 'Single']
            },
            border: {
                position: ['Left', 'Top', 'Right', 'Bottom'],
                lineStyle: ['None', 'Continuous', 'Dash', 'Dot']
            },
            interior: {
                pattern: ['None', 'Solid']
            }
        }
    },
    datePatterns: {
        'General Date': 'Y-m-d H:i:s',
        'Long Date': 'l, F d, Y',
        'Medium Date': 'Y-m-d',
        'Short Date': 'n/j/Y',
        'Long Time': 'g:i:s A',
        'Medium Time': 'H:i:s',
        'Short Time': 'g:i A'
    },
    numberPatterns: {
        'General Number': '0',
        'Fixed': '0.00',
        'Standard': '0.00'
    },
    booleanPatterns: {
        'Yes/No': ['Yes', 'No'],
        'True/False': ['True', 'False'],
        'On/Off': ['On', 'Off']
    },
    isStyle: !0,
    autoGenerateKey: ['alignment', 'font', 'interior', 'format', 'borders'],
    constructor: function(a) {
        Ext.exporter.file.Base.prototype.constructor.call(this, this.uncapitalizeKeys(a))
    },
    uncapitalizeKeys: function(b) {
        var c = b,
            e, d, a, f, g;
        if (Ext.isObject(b)) {
            c = {};
            e = Ext.Object.getAllKeys(b);
            d = e.length;
            for (a = 0; a < d; a++) {
                f = e[a];
                c[Ext.String.uncapitalize(f)] = this.uncapitalizeKeys(b[f])
            }
        } else if (Ext.isArray(b)) {
            c = [];
            d = b.length;
            for (a = 0; a < d; a++) {
                c.push(this.uncapitalizeKeys(b[a]))
            }
        }
        return c
    },
    destroy: function() {
        var a = this;
        a.setAlignment(null);
        a.setFont(null);
        a.setInterior(null);
        a.setBorders(null);
        a.setChecks(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    updateAlignment: function(a) {
        this.checkAttribute(a, 'alignment')
    },
    updateFont: function(a) {
        this.checkAttribute(a, 'font')
    },
    updateInterior: function(a) {
        this.checkAttribute(a, 'interior')
    },
    applyBorders: function(a, b) {
        if (!a) {
            return a
        }
        a = Ext.Array.from(a);
        return a
    },
    updateBorders: function(a) {
        this.checkAttribute(a, 'border')
    },
    checkAttribute: function(k, i) {
        var f = this.getChecks(),
            g, h, m, d, e, c, a, b, l, j;
        if (!k || !f || !f[i]) {
            return
        }
        g = Ext.Array.from(k);
        l = g.length;
        for (d = 0; d < l; d++) {
            b = g[d];
            h = Ext.Object.getKeys(b || {});
            m = h.length;
            for (e = 0; e < m; e++) {
                a = h[e];
                if (c = f[i][a] && b[a]) {
                    j = (Ext.isArray(c) ? Ext.Array.indexOf(c, b[a]) : c === b[a]);
                    if (!j) {
                        delete(b[a])
                    }
                }
            }
        }
    },
    getFormattedValue: function(b) {
        var c = this,
            a = c.getFormat(),
            e = b,
            d = Ext.util.Format;
        if (!a || a === 'General' || Ext.isEmpty(b)) {
            return e
        }
        if (a === 'Currency') {
            return d.currency(b)
        } else if (a === 'Euro Currency') {
            return d.currency(b, 'â‚¬')
        } else if (a === 'Percent') {
            return d.number(b * 100, '0.00') + '%'
        } else if (a === 'Scientific') {
            return Number(b).toExponential()
        } else if (c.datePatterns[a]) {
            return d.date(b, c.datePatterns[a])
        } else if (c.numberPatterns[a]) {
            return d.number(b, c.numberPatterns[a])
        } else if (c.booleanPatterns[a]) {
            return b ? c.booleanPatterns[a][0] : c.booleanPatterns[a][1]
        } else if (Ext.isFunction(a)) {
            return a(b)
        }
        return d.number(b, a)
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file, 'Style'], 0));
(Ext.cmd.derive('Ext.exporter.File', Ext.Base, {
    singleton: !0,
    textPopupWait: 'You may close this window after the file is downloaded!',
    textPopupBlocker: 'The file was not saved because pop-up blocker might be enabled! Please check your browser settings.',
    url: 'https://exporter.sencha.com',
    forceDownload: !1,
    requiresPopup: function() {
        var a = Ext.platformTags;
        return this.forceDownload || Ext.isSafari || a.phone || a.tablet
    },
    initializePopup: function(d) {
        var a = this,
            c = a.requiresPopup(),
            b;
        if (!c && d) {
            c = !a.saveBlobAs
        }
        a.popup = null;
        if (c) {
            b = window.open('', '_blank');
            if (b) {
                a.popup = b;
                b.document.write(Ext.dom.Helper.markup({
                    tag: 'html',
                    children: [{
                        tag: 'head'
                    }, {
                        tag: 'body',
                        children: [{
                            tag: 'p',
                            html: a.textPopupWait
                        }]
                    }]
                }))
            }
        }
    },
    saveBinaryAs: function(f, c, e, d) {
        var a = this,
            b = a.downloadBinaryAs;
        if (!a.requiresPopup() && a.saveBlobAs) {
            b = a.saveBlobAs
        }
        return b.call(a, f, c, e, d)
    },
    downloadBinaryAs: function(g, d, f, e) {
        var a = new Ext.Deferred(),
            c, b;
        c = Ext.dom.Helper.markup({
            tag: 'html',
            children: [{
                tag: 'head'
            }, {
                tag: 'body',
                children: [{
                    tag: 'form',
                    method: 'POST',
                    action: this.url,
                    children: [{
                        tag: 'input',
                        type: 'hidden',
                        name: 'content',
                        value: Ext.util.Base64.encode(g)
                    }, {
                        tag: 'input',
                        type: 'hidden',
                        name: 'filename',
                        value: d
                    }, {
                        tag: 'input',
                        type: 'hidden',
                        name: 'charset',
                        value: f || 'UTF-8'
                    }, {
                        tag: 'input',
                        type: 'hidden',
                        name: 'mime',
                        value: e || 'application/octet-stream'
                    }]
                }, {
                    tag: 'script',
                    type: 'text/javascript',
                    children: 'document.getElementsByTagName("form")[0].submit();'
                }]
            }]
        });
        b = this.popup || window.open('', '_blank');
        if (b) {
            b.document.write(c);
            a.resolve()
        } else {
            a.reject(this.textPopupBlocker)
        }
        this.popup = null;
        return a.promise
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter, 'File'], function(c) {
    var a = window.navigator,
        b = window.saveAs || (function(b) {
            "use strict";
            if (typeof a !== "undefined" && /MSIE [1-9]\./.test(a.userAgent)) {
                return
            }
            var u = b.document,
                i = function() {
                    return b.URL || b.webkitURL || b
                },
                e = u.createElementNS("http://www.w3.org/1999/xhtml", "a"),
                q = "download" in e,
                t = function(d) {
                    var a = new MouseEvent("click");
                    d.dispatchEvent(a)
                },
                m = /Version\/[\d\.]+.*Safari/.test(a.userAgent),
                g = b.webkitRequestFileSystem,
                o = b.requestFileSystem || g || b.mozRequestFileSystem,
                r = function(a) {
                    (b.setImmediate || b.setTimeout)(function() {
                        throw a
                    }, 0)
                },
                f = "application/octet-stream",
                k = 0,
                p = 1000 * 40,
                j = function(a) {
                    var d = function() {
                        if (typeof a === "string") {
                            i().revokeObjectURL(a)
                        } else {
                            a.remove()
                        }
                    };
                    setTimeout(d, p)
                },
                h = function(d, a, g) {
                    a = [].concat(a);
                    var f = a.length;
                    while (f--) {
                        var e = d["on" + a[f]];
                        if (typeof e === "function") {
                            try {
                                e.call(d, g || d)
                            } catch (v) {
                                r(v)
                            }
                        }
                    }
                },
                n = function(a) {
                    if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(a.type)) {
                        return new Blob(["\ufeff", a], {
                            type: a.type
                        })
                    }
                    return a
                },
                l = function(d, r, A) {
                    if (!A) {
                        d = n(d)
                    }
                    var a = this,
                        w = d.type,
                        y = !1,
                        l, s, v = function() {
                            h(a, "writestart progress write writeend".split(" "))
                        },
                        p = function() {
                            if (s && m && typeof FileReader !== "undefined") {
                                var e = new FileReader();
                                e.onloadend = function() {
                                    var f = e.result;
                                    s.location.href = "data:attachment/file" + f.slice(f.search(/[,;]/));
                                    a.readyState = a.DONE;
                                    v()
                                };
                                e.readAsDataURL(d);
                                a.readyState = a.INIT;
                                return
                            }
                            if (y || !l) {
                                l = i().createObjectURL(d)
                            }
                            if (s) {
                                s.location.href = l
                            } else {
                                var f = b.open(l, "_blank");
                                if (f === undefined && m) {
                                    b.location.href = l
                                }
                            }
                            a.readyState = a.DONE;
                            v();
                            j(l)
                        },
                        u = function(e) {
                            return function() {
                                if (a.readyState !== a.DONE) {
                                    return e.apply(this, arguments)
                                }
                            }
                        },
                        x = {
                            create: !0,
                            exclusive: !1
                        },
                        z;
                    a.readyState = a.INIT;
                    if (!r) {
                        r = "download"
                    }
                    if (q) {
                        l = i().createObjectURL(d);
                        setTimeout(function() {
                            e.href = l;
                            e.download = r;
                            t(e);
                            v();
                            j(l);
                            a.readyState = a.DONE
                        });
                        return
                    }
                    if (b.chrome && w && w !== f) {
                        z = d.slice || d.webkitSlice;
                        d = z.call(d, 0, d.size, f);
                        y = !0
                    }
                    if (g && r !== "download") {
                        r += ".download"
                    }
                    if (w === f || g) {
                        s = b
                    }
                    if (!o) {
                        p();
                        return
                    }
                    k += d.size;
                    o(b.TEMPORARY, k, u(function(e) {
                        e.root.getDirectory("saved", x, u(function(g) {
                            var f = function() {
                                g.getFile(r, x, u(function(f) {
                                    f.createWriter(u(function(i) {
                                        i.onwriteend = function(k) {
                                            s.location.href = f.toURL();
                                            a.readyState = a.DONE;
                                            h(a, "writeend", k);
                                            j(f)
                                        };
                                        i.onerror = function() {
                                            var a = i.error;
                                            if (a.code !== a.ABORT_ERR) {
                                                p()
                                            }
                                        };
                                        "writestart progress write abort".split(" ").forEach(function(h) {
                                            i["on" + h] = a["on" + h]
                                        });
                                        i.write(d);
                                        a.abort = function() {
                                            i.abort();
                                            a.readyState = a.DONE
                                        };
                                        a.readyState = a.WRITING
                                    }), p)
                                }), p)
                            };
                            g.getFile(r, {
                                create: !1
                            }, u(function(a) {
                                a.remove();
                                f()
                            }), u(function(a) {
                                if (a.code === a.NOT_FOUND_ERR) {
                                    f()
                                } else {
                                    p()
                                }
                            }))
                        }), p)
                    }), p)
                },
                d = l.prototype,
                s = function(d, e, a) {
                    return new l(d, e, a)
                };
            if (typeof a !== "undefined" && a.msSaveOrOpenBlob) {
                return function(d, f, e) {
                    if (!e) {
                        d = n(d)
                    }
                    return a.msSaveOrOpenBlob(d, f || "download")
                }
            }
            d.abort = function() {
                var a = this;
                a.readyState = a.DONE;
                h(a, "abort")
            };
            d.readyState = d.INIT = 0;
            d.WRITING = 1;
            d.DONE = 2;
            d.error = d.onwritestart = d.onprogress = d.onwrite = d.onabort = d.onerror = d.onwriteend = null;
            return s
        }(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content));
    if (typeof module !== "undefined" && module.exports) {
        module.exports.saveAs = b
    } else if ((typeof define !== "undefined" && define !== null) && (define.amd !== null)) {
        define([], function() {
            return b
        })
    }
    var d = window.saveTextAs || (function(e, f, g) {
        f = f || 'download.txt';
        g = g || 'utf-8';
        e = (e || '').replace(/\r?\n/g, "\r\n");
        if (b && Blob) {
            var i = new Blob([e], {
                type: "text/plain;charset=" + g
            });
            b(i, f);
            return !0
        } else {
            var a = window.frames.saveTxtWindow;
            if (!a) {
                a = document.createElement('iframe');
                a.id = 'saveTxtWindow';
                a.style.display = 'none';
                document.body.insertBefore(a, null);
                a = window.frames.saveTxtWindow;
                if (!a) {
                    a = c.popup || window.open('', '_temp', 'width=100,height=100');
                    if (!a) {
                        return !1
                    }
                }
            }
            var d = a.document;
            d.open('text/html', 'replace');
            d.charset = g;
            d.write(e);
            d.close();
            var h = d.execCommand('SaveAs', null, f);
            a.close();
            return h
        }
    });
    c.saveAs = function(f, b, e, g) {
        var a;
        if (this.requiresPopup()) {
            return this.downloadBinaryAs(f, b, e || 'UTF-8', g || 'text/plain')
        } else {
            a = new Ext.Deferred();
            if (d(f, b, e)) {
                a.resolve()
            } else {
                a.reject()
            }
            return a.promise
        }
    };
    if (b && Blob) {
        c.saveBlobAs = function(e, h, l, i) {
            var f = new Ext.Deferred();
            var d = new Uint8Array(e.length),
                k = d.length,
                j = {
                    type: i || 'application/octet-stream'
                },
                g, a;
            for (a = 0; a < k; a++) {
                d[a] = e.charCodeAt(a)
            }
            g = new Blob([d], j);
            b(g, h);
            f.resolve();
            return f.promise
        }
    }
}));
(Ext.cmd.derive('Ext.exporter.Base', Ext.Base, {
    config: {
        data: null,
        showSummary: !0,
        title: null,
        author: 'Sencha',
        fileName: 'export.txt',
        charset: 'UTF-8',
        mimeType: 'text/plain',
        binary: !1
    },
    constructor: function(a) {
        this.initConfig(a || {});
        Ext.exporter.File.initializePopup(this.getBinary());
        return this.callParent([a])
    },
    destroy: function() {
        this.setData(Ext.destroy(this.getData()));
        this.callParent()
    },
    getContent: Ext.identityFn,
    saveAs: function() {
        var b = this,
            a = new Ext.Deferred();
        Ext.asap(b.delayedSave, b, [a]);
        return a.promise
    },
    delayedSave: function(b) {
        var a = this,
            d = a.getBinary() ? 'saveBinaryAs' : 'saveAs',
            c = Ext.exporter.File[d](a.getContent(), a.getFileName(), a.getCharset(), a.getMimeType());
        c.then(function() {
            b.resolve()
        }, function(a) {
            b.reject(a)
        })
    },
    getColumnCount: function(a) {
        var c = 0;
        if (!a) {
            return c
        }
        for (var b = 0; b < a.length; b++) {
            if (!a[b].columns) {
                c += 1
            } else {
                c += this.getColumnCount(a[b].columns)
            }
        }
        return c
    },
    applyData: function(a) {
        if (!a || a.isDataTable) {
            return a
        }
        return new Ext.exporter.data.Table(a)
    }
}, 1, 0, 0, 0, ["exporter.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.exporter, 'Base'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.Base', Ext.exporter.file.Base, {
    config: {
        tplAttributes: {
            $value: [],
            merge: function(a, b) {
                return [].concat(a, b)
            }
        },
        tplNonAttributes: {
            $value: ['idPrefix', 'id', 'autoGenerateId', 'self', 'tplAttributes', 'tplNonAttributes'],
            merge: function(a, b) {
                return [].concat(a, b)
            }
        }
    },
    generateTplAttributes: !1,
    processRenderData: function(a) {
        var f = this.getTplAttributes(),
            e = this.getTplNonAttributes(),
            g = Ext.Object.getAllKeys(a),
            h = g.length,
            c = '',
            d, b;
        if (!this.generateTplAttributes) {
            a.attributes = '';
            return a
        }
        for (d = 0; d < h; d++) {
            b = g[d];
            if (f && f.length) {
                if (Ext.Array.indexOf(f, b) >= 0 && a[b] !== null) {
                    c += (c.length ? ' ' : '') + this.processTplAttribute(b, a[b])
                }
            } else if (e && e.length) {
                if (Ext.Array.indexOf(e, b) < 0 && a[b] !== null) {
                    c += (c.length ? ' ' : '') + this.processTplAttribute(b, a[b])
                }
            }
        }
        a.attributes = c;
        return a
    },
    processTplAttribute: function(c, a) {
        var b = a;
        if (typeof a === 'boolean') {
            b = Number(a)
        } else if (typeof a === 'string') {
            b = Ext.util.Base64._utf8_encode(Ext.util.Format.htmlEncode(a || ''))
        }
        return (c + '="' + b + '"')
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'Base'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.Relationship', Ext.exporter.file.Base, {
    isRelationship: !0,
    config: {
        idPrefix: 'rId',
        schema: '',
        target: '',
        parentFolder: null,
        path: null
    },
    tpl: ['<Relationship Id="{id}" Type="{schema}" Target="{path}"/>'],
    updateTarget: function(a) {
        this.calculatePath()
    },
    applyParentFolder: function(a) {
        a = a || '';
        if (a[a.length - 1] == '/') {
            a = a.slice(0, a.length - 1)
        }
        return a
    },
    updateParentFolder: function(a) {
        this.calculatePath()
    },
    calculatePath: function() {
        var i = String(this.getParentFolder() || ''),
            h = String(this.getTarget() || ''),
            d = i.split('/'),
            e = h.split('/'),
            g = Math.min(d.length, e.length),
            b = g,
            f = '',
            c = [],
            a;
        for (a = 0; a < g; a++) {
            if (d[a] !== e[a]) {
                b = a;
                break
            }
        }
        if (b == 0) {
            f = h
        } else {
            for (a = b; a < d.length; a++) {
                c.push('..')
            }
            c = c.concat(e.slice(b));
            f = c.join('/')
        }
        this.setPath(f)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'Relationship'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.ContentType', Ext.exporter.file.Base, {
    isContentType: !0,
    config: {
        tag: 'Override',
        partName: '',
        contentType: '',
        extension: ''
    },
    tpl: ['<{tag}', '<tpl if="extension"> Extension="{extension}"</tpl>', '<tpl if="partName"> PartName="{partName}"</tpl>', '<tpl if="contentType"> ContentType="{contentType}"</tpl>', '/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'ContentType'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.Xml', Ext.exporter.file.ooxml.Base, {
    config: {
        folder: null,
        fileName: null,
        path: null,
        relationship: null,
        contentType: null
    },
    cachedConfig: {
        fileNameTemplate: '{fileName}.xml'
    },
    tplNonAttributes: ['path', 'relationship', 'contentType', 'fileName', 'folder', 'fileNameTemplate'],
    destroy: function() {
        this.setRelationship(null);
        this.setContentType(null);
        Ext.exporter.file.ooxml.Base.prototype.destroy.call(this)
    },
    applyFolder: function(a) {
        a = a || '';
        if (a[a.length - 1] !== '/') {
            a += '/'
        }
        return a
    },
    updateFolder: function() {
        this.generatePath()
    },
    updateFileName: function() {
        this.generatePath()
    },
    getFileNameFromTemplate: function() {
        var a = Ext.XTemplate.getTpl(this, '_fileNameTemplate');
        return (a ? a.apply(this.getConfig()) : '')
    },
    generatePath: function() {
        this.setPath((this.getFolder() || '') + this.getFileNameFromTemplate())
    },
    updatePath: function(b) {
        var a = this.getRelationship(),
            c = this.getContentType();
        if (a) {
            a.setTarget(b)
        }
        if (c) {
            c.setPartName(b)
        }
    },
    applyRelationship: function(a) {
        if (!a || a.isRelationship) {
            return a
        }
        return new Ext.exporter.file.ooxml.Relationship(a)
    },
    updateRelationship: function(b, a) {
        Ext.destroy(a)
    },
    applyContentType: function(a) {
        if (!a || a.isContentType) {
            return a
        }
        return new Ext.exporter.file.ooxml.ContentType(a)
    },
    updateContentType: function(b, a) {
        Ext.destroy(a)
    },
    collectFiles: Ext.emptyFn
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'Xml'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.Relationships', Ext.exporter.file.ooxml.Xml, {
    isRelationships: !0,
    currentIndex: 1,
    config: {
        parentFolder: null,
        items: []
    },
    contentType: {
        contentType: 'application/vnd.openxmlformats-package.relationships+xml'
    },
    fileNameTemplate: '{fileName}.rels',
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">', '<tpl if="items"><tpl for="items.getRange()">{[values.render()]}</tpl></tpl>', '</Relationships>'],
    collectFiles: function(e) {
        var c = this.getItems(),
            b = c.length,
            d = this.getParentFolder(),
            a;
        if (b) {
            for (a = 0; a < b; a++) {
                c.getAt(a).setParentFolder(d)
            }
            e[this.getPath()] = this.render()
        }
    },
    applyFolder: function(a, b) {
        a = Ext.exporter.file.ooxml.Xml.prototype.applyFolder.call(this, a, b);
        return a + '_rels/'
    },
    applyItems: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.Relationship')
    },
    updateItems: function(c, b) {
        var a = this;
        if (b) {
            b.un({
                add: a.updateIds,
                remove: a.updateIds,
                scope: a
            })
        }
        if (c) {
            c.on({
                add: a.updateIds,
                remove: a.updateIds,
                scope: a
            })
        }
    },
    updateIds: function(b) {
        var a, d, c;
        if (!b) {
            return
        }
        d = b.length;
        for (a = 0; a < d; a++) {
            c = b.getAt(a);
            c.setId('rId' + (a + 1))
        }
    },
    addRelationship: function(a) {
        return this.getItems().add(a || {})
    },
    removeRelationship: function(a) {
        return this.getItems().remove(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'Relationships'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.XmlRels', Ext.exporter.file.ooxml.Xml, {
    config: {
        index: null,
        name: null,
        relationships: {
            contentType: {
                contentType: 'application/vnd.openxmlformats-package.relationships+xml'
            }
        }
    },
    cachedConfig: {
        nameTemplate: '{name}'
    },
    tplNonAttributes: ['index', 'relationships', 'nameTemplate'],
    contentType: {},
    relationship: {},
    fileNameTemplate: '{fileName}{index}.xml',
    destroy: function() {
        this.setRelationships(null);
        Ext.exporter.file.ooxml.Xml.prototype.destroy.call(this)
    },
    updateFolder: function(a, c) {
        var b = this.getRelationships();
        if (b) {
            b.setFolder(a)
        }
        Ext.exporter.file.ooxml.Xml.prototype.updateFolder.call(this, a, c)
    },
    applyRelationships: function(a) {
        if (!a || a.isRelationships) {
            return a
        }
        return new Ext.exporter.file.ooxml.Relationships(a)
    },
    updateRelationships: function(b, a) {
        Ext.destroy(a)
    },
    updateIndex: function() {
        this.generatePath()
    },
    generateName: function() {
        var a = Ext.XTemplate.getTpl(this, '_nameTemplate');
        this.setName(a ? a.apply(this.getConfig()) : '')
    },
    collectFiles: function(a) {
        this.collectRelationshipsFiles(a);
        a[this.getPath()] = this.render()
    },
    collectRelationshipsFiles: function(b) {
        var a = this.getRelationships(),
            c = this.getFileName();
        if (a) {
            a.setFileName(c ? this.getFileNameFromTemplate() : '');
            a.setParentFolder(this.getFolder());
            a.collectFiles(b)
        }
    },
    collectContentTypes: function(a) {
        a.push(this.getContentType())
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'XmlRels'], 0));
(Ext.cmd.derive('Ext.exporter.file.zip.File', Ext.Base, {
    config: {
        path: '',
        data: null,
        dateTime: null,
        folder: !1
    },
    constructor: function(b) {
        var a = this;
        a.initConfig(b);
        if (!a.getDateTime()) {
            a.setDateTime(new Date())
        }
        return a.callParent([b])
    },
    getId: function() {
        return this.getPath()
    },
    crc32: function(b, a) {
        var h = this.self.crcTable,
            f = 0,
            g = 0,
            e = 0,
            d;
        if (typeof b === "undefined" || !b.length) {
            return 0
        }
        d = (typeof b !== "string");
        if (typeof(a) == "undefined") {
            a = 0
        }
        a = a ^ (-1);
        for (var c = 0, i = b.length; c < i; c++) {
            e = d ? b[c] : b.charCodeAt(c);
            g = (a ^ e) & 255;
            f = h[g];
            a = (a >>> 8) ^ f
        }
        return a ^ (-1)
    },
    getHeader: function(n) {
        var e = this.getData(),
            m = this.getPath(),
            g = Ext.util.Base64._utf8_encode(m),
            l = g !== m,
            f = this.getDateTime(),
            h = '',
            i = '',
            b = Ext.util.Format.decToHex,
            a = '',
            d, c, j, k;
        d = f.getHours();
        d = d << 6;
        d = d | f.getMinutes();
        d = d << 5;
        d = d | f.getSeconds() / 2;
        c = f.getFullYear() - 1980;
        c = c << 4;
        c = c | (f.getMonth() + 1);
        c = c << 5;
        c = c | f.getDate();
        if (l) {
            i = b(1, 1) + b(this.crc32(g), 4) + g;
            h += "up" + b(i.length, 2) + i
        }
        a += "\n\x00";
        a += l ? "\x00\b" : "\x00\x00";
        a += "\x00\x00";
        a += b(d, 2);
        a += b(c, 2);
        a += b(e ? this.crc32(e) : 0, 4);
        a += b(e ? e.length : 0, 4);
        a += b(e ? e.length : 0, 4);
        a += b(g.length, 2);
        a += b(h.length, 2);
        j = "PK\x03\x04" + a + g + h;
        k = "PK\x01\x02\x14\x00" + a + "\x00\x00\x00\x00\x00\x00" + (this.getFolder() === !0 ? "\x10\x00\x00\x00" : "\x00\x00\x00\x00") + b(n, 4) + g + h;
        return {
            fileHeader: j,
            dirHeader: k,
            data: e || ''
        }
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.zip, 'File'], function(e) {
    var a, c = [];
    for (var b = 0; b < 256; b++) {
        a = b;
        for (var d = 0; d < 8; d++) {
            a = ((a & 1) ? (3.988292384E9 ^ (a >>> 1)) : (a >>> 1))
        }
        c[b] = a
    }
    e.crcTable = c
}));
(Ext.cmd.derive('Ext.exporter.file.zip.Folder', Ext.exporter.file.zip.File, {
    folder: !0
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.zip, 'Folder'], 0));
(Ext.cmd.derive('Ext.exporter.file.zip.Archive', Ext.exporter.file.Base, {
    config: {
        folders: [],
        files: []
    },
    destroy: function() {
        this.setFolders(null);
        this.setFiles(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyFolders: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.zip.Folder')
    },
    applyFiles: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.zip.File')
    },
    updateFiles: function(b, c) {
        var a = this;
        if (c) {
            c.un({
                add: a.onFileAdd,
                remove: a.onFileRemove,
                scope: a
            })
        }
        if (b) {
            b.on({
                add: a.onFileAdd,
                remove: a.onFileRemove,
                scope: a
            });
            a.onFileAdd(b, {
                items: b.getRange()
            })
        }
    },
    onFileAdd: function(h, e) {
        var f = this.getFolders(),
            c = e.items,
            g = c.length,
            a, d, b;
        for (a = 0; a < g; a++) {
            d = c[a];
            b = this.getParentFolder(d.getPath());
            if (b) {
                f.add({
                    path: b
                })
            }
        }
    },
    onFileRemove: function(b, a) {
        Ext.destroy(a.items)
    },
    getParentFolder: function(a) {
        var b;
        if (a.slice(-1) == '/') {
            a = a.substring(0, a.length - 1)
        }
        b = a.lastIndexOf('/');
        return (b > 0) ? a.substring(0, b + 1) : ""
    },
    addFile: function(a) {
        return this.getFiles().add(a || {})
    },
    removeFile: function(a) {
        return this.getFiles().remove(a)
    },
    getContent: function() {
        var g = '',
            i = '',
            f = 0,
            h = 0,
            c = Ext.util.Format.decToHex,
            b = [],
            d, j, e, k, a;
        Ext.Array.insert(b, 0, this._folders.items);
        Ext.Array.insert(b, b.length, this._files.items);
        d = b.length;
        for (e = 0; e < d; e++) {
            k = b[e];
            a = k.getHeader(f);
            f += a.fileHeader.length + a.data.length;
            h += a.dirHeader.length;
            g += a.fileHeader + a.data;
            i += a.dirHeader
        }
        j = "PK\x05\x06\x00\x00\x00\x00" + c(d, 2) + c(d, 2) + c(h, 4) + c(f, 4) + "\x00\x00";
        g += i + j;
        return g
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.zip, 'Archive'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Sheet', Ext.exporter.file.ooxml.XmlRels, {
    config: {
        workbook: null
    },
    folder: 'sheet',
    fileName: 'sheet',
    nameTemplate: 'Sheet{index}',
    fileNameTemplate: '{fileName}{index}.xml',
    nameLengthLimit: 31,
    nameRegex: /[\/*?:\[\]]/gi,
    destroy: function() {
        Ext.exporter.file.ooxml.XmlRels.prototype.destroy.call(this);
        this.setWorkbook(null)
    },
    updateIndex: function() {
        if (this._name == null) {
            this.generateName()
        }
        Ext.exporter.file.ooxml.XmlRels.prototype.updateIndex.apply(this, arguments)
    },
    applyName: function(c) {
        var b = this.nameLengthLimit,
            a = Ext.String.trim(String(c || '').replace(this.nameRegex, ''));
        if (a.length > b + 3) {
            a = Ext.String.ellipsis(a, b)
        } else {
            a = a.substr(0, b)
        }
        return Ext.util.Format.htmlEncode(a)
    },
    updateName: function(d) {
        var a = this,
            e = a.getWorkbook(),
            b = !1,
            c;
        if (!a.isNaming && !a.isConfiguring) {
            if (e) {
                c = e.getSheets();
                b = !!c.findBy(function(b) {
                    return (b !== a && b.getName() === d)
                })
            }
            b = b || Ext.isEmpty(d);
            if (b) {
                a.isNaming = !0;
                a.generateName();
                a.isNaming = !1
            }
        }
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Sheet'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Column', Ext.exporter.file.ooxml.Base, {
    config: {
        min: 1,
        max: 1,
        width: 10,
        autoFitWidth: !1,
        hidden: !1,
        styleId: null
    },
    tpl: ['<col ', 'min="{min}" ', 'max="{max}" ', 'width="{width}"', '<tpl if="styleId"> style="{styleId}"</tpl>', '<tpl if="hidden"> hidden="1"</tpl>', '<tpl if="autoFitWidth"> bestFit="1"</tpl>', '<tpl if="width != 10"> customWidth="1"</tpl>', '/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Column'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Cell', Ext.exporter.file.Base, {
    isCell: !0,
    config: {
        row: null,
        dataType: null,
        showPhonetic: null,
        index: null,
        styleId: null,
        mergeAcross: null,
        mergeDown: null,
        value: null,
        serializeDateToNumber: !0
    },
    isMergedCell: !1,
    tpl: ['<c r="{ref}"', '<tpl if="value != null"> t="{dataType}"</tpl>', '<tpl if="showPhonetic"> ph="1"</tpl>', '<tpl if="styleId"> s="{styleId}"</tpl>', '<tpl if="value == null">/><tpl else>><v>{value}</v></c></tpl>'],
    constructor: function(a) {
        var b = a;
        if (a == null || Ext.isDate(a) || Ext.isPrimitive(a)) {
            b = {
                value: a
            }
        }
        return Ext.exporter.file.Base.prototype.constructor.call(this, b)
    },
    destroy: function() {
        this.setRow(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    getRef: function() {
        return this.getNotation(this._index) + this._row._index
    },
    getRenderData: function() {
        var b = this,
            a = {},
            c = b._row && b._row._worksheet,
            d = c && c._workbook;
        a.dataType = b._dataType;
        a.value = b._value;
        a.showPhonetic = b._showPhonetic;
        a.styleId = b._styleId;
        if (this.isMergedCell && c) {
            c.setMergedCellsNo(c._mergedCellsNo + 1)
        }
        if (a.dataType === 's' && d) {
            a.value = d._sharedStrings.addString(a.value)
        }
        a.ref = this.getRef();
        return a
    },
    applyValue: function(a) {
        var c = this,
            b;
        if (a != null) {
            if (typeof a === 'number') {
                b = 'n'
            } else if (typeof a === 'string') {
                b = 's';
                a = Ext.util.Format.stripTags(a)
            } else if (a instanceof Date) {
                if (c.getSerializeDateToNumber()) {
                    b = 'n';
                    a = c.dateValue(a)
                } else {
                    b = 'd';
                    a = Ext.Date.format(a, 'Y-m-d\\TH:i:s.u')
                }
            } else {
                b = 'b'
            }
            c.setDataType(b)
        }
        return a
    },
    updateMergeAcross: function(a) {
        this.isMergedCell = (a || this._mergeDown)
    },
    updateMergeDown: function(a) {
        this.isMergedCell = (a || this._mergeAcross)
    },
    getMergedCellRef: function() {
        var a = this,
            b = a._index,
            c = a._row._index,
            d = a._mergeAcross,
            e = a._mergeDown,
            f = a.getNotation(b) + c + ':';
        if (d) {
            b += d
        }
        if (e) {
            c += e
        }
        f += a.getNotation(b) + c;
        return f
    },
    getNotation: function(a) {
        var e = 65,
            d = 26,
            c = String.fromCharCode,
            f, b;
        if (a <= 0) {
            a = 1
        }
        b = Math.floor(a / d);
        f = a % d;
        if (b === 0 || a === d) {
            return c(e + a - 1)
        } else if (f === 0) {
            return this.getNotation(b - 1) + 'Z'
        } else if (b < d) {
            return c(e + b - 1) + c(e + f - 1)
        } else {
            return this.getNotation(b) + c(e + f - 1)
        }
    },
    dateValue: function(a) {
        return 25569 + ((a.getTime() - (a.getTimezoneOffset() * 60 * 1000)) / (1000 * 60 * 60 * 24))
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Cell'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Row', Ext.exporter.file.Base, {
    config: {
        collapsed: null,
        hidden: null,
        height: null,
        outlineLevel: null,
        showPhonetic: null,
        index: null,
        styleId: null,
        worksheet: null,
        cells: [],
        cachedCells: null
    },
    tpl: ['<row', '<tpl if="index"> r="{index}"</tpl>', '<tpl if="collapsed"> collapsed="{collapsed}"</tpl>', '<tpl if="hidden"> hidden="1"</tpl>', '<tpl if="height"> ht="{height}" customHeight="1"</tpl>', '<tpl if="outlineLevel"> outlineLevel="{outlineLevel}"</tpl>', '<tpl if="styleId"> s="{styleId}" customFormat="1"</tpl>', '<tpl if="cachedCells">', '>{cachedCells}</row>', '<tpl elseif="cells && cells.length">', '><tpl for="cells.items">{[values.render()]}</tpl></row>', '<tpl else>', '/>', '</tpl>'],
    lastCellIndex: 1,
    constructor: function(a) {
        var b = a;
        if (Ext.isArray(a)) {
            b = {
                cells: a
            }
        }
        return Ext.exporter.file.Base.prototype.constructor.call(this, b)
    },
    destroy: function() {
        this.setWorksheet(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyCells: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Cell')
    },
    updateCells: function(b, c) {
        var a = this;
        if (c) {
            b.un({
                add: a.onCellAdd,
                remove: a.onCellRemove,
                scope: a
            })
        }
        if (b) {
            b.on({
                add: a.onCellAdd,
                remove: a.onCellRemove,
                scope: a
            });
            a.onCellAdd(b, {
                items: b.getRange()
            })
        }
    },
    onCellAdd: function(e, f) {
        var d = f.items,
            g = d.length,
            b, a, c;
        for (b = 0; b < g; b++) {
            a = d[b];
            a.setRow(this);
            c = a._index;
            if (!c) {
                a.setIndex(this.lastCellIndex++)
            } else {
                this.lastCellIndex = Math.max(e.length, c) + 1
            }
        }
    },
    onCellRemove: function(b, a) {
        Ext.destroy(a.items);
        this.updateCellIndexes()
    },
    addCell: function(a) {
        if (!this._cells) {
            this.setCells([])
        }
        return this._cells.add(a || {})
    },
    getCell: function(a) {
        return this._cells ? this._cells.get(a) : null
    },
    beginCellRendering: function() {
        var a = this;
        a.tempCells = [];
        a.startCaching = !0;
        a.lastCellIndex = 1;
        if (!a.cachedCell) {
            a.cachedCell = new Ext.exporter.file.ooxml.excel.Cell({
                row: a
            });
            a.cachedCellConfig = a.cachedCell.getConfig();
            a.cachedCellConfig.id = null
        }
    },
    endCellRendering: function() {
        var a = this;
        a.setCachedCells(a.tempCells.join(''));
        a.tempCells = null;
        a.startCaching = !1;
        a.lastCellIndex = 1
    },
    renderCells: function(i) {
        var c = this,
            d = {
                first: null,
                last: null,
                row: '',
                merged: ''
            },
            j = i.length,
            h = [],
            e, f, b, a, g;
        c.beginCellRendering();
        a = c.cachedCell;
        for (e = 0; e < j; e++) {
            f = i[e] || {};
            if (typeof f === 'object' && !(f instanceof Date)) {
                b = f
            } else {
                b = {
                    value: f
                }
            }
            Ext.applyIf(b, c.cachedCellConfig);
            a.setValue(b.value);
            a.setShowPhonetic(b.showPhonetic);
            a.setStyleId(b.styleId);
            a.setMergeAcross(b.mergeAcross);
            a.setMergeDown(b.mergeDown);
            a.setIndex(b.index);
            g = a.getIndex();
            if (!g) {
                a.setIndex(c.lastCellIndex++)
            } else {
                c.lastCellIndex = Math.max(c.lastCellIndex, g) + 1
            }
            if (e === 0) {
                d.first = d.last = a.getRef()
            } else if (e === j - 1) {
                d.last = a.getRef()
            }
            c.tempCells.push(a.render());
            if (a.isMergedCell) {
                h.push('<mergeCell ref="' + a.getMergedCellRef() + '"/>')
            }
        }
        c.endCellRendering();
        d.row = c.render();
        d.merged = h.join('');
        return d
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Row'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Location', Ext.exporter.file.ooxml.Base, {
    config: {
        ref: null,
        firstHeaderRow: null,
        firstDataRow: null,
        firstDataCol: null,
        rowPageCount: null,
        colPageCount: null
    },
    generateTplAttributes: !0,
    tpl: ['<location {attributes}/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Location'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.FieldItem', Ext.exporter.file.ooxml.Base, {
    config: {
        c: null,
        d: null,
        e: null,
        f: null,
        h: null,
        m: null,
        n: null,
        s: null,
        sd: null,
        t: null,
        x: null
    },
    generateTplAttributes: !0,
    tpl: ['<item {attributes}/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'FieldItem'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotAreaReference', Ext.exporter.file.ooxml.Base, {
    config: {
        avgSubtotal: null,
        byPosition: null,
        count: null,
        countASubtotal: null,
        countSubtotal: null,
        defaultSubtotal: null,
        field: null,
        maxSubtotal: null,
        minSubtotal: null,
        productSubtotal: null,
        relative: null,
        selected: null,
        stdDevPSubtotal: null,
        stdDevSubtotal: null,
        sumSubtotal: null,
        varPSubtotal: null,
        varSubtotal: null,
        items: []
    },
    tplNonAttributes: ['items'],
    generateTplAttributes: !0,
    tpl: ['<reference {attributes}>', '<tpl if="items"><tpl for="items"><x v="{.}"/></tpl></tpl>', '</reference>'],
    getCount: function() {
        return this.getItems().length
    },
    applyItems: function(a) {
        return a !== null ? Ext.Array.from(a) : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotAreaReference'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotArea', Ext.exporter.file.ooxml.Base, {
    config: {
        axis: null,
        cacheIndex: null,
        collapsedLevelsAreSubtotals: null,
        dataOnly: null,
        field: null,
        fieldPosition: null,
        grandCol: null,
        grandRow: null,
        labelOnly: null,
        offset: null,
        outline: null,
        type: null,
        references: null
    },
    tplNonAttributes: ['references'],
    generateTplAttributes: !0,
    tpl: ['<pivotArea {attributes}>', '<tpl if="references"><references count="{references.length}"><tpl for="references.getRange()">{[values.render()]}</tpl></references></tpl>', '</pivotArea>'],
    applyReferences: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.PivotAreaReference')
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotArea'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.AutoSortScope', Ext.exporter.file.ooxml.Base, {
    config: {
        pivotArea: {}
    },
    tpl: ['<autoSortScope>{[values.pivotArea.render()]}</autoSortScope>'],
    destroy: function() {
        this.setPivotArea(null);
        Ext.exporter.file.ooxml.Base.prototype.destroy.call(this)
    },
    applyPivotArea: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.PivotArea(a)
    },
    updatePivotArea: function(b, a) {
        Ext.destroy(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'AutoSortScope'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotField', Ext.exporter.file.ooxml.Base, {
    config: {
        allDrilled: null,
        autoShow: null,
        avgSubtotal: null,
        axis: null,
        compact: null,
        countASubtotal: null,
        countSubtotal: null,
        dataField: null,
        dataSourceSort: null,
        defaultAttributeDrillState: null,
        defaultSubtotal: null,
        dragOff: null,
        dragToCol: null,
        dragToData: null,
        dragToPage: null,
        dragToRow: null,
        hiddenLevel: null,
        hideNewItems: null,
        includeNewItemsInFilter: null,
        insertBlankRow: null,
        insertPageBreak: null,
        itemPageCount: null,
        maxSubtotal: null,
        measureFilter: null,
        minSubtotal: null,
        multipleItemSelectionAllowed: null,
        nonAutoSortDefault: null,
        numFmtId: null,
        outline: null,
        productSubtotal: null,
        rankBy: null,
        serverField: null,
        showAll: null,
        showDropDowns: null,
        showPropAsCaption: null,
        showPropCell: null,
        showPropTip: null,
        sortType: null,
        stdDevPSubtotal: null,
        stdDevSubtotal: null,
        subtotalCaption: null,
        subtotalTop: null,
        sumSubtotal: null,
        topAutoShow: null,
        uniqueMemberProperty: null,
        varPSubtotal: null,
        varSubtotal: null,
        items: null,
        autoSortScope: null
    },
    tplNonAttributes: ['items', 'autoSortScope'],
    generateTplAttributes: !0,
    tpl: ['<tpl if="items || autoSortScope">', '<pivotField {attributes}>', '<tpl if="items"><items count="{items.length}"><tpl for="items.getRange()">{[values.render()]}</tpl></items></tpl>', '<tpl if="autoSortScope">{[values.autoSortScope.render()]}</tpl>', '</pivotField>', '<tpl else>', '<pivotField {attributes} />', '</tpl>'],
    destroy: function() {
        this.setAutoSortScope(null);
        Ext.exporter.file.ooxml.Base.prototype.destroy.call(this)
    },
    applyItems: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.FieldItem')
    },
    applyAutoSortScope: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.AutoSortScope(a)
    },
    updateAutoSortScope: function(b, a) {
        Ext.destroy(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotField'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Field', Ext.exporter.file.ooxml.Base, {
    config: {
        x: null
    },
    tpl: ['<field x="{x}"/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Field'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Item', Ext.exporter.file.ooxml.Base, {
    config: {
        i: null,
        r: null,
        t: null,
        x: null
    },
    tpl: ['<tpl if="x"><i{attr}>{x}</i><tpl else><i{attr}/></tpl>'],
    getRenderData: function() {
        var a = Ext.exporter.file.ooxml.Base.prototype.getRenderData.call(this),
            e = a.x ? a.x.length : 0,
            d = '',
            c = '',
            b;
        for (b = 0; b < e; b++) {
            if (a.x[b] > 0) {
                d += '<x v="' + a.x[b] + '"/>'
            } else {
                d += '<x/>'
            }
        }
        a.x = d;
        if (a.t) {
            c += ' t="' + a.t + '"'
        }
        if (a.r > 0) {
            c += ' r="' + a.r + '"'
        }
        if (a.i > 0) {
            c += ' i="' + a.i + '"'
        }
        a.attr = c;
        return a
    },
    applyX: function(a) {
        return a != null ? Ext.Array.from(a) : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Item'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.DataField', Ext.exporter.file.ooxml.Base, {
    config: {
        baseField: null,
        baseItem: null,
        fld: null,
        name: null,
        numFmtId: null,
        showDataAs: null,
        subtotal: null
    },
    generateTplAttributes: !0,
    tpl: ['<dataField {attributes}/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'DataField'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Record', Ext.exporter.file.ooxml.Base, {
    config: {
        items: null
    },
    tplNonAttributes: ['items', 'stritems'],
    tpl: ['<tpl if="stritems">', '<r>', '{stritems}', '</r>', '<tpl else>', '<r/>', '</tpl>'],
    numberTpl: '<n v="{0}"/>',
    booleanTpl: '<b v="{0}"/>',
    stringTpl: '<s v="{0}"/>',
    dateTpl: '<d v="{0}"/>',
    constructor: function(a) {
        var b;
        if (Ext.isArray(a) || Ext.isDate(a) || Ext.isPrimitive(a)) {
            b = {
                items: a
            }
        } else {
            b = a
        }
        return Ext.exporter.file.ooxml.Base.prototype.constructor.call(this, b)
    },
    getRenderData: function() {
        var d = this,
            g = Ext.exporter.file.ooxml.Base.prototype.getRenderData.call(this),
            f = g.items,
            i = '',
            c = [],
            e, h, a, b;
        if (f) {
            h = f.length;
            for (e = 0; e < h; e++) {
                a = f[e];
                if (a == null || a === '') {} else {
                    if (typeof a === 'string') {
                        b = d.stringTpl;
                        a = Ext.util.Base64._utf8_encode(Ext.util.Format.htmlEncode(a));
                        c.push('s')
                    } else if (typeof a === 'boolean') {
                        b = d.booleanTpl;
                        c.push('b')
                    } else if (typeof a === 'number') {
                        b = d.numberTpl;
                        c.push('n')
                    } else if (a instanceof Date) {
                        b = d.dateTpl;
                        a = Ext.Date.format(a, 'Y-m-d\\TH:i:s.u');
                        c.push('d')
                    }
                    i += Ext.String.format(b, a)
                }
            }
        }
        g.stritems = i;
        return g
    },
    applyItems: function(a) {
        return a !== null ? Ext.Array.from(a) : null
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Record'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotCacheRecords', Ext.exporter.file.ooxml.XmlRels, {
    config: {
        items: []
    },
    folder: '/xl/pivotCache/',
    fileName: 'pivotCacheRecords',
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheRecords+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/pivotCacheRecords'
    },
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<pivotCacheRecords xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" ', 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" count="{items.length}">', '<tpl for="items.getRange()">{[values.render()]}</tpl>', '</pivotCacheRecords>'],
    applyItems: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Record')
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotCacheRecords'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.WorksheetSource', Ext.exporter.file.ooxml.Base, {
    config: {
        id: null,
        name: null,
        ref: null,
        sheet: null
    },
    autoGenerateId: !1,
    tplAttributes: ['id', 'name', 'ref', 'sheet'],
    generateTplAttributes: !0,
    tpl: ['<worksheetSource {attributes} />']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'WorksheetSource'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.CacheSource', Ext.exporter.file.ooxml.Base, {
    config: {
        type: 'worksheet',
        worksheetSource: {}
    },
    tplNonAttributes: ['worksheetSource'],
    generateTplAttributes: !0,
    tpl: ['<cacheSource {attributes}>', '<tpl if="type == \'worksheet\'">', '{[values.worksheetSource.render()]}', '</tpl>', '</cacheSource>'],
    destroy: function() {
        this.setWorksheetSource(null);
        Ext.exporter.file.ooxml.Base.prototype.destroy.call(this)
    },
    applyWorksheetSource: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.WorksheetSource(a)
    },
    updateWorksheetSource: function(b, a) {
        Ext.destroy(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'CacheSource'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.SharedItems', Ext.exporter.file.ooxml.Base, {
    config: {
        containsBlank: null,
        containsDate: null,
        containsInteger: null,
        containsMixedTypes: null,
        containsNonDate: null,
        containsNumber: null,
        containsSemiMixedTypes: null,
        containsString: null,
        longText: null,
        maxDate: null,
        maxValue: null,
        minDate: null,
        minValue: null,
        items: null
    },
    tplNonAttributes: ['items', 'stritems'],
    generateTplAttributes: !0,
    tpl: ['<tpl if="stritems">', '<sharedItems {attributes}>', '{stritems}', '</sharedItems>', '<tpl else>', '<sharedItems {attributes}/>', '</tpl>'],
    numberTpl: '<n v="{0}"/>',
    booleanTpl: '<b v="{0}"/>',
    stringTpl: '<s v="{0}"/>',
    dateTpl: '<d v="{0}"/>',
    getRenderData: function() {
        var g = this,
            b = Ext.exporter.file.ooxml.Base.prototype.getRenderData.call(this),
            l = b.items,
            p = '',
            q = !1,
            r = !1,
            m = !1,
            o = !1,
            f = !1,
            n = !1,
            k = 0,
            c = [],
            j = null,
            i = null,
            h, d, a, e;
        if (l) {
            d = l.length;
            for (h = 0; h < d; h++) {
                a = l[h];
                if (a == null || a === '') {
                    q = !0
                } else {
                    k++;
                    if (typeof a === 'string') {
                        f = !0;
                        e = g.stringTpl;
                        a = Ext.util.Base64._utf8_encode(Ext.util.Format.htmlEncode(a));
                        c.push('s')
                    } else if (typeof a === 'boolean') {
                        r = !0;
                        e = g.booleanTpl;
                        c.push('b')
                    } else if (typeof a === 'number') {
                        m = !0;
                        e = g.numberTpl;
                        j = Math.min(j, a);
                        i = Math.max(i, a);
                        if (String(a).indexOf('.') >= 0) {
                            n = !0
                        }
                        c.push('n')
                    } else if (a instanceof Date) {
                        o = !0;
                        e = g.dateTpl;
                        a = Ext.Date.format(a, 'Y-m-d\\TH:i:s.u');
                        c.push('d')
                    }
                    p += Ext.String.format(e, a)
                }
            }
        }
        if (k > 0) {
            b.count = k
        }
        b.stritems = p;
        if (o) {
            b.containsSemiMixedTypes = f;
            b.containsDate = !0;
            b.stritems = ''
        }
        if (m) {
            b.containsSemiMixedTypes = f;
            b.containsNumber = !0;
            b.minValue = j;
            b.maxValue = i;
            if (!n) {
                b.containsInteger = !0
            }
        }
        b.containsString = f;
        d = Ext.Array.unique(c);
        if (d > 0) {
            b.containsMixedTypes = d > 1
        }
        return b
    },
    applyItems: function(a) {
        return a !== null ? Ext.Array.from(a) : null
    },
    updateMinValue: function(a) {
        if (a != null) {
            this.setContainsNumber(!0)
        }
    },
    updateMaxValue: function(a) {
        if (a != null) {
            this.setContainsNumber(!0)
        }
    },
    applyMinDate: function(a) {
        if (a) {
            a = Ext.Date.format(a, 'Y-m-d\\TH:i:s.u')
        }
        return a
    },
    updateMinDate: function(a) {
        if (a != null) {
            this.setContainsDate(!0)
        }
    },
    applyMaxDate: function(a) {
        if (a) {
            a = Ext.Date.format(a, 'Y-m-d\\TH:i:s.u')
        }
        return a
    },
    updateMaxDate: function(a) {
        if (a != null) {
            this.setContainsDate(!0)
        }
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'SharedItems'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.CacheField', Ext.exporter.file.ooxml.Base, {
    config: {
        caption: null,
        databaseField: null,
        formula: null,
        hierarchy: null,
        level: null,
        mappingCount: null,
        memberPropertyField: null,
        name: null,
        numFmtId: null,
        propertyName: null,
        serverField: null,
        sqlType: null,
        uniqueList: null,
        sharedItems: {},
        fieldGroup: null,
        mpMap: null
    },
    tplNonAttributes: ['sharedItems', 'fieldGroup', 'mpMap'],
    generateTplAttributes: !0,
    tpl: ['<cacheField {attributes}>', '<tpl if="sharedItems">{[values.sharedItems.render()]}</tpl>', '</cacheField>'],
    destroy: function() {
        this.setSharedItems(null);
        Ext.exporter.file.ooxml.Base.prototype.destroy.call(this)
    },
    applySharedItems: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.SharedItems(a)
    },
    updateSharedItems: function(b, a) {
        Ext.destroy(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'CacheField'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotCache', Ext.exporter.file.ooxml.Base, {
    config: {
        id: null,
        cacheId: null
    },
    autoGenerateId: !1,
    tpl: ['<pivotCache cacheId="{cacheId}" r:id="{id}"/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotCache'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotCacheDefinition', Ext.exporter.file.ooxml.XmlRels, {
    config: {
        backgroundQuery: null,
        createdVersion: null,
        enableRefresh: null,
        invalid: null,
        minRefreshableVersion: null,
        missingItemsLimit: null,
        optimizeMemory: null,
        recordCount: null,
        refreshedBy: null,
        refreshedDateIso: null,
        refreshedVersion: null,
        refreshOnLoad: null,
        saveData: null,
        supportAdvancedDrill: null,
        supportSubquery: null,
        tupleCache: null,
        upgradeOnRefresh: null,
        cacheRecords: {},
        cacheSource: {},
        cacheFields: null,
        pivotCache: {}
    },
    folder: '/xl/pivotCache/',
    fileName: 'pivotCacheDefinition',
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheDefinition+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/pivotCacheDefinition'
    },
    tplNonAttributes: ['cacheRecords', 'cacheSource', 'cacheFields', 'pivotCache'],
    generateTplAttributes: !0,
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<pivotCacheDefinition xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" ', 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:id="{[values.relationship.getId()]}" {attributes}>', '{[values.cacheSource.render()]}', '<tpl if="cacheFields"><cacheFields count="{cacheFields.length}"><tpl for="cacheFields.getRange()">{[values.render()]}</tpl></cacheFields></tpl>', '</pivotCacheDefinition>'],
    destroy: function() {
        this.setCacheRecords(null);
        this.setCacheSource(null);
        this.setPivotCache(null);
        Ext.exporter.file.ooxml.XmlRels.prototype.destroy.call(this)
    },
    getRenderData: function() {
        var b = Ext.exporter.file.ooxml.XmlRels.prototype.getRenderData.call(this),
            a = this.getCacheRecords();
        if (a) {
            a = a.getItems();
            b.recordCount = a.length
        }
        return b
    },
    collectFiles: function(b) {
        var a = this.getCacheRecords();
        if (a) {
            a.collectFiles(b)
        }
        Ext.exporter.file.ooxml.XmlRels.prototype.collectFiles.call(this, b)
    },
    collectContentTypes: function(b) {
        var a = this.getCacheRecords();
        if (a) {
            a.collectContentTypes(b)
        }
        Ext.exporter.file.ooxml.XmlRels.prototype.collectContentTypes.call(this, b)
    },
    updateIndex: function(b, c) {
        var a = this.getCacheRecords();
        if (a) {
            a.setIndex(b)
        }
        Ext.exporter.file.ooxml.XmlRels.prototype.updateIndex.call(this, b, c)
    },
    applyPivotCache: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.PivotCache(a)
    },
    updatePivotCache: function(b, a) {
        Ext.destroy(a)
    },
    applyCacheRecords: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.PivotCacheRecords(a)
    },
    updateCacheRecords: function(c, a) {
        var d = this.getRelationships(),
            b;
        if (a) {
            d.removeRelationship(a.getRelationship())
        }
        Ext.destroy(a);
        if (c) {
            b = c.getRelationship();
            d.addRelationship(b);
            this.setId(b.getId())
        }
    },
    applyCacheSource: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.CacheSource(a)
    },
    updateCacheSource: function(b, a) {
        Ext.destroy(a)
    },
    applyCacheFields: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.CacheField')
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotCacheDefinition'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotTableStyleInfo', Ext.exporter.file.ooxml.Base, {
    config: {
        name: 'PivotStyleLight2',
        showColHeaders: !0,
        showColStripes: null,
        showLastColumn: !0,
        showRowHeaders: !0,
        showRowStripes: null
    },
    generateTplAttributes: !0,
    tpl: ['<pivotTableStyleInfo {attributes}/>']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotTableStyleInfo'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.PivotTable', Ext.exporter.file.ooxml.XmlRels, {
    config: {
        applyAlignmentFormats: !1,
        applyBorderFormats: !1,
        applyFontFormats: !1,
        applyNumberFormats: !1,
        applyPatternFormats: !1,
        applyWidthHeightFormats: !0,
        asteriskTotals: null,
        autoFormatId: 4096,
        cacheId: null,
        chartFormat: null,
        colGrandTotals: null,
        colHeaderCaption: null,
        compact: !1,
        compactData: !1,
        createdVersion: null,
        customListSort: null,
        dataCaption: 'Values',
        dataOnRows: null,
        dataPosition: null,
        disableFieldList: null,
        editData: null,
        enableDrill: null,
        enableFieldProperties: null,
        enableWizard: null,
        errorCaption: null,
        fieldListSortAscending: null,
        fieldPrintTitles: null,
        grandTotalCaption: null,
        gridDropZones: null,
        immersive: null,
        indent: null,
        itemPrintTitles: !0,
        mdxSubqueries: null,
        mergeItem: null,
        minRefreshableVersion: null,
        missingCaption: null,
        multipleFieldFilters: !1,
        name: null,
        outline: !0,
        outlineData: null,
        pageOverThenDown: null,
        pageStyle: null,
        pageWrap: null,
        pivotTableStyle: null,
        preserveFormatting: null,
        printDrill: null,
        published: null,
        rowGrandTotals: null,
        rowHeaderCaption: null,
        showCalcMbrs: null,
        showDataDropDown: null,
        showDataTips: null,
        showDrill: null,
        showDropZones: null,
        showEmptyCol: null,
        showEmptyRow: null,
        showError: null,
        showHeaders: null,
        showItems: null,
        showMemberPropertyTips: null,
        showMissing: null,
        showMultipleLabel: null,
        subtotalHiddenItems: null,
        tag: null,
        updatedVersion: null,
        useAutoFormatting: !0,
        vacatedStyle: null,
        visualTotals: null,
        location: {},
        pivotFields: null,
        rowFields: null,
        rowItems: null,
        colFields: null,
        colItems: null,
        pageFields: null,
        dataFields: null,
        pivotTableStyleInfo: {},
        worksheet: null,
        cacheDefinition: {},
        viewLayoutType: 'outline'
    },
    folder: '/xl/pivotTables/',
    fileName: 'pivotTable',
    nameTemplate: 'PivotTable{index}',
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pivotTable+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/pivotTable'
    },
    tplNonAttributes: ['location', 'worksheet', 'cacheDefinition', 'pivotFields', 'rowFields', 'rowItems', 'colFields', 'colItems', 'pageFields', 'dataFields', 'pivotTableStyleInfo', 'viewLayoutType'],
    generateTplAttributes: !0,
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<pivotTableDefinition xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" {attributes}>', '{[values.location.render()]}', '<tpl if="pivotFields && pivotFields.length"><pivotFields count="{pivotFields.length}"><tpl for="pivotFields.getRange()">{[values.render()]}</tpl></pivotFields></tpl>', '<tpl if="rowFields && rowFields.length"><rowFields count="{rowFields.length}"><tpl for="rowFields.getRange()">{[values.render()]}</tpl></rowFields></tpl>', '<tpl if="rowItems && rowItems.length"><rowItems count="{rowItems.length}"><tpl for="rowItems.getRange()">{[values.render()]}</tpl></rowItems></tpl>', '<tpl if="colFields && colFields.length"><colFields count="{colFields.length}"><tpl for="colFields.getRange()">{[values.render()]}</tpl></colFields></tpl>', '<tpl if="colItems && colItems.length"><colItems count="{colItems.length}"><tpl for="colItems.getRange()">{[values.render()]}</tpl></colItems></tpl>', '<tpl if="pageFields && pageFields.length"><pageFields count="{pageFields.length}"><tpl for="pageFields.getRange()">{[values.render()]}</tpl></pageFields></tpl>', '<tpl if="dataFields && dataFields.length"><dataFields count="{dataFields.length}"><tpl for="dataFields.getRange()">{[values.render()]}</tpl></dataFields></tpl>', '<tpl if="pivotTableStyleInfo">{[values.pivotTableStyleInfo.render()]}</tpl>', '</pivotTableDefinition>'],
    destroy: function() {
        var a = this;
        a.setWorksheet(null);
        a.setLocation(null);
        a.setCacheDefinition(null);
        a.setPivotTableStyleInfo(null);
        Ext.exporter.file.ooxml.XmlRels.prototype.destroy.call(this)
    },
    collectFiles: function(a) {
        this.getCacheDefinition().collectFiles(a);
        Ext.exporter.file.ooxml.XmlRels.prototype.collectFiles.call(this, a)
    },
    collectContentTypes: function(a) {
        this.getCacheDefinition().collectContentTypes(a);
        Ext.exporter.file.ooxml.XmlRels.prototype.collectContentTypes.call(this, a)
    },
    updateIndex: function(c, d) {
        var a = this,
            b = a.getCacheDefinition();
        if (b) {
            b.setIndex(c)
        }
        if (a._name == null) {
            a.generateName()
        }
        Ext.exporter.file.ooxml.XmlRels.prototype.updateIndex.call(this, c, d)
    },
    updateWorksheet: function(e, b) {
        var a = this.getCacheDefinition(),
            d, c;
        if (b && a && b.getWorkbook() && b.getWorkbook().getRelationships()) {
            b.getWorkbook().getRelationships().removeRelationship(a.getRelationship())
        }
        if (e && a) {
            d = e.getWorkbook();
            d.getRelationships().addRelationship(a.getRelationship());
            c = a.getPivotCache();
            d.addPivotCache(c);
            this.setCacheId(c.getCacheId());
            c.setId(a.getRelationship().getId())
        }
    },
    applyPivotTableStyleInfo: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.PivotTableStyleInfo(a)
    },
    updatePivotTableStyleInfo: function(b, a) {
        Ext.destroy(a)
    },
    applyCacheDefinition: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.PivotCacheDefinition(a)
    },
    updateCacheDefinition: function(b, a) {
        var c = this.getRelationships();
        if (a) {
            c.removeRelationship(a.getRelationship())
        }
        Ext.destroy(a);
        if (b) {
            c.addRelationship(b.getRelationship())
        }
    },
    updateViewLayoutType: function(b) {
        var a = this;
        if (b === 'compact') {
            a.setOutline(!0);
            a.setOutlineData(!0);
            a.setCompact(null);
            a.setCompactData(null)
        } else if (b === 'outline') {
            a.setOutline(!0);
            a.setOutlineData(!0);
            a.setCompact(!1);
            a.setCompactData(!1)
        } else {
            a.setOutline(null);
            a.setOutlineData(null);
            a.setCompact(!1);
            a.setCompactData(!1)
        }
        a.processPivotFields(a.getPivotFields().getRange())
    },
    applyLocation: function(a) {
        if (!a || a.isInstance) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.Location(a)
    },
    updateLocation: function(b, a) {
        Ext.destroy(a)
    },
    applyPivotFields: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.PivotField')
    },
    updatePivotFields: function(b, c) {
        var a = this;
        if (c) {
            c.un({
                add: a.onPivotFieldAdd,
                scope: a
            })
        }
        if (b) {
            b.on({
                add: a.onPivotFieldAdd,
                scope: a
            });
            this.processPivotFields(b.getRange())
        }
    },
    onPivotFieldAdd: function(b, a) {
        this.processPivotFields(a.items)
    },
    processPivotFields: function(f) {
        var e = this.getViewLayoutType(),
            g = f.length,
            c, d, a, b;
        if (e === 'compact') {
            a = null;
            b = null
        } else if (e === 'outline') {
            a = !1;
            b = null
        } else {
            a = !1;
            b = !1
        }
        for (c = 0; c < g; c++) {
            d = f[c];
            d.setCompact(a);
            d.setOutline(b)
        }
    },
    applyRowFields: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Field')
    },
    applyRowItems: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Item')
    },
    applyColFields: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Field')
    },
    applyColItems: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Item')
    },
    applyDataFields: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.DataField')
    },
    applyAutoFormatId: function(a) {
        return (a >= 4096 && a <= 4117) ? a : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'PivotTable'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Worksheet', Ext.exporter.file.ooxml.excel.Sheet, {
    isWorksheet: !0,
    config: {
        columns: null,
        rows: [],
        drawings: null,
        tables: null,
        mergeCells: null,
        mergedCellsNo: 0,
        topLeftRef: null,
        bottomRightRef: null,
        cachedRows: '',
        cachedMergeCells: '',
        pivotTables: null
    },
    folder: '/xl/worksheets/',
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet'
    },
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" ', 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">', '<tpl if="columns">', '<cols>', '<tpl for="columns.items">{[values.render()]}</tpl>', '</cols>', '</tpl>', '<tpl if="cachedRows">', '<sheetData>{cachedRows}</sheetData>', '<tpl if="cachedMergeCells"><mergeCells>{cachedMergeCells}</mergeCells></tpl>', '<tpl elseif="rows">', '<sheetData><tpl for="rows.items">{[values.render()]}</tpl></sheetData>', '<tpl if="values.self.getMergedCellsNo() &gt; 0">', '<mergeCells>', '<tpl for="rows.items">', '<tpl for="_cells.items">', '<tpl if="isMergedCell"><mergeCell ref="{[values.getMergedCellRef()]}"/></tpl>', '</tpl>', '</tpl>', '</mergeCells>', '</tpl>', '<tpl else>', '</sheetData>', '</tpl>', '</worksheet>'],
    lastRowIndex: 1,
    destroy: function() {
        var a = this;
        Ext.destroy(a.cachedRow);
        a.cachedRow = a.cachedRowConfig = null;
        Ext.exporter.file.ooxml.excel.Sheet.prototype.destroy.call(this)
    },
    getRenderData: function() {
        var a = this,
            b = a.getName();
        if (Ext.isEmpty(b)) {
            a.generateName()
        }
        a.setMergedCellsNo(0);
        return Ext.exporter.file.ooxml.excel.Sheet.prototype.getRenderData.call(this)
    },
    collectFiles: function(d) {
        var b = this.getPivotTables(),
            c, a;
        if (b) {
            c = b.length;
            for (a = 0; a < c; a++) {
                b.getAt(a).collectFiles(d)
            }
        }
        Ext.exporter.file.ooxml.excel.Sheet.prototype.collectFiles.call(this, d)
    },
    collectContentTypes: function(d) {
        var b = this.getPivotTables(),
            c, a;
        if (b) {
            c = b.length;
            for (a = 0; a < c; a++) {
                b.getAt(a).collectContentTypes(d)
            }
        }
        Ext.exporter.file.ooxml.excel.Sheet.prototype.collectContentTypes.call(this, d)
    },
    applyColumns: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Column')
    },
    applyRows: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Row')
    },
    updateRows: function(b, c) {
        var a = this;
        if (c) {
            c.un({
                add: a.onRowAdd,
                remove: a.onRowRemove,
                scope: a
            })
        }
        if (b) {
            b.on({
                add: a.onRowAdd,
                remove: a.onRowRemove,
                scope: a
            });
            a.onRowAdd(b, {
                items: b.getRange()
            })
        }
    },
    onRowAdd: function(e, f) {
        var d = f.items,
            g = d.length,
            b, a, c;
        for (b = 0; b < g; b++) {
            a = d[b];
            a.setWorksheet(this);
            c = a._index;
            if (!c) {
                a.setIndex(this.lastRowIndex++)
            } else {
                this.lastRowIndex = Math.max(e.length, c) + 1
            }
        }
    },
    onRowRemove: function(b, a) {
        Ext.destroy(a.items)
    },
    updateItemIndexes: function(b) {
        var a, d, c;
        if (!b) {
            return
        }
        d = b.length;
        for (a = 0; a < d; a++) {
            c = b.getAt(a);
            if (!c.getIndex()) {
                c.setIndex(a + 1)
            }
        }
    },
    updateDrawings: function(b) {
        var a = this.getRelationships();
        if (oldData && a) {
            a.removeRelationship(oldData.getRelationship())
        }
        if (b && a) {
            a.addRelationship(b.getRelationship())
        }
    },
    updateTables: function(b) {
        var a = this.getRelationships();
        if (oldData && a) {
            a.removeRelationship(oldData.getRelationship())
        }
        if (b && a) {
            a.addRelationship(b.getRelationship())
        }
    },
    applyPivotTables: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.PivotTable')
    },
    updatePivotTables: function(b, c) {
        var a = this;
        if (c) {
            c.un({
                add: a.onPivotTableAdd,
                remove: a.onPivotTableRemove,
                scope: a
            })
        }
        if (b) {
            b.on({
                add: a.onPivotTableAdd,
                remove: a.onPivotTableRemove,
                scope: a
            });
            this.processPivotTables(b.getRange())
        }
    },
    onPivotTableAdd: function(b, a) {
        this.processPivotTables(a.items)
    },
    processPivotTables: function(c) {
        var e = this.getRelationships(),
            d = c.length,
            a, b;
        for (a = 0; a < d; a++) {
            b = c[a];
            e.addRelationship(b.getRelationship());
            b.setWorksheet(this)
        }
        this.updateItemIndexes(this.getPivotTables())
    },
    onPivotTableRemove: function(f, c) {
        var e = this.getRelationships(),
            d = c.items.length,
            a, b;
        for (a = 0; a < d; a++) {
            b = c.items[a];
            e.removeRelationship(b.getRelationship());
            Ext.destroy(b)
        }
    },
    addColumn: function(a) {
        if (!this._columns) {
            this.setColumns([])
        }
        return this._columns.add(a || {})
    },
    addRow: function(a) {
        if (!this._rows) {
            this.setRows([])
        }
        return this._rows.add(a || {})
    },
    getRow: function(a) {
        return this._rows ? this._rows.get(a) : null
    },
    addPivotTable: function(a) {
        if (!this._pivotTables) {
            this.setPivotTables([])
        }
        return this._pivotTables.add(a || {})
    },
    getPivotTable: function(a) {
        return this._pivotTables ? this._pivotTables.get(a) : null
    },
    beginRowRendering: function() {
        var a = this;
        a.tempRows = [];
        a.tempMergeCells = [];
        a.startCaching = !0;
        a.setMergedCellsNo(0);
        a.lastRowIndex = 1;
        a.cachedIndex = 0;
        if (!a.cachedRow) {
            a.cachedRow = new Ext.exporter.file.ooxml.excel.Row({
                worksheet: a
            });
            a.cachedRowConfig = a.cachedRow.getConfig();
            a.cachedRowConfig.id = a.cachedRowConfig.cells = null
        }
    },
    endRowRendering: function() {
        var a = this;
        a.setCachedRows(a.tempRows.join(''));
        a.setCachedMergeCells(a.tempMergeCells.join(''));
        a.tempRows = a.tempMergeCells = null;
        a.startCaching = !1;
        a.lastRowIndex = 1
    },
    renderRows: function(c) {
        var b = Ext.Array.from(c),
            d = b.length,
            a;
        for (a = 0; a < d; a++) {
            this.renderRow(b[a])
        }
    },
    renderRow: function(g) {
        var a = this,
            b, h, i, c, f, e, d;
        if (!a.startCaching) {
            a.beginRowRendering()
        }
        c = a.cachedRow;
        if (Ext.isArray(g)) {
            e = g;
            b = {}
        } else {
            b = g;
            e = Ext.Array.from(b.cells || [])
        }
        delete(b.cells);
        Ext.applyIf(b, a.cachedRowConfig);
        c.setCollapsed(b.collapsed);
        c.setHidden(b.hidden);
        c.setHeight(b.height);
        c.setOutlineLevel(b.outlineLevel);
        c.setShowPhonetic(b.showPhonetic);
        c.setStyleId(b.styleId);
        c.setIndex(b.index);
        f = c.getIndex();
        if (!f) {
            c.setIndex(a.lastRowIndex++)
        } else {
            a.lastRowIndex = Math.max(a.lastRowIndex, f) + 1
        }
        d = c.renderCells(e);
        a.tempRows.push(d.row);
        if (a.cachedIndex === 0) {
            a._topLeftRef = d.first
        }
        a._bottomRightRef = d.last;
        a.tempMergeCells.push(d.merged);
        a.cachedIndex++;
        d.rowIndex = c.getIndex();
        return d
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Worksheet'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Font', Ext.exporter.file.ooxml.Base, {
    config: {
        size: 10,
        fontName: '',
        family: null,
        charset: null,
        bold: !1,
        italic: !1,
        underline: !1,
        outline: !1,
        strikeThrough: !1,
        color: null,
        verticalAlign: null
    },
    mappings: {
        family: {
            Automatic: 0,
            Roman: 1,
            Swiss: 2,
            Modern: 3,
            Script: 4,
            Decorative: 5
        }
    },
    tpl: ['<font>', '<tpl if="size"><sz val="{size}"/></tpl>', '<tpl if="fontName"><name val="{fontName}"/></tpl>', '<tpl if="family"><family val="{family}"/></tpl>', '<tpl if="charset"><charset val="{charset}"/></tpl>', '<tpl if="bold"><b/></tpl>', '<tpl if="italic"><i/></tpl>', '<tpl if="underline"><u/></tpl>', '<tpl if="outline"><outline/></tpl>', '<tpl if="strikeThrough"><strike/></tpl>', '<tpl if="color"><color rgb="{color}"/></tpl>', '<tpl if="verticalAlign"><vertAlign val="{verticalAlign}"/></tpl>', '</font>'],
    autoGenerateKey: ['size', 'fontName', 'family', 'charset', 'bold', 'italic', 'underline', 'outline', 'strikeThrough', 'color', 'verticalAlign'],
    constructor: function(b) {
        var d = {},
            c = Ext.Object.getKeys(b || {}),
            e = c.length,
            a;
        if (b) {
            for (a = 0; a < e; a++) {
                d[Ext.String.uncapitalize(c[a])] = b[c[a]]
            }
        }
        Ext.exporter.file.ooxml.Base.prototype.constructor.call(this, d)
    },
    applyFamily: function(a) {
        if (typeof a === 'string') {
            return this.mappings.family[a]
        }
        return a
    },
    applyBold: function(a) {
        return !!a
    },
    applyItalic: function(a) {
        return !!a
    },
    applyStrikeThrough: function(a) {
        return !!a
    },
    applyUnderline: function(a) {
        return !!a
    },
    applyOutline: function(a) {
        return !!a
    },
    applyColor: function(b) {
        var a;
        if (!b) {
            return b
        }
        a = String(b);
        return a.indexOf('#') >= 0 ? a.replace('#', '') : a
    },
    applyVerticalAlign: function(a) {
        return Ext.util.Format.lowercase(a)
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Font'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.NumberFormat', Ext.exporter.file.ooxml.Base, {
    config: {
        isDate: !1,
        numFmtId: null,
        formatCode: ''
    },
    tpl: ['<numFmt numFmtId="{numFmtId}" formatCode="{formatCode:htmlEncode}"/>'],
    spaceRe: /(,| )/g,
    getRenderData: function() {
        var b = Ext.exporter.file.ooxml.Base.prototype.getRenderData.call(this),
            a = b.formatCode;
        a = (a && b.isDate) ? a.replace(this.spaceRe, '\\$1') : a;
        b.formatCode = a;
        return b
    },
    getKey: function() {
        return this.getFormatCode()
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'NumberFormat'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Fill', Ext.exporter.file.ooxml.Base, {
    config: {
        patternType: 'none',
        fgColor: null,
        bgColor: null
    },
    tpl: ['<fill>', '<tpl if="fgColor || bgColor">', '<patternFill patternType="{patternType}">', '<tpl if="fgColor"><fgColor rgb="{fgColor}"/></tpl>', '<tpl if="bgColor"><bgColor rgb="{bgColor}"/></tpl>', '</patternFill>', '<tpl else>', '<patternFill patternType="{patternType}"/>', '</tpl>', '</fill>'],
    autoGenerateKey: ['patternType', 'fgColor', 'bgColor'],
    constructor: function(b) {
        var a = Ext.clone(b);
        if (b) {
            a.patternType = a.patternType || b.pattern;
            a.bgColor = a.bgColor || (a.patternType !== 'solid' ? b.color : null);
            a.fgColor = a.fgColor || (a.patternType === 'solid' ? b.color : null)
        }
        Ext.exporter.file.ooxml.Base.prototype.constructor.call(this, a)
    },
    formatColor: function(b) {
        var a;
        if (!b) {
            return b
        }
        a = String(b);
        return a.indexOf('#') >= 0 ? a.replace('#', '') : a
    },
    applyFgColor: function(a) {
        return this.formatColor(a)
    },
    applyBgColor: function(a) {
        return this.formatColor(a)
    },
    applyPatternType: function(c) {
        var b = ['none', 'solid', 'mediumGray', 'darkGray', 'lightGray', 'darkHorizontal', 'darkVertical', 'darkDown', 'darkUp', 'darkGrid', 'darkTrellis', 'lightHorizontal', 'lightVertical', 'lightDown', 'lightUp', 'lightGrid', 'lightTrellis', 'gray125', 'gray0625'],
            a = Ext.util.Format.uncapitalize(c);
        return Ext.Array.indexOf(b, a) >= 0 ? a : 'none'
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Fill'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.BorderPr', Ext.exporter.file.ooxml.Base, {
    isBorderPr: !0,
    config: {
        tag: 'left',
        color: null,
        lineStyle: 'none'
    },
    mappings: {
        lineStyle: {
            'None': 'none',
            'Continuous': 'thin',
            'Dash': 'dashed',
            'Dot': 'dotted',
            'DashDot': 'dashDot',
            'DashDotDot': 'dashDotDot',
            'SlantDashDot': 'slantDashDot',
            'Double': 'double'
        }
    },
    tpl: ['<tpl if="color">', '<{tag} style="{lineStyle}"><color rgb="{color}"/></{tag}>', '<tpl else>', '<{tag} style="{lineStyle}"/>', '</tpl>'],
    autoGenerateKey: ['tag', 'color', 'lineStyle'],
    applyColor: function(b) {
        var a;
        if (!b) {
            return b
        }
        a = String(b);
        return a.indexOf('#') >= 0 ? a.replace('#', '') : a
    },
    applyLineStyle: function(a) {
        var b = ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot'];
        return Ext.Array.indexOf(b, a) >= 0 ? a : (this.mappings.lineStyle[a] || 'none')
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'BorderPr'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Border', Ext.exporter.file.ooxml.Base, {
    config: {
        left: null,
        right: null,
        top: null,
        bottom: null
    },
    tpl: ['<border>', '<tpl if="left">{[values.left.render()]}</tpl>', '<tpl if="right">{[values.right.render()]}</tpl>', '<tpl if="top">{[values.top.render()]}</tpl>', '<tpl if="bottom">{[values.bottom.render()]}</tpl>', '</border>'],
    autoGenerateKey: ['left', 'right', 'top', 'bottom'],
    destroy: function() {
        this.setConfig({
            left: null,
            right: null,
            top: null,
            bottom: null
        });
        Ext.exporter.file.ooxml.Base.prototype.destroy.call(this)
    },
    applyLeft: function(a) {
        if (a && !a.isBorderPr) {
            return new Ext.exporter.file.ooxml.excel.BorderPr(a)
        }
        return a
    },
    applyTop: function(a) {
        if (a && !a.isBorderPr) {
            return new Ext.exporter.file.ooxml.excel.BorderPr(a)
        }
        return a
    },
    applyRight: function(a) {
        if (a && !a.isBorderPr) {
            return new Ext.exporter.file.ooxml.excel.BorderPr(a)
        }
        return a
    },
    applyBottom: function(a) {
        if (a && !a.isBorderPr) {
            return new Ext.exporter.file.ooxml.excel.BorderPr(a)
        }
        return a
    },
    updateLeft: function(a, b) {
        Ext.destroy(b);
        if (a) {
            a.setTag('left')
        }
    },
    updateTop: function(a, b) {
        Ext.destroy(b);
        if (a) {
            a.setTag('top')
        }
    },
    updateRight: function(a, b) {
        Ext.destroy(b);
        if (a) {
            a.setTag('right')
        }
    },
    updateBottom: function(a, b) {
        Ext.destroy(b);
        if (a) {
            a.setTag('bottom')
        }
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Border'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.CellAlignment', Ext.exporter.file.ooxml.Base, {
    isCellAlignment: !0,
    config: {
        horizontal: 'general',
        vertical: 'top',
        rotate: null,
        wrapText: !1,
        indent: null,
        relativeIndent: null,
        justifyLastLine: !1,
        shrinkToFit: !1,
        readingOrder: null
    },
    autoGenerateKey: ['horizontal', 'vertical', 'rotate', 'wrapText', 'indent', 'relativeIndent', 'justifyLastLine', 'shrinkToFit', 'readingOrder'],
    mappings: {
        horizontal: {
            Automatic: 'general',
            CenterAcrossSelection: 'centerContinuous',
            JustifyDistributed: 'distributed'
        },
        vertical: {
            Automatic: 'top',
            JustifyDistributed: 'distributed'
        },
        readingOrder: {
            Context: 0,
            LeftToRight: 1,
            RightToLeft: 2
        }
    },
    tpl: ['<alignment', '<tpl if="horizontal"> horizontal="{horizontal}"</tpl>', '<tpl if="vertical"> vertical="{vertical}"</tpl>', '<tpl if="rotate"> textRotation="{rotate}"</tpl>', '<tpl if="wrapText"> wrapText="{wrapText}"</tpl>', '<tpl if="indent"> indent="{indent}"</tpl>', '<tpl if="relativeIndent"> relativeIndent="{relativeIndent}"</tpl>', '<tpl if="justifyLastLine"> justifyLastLine="{justifyLastLine}"</tpl>', '<tpl if="shrinkToFit"> shrinkToFit="{shrinkToFit}"</tpl>', '<tpl if="readingOrder"> readingOrder="{readingOrder}"</tpl>', '/>'],
    constructor: function(b) {
        var d = {},
            c = Ext.Object.getKeys(b || {}),
            e = c.length,
            a;
        if (b) {
            for (a = 0; a < e; a++) {
                d[Ext.String.uncapitalize(c[a])] = b[c[a]]
            }
        }
        Ext.exporter.file.ooxml.Base.prototype.constructor.call(this, d)
    },
    applyHorizontal: function(a) {
        var c = ['general', 'left', 'center', 'right', 'fill', 'justify', 'centerContinuous', 'distributed'],
            b = Ext.util.Format.uncapitalize(a);
        return Ext.Array.indexOf(c, b) >= 0 ? b : (this.mappings.horizontal[a] || 'general')
    },
    applyVertical: function(a) {
        var c = ['top', 'center', 'bottom', 'justify', 'distributed'],
            b = Ext.util.Format.uncapitalize(a);
        return Ext.Array.indexOf(c, b) >= 0 ? b : (this.mappings.vertical[a] || 'top')
    },
    applyReadingOrder: function(a) {
        if (typeof a === 'string') {
            return this.mappings.readingOrder[a] || 0
        }
        return a
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'CellAlignment'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.CellStyleXf', Ext.exporter.file.ooxml.Base, {
    config: {
        numFmtId: 0,
        fontId: 0,
        fillId: 0,
        borderId: 0,
        alignment: null
    },
    autoGenerateKey: ['numFmtId', 'fontId', 'fillId', 'borderId', 'alignment'],
    tpl: ['<xf numFmtId="{numFmtId}" fontId="{fontId}" fillId="{fillId}" borderId="{borderId}"', '<tpl if="numFmtId"> applyNumberFormat="1"</tpl>', '<tpl if="fillId"> applyFill="1"</tpl>', '<tpl if="borderId"> applyBorder="1"</tpl>', '<tpl if="fontId"> applyFont="1"</tpl>', '<tpl if="alignment">', ' applyAlignment="1">{[values.alignment.render()]}</xf>', '<tpl else>', '/>', '</tpl>'],
    applyAlignment: function(a) {
        if (a && !a.isCellAlignment) {
            return new Ext.exporter.file.ooxml.excel.CellAlignment(a)
        }
        return a
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'CellStyleXf'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.CellXf', Ext.exporter.file.ooxml.excel.CellStyleXf, {
    config: {
        xfId: 0
    },
    tpl: ['<xf numFmtId="{numFmtId}" fontId="{fontId}" fillId="{fillId}" borderId="{borderId}" xfId="{xfId}"', '<tpl if="numFmtId"> applyNumberFormat="1"</tpl>', '<tpl if="fillId"> applyFill="1"</tpl>', '<tpl if="borderId"> applyBorder="1"</tpl>', '<tpl if="fontId"> applyFont="1"</tpl>', '<tpl if="alignment">', ' applyAlignment="1">{[values.alignment.render()]}</xf>', '<tpl else>', '/>', '</tpl>'],
    autoGenerateKey: ['xfId']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'CellXf'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Stylesheet', Ext.exporter.file.ooxml.Xml, {
    isStylesheet: !0,
    config: {
        fonts: [{
            fontName: 'Arial',
            size: 10,
            family: 2
        }],
        numberFormats: null,
        fills: [{
            patternType: 'none'
        }, {
            patternType: 'gray125'
        }],
        borders: [{
            left: {},
            top: {},
            right: {},
            bottom: {}
        }],
        cellStyleXfs: [{}],
        cellXfs: [{}]
    },
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles'
    },
    folder: '/xl/',
    fileName: 'styles',
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main">', '<tpl if="numberFormats"><numFmts count="{numberFormats.length}"><tpl for="numberFormats.items">{[values.render()]}</tpl></numFmts></tpl>', '<tpl if="fonts"><fonts count="{fonts.length}"><tpl for="fonts.items">{[values.render()]}</tpl></fonts></tpl>', '<tpl if="fills"><fills count="{fills.length}"><tpl for="fills.items">{[values.render()]}</tpl></fills></tpl>', '<tpl if="borders"><borders count="{borders.length}"><tpl for="borders.items">{[values.render()]}</tpl></borders></tpl>', '<tpl if="cellStyleXfs"><cellStyleXfs count="{cellStyleXfs.length}"><tpl for="cellStyleXfs.items">{[values.render()]}</tpl></cellStyleXfs></tpl>', '<tpl if="cellXfs"><cellXfs count="{cellXfs.length}"><tpl for="cellXfs.items">{[values.render()]}</tpl></cellXfs></tpl>', '<tableStyles count="0" defaultTableStyle="TableStyleMedium9" defaultPivotStyle="PivotStyleMedium7"/>', '</styleSheet>'],
    lastNumberFormatId: 164,
    datePatterns: {
        'General Date': '[$-F800]dddd, mmmm dd, yyyy',
        'Long Date': '[$-F800]dddd, mmmm dd, yyyy',
        'Medium Date': 'mm/dd/yy;@',
        'Short Date': 'm/d/yy;@',
        'Long Time': 'h:mm:ss;@',
        'Medium Time': '[$-409]h:mm AM/PM;@',
        'Short Time': 'h:mm;@'
    },
    numberPatterns: {
        'General Number': 1,
        'Fixed': 2,
        'Standard': 2,
        'Percent': 10,
        'Scientific': 11,
        'Currency': '"$"#,##0.00',
        'Euro Currency': '"â‚¬"#,##0.00'
    },
    booleanPatterns: {
        'Yes/No': '"Yes";-;"No"',
        'True/False': '"True";-;"False"',
        'On/Off': '"On";-;"Off"'
    },
    applyFonts: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Font')
    },
    applyNumberFormats: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.NumberFormat')
    },
    applyFills: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Fill')
    },
    applyBorders: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Border')
    },
    applyCellXfs: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.CellXf')
    },
    applyCellStyleXfs: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.CellStyleXf')
    },
    addFont: function(d) {
        var b = this._fonts,
            c, a;
        if (!b) {
            this.setFonts([]);
            b = this._fonts
        }
        a = new Ext.exporter.file.ooxml.excel.Font(d);
        c = b.indexOfKey(a.getKey());
        if (c >= 0) {
            a.destroy()
        } else {
            b.add(a);
            c = b.indexOf(a)
        }
        return c
    },
    addNumberFormat: function(d) {
        var b = this._numberFormats,
            a, c;
        if (!b) {
            this.setNumberFormats([]);
            b = this._numberFormats
        }
        c = new Ext.exporter.file.ooxml.excel.NumberFormat(d);
        a = b.get(c.getKey());
        if (!a) {
            a = c;
            b.add(a);
            a.setNumFmtId(this.lastNumberFormatId++)
        }
        return a.getNumFmtId()
    },
    addFill: function(d) {
        var b = this._fills,
            c, a;
        if (!b) {
            this.setFills([]);
            b = this._fills
        }
        a = new Ext.exporter.file.ooxml.excel.Fill(d);
        c = b.indexOfKey(a.getKey());
        if (c >= 0) {
            a.destroy()
        } else {
            b.add(a);
            c = b.indexOf(a)
        }
        return c
    },
    addBorder: function(d) {
        var b = this._borders,
            c, a;
        if (!b) {
            this.setBorders([]);
            b = this._borders
        }
        a = new Ext.exporter.file.ooxml.excel.Border(d);
        c = b.indexOfKey(a.getKey());
        if (c >= 0) {
            a.destroy()
        } else {
            b.add(a);
            c = b.indexOf(a)
        }
        return c
    },
    addCellXf: function(d) {
        var b = this._cellXfs,
            c, a;
        if (!b) {
            this.setCellXfs([]);
            b = this._cellXfs
        }
        a = new Ext.exporter.file.ooxml.excel.CellXf(d);
        c = b.indexOfKey(a.getKey());
        if (c >= 0) {
            a.destroy()
        } else {
            b.add(a);
            c = b.indexOf(a)
        }
        return c
    },
    addCellStyleXf: function(d) {
        var b = this._cellStyleXfs,
            c, a;
        if (!b) {
            this.setCellStyleXfs([]);
            b = this._cellStyleXfs
        }
        a = new Ext.exporter.file.ooxml.excel.CellStyleXf(d);
        c = b.indexOfKey(a.getKey());
        if (c >= 0) {
            a.destroy()
        } else {
            b.add(a);
            c = b.indexOf(a)
        }
        return c
    },
    getStyleParams: function(b) {
        var c = this,
            i = (b && b.isStyle) ? b : new Ext.exporter.file.Style(b),
            a = i.getConfig(),
            e = 0,
            g = 0,
            f = 0,
            d = 0,
            h = 0;
        a.parentId = b ? b.parentId : null;
        if (a.font) {
            g = c.addFont(a.font)
        }
        if (a.format) {
            e = c.getNumberFormatId(a.format)
        }
        if (a.interior) {
            f = c.addFill(a.interior)
        }
        if (a.borders) {
            d = c.getBorderId(a.borders)
        }
        if (a.parentId) {
            h = a.parentId
        }
        return {
            numFmtId: e,
            fontId: g,
            fillId: f,
            borderId: d,
            xfId: h,
            alignment: a.alignment || null
        }
    },
    addStyle: function(a) {
        return this.addCellStyleXf(this.getStyleParams(a))
    },
    addCellStyle: function(e, d) {
        var c = this.getCellXfs(),
            a, b, f;
        if (c) {
            a = c.getAt(d);
            if (a) {
                b = a.getConfig()
            }
        }
        return this.addCellXf(Ext.merge(b || {}, this.getStyleParams(e)))
    },
    getNumberFormatId: function(b) {
        var c = this,
            e = !!c.datePatterns[b],
            d, a;
        if (b === 'General') {
            return 0
        }
        a = c.datePatterns[b] || c.booleanPatterns[b] || c.numberPatterns[b];
        if (Ext.isNumeric(a)) {
            d = a
        } else if (!a) {
            a = b
        }
        return d || c.addNumberFormat({
            isDate: e,
            formatCode: a
        })
    },
    getBorderId: function(c) {
        var d = {},
            f = c.length,
            b, a, e;
        for (b = 0; b < f; b++) {
            a = c[b];
            e = Ext.util.Format.lowercase(a.position);
            delete(a.position);
            d[e] = a
        }
        return this.addBorder(d)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Stylesheet'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.SharedStrings', Ext.exporter.file.ooxml.Xml, {
    isSharedStrings: !0,
    config: {
        strings: []
    },
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings'
    },
    folder: '/xl/',
    fileName: 'sharedStrings',
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="{strings.length}" uniqueCount="{strings.length}">', '<tpl for="strings.getRange()"><si><t>{.:this.utf8}</t></si></tpl>', '</sst>', {
        utf8: function(a) {
            return Ext.util.Base64._utf8_encode(a)
        }
    }],
    destroy: function() {
        this.setStrings(null);
        Ext.exporter.file.ooxml.Xml.prototype.destroy.call(this)
    },
    applyStrings: function(b, c) {
        var a;
        if (b) {
            a = new Ext.util.Collection({
                keyFn: Ext.identityFn
            });
            a.add(b)
        }
        Ext.destroy(c);
        return a
    },
    addString: function(d) {
        var c = Ext.util.Format.htmlEncode(d),
            a = this.getStrings(),
            b;
        if (!a) {
            this.setStrings([]);
            a = this.getStrings()
        }
        b = a.indexOfKey(c);
        if (b < 0) {
            a.add(c);
            b = a.length - 1
        }
        return b
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'SharedStrings'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.theme.Base', Ext.exporter.file.ooxml.XmlRels, {
    folder: '/theme/',
    fileName: 'theme',
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.theme+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme'
    }
}, 0, 0, 0, 0, ["ooxmltheme.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.exporter.file.ooxml.theme, 'Base'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.theme.Office', Ext.exporter.file.ooxml.theme.Base, {
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office Theme">', '<a:themeElements>', '<a:clrScheme name="Office">', '<a:dk1>', '<a:sysClr val="windowText" lastClr="000000"/>', '</a:dk1>', '<a:lt1>', '<a:sysClr val="window" lastClr="FFFFFF"/>', '</a:lt1>', '<a:dk2>', '<a:srgbClr val="44546A"/>', '</a:dk2>', '<a:lt2>', '<a:srgbClr val="E7E6E6"/>', '</a:lt2>', '<a:accent1>', '<a:srgbClr val="4472C4"/>', '</a:accent1>', '<a:accent2>', '<a:srgbClr val="ED7D31"/>', '</a:accent2>', '<a:accent3>', '<a:srgbClr val="A5A5A5"/>', '</a:accent3>', '<a:accent4>', '<a:srgbClr val="FFC000"/>', '</a:accent4>', '<a:accent5>', '<a:srgbClr val="5B9BD5"/>', '</a:accent5>', '<a:accent6>', '<a:srgbClr val="70AD47"/>', '</a:accent6>', '<a:hlink>', '<a:srgbClr val="0563C1"/>', '</a:hlink>', '<a:folHlink>', '<a:srgbClr val="954F72"/>', '</a:folHlink>', '</a:clrScheme>', '<a:fontScheme name="Office">', '<a:majorFont>', '<a:latin typeface="Calibri Light" panose="020F0302020204030204"/>', '<a:ea typeface=""/>', '<a:cs typeface=""/>', '<a:font script="Jpan" typeface="Yu Gothic Light"/>', '<a:font script="Hang" typeface="{[this.utf8(\'ë§‘ì€ ê³ ë”•\')]}"/>', '<a:font script="Hans" typeface="DengXian Light"/>', '<a:font script="Hant" typeface="{[this.utf8(\'æ–°ç´°æ˜Žé«”\')]}"/>', '<a:font script="Arab" typeface="Times New Roman"/>', '<a:font script="Hebr" typeface="Times New Roman"/>', '<a:font script="Thai" typeface="Tahoma"/>', '<a:font script="Ethi" typeface="Nyala"/>', '<a:font script="Beng" typeface="Vrinda"/>', '<a:font script="Gujr" typeface="Shruti"/>', '<a:font script="Khmr" typeface="MoolBoran"/>', '<a:font script="Knda" typeface="Tunga"/>', '<a:font script="Guru" typeface="Raavi"/>', '<a:font script="Cans" typeface="Euphemia"/>', '<a:font script="Cher" typeface="Plantagenet Cherokee"/>', '<a:font script="Yiii" typeface="Microsoft Yi Baiti"/>', '<a:font script="Tibt" typeface="Microsoft Himalaya"/>', '<a:font script="Thaa" typeface="MV Boli"/>', '<a:font script="Deva" typeface="Mangal"/>', '<a:font script="Telu" typeface="Gautami"/>', '<a:font script="Taml" typeface="Latha"/>', '<a:font script="Syrc" typeface="Estrangelo Edessa"/>', '<a:font script="Orya" typeface="Kalinga"/>', '<a:font script="Mlym" typeface="Kartika"/>', '<a:font script="Laoo" typeface="DokChampa"/>', '<a:font script="Sinh" typeface="Iskoola Pota"/>', '<a:font script="Mong" typeface="Mongolian Baiti"/>', '<a:font script="Viet" typeface="Times New Roman"/>', '<a:font script="Uigh" typeface="Microsoft Uighur"/>', '<a:font script="Geor" typeface="Sylfaen"/>', '</a:majorFont>', '<a:minorFont>', '<a:latin typeface="Calibri" panose="020F0502020204030204"/>', '<a:ea typeface=""/>', '<a:cs typeface=""/>', '<a:font script="Jpan" typeface="Yu Gothic"/>', '<a:font script="Hang" typeface="{[this.utf8(\'ë§‘ì€ ê³ ë”•\')]}"/>', '<a:font script="Hans" typeface="DengXian"/>', '<a:font script="Hant" typeface="{[this.utf8(\'æ–°ç´°æ˜Žé«”\')]}"/>', '<a:font script="Arab" typeface="Arial"/>', '<a:font script="Hebr" typeface="Arial"/>', '<a:font script="Thai" typeface="Tahoma"/>', '<a:font script="Ethi" typeface="Nyala"/>', '<a:font script="Beng" typeface="Vrinda"/>', '<a:font script="Gujr" typeface="Shruti"/>', '<a:font script="Khmr" typeface="DaunPenh"/>', '<a:font script="Knda" typeface="Tunga"/>', '<a:font script="Guru" typeface="Raavi"/>', '<a:font script="Cans" typeface="Euphemia"/>', '<a:font script="Cher" typeface="Plantagenet Cherokee"/>', '<a:font script="Yiii" typeface="Microsoft Yi Baiti"/>', '<a:font script="Tibt" typeface="Microsoft Himalaya"/>', '<a:font script="Thaa" typeface="MV Boli"/>', '<a:font script="Deva" typeface="Mangal"/>', '<a:font script="Telu" typeface="Gautami"/>', '<a:font script="Taml" typeface="Latha"/>', '<a:font script="Syrc" typeface="Estrangelo Edessa"/>', '<a:font script="Orya" typeface="Kalinga"/>', '<a:font script="Mlym" typeface="Kartika"/>', '<a:font script="Laoo" typeface="DokChampa"/>', '<a:font script="Sinh" typeface="Iskoola Pota"/>', '<a:font script="Mong" typeface="Mongolian Baiti"/>', '<a:font script="Viet" typeface="Arial"/>', '<a:font script="Uigh" typeface="Microsoft Uighur"/>', '<a:font script="Geor" typeface="Sylfaen"/>', '</a:minorFont>', '</a:fontScheme>', '<a:fmtScheme name="Office">', '<a:fillStyleLst>', '<a:solidFill>', '<a:schemeClr val="phClr"/>', '</a:solidFill>', '<a:gradFill rotWithShape="1">', '<a:gsLst>', '<a:gs pos="0">', '<a:schemeClr val="phClr">', '<a:lumMod val="110000"/>', '<a:satMod val="105000"/>', '<a:tint val="67000"/>', '</a:schemeClr>', '</a:gs>', '<a:gs pos="50000">', '<a:schemeClr val="phClr">', '<a:lumMod val="105000"/>', '<a:satMod val="103000"/>', '<a:tint val="73000"/>', '</a:schemeClr>', '</a:gs>', '<a:gs pos="100000">', '<a:schemeClr val="phClr">', '<a:lumMod val="105000"/>', '<a:satMod val="109000"/>', '<a:tint val="81000"/>', '</a:schemeClr>', '</a:gs>', '</a:gsLst>', '<a:lin ang="5400000" scaled="0"/>', '</a:gradFill>', '<a:gradFill rotWithShape="1">', '<a:gsLst>', '<a:gs pos="0">', '<a:schemeClr val="phClr">', '<a:satMod val="103000"/>', '<a:lumMod val="102000"/>', '<a:tint val="94000"/>', '</a:schemeClr>', '</a:gs>', '<a:gs pos="50000">', '<a:schemeClr val="phClr">', '<a:satMod val="110000"/>', '<a:lumMod val="100000"/>', '<a:shade val="100000"/>', '</a:schemeClr>', '</a:gs>', '<a:gs pos="100000">', '<a:schemeClr val="phClr">', '<a:lumMod val="99000"/>', '<a:satMod val="120000"/>', '<a:shade val="78000"/>', '</a:schemeClr>', '</a:gs>', '</a:gsLst>', '<a:lin ang="5400000" scaled="0"/>', '</a:gradFill>', '</a:fillStyleLst>', '<a:lnStyleLst>', '<a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">', '<a:solidFill>', '<a:schemeClr val="phClr"/>', '</a:solidFill>', '<a:prstDash val="solid"/>', '<a:miter lim="800000"/>', '</a:ln>', '<a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">', '<a:solidFill>', '<a:schemeClr val="phClr"/>', '</a:solidFill>', '<a:prstDash val="solid"/>', '<a:miter lim="800000"/>', '</a:ln>', '<a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">', '<a:solidFill>', '<a:schemeClr val="phClr"/>', '</a:solidFill>', '<a:prstDash val="solid"/>', '<a:miter lim="800000"/>', '</a:ln>', '</a:lnStyleLst>', '<a:effectStyleLst>', '<a:effectStyle>', '<a:effectLst/>', '</a:effectStyle>', '<a:effectStyle>', '<a:effectLst/>', '</a:effectStyle>', '<a:effectStyle>', '<a:effectLst>', '<a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">', '<a:srgbClr val="000000">', '<a:alpha val="63000"/>', '</a:srgbClr>', '</a:outerShdw>', '</a:effectLst>', '</a:effectStyle>', '</a:effectStyleLst>', '<a:bgFillStyleLst>', '<a:solidFill>', '<a:schemeClr val="phClr"/>', '</a:solidFill>', '<a:solidFill>', '<a:schemeClr val="phClr">', '<a:tint val="95000"/>', '<a:satMod val="170000"/>', '</a:schemeClr>', '</a:solidFill>', '<a:gradFill rotWithShape="1">', '<a:gsLst>', '<a:gs pos="0">', '<a:schemeClr val="phClr">', '<a:tint val="93000"/>', '<a:satMod val="150000"/>', '<a:shade val="98000"/>', '<a:lumMod val="102000"/>', '</a:schemeClr>', '</a:gs>', '<a:gs pos="50000">', '<a:schemeClr val="phClr">', '<a:tint val="98000"/>', '<a:satMod val="130000"/>', '<a:shade val="90000"/>', '<a:lumMod val="103000"/>', '</a:schemeClr>', '</a:gs>', '<a:gs pos="100000">', '<a:schemeClr val="phClr">', '<a:shade val="63000"/>', '<a:satMod val="120000"/>', '</a:schemeClr>', '</a:gs>', '</a:gsLst>', '<a:lin ang="5400000" scaled="0"/>', '</a:gradFill>', '</a:bgFillStyleLst>', '</a:fmtScheme>', '</a:themeElements>', '<a:objectDefaults/>', '<a:extraClrSchemeLst/>', '</a:theme>', {
        utf8: function(a) {
            return Ext.util.Base64._utf8_encode(a || '')
        }
    }]
}, 0, 0, 0, 0, ["ooxmltheme.office"], 0, [Ext.exporter.file.ooxml.theme, 'Office'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.excel.Workbook', Ext.exporter.file.ooxml.XmlRels, {
    isWorkbook: !0,
    currentSheetIndex: 1,
    currentPivotCacheIndex: 0,
    config: {
        stylesheet: {},
        sharedStrings: {},
        sheets: [],
        pivotCaches: null,
        theme: {
            type: 'office',
            folder: '/xl/theme/',
            index: 1
        }
    },
    contentType: {
        contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument'
    },
    folder: '/xl/',
    fileName: 'workbook',
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" ', 'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">', '<tpl if="sheets">', '<sheets>', '<tpl if="sheets"><tpl for="sheets.items"><sheet name="{[this.utf8(values.getName())]}" sheetId="{[xindex]}" state="visible" r:id="{[values.getRelationship().getId()]}"/></tpl></tpl>', '</sheets>', '</tpl>', '<tpl if="pivotCaches">', '<pivotCaches>', '<tpl for="pivotCaches.getRange()">{[values.render()]}</tpl>', '</pivotCaches>', '</tpl>', '</workbook>', {
        utf8: function(a) {
            return Ext.util.Base64._utf8_encode(a || '')
        }
    }],
    destroy: function() {
        var a = this;
        a.setStylesheet(null);
        a.setSharedStrings(null);
        a.setTheme(null);
        Ext.exporter.file.ooxml.XmlRels.prototype.destroy.call(this)
    },
    collectFiles: function(b) {
        var a = this,
            g = a._stylesheet,
            e = a._sharedStrings,
            h = a._theme,
            d, c, f;
        d = a._sheets;
        f = d.length;
        for (c = 0; c < f; c++) {
            d.items[c].collectFiles(b)
        }
        b[a._path] = a.render();
        b[g._path] = g.render();
        b[e._path] = e.render();
        b[h._path] = h.render();
        a.collectRelationshipsFiles(b)
    },
    collectContentTypes: function(a) {
        var b = this,
            d, c, e;
        a.push(b.getStylesheet().getContentType());
        a.push(b.getSharedStrings().getContentType());
        a.push(b.getTheme().getContentType());
        d = b.getSheets();
        e = d.length;
        for (c = 0; c < e; c++) {
            d.getAt(c).collectContentTypes(a)
        }
        Ext.exporter.file.ooxml.XmlRels.prototype.collectContentTypes.call(this, a)
    },
    applyStylesheet: function(a) {
        if (!a || a.isStylesheet) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.Stylesheet()
    },
    updateStylesheet: function(c, b) {
        var a = this.getRelationships();
        if (b && a) {
            a.removeRelationship(b.getRelationship())
        }
        if (c && a) {
            a.addRelationship(c.getRelationship())
        }
        Ext.destroy(b)
    },
    applySharedStrings: function(a) {
        if (!a || a.isSharedStrings) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.SharedStrings()
    },
    updateSharedStrings: function(c, a) {
        var b = this.getRelationships();
        if (a && b) {
            b.removeRelationship(a.getRelationship())
        }
        if (c) {
            b.addRelationship(c.getRelationship())
        }
        Ext.destroy(a)
    },
    applyPivotCaches: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.PivotCache')
    },
    updatePivotCaches: function(c, b) {
        var a = this;
        if (b) {
            b.un({
                add: a.onPivotCacheAdd,
                scope: a
            })
        }
        if (c) {
            c.on({
                add: a.onPivotCacheAdd,
                scope: a
            })
        }
    },
    onPivotCacheAdd: function(e, b) {
        var d = b.items.length,
            a, c;
        for (a = 0; a < d; a++) {
            c = b.items[a];
            c.setCacheId(this.currentPivotCacheIndex++)
        }
    },
    applySheets: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.excel.Sheet')
    },
    updateSheets: function(c, b) {
        var a = this;
        if (b) {
            b.un({
                add: a.onSheetAdd,
                remove: a.onSheetRemove,
                scope: a
            })
        }
        if (c) {
            c.on({
                add: a.onSheetAdd,
                remove: a.onSheetRemove,
                scope: a
            })
        }
    },
    applyTheme: function(a) {
        var b = {
            type: 'office'
        };
        if (a) {
            if (typeof a == 'string') {
                b.type = a
            } else {
                Ext.apply(b, a)
            }
            a = Ext.Factory.ooxmltheme(a)
        }
        return a
    },
    updateTheme: function(c, b) {
        var a = this.getRelationships();
        if (b && a) {
            a.removeRelationship(b.getRelationship())
        }
        if (c && a) {
            a.addRelationship(c.getRelationship())
        }
        Ext.destroy(b)
    },
    onSheetAdd: function(f, c) {
        var e = this.getRelationships(),
            d = c.items.length,
            b, a;
        for (b = 0; b < d; b++) {
            a = c.items[b];
            a.setIndex(this.currentSheetIndex++);
            a.setWorkbook(this);
            e.addRelationship(a.getRelationship())
        }
    },
    onSheetRemove: function(f, c) {
        var e = this.getRelationships(),
            d = c.items.length,
            a, b;
        for (a = 0; a < d; a++) {
            b = c.items[a];
            e.removeRelationship(b.getRelationship());
            Ext.destroy(b)
        }
    },
    addWorksheet: function(d) {
        var c = Ext.Array.from(d || {}),
            e = c.length,
            a, b;
        for (a = 0; a < e; a++) {
            b = c[a];
            if (b && !b.isWorksheet) {
                b.workbook = this;
                c[a] = new Ext.exporter.file.ooxml.excel.Worksheet(b)
            }
        }
        return this.getSheets().add(c)
    },
    removeWorksheet: function(a) {
        return this.getSheets().remove(a)
    },
    addPivotCache: function(a) {
        if (!this.getPivotCaches()) {
            this.setPivotCaches([])
        }
        return this.getPivotCaches().add(a || {})
    },
    removePivotCache: function(a) {
        return this.getPivotCaches().remove(a)
    },
    addStyle: function(a) {
        return this.getStylesheet().addStyle(a)
    },
    addCellStyle: function(a) {
        return this.getStylesheet().addCellStyle(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml.excel, 'Workbook'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.ContentTypes', Ext.exporter.file.ooxml.Xml, {
    isContentTypes: !0,
    config: {
        contentTypes: [{
            tag: 'Default',
            contentType: 'application/vnd.openxmlformats-package.relationships+xml',
            extension: 'rels'
        }, {
            tag: 'Default',
            contentType: 'application/xml',
            extension: 'xml'
        }]
    },
    tpl: ['<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">', '<tpl if="contentTypes"><tpl for="contentTypes.getRange()">{[values.render()]}</tpl></tpl>', '</Types>'],
    folder: '/',
    fileName: '[Content_Types]',
    applyContentTypes: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.ooxml.ContentType')
    },
    addContentType: function(a) {
        return this.getContentTypes().add(a || {})
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'ContentTypes'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.CoreProperties', Ext.exporter.file.ooxml.Xml, {
    isCoreProperties: !0,
    config: {
        title: "Workbook",
        author: 'Sencha',
        subject: ''
    },
    contentType: {
        contentType: 'application/vnd.openxmlformats-package.core-properties+xml',
        partName: '/docProps/core.xml'
    },
    relationship: {
        schema: 'http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties',
        target: 'docProps/core.xml'
    },
    path: '/docProps/core.xml',
    tpl: ['<coreProperties xmlns="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" ', 'xmlns:dcterms="http://purl.org/dc/terms/" ', 'xmlns:dc="http://purl.org/dc/elements/1.1/" ', 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', '   <dc:creator>{author:this.utf8}</dc:creator>', '   <dc:title>{title:this.utf8}</dc:title>', '   <dc:subject>{subject:this.utf8}</dc:subject>', '</coreProperties>', {
        utf8: function(a) {
            return Ext.util.Base64._utf8_encode(a || '')
        }
    }]
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'CoreProperties'], 0));
(Ext.cmd.derive('Ext.exporter.file.ooxml.Excel', Ext.exporter.file.ooxml.XmlRels, {
    config: {
        properties: null,
        workbook: {}
    },
    folder: '/',
    fileName: null,
    tpl: [],
    constructor: function(a) {
        var b = Ext.exporter.file.ooxml.XmlRels.prototype.constructor.call(this, a);
        if (!this.getWorkbook()) {
            this.setWorkbook({})
        }
        return b
    },
    destroy: function() {
        var a = this;
        a.setWorkbook(null);
        a.setProperties(null);
        a.setRelationships(null);
        Ext.exporter.file.ooxml.XmlRels.prototype.destroy.call(this)
    },
    render: function() {
        var e = {},
            f, a, c, d, g, b;
        this.collectFiles(e);
        f = Ext.Object.getKeys(e);
        g = f.length;
        if (!g) {
            return
        }
        b = new Ext.exporter.file.zip.Archive();
        for (d = 0; d < g; d++) {
            a = f[d];
            c = e[a];
            a = a.substr(1);
            if (a.indexOf('.xml') !== -1 || a.indexOf('.rel') !== -1) {
                b.addFile({
                    path: a,
                    data: c
                })
            }
        }
        c = b.getContent();
        b = Ext.destroy(b);
        return c
    },
    collectFiles: function(b) {
        var a = new Ext.exporter.file.ooxml.ContentTypes(),
            e = this.getWorkbook(),
            c = this.getProperties(),
            d = [];
        e.collectFiles(b);
        if (c) {
            a.addContentType(c.getContentType());
            b[c.getPath()] = c.render()
        }
        e.collectContentTypes(d);
        a.addContentType(d);
        b[a.getPath()] = a.render();
        Ext.destroy(a);
        this.collectRelationshipsFiles(b)
    },
    applyProperties: function(a) {
        if (!a || a.isCoreProperties) {
            return a
        }
        return new Ext.exporter.file.ooxml.CoreProperties(a)
    },
    updateProperties: function(b, a) {
        var c = this.getRelationships();
        if (a) {
            c.removeRelationship(a.getRelationship());
            a.destroy()
        }
        if (b) {
            c.addRelationship(b.getRelationship())
        }
    },
    applyWorkbook: function(a) {
        if (!a || a.isWorkbook) {
            return a
        }
        return new Ext.exporter.file.ooxml.excel.Workbook(a)
    },
    updateWorkbook: function(b, a) {
        var c = this.getRelationships();
        if (a) {
            c.removeRelationship(a.getRelationship());
            a.destroy()
        }
        if (b) {
            c.addRelationship(b.getRelationship())
        }
    },
    addWorksheet: function(a) {
        return this.getWorkbook().addWorksheet(a)
    },
    addStyle: function(a) {
        return this.getWorkbook().getStylesheet().addStyle(a)
    },
    addCellStyle: function(b, a) {
        return this.getWorkbook().getStylesheet().addCellStyle(b, a)
    }
}, 1, 0, 0, 0, 0, 0, [Ext.exporter.file.ooxml, 'Excel'], 0));
(Ext.cmd.derive('Ext.exporter.excel.Xlsx', Ext.exporter.Base, {
    alternateClassName: 'Ext.exporter.Excel',
    config: {
        defaultStyle: {
            alignment: {
                vertical: 'Top'
            },
            font: {
                fontName: 'Arial',
                family: 'Swiss',
                size: 11,
                color: '#000000'
            }
        },
        titleStyle: {
            alignment: {
                horizontal: 'Center',
                vertical: 'Center'
            },
            font: {
                fontName: 'Arial',
                family: 'Swiss',
                size: 18,
                color: '#1F497D'
            }
        },
        groupHeaderStyle: {
            borders: [{
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        groupFooterStyle: {
            borders: [{
                position: 'Top',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        tableHeaderStyle: {
            alignment: {
                horizontal: 'Center',
                vertical: 'Center'
            },
            borders: [{
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }],
            font: {
                fontName: 'Arial',
                family: 'Swiss',
                size: 11,
                color: '#1F497D'
            }
        }
    },
    fileName: 'export.xlsx',
    charset: 'ascii',
    mimeType: 'application/zip',
    binary: !0,
    titleRowHeight: 22.5,
    headerRowHeight: 20.25,
    destroy: function() {
        var a = this;
        a.excel = a.worksheet = Ext.destroy(a.excel, a.worksheet);
        Ext.exporter.Base.prototype.destroy.call(this)
    },
    getContent: function() {
        var a = this,
            b = this.getConfig(),
            d = b.data,
            e, c;
        a.excel = new Ext.exporter.file.ooxml.Excel({
            properties: {
                title: b.title,
                author: b.author
            }
        });
        a.worksheet = c = a.excel.addWorksheet({
            name: b.title
        });
        a.tableHeaderStyleId = a.excel.addCellStyle(b.tableHeaderStyle);
        e = d ? d.getColumnCount() : 1;
        c.beginRowRendering();
        a.addTitle(b, e);
        if (d) {
            c.renderRows(a.buildHeader());
            c.renderRows(a.buildRows(d, e, -1))
        }
        c.endRowRendering();
        a.columnStylesNormal = a.columnStylesNormalId = a.columnStylesFooter = a.columnStylesFooterId = null;
        a.headerStyles = a.footerStyles = null;
        return a.excel.render()
    },
    addTitle: function(a, b) {
        if (!Ext.isEmpty(a.title)) {
            this.worksheet.renderRow({
                height: this.titleRowHeight,
                cells: [{
                    mergeAcross: b - 1,
                    value: a.title,
                    styleId: this.excel.addCellStyle(a.titleStyle)
                }]
            })
        }
    },
    buildRows: function(b, t, n) {
        var c = this,
            s = c._showSummary,
            e = [],
            k, j, l, v, h, r, a, f, w, q, p, m, u, g, d, i, o;
        if (!b) {
            return e
        }
        k = b._groups;
        u = b._text;
        m = (!k && !b._rows);
        if (s !== !1 && !Ext.isEmpty(u) && !m) {
            l = c.getGroupHeaderStyleByLevel(n);
            e.push({
                styleId: l,
                cells: [{
                    mergeAcross: t - 1,
                    value: u,
                    styleId: l
                }]
            })
        }
        if (k) {
            w = k.length;
            for (r = 0; r < w; r++) {
                Ext.Array.insert(e, e.length, c.buildRows(k.items[r], t, n + 1))
            }
        }
        if (b._rows) {
            g = b._rows.items;
            q = g.length;
            if (s === !1) {
                l = c.getGroupHeaderStyleByLevel(n);
                e.push({
                    cells: [{
                        mergeAcross: t - 1,
                        value: b._text,
                        styleId: l
                    }]
                })
            }
            for (f = 0; f < q; f++) {
                i = g[f];
                j = {
                    id: i._id,
                    cells: []
                };
                h = i._cells;
                p = h.length;
                for (a = 0; a < p; a++) {
                    d = h.items[a];
                    o = c.columnStylesNormalId[a];
                    j.cells.push({
                        id: d._id,
                        value: d._value,
                        styleId: c.getCellStyleId(d._style, o)
                    })
                }
                e.push(j)
            }
        }
        g = b._summaries && b._summaries.items;
        if (g && (s || m)) {
            v = c.getGroupFooterStyleByLevel(n);
            q = g.length;
            for (f = 0; f < q; f++) {
                i = g[f];
                j = {
                    id: i._id,
                    cells: []
                };
                h = i._cells;
                p = h.length;
                for (a = 0; a < p; a++) {
                    d = h.items[a];
                    o = m ? c.columnStylesNormalId[a] : (a === 0 ? v : c.columnStylesFooterId[a]);
                    j.cells.push({
                        id: d._id,
                        value: d._value,
                        styleId: c.getCellStyleId(d._style, o)
                    })
                }
                e.push(j)
            }
        }
        b.destroy();
        return e
    },
    getGroupHeaderStyleByLevel: function(c) {
        var b = this,
            d = 'l' + c,
            a = b.headerStyles;
        if (!a) {
            b.headerStyles = a = {}
        }
        if (!a.hasOwnProperty(d)) {
            a[d] = b.excel.addCellStyle(Ext.applyIf({
                alignment: {
                    Indent: c > 0 ? c : 0
                }
            }, b._groupHeaderStyle))
        }
        return a[d]
    },
    getGroupFooterStyleByLevel: function(c) {
        var b = this,
            d = 'l' + c,
            a = b.footerStyles;
        if (!a) {
            b.footerStyles = a = {}
        }
        if (!a.hasOwnProperty(d)) {
            a[d] = b.excel.addCellStyle(Ext.applyIf({
                alignment: {
                    Indent: c > 0 ? c : 0
                }
            }, b.columnStylesFooter[0]))
        }
        return a[d]
    },
    buildHeader: function() {
        var a = this,
            k = {},
            n = a.getData(),
            o = [],
            i, l, g, c, p, f, d, e, m, j, b, h;
        a.buildHeaderRows(n.getColumns(), k);
        i = Ext.Object.getKeys(k);
        p = i.length;
        for (g = 0; g < p; g++) {
            l = {
                height: a.headerRowHeight,
                styleId: a.tableHeaderStyleId,
                cells: []
            };
            e = k[i[g]];
            f = e.length;
            for (c = 0; c < f; c++) {
                h = e[c];
                h.styleId = a.tableHeaderStyleId;
                l.cells.push(h)
            }
            o.push(l)
        }
        e = n.getBottomColumns();
        f = e.length;
        a.columnStylesNormal = [];
        a.columnStylesNormalId = [];
        a.columnStylesFooter = [];
        a.columnStylesFooterId = [];
        m = a.getGroupFooterStyle();
        for (c = 0; c < f; c++) {
            j = e[c];
            b = {
                style: j.getStyle(),
                width: j.getWidth()
            };
            d = Ext.applyIf({
                parentId: 0
            }, m);
            d = Ext.merge(d, b.style);
            a.columnStylesFooter.push(d);
            a.columnStylesFooterId.push(a.excel.addCellStyle(d));
            d = Ext.applyIf({
                parentId: 0
            }, b.style);
            a.columnStylesNormal.push(d);
            b.styleId = a.excel.addCellStyle(d);
            a.columnStylesNormalId.push(b.styleId);
            b.min = b.max = c + 1;
            b.style = null;
            if (b.width) {
                b.width = b.width / 10
            }
            a.worksheet.addColumn(b)
        }
        return o
    },
    getCellStyleId: function(b, a) {
        return b ? this.excel.addCellStyle(b, a) : a
    },
    buildHeaderRows: function(e, b) {
        var a, f, d, g, c;
        if (!e) {
            return
        }
        g = e.length;
        for (d = 0; d < g; d++) {
            a = e.items[d].getConfig();
            a.value = a.text;
            f = a.columns;
            delete(a.columns);
            delete(a.table);
            c = 's' + a.level;
            b[c] = b[c] || [];
            b[c].push(a);
            this.buildHeaderRows(f, b)
        }
    }
}, 0, 0, 0, 0, ["exporter.excel", "exporter.excel07", "exporter.xlsx"], 0, [Ext.exporter.excel, 'Xlsx', Ext.exporter, 'Excel'], 0));
(Ext.cmd.derive('Ext.exporter.Plugin', Ext.plugin.Abstract, {
    init: function(b) {
        var a = this;
        b.saveDocumentAs = Ext.bind(a.saveDocumentAs, a);
        b.getDocumentData = Ext.bind(a.getDocumentData, a);
        a.cmp = b;
        return Ext.plugin.Abstract.prototype.init.call(this, b)
    },
    destroy: function() {
        var a = this,
            b = a.cmp;
        b.saveDocumentAs = b.getDocumentData = a.cmp = a.delayedSaveTimer = Ext.unasap(a.delayedSaveTimer);
        Ext.plugin.Abstract.prototype.destroy.call(this)
    },
    saveDocumentAs: function(a) {
        var d = this.cmp,
            b = new Ext.Deferred(),
            c = this.getExporter(a);
        d.fireEvent('beforedocumentsave', d, {
            config: a,
            exporter: c
        });
        this.delayedSaveTimer = Ext.asap(this.delayedSave, this, [c, a, b]);
        return b.promise
    },
    delayedSave: function(b, c, d) {
        var a = this.cmp;
        if (this.disabled || !a) {
            Ext.destroy(b);
            d.reject();
            return
        }
        this.setExporterData(b, c);
        b.saveAs().then(function() {
            d.resolve(c)
        }, function(a) {
            d.reject(a)
        }).always(function() {
            var e = a && !a.destroyed;
            if (e) {
                a.fireEvent('documentsave', a, {
                    config: c,
                    exporter: b
                })
            }
            Ext.destroy(b);
            if (e) {
                a.fireEvent('exportfinished', a, {
                    config: c
                })
            }
        })
    },
    getDocumentData: function(b) {
        var a, c;
        if (this.disabled) {
            return
        }
        a = this.getExporter(b);
        this.setExporterData(a, b);
        c = a.getContent();
        Ext.destroy(a);
        return c
    },
    getExporter: function(a) {
        var b = Ext.apply({
            type: 'excel'
        }, a);
        return Ext.Factory.exporter(b)
    },
    setExporterData: function(a, b) {
        var c = this.cmp;
        a.setData(this.prepareData(b));
        c.fireEvent('dataready', c, {
            config: b,
            exporter: a
        })
    },
    getExportStyle: function(b, d) {
        var f = (d && d.type),
            c, e, a;
        if (Ext.isArray(b)) {
            c = Ext.Array.pluck(b, 'type');
            a = Ext.Array.indexOf(c, undefined);
            if (a >= 0) {
                e = b[a]
            }
            a = Ext.Array.indexOf(c, f);
            return a >= 0 ? b[a] : e
        } else {
            return b
        }
    },
    prepareData: Ext.emptyFn
}, 0, 0, 0, 0, ["plugin.exporterplugin"], 0, [Ext.exporter, 'Plugin'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Worksheet', Ext.exporter.file.Base, {
    config: {
        name: 'Sheet',
        protection: null,
        rightToLeft: null,
        showGridLines: !0,
        tables: []
    },
    tpl: ['   <Worksheet ss:Name="{name:htmlEncode}"', '<tpl if="this.exists(protection)"> ss:Protected="{protection:this.toNumber}"</tpl>', '<tpl if="this.exists(rightToLeft)"> ss:RightToLeft="{rightToLeft:this.toNumber}"</tpl>', '>\n', '<tpl if="tables"><tpl for="tables.getRange()">{[values.render()]}</tpl></tpl>', '       <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">\n', '          <PageSetup>\n', '              <Layout x:CenterHorizontal="1" x:Orientation="Portrait" />\n', '              <Header x:Margin="0.3" />\n', '              <Footer x:Margin="0.3" x:Data="Page &amp;P of &amp;N" />\n', '              <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75" />\n', '          </PageSetup>\n', '          <FitToPage />\n', '          <Print>\n', '              <PrintErrors>Blank</PrintErrors>\n', '              <FitWidth>1</FitWidth>\n', '              <FitHeight>32767</FitHeight>\n', '              <ValidPrinterInfo />\n', '              <VerticalResolution>600</VerticalResolution>\n', '          </Print>\n', '          <Selected />\n', '<tpl if="!showGridLines">', '          <DoNotDisplayGridlines />\n', '</tpl>', '          <ProtectObjects>False</ProtectObjects>\n', '          <ProtectScenarios>False</ProtectScenarios>\n', '      </WorksheetOptions>\n', '   </Worksheet>\n', {
        exists: function(a) {
            return !Ext.isEmpty(a)
        },
        toNumber: function(a) {
            return Number(Boolean(a))
        }
    }],
    destroy: function() {
        this.setTables(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyTables: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.excel.Table')
    },
    addTable: function(a) {
        return this.getTables().add(a || {})
    },
    getTable: function(a) {
        return this.getTables().get(a)
    },
    applyName: function(a) {
        return Ext.String.ellipsis(String(a), 31)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Worksheet'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Table', Ext.exporter.file.Base, {
    config: {
        expandedColumnCount: null,
        expandedRowCount: null,
        fullColumns: 1,
        fullRows: 1,
        defaultColumnWidth: 48,
        defaultRowHeight: 12.75,
        styleId: null,
        leftCell: 1,
        topCell: 1,
        columns: [],
        rows: []
    },
    tpl: ['       <Table x:FullColumns="{fullColumns}" x:FullRows="{fullRows}"', '<tpl if="this.exists(expandedRowCount)"> ss:ExpandedRowCount="{expandedRowCount}"</tpl>', '<tpl if="this.exists(expandedColumnCount)"> ss:ExpandedColumnCount="{expandedColumnCount}"</tpl>', '<tpl if="this.exists(defaultRowHeight)"> ss:DefaultRowHeight="{defaultRowHeight}"</tpl>', '<tpl if="this.exists(defaultColumnWidth)"> ss:DefaultColumnWidth="{defaultColumnWidth}"</tpl>', '<tpl if="this.exists(leftCell)"> ss:LeftCell="{leftCell}"</tpl>', '<tpl if="this.exists(topCell)"> ss:TopCell="{topCell}"</tpl>', '<tpl if="this.exists(styleId)"> ss:StyleID="{styleId}"</tpl>', '>\n', '<tpl if="columns"><tpl for="columns.getRange()">{[values.render()]}</tpl></tpl>', '<tpl if="rows">', '<tpl for="rows.getRange()">{[values.render()]}</tpl>', '<tpl else>         <Row ss:AutoFitHeight="0"/>\n</tpl>', '       </Table>\n', {
        exists: function(a) {
            return !Ext.isEmpty(a)
        }
    }],
    destroy: function() {
        this.setColumns(null);
        this.setRows(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyColumns: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.excel.Column')
    },
    applyRows: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.excel.Row')
    },
    addColumn: function(a) {
        return this.getColumns().add(a || {})
    },
    getColumn: function(a) {
        return this.getColumns().get(a)
    },
    addRow: function(a) {
        return this.getRows().add(a || {})
    },
    getRow: function(a) {
        return this.getRows().get(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Table'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Style', Ext.exporter.file.Style, {
    config: {
        parentId: null,
        protection: null
    },
    checks: {
        alignment: {
            horizontal: ['Automatic', 'Left', 'Center', 'Right', 'Fill', 'Justify', 'CenterAcrossSelection', 'Distributed', 'JustifyDistributed'],
            shrinkToFit: [!0, !1],
            vertical: ['Automatic', 'Top', 'Bottom', 'Center', 'Justify', 'Distributed', 'JustifyDistributed'],
            verticalText: [!0, !1],
            wrapText: [!0, !1]
        },
        font: {
            family: ['Automatic', 'Decorative', 'Modern', 'Roman', 'Script', 'Swiss'],
            outline: [!0, !1],
            shadow: [!0, !1],
            underline: ['None', 'Single', 'Double', 'SingleAccounting', 'DoubleAccounting'],
            verticalAlign: ['None', 'Subscript', 'Superscript']
        },
        border: {
            position: ['Left', 'Top', 'Right', 'Bottom', 'DiagonalLeft', 'DiagonalRight'],
            lineStyle: ['None', 'Continuous', 'Dash', 'Dot', 'DashDot', 'DashDotDot', 'SlantDashDot', 'Double'],
            weight: [0, 1, 2, 3]
        },
        interior: {
            pattern: ['None', 'Solid', 'Gray75', 'Gray50', 'Gray25', 'Gray125', 'Gray0625', 'HorzStripe', 'VertStripe', 'ReverseDiagStripe', 'DiagStripe', 'DiagCross', 'ThickDiagCross', 'ThinHorzStripe', 'ThinVertStripe', 'ThinReverseDiagStripe', 'ThinDiagStripe', 'ThinHorzCross', 'ThinDiagCross']
        },
        protection: {
            "protected": [!0, !1],
            hideFormula: [!0, !1]
        }
    },
    tpl: ['       <Style ss:ID="{id}"', '<tpl if="this.exists(parentId)"> ss:Parent="{parentId}"</tpl>', '<tpl if="this.exists(name)"> ss:Name="{name}"</tpl>', '>\n', '<tpl if="this.exists(alignment)">           <Alignment{[this.getAttributes(values.alignment, "alignment")]}/>\n</tpl>', '<tpl if="this.exists(borders)">', '           <Borders>\n', '<tpl for="borders">               <Border{[this.getAttributes(values, "border")]}/>\n</tpl>', '           </Borders>\n', '</tpl>', '<tpl if="this.exists(font)">           <Font{[this.getAttributes(values.font, "font")]}/>\n</tpl>', '<tpl if="this.exists(interior)">           <Interior{[this.getAttributes(values.interior, "interior")]}/>\n</tpl>', '<tpl if="this.exists(format)">           <NumberFormat ss:Format="{format}"/>\n</tpl>', '<tpl if="this.exists(protection)">           <Protection{[this.getAttributes(values.protection, "protection")]}/>\n</tpl>', '       </Style>\n', {
        exists: function(a) {
            return !Ext.isEmpty(a)
        },
        getAttributes: function(b, h) {
            var f = ' ss:{0}="{1}"',
                d = Ext.Object.getKeys(b || {}),
                g = d.length,
                e = '',
                c, a;
            for (c = 0; c < g; c++) {
                a = d[c];
                e += Ext.String.format(f, Ext.String.capitalize(a), Ext.isBoolean(b[a]) ? Number(b[a]) : b[a])
            }
            return e
        }
    }],
    autoGenerateKey: ['parentId', 'protection']
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Style'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Row', Ext.exporter.file.Base, {
    config: {
        autoFitHeight: !1,
        caption: null,
        cells: [],
        height: null,
        index: null,
        span: null,
        styleId: null
    },
    tpl: ['           <Row', '<tpl if="this.exists(index)"> ss:Index="{index}"</tpl>', '<tpl if="this.exists(caption)"> c:Caption="{caption}"</tpl>', '<tpl if="this.exists(autoFitHeight)"> ss:AutoFitHeight="{autoFitHeight:this.toNumber}"</tpl>', '<tpl if="this.exists(span)"> ss:Span="{span}"</tpl>', '<tpl if="this.exists(height)"> ss:Height="{height}"</tpl>', '<tpl if="this.exists(styleId)"> ss:StyleID="{styleId}"</tpl>', '>\n', '<tpl if="cells"><tpl for="cells.getRange()">{[values.render()]}</tpl></tpl>', '           </Row>\n', {
        exists: function(a) {
            return !Ext.isEmpty(a)
        },
        toNumber: function(a) {
            return Number(Boolean(a))
        }
    }],
    destroy: function() {
        this.setCells(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyCells: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.excel.Cell')
    },
    addCell: function(a) {
        return this.getCells().add(a || {})
    },
    getCell: function(a) {
        return this.getCells().get(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Row'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Column', Ext.exporter.file.Base, {
    config: {
        autoFitWidth: !1,
        caption: null,
        hidden: null,
        index: null,
        span: null,
        styleId: null,
        width: null
    },
    tpl: ['<Column', '<tpl if="this.exists(index)"> ss:Index="{index}"</tpl>', '<tpl if="this.exists(caption)"> c:Caption="{caption}"</tpl>', '<tpl if="this.exists(styleId)"> ss:StyleID="{styleId}"</tpl>', '<tpl if="this.exists(hidden)"> ss:Hidden="{hidden}"</tpl>', '<tpl if="this.exists(span)"> ss:Span="{span}"</tpl>', '<tpl if="this.exists(width)"> ss:Width="{width}"</tpl>', '<tpl if="this.exists(autoFitWidth)"> ss:AutoFitWidth="{autoFitWidth:this.toNumber}"</tpl>', '/>\n', {
        exists: function(a) {
            return !Ext.isEmpty(a)
        },
        toNumber: function(a) {
            return Number(Boolean(a))
        }
    }]
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Column'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Cell', Ext.exporter.file.Base, {
    config: {
        dataType: 'String',
        formula: null,
        index: null,
        styleId: null,
        mergeAcross: null,
        mergeDown: null,
        value: ''
    },
    tpl: ['               <Cell', '<tpl if="this.exists(index)"> ss:Index="{index}"</tpl>', '<tpl if="this.exists(styleId)"> ss:StyleID="{styleId}"</tpl>', '<tpl if="this.exists(mergeAcross)"> ss:MergeAcross="{mergeAcross}"</tpl>', '<tpl if="this.exists(mergeDown)"> ss:MergeDown="{mergeDown}"</tpl>', '<tpl if="this.exists(formula)"> ss:Formula="{formula}"</tpl>', '>\n', '                   <Data ss:Type="{dataType}">{value}</Data>\n', '               </Cell>\n', {
        exists: function(a) {
            return !Ext.isEmpty(a)
        }
    }],
    applyValue: function(a) {
        var b = 'String',
            c = Ext.util.Format;
        if (a instanceof Date) {
            b = 'DateTime';
            a = Ext.Date.format(a, 'Y-m-d\\TH:i:s.u')
        } else if (Ext.isNumber(a)) {
            b = 'Number'
        } else if (Ext.isBoolean(a)) {
            b = 'Boolean';
            a = Number(a)
        } else {
            a = c.htmlEncode(c.htmlDecode(a))
        }
        this.setDataType(b);
        return a
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Cell'], 0));
(Ext.cmd.derive('Ext.exporter.file.excel.Workbook', Ext.exporter.file.Base, {
    config: {
        title: "Workbook",
        author: 'Sencha',
        windowHeight: 9000,
        windowWidth: 50000,
        protectStructure: !1,
        protectWindows: !1,
        styles: [],
        worksheets: []
    },
    tpl: ['<?xml version="1.0" encoding="utf-8"?>\n', '<?mso-application progid="Excel.Sheet"?>\n', '<Workbook ', 'xmlns="urn:schemas-microsoft-com:office:spreadsheet" ', 'xmlns:o="urn:schemas-microsoft-com:office:office" ', 'xmlns:x="urn:schemas-microsoft-com:office:excel" ', 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ', 'xmlns:html="http://www.w3.org/TR/REC-html40">\n', '   <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">\n', '       <Title>{title:htmlEncode}</Title>\n', '       <Author>{author:htmlEncode}</Author>\n', '       <Created>{createdAt}</Created>\n', '   </DocumentProperties>\n', '   <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">\n', '       <WindowHeight>{windowHeight}</WindowHeight>\n', '       <WindowWidth>{windowWidth}</WindowWidth>\n', '       <ProtectStructure>{protectStructure}</ProtectStructure>\n', '       <ProtectWindows>{protectWindows}</ProtectWindows>\n', '   </ExcelWorkbook>\n', '   <Styles>\n', '<tpl if="styles"><tpl for="styles.getRange()">{[values.render()]}</tpl></tpl>', '   </Styles>\n', '<tpl if="worksheets"><tpl for="worksheets.getRange()">{[values.render()]}</tpl></tpl>', '</Workbook>'],
    destroy: function() {
        this.setStyles(null);
        this.setWorksheets(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyStyles: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.excel.Style')
    },
    applyWorksheets: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.excel.Worksheet')
    },
    addStyle: function(h) {
        var e = this.getStyles(),
            f = e.decodeItems(arguments, 0),
            g = f.length,
            c = [],
            d, a, b;
        for (d = 0; d < g; d++) {
            b = f[d];
            a = e.get(b.getKey());
            c.push(a ? a : e.add(b));
            if (a) {
                b.destroy()
            }
        }
        return c.length === 1 ? c[0] : c
    },
    getStyle: function(a) {
        return this.getStyles().get(a)
    },
    addWorksheet: function(a) {
        return this.getWorksheets().add(a || {})
    },
    getWorksheet: function(a) {
        return this.getWorksheets().get(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.excel, 'Workbook'], 0));
(Ext.cmd.derive('Ext.exporter.excel.Xml', Ext.exporter.Base, {
    config: {
        windowHeight: 9000,
        windowWidth: 50000,
        protectStructure: !1,
        protectWindows: !1,
        defaultStyle: {
            alignment: {
                vertical: 'Top'
            },
            font: {
                fontName: 'Calibri',
                family: 'Swiss',
                size: 11,
                color: '#000000'
            }
        },
        titleStyle: {
            name: 'Title',
            parentId: 'Default',
            alignment: {
                horizontal: 'Center',
                vertical: 'Center'
            },
            font: {
                fontName: 'Cambria',
                family: 'Swiss',
                size: 18,
                color: '#1F497D'
            }
        },
        groupHeaderStyle: {
            name: 'Group Header',
            parentId: 'Default',
            borders: [{
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        groupFooterStyle: {
            borders: [{
                position: 'Top',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        tableHeaderStyle: {
            name: 'Heading 1',
            parentId: 'Default',
            alignment: {
                horizontal: 'Center',
                vertical: 'Center'
            },
            borders: [{
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }],
            font: {
                fontName: 'Calibri',
                family: 'Swiss',
                size: 11,
                color: '#1F497D'
            }
        }
    },
    fileName: 'export.xml',
    mimeType: 'text/xml',
    titleRowHeight: 22.5,
    headerRowHeight: 20.25,
    destroy: function() {
        var a = this;
        a.workbook = a.table = Ext.destroy(a.workbook);
        Ext.exporter.Base.prototype.destroy.call(this)
    },
    applyDefaultStyle: function(a) {
        return Ext.applyIf({
            id: 'Default',
            name: 'Normal'
        }, a || {})
    },
    getContent: function() {
        var a = this,
            b = this.getConfig(),
            c = b.data,
            d;
        a.workbook = new Ext.exporter.file.excel.Workbook({
            title: b.title,
            author: b.author,
            windowHeight: b.windowHeight,
            windowWidth: b.windowWidth,
            protectStructure: b.protectStructure,
            protectWindows: b.protectWindows
        });
        a.table = a.workbook.addWorksheet({
            name: b.title
        }).addTable();
        a.workbook.addStyle(b.defaultStyle);
        a.tableHeaderStyleId = a.workbook.addStyle(b.tableHeaderStyle).getId();
        a.groupHeaderStyleId = a.workbook.addStyle(b.groupHeaderStyle).getId();
        d = c ? c.getColumnCount() : 1;
        a.addTitle(b, d);
        if (c) {
            a.buildHeader();
            a.table.addRow(a.buildRows(c, d, -1))
        }
        a.columnStylesFooter = a.columnStylesNormal = null;
        a.headerStyles = a.footerStyles = null;
        return a.workbook.render()
    },
    addTitle: function(a, b) {
        if (!Ext.isEmpty(a.title)) {
            this.table.addRow({
                autoFitHeight: 1,
                height: this.titleRowHeight,
                styleId: this.workbook.addStyle(a.titleStyle).getId()
            }).addCell({
                mergeAcross: b - 1,
                value: a.title
            })
        }
    },
    buildRows: function(a, r, l) {
        var c = this,
            q = c.getShowSummary(),
            e = [],
            i, h, k, t, f, p, b, d, u, o, n, j, g, s, m;
        if (!a) {
            return
        }
        i = a._groups;
        s = a._text;
        j = (!i && !a._rows);
        if (q !== !1 && !Ext.isEmpty(s) && !j) {
            k = c.getGroupHeaderStyleByLevel(l);
            e.push({
                cells: [{
                    mergeAcross: r - 1,
                    value: s,
                    styleId: k
                }]
            })
        }
        if (i) {
            u = i.length;
            for (p = 0; p < u; p++) {
                Ext.Array.insert(e, e.length, c.buildRows(i.items[p], r, l + 1))
            }
        }
        if (a._rows) {
            o = a._rows.length;
            if (q === !1) {
                k = c.getGroupHeaderStyleByLevel(l);
                e.push({
                    cells: [{
                        mergeAcross: r - 1,
                        value: a._text,
                        styleId: k
                    }]
                })
            }
            for (d = 0; d < o; d++) {
                h = {
                    cells: []
                };
                f = a._rows.items[d]._cells;
                n = f.length;
                for (b = 0; b < n; b++) {
                    g = f.items[b];
                    m = c.columnStylesNormal[b];
                    h.cells.push({
                        value: g._value,
                        styleId: c.getCellStyleId(g._style, m)
                    })
                }
                e.push(h)
            }
        }
        if (a._summaries && (q || j)) {
            t = c.getGroupFooterStyleByLevel(l);
            o = a._summaries.length;
            for (d = 0; d < o; d++) {
                h = {
                    cells: []
                };
                f = a._summaries.items[d]._cells;
                n = f.length;
                for (b = 0; b < n; b++) {
                    g = f.items[b];
                    m = j ? c.columnStylesNormal[b] : (b === 0 ? t : c.columnStylesFooter[b]);
                    h.cells.push({
                        value: g._value,
                        styleId: c.getCellStyleId(g._style, m)
                    })
                }
                e.push(h)
            }
        }
        a.destroy();
        return e
    },
    getCellStyleId: function(b, a) {
        var c = Ext.applyIf({
            parentId: a
        }, b);
        return b ? this.workbook.addStyle(c).getId() : a
    },
    getGroupHeaderStyleByLevel: function(c) {
        var b = this,
            d = 'l' + c,
            a = b.headerStyles;
        if (!a) {
            b.headerStyles = a = {}
        }
        if (!a.hasOwnProperty(d)) {
            a[d] = b.workbook.addStyle({
                parentId: b.groupHeaderStyleId,
                alignment: {
                    Indent: c > 0 ? c : 0
                }
            }).getId()
        }
        return a[d]
    },
    getGroupFooterStyleByLevel: function(c) {
        var b = this,
            d = 'l' + c,
            a = b.footerStyles;
        if (!a) {
            b.footerStyles = a = {}
        }
        if (!a.hasOwnProperty(d)) {
            a[d] = b.workbook.addStyle({
                parentId: b.columnStylesFooter[0],
                alignment: {
                    Indent: c > 0 ? c : 0
                }
            }).getId()
        }
        return a[d]
    },
    buildHeader: function() {
        var a = this,
            j = {},
            l = a.getData(),
            i, n, h, b, m, e, c, d, k, g, f;
        a.buildHeaderRows(l.getColumns(), j);
        i = Ext.Object.getKeys(j);
        m = i.length;
        for (h = 0; h < m; h++) {
            n = a.table.addRow({
                height: a.headerRowHeight,
                autoFitHeight: 1
            });
            d = j[i[h]];
            e = d.length;
            for (b = 0; b < e; b++) {
                n.addCell(d[b]).setStyleId(a.tableHeaderStyleId)
            }
        }
        d = l.getBottomColumns();
        e = d.length;
        a.columnStylesNormal = [];
        a.columnStylesFooter = [];
        k = a.getGroupFooterStyle();
        for (b = 0; b < e; b++) {
            g = d[b];
            f = {
                width: g.getWidth()
            };
            c = Ext.applyIf({
                parentId: 'Default'
            }, k);
            c = Ext.merge(c, g.getStyle());
            c.id = null;
            a.columnStylesFooter.push(a.workbook.addStyle(c).getId());
            c = Ext.merge({
                parentId: 'Default'
            }, g.getStyle());
            f.styleId = a.workbook.addStyle(c).getId();
            a.columnStylesNormal.push(f.styleId);
            a.table.addColumn(f)
        }
    },
    buildHeaderRows: function(e, b) {
        var a, f, d, g, c;
        if (!e) {
            return
        }
        g = e.length;
        for (d = 0; d < g; d++) {
            a = e.items[d].getConfig();
            a.value = a.text;
            f = a.columns;
            delete(a.columns);
            delete(a.table);
            c = 's' + a.level;
            b[c] = b[c] || [];
            b[c].push(a);
            this.buildHeaderRows(f, b)
        }
    }
}, 0, 0, 0, 0, ["exporter.excel03"], 0, [Ext.exporter.excel, 'Xml'], 0));
(Ext.cmd.derive('Ext.exporter.file.html.Style', Ext.exporter.file.Style, {
    idPrefix: 'ext-',
    indentation: 10,
    mappings: {
        readingOrder: {
            LeftToRight: 'ltr',
            RightToLeft: 'rtl',
            Context: 'initial',
            Automatic: 'initial'
        },
        horizontal: {
            Automatic: 'initial',
            Left: 'left',
            Center: 'center',
            Right: 'right',
            Justify: 'justify'
        },
        vertical: {
            Top: 'top',
            Bottom: 'bottom',
            Center: 'middle',
            Automatic: 'baseline'
        },
        lineStyle: {
            None: 'none',
            Continuous: 'solid',
            Dash: 'dashed',
            Dot: 'dotted'
        }
    },
    updateId: function(a) {
        if (a && !this.getName()) {
            this.setName('.' + a)
        }
    },
    render: function() {
        var e = this.getConfig(),
            g = this.mappings,
            a = '',
            b = e.alignment,
            c = e.font,
            j = e.borders,
            i = e.interior,
            h, k, f, d;
        if (b) {
            if (b.horizontal) {
                a += 'text-align: ' + g.horizontal[b.horizontal] + ';\n'
            }
            if (b.readingOrder) {
                a += 'direction: ' + g.readingOrder[b.readingOrder] + ';\n'
            }
            if (b.vertical) {
                a += 'vertical-align: ' + g.vertical[b.vertical] + ';\n'
            }
            if (b.indent) {
                a += 'padding-left: ' + (b.indent * this.indentation) + 'px;\n'
            }
        }
        if (c) {
            if (c.size) {
                a += 'font-size: ' + c.size + 'px;\n'
            }
            if (c.bold) {
                a += 'font-weight: bold;\n'
            }
            if (c.italic) {
                a += 'font-style: italic;\n'
            }
            if (c.strikeThrough) {
                a += 'text-decoration: line-through;\n'
            }
            if (c.underline === 'Single') {
                a += 'text-decoration: underline;\n'
            }
            if (c.color) {
                a += 'color: ' + c.color + ';\n'
            }
        }
        if (i && i.color) {
            a += 'background-color: ' + i.color + ';\n'
        }
        if (j) {
            k = j.length;
            for (h = 0; h < k; h++) {
                d = j[h];
                f = 'border-' + d.position.toLowerCase();
                a += f + '-width: ' + (d.weight || 0) + 'px;\n';
                a += f + '-style: ' + (g.lineStyle[d.lineStyle] || 'initial') + ';\n';
                a += f + '-color: ' + (d.color || 'initial') + ';\n'
            }
        }
        return e.name + '{\n' + a + '}\n'
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.html, 'Style'], 0));
(Ext.cmd.derive('Ext.exporter.file.html.Doc', Ext.exporter.file.Base, {
    config: {
        title: "Title",
        author: 'Sencha',
        charset: 'UTF-8',
        styles: [],
        table: null
    },
    destroy: function() {
        this.setStyles(null);
        this.setTable(null);
        Ext.exporter.file.Base.prototype.destroy.call(this)
    },
    applyStyles: function(b, a) {
        return this.checkCollection(b, a, 'Ext.exporter.file.html.Style')
    },
    addStyle: function(h) {
        var e = this.getStyles(),
            f = e.decodeItems(arguments, 0),
            g = f.length,
            c = [],
            d, a, b;
        for (d = 0; d < g; d++) {
            b = f[d];
            a = e.get(b.getKey());
            c.push(a ? a : e.add(b));
            if (a) {
                b.destroy()
            }
        }
        return c.length === 1 ? c[0] : c
    },
    getStyle: function(a) {
        return this.getStyles().get(a)
    }
}, 0, 0, 0, 0, 0, 0, [Ext.exporter.file.html, 'Doc'], 0));
(Ext.cmd.derive('Ext.exporter.text.CSV', Ext.exporter.Base, {
    fileName: 'export.csv',
    getHelper: function() {
        return Ext.util.CSV
    },
    getContent: function() {
        var a = this,
            b = [],
            c = a.getData();
        if (c) {
            a.buildHeader(b);
            a.buildRows(c, b, c.getColumnCount());
            a.columnStyles = Ext.destroy(a.columnStyles)
        }
        return a.getHelper().encode(b)
    },
    buildHeader: function(f) {
        var c = this,
            h = {},
            g = c.getData(),
            d, e, b, a;
        c.buildHeaderRows(g.getColumns(), h);
        f.push.apply(f, Ext.Object.getValues(h));
        d = g.getBottomColumns();
        e = d.length;
        c.columnStyles = [];
        for (b = 0; b < e; b++) {
            a = d[b].getStyle() || {};
            if (!a.id) {
                a.id = 'c' + b
            }
            a.name = '.' + a.id;
            c.columnStyles.push(new Ext.exporter.file.Style(a))
        }
    },
    buildHeaderRows: function(f, b) {
        var d, e, j, a, h, i, c, g;
        if (!f) {
            return
        }
        j = f.length;
        for (e = 0; e < j; e++) {
            d = f.items[e];
            h = d._mergeAcross;
            i = d._mergeDown;
            g = d._level;
            a = 's' + g;
            b[a] = b[a] || [];
            b[a].push(d._text);
            for (c = 1; c <= h; c++) {
                b[a].push('')
            }
            for (c = 1; c <= i; c++) {
                a = 's' + (g + c);
                b[a] = b[a] || [];
                b[a].push('')
            }
            this.buildHeaderRows(d._columns, b)
        }
    },
    buildRows: function(a, i, p) {
        var r = this._showSummary,
            h, l, c, q, e, k, d, j, m, f, n, b, g, o;
        if (!a) {
            return
        }
        h = a._groups;
        o = a._text;
        n = (!h && !a._rows);
        if (!Ext.isEmpty(o) && !n) {
            c = [];
            c.length = p;
            c[a.level || 0] = o;
            i.push(c)
        }
        if (h) {
            q = h.length;
            for (l = 0; l < q; l++) {
                this.buildRows(h.items[l], i, p)
            }
        }
        if (a._rows) {
            k = a._rows.length;
            for (e = 0; e < k; e++) {
                c = [];
                m = a._rows.items[e];
                f = m._cells;
                j = f.length;
                for (d = 0; d < j; d++) {
                    b = f.items[d];
                    g = this.columnStyles[d];
                    b = g ? g.getFormattedValue(b._value) : b._value;
                    c.push(b)
                }
                i.push(c)
            }
        }
        if (a._summaries && (r || n)) {
            k = a._summaries.length;
            for (e = 0; e < k; e++) {
                c = [];
                m = a._summaries.items[e];
                f = m._cells;
                j = f.length;
                for (d = 0; d < j; d++) {
                    b = f.items[d];
                    g = this.columnStyles[d];
                    b = g ? g.getFormattedValue(b._value) : b._value;
                    c.push(b)
                }
                i.push(c)
            }
        }
    }
}, 0, 0, 0, 0, ["exporter.csv"], 0, [Ext.exporter.text, 'CSV'], 0));
(Ext.cmd.derive('Ext.exporter.text.Html', Ext.exporter.Base, {
    config: {
        tpl: ['<!DOCTYPE html>\n', '<html>\n', '   <head>\n', '       <meta charset="{charset}">\n', '       <title>{title}</title>\n', '       <style>\n', '       table { border-collapse: collapse; border-spacing: 0; }\n', '<tpl if="styles"><tpl for="styles.getRange()">{[values.render()]}</tpl></tpl>', '       </style>\n', '   </head>\n', '   <body>\n', '       <h1>{title}</h1>\n', '       <table>\n', '           <thead>\n', '<tpl for="table.columns">', '               <tr>\n', '<tpl for=".">', '                   <th<tpl if="width"> width="{width}"</tpl><tpl if="mergeAcross"> colSpan="{mergeAcross}"</tpl><tpl if="mergeDown"> rowSpan="{mergeDown}"</tpl>>{text}</th>\n', '</tpl>', '               </tr>\n', '</tpl>', '           </thead>\n', '           <tbody>\n', '<tpl for="table.rows">', '               <tr<tpl if="cls"> class="{cls}"</tpl>>\n', '<tpl for="cells">', '                   <td<tpl if="cls"> class="{cls}"</tpl><tpl if="mergeAcross"> colSpan="{mergeAcross}"</tpl><tpl if="mergeDown"> rowSpan="{mergeDown}"</tpl>>{value}</td>\n', '</tpl>', '               </tr>\n', '</tpl>', '           </tbody>\n', '           <tfoot>\n', '               <tr>\n', '                   <th<tpl if="table.columnsCount"> colSpan="{table.columnsCount}"</tpl>>&nbsp;</th>\n', '               </tr>\n', '           </tfoot>\n', '       </table>\n', '   </body>\n', '</html>'],
        defaultStyle: {
            name: 'table tbody td, table thead th',
            alignment: {
                vertical: 'Top'
            },
            font: {
                fontName: 'Arial',
                size: 12,
                color: '#000000'
            },
            borders: [{
                position: 'Left',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }, {
                position: 'Right',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        titleStyle: {
            name: 'h1',
            font: {
                fontName: 'Arial',
                size: 18,
                color: '#1F497D'
            }
        },
        groupHeaderStyle: {
            name: '.groupHeader td',
            borders: [{
                position: 'Top',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }, {
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        groupFooterStyle: {
            name: '.groupFooter td',
            borders: [{
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        },
        tableHeaderStyle: {
            name: 'table thead th',
            alignment: {
                horizontal: 'Center',
                vertical: 'Center'
            },
            borders: [{
                position: 'Top',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }, {
                position: 'Bottom',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }],
            font: {
                fontName: 'Arial',
                size: 12,
                color: '#1F497D'
            }
        },
        tableFooterStyle: {
            name: 'table tfoot th',
            borders: [{
                position: 'Top',
                lineStyle: 'Continuous',
                weight: 1,
                color: '#4F81BD'
            }]
        }
    },
    fileName: 'export.html',
    mimeType: 'text/html',
    getContent: function() {
        var b = this,
            a = b.getConfig(),
            c = a.data,
            e = {
                columnsCount: 0,
                columns: [],
                rows: []
            },
            d, f;
        b.doc = new Ext.exporter.file.html.Doc({
            title: a.title,
            author: a.author,
            tpl: a.tpl,
            styles: [a.defaultStyle, a.titleStyle, a.groupHeaderStyle, a.groupFooterStyle, a.tableHeaderStyle, a.tableFooterStyle]
        });
        if (c) {
            d = c.getColumnCount();
            Ext.apply(e, {
                columnsCount: c.getColumnCount(),
                columns: b.buildHeader(),
                rows: b.buildRows(c, d, 0)
            })
        }
        b.doc.setTable(e);
        f = b.doc.render();
        b.doc = b.columnStyles = Ext.destroy(b.doc);
        return f
    },
    buildRows: function(d, r, b) {
        var f = this,
            t = f._showSummary,
            h = [],
            k, j, o, g, c, s, n, m, a, p, i, l, e, q;
        if (!d) {
            return h
        }
        f.doc.addStyle({
            id: '.levelHeader' + b,
            name: '.levelHeader' + b,
            alignment: {
                Horizontal: 'Left',
                Indent: (b > 0 ? b : 0) * 5
            }
        });
        f.doc.addStyle({
            id: '.levelFooter' + b,
            name: '.levelFooter' + b,
            alignment: {
                Horizontal: 'Left',
                Indent: (b > 0 ? b : 0) * 5
            }
        });
        k = d._groups;
        q = d._text;
        l = (!k && !d._rows);
        if (!Ext.isEmpty(q) && !l) {
            h.push({
                cls: 'groupHeader',
                cells: [{
                    value: q,
                    mergeAcross: r,
                    cls: 'levelHeader' + b
                }]
            })
        }
        if (k) {
            s = k.length;
            for (o = 0; o < s; o++) {
                Ext.Array.insert(h, h.length, f.buildRows(k.items[o], r, b + 1))
            }
        }
        if (d._rows) {
            n = d._rows.length;
            for (g = 0; g < n; g++) {
                j = [];
                p = d._rows.items[g];
                i = p._cells;
                m = i.length;
                for (c = 0; c < m; c++) {
                    a = i.items[c].getConfig();
                    e = f.columnStyles[c];
                    if (e) {
                        a.cls = e._id + (a.style ? ' ' + f.doc.addStyle(a.style)._id : '');
                        a.value = e.getFormattedValue(a.value)
                    }
                    j.push(a)
                }
                h.push({
                    cells: j
                })
            }
        }
        if (d._summaries && (t || l)) {
            n = d._summaries.length;
            for (g = 0; g < n; g++) {
                j = [];
                p = d._summaries.items[g];
                i = p._cells;
                m = i.length;
                for (c = 0; c < m; c++) {
                    a = i.items[c].getConfig();
                    e = f.columnStyles[c];
                    a.cls = (c === 0 ? 'levelFooter' + b : '');
                    if (e) {
                        a.cls += ' ' + e.getId() + (a.style ? ' ' + f.doc.addStyle(a.style).getId() : '');
                        a.value = e.getFormattedValue(a.value)
                    }
                    j.push(a)
                }
                h.push({
                    cls: 'groupFooter' + (l ? ' groupHeader' : ''),
                    cells: j
                })
            }
        }
        return h
    },
    buildHeader: function() {
        var b = this,
            g = {},
            f = b.getData(),
            d, e, c, a;
        b.buildHeaderRows(f.getColumns(), g);
        d = f.getBottomColumns();
        e = d.length;
        b.columnStyles = [];
        for (c = 0; c < e; c++) {
            a = d[c].getStyle() || {};
            if (!a.id) {
                a.id = Ext.id()
            }
            a.name = '.' + a.id;
            b.columnStyles.push(b.doc.addStyle(a))
        }
        return Ext.Object.getValues(g)
    },
    buildHeaderRows: function(e, b) {
        var a, d, f, c, g;
        if (!e) {
            return
        }
        f = e.length;
        for (d = 0; d < f; d++) {
            a = e.items[d].getConfig();
            c = 's' + a.level;
            b[c] = b[c] || [];
            if (a.mergeAcross) {
                a.mergeAcross++
            }
            if (a.mergeDown) {
                a.mergeDown++
            }
            b[c].push(a);
            this.buildHeaderRows(a.columns, b)
        }
    }
}, 0, 0, 0, 0, ["exporter.html"], 0, [Ext.exporter.text, 'Html'], 0));
(Ext.cmd.derive('Ext.exporter.text.TSV', Ext.exporter.text.CSV, {
    getHelper: function() {
        return Ext.util.TSV
    }
}, 0, 0, 0, 0, ["exporter.tsv"], 0, [Ext.exporter.text, 'TSV'], 0));
(Ext.cmd.derive('Ext.grid.plugin.BaseExporter', Ext.exporter.Plugin, {
    prepareData: function(b) {
        var c = this,
            a = c.cmp.getStore(),
            d = new Ext.exporter.data.Table(),
            f, e;
        f = c.getColumnHeaders(b, c.getGridColumns());
        d.setColumns(f.headers);
        if (!a || (a && a.destroyed)) {
            return d
        }
        e = c.prepareDataIndexColumns(b, f.dataIndexes);
        if (a.isTreeStore) {
            c.extractNodeData(b, d, e, a.getRoot())
        } else {
            if (b && b.includeGroups && a.isGrouped()) {
                c.extractData(b, d, e, a.getGroups());
                c.extractSummaryRow(b, d, e, a)
            } else {
                c.extractRows(b, d, e, a)
            }
        }
        return d
    },
    getColumnHeaders: function(f, d) {
        var e = [],
            c = [],
            g = d.length,
            b, a;
        for (b = 0; b < g; b++) {
            a = this.getColumnHeader(f, d[b]);
            if (a) {
                e.push(a.header);
                Ext.Array.insert(c, c.length, a.dataIndexes)
            }
        }
        return {
            headers: e,
            dataIndexes: c
        }
    },
    getGridColumns: function() {
        return []
    },
    getColumnHeader: Ext.emptyFn,
    prepareDataIndexColumns: function(d, b) {
        var e = b.length,
            c = [],
            a;
        for (a = 0; a < e; a++) {
            c.push(this.prepareDataIndexColumn(d, b[a]))
        }
        return c
    },
    prepareDataIndexColumn: function(b, a) {
        return {
            column: a,
            fn: Ext.emptyFn
        }
    },
    extractData: function(g, i, f, c) {
        var b, h, e, a, d;
        if (!c) {
            return
        }
        h = c.getCount();
        for (b = 0; b < h; b++) {
            a = c.getAt(b);
            e = a.getGroups();
            d = i.addGroup({
                text: a.getGroupKey()
            });
            if (e) {
                this.extractData(g, d, f, e)
            } else {
                this.extractRows(g, d, f, a)
            }
        }
    },
    extractNodeData: function(n, k, f, h) {
        var j = this,
            o = j.cmp.getStore(),
            m = f.length,
            c, d, a, i, b, g, e, l;
        if (h && h.hasChildNodes()) {
            e = h.childNodes;
            l = e.length;
            for (c = 0; c < l; c++) {
                a = e[c];
                i = {
                    cells: []
                };
                for (d = 0; d < m; d++) {
                    g = f[d];
                    b = j.getCell(o, a, g) || {
                        value: null
                    };
                    if (g.column.isTreeColumn && b) {
                        b.style = Ext.merge(b.style || {}, {
                            alignment: {
                                indent: a.getDepth() - 1
                            }
                        })
                    }
                    i.cells.push(b)
                }
                k.addRow(i);
                if (a.hasChildNodes()) {
                    j.extractNodeData(n, k, f, a)
                }
            }
        }
    },
    extractRows: function(k, g, d, c) {
        var m = this.cmp,
            l = m.getStore(),
            n = c.getCount(),
            j = d.length,
            i = [],
            a, b, f, e, h;
        for (a = 0; a < n; a++) {
            f = c.getAt(a);
            e = {
                cells: []
            };
            for (b = 0; b < j; b++) {
                h = this.getCell(l, f, d[b]);
                e.cells.push(h || {
                    value: null
                })
            }
            i.push(e)
        }
        g.setRows(i);
        this.extractSummaryRow(k, g, d, c)
    },
    extractSummaryRow: function(h, i, b, d) {
        var g = b.length,
            a, e, c, f;
        if (h.includeSummary) {
            c = {
                cells: []
            };
            e = this.getSummaryRecord(d, b);
            for (a = 0; a < g; a++) {
                f = this.getSummaryCell(d, e, b[a]);
                c.cells.push(f || {
                    value: null
                })
            }
            i.setSummaries(c)
        }
    },
    getCell: Ext.emptyFn,
    getSummaryCell: Ext.emptyFn,
    getSummaryRecord: function(c, f) {
        var h = f.length,
            g = c.getSummaryRecord(),
            b = new Ext.data.Model({
                id: 'summary-record'
            }),
            d, a, e;
        b.beginEdit();
        b.set(g.getData());
        for (d = 0; d < h; d++) {
            a = f[d];
            if (a.summary) {
                e = c.isStore ? c.data.items.slice() : c.items.slice();
                b.set(a.summaryIndex, a.summary.calculate(e, a.summaryIndex, 'data', 0, e.length))
            } else if (a.summaryType) {
                b.set(a.summaryIndex, this.getSummary(c, a.summaryType, a.summaryIndex))
            }
        }
        b.endEdit();
        b.commit(!0);
        return b
    },
    getSummary: function(a, c, b) {
        var d = a.isStore;
        if (c) {
            if (Ext.isFunction(c)) {
                if (d) {
                    return a.aggregate(c, null, !1, [b])
                } else {
                    return a.aggregate(b, c)
                }
            }
            switch (c) {
                case 'count':
                    return a.count();
                case 'min':
                    return a.min(b);
                case 'max':
                    return a.max(b);
                case 'sum':
                    return a.sum(b);
                case 'average':
                    return a.average(b);
                default:
                    return null;
            }
        }
    }
}, 0, 0, 0, 0, 0, 0, [Ext.grid.plugin, 'BaseExporter'], 0));
(Ext.cmd.derive('Ext.grid.plugin.Exporter', Ext.grid.plugin.BaseExporter, {
    getGridColumns: function() {
        return this.cmp.getHeaderContainer().innerItems
    },
    getColumnHeader: function(f, b) {
        var c = [],
            a, e, d;
        a = {
            text: b.getText(),
            width: b.getWidth()
        };
        if (b.isHeaderGroup) {
            e = this.getColumnHeaders(f, b.innerItems);
            a.columns = e.headers;
            if (a.columns.length === 0) {
                a = null
            } else {
                Ext.Array.insert(c, c.length, e.dataIndexes)
            }
        } else if (!b.getHidden() && !b.getIgnoreExport()) {
            d = this.getExportStyle(b.getExportStyle(), f);
            a.style = d;
            a.width = a.width || b.getComputedWidth();
            if (d) {
                a.width = d.width || a.width
            }
            c.push(b)
        } else {
            a = null
        }
        if (a) {
            return {
                header: a,
                dataIndexes: c
            }
        }
    },
    prepareDataIndexColumn: function(e, a) {
        var b = Ext.identityFn,
            d = Ext.identityFn,
            c = this.getExportStyle(a.getExportStyle(), e);
        if (!c || (c && !c.format)) {
            b = this.getSpecialFn({
                renderer: 'renderer',
                exportRenderer: 'exportRenderer',
                formatter: 'formatter'
            }, a) || b;
            d = this.getSpecialFn({
                renderer: 'summaryRenderer',
                exportRenderer: 'exportSummaryRenderer',
                formatter: 'summaryFormatter'
            }, a) || b
        }
        return {
            dataIndex: a.getDataIndex(),
            column: a,
            fn: b,
            summary: a.getSummary(),
            summaryType: a.getSummaryType(),
            summaryIndex: a.getSummaryDataIndex() || a.getDataIndex(),
            summaryFn: d
        }
    },
    getSpecialFn: function(d, b) {
        var g = b['get' + Ext.String.capitalize(d.exportRenderer)](),
            h = b['get' + Ext.String.capitalize(d.renderer)](),
            f = b['get' + Ext.String.capitalize(d.formatter)](),
            c, e, a;
        e = b.getScope() || b.resolveListenerScope() || b;
        a = g;
        if (f && !a) {
            c = f
        } else {
            if (a === !0) {
                a = h
            }
            if (typeof a == 'string') {
                c = function() {
                    return Ext.callback(a, e, arguments, 0, b)
                }
            } else if (typeof a == 'function') {
                c = function() {
                    return a.apply(e, arguments)
                }
            }
        }
        return c
    },
    getCell: function(e, c, a) {
        var b = a.dataIndex,
            d = c.get(b);
        return {
            value: a.fn(d, c, b, null, a.column)
        }
    },
    getSummaryCell: function(e, c, a) {
        var b = a.summaryIndex,
            d = c.get(b);
        return {
            value: a.summaryFn(d, c, b, null, a.column)
        }
    }
}, 0, 0, 0, 0, ["plugin.gridexporter"], 0, [Ext.grid.plugin, 'Exporter'], 0));
(Ext.cmd.derive('Ext.exporter.excel.PivotXlsx', Ext.exporter.Base, {
    config: {
        titleStyle: {
            alignment: {
                horizontal: 'Center',
                vertical: 'Center'
            },
            font: {
                fontName: 'Arial',
                family: 'Swiss',
                size: 18,
                color: '#1F497D'
            }
        },
        matrix: null,
        pivotTableStyle: {
            name: 'PivotStyleMedium7'
        }
    },
    fileName: 'export.xlsx',
    charset: 'ascii',
    mimeType: 'application/zip',
    binary: !0,
    aggregateMap: {
        avg: 'average',
        sum: 'sum',
        count: 'count',
        countNumbers: 'countNums',
        min: 'min',
        max: 'max',
        variance: 'var',
        varianceP: 'varp',
        stdDev: 'stdDev',
        stdDevP: 'stdDevp'
    },
    titleRowHeight: 22.5,
    columnLabelsText: 'Column Labels',
    rowLabelsText: 'Row Labels',
    valuesText: 'Values',
    totalText: 'Total',
    grandTotalText: 'Grand total',
    getContent: function() {
        var c = this,
            e = c.getMatrix(),
            a, f, g, b, d;
        a = new Ext.exporter.file.ooxml.Excel({
            properties: {
                title: c.getTitle(),
                author: c.getAuthor()
            }
        });
        b = e && e.store;
        if (!e || !b || !b.isStore || b.isDestroyed) {
            a.addWorksheet({
                rows: [{
                    cells: 'Unable to export the pivot table since no raw data is available'
                }]
            });
            return a.render()
        }
        f = a.addWorksheet();
        g = a.addWorksheet();
        d = c.generateDataSheet({
            store: b,
            worksheet: g
        });
        c.generatePivotSheet({
            worksheet: f,
            data: d
        });
        d = a.render();
        a.destroy();
        return d
    },
    generateDataSheet: function(c) {
        var d = c.store,
            b = c.worksheet,
            a;
        a = this.buildStoreRows(d);
        a.worksheet = b;
        b.beginRowRendering();
        b.renderRows([a.fields]);
        b.renderRows(a.rows);
        b.endRowRendering();
        return a
    },
    generatePivotSheet: function(d) {
        var a = this,
            c = a._matrix,
            e = d.worksheet,
            b = d.data;
        e.beginRowRendering();
        a.generatePivotSheetTitle(e);
        a.setupUniqueAggregateNames(b.fields);
        a.setupUniqueValues(c.aggregate, b.uniqueValues);
        a.setupUniqueValues(c.leftAxis.dimensions, b.uniqueValues);
        a.setupUniqueValues(c.topAxis.dimensions, b.uniqueValues);
        a.generatePivotSheetData(d);
        e.endRowRendering()
    },
    generatePivotSheetTitle: function(a) {
        a.renderRow({
            height: this.titleRowHeight,
            cells: [{
                mergeAcross: 5,
                value: this.getTitle(),
                styleId: a.getWorkbook().addCellStyle(this.getTitleStyle())
            }]
        })
    },
    generatePivotSheetData: function(a) {
        var b = this,
            c = a.worksheet;
        b.generatePivotConfig(a);
        b.generatePivotLocation(a);
        b.generatePivotDataFields(a);
        b.generatePivotFields(a);
        b.generatePivotRowFields(a);
        b.generatePivotColumnFields(a);
        c.addPivotTable(a.pivotConfig)
    },
    generatePivotConfig: function(d) {
        var c = this._matrix,
            e = d.data,
            b = e.worksheet,
            f = c.leftAxis.dimensions.items.length,
            a;
        a = d.pivotConfig = {
            grandTotalCaption: c.textGrandTotalTpl,
            location: {
                ref: '',
                firstHeaderRow: 1,
                firstDataRow: 1,
                firstDataCol: 1
            },
            cacheDefinition: {
                cacheSource: {
                    worksheetSource: {
                        ref: b.getTopLeftRef() + ':' + b.getBottomRightRef(),
                        sheet: b.getName()
                    }
                },
                cacheFields: [],
                cacheRecords: {
                    items: e.cache
                }
            },
            pivotTableStyleInfo: this.getPivotTableStyle(),
            pivotFields: [],
            rowFields: [],
            colFields: [],
            dataFields: [],
            rowItems: [],
            colItems: []
        };
        a.rowHeaderCaption = this.rowLabelsText;
        a.colHeaderCaption = this.columnLabelsText;
        a.viewLayoutType = c.viewLayoutType
    },
    generatePivotLocation: function(a) {
        var b = this,
            g = b._matrix,
            o = a.worksheet,
            f = a.pivotConfig.location,
            h = a.data.uniqueValues,
            e = g.aggregate.items.length,
            c = g.leftAxis.dimensions.items.length,
            d = g.topAxis.dimensions.items.length,
            j, p, q, l, i, n, k, m;
        a.header = [];
        a.body = [];
        a.totals = [];
        a.dataIndexes = [];
        a.columns = [];
        if (c === 0 && d === 0) {
            if (e === 0) {
                b.generatePivotDataEmpty(a)
            } else {
                b.generatePivotDataAgg(a)
            }
        } else {
            f.firstDataRow += d + (e > 1 ? 1 : 0);
            f.firstDataCol = 1;
            f.firstHeaderRow = 1;
            b.generatePivotHeader(a);
            if (e === 0) {
                if (d && c) {
                    b.generatePivotDataLeftTopAgg(a)
                } else if (c) {
                    b.generatePivotDataLeft(a)
                } else {
                    b.generatePivotDataTop(a)
                }
            } else {
                if (d && c) {
                    b.generatePivotDataLeftTopAgg(a)
                } else if (c) {
                    b.generatePivotDataLeftAgg(a)
                } else {
                    b.generatePivotDataTopAgg(a)
                }
            }
            b.generatePivotBody(a);
            b.generatePivotRowTotals(a)
        }
        b.generatePivotData(a)
    },
    generatePivotDataEmpty: function(b) {
        var a;
        b.header.push({
            cells: [null, null, null]
        });
        for (a = 0; a < 17; a++) {
            b.header.push({
                cells: [null, null, null]
            })
        }
    },
    generatePivotDataAgg: function(h) {
        var a = this._matrix,
            i = h.pivotConfig.location,
            g = a.aggregate.items.length,
            j, k, b, f, e, c, d;
        f = {
            cells: [{
                value: null
            }]
        };
        e = {
            cells: [{
                value: this.totalText
            }]
        };
        h.header.push(f, e);
        for (b = 0; b < g; b++) {
            d = a.aggregate.items[b];
            f.cells.push({
                value: d.excelName
            });
            c = a.results.items.map[a.grandTotalKey + '/' + a.grandTotalKey];
            e.cells.push({
                value: c ? c.values[d.id] : null
            })
        }
        if (g > 1) {
            i.firstHeaderRow = 0
        }
    },
    generatePivotDataLeftTopAgg: function(e) {
        var b = this._matrix,
            m = e.pivotConfig,
            o = m.location,
            h = m.viewLayoutType,
            g = b.aggregate.items.length,
            f = b.leftAxis.dimensions.items.length,
            n = b.topAxis.dimensions.items.length,
            l = e.dataIndexes,
            d = e.header,
            k = d[0],
            j = d.length,
            a, c, i;
        for (a = 0; a < j; a++) {
            if (h === 'compact') {
                Ext.Array.insert(d[a].cells, 0, [{
                    value: a === j - 1 ? this.rowLabelsText : (a === 0 && g === 1 ? b.aggregate.items[0].excelName : null)
                }])
            } else {
                for (c = 0; c < f; c++) {
                    if (a === 0 && c === 0 && g === 1) {
                        i = {
                            value: b.aggregate.items[0].excelName
                        }
                    } else {
                        i = {
                            value: a === j - 1 ? b.leftAxis.dimensions.items[c].dataIndex : null
                        }
                    }
                    Ext.Array.insert(d[a].cells, c, [i])
                }
            }
        }
        if (h === 'compact') {
            k.cells.push({
                value: this.columnLabelsText
            });
            Ext.Array.insert(l, 0, [{
                aggregate: !1,
                dataIndex: b.compactViewKey
            }])
        } else {
            for (a = 0; a < f; a++) {
                Ext.Array.insert(l, a, [{
                    aggregate: !1,
                    dataIndex: b.leftAxis.dimensions.items[a].id
                }])
            }
            for (a = 0; a < n; a++) {
                k.cells.push({
                    value: b.topAxis.dimensions.items[a].dataIndex
                })
            }
            if (g > 1) {
                k.cells.push({
                    value: this.valuesText
                })
            }
        }
        this.generatePivotColTotals(e, d[1]);
        o.firstDataCol = (h === 'compact' ? 1 : f)
    },
    generatePivotDataLeftTop: function(e) {
        var b = this._matrix,
            j = e.pivotConfig,
            m = j.location,
            g = j.viewLayoutType,
            f = b.leftAxis.dimensions.items.length,
            l = b.topAxis.dimensions.items.length,
            i = e.dataIndexes,
            c = e.header,
            k = c[0],
            h = c.length,
            a, d;
        for (a = 0; a < h; a++) {
            if (g === 'compact') {
                Ext.Array.insert(c[a].cells, 0, [{
                    value: a === h - 1 ? this.rowLabelsText : null
                }])
            } else {
                for (d = 0; d < f; d++) {
                    Ext.Array.insert(c[a].cells, d, [{
                        value: a === h - 1 ? b.leftAxis.dimensions.items[d].dataIndex : null
                    }])
                }
            }
        }
        if (g === 'compact') {
            k.cells.push({
                value: this.columnLabelsText
            });
            Ext.Array.insert(i, 0, [{
                aggregate: !1,
                dataIndex: b.compactViewKey
            }])
        } else {
            for (a = 0; a < f; a++) {
                Ext.Array.insert(i, a, [{
                    aggregate: !1,
                    dataIndex: b.leftAxis.dimensions.items[a].id
                }])
            }
            for (a = 0; a < l; a++) {
                k.cells.push({
                    value: b.topAxis.dimensions.items[a].dataIndex
                })
            }
        }
        this.generatePivotColTotals(e, c[1]);
        m.firstDataCol = (g === 'compact' ? 1 : f)
    },
    generatePivotDataLeftAgg: function(b) {
        var a = this._matrix,
            h = b.pivotConfig,
            d = h.location,
            j = h.viewLayoutType,
            k = a.aggregate.items.length,
            i = a.leftAxis.dimensions.items.length,
            g = b.dataIndexes,
            f = b.header[0],
            c, e;
        if (j === 'compact') {
            f.cells.push({
                value: this.rowLabelsText
            });
            g.push({
                aggregate: !1,
                dataIndex: a.compactViewKey
            })
        } else {
            for (c = 0; c < i; c++) {
                e = a.leftAxis.dimensions.items[c];
                f.cells.push({
                    value: e.dataIndex
                });
                g.push({
                    aggregate: !1,
                    dataIndex: e.id
                })
            }
        }
        this.generatePivotColTotals(b, f);
        d.firstHeaderRow = k > 1 ? 0 : 1;
        d.firstDataRow = 1;
        d.firstDataCol = (j === 'compact' ? 1 : i)
    },
    generatePivotDataLeft: function(d) {
        var c = this._matrix,
            f = d.pivotConfig,
            j = f.location,
            h = f.viewLayoutType,
            g = c.leftAxis.dimensions.items.length,
            i = d.header[0],
            b = d.dataIndexes,
            a, e;
        if (h === 'compact') {
            i.cells.push({
                value: this.rowLabelsText
            });
            b.push({
                aggregate: !1,
                dataIndex: c.compactViewKey
            })
        } else {
            for (a = 0; a < g; a++) {
                e = c.leftAxis.dimensions.items[a];
                i.cells.push({
                    value: e.dataIndex
                });
                b.push({
                    aggregate: !1,
                    dataIndex: e.id
                })
            }
        }
        b.push({
            aggregate: !1
        });
        j.firstDataCol = (h === 'compact' ? 1 : g)
    },
    generatePivotDataTopAgg: function(c) {
        var b = this._matrix,
            i = c.pivotConfig,
            g = i.viewLayoutType,
            f = b.aggregate.items.length,
            j = b.topAxis.dimensions.items.length,
            h = c.dataIndexes,
            d = c.header,
            e = d[0],
            k = d.length,
            a;
        for (a = 0; a < k; a++) {
            Ext.Array.insert(d[a].cells, 0, [{
                value: f === 1 && a === 0 ? b.aggregate.items[0].excelName : null
            }])
        }
        Ext.Array.insert(h, 0, [{
            aggregate: !1,
            dataIndex: g === 'compact' ? b.compactViewKey : ''
        }]);
        if (g === 'compact') {
            e.cells.push({
                value: this.columnLabelsText
            })
        } else {
            for (a = 0; a < j; a++) {
                e.cells.push({
                    value: b.topAxis.dimensions.items[a].dataIndex
                })
            }
            if (f > 1) {
                e.cells.push({
                    value: this.valuesText
                })
            }
        }
        this.generatePivotColTotals(c, d[1])
    },
    generatePivotDataTop: function(b) {
        var d = this._matrix,
            e = b.pivotConfig,
            g = e.location,
            h = e.viewLayoutType,
            f = d.topAxis.dimensions.items.length,
            j = b.dataIndexes,
            c = b.header,
            i = c[0],
            k = c.length,
            a;
        Ext.Array.insert(j, 0, [{
            aggregate: !1,
            dataIndex: h === 'compact' ? d.compactViewKey : ''
        }]);
        for (a = 0; a < k; a++) {
            Ext.Array.insert(c[a].cells, 0, [{
                value: null
            }])
        }
        if (h === 'compact') {
            i.cells.push({
                value: this.columnLabelsText
            })
        } else {
            for (a = 0; a < f; a++) {
                i.cells.push({
                    value: d.topAxis.dimensions.items[a].dataIndex
                })
            }
        }
        this.generatePivotColTotals(b, c[1]);
        g.firstDataCol = 1;
        g.firstDataRow = f + 1
    },
    generatePivotHeader: function(a) {
        var j = this,
            c = a.columns,
            g, d, e, b, i, f, h;
        j.generateTopAxisColumns(a, -1);
        g = a.header;
        g.push({
            cells: []
        });
        b = c.length;
        for (d = 0; d < b; d++) {
            h = {
                cells: []
            };
            f = c[d];
            i = f.length;
            for (e = 0; e < i; e++) {
                h.cells.push({
                    value: f[e].value
                })
            }
            g.push(h)
        }
        if (b) {
            a.pivotConfig.colItems = Ext.Array.pluck(c[b - 1], 'colItem');
            a.dataIndexes = c[b - 1]
        }
    },
    generatePivotBody: function(e, a) {
        var d = this._matrix,
            b, c;
        if (!a) {
            a = d.leftAxis.getTree()
        }
        c = a.length;
        for (b = 0; b < c; b++) {
            this.generatePivotBodyItem(e, a[b])
        }
    },
    generatePivotBodyItem: function(a, b) {
        var l = this._matrix,
            q = a.worksheet,
            m = a.data.uniqueValues,
            i = a.dataIndexes,
            o = i.length,
            k = a.pivotConfig.viewLayoutType,
            p = a.body,
            n = a.pivotConfig.rowItems,
            f, d, c, g, j, h, e;
        j = {
            r: b.level,
            x: [Ext.Array.indexOf(m[b.dimension.dataIndex], b.value)]
        };
        h = {
            cells: []
        };
        for (f = 0; f < o; f++) {
            d = i[f];
            c = {
                value: null
            };
            if (d.aggregate) {
                g = l.results.items.map[b.key + '/' + d.key];
                c.value = g ? g.values[d.dataIndex] : null
            } else if (d.dataIndex === b.dimensionId) {
                c.value = b.value
            } else if (k === 'compact' && d.dataIndex === l.compactViewKey) {
                a.styles = a.styles || {};
                e = 'level' + b.level;
                a.styles[e] = a.styles[e] || q.getWorkbook().addCellStyle({
                    alignment: {
                        horizontal: 'left',
                        indent: b.level * 2
                    }
                });
                c.styleId = a.styles[e];
                c.value = b.value
            }
            h.cells.push(c)
        }
        n.push(j);
        p.push(h);
        if (b.children) {
            if (k === 'tabular') {
                this.generatePivotBodyTabularItem(a, b, b)
            } else {
                this.generatePivotBody(a, b.children)
            }
        }
    },
    generatePivotBodyTabularItem: function(f, e) {
        var o = this._matrix,
            m = f.data.uniqueValues,
            p = e.children,
            r = p.length,
            k = f.dataIndexes,
            n = k.length,
            l = f.body,
            i = l[l.length - 1].cells,
            j = f.pivotConfig.rowItems,
            q = j[j.length - 1],
            h, c, a, d, g, b;
        for (h = 0; h < r; h++) {
            b = p[h];
            if (h === 0) {
                q.x.push(Ext.Array.indexOf(m[b.dimension.dataIndex], b.value));
                for (c = 0; c < n; c++) {
                    a = k[c];
                    d = i[c];
                    if (a.aggregate) {
                        g = o.results.items.map[b.key + '/' + a.key];
                        d.value = g ? g.values[a.dataIndex] : null
                    } else if (a.dataIndex === b.dimensionId) {
                        d.value = b.value
                    }
                }
                if (b.children) {
                    this.generatePivotBodyTabularItem(f, b)
                }
            } else {
                this.generatePivotBodyItem(f, b)
            }
        }
        j.push({
            r: e.level,
            t: 'default',
            x: [Ext.Array.indexOf(m[e.dimension.dataIndex], e.value)]
        });
        i = [];
        for (c = 0; c < n; c++) {
            a = k[c];
            d = {
                value: null
            };
            if (a.aggregate) {
                g = o.results.items.map[e.key + '/' + a.key];
                d.value = g ? g.values[a.dataIndex] : null
            } else if (a.dataIndex === e.dimensionId) {
                d.value = e.data[a.dataIndex] + ' ' + this.totalText
            }
            i.push(d)
        }
        l.push({
            cells: i
        })
    },
    generatePivotRowTotals: function(e) {
        var b = this._matrix,
            j = b.aggregate.items.length,
            i = b.leftAxis.dimensions.items.length,
            g = e.dataIndexes,
            l = g.length,
            m = e.totals,
            h = [],
            k = e.pivotConfig.rowItems,
            p, a, o, n, d, c, f;
        for (a = 0; a < l; a++) {
            d = g[a];
            c = {
                value: null
            };
            if (a === 0) {
                c.value = i ? this.grandTotalText : (j ? this.totalText : null)
            } else if (d.aggregate) {
                f = b.results.items.map[b.grandTotalKey + '/' + d.key];
                c.value = f ? f.values[d.dataIndex] : null
            }
            h.push(c)
        }
        m.push({
            cells: h
        });
        k.push({
            t: 'grand',
            x: 0
        })
    },
    generatePivotColTotals: function(i, h) {
        var b = this._matrix,
            g = i.pivotConfig.colItems,
            e = i.dataIndexes,
            f = b.aggregate.items,
            d = b.aggregate.items.length,
            j = b.topAxis.dimensions.items.length,
            a, c;
        if (j === 0) {
            for (a = 0; a < d; a++) {
                c = f[a];
                h.cells.push({
                    value: c.excelName
                });
                e.push({
                    aggregate: !0,
                    key: b.grandTotalKey,
                    dataIndex: c.id
                });
                g.push({
                    t: 'grand',
                    i: a,
                    x: 0
                })
            }
        } else {
            if (d <= 1) {
                h.cells.push({
                    value: this.grandTotalText
                });
                e.push({
                    aggregate: !0,
                    key: b.grandTotalKey,
                    dataIndex: d ? f[0].id : ''
                });
                g.push({
                    t: 'grand',
                    x: 0
                })
            } else {
                for (a = 0; a < d; a++) {
                    c = f[a];
                    h.cells.push({
                        value: this.totalText + ' ' + c.excelName
                    });
                    e.push({
                        aggregate: !0,
                        key: b.grandTotalKey,
                        dataIndex: c.id
                    });
                    g.push({
                        t: 'grand',
                        i: a,
                        x: 0
                    })
                }
            }
        }
    },
    generatePivotData: function(a) {
        var c = a.worksheet,
            d = a.pivotConfig.location,
            e, b, f;
        e = a.header.length;
        for (b = 0; b < e; b++) {
            f = c.renderRow(a.header[b]);
            if (b === 0) {
                d.ref = f.first + ':'
            }
        }
        c.renderRows(a.body);
        c.renderRows(a.totals);
        d.ref += c.getBottomRightRef()
    },
    generatePivotDataFields: function(d) {
        var c = this._matrix,
            g = d.data.fields,
            f = this.aggregateMap,
            e = c.aggregate.items.length,
            b, a;
        for (b = 0; b < e; b++) {
            a = c.aggregate.items[b];
            d.pivotConfig.dataFields.push({
                name: a.excelName,
                fld: Ext.Array.indexOf(g, a.dataIndex),
                subtotal: f[a.aggregator] || 'sum',
                baseField: 0,
                baseItem: 0
            })
        }
    },
    generatePivotFields: function(h) {
        var f = this,
            e = f._matrix,
            l = h.data,
            j = l.fields,
            i = l.uniqueValues,
            n = e.aggregate.items.length,
            g, d, m, k, c, b, a;
        m = j.length;
        for (g = 0; g < m; g++) {
            c = j[g];
            b = {
                showAll: !1
            };
            for (d = 0; d < n; d++) {
                a = e.aggregate.items[d];
                if (a.dataIndex === c) {
                    b.dataField = !0;
                    break
                }
            }
            if (a = f.getDimension(e.aggregate, c)) {} else if (a = f.getDimension(e.leftAxis.dimensions, c)) {
                b.axis = 'axisRow';
                if (a.getSortable()) {
                    b.sortType = (a.direction === 'ASC' ? 'ascending' : 'descending')
                } else {
                    b.sortType = 'manual'
                }
            } else if (a = f.getDimension(e.topAxis.dimensions, c)) {
                b.axis = 'axisCol';
                if (a.getSortable()) {
                    b.sortType = (a.direction === 'ASC' ? 'ascending' : 'descending')
                } else {
                    b.sortType = 'manual'
                }
            }
            if (a) {
                b.items = [];
                k = i[c].length;
                for (d = 0; d < k; d++) {
                    b.items.push({
                        x: d
                    })
                }
                b.items.push({
                    t: 'default'
                })
            }
            h.pivotConfig.cacheDefinition.cacheFields.push({
                name: c,
                sharedItems: {
                    items: a && !a.isAggregate ? i[c] : []
                }
            });
            h.pivotConfig.pivotFields.push(b)
        }
    },
    generatePivotRowFields: function(c) {
        var b = this._matrix,
            e = c.data.fields,
            d = b.leftAxis.dimensions.items.length,
            a;
        for (a = 0; a < d; a++) {
            c.pivotConfig.rowFields.push({
                x: Ext.Array.indexOf(e, b.leftAxis.dimensions.items[a].dataIndex)
            })
        }
    },
    generatePivotColumnFields: function(c) {
        var b = this._matrix,
            f = c.data.fields,
            d = b.aggregate.items.length,
            e = b.topAxis.dimensions.items.length,
            a;
        for (a = 0; a < e; a++) {
            c.pivotConfig.colFields.push({
                x: Ext.Array.indexOf(f, b.topAxis.dimensions.items[a].dataIndex)
            })
        }
        if (d > 1) {
            c.pivotConfig.colFields.push({
                x: -2
            })
        }
    },
    buildStoreRows: function(h) {
        var a = {
                rows: [],
                cache: [],
                fields: [],
                uniqueValues: {}
            },
            f, b, e, i, k, c, j, l, d, g;
        f = h.model.getFields();
        i = f.length;
        for (b = 0; b < i; b++) {
            c = f[b].getName();
            a.fields.push(c);
            a.uniqueValues[c] = []
        }
        k = h.data.length;
        for (b = 0; b < k; b++) {
            j = [];
            g = [];
            l = h.data.items[b];
            for (e = 0; e < i; e++) {
                c = a.fields[e];
                d = l.get(c);
                j.push({
                    value: d
                });
                g.push(d);
                if (Ext.Array.indexOf(a.uniqueValues[c], d) === -1) {
                    a.uniqueValues[c].push(d)
                }
            }
            a.rows.push(j);
            a.cache.push(g)
        }
        return a
    },
    setupUniqueAggregateNames: function(h) {
        var g = this._matrix.aggregate,
            k = g.getCount(),
            j = h.length,
            i = [],
            e, d, b, c, f, a;
        for (d = 0; d < k; d++) {
            c = g.getAt(d);
            f = c.dataIndex || c.header;
            a = f;
            e = 2;
            for (b = 0; b < j; b++) {
                if (String(a).toLowerCase() === String(h[b]).toLowerCase() || Ext.Array.indexOf(i, a) >= 0) {
                    a = f + e;
                    e++;
                    b = -1
                }
            }
            i.push(a);
            c.excelName = a
        }
    },
    setupUniqueValues: function(e, d) {
        var c = e.items,
            f = c.length,
            a, b;
        for (a = 0; a < f; a++) {
            b = c[a];
            d[b.dataIndex] = Ext.Array.pluck(b.values.items, 'value')
        }
    },
    getDimension: function(a, c) {
        var b = a.findIndex('dataIndex', c, 0, !1, !0);
        return b >= 0 ? a.getAt(b) : null
    },
    generateTopAxisColumns: function(q, g, o) {
        var s = this._matrix,
            u = q.data.uniqueValues,
            b = q.columns,
            n = s.aggregate.items,
            m = [],
            p, a, j, t, h, k, d, i, e, c, f, l, r;
        if (!o) {
            o = s.topAxis.getTree()
        }
        t = o.length;
        h = n.length;
        for (p = 0; p < t; p++) {
            d = o[p];
            r = Ext.Array.indexOf(u[d.dimension.dataIndex], d.value);
            c = d.level;
            b[c] = b[c] || [];
            e = {
                value: d.value == null ? '(blank)' : d.value,
                index: r
            };
            b[c].push(e);
            if (d.children) {
                l = this.generateTopAxisColumns(q, g, d.children);
                k = l.length;
                for (a = 1; a < k; a++) {
                    b[c].push({
                        value: '',
                        empty: !0
                    })
                }
                for (a = 0; a < h; a++) {
                    i = n[a];
                    f = {
                        value: e.value + ' ' + (i.excelName)
                    };
                    b[c].push(f);
                    l.push(f);
                    k = b.length;
                    for (j = c + 1; j < k; j++) {
                        if (j === k - 1) {
                            b[j].push({
                                aggregate: !0,
                                value: '',
                                key: d.key,
                                dataIndex: i.id,
                                colItem: {
                                    t: 'default',
                                    i: a,
                                    x: r
                                }
                            })
                        } else {
                            b[j].push({
                                value: '',
                                empty: !0
                            })
                        }
                    }
                }
                Ext.Array.insert(m, m.length, l);
                g += l.length
            } else {
                if (h <= 1) {
                    m.push(e);
                    g++;
                    e.aggregate = !0;
                    e.colItem = this.getColItem(g, 0, b)
                }
                if (h === 1) {
                    e.key = d.key;
                    e.dataIndex = n[0].id
                } else if (h > 1) {
                    c++;
                    b[c] = b[c] || [];
                    for (a = 0; a < h; a++) {
                        i = n[a];
                        g++;
                        f = {
                            aggregate: !0,
                            value: i.excelName,
                            index: a,
                            key: d.key,
                            dataIndex: i.id
                        };
                        b[c].push(f);
                        f.colItem = this.getColItem(g, a, b);
                        m.push(f);
                        if (a > 0) {
                            b[c - 1].push({
                                value: '',
                                empty: !0
                            })
                        }
                    }
                }
            }
        }
        return m
    },
    getColItem: function(h, g, d) {
        var c = {
                i: g
            },
            e = 0,
            f = [],
            i = d.length,
            b, a;
        for (b = 0; b < i; b++) {
            a = d[b][h];
            if (a) {
                if (!a.empty) {
                    f.push(a.index)
                } else {}
            } else {
                e++
            }
        }
        c.r = e;
        c.x = f;
        return c
    }
}, 0, 0, 0, 0, ["exporter.pivotxlsx"], 0, [Ext.exporter.excel, 'PivotXlsx'], 0));
(Ext.cmd.derive('Ext.pivot.Aggregators', Ext.Base, {
    alternateClassName: ['Mz.aggregate.Aggregators'],
    singleton: !0,
    customText: 'Custom',
    sumText: 'Sum',
    avgText: 'Avg',
    minText: 'Min',
    maxText: 'Max',
    countText: 'Count',
    countNumbersText: 'Count numbers',
    groupSumPercentageText: 'Group sum percentage',
    groupCountPercentageText: 'Group count percentage',
    varianceText: 'Var',
    variancePText: 'Varp',
    stdDevText: 'StdDev',
    stdDevPText: 'StdDevp',
    sum: function(d, e, g, i, h) {
        var f = d.length,
            a = g.calculateAsExcel ? null : 0,
            c, b;
        for (c = 0; c < f; c++) {
            b = d[c].get(e);
            if (b !== null) {
                if (a === null) {
                    a = 0
                }
                if (typeof b === 'number') {
                    a += b
                }
            }
        }
        return a
    },
    avg: function(f, h, i, k, j) {
        var g = f.length,
            e = i.calculateAsExcel,
            a = e ? null : 0,
            b = 0,
            c, d;
        for (c = 0; c < g; c++) {
            d = f[c].get(h);
            if (typeof d === 'number') {
                if (a === null) {
                    a = 0
                }
                a += d;
                b++
            }
        }
        if (!e) {
            b = g
        }
        return (b > 0 && a !== null) ? (a / b) : null
    },
    min: function(e, f, h, j, i) {
        var g = e.length,
            b = null,
            d, a, c;
        for (d = 0; d < g; d++) {
            a = e[d].get(f);
            c = !0;
            if (h.calculateAsExcel) {
                if (a !== null) {
                    if (typeof a !== 'number') {
                        a = 0;
                        c = !1
                    }
                    if (b === null) {
                        b = a
                    }
                } else {
                    c = !1
                }
            }
            if (c && a < b) {
                b = a
            }
        }
        return b
    },
    max: function(e, f, h, j, i) {
        var g = e.length,
            b = null,
            d, a, c;
        for (d = 0; d < g; d++) {
            a = e[d].get(f);
            c = !0;
            if (h.calculateAsExcel) {
                if (a !== null) {
                    if (typeof a !== 'number') {
                        a = 0;
                        c = !1
                    }
                    if (b === null) {
                        b = a
                    }
                } else {
                    c = !1
                }
            }
            if (c && a > b) {
                b = a
            }
        }
        return b
    },
    count: function(c, f, g, i, h) {
        var d = c.length,
            a = null,
            b, e;
        if (g.calculateAsExcel) {
            for (b = 0; b < d; b++) {
                e = c[b].get(f);
                if (e !== null) {
                    if (a === null) {
                        a = 0
                    }
                    a++
                }
            }
        } else {
            a = d
        }
        return a
    },
    countNumbers: function(d, e, g, i, h) {
        var f = d.length,
            a = null,
            b, c;
        for (b = 0; b < f; b++) {
            c = d[b].get(e);
            if (c !== null) {
                if (a === null) {
                    a = 0
                }
                if (typeof c === 'number') {
                    a++
                }
            }
        }
        return g.calculateAsExcel ? a : a || 0
    },
    groupSumPercentage: function(k, i, d, h, g) {
        var j = Ext.pivot.Aggregators.sum,
            l = k.length,
            a = h.split(d.keysSeparator),
            f = null,
            c = null,
            e, b;
        if (!l) {
            return null
        }
        a.pop();
        a = a.join(d.keysSeparator);
        if (Ext.isEmpty(a)) {
            a = d.grandTotalKey
        }
        e = d.results.get(h, g);
        if (e) {
            if (e.hasValue('groupSum')) {
                f = e.getValue('groupSum')
            } else {
                f = e.calculateByFn('groupSum', i, j)
            }
        }
        b = d.results.get(a, g);
        if (b) {
            if (b.hasValue('groupSum')) {
                c = b.getValue('groupSum')
            } else {
                c = b.calculateByFn('groupSum', i, j)
            }
        }
        return (c !== null && f !== null && c !== 0) ? f / c * 100 : null
    },
    groupCountPercentage: function(k, j, d, h, g) {
        var i = Ext.pivot.Aggregators.count,
            l = k.length,
            a = h.split(d.keysSeparator),
            f = null,
            c = null,
            e, b;
        if (!l) {
            return null
        }
        a.pop();
        a = a.join(d.keysSeparator);
        if (Ext.isEmpty(a)) {
            a = d.grandTotalKey
        }
        e = d.results.get(h, g);
        if (e) {
            if (e.hasValue('groupCount')) {
                f = e.getValue('groupCount')
            } else {
                f = e.calculateByFn('groupCount', j, i)
            }
        }
        b = d.results.get(a, g);
        if (b) {
            if (b.hasValue('groupCount')) {
                c = b.getValue('groupCount')
            } else {
                c = b.calculateByFn('groupCount', j, i)
            }
        }
        return (c !== null && f !== null && c !== 0) ? f / c * 100 : null
    },
    variance: function(e, i, k, m, l) {
        var g = k.calculateAsExcel,
            c = Ext.pivot.Aggregators,
            h = g ? c.countNumbers.apply(c, arguments) : e.length,
            f = c.avg.apply(c, arguments),
            j = e.length,
            a = 0,
            d, b;
        if (f > 0) {
            for (d = 0; d < j; d++) {
                b = e[d].get(i);
                if (g) {
                    if (typeof b === 'number') {
                        a += Math.pow(b - f, 2)
                    }
                } else {
                    a += Math.pow(Ext.Number.from(b, 0) - f, 2)
                }
            }
        }
        return (a > 0 && h > 1) ? (a / (h - 1)) : null
    },
    varianceP: function(e, i, k, m, l) {
        var g = k.calculateAsExcel,
            c = Ext.pivot.Aggregators,
            h = g ? c.countNumbers.apply(c, arguments) : e.length,
            f = c.avg.apply(c, arguments),
            j = e.length,
            a = 0,
            d, b;
        if (f > 0) {
            for (d = 0; d < j; d++) {
                b = e[d].get(i);
                if (g) {
                    if (typeof b === 'number') {
                        a += Math.pow(b - f, 2)
                    }
                } else {
                    a += Math.pow(Ext.Number.from(b, 0) - f, 2)
                }
            }
        }
        return (a > 0 && h > 0) ? (a / h) : null
    },
    stdDev: function(f, e, g, d, c) {
        var a = Ext.pivot.Aggregators,
            b = a.variance.apply(a, arguments);
        return b > 0 ? Math.sqrt(b) : null
    },
    stdDevP: function(f, e, g, d, c) {
        var a = Ext.pivot.Aggregators,
            b = a.varianceP.apply(a, arguments);
        return b > 0 ? Math.sqrt(b) : null
    }
}, 0, 0, 0, 0, 0, 0, [Ext.pivot, 'Aggregators', Mz.aggregate, 'Aggregators'], 0));
(Ext.cmd.derive('Ext.pivot.MixedCollection', Ext.util.MixedCollection, {
    alternateClassName: ['Mz.aggregate.MixedCollection'],
    removeAt: function(a) {
        Ext.destroy(Ext.util.MixedCollection.prototype.removeAt.apply(this, arguments))
    },
    clear: function() {
        this.destroyItems();
        Ext.util.MixedCollection.prototype.clear.apply(this, arguments)
    },
    removeAll: function() {
        this.destroyItems();
        Ext.util.MixedCollection.prototype.removeAll.apply(this, arguments)
    },
    destroy: function() {
        this.clear()
    },
    destroyItems: function() {
        var b = this.items,
            d, a, c;
        if (b) {
            d = b.length;
            for (a = 0; a < d; a++) {
                c = b[a];
                if (c.destroy) {
                    c.destroy()
                }
            }
        }
    }
}, 0, 0, 0, 0, 0, 0, [Ext.pivot, 'MixedCollection', Mz.aggregate, 'MixedCollection'], 0));
(Ext.cmd.derive('Ext.pivot.filter.Base', Ext.Base, {
    alternateClassName: ['Mz.aggregate.filter.Abstract'],
    isFilter: !0,
    operator: null,
    value: null,
    caseSensitive: !0,
    parent: null,
    constructor: function(a) {
        var c = this,
            b = Ext.util.Format;
        c.thousandRe = new RegExp('[' + b.thousandSeparator + ']', 'g');
        c.decimalRe = new RegExp('[' + b.decimalSeparator + ']');
        Ext.apply(this, a);
        return this.callParent([a])
    },
    destroy: function() {
        var a = this;
        a.parent = a.thousandRe = a.decimalRe = null;
        a.callParent()
    },
    serialize: function() {
        var a = this;
        return Ext.apply({
            type: a.type,
            operator: a.operator,
            value: a.value,
            caseSensitive: a.caseSensitive
        }, a.getSerialArgs() || {})
    },
    getSerialArgs: Ext.emptyFn,
    isMatch: function(f) {
        var a = this,
            b = a.value,
            c, d, e, g, h;
        b = (Ext.isArray(b) ? b[0] : b) || '';
        c = a.compare(f, b);
        if (a.operator == '=') {
            return (c === 0)
        }
        if (a.operator == '!=') {
            return (c !== 0)
        }
        if (a.operator == '>') {
            return (c > 0)
        }
        if (a.operator == '>=') {
            return (c >= 0)
        }
        if (a.operator == '<') {
            return (c < 0)
        }
        if (a.operator == '<=') {
            return (c <= 0)
        }
        b = a.value;
        g = (Ext.isArray(b) ? b[0] : b) || '';
        h = (Ext.isArray(b) ? b[1] : b) || '';
        d = a.compare(f, g);
        e = a.compare(f, h);
        if (a.operator == 'between') {
            return (d >= 0 && e <= 0)
        }
        if (a.operator == 'not between') {
            return !(d >= 0 && e <= 0)
        }
        return !0
    },
    parseNumber: function(a) {
        var b;
        if (typeof a === 'number') {
            return a
        }
        if (Ext.isEmpty(a)) {
            a = ''
        }
        b = String(a).replace(this.thousandRe, '');
        b = b.replace(this.decimalRe, '.');
        if (Ext.isNumeric(b)) {
            return parseFloat(b)
        }
        return null
    },
    compare: function(a, b) {
        var c = Ext.pivot.matrix.Base.prototype.naturalSort,
            d = this.parseNumber(a),
            e = this.parseNumber(b);
        if (Ext.isNumber(d) && Ext.isNumber(e)) {
            return c(d, e)
        }
        if (Ext.isDate(a)) {
            if (Ext.isDate(b)) {
                return c(a, b)
            } else {
                return c(a, Ext.Date.parse(b, Ext.Date.defaultFormat))
            }
        }
        return (this.caseSensitive ? c(a || '', b || '') : c(String(a || '').toLowerCase(), String(b || '').toLowerCase()))
    },
    deprecated: {
        '6.0': {
            properties: {
                mztype: null,
                from: null,
                to: null
            }
        }
    }
}, 1, 0, 0, 0, ["pivotfilter.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.pivot.filter, 'Base', Mz.aggregate.filter, 'Abstract'], 0));
(Ext.cmd.derive('Ext.pivot.filter.Label', Ext.pivot.filter.Base, {
    alternateClassName: ['Mz.aggregate.filter.Label'],
    isMatch: function(b) {
        var a = this,
            c;
        if (a.operator == 'begins') {
            return Ext.String.startsWith(String(b || ''), String(a.value || ''), !a.caseSensitive)
        }
        if (a.operator == 'not begins') {
            return !Ext.String.startsWith(String(b || ''), String(a.value || ''), !a.caseSensitive)
        }
        if (a.operator == 'ends') {
            return Ext.String.endsWith(String(b || ''), String(a.value || ''), !a.caseSensitive)
        }
        if (a.operator == 'not ends') {
            return !Ext.String.endsWith(String(b || ''), String(a.value || ''), !a.caseSensitive)
        }
        if (a.operator == 'contains') {
            return a.stringContains(String(b || ''), String(a.value || ''), !a.caseSensitive)
        }
        if (a.operator == 'not contains') {
            return !a.stringContains(String(b || ''), String(a.value || ''), !a.caseSensitive)
        }
        if (a.operator == 'in') {
            return a.foundInArray(a.value)
        }
        if (a.operator == 'not in') {
            return !a.foundInArray(a.value)
        }
        return Ext.pivot.filter.Base.prototype.isMatch.apply(this, arguments)
    },
    foundInArray: function(d) {
        var c = Ext.Array.from(this.value),
            e = c.length,
            a = !1,
            b;
        if (this.caseSensitive) {
            return Ext.Array.indexOf(c, d) >= 0
        } else {
            for (b = 0; b < e; b++) {
                a = a || (String(d).toLowerCase() == String(c[b]).toLowerCase());
                if (a) {
                    break
                }
            }
            return a
        }
    },
    stringContains: function(b, a, d) {
        var c = (a.length <= b.length);
        if (c) {
            if (d) {
                b = b.toLowerCase();
                a = a.toLowerCase()
            }
            c = (b.lastIndexOf(a) >= 0)
        }
        return c
    },
    deprecated: {
        '6.0': {
            methods: {
                startsWith: Ext.emptyFn,
                endsWith: Ext.emptyFn
            }
        }
    }
}, 0, 0, 0, 0, ["pivotfilter.label"], 0, [Ext.pivot.filter, 'Label', Mz.aggregate.filter, 'Label'], 0));
(Ext.cmd.derive('Ext.pivot.filter.Value', Ext.pivot.filter.Base, {
    alternateClassName: ['Mz.aggregate.filter.Value'],
    dimensionId: '',
    topType: 'items',
    topOrder: 'top',
    topSort: !0,
    isTopFilter: !1,
    constructor: function(a) {
        var b = Ext.pivot.filter.Base.prototype.constructor.call(this, a);
        this.isTopFilter = (this.operator === 'top10');
        return b
    },
    destroy: function() {
        this.dimension = null;
        Ext.pivot.filter.Base.prototype.destroy.call(this)
    },
    getDimension: function() {
        return this.parent.matrix.aggregate.getByKey(this.dimensionId)
    },
    getSerialArgs: function() {
        var a = this;
        return {
            dimensionId: a.dimensionId,
            topType: a.topType,
            topOrder: a.topOrder,
            topSort: a.topSort
        }
    },
    applyFilter: function(e, d) {
        var a = this,
            b = a.topSort ? d : Ext.Array.clone(d),
            c = [];
        if (d.length == 0) {
            return c
        }
        a.sortItemsByGrandTotal(e, b);
        switch (a.topType) {
            case 'items':
                c = a.extractTop10Items(b);
                break;
            case 'sum':
                c = a.extractTop10Sum(b);
                break;
            case 'percent':
                c = a.extractTop10Percent(e, b);
                break;
        }
        if (!a.topSort) {
            b.length = 0
        }
        return c
    },
    extractTop10Items: function(b) {
        var d = this,
            c = [],
            a;
        for (a = 0; a < b.length; a++) {
            if (c.indexOf(b[a]['tempVar']) < 0) {
                c.push(b[a]['tempVar']);
                if (c.length > d.value || (d.value < a + 1 && a > 0)) {
                    break
                }
            }
        }
        return Ext.Array.slice(b, a)
    },
    extractTop10Sum: function(b) {
        var d = this,
            c = 0,
            a;
        for (a = 0; a < b.length; a++) {
            c += b[a]['tempVar'];
            if (c >= d.value) {
                break
            }
        }
        return Ext.Array.slice(b, a + 1)
    },
    extractTop10Percent: function(a, c) {
        var k = this,
            j = 0,
            f = c[0].key.split(a.matrix.keysSeparator),
            b, h, i, d, e, g;
        f.length--;
        d = (f.length > 0 ? f.join(a.matrix.keysSeparator) : a.matrix.grandTotalKey);
        h = (a.isLeftAxis ? d : a.matrix.grandTotalKey);
        i = (a.isLeftAxis ? a.matrix.grandTotalKey : d);
        e = a.matrix.results.get(h, i);
        g = (e ? e.getValue(k.dimensionId) : 0);
        for (b = 0; b < c.length; b++) {
            j += c[b]['tempVar'];
            if ((j * 100 / g) >= k.value) {
                break
            }
        }
        return Ext.Array.slice(c, b + 1)
    },
    sortItemsByGrandTotal: function(a, c) {
        var e = this,
            f, g, d, b;
        for (b = 0; b < c.length; b++) {
            f = (a.isLeftAxis ? c[b].key : a.matrix.grandTotalKey);
            g = (a.isLeftAxis ? a.matrix.grandTotalKey : c[b].key);
            d = a.matrix.results.get(f, g);
            c[b]['tempVar'] = (d ? d.getValue(e.dimensionId) : 0)
        }
        Ext.Array.sort(c, function(d, f) {
            var b = a.matrix.naturalSort(d['tempVar'], f['tempVar']);
            if (b < 0 && e.topOrder === 'top') {
                return 1
            }
            if (b > 0 && e.topOrder === 'top') {
                return -1
            }
            return b
        })
    }
}, 1, 0, 0, 0, ["pivotfilter.value"], 0, [Ext.pivot.filter, 'Value', Mz.aggregate.filter, 'Value'], 0));
(Ext.cmd.derive('Ext.pivot.dimension.Item', Ext.Base, {
    alternateClassName: ['Mz.aggregate.dimension.Item'],
    $configPrefixed: !1,
    $configStrict: !1,
    config: {
        id: null,
        header: '',
        dataIndex: '',
        sortIndex: '',
        width: 100,
        flex: 0,
        align: 'left',
        sortable: !0,
        direction: 'ASC',
        sorterFn: null,
        caseSensitiveSort: !0,
        filter: null,
        labelRenderer: null,
        renderer: null,
        formatter: null,
        column: null,
        exportStyle: null,
        scope: null,
        grouperFn: null,
        blankText: '(blank)',
        showZeroAsBlank: !1,
        aggregator: 'sum',
        values: []
    },
    isAggregate: !1,
    matrix: null,
    constructor: function(b) {
        var a = this;
        this.initConfig(b);
        if (!a.getId()) {
            a.setId(Ext.id())
        }
        if (!a.grouperFn) {
            a.groupFn = Ext.bind(a.defaultGrouperFn, a)
        }
        if (a.sortable) {
            if (!a.sorterFn) {
                a.sortFn = Ext.bind(a.defaultSorterFn, a)
            }
        } else {
            a.sortFn = Ext.bind(a.manualSorterFn, a)
        }
        if (Ext.isEmpty(a.getSortIndex())) {
            a.setSortIndex(a.getDataIndex())
        }
        if (a.isAggregate && !a.getFormatter() && !a.getRenderer()) {
            a.setRenderer(a.getDefaultFormatRenderer('0,000.00'))
        }
        return this.callParent([b])
    },
    destroy: function() {
        this.setConfig({
            values: null,
            grouperFn: null,
            sorterFn: null,
            filter: null,
            renderer: null,
            labelRenderer: null,
            aggregator: null
        });
        this.callParent()
    },
    serialize: function() {
        return this.getConfiguration(!0)
    },
    getConfiguration: function(b) {
        var c = this,
            a = c.getConfig();
        delete(a.values);
        if (a.filter) {
            a.filter = a.filter.serialize()
        }
        if (b && typeof a.aggregator === 'function') {
            a.aggregator = 'sum'
        }
        if (b && typeof a.renderer === 'function') {
            a.renderer = null
        }
        if (b && typeof a.labelRenderer === 'function') {
            a.labelRenderer = null
        }
        return a
    },
    applyId: function(a) {
        return a ? a : Ext.id()
    },
    updateExportStyle: function(a) {
        if (a && !a.id) {
            a.id = this.getId()
        }
    },
    applyFilter: function(a, b) {
        if (a == null) {
            return a
        }
        if (a && a.isFilter) {
            a.parent = this;
            return a
        }
        if (Ext.isObject(a)) {
            Ext.applyIf(a, {
                type: 'label',
                parent: this
            });
            a = Ext.Factory.pivotfilter(a)
        } else {
            a = !1
        }
        return a
    },
    updateAggregator: function(a) {
        var b = Ext.pivot.Aggregators;
        if (Ext.isString(a) && Ext.isFunction(b[a])) {
            this.aggregatorFn = Ext.bind(b[a], b)
        } else {
            this.aggregatorFn = a || 'sum'
        }
    },
    updateGrouperFn: function(a) {
        this.groupFn = (Ext.isFunction(a) ? Ext.bind(a, this) : a)
    },
    updateSorterFn: function(a) {
        this.sortFn = (Ext.isFunction(a) ? Ext.bind(a, this) : a)
    },
    addValue: function(a, c) {
        var b = this.values;
        if (!b.getByKey(a)) {
            b.add({
                sortValue: a,
                value: a,
                display: c
            })
        }
    },
    applyValues: function(a, c) {
        var b;
        Ext.destroy(c);
        if (a && !a.isInstance) {
            b = new Ext.pivot.MixedCollection();
            b.getKey = function(b) {
                return b.value
            };
            b.addAll(a);
            return b
        }
        return a
    },
    sortValues: function() {
        if (this.sortable) {
            this.values.sortBy(this.sortFn)
        }
    },
    defaultSorterFn: function(e, f) {
        var d = this,
            a = e.sortValue,
            b = f.sortValue,
            c;
        if (a instanceof Date) {
            a = a.getTime()
        }
        if (b instanceof Date) {
            b = b.getTime()
        }
        if (!d.caseSensitiveSort) {
            a = typeof a === 'string' ? a.toUpperCase() : a;
            b = typeof b === 'string' ? b.toUpperCase() : b
        }
        if (d.matrix.useNaturalSorting) {
            c = d.matrix.naturalSort(a, b)
        } else {
            c = (a === b ? 0 : (a < b ? -1 : 1))
        }
        if (c < 0 && d.direction === 'DESC') {
            return 1
        }
        if (c > 0 && d.direction === 'DESC') {
            return -1
        }
        return c
    },
    manualSorterFn: function(d, e) {
        var a = this.values,
            b = a ? a.indexOfKey(d.value) : 0,
            c = a ? a.indexOfKey(e.value) : 0;
        return (b === c ? 0 : (b < c ? -1 : 1))
    },
    getDefaultFormatRenderer: function(a) {
        var b = this;
        return function(c) {
            var d;
            if (Ext.isEmpty(a)) {
                return c
            }
            if (Ext.isFunction(a)) {
                return a.apply(b, arguments)
            }
            if (!Ext.isNumber(c)) {
                return c
            }
            if (b.isAggregate && c === 0 && b.showZeroAsBlank) {
                return ''
            }
            d = (c >= 0);
            c = Math.abs(c);
            c = Ext.util.Format.number(c, a);
            return d ? c : '-' + c
        }
    },
    defaultGrouperFn: function(a) {
        return a.get(this.dataIndex)
    },
    getFormatterFn: function() {
        var c = this,
            a = c.getFormatter(),
            b;
        if (a) {
            b = a.indexOf('this.') === 0;
            if (b) {
                a = a.substring(5)
            }
            a = Ext.app.bind.Template.prototype.parseFormat(a);
            if (b) {
                a.scope = null
            }
            return function(b) {
                return a.format(b, a.scope || c.getScope() || c.matrix.cmp.resolveListenerScope('self.controller') || this)
            }
        }
    },
    aggregatorFn: Ext.emptyFn,
    groupFn: Ext.emptyFn,
    sortFn: Ext.emptyFn
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.dimension, 'Item', Mz.aggregate.dimension, 'Item'], 0));
(Ext.cmd.derive('Ext.pivot.axis.Item', Ext.Base, {
    alternateClassName: ['Mz.aggregate.axis.Item'],
    level: 0,
    key: '',
    value: '',
    sortValue: '',
    name: '',
    dimensionId: '',
    dimension: null,
    children: null,
    record: null,
    records: null,
    axis: null,
    data: null,
    expanded: !1,
    constructor: function(c) {
        var a = this,
            b;
        Ext.apply(a, c || {});
        if (Ext.isEmpty(a.sortValue)) {
            a.sortValue = a.value
        }
        b = a.axis;
        a.expanded = (b && ((b.isLeftAxis && !b.matrix.collapsibleRows) || (!b.isLeftAxis && !b.matrix.collapsibleColumns)));
        a.callParent(arguments)
    },
    destroy: function() {
        var a = this;
        Ext.destroy(a.children);
        a.axis = a.data = a.dimension = a.record = a.children = a.records = null;
        a.callParent(arguments)
    },
    getTextTotal: function() {
        var a = this,
            b = Ext.XTemplate.getTpl(a.axis.matrix, 'textTotalTpl');
        return b.apply({
            groupField: a.dimension.dataIndex,
            columnName: a.dimension.dataIndex,
            name: a.name,
            rows: a.children || []
        })
    },
    expand: function(b) {
        var a = this;
        a.expanded = !0;
        if (b === !0) {
            a.expandCollapseChildrenTree(!0)
        }
        a.axis.matrix.fireEvent('groupexpand', a.axis.matrix, (a.axis.isLeftAxis ? 'row' : 'col'), a)
    },
    collapse: function(b) {
        var a = this;
        a.expanded = !1;
        if (b === !0) {
            a.expandCollapseChildrenTree(!1)
        }
        a.axis.matrix.fireEvent('groupcollapse', a.axis.matrix, (a.axis.isLeftAxis ? 'row' : 'col'), a)
    },
    expandCollapseChildrenTree: function(c) {
        var a = this,
            b;
        a.expanded = c;
        if (Ext.isArray(a.children)) {
            for (b = 0; b < a.children.length; b++) {
                a.children[b].expandCollapseChildrenTree(c)
            }
        }
    }
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.axis, 'Item', Mz.aggregate.axis, 'Item'], 0));
(Ext.cmd.derive('Ext.pivot.axis.Base', Ext.Base, {
    alternateClassName: ['Mz.aggregate.axis.Abstract'],
    dimensions: null,
    matrix: null,
    items: null,
    tree: null,
    levels: 0,
    isLeftAxis: !1,
    constructor: function(b) {
        var a = this,
            d, c;
        if (!b || !b.matrix) {
            return
        }
        a.isLeftAxis = b.isLeftAxis || a.isLeftAxis;
        a.matrix = b.matrix;
        a.tree = [];
        a.dimensions = new Ext.pivot.MixedCollection();
        a.dimensions.getKey = function(a) {
            return a.getId()
        };
        a.items = new Ext.pivot.MixedCollection();
        a.items.getKey = function(a) {
            return a.key
        };
        Ext.Array.each(Ext.Array.from(b.dimensions || []), a.addDimension, a)
    },
    destroy: function() {
        var a = this;
        Ext.destroyMembers(a, 'dimensions', 'items', 'tree');
        a.matrix = a.dimensions = a.items = a.tree = null
    },
    addDimension: function(a) {
        var b = a;
        if (!a) {
            return
        }
        if (!a.isInstance) {
            b = new Ext.pivot.dimension.Item(a)
        }
        b.matrix = this.matrix;
        this.dimensions.add(b)
    },
    addItem: function(a) {
        var b = this;
        if (!Ext.isObject(a) || Ext.isEmpty(a.key) || Ext.isEmpty(a.value) || Ext.isEmpty(a.dimensionId)) {
            return !1
        }
        a.key = String(a.key);
        a.dimension = b.dimensions.getByKey(a.dimensionId);
        a.name = a.name || Ext.callback(a.dimension.labelRenderer, a.dimension.scope || 'self.controller', [a.value], 0, b.matrix.cmp) || a.value;
        a.dimension.addValue(a.value, a.name);
        a.axis = b;
        if (!b.items.map[a.key] && a.dimension) {
            b.items.add(new Ext.pivot.axis.Item(a));
            return !0
        }
        return !1
    },
    clear: function() {
        this.items.clear();
        this.tree = null
    },
    getTree: function() {
        if (!this.tree) {
            this.buildTree()
        }
        return this.tree
    },
    expandAll: function() {
        var a = this,
            c = a.getTree(),
            d = c.length,
            b;
        for (b = 0; b < d; b++) {
            c[b].expandCollapseChildrenTree(!0)
        }
        if (d > 0) {
            a.matrix.fireEvent('groupexpand', a.matrix, (a.isLeftAxis ? 'row' : 'col'), null)
        }
    },
    collapseAll: function() {
        var a = this,
            c = a.getTree(),
            d = c.length,
            b;
        for (b = 0; b < d; b++) {
            c[b].expandCollapseChildrenTree(!1)
        }
        if (d > 0) {
            a.matrix.fireEvent('groupcollapse', a.matrix, (a.isLeftAxis ? 'row' : 'col'), null)
        }
    },
    findTreeElement: function(d, c) {
        var f = this.items,
            g = f.getCount(),
            e = !1,
            b, a;
        for (b = 0; b < g; b++) {
            a = f.items[b];
            if (Ext.isDate(c) ? Ext.Date.isEqual(a[d], c) : a[d] === c) {
                e = !0;
                break
            }
        }
        return e ? {
            level: a.level,
            node: a
        } : null
    },
    buildTree: function() {
        var b = this,
            e = b.dimensions.items,
            g = e.length,
            c, a, f, h, d;
        for (c = 0; c < g; c++) {
            e[c].sortValues()
        }
        b.tree = [];
        e = b.items.items;
        g = e.length;
        for (c = 0; c < g; c++) {
            a = e[c];
            f = String(a.key).split(b.matrix.keysSeparator);
            f = Ext.Array.slice(f, 0, f.length - 1);
            h = f.join(b.matrix.keysSeparator);
            d = b.items.map[h];
            if (d) {
                a.level = d.level + 1;
                a.data = Ext.clone(d.data || {});
                d.children = d.children || [];
                d.children.push(a)
            } else {
                a.level = 0;
                a.data = {};
                b.tree.push(a)
            }
            a.data[a.dimension.getId()] = a.name;
            b.levels = Math.max(b.levels, a.level)
        }
        b.sortTree()
    },
    rebuildTree: function() {
        var b = this.items.items,
            c = b.length,
            a;
        this.tree = null;
        for (a = 0; a < c; a++) {
            b[a].children = null
        }
        this.buildTree()
    },
    sortTree: function() {
        var a = arguments[0] || this.tree,
            e = a.length,
            c, b, d;
        if (a.length > 0) {
            c = a[0].dimension
        }
        if (c) {
            Ext.Array.sort(a, c.sortFn)
        }
        for (b = 0; b < e; b++) {
            d = a[b];
            if (d.children) {
                this.sortTree(d.children)
            }
        }
    },
    sortTreeByField: function(f, b) {
        var a = this,
            d = !1,
            c, g, e;
        if (f == a.matrix.compactViewKey) {
            d = a.sortTreeByDimension(a.tree, a.dimensions.items, b);
            g = a.dimensions.items.length;
            for (e = 0; e < g; e++) {
                a.dimensions.items[e].direction = b
            }
        } else {
            b = b || 'ASC';
            c = a.dimensions.getByKey(f);
            if (c) {
                d = a.sortTreeByDimension(a.tree, c, b);
                c.direction = b
            } else {
                d = a.sortTreeByRecords(a.tree, f, b)
            }
        }
        return d
    },
    sortTreeByDimension: function(b, f, g) {
        var c = !1,
            i = Ext.Array.from(f),
            a, e, d, h;
        b = b || [];
        e = b.length;
        if (e > 0) {
            a = b[0].dimension
        }
        if (Ext.Array.indexOf(i, a) >= 0) {
            if (a.sortable) {
                h = a.direction;
                a.direction = g;
                Ext.Array.sort(b, a.sortFn);
                a.direction = h
            }
            c = a.sortable
        }
        for (d = 0; d < e; d++) {
            c = this.sortTreeByDimension(b[d].children, f, g) || c
        }
        return c
    },
    sortTreeByRecords: function(a, d, c) {
        var b, e;
        a = a || [];
        e = a.length;
        if (e <= 0) {
            return !1
        }
        if (a[0].record) {
            this.sortTreeRecords(a, d, c)
        } else {
            this.sortTreeLeaves(a, d, c)
        }
        for (b = 0; b < e; b++) {
            this.sortTreeByRecords(a[b].children, d, c)
        }
        return !0
    },
    sortTreeRecords: function(d, b, a) {
        var c = this.matrix.naturalSort;
        a = a || 'ASC';
        Ext.Array.sort(d || [], function(h, i) {
            var e, f = h.record,
                g = i.record;
            if (!(f && f.isModel && g && g.isModel)) {
                return 0
            }
            e = c(f.get(b) || '', g.get(b) || '');
            if (e < 0 && a === 'DESC') {
                return 1
            }
            if (e > 0 && a === 'DESC') {
                return -1
            }
            return e
        })
    },
    sortTreeLeaves: function(i, h, a) {
        var g = this.matrix.naturalSort,
            f = this.matrix.results,
            b = this.matrix.model,
            e = Ext.Array.indexOf(Ext.Array.pluck(b, 'name'), h),
            d, c;
        if (e < 0) {
            return !1
        }
        d = b[e]['col'];
        c = b[e]['agg'];
        a = a || 'ASC';
        Ext.Array.sort(i || [], function(k, l) {
            var j, b, e;
            b = f.get(k.key, d);
            if (b) {
                b = b.getValue(c)
            } else {
                b = 0
            }
            e = f.get(l.key, d);
            if (e) {
                e = e.getValue(c)
            } else {
                e = 0
            }
            j = g(b, e);
            if (j < 0 && a === 'DESC') {
                return 1
            }
            if (j > 0 && a === 'DESC') {
                return -1
            }
            return j
        })
    }
}, 1, 0, 0, 0, ["pivotaxis.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.pivot.axis, 'Base', Mz.aggregate.axis, 'Abstract'], 0));
(Ext.cmd.derive('Ext.pivot.axis.Local', Ext.pivot.axis.Base, {
    alternateClassName: ['Mz.aggregate.axis.Local'],
    processRecord: function(h) {
        var c = this,
            i = [],
            f = '',
            g = !0,
            j = c.dimensions.items.length,
            b, d, a, e;
        for (e = 0; e < j; e++) {
            a = c.dimensions.items[e];
            b = Ext.callback(a.groupFn, a.scope || 'self.controller', [h], 0, c.matrix.cmp);
            d = f ? f + c.matrix.keysSeparator : '';
            b = Ext.isEmpty(b) ? a.blankText : b;
            d += c.matrix.getKey(b);
            if (a.filter instanceof Ext.pivot.filter.Label) {
                g = a.filter.isMatch(b)
            }
            if (!g) {
                break
            }
            i.push({
                value: b,
                sortValue: h.get(a.sortIndex),
                key: d,
                dimensionId: a.getId()
            });
            f = d
        }
        if (g) {
            return i
        } else {
            return null
        }
    },
    buildTree: function() {
        Ext.pivot.axis.Base.prototype.buildTree.apply(this, arguments);
        this.filterTree()
    },
    filterTree: function() {
        var a = this,
            d = a.dimensions.items.length,
            c = !1,
            b;
        for (b = 0; b < d; b++) {
            c = c || (a.dimensions.items[b].filter instanceof Ext.pivot.filter.Value)
        }
        if (!c) {
            return
        }
        a.matrix.filterApplied = !0;
        a.filterTreeItems(a.tree)
    },
    filterTreeItems: function(a) {
        var c = this,
            e, b, d;
        if (!a || !Ext.isArray(a) || a.length <= 0) {
            return
        }
        e = a[0].dimension.filter;
        if (e && (e instanceof Ext.pivot.filter.Value)) {
            if (e.isTopFilter) {
                d = e.applyFilter(c, a) || []
            } else {
                d = Ext.Array.filter(a, c.canRemoveItem, c)
            }
            c.removeRecordsFromResults(d);
            c.removeItemsFromArray(a, d);
            for (b = 0; b < d.length; b++) {
                c.removeTreeChildren(d[b])
            }
        }
        for (b = 0; b < a.length; b++) {
            if (a[b].children) {
                c.filterTreeItems(a[b].children);
                if (a[b].children.length === 0) {
                    c.items.remove(a[b]);
                    Ext.Array.erase(a, b, 1);
                    b--
                }
            }
        }
    },
    removeTreeChildren: function(a) {
        var b, c;
        if (a.children) {
            c = a.children.length;
            for (b = 0; b < c; b++) {
                this.removeTreeChildren(a.children[b])
            }
        }
        this.items.remove(a)
    },
    canRemoveItem: function(b) {
        var a = this,
            e = (a.isLeftAxis ? b.key : a.matrix.grandTotalKey),
            f = (a.isLeftAxis ? a.matrix.grandTotalKey : b.key),
            d = a.matrix.results.get(e, f),
            c = b.dimension.filter;
        return (d ? !c.isMatch(d.getValue(c.dimensionId)) : !1)
    },
    removeItemsFromArray: function(b, c) {
        for (var a = 0; a < b.length; a++) {
            if (Ext.Array.indexOf(c, b[a]) >= 0) {
                Ext.Array.erase(b, a, 1);
                a--
            }
        }
    },
    removeRecordsFromResults: function(b) {
        for (var a = 0; a < b.length; a++) {
            this.removeRecordsByItem(b[a])
        }
    },
    removeRecordsByItem: function(f) {
        var a = this,
            c, d, b, g, e;
        if (f.children) {
            a.removeRecordsFromResults(f.children)
        }
        if (a.isLeftAxis) {
            e = a.matrix.results.get(f.key, a.matrix.grandTotalKey);
            b = a.matrix.results.getByLeftKey(a.matrix.grandTotalKey)
        } else {
            e = a.matrix.results.get(a.matrix.grandTotalKey, f.key);
            b = a.matrix.results.getByTopKey(a.matrix.grandTotalKey)
        }
        if (!e) {
            return
        }
        for (d = 0; d < b.length; d++) {
            a.removeItemsFromArray(b[d].records, e.records)
        }
        c = f.key.split(a.matrix.keysSeparator);
        c.length = c.length - 1;
        while (c.length > 0) {
            if (a.isLeftAxis) {
                b = a.matrix.results.getByLeftKey(c.join(a.matrix.keysSeparator))
            } else {
                b = a.matrix.results.getByTopKey(c.join(a.matrix.keysSeparator))
            }
            for (d = 0; d < b.length; d++) {
                a.removeItemsFromArray(b[d].records, e.records)
            }
            c.length = c.length - 1
        }
    }
}, 0, 0, 0, 0, ["pivotaxis.local"], 0, [Ext.pivot.axis, 'Local', Mz.aggregate.axis, 'Local'], 0));
(Ext.cmd.derive('Ext.pivot.feature.PivotStore', Ext.Base, {
    config: {
        store: null,
        grid: null,
        matrix: null,
        clsGrandTotal: '',
        clsGroupTotal: ''
    },
    totalRowEvent: 'pivottotal',
    groupRowEvent: 'pivotgroup',
    itemRowEvent: 'pivotitem',
    constructor: function(a) {
        this.initConfig(a);
        return this.callParent(arguments)
    },
    destroy: function() {
        var a = this;
        Ext.destroy(a.storeListeners, a.matrixListeners);
        a.setConfig({
            store: null,
            matrix: null,
            grid: null
        });
        a.storeInfo = a.storeListeners = null;
        a.callParent(arguments)
    },
    updateStore: function(b) {
        var a = this;
        Ext.destroy(a.storeListeners);
        if (b) {
            a.storeListeners = b.on({
                pivotstoreremodel: a.processStore,
                scope: a,
                destroyable: !0
            })
        }
    },
    updateMatrix: function(b) {
        var a = this;
        Ext.destroy(a.matrixListeners);
        if (b) {
            a.matrixListeners = b.on({
                groupexpand: a.onGroupExpand,
                groupcollapse: a.onGroupCollapse,
                scope: a,
                destroyable: !0
            })
        }
    },
    processStore: function() {
        var c = this,
            b = c.getStore(),
            e = c.getMatrix(),
            a = [],
            f, h, d, i, g;
        if (!e || !b) {
            return
        }
        g = e.getColumns();
        b.suspendEvents();
        b.model.replaceFields(g, !0);
        c.storeInfo = {};
        if (e.rowGrandTotalsPosition == 'first') {
            a.push.apply(a, c.processGrandTotal() || [])
        }
        f = e.leftAxis.getTree();
        h = f.length;
        for (d = 0; d < h; d++) {
            i = f[d];
            a.push.apply(a, c.processGroup({
                group: i,
                previousExpanded: (d > 0 ? f[d - 1].expanded : !1)
            }) || [])
        }
        if (e.rowGrandTotalsPosition == 'last') {
            a.push.apply(a, c.processGrandTotal() || [])
        }
        b.loadData(a);
        b.resumeEvents();
        b.fireEvent('refresh', b)
    },
    processGroup: function(c) {
        var a = this,
            b = a['processGroup' + Ext.String.capitalize(a.getMatrix().viewLayoutType)];
        if (!Ext.isFunction(b)) {
            b = a.processGroupOutline
        }
        return b.call(a, c)
    },
    processGrandTotal: function() {
        var c = this,
            m = !1,
            b = c.getMatrix(),
            i = {
                key: b.grandTotalKey
            },
            l = [],
            o = b.totals.length,
            k = b.leftAxis.dimensions.items,
            n = k.length,
            f, d, g, j, h, a, e;
        for (f = 0; f < o; f++) {
            j = b.totals[f];
            a = j.record;
            g = n;
            if (a && a.isModel) {
                c.storeInfo[a.internalId] = {
                    leftKey: i.key,
                    rowStyle: '',
                    rowClasses: [c.getClsGrandTotal()],
                    rowEvent: c.totalRowEvent,
                    rendererParams: {}
                };
                for (d = 0; d < n; d++) {
                    h = k[d];
                    if (b.viewLayoutType == 'compact' || d === 0) {
                        if (b.viewLayoutType == 'compact') {
                            e = b.compactViewKey;
                            g = 1
                        } else {
                            e = h.getId()
                        }
                        a.data[e] = j.title;
                        c.storeInfo[a.internalId].rendererParams[e] = {
                            fn: 'groupOutlineRenderer',
                            group: i,
                            colspan: g,
                            hidden: !1,
                            subtotalRow: !0
                        };
                        m = !0
                    } else {
                        c.storeInfo[a.internalId].rendererParams[h.id] = {
                            fn: 'groupOutlineRenderer',
                            group: i,
                            colspan: 0,
                            hidden: m,
                            subtotalRow: !0
                        };
                        g--
                    }
                }
                c.storeInfo[a.internalId].rendererParams['topaxis'] = {
                    fn: 'topAxisRenderer'
                };
                l.push(a)
            }
        }
        return l
    },
    processGroupOutline: function(c) {
        var d = this,
            b = c['group'],
            a = [];
        if (b.record) {
            d.processRecordOutline({
                results: a,
                group: b
            })
        } else {
            d.processGroupOutlineWithChildren({
                results: a,
                group: b,
                previousExpanded: c.previousExpanded
            })
        }
        return a
    },
    processGroupOutlineWithChildren: function(b) {
        var d = this,
            h = d.getMatrix(),
            a = b['group'],
            f = b['previousExpanded'],
            g, e, c, i;
        g = (!a.expanded || (a.expanded && h.rowSubTotalsPosition == 'first'));
        e = a.expanded ? a.records.expanded : a.records.collapsed;
        d.processGroupHeaderRecordOutline({
            results: b.results,
            group: a,
            record: e,
            previousExpanded: f,
            hasSummaryData: g
        });
        if (a.expanded) {
            if (a.children) {
                i = a.children.length;
                for (c = 0; c < i; c++) {
                    if (a.children[c]['children']) {
                        d.processGroupOutlineWithChildren({
                            results: b.results,
                            group: a.children[c]
                        })
                    } else {
                        d.processRecordOutline({
                            results: b.results,
                            group: a.children[c]
                        })
                    }
                }
            }
            if (h.rowSubTotalsPosition == 'last') {
                e = a.records.footer;
                d.processGroupHeaderRecordOutline({
                    results: b.results,
                    group: a,
                    record: e,
                    previousExpanded: f,
                    subtotalRow: !0,
                    hasSummaryData: !0
                })
            }
        }
    },
    processGroupHeaderRecordOutline: function(b) {
        var a = this,
            n = a.getMatrix(),
            d = b['group'],
            c = b['record'],
            g = b['previousExpanded'],
            h = b['subtotalRow'],
            m = b['hasSummaryData'],
            j = n.leftAxis.dimensions.items,
            k = j.length,
            l = k,
            i = !1,
            f, e;
        a.storeInfo[c.internalId] = {
            leftKey: d.key,
            rowStyle: '',
            rowClasses: [a.getClsGroupTotal()],
            rowEvent: a.groupRowEvent,
            rendererParams: {}
        };
        for (f = 0; f < k; f++) {
            e = j[f];
            if (e.id == d.dimension.id) {
                a.storeInfo[c.internalId].rendererParams[e.id] = {
                    fn: 'groupOutlineRenderer',
                    group: d,
                    colspan: l,
                    hidden: !1,
                    previousExpanded: g,
                    subtotalRow: h
                };
                i = !0
            } else {
                a.storeInfo[c.internalId].rendererParams[e.id] = {
                    fn: 'groupOutlineRenderer',
                    group: d,
                    colspan: 0,
                    hidden: i,
                    previousExpanded: g,
                    subtotalRow: h
                };
                l--
            }
        }
        a.storeInfo[c.internalId].rendererParams['topaxis'] = {
            fn: (m ? 'topAxisRenderer' : 'topAxisNoRenderer'),
            group: d
        };
        b.results.push(c)
    },
    processRecordOutline: function(f) {
        var b = this,
            a = f['group'],
            g = !1,
            c = a.record,
            h = b.getMatrix().leftAxis.dimensions.items,
            i = h.length,
            d, e;
        b.storeInfo[c.internalId] = {
            leftKey: a.key,
            rowStyle: '',
            rowClasses: [],
            rowEvent: b.itemRowEvent,
            rendererParams: {}
        };
        for (d = 0; d < i; d++) {
            e = h[d];
            if (e.id == a.dimension.id) {
                g = !0
            }
            b.storeInfo[c.internalId].rendererParams[e.id] = {
                fn: 'recordOutlineRenderer',
                group: a,
                hidden: !g
            }
        }
        b.storeInfo[c.internalId].rendererParams['topaxis'] = {
            fn: 'topAxisRenderer',
            group: a
        };
        f.results.push(c)
    },
    processGroupCompact: function(c) {
        var d = this,
            b = c['group'],
            e = c['previousExpanded'],
            a = [];
        if (b.record) {
            d.processRecordCompact({
                results: a,
                group: b
            })
        } else {
            d.processGroupCompactWithChildren({
                results: a,
                group: b,
                previousExpanded: e
            })
        }
        return a
    },
    processGroupCompactWithChildren: function(b) {
        var d = this,
            g = d.getMatrix(),
            a = b['group'],
            e = b['previousExpanded'],
            f, c, h;
        f = (!a.expanded || (a.expanded && g.rowSubTotalsPosition == 'first'));
        d.processGroupHeaderRecordCompact({
            results: b.results,
            group: a,
            record: a.expanded ? a.records.expanded : a.records.collapsed,
            previousExpanded: e,
            hasSummaryData: f
        });
        if (a.expanded) {
            if (a.children) {
                h = a.children.length;
                for (c = 0; c < h; c++) {
                    if (a.children[c]['children']) {
                        d.processGroupCompactWithChildren({
                            results: b.results,
                            group: a.children[c]
                        })
                    } else {
                        d.processRecordCompact({
                            results: b.results,
                            group: a.children[c]
                        })
                    }
                }
            }
            if (g.rowSubTotalsPosition == 'last') {
                d.processGroupHeaderRecordCompact({
                    results: b.results,
                    group: a,
                    record: a.records.footer,
                    previousExpanded: e,
                    subtotalRow: !0,
                    hasSummaryData: !0
                })
            }
        }
    },
    processGroupHeaderRecordCompact: function(a) {
        var b = this,
            h = b.getMatrix(),
            d = a['group'],
            c = a['record'],
            e = a['previousExpanded'],
            g = a['subtotalRow'],
            f = a['hasSummaryData'];
        b.storeInfo[c.internalId] = {
            leftKey: d.key,
            rowStyle: '',
            rowClasses: [b.getClsGroupTotal()],
            rowEvent: b.groupRowEvent,
            rendererParams: {}
        };
        b.storeInfo[c.internalId].rendererParams[h.compactViewKey] = {
            fn: 'groupCompactRenderer',
            group: d,
            colspan: 0,
            previousExpanded: e,
            subtotalRow: g
        };
        b.storeInfo[c.internalId].rendererParams['topaxis'] = {
            fn: (f ? 'topAxisRenderer' : 'topAxisNoRenderer'),
            group: d
        };
        a.results.push(c)
    },
    processRecordCompact: function(d) {
        var a = this,
            c = d['group'],
            b = c.record;
        a.storeInfo[b.internalId] = {
            leftKey: c.key,
            rowStyle: '',
            rowClasses: [],
            rowEvent: a.itemRowEvent,
            rendererParams: {}
        };
        a.storeInfo[b.internalId].rendererParams[a.getMatrix().compactViewKey] = {
            fn: 'recordCompactRenderer',
            group: c
        };
        a.storeInfo[b.internalId].rendererParams['topaxis'] = {
            fn: 'topAxisRenderer',
            group: c
        };
        d.results.push(b)
    },
    processGroupTabular: function(c) {
        var d = this,
            b = c['group'],
            a = [];
        if (b.record) {
            d.processRecordTabular({
                results: a,
                group: b
            })
        } else {
            d.processGroupTabularWithChildren({
                results: a,
                group: b,
                previousExpanded: c.previousExpanded
            })
        }
        return a
    },
    processGroupTabularWithChildren: function(b, i) {
        var d = this,
            j = d.getMatrix(),
            a = b['group'],
            f = b['previousExpanded'],
            g, e, c, h;
        if (!i) {
            d.processGroupHeaderRecordTabular({
                results: b.results,
                group: a,
                previousExpanded: f,
                hasSummaryData: !a.expanded
            })
        }
        if (a.expanded) {
            if (a.children) {
                h = a.children.length;
                for (e = 0; e < h; e++) {
                    c = a.children[e];
                    if (e === 0 && c.children && c.expanded) {
                        d.processGroupTabularWithChildren({
                            results: b.results,
                            group: c
                        }, !0)
                    } else if (e > 0) {
                        if (c.children) {
                            d.processGroupTabularWithChildren({
                                results: b.results,
                                group: c
                            })
                        } else {
                            d.processRecordTabular({
                                results: b.results,
                                group: c
                            })
                        }
                    }
                }
            }
            if (j.rowSubTotalsPosition !== 'none') {
                g = a.records.footer;
                d.processGroupHeaderRecordTabular({
                    results: b.results,
                    group: a,
                    record: g,
                    previousExpanded: f,
                    subtotalRow: !0,
                    hasSummaryData: !0
                })
            }
        }
    },
    getTabularGroupRecord: function(b) {
        var a = b.record;
        if (!a) {
            if (!b.expanded) {
                a = b.records.collapsed
            } else {
                a = this.getTabularGroupRecord(b.children[0])
            }
        }
        return a
    },
    processGroupHeaderRecordTabular: function(i) {
        var k = this,
            f = k.getMatrix(),
            g = i['group'],
            b = i['record'],
            n = i['previousExpanded'],
            o = i['subtotalRow'],
            t = i['hasSummaryData'],
            u = f.leftAxis.dimensions,
            p = u.length,
            s = p,
            m = !1,
            d = {},
            q = [],
            r = k.itemRowEvent,
            h, c, a, j, e, l;
        if (!b) {
            a = g;
            b = a.record;
            while (!b) {
                d[a.dimension.id] = {
                    fn: 'groupTabularRenderer',
                    group: a,
                    colspan: 0,
                    hidden: !1,
                    previousExpanded: n,
                    subtotalRow: o
                };
                if (a.children) {
                    if (!a.expanded) {
                        b = a.records.collapsed;
                        d[a.dimension.id].colspan = p - a.level
                    } else {
                        a = a.children[0]
                    }
                } else {
                    b = a.record
                }
            }
            m = !1;
            for (h = 0; h < p; h++) {
                c = f.leftAxis.dimensions.items[h];
                if (d[c.id]) {
                    b.data[c.id] = d[c.id].group.name;
                    m = !0
                } else if (m) {
                    d[c.id] = {
                        fn: 'groupTabularRenderer',
                        group: g,
                        colspan: 0,
                        hidden: !0,
                        previousExpanded: n,
                        subtotalRow: o
                    }
                } else {
                    b.data[c.id] = ''
                }
            }
            if (g.level > 0) {
                l = g;
                j = l.key.split(f.keysSeparator);
                j.length--;
                e = f.leftAxis.items.getByKey(j.join(f.keysSeparator));
                while (e && e.children[0] == l) {
                    d[e.dimension.id] = {
                        fn: 'groupTabularRenderer',
                        group: e,
                        colspan: 0,
                        hidden: !1,
                        previousExpanded: n,
                        subtotalRow: o
                    };
                    b.data[e.dimension.id] = e.name;
                    l = e;
                    j = l.key.split(f.keysSeparator);
                    j.length--;
                    e = f.leftAxis.items.getByKey(j.join(f.keysSeparator))
                }
            }
        } else {
            for (h = 0; h < p; h++) {
                c = f.leftAxis.dimensions.items[h];
                d[c.id] = {
                    fn: 'groupTabularRenderer',
                    group: g,
                    colspan: 0,
                    hidden: !1,
                    previousExpanded: n,
                    subtotalRow: o
                };
                if (c.id == g.dimension.id) {
                    d[c.id].colspan = s;
                    m = !0
                } else {
                    d[c.id].hidden = m;
                    s--
                }
            }
            a = g
        }
        if (t) {
            q.push(k.getClsGroupTotal());
            r = k.groupRowEvent
        }
        k.storeInfo[b.internalId] = {
            leftKey: g.key,
            rowStyle: '',
            rowClasses: q,
            rowEvent: r,
            rendererParams: d
        };
        k.storeInfo[b.internalId].rendererParams['topaxis'] = {
            fn: 'topAxisRenderer',
            group: a
        };
        i.results.push(b)
    },
    processRecordTabular: function(f) {
        var c = this,
            b = f['group'],
            g = !1,
            a = b.record,
            h = c.getMatrix().leftAxis.dimensions.items,
            i = h.length,
            e, d;
        c.storeInfo[a.internalId] = {
            leftKey: b.key,
            rowStyle: '',
            rowClasses: [],
            rowEvent: c.itemRowEvent,
            rendererParams: {}
        };
        for (e = 0; e < i; e++) {
            d = h[e];
            if (d.id == b.dimension.id) {
                g = !0
            } else {
                a.data[d.id] = null
            }
            c.storeInfo[a.internalId].rendererParams[d.id] = {
                fn: 'recordTabularRenderer',
                group: b,
                hidden: !g
            }
        }
        c.storeInfo[a.internalId].rendererParams['topaxis'] = {
            fn: 'topAxisRenderer',
            group: b
        };
        f.results.push(a)
    },
    doExpandCollapse: function(e, d) {
        var b = this,
            c = b.getGrid(),
            a;
        a = b.getMatrix().leftAxis.findTreeElement('key', e);
        if (!a) {
            return
        }
        b.doExpandCollapseInternal(a.node, d);
        c.fireEvent((a.node.expanded ? 'pivotgroupexpand' : 'pivotgroupcollapse'), c, 'row', a.node)
    },
    doExpandCollapseInternal: function(b, h) {
        var f = this,
            a = f.getStore(),
            g = Ext.isClassic,
            e, c, d;
        b.expanded = !b.expanded;
        c = f.processGroup({
            group: b,
            previousExpanded: !1
        });
        b.expanded = !b.expanded;
        e = f.processGroup({
            group: b,
            previousExpanded: !1
        });
        if (e.length && c.length && (d = a.indexOf(c[0])) !== -1) {
            a.suspendEvents();
            if (b.expanded) {
                a.remove(a.getAt(d));
                a.insert(d, e)
            } else {
                a.remove(c);
                a.insert(d, e)
            }
            f.removeStoreInfoData(c);
            a.resumeEvents();
            if (g) {
                a.fireEvent('replace', a, d, c, e)
            } else {
                a.fireEvent('refresh', a)
            }
        }
    },
    removeStoreInfoData: function(c) {
        var d = c.length,
            b, a;
        for (a = 0; a < d; a++) {
            b = c[a];
            if (this.storeInfo[b.internalId]) {
                delete this.storeInfo[b.internalId]
            }
        }
    },
    onGroupExpand: function(c, b, a) {
        if (b == 'row') {
            if (a) {
                this.doExpandCollapseInternal(a, a.records.collapsed)
            } else {
                this.processStore()
            }
        }
    },
    onGroupCollapse: function(c, b, a) {
        if (b == 'row') {
            if (a) {
                this.doExpandCollapseInternal(a, a.records.expanded)
            } else {
                this.processStore()
            }
        }
    }
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.feature, 'PivotStore'], 0));
(Ext.cmd.derive('Ext.pivot.result.Base', Ext.Base, {
    leftKey: '',
    topKey: '',
    dirty: !1,
    values: null,
    matrix: null,
    constructor: function(b) {
        var a = this;
        Ext.apply(a, b || {});
        a.values = {};
        return a.callParent(arguments)
    },
    destroy: function() {
        var a = this;
        a.matrix = a.values = null;
        a.leftAxisItem = a.topAxisItem = null;
        return a.callParent(arguments)
    },
    calculate: Ext.emptyFn,
    calculateByFn: Ext.emptyFn,
    addValue: function(a, b) {
        this.values[a] = b
    },
    getValue: function(a) {
        return this.values[a]
    },
    hasValue: function(a) {
        return (a in this.values)
    },
    getLeftAxisItem: function() {
        return this.matrix.leftAxis.items.getByKey(this.leftKey)
    },
    getTopAxisItem: function() {
        return this.matrix.topAxis.items.getByKey(this.topKey)
    }
}, 1, 0, 0, 0, ["pivotresult.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.pivot.result, 'Base'], 0));
(Ext.cmd.derive('Ext.pivot.result.Collection', Ext.Base, {
    alternateClassName: ['Mz.aggregate.matrix.Results'],
    resultType: 'base',
    items: null,
    matrix: null,
    constructor: function(b) {
        var a = this;
        Ext.apply(a, b || {});
        a.items = new Ext.pivot.MixedCollection();
        a.items.getKey = function(a) {
            return a.leftKey + '/' + a.topKey
        };
        return a.callParent(arguments)
    },
    destroy: function() {
        var a = this;
        Ext.destroy(a.items);
        a.matrix = a.items = null;
        a.callParent(arguments)
    },
    clear: function() {
        this.items.clear()
    },
    add: function(b, c) {
        var a = this.get(b, c);
        if (!a) {
            a = this.items.add(Ext.Factory.pivotresult({
                type: this.resultType,
                leftKey: b,
                topKey: c,
                matrix: this.matrix
            }))
        }
        return a
    },
    get: function(a, b) {
        return this.items.getByKey(a + '/' + b)
    },
    remove: function(a, b) {
        this.items.removeAtKey(a + '/' + b)
    },
    getByLeftKey: function(a) {
        var b = this.items.filterBy(function(d, c) {
            var b = String(c).split('/');
            return (a == b[0])
        });
        return b.getRange()
    },
    getByTopKey: function(a) {
        var b = this.items.filterBy(function(d, c) {
            var b = String(c).split('/');
            return (b.length > 1 && a == b[1])
        });
        return b.getRange()
    },
    calculate: function() {
        var b = this.items.getCount(),
            a;
        for (a = 0; a < b; a++) {
            this.items.getAt(a).calculate()
        }
    }
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.result, 'Collection', Mz.aggregate.matrix, 'Results'], 0));
(Ext.cmd.derive('Ext.pivot.matrix.Base', Ext.util.Observable, {
    alternateClassName: ['Mz.aggregate.matrix.Abstract'],
    resultType: 'base',
    leftAxisType: 'base',
    topAxisType: 'base',
    textRowLabels: 'Row labels',
    textTotalTpl: 'Total ({name})',
    textGrandTotalTpl: 'Grand total',
    keysSeparator: '#_#',
    grandTotalKey: 'grandtotal',
    compactViewKey: '_compactview_',
    compactViewColumnWidth: 200,
    viewLayoutType: 'outline',
    rowSubTotalsPosition: 'first',
    rowGrandTotalsPosition: 'last',
    colSubTotalsPosition: 'last',
    colGrandTotalsPosition: 'last',
    showZeroAsBlank: !1,
    calculateAsExcel: !1,
    leftAxis: null,
    topAxis: null,
    aggregate: null,
    results: null,
    pivotStore: null,
    isDestroyed: !1,
    cmp: null,
    useNaturalSorting: !1,
    collapsibleRows: !0,
    collapsibleColumns: !0,
    isPivotMatrix: !0,
    serializeProperties: ['viewLayoutType', 'rowSubTotalsPosition', 'rowGrandTotalsPosition', 'colSubTotalsPosition', 'colGrandTotalsPosition', 'showZeroAsBlank', 'collapsibleRows', 'collapsibleColumns'],
    constructor: function(a) {
        var b = Ext.util.Observable.prototype.constructor.apply(this, arguments);
        this.initialize(!0, a);
        return b
    },
    destroy: function() {
        var a = this;
        a.delayedTask.cancel();
        a.delayedTask = null;
        if (Ext.isFunction(a.onDestroy)) {
            a.onDestroy()
        }
        Ext.destroy(a.results, a.leftAxis, a.topAxis, a.aggregate, a.pivotStore);
        a.results = a.leftAxis = a.topAxis = a.aggregate = a.pivotStore = null;
        if (Ext.isArray(a.columns)) {
            a.columns.length = 0
        }
        if (Ext.isArray(a.model)) {
            a.model.length = 0
        }
        if (Ext.isArray(a.totals)) {
            a.totals.length = 0
        }
        a.columns = a.model = a.totals = a.keysMap = a.cmp = a.modelInfo = null;
        a.isDestroyed = !0;
        Ext.util.Observable.prototype.destroy.apply(this, arguments)
    },
    getKey: function(b) {
        var a = this;
        a.keysMap = a.keysMap || {};
        if (!Ext.isDefined(a.keysMap[b])) {
            a.keysMap[b] = Ext.id()
        }
        return a.keysMap[b]
    },
    naturalSort: (function() {
        var d = /(^([+\-]?(?:\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[\da-fA-F]+$|\d+)/g,
            a = /^\s+|\s+$/g,
            e = /\s+/g,
            f = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
            c = /^0x[0-9a-f]+$/i,
            g = /^0/,
            b = function(b, c) {
                b = b || '';
                return (!b.match(g) || c == 1) && parseFloat(b) || b.replace(e, ' ').replace(a, '') || 0
            };
        return function(m, n) {
            var o = String(m instanceof Date ? m.getTime() : (m || '')).replace(a, ''),
                i = String(n instanceof Date ? n.getTime() : (n || '')).replace(a, ''),
                k = o.replace(d, '\x00$1\x00').replace(/\0$/, '').replace(/^\0/, '').split('\x00'),
                r = i.replace(d, '\x00$1\x00').replace(/\0$/, '').replace(/^\0/, '').split('\x00'),
                j = parseInt(o.match(c), 16) || (k.length !== 1 && Date.parse(o)),
                l = parseInt(i.match(c), 16) || j && i.match(f) && Date.parse(i) || null,
                e, g;
            if (l) {
                if (j < l) {
                    return -1
                } else if (j > l) {
                    return 1
                }
            }
            for (var h = 0, p = k.length, q = r.length, s = Math.max(p, q); h < s; h++) {
                e = b(k[h], p);
                g = b(r[h], q);
                if (isNaN(e) !== isNaN(g)) {
                    return (isNaN(e)) ? 1 : -1
                } else if (typeof e !== typeof g) {
                    e += '';
                    g += ''
                }
                if (e < g) {
                    return -1
                }
                if (e > g) {
                    return 1
                }
            }
            return 0
        }
    }()),
    initialize: function(d, b) {
        var a = this,
            e = a.serializeProperties,
            c;
        b = b || {};
        a.initResults();
        if (d || b.aggregate) {
            a.initAggregates(b.aggregate || [])
        }
        if (d || b.leftAxis) {
            a.initLeftAxis(b.leftAxis || [])
        }
        if (d || b.topAxis) {
            a.initTopAxis(b.topAxis || [])
        }
        for (c = 0; c < e.length; c++) {
            if (Ext.isDefined(b[e[c]])) {
                a[e[c]] = b[e[c]]
            }
        }
        a.totals = [];
        a.modelInfo = {};
        a.keysMap = null;
        if (!d) {
            if (!a.collapsibleRows) {
                a.leftAxis.expandAll()
            }
            if (!a.collapsibleColumns) {
                a.topAxis.expandAll()
            }
        }
        if (d) {
            a.pivotStore = new Ext.data.ArrayStore({
                autoDestroy: !1,
                fields: []
            });
            a.delayedTask = new Ext.util.DelayedTask(a.startProcess, a);
            if (Ext.isFunction(a.onInitialize)) {
                a.onInitialize()
            }
        }
        a.delayedTask.delay(5)
    },
    onInitialize: Ext.emptyFn,
    onDestroy: Ext.emptyFn,
    reconfigure: function(c) {
        var a = this,
            b = Ext.clone(c || {});
        if (a.fireEvent('beforereconfigure', a, b) !== !1) {
            if (Ext.isFunction(a.onReconfigure)) {
                a.onReconfigure(b)
            }
            a.fireEvent('reconfigure', a, b);
            a.initialize(!1, b);
            a.clearData()
        } else {
            a.delayedTask.cancel()
        }
    },
    onReconfigure: Ext.emptyFn,
    serialize: function() {
        var a = this,
            e = a.serializeProperties,
            f = e.length,
            b = {},
            c, d;
        for (c = 0; c < f; c++) {
            d = e[c];
            b[d] = a[d]
        }
        b.leftAxis = a.serializeDimensions(a.leftAxis.dimensions);
        b.topAxis = a.serializeDimensions(a.topAxis.dimensions);
        b.aggregate = a.serializeDimensions(a.aggregate);
        return b
    },
    serializeDimensions: function(b) {
        var d = b.getCount(),
            c = [],
            a;
        for (a = 0; a < d; a++) {
            c.push(b.getAt(a).serialize())
        }
        return c
    },
    initResults: function() {
        Ext.destroy(this.results);
        this.results = new Ext.pivot.result.Collection({
            resultType: this.resultType,
            matrix: this
        })
    },
    initAggregates: function(b) {
        var c = this,
            d, a;
        Ext.destroy(c.aggregate);
        c.aggregate = new Ext.pivot.MixedCollection();
        c.aggregate.getKey = function(a) {
            return a.getId()
        };
        if (Ext.isEmpty(b)) {
            return
        }
        b = Ext.Array.from(b);
        for (d = 0; d < b.length; d++) {
            a = b[d];
            if (!a.isInstance) {
                Ext.applyIf(a, {
                    isAggregate: !0,
                    align: 'right',
                    showZeroAsBlank: c.showZeroAsBlank
                });
                a = new Ext.pivot.dimension.Item(a)
            }
            a.matrix = this;
            c.aggregate.add(a)
        }
    },
    initLeftAxis: function(b) {
        var a = this;
        b = Ext.Array.from(b || []);
        Ext.destroy(a.leftAxis);
        a.leftAxis = Ext.Factory.pivotaxis({
            type: a.leftAxisType,
            matrix: a,
            dimensions: b,
            isLeftAxis: !0
        })
    },
    initTopAxis: function(b) {
        var a = this;
        b = Ext.Array.from(b || []);
        Ext.destroy(a.topAxis);
        a.topAxis = Ext.Factory.pivotaxis({
            type: a.topAxisType,
            matrix: a,
            dimensions: b,
            isLeftAxis: !1
        })
    },
    clearData: function() {
        var a = this;
        a.fireEvent('cleardata', a);
        a.leftAxis.clear();
        a.topAxis.clear();
        a.results.clear();
        if (Ext.isArray(a.columns)) {
            a.columns.length = 0
        }
        if (Ext.isArray(a.model)) {
            a.model.length = 0
        }
        a.totals = [];
        a.modelInfo = {};
        a.keysMap = null;
        if (a.pivotStore) {
            a.pivotStore.removeAll(!0)
        }
    },
    startProcess: Ext.emptyFn,
    endProcess: function() {
        var a = this;
        a.leftAxis.getTree();
        a.topAxis.getTree();
        a.buildModelAndColumns();
        a.buildPivotStore();
        if (Ext.isFunction(a.onBuildStore)) {
            a.onBuildStore(a.pivotStore)
        }
        a.fireEvent('storebuilt', a, a.pivotStore);
        a.fireEvent('done', a)
    },
    onBuildModel: Ext.emptyFn,
    onBuildColumns: Ext.emptyFn,
    onBuildRecord: Ext.emptyFn,
    onBuildTotals: Ext.emptyFn,
    onBuildStore: Ext.emptyFn,
    buildModelAndColumns: function() {
        var a = this;
        a.model = [{
            name: 'id',
            type: 'string'
        }, {
            name: 'isRowGroupHeader',
            type: 'boolean',
            defaultValue: !1
        }, {
            name: 'isRowGroupTotal',
            type: 'boolean',
            defaultValue: !1
        }, {
            name: 'isRowGrandTotal',
            type: 'boolean',
            defaultValue: !1
        }, {
            name: 'leftAxisKey',
            type: 'string',
            defaultValue: null
        }];
        a.internalCounter = 0;
        a.columns = [];
        if (a.viewLayoutType == 'compact') {
            a.generateCompactLeftAxis()
        } else {
            a.leftAxis.dimensions.each(a.parseLeftAxisDimension, a)
        }
        if (a.colGrandTotalsPosition == 'first') {
            a.columns.push(a.parseAggregateForColumn(null, {
                text: a.textGrandTotalTpl,
                grandTotal: !0
            }))
        }
        Ext.Array.each(a.topAxis.getTree(), a.parseTopAxisItem, a);
        if (a.colGrandTotalsPosition == 'last') {
            a.columns.push(a.parseAggregateForColumn(null, {
                text: a.textGrandTotalTpl,
                grandTotal: !0
            }))
        }
        if (Ext.isFunction(a.onBuildModel)) {
            a.onBuildModel(a.model)
        }
        a.fireEvent('modelbuilt', a, a.model);
        if (Ext.isFunction(a.onBuildColumns)) {
            a.onBuildColumns(a.columns)
        }
        a.fireEvent('columnsbuilt', a, a.columns)
    },
    getDefaultFieldInfo: function(a) {
        return Ext.apply({
            isColGroupTotal: !1,
            isColGrandTotal: !1,
            leftAxisColumn: !1,
            topAxisColumn: !1,
            topAxisKey: null
        }, a)
    },
    parseLeftAxisDimension: function(a) {
        var b = this,
            c = a.getId();
        b.model.push({
            name: c,
            type: 'auto'
        });
        b.columns.push(Ext.merge({
            dataIndex: c,
            text: a.header,
            dimension: a,
            leftAxis: !0
        }, a.column));
        b.modelInfo[c] = b.getDefaultFieldInfo({
            leftAxisColumn: !0
        })
    },
    generateCompactLeftAxis: function() {
        var a = this;
        a.model.push({
            name: a.compactViewKey,
            type: 'auto'
        });
        a.columns.push({
            dataIndex: a.compactViewKey,
            text: a.textRowLabels,
            leftAxis: !0,
            width: a.compactViewColumnWidth
        });
        a.modelInfo[a.compactViewKey] = a.getDefaultFieldInfo({
            leftAxisColumn: !0
        })
    },
    parseTopAxisItem: function(a) {
        var b = this,
            d = [],
            e = [],
            h, c, i, g, f;
        if (!a.children) {
            d = b.parseAggregateForColumn(a, null);
            if (a.level === 0) {
                b.columns.push(d)
            } else {
                return d
            }
        } else {
            if (b.colSubTotalsPosition == 'first') {
                c = b.addColSummary(a);
                if (c) {
                    e.push(c)
                }
            }
            i = a.children.length;
            for (g = 0; g < i; g++) {
                f = b.parseTopAxisItem(a.children[g]);
                if (Ext.isArray(f)) {
                    Ext.Array.insert(d, d.length, f)
                } else {
                    d.push(f)
                }
            }
            h = {
                text: a.name,
                group: a,
                columns: d,
                key: a.key,
                xexpandable: !0,
                xgrouped: !0
            };
            if (a.level === 0) {
                b.columns.push(h)
            }
            e.push(h);
            if (b.colSubTotalsPosition == 'last') {
                c = b.addColSummary(a);
                if (c) {
                    e.push(c)
                }
            }
            if (b.colSubTotalsPosition == 'none') {
                c = b.addColSummary(a);
                if (c) {
                    e.push(c)
                }
            }
            return e
        }
    },
    addColSummary: function(a) {
        var c = this,
            b, d = !1;
        b = c.parseAggregateForColumn(a, {
            text: a.expanded ? a.getTextTotal() : a.name,
            group: a,
            subTotal: !0
        });
        if (a.level === 0) {
            c.columns.push(b)
        }
        Ext.apply(b, {
            key: a.key,
            xexpandable: !0,
            xgrouped: !0
        });
        return b
    },
    parseAggregateForColumn: function(c, b) {
        var a = this,
            e = [],
            d = {},
            h = a.aggregate.getRange(),
            i = h.length,
            g, f;
        for (g = 0; g < i; g++) {
            f = h[g];
            a.internalCounter++;
            a.model.push({
                name: 'c' + a.internalCounter,
                type: 'auto',
                defaultValue: undefined,
                useNull: !0,
                col: c ? c.key : a.grandTotalKey,
                agg: f.getId()
            });
            e.push(Ext.merge({
                dataIndex: 'c' + a.internalCounter,
                text: f.header,
                topAxis: !0,
                subTotal: (b ? b.subTotal === !0 : !1),
                grandTotal: (b ? b.grandTotal === !0 : !1),
                dimension: f
            }, f.column));
            a.modelInfo['c' + a.internalCounter] = a.getDefaultFieldInfo({
                isColGroupTotal: (b ? b.subTotal === !0 : !1),
                isColGrandTotal: (b ? b.grandTotal === !0 : !1),
                topAxisColumn: !0,
                topAxisKey: c ? c.key : a.grandTotalKey
            })
        }
        if (e.length == 0 && a.aggregate.getCount() == 0) {
            a.internalCounter++;
            d = Ext.apply({
                text: c ? c.name : '',
                dataIndex: 'c' + a.internalCounter
            }, b || {})
        } else if (e.length == 1) {
            d = Ext.applyIf({
                text: c ? c.name : ''
            }, e[0]);
            Ext.apply(d, b || {});
            if (b && b.grandTotal && a.aggregate.getCount() == 1) {
                d.text = a.aggregate.getAt(0).header || b.text
            }
        } else {
            d = Ext.apply({
                text: c ? c.name : '',
                columns: e
            }, b || {})
        }
        return d
    },
    buildPivotStore: function() {
        var a = this;
        if (Ext.isFunction(a.pivotStore.model.setFields)) {
            a.pivotStore.model.setFields(a.model)
        } else {
            a.pivotStore.model.replaceFields(a.model, !0)
        }
        a.pivotStore.removeAll(!0);
        Ext.Array.each(a.leftAxis.getTree(), a.addRecordToPivotStore, a);
        a.addGrandTotalsToPivotStore()
    },
    addGrandTotalsToPivotStore: function() {
        var a = this,
            c = [],
            e, d, b;
        c.push({
            title: a.textGrandTotalTpl,
            values: a.preparePivotStoreRecordData({
                key: a.grandTotalKey
            }, {
                isRowGrandTotal: !0
            })
        });
        if (Ext.isFunction(a.onBuildTotals)) {
            a.onBuildTotals(c)
        }
        a.fireEvent('buildtotals', a, c);
        e = c.length;
        for (d = 0; d < e; d++) {
            b = c[d];
            if (Ext.isObject(b) && Ext.isObject(b.values)) {
                Ext.applyIf(b.values, {
                    isRowGrandTotal: !0
                });
                a.totals.push({
                    title: b.title || '',
                    record: a.pivotStore.add(b.values)[0]
                })
            }
        }
    },
    addRecordToPivotStore: function(b) {
        var a = this,
            c, d;
        if (!b.children) {
            c = a.pivotStore.add(a.preparePivotStoreRecordData(b))[0];
            b.record = c;
            if (Ext.isFunction(a.onBuildRecord)) {
                a.onBuildRecord(c, b)
            }
            a.fireEvent('recordbuilt', a, c, b)
        } else {
            b.records = {};
            d = (a.viewLayoutType === 'compact' ? a.compactViewKey : b.dimensionId);
            b.records.collapsed = a.pivotStore.add(a.preparePivotStoreRecordData(b, {
                isRowGroupHeader: !0,
                isRowGroupTotal: !0
            }))[0];
            if (a.rowSubTotalsPosition === 'first' && a.viewLayoutType !== 'tabular') {
                b.records.expanded = a.pivotStore.add(a.preparePivotStoreRecordData(b, {
                    isRowGroupHeader: !0
                }))[0]
            } else {
                c = {};
                c = a.preparePivotStoreRecordData(b, {
                    isRowGroupHeader: !0
                });
                c[d] = b.name;
                b.records.expanded = a.pivotStore.add(c)[0];
                if (a.rowSubTotalsPosition === 'last' || a.viewLayoutType === 'tabular') {
                    c = a.preparePivotStoreRecordData(b, {
                        isRowGroupTotal: !0
                    });
                    c[d] = b.getTextTotal();
                    b.records.footer = a.pivotStore.add(c)[0]
                }
            }
            Ext.Array.each(b.children, a.addRecordToPivotStore, a)
        }
    },
    preparePivotStoreRecordData: function(a, g) {
        var d = this,
            c = {},
            h = d.model.length,
            e, b, f;
        if (a) {
            if (a.dimensionId) {
                c[a.dimensionId] = a.name
            }
            c.leftAxisKey = a.key;
            for (e = 0; e < h; e++) {
                b = d.model[e];
                if (b.col && b.agg) {
                    f = d.results.items.map[a.key + '/' + b.col];
                    c[b.name] = f ? f.values[b.agg] : null
                }
            }
            if (d.viewLayoutType === 'compact') {
                c[d.compactViewKey] = a.name
            }
        }
        return Ext.applyIf(c, g)
    },
    getColumns: function() {
        return this.model
    },
    getColumnHeaders: function() {
        var a = this;
        if (!a.model) {
            a.buildModelAndColumns()
        }
        return a.columns
    },
    isGroupRow: function(b) {
        var a = this.leftAxis.findTreeElement('key', b);
        if (!a) {
            return !1
        }
        return (a.node.children && a.node.children.length == 0) ? 0 : a.level
    },
    isGroupCol: function(b) {
        var a = this.topAxis.findTreeElement('key', b);
        if (!a) {
            return !1
        }
        return (a.node.children && a.node.children.length == 0) ? 0 : a.level
    },
    deprecated: {
        '6.0': {
            properties: {
                mztype: 'type',
                mztypeLeftAxis: 'leftAxisType',
                mztypeTopAxis: 'topAxisType'
            }
        }
    }
}, 1, 0, 0, 0, ["pivotmatrix.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.pivot.matrix, 'Base', Mz.aggregate.matrix, 'Abstract'], 0));
(Ext.cmd.derive('Ext.pivot.result.Local', Ext.pivot.result.Base, {
    alternateClassName: ['Mz.aggregate.matrix.Result'],
    records: null,
    constructor: function(a) {
        this.records = [];
        return Ext.pivot.result.Base.prototype.constructor.apply(this, arguments)
    },
    destroy: function() {
        this.records.length = 0;
        this.records = null;
        return Ext.pivot.result.Base.prototype.destroy.apply(this, arguments)
    },
    calculate: function() {
        var a = this,
            c, b, d = a.matrix.aggregate.getCount();
        for (c = 0; c < d; c++) {
            b = a.matrix.aggregate.getAt(c);
            a.addValue(b.getId(), Ext.callback(b.aggregatorFn, b.getScope() || 'self.controller', [a.records, b.dataIndex, a.matrix, a.leftKey, a.topKey], 0, a.matrix.cmp))
        }
    },
    calculateByFn: function(e, d, b) {
        var a = this,
            c;
        if (Ext.isString(b)) {
            b = Ext.pivot.Aggregators[b]
        }
        c = b(a.records, d, a.matrix, a.leftKey, a.topKey);
        a.addValue(e, c);
        return c
    },
    addRecord: function(a) {
        this.records.push(a)
    },
    removeRecord: function(a) {
        Ext.Array.remove(this.records, a)
    }
}, 1, 0, 0, 0, ["pivotresult.local"], 0, [Ext.pivot.result, 'Local', Mz.aggregate.matrix, 'Result'], 0));
(Ext.cmd.derive('Ext.pivot.matrix.Local', Ext.pivot.matrix.Base, {
    alternateClassName: ['Mz.aggregate.matrix.Local'],
    isLocalMatrix: !0,
    resultType: 'local',
    leftAxisType: 'local',
    topAxisType: 'local',
    store: null,
    recordsPerJob: 1000,
    timeBetweenJobs: 2,
    onInitialize: function() {
        var a = this;
        a.localDelayedTask = new Ext.util.DelayedTask(a.delayedProcess, a);
        a.storeCleanDelayedTask = new Ext.util.DelayedTask(a.onOriginalStoreCleanDelayed, a);
        a.storeChangedDelayedTask = new Ext.util.DelayedTask(a.onOriginalStoreChangedDelayed, a);
        a.initializeStore({
            store: a.store
        });
        Ext.pivot.matrix.Base.prototype.onInitialize.apply(this, arguments)
    },
    initializeStore: function(d) {
        var a = this,
            b, c;
        a.processedRecords = {};
        if (d.store) {
            c = d.store
        } else {
            if (a.store) {
                if (a.store.isStore && !a.storeListeners) {
                    b = a.store
                } else {
                    c = a.store
                }
            }
        }
        if (c) {
            b = Ext.getStore(c || '');
            if (Ext.isEmpty(b) && Ext.isString(c)) {
                b = Ext.create(c)
            }
        }
        if (b && b.isStore) {
            Ext.destroy(a.storeListeners);
            if (a.store && a.store.autoDestroy && b != a.store) {
                Ext.destroy(a.store)
            }
            a.store = b;
            a.storeListeners = a.store.on({
                refresh: a.startProcess,
                beforeload: a.onOriginalStoreBeforeLoad,
                add: a.onOriginalStoreAdd,
                update: a.onOriginalStoreUpdate,
                remove: a.onOriginalStoreRemove,
                commit: a.onOriginalStoreClean,
                reject: a.onOriginalStoreClean,
                clear: a.startProcess,
                scope: a,
                destroyable: !0
            });
            if (b.isLoaded()) {
                a.startProcess()
            }
        }
    },
    onReconfigure: function(a) {
        this.initializeStore(a);
        Ext.pivot.matrix.Base.prototype.onReconfigure.apply(this, arguments)
    },
    onDestroy: function() {
        var a = this;
        a.localDelayedTask.cancel();
        a.localDelayedTask = null;
        a.storeCleanDelayedTask.cancel();
        a.storeCleanDelayedTask = null;
        a.storeChangedDelayedTask.cancel();
        a.storeChangedDelayedTask = null;
        if (Ext.isArray(a.records)) {
            a.records.length = 0
        }
        a.records = a.changedRecords = null;
        Ext.destroy(a.storeListeners);
        if (a.store && a.store.isStore && a.store.autoDestroy) {
            Ext.destroy(a.store)
        }
        a.store = a.storeListeners = a.processedRecords = null;
        Ext.pivot.matrix.Base.prototype.onDestroy.apply(this, arguments)
    },
    onOriginalStoreBeforeLoad: function(a) {
        this.fireEvent('start', this)
    },
    createStoreChangesQueue: function() {
        var a = this;
        a.changedRecords = a.changedRecords || {};
        a.changedRecords.add = a.changedRecords.add || [];
        a.changedRecords.update = a.changedRecords.update || [];
        a.changedRecords.remove = a.changedRecords.remove || []
    },
    dropStoreChangesQueue: function() {
        var a = this;
        if (a.changedRecords) {
            a.changedRecords.add.length = 0;
            a.changedRecords.update.length = 0;
            a.changedRecords.remove.length = 0
        }
    },
    onOriginalStoreAdd: function(c, b) {
        var a = this;
        a.createStoreChangesQueue();
        Ext.Array.insert(a.changedRecords.add, a.changedRecords.add.length, b);
        a.storeChangedDelayedTask.delay(100)
    },
    onOriginalStoreUpdate: function(c, b) {
        var a = this;
        a.createStoreChangesQueue();
        if (Ext.Array.indexOf(a.changedRecords.add, b) < 0) {
            Ext.Array.insert(a.changedRecords.update, a.changedRecords.update.length, [b])
        }
        a.storeChangedDelayedTask.delay(100)
    },
    onOriginalStoreRemove: function(g, c, f, d) {
        var a = this,
            e = c.length,
            b;
        if (d) {
            return
        }
        a.createStoreChangesQueue();
        for (b = 0; b < e; b++) {
            Ext.Array.remove(a.changedRecords.update, c[b]);
            Ext.Array.remove(a.changedRecords.add, c[b])
        }
        Ext.Array.insert(a.changedRecords.remove, a.changedRecords.remove.length, c);
        a.storeChangedDelayedTask.delay(100)
    },
    onOriginalStoreChangedDelayed: function() {
        var a = this,
            b = a.changedRecords;
        if (a.isDestroyed) {
            return
        }
        a.storeChanged = !!(b.add.length || b.update.length || b.remove.length);
        if (a.storeChanged) {
            a.onOriginalStoreAddDelayed();
            a.onOriginalStoreUpdateDelayed();
            a.onOriginalStoreRemoveDelayed()
        }
    },
    onOriginalStoreAddDelayed: function() {
        var c = this,
            g = [],
            f = !1,
            d, b, e, h, a;
        e = c.changedRecords.add;
        d = e.length;
        if (!d) {
            return
        }
        for (b = 0; b < d; b++) {
            h = e[b];
            c.processRecord(h, b, d);
            a = c.processedRecords[h.internalId];
            f = f || a.left.length || a.top.length;
            if (a.left.length) {
                Ext.Array.insert(g, g.length, a.left)
            }
        }
        e.length = 0;
        if (f) {
            c.leftAxis.rebuildTree();
            c.topAxis.rebuildTree()
        }
        d = g.length;
        if (d) {
            for (b = 0; b < d; b++) {
                a = g[b];
                if ((a.children && !a.records) || (!a.children && !a.record)) {
                    c.addRecordToPivotStore(a)
                }
            }
        }
        c.recalculateResults(c.store, e, f)
    },
    onOriginalStoreUpdateDelayed: function() {
        var a = this,
            i = [],
            h = !1,
            c, b, m, f, g, d, j, e, k, l;
        f = a.changedRecords.update;
        c = f.length;
        if (!c) {
            return
        }
        for (b = 0; b < c; b++) {
            g = f[b];
            j = a.processedRecords[g.internalId];
            a.removeRecordFromResults(g);
            a.processRecord(g, b, c);
            e = a.processedRecords[g.internalId];
            if (j && e) {
                k = Ext.Array.equals(j.left, e.left);
                l = Ext.Array.equals(j.top, e.top);
                h = h || !k || !l;
                if (!k) {
                    Ext.Array.insert(i, i.length, e.left)
                }
            }
        }
        f.length = 0;
        if (h) {
            a.leftAxis.rebuildTree();
            a.topAxis.rebuildTree()
        }
        c = i.length;
        for (b = 0; b < c; b++) {
            d = i[b];
            if ((d.children && !d.records) || (!d.children && !d.record)) {
                a.addRecordToPivotStore(d)
            }
        }
        a.recalculateResults(a.store, f, h)
    },
    onOriginalStoreRemoveDelayed: function() {
        var a = this,
            e, d, b, c;
        b = a.changedRecords.remove;
        e = b.length;
        if (!e) {
            return
        }
        for (d = 0; d < e; d++) {
            c = a.removeRecordFromResults(b[d]) || c
        }
        b.length = 0;
        if (c) {
            a.leftAxis.rebuildTree();
            a.topAxis.rebuildTree()
        }
        a.recalculateResults(a.store, b, c)
    },
    onOriginalStoreClean: function() {
        var a = this;
        if (a.localDelayedTask.id) {
            a.dropStoreChangesQueue();
            a.storeChanged = !1
        } else {
            a.storeCleanDelayedTask.delay(100)
        }
    },
    onOriginalStoreCleanDelayed: function() {
        var a = this,
            c, d, b;
        if (a.isDestroyed) {
            return
        }
        c = a.pivotStore.getRange();
        d = c.length;
        for (b = 0; b < d; b++) {
            c[b].commit(!0)
        }
        a.storeChanged = !1;
        a.fireEvent('afterupdate', a, !1)
    },
    removeRecordFromResults: function(g) {
        var b = this,
            c = b.processedRecords[g.internalId],
            e = b.grandTotalKey,
            i = !1,
            a, f, d, h, j, k;
        if (!c) {
            return i
        }
        a = b.results.get(e, e);
        if (a) {
            a.removeRecord(g);
            if (a.records.length === 0) {
                b.results.remove(e, e)
            }
        }
        j = c.top.length;
        for (d = 0; d < j; d++) {
            f = c.top[d];
            a = b.results.get(e, f.key);
            if (a) {
                a.removeRecord(g);
                if (a.records.length === 0) {
                    b.results.remove(e, f.key);
                    b.topAxis.items.remove(f);
                    i = !0
                }
            }
        }
        j = c.left.length;
        for (d = 0; d < j; d++) {
            f = c.left[d];
            a = b.results.get(f.key, e);
            if (a) {
                a.removeRecord(g);
                if (a.records.length === 0) {
                    b.results.remove(f.key, e);
                    b.leftAxis.items.remove(f);
                    i = !0
                }
            }
            k = c.top.length;
            for (h = 0; h < k; h++) {
                a = b.results.get(c.left[d].key, c.top[h].key);
                if (a) {
                    a.removeRecord(g);
                    if (a.records.length === 0) {
                        b.results.remove(c.left[d].key, c.top[h].key)
                    }
                }
            }
        }
        return i
    },
    recalculateResults: function(d, c, b) {
        var a = this;
        a.fireEvent('beforeupdate', a, b);
        a.buildModelAndColumns();
        a.results.calculate();
        Ext.Array.each(a.leftAxis.getTree(), a.updateRecordToPivotStore, a);
        a.updateGrandTotalsToPivotStore();
        a.fireEvent('afterupdate', a, b)
    },
    updateGrandTotalsToPivotStore: function() {
        var a = this,
            c = [],
            b;
        if (a.totals.length <= 0) {
            return
        }
        c.push({
            title: a.textGrandTotalTpl,
            values: a.preparePivotStoreRecordData({
                key: a.grandTotalKey
            })
        });
        if (Ext.isFunction(a.onBuildTotals)) {
            a.onBuildTotals(c)
        }
        a.fireEvent('buildtotals', a, c);
        if (a.totals.length === c.length) {
            for (b = 0; b < a.totals.length; b++) {
                if (Ext.isObject(c[b]) && Ext.isObject(c[b].values) && (a.totals[b].record instanceof Ext.data.Model)) {
                    delete(c[b].values.id);
                    a.totals[b].record.set(c[b].values)
                }
            }
        }
    },
    updateRecordToPivotStore: function(a) {
        var c = this,
            d, b;
        if (!a.children) {
            if (a.record) {
                b = c.preparePivotStoreRecordData(a);
                delete(b['id']);
                a.record.set(b)
            }
        } else {
            if (a.records) {
                d = (c.viewLayoutType === 'compact' ? c.compactViewKey : a.dimensionId);
                b = c.preparePivotStoreRecordData(a);
                delete(b['id']);
                delete(b[d]);
                a.records.collapsed.set(b);
                if (a.records.expanded) {
                    a.records.expanded.set(b)
                }
                if (a.records.footer) {
                    a.records.footer.set(b)
                }
            }
            Ext.Array.each(a.children, c.updateRecordToPivotStore, c)
        }
    },
    startProcess: function() {
        var a = this;
        if (!a.store || (a.store && !a.store.isStore) || a.isDestroyed || a.store.isLoading()) {
            return
        }
        a.dropStoreChangesQueue();
        a.clearData();
        a.localDelayedTask.delay(50)
    },
    delayedProcess: function() {
        var a = this;
        if (a.isDestroyed) {
            return
        }
        a.fireEvent('start', a);
        a.records = a.store.getRange();
        if (a.records.length == 0) {
            a.endProcess();
            return
        }
        a.statusInProgress = !1;
        a.processRecords(0)
    },
    processRecords: function(d) {
        var a = this,
            b = d,
            c;
        if (a.isDestroyed) {
            return
        }
        c = a.records.length;
        a.statusInProgress = !0;
        while (b < c && b < d + a.recordsPerJob && a.statusInProgress) {
            a.processRecord(a.records[b], b, c);
            b++
        }
        if (b >= c) {
            a.statusInProgress = !1;
            a.results.calculate();
            a.leftAxis.buildTree();
            a.topAxis.buildTree();
            if (a.filterApplied) {
                a.results.calculate()
            }
            a.records = null;
            a.endProcess();
            return
        }
        if (a.statusInProgress && c > 0) {
            Ext.defer(a.processRecords, a.timeBetweenJobs, a, [b])
        }
    },
    processRecord: function(d, l, m) {
        var a = this,
            f = a.grandTotalKey,
            g, e, c, h, j, k, i, b;
        a.processedRecords[d.internalId] = i = {
            left: [],
            top: []
        };
        g = a.leftAxis.processRecord(d);
        e = a.topAxis.processRecord(d);
        if (g && e) {
            a.results.add(f, f).addRecord(d);
            j = e.length;
            for (c = 0; c < j; c++) {
                b = e[c];
                a.topAxis.addItem(b);
                i.top.push(a.topAxis.items.map[b.key]);
                a.results.add(f, b.key).addRecord(d)
            }
            k = g.length;
            for (c = 0; c < k; c++) {
                b = g[c];
                a.leftAxis.addItem(b);
                i.left.push(a.leftAxis.items.map[b.key]);
                a.results.add(b.key, f).addRecord(d);
                for (h = 0; h < j; h++) {
                    a.results.add(b.key, e[h].key).addRecord(d)
                }
            }
        }
        a.fireEvent('progress', a, l + 1, m)
    },
    getRecordsByRowGroup: function(e) {
        var c = this.results.getByLeftKey(e),
            d = c.length,
            b = [],
            a;
        for (a = 0; a < d; a++) {
            Ext.Array.insert(b, b.length, c[a].records || [])
        }
        return b
    },
    getRecordsByColGroup: function(e) {
        var c = this.results.getByTopKey(e),
            d = c.length,
            b = [],
            a;
        for (a = 0; a < d; a++) {
            Ext.Array.insert(b, b.length, c[a].records || [])
        }
        return b
    },
    getRecordsByGroups: function(c, b) {
        var a = this.results.get(c, b);
        return (a ? a.records || [] : [])
    }
}, 0, 0, 0, 0, ["pivotmatrix.local"], 0, [Ext.pivot.matrix, 'Local', Mz.aggregate.matrix, 'Local'], 0));
(Ext.cmd.derive('Ext.pivot.matrix.Remote', Ext.pivot.matrix.Base, {
    alternateClassName: ['Mz.aggregate.matrix.Remote'],
    isRemoteMatrix: !0,
    url: '',
    timeout: 3000,
    onBeforeRequest: Ext.emptyFn,
    onRequestException: Ext.emptyFn,
    onInitialize: function() {
        var a = this;
        a.remoteDelayedTask = new Ext.util.DelayedTask(a.delayedProcess, a);
        Ext.pivot.matrix.Base.prototype.onInitialize.apply(this, arguments)
    },
    onDestroy: function() {
        this.remoteDelayedTask.cancel();
        this.remoteDelayedTask = null;
        Ext.pivot.matrix.Base.prototype.onDestroy.call(this)
    },
    startProcess: function() {
        var a = this;
        if (Ext.isEmpty(a.url)) {
            return
        }
        a.clearData();
        a.fireEvent('start', a);
        a.statusInProgress = !1;
        a.remoteDelayedTask.delay(5)
    },
    delayedProcess: function() {
        var a = this,
            d = a.serialize(),
            c, b;
        b = {
            keysSeparator: a.keysSeparator,
            grandTotalKey: a.grandTotalKey,
            leftAxis: d.leftAxis,
            topAxis: d.topAxis,
            aggregate: d.aggregate
        };
        c = a.fireEvent('beforerequest', a, b);
        if (c !== !1) {
            if (Ext.isFunction(a.onBeforeRequest)) {
                c = a.onBeforeRequest(b)
            }
        }
        if (c === !1) {
            a.endProcess()
        } else {
            Ext.Ajax.request({
                url: a.url,
                timeout: a.timeout,
                jsonData: b,
                callback: a.processRemoteResults,
                scope: a
            })
        }
    },
    processRemoteResults: function(k, i, g) {
        var d = this,
            h = !i,
            f = Ext.JSON.decode(g.responseText, !0),
            c, b, e, a;
        if (i) {
            h = (!f || !f['success'])
        }
        if (h) {
            d.fireEvent('requestexception', d, g);
            if (Ext.isFunction(d.onRequestException)) {
                d.onRequestException(g)
            }
            d.endProcess();
            return
        }
        c = Ext.Array.from(f.leftAxis || []);
        e = c.length;
        for (a = 0; a < e; a++) {
            b = c[a];
            if (Ext.isObject(b)) {
                d.leftAxis.addItem(b)
            }
        }
        c = Ext.Array.from(f.topAxis || []);
        e = c.length;
        for (a = 0; a < e; a++) {
            b = c[a];
            if (Ext.isObject(b)) {
                d.topAxis.addItem(b)
            }
        }
        c = Ext.Array.from(f.results || []);
        e = c.length;
        for (a = 0; a < e; a++) {
            b = c[a];
            if (Ext.isObject(b)) {
                var j = d.results.add(b.leftKey || '', b.topKey || '');
                Ext.Object.each(b.values || {}, j.addValue, j)
            }
        }
        d.endProcess()
    }
}, 0, 0, 0, 0, ["pivotmatrix.remote"], 0, [Ext.pivot.matrix, 'Remote', Mz.aggregate.matrix, 'Remote'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.Exporter', Ext.exporter.Plugin, {
    alternateClassName: ['Mz.pivot.plugin.ExcelExport'],
    lockableScope: 'top',
    init: function(a) {
        var b = this;
        return Ext.exporter.Plugin.prototype.init.call(this, a)
    },
    prepareData: function(i) {
        var a = this,
            p, b, c, h, m, j, f, g, n, k, d, l, e, o;
        a.matrix = b = a.cmp.getMatrix();
        a.onlyExpandedNodes = (i && i.onlyExpandedNodes);
        if (!a.onlyExpandedNodes) {
            a.setColumnsExpanded(b.topAxis.getTree(), !0)
        }
        h = Ext.clone(b.getColumnHeaders());
        m = a.getColumnHeaders(h, i);
        d = a.getDataIndexColumns(h);
        if (!a.onlyExpandedNodes) {
            a.setColumnsExpanded(b.topAxis.getTree())
        }
        c = {
            columns: m,
            groups: []
        };
        a.extractGroups(c, b.leftAxis.getTree(), d);
        k = b.totals.length;
        n = d.length;
        if (k) {
            c.summaries = [];
            for (f = 0; f < k; f++) {
                j = b.totals[f];
                l = {
                    cells: [{
                        value: j.title
                    }]
                };
                for (g = 1; g < n; g++) {
                    e = j.record.data[d[g].dataIndex];
                    l.cells.push({
                        value: (e == null || e === 0) && b.showZeroAsBlank ? '' : e
                    })
                }
                c.summaries.push(l)
            }
        }
        a.matrix = a.onlyExpandedNodes = null;
        return new Ext.exporter.data.Table(c)
    },
    setColumnsExpanded: function(b, c) {
        for (var a = 0; a < b.length; a++) {
            if (Ext.isDefined(c)) {
                b[a].backupExpanded = b[a].expanded;
                b[a].expanded = c
            } else {
                b[a].expanded = b[a].backupExpanded;
                b[a].backupExpanded = null
            }
            if (b[a].children) {
                this.setColumnsExpanded(b[a].children, c)
            }
        }
    },
    getColumnHeaders: function(e, f) {
        var g = [],
            h = e.length,
            c, b, a, d;
        for (c = 0; c < h; c++) {
            a = e[c];
            d = this.onlyExpandedNodes ? (!a.group || a.group.expanded || (!a.group.expanded && a.subTotal)) : !0;
            if (d) {
                b = {
                    text: (a.subTotal && a.group && a.group.expanded ? a.group.getTextTotal() : a.text)
                };
                if (a.columns) {
                    b.columns = this.getColumnHeaders(a.columns, f)
                } else {
                    b.width = a.dimension ? a.dimension.getWidth() : a.width || 100;
                    b.style = a.dimension ? this.getExportStyle(a.dimension.getExportStyle(), f) : null
                }
                g.push(b)
            }
        }
        return g
    },
    getDataIndexColumns: function(e) {
        var b = [],
            c, a, d;
        for (c = 0; c < e.length; c++) {
            a = e[c];
            d = this.onlyExpandedNodes ? (!a.group || a.group.expanded || (!a.group.expanded && a.subTotal)) : !0;
            if (d) {
                if (a.dataIndex) {
                    b.push({
                        dataIndex: a.dataIndex,
                        agg: a.dimension ? a.dimension.getId() : null
                    })
                } else if (a.columns) {
                    Ext.Array.insert(b, b.length, this.getDataIndexColumns(a.columns))
                }
            }
        }
        return b
    },
    extractGroups: function(e, l, g) {
        var i, c, n, m, j, a, h, f, k, b, d;
        n = l.length;
        for (i = 0; i < n; i++) {
            a = l[i];
            if (a.record) {
                e.rows = e.rows || [];
                d = [];
                h = {
                    cells: d
                };
                for (c = 0; c < g.length; c++) {
                    b = a.record.data[g[c].dataIndex];
                    d.push({
                        value: (b == null || b === 0) && this.matrix.showZeroAsBlank ? '' : b
                    })
                }
                e.rows.push(h)
            } else if (a.children) {
                e.groups = e.groups || [];
                f = {
                    text: a.name
                };
                j = this.onlyExpandedNodes ? a.expanded : !0;
                if (j) {
                    this.extractGroups(f, a.children, g)
                }
                f.summaries = [];
                d = [{
                    value: (j ? a.getTextTotal() : a.value)
                }];
                h = {
                    cells: d
                };
                k = (a.expanded ? a.records.expanded : a.records.collapsed);
                m = g.length;
                for (c = 1; c < m; c++) {
                    b = k.data[g[c].dataIndex];
                    d.push({
                        value: (b == null || b === 0) && this.matrix.showZeroAsBlank ? '' : b
                    })
                }
                f.summaries.push(h);
                e.groups.push(f)
            }
        }
    }
}, 0, 0, 0, 0, ["plugin.mzexcelexport", "plugin.pivotexporter"], 0, [Ext.pivot.plugin, 'Exporter', Mz.pivot.plugin, 'ExcelExport'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.FieldSettings', Ext.Base, {
    $configStrict: !1,
    config: {
        cls: '',
        style: null,
        fixed: [],
        allowed: ['leftAxis', 'topAxis', 'aggregate'],
        aggregators: ['sum', 'avg', 'min', 'max', 'count', 'countNumbers', 'groupSumPercentage', 'groupCountPercentage', 'variance', 'varianceP', 'stdDev', 'stdDevP'],
        renderers: {},
        formatters: {}
    },
    isFieldSettings: !0,
    constructor: function(a) {
        this.initConfig(a || {});
        return this.callParent(arguments)
    },
    getDefaultEmptyArray: function(b) {
        var a = this['_' + b];
        if (!a) {
            a = [];
            this['set' + Ext.String.capitalize(b)](a)
        }
        return a
    },
    applyArrayValues: function(c, a, b) {
        if (a == null || (a && Ext.isArray(a))) {
            return a
        }
        if (a) {
            if (!b) {
                b = this['get' + Ext.String.capitalize(c)]()
            }
            Ext.Array.splice(b, 0, b.length, a)
        }
        return b
    },
    getFixed: function() {
        return this.getDefaultEmptyArray('fixed')
    },
    applyFixed: function(a, b) {
        return this.applyArrayValues('fixed', a, b)
    },
    getAllowed: function() {
        return this.getDefaultEmptyArray('allowed')
    },
    applyAllowed: function(a, b) {
        return this.applyArrayValues('allowed', a, b)
    },
    getAggregators: function() {
        return this.getDefaultEmptyArray('aggregators')
    },
    applyAggregators: function(a, b) {
        return this.applyArrayValues('aggregators', a, b)
    },
    isFixed: function(a) {
        var b;
        if (!a) {
            return !1
        }
        b = a.getFieldType();
        return Ext.Array.indexOf(this.getFixed(), b) >= 0
    },
    isAllowed: function(b) {
        var c = this.getFixed(),
            a;
        if (!b) {
            return !1
        }
        a = b.getFieldType();
        if (c.length) {
            return Ext.Array.indexOf(c, a) >= 0
        }
        return (a === 'all') || (Ext.Array.indexOf(this.getAllowed(), a) >= 0)
    }
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.plugin.configurator, 'FieldSettings'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.Field', Ext.pivot.dimension.Item, {
    config: {
        editor: null,
        settings: {}
    },
    isField: !0,
    clone: function() {
        return new Ext.pivot.plugin.configurator.Field(Ext.applyIf({
            id: Ext.id()
        }, this.getInitialConfig()))
    },
    getConfiguration: function(b) {
        var a = Ext.pivot.dimension.Item.prototype.getConfiguration.call(this, b);
        if (a.settings) {
            a.settings = a.settings.getConfig()
        }
        return a
    },
    updateAggregator: function(b, d) {
        var a, c;
        Ext.pivot.dimension.Item.prototype.updateAggregator.call(this, b, d);
        if (b) {
            a = this.getSettings();
            c = a.getAggregators();
            if (c.length === 0) {
                Ext.Array.remove(a.getAllowed(), 'aggregate')
            } else {
                Ext.Array.include(c, b)
            }
        }
    },
    getSettings: function() {
        var a = this.settings;
        if (!a) {
            a = new Ext.pivot.plugin.configurator.FieldSettings({});
            this.setSettings(a)
        }
        return a
    },
    applySettings: function(a, b) {
        if (a == null || (a && a.isFieldSettings)) {
            return a
        }
        if (a) {
            if (!b) {
                b = this.getSettings()
            }
            b.setConfig(a)
        }
        if (b) {
            this.setAggregator(this.getAggregator())
        }
        return b
    },
    getFieldText: function() {
        var a = this.getHeader();
        if (this.isAggregate) {
            a += ' (' + this.getAggText() + ')'
        }
        return a
    },
    getAggText: function(c) {
        var a = Ext.pivot.Aggregators,
            b = c || this.getAggregator();
        if (Ext.isFunction(b)) {
            return a.customText
        }
        return a[b + 'Text'] || a.customText
    }
}, 0, 0, 0, 0, 0, 0, [Ext.pivot.plugin.configurator, 'Field'], 0));
(Ext.cmd.derive('Ext.pivot.update.Base', Ext.mixin.Observable, {
    config: {
        leftKey: null,
        topKey: null,
        matrix: null,
        dataIndex: null,
        value: null
    },
    destroy: function() {
        Ext.unasap(this.updateTimer);
        this.setMatrix(null);
        Ext.mixin.Observable.prototype.destroy.call(this)
    },
    getResult: function() {
        var a = this.getMatrix();
        return a ? a.results.get(this.getLeftKey(), this.getTopKey()) : null
    },
    update: function() {
        var a = this;
        Ext.unasap(a.updateTimer);
        return new Ext.Promise(function(d, b) {
            var c = a.getResult();
            if (c) {
                if (a.fireEvent('beforeupdate', a) !== !1) {
                    a.updateTimer = Ext.asap(a.onUpdate, a, [c, d, b])
                } else {
                    b('Operation canceled!')
                }
            } else {
                b('No Result found!')
            }
        })
    },
    onUpdate: function(c, a, b) {
        this.fireEvent('update', this);
        a(this)
    }
}, 0, 0, 0, 0, ["pivotupdate.base"], [
    [Ext.mixin.Factoryable.prototype.mixinId || Ext.mixin.Factoryable.$className, Ext.mixin.Factoryable]
], [Ext.pivot.update, 'Base'], 0));
(Ext.cmd.derive('Ext.pivot.update.Increment', Ext.pivot.update.Base, {
    onUpdate: function(f, h, i) {
        if (this.destroyed) {
            return
        }
        var e = this.getDataIndex(),
            a = parseFloat(this.getValue()),
            c = f.records,
            g, b, d;
        if (isNaN(a)) {
            a = null
        }
        if (c && a) {
            g = c.length;
            for (b = 0; b < g; b++) {
                d = c[b];
                d.set(e, d.get(e) + a)
            }
        }
        Ext.pivot.update.Base.prototype.onUpdate.call(this, f, h, i)
    }
}, 0, 0, 0, 0, ["pivotupdate.increment"], 0, [Ext.pivot.update, 'Increment'], 0));
(Ext.cmd.derive('Ext.pivot.update.Overwrite', Ext.pivot.update.Base, {
    onUpdate: function(d, g, h) {
        if (this.destroyed) {
            return
        }
        var f = this.getDataIndex(),
            c = parseFloat(this.getValue()),
            b = d.records,
            e, a;
        if (isNaN(c)) {
            c = null
        }
        if (b) {
            e = b.length;
            for (a = 0; a < e; a++) {
                b[a].set(f, c)
            }
        }
        Ext.pivot.update.Base.prototype.onUpdate.call(this, d, g, h)
    }
}, 0, 0, 0, 0, ["pivotupdate.overwrite"], 0, [Ext.pivot.update, 'Overwrite'], 0));
(Ext.cmd.derive('Ext.pivot.update.Percentage', Ext.pivot.update.Base, {
    onUpdate: function(f, h, i) {
        if (this.destroyed) {
            return
        }
        var e = this.getDataIndex(),
            a = parseFloat(this.getValue()),
            c = f.records,
            g, b, d;
        if (isNaN(a)) {
            a = null
        }
        if (c) {
            g = c.length;
            for (b = 0; b < g; b++) {
                d = c[b];
                d.set(e, a === null ? null : Math.floor(d.get(e) * a / 100))
            }
        }
        Ext.pivot.update.Base.prototype.onUpdate.call(this, f, h, i)
    }
}, 0, 0, 0, 0, ["pivotupdate.percentage"], 0, [Ext.pivot.update, 'Percentage'], 0));
(Ext.cmd.derive('Ext.pivot.update.Uniform', Ext.pivot.update.Base, {
    onUpdate: function(e, h, i) {
        if (this.destroyed) {
            return
        }
        var g = this.getDataIndex(),
            d = e.records,
            a = parseFloat(this.getValue()),
            b, c, f;
        if (isNaN(a)) {
            a = null
        }
        if (d) {
            b = d.length;
            if (b > 0) {
                f = (a === null ? null : (a / b));
                for (c = 0; c < b; c++) {
                    d[c].set(g, f)
                }
            }
        }
        Ext.pivot.update.Base.prototype.onUpdate.call(this, e, h, i)
    }
}, 0, 0, 0, 0, ["pivotupdate.uniform"], 0, [Ext.pivot.update, 'Uniform'], 0));
(Ext.cmd.derive('Ext.pivot.cell.Cell', Ext.grid.cell.Cell, {
    config: {
        eventCell: !0,
        collapsible: null
    },
    outlineGroupHeaderCls: 'x-pivot-grid-group-header-outline',
    outlineCellHiddenCls: 'x-pivot-grid-outline-cell-hidden',
    outlineCellGroupExpandedCls: 'x-pivot-grid-outline-cell-previous-expanded',
    compactGroupHeaderCls: 'x-pivot-grid-group-header-compact',
    compactLayoutPadding: 25,
    tabularGroupHeaderCls: 'x-pivot-grid-group-header-tabular',
    encodeHtml: !1,
    onRender: function() {
        var a = this,
            b = a.dataIndex,
            d = a.getViewModel(),
            e = a.getRecord(),
            c;
        (arguments.callee.$previous || Ext.grid.cell.Cell.prototype.onRender).call(this);
        if (d && b) {
            c = a.row.getGrid().getMatrix();
            d.setData({
                column: c.modelInfo[b] || {},
                value: a.getValue(),
                record: e
            })
        }
    },
    handleEvent: function(n, p) {
        var c = this,
            j = c.row,
            f = j.getGrid(),
            m = c.getRecord(),
            d = {
                grid: f
            },
            g, h, o, i, k, e, l, a, b;
        if (!m) {
            return
        }
        h = j.getEventName() || '';
        g = j.getRecordInfo(m);
        l = f.getMatrix();
        i = c.getColumn();
        a = g.rendererParams[i._dataIndex];
        if (!a) {
            a = g.rendererParams['topaxis']
        }
        a = a ? a.group : null;
        k = a ? a.key : g.leftKey;
        Ext.apply(d, {
            cell: c,
            leftKey: k,
            leftItem: a,
            column: i
        });
        if (c.getEventCell()) {
            h += 'cell';
            e = f.getTopAxisKey(d.column);
            d.topKey = e;
            if (e) {
                b = l.topAxis.findTreeElement('key', e);
                b = b ? b.node : null;
                if (b) {
                    Ext.apply(d, {
                        topItem: b,
                        dimensionId: b.dimensionId
                    })
                }
            }
        }
        o = f.fireEvent(h + n, d, p);
        if (o !== !1 && n == 'tap' && c.getCollapsible()) {
            if (a.expanded) {
                a.collapse()
            } else {
                a.expand()
            }
        }
        return !1
    },
    updateRecord: function(a, c) {
        var b = this.getViewModel();
        Ext.grid.cell.Cell.prototype.updateRecord.call(this, a, c);
        if (b) {
            b.setData({
                value: this.getValue(),
                record: a
            })
        }
    }
}, 0, ["pivotgridcell"], ["widget", "gridcellbase", "textcell", "gridcell", "pivotgridcell"], {
    "widget": !0,
    "gridcellbase": !0,
    "textcell": !0,
    "gridcell": !0,
    "pivotgridcell": !0
}, ["widget.pivotgridcell"], 0, [Ext.pivot.cell, 'Cell'], 0));
(Ext.cmd.derive('Ext.pivot.cell.Group', Ext.pivot.cell.Cell, {
    config: {
        innerGroupStyle: null,
        innerGroupCls: null,
        userGroupStyle: null,
        userGroupCls: null
    },
    innerTemplate: [{
        reference: 'iconElement',
        classList: ['x-pivot-grid-group-icon', 'x-font-icon']
    }, {
        reference: 'groupElement',
        classList: ['x-pivot-grid-group-title']
    }],
    groupCls: 'x-pivot-grid-group',
    groupHeaderCls: 'x-pivot-grid-group-header',
    groupHeaderCollapsibleCls: 'x-pivot-grid-group-header-collapsible',
    groupHeaderCollapsedCls: 'x-pivot-grid-group-header-collapsed',
    updateCellCls: function(a, d) {
        var b = this,
            c = typeof a == 'string' ? a.split(' ') : Ext.Array.from(a);
        Ext.pivot.cell.Cell.prototype.updateCellCls.call(this, a, d);
        b.setEventCell(c.indexOf(b.groupHeaderCls) < 0);
        b.setCollapsible(c.indexOf(b.groupHeaderCollapsibleCls) >= 0)
    },
    updateInnerGroupStyle: function(a) {
        this.groupElement.applyStyles(a)
    },
    updateInnerGroupCls: function(b, a) {
        this.groupElement.replaceCls(a, b)
    },
    updateUserGroupStyle: function(a) {
        this.groupElement.applyStyles(a)
    },
    updateUserGroupCls: function(b, a) {
        this.groupElement.replaceCls(a, b)
    },
    updateRawValue: function(a) {
        var c = this.groupElement.dom,
            b = a == null ? '' : a;
        if (this.getEncodeHtml()) {
            c.textContent = b
        } else {
            c.innerHTML = b
        }
    },
    updateRecord: function(e, d) {
        var b = this,
            a = b.row.getRecordInfo(),
            c = b.dataIndex;
        if (a && c) {
            a = a.rendererParams[c];
            if (a && b[a.fn]) {
                b[a.fn](a, b.row.getGrid())
            } else {
                b.setCellCls('')
            }
        }
        Ext.pivot.cell.Cell.prototype.updateRecord.call(this, e, d)
    },
    groupOutlineRenderer: function(c, e) {
        var a = this,
            b = '',
            d = c.group;
        if (e.getMatrix().viewLayoutType == 'compact') {
            a.bodyElement.setStyle(e.isRTL() ? 'padding-right' : 'padding-left', '0')
        }
        if (c.colspan > 0) {
            b += a.groupHeaderCls + ' ' + a.outlineGroupHeaderCls;
            if (!c.subtotalRow) {
                if (d && d.children && d.axis.matrix.collapsibleRows) {
                    b += ' ' + a.groupHeaderCollapsibleCls;
                    if (!c.group.expanded) {
                        b += ' ' + a.groupHeaderCollapsedCls
                    }
                }
                if (c.previousExpanded) {
                    b += ' ' + a.outlineCellGroupExpandedCls
                }
            }
            a.setCellCls(b);
            a.setInnerGroupCls(a.groupCls);
            return
        }
        a.setCellCls(a.outlineCellHiddenCls)
    },
    recordOutlineRenderer: function(b, c) {
        var a = this;
        if (b.hidden) {
            a.setCellCls(a.outlineCellHiddenCls);
            return
        }
        a.setCellCls(a.groupHeaderCls)
    },
    groupCompactRenderer: function(c, e) {
        var a = this,
            b = '',
            d = c.group;
        a.bodyElement.setStyle(e.isRTL() ? 'padding-right' : 'padding-left', (a.compactLayoutPadding * d.level) + 'px');
        b += a.groupHeaderCls + ' ' + a.compactGroupHeaderCls;
        if (!c.subtotalRow) {
            if (d && d.children && d.axis.matrix.collapsibleRows) {
                b += ' ' + a.groupHeaderCollapsibleCls;
                if (!c.group.expanded) {
                    b += ' ' + a.groupHeaderCollapsedCls
                }
            }
            if (c.previousExpanded) {
                b += ' ' + a.outlineCellGroupExpandedCls
            }
        }
        a.setCellCls(b);
        a.setInnerGroupCls(a.groupCls)
    },
    recordCompactRenderer: function(b, c) {
        var a = this;
        a.bodyElement.setStyle(c.isRTL() ? 'padding-right' : 'padding-left', (a.compactLayoutPadding * b.group.level) + 'px');
        a.setCellCls(a.groupHeaderCls + ' ' + a.compactGroupHeaderCls)
    },
    groupTabularRenderer: function(d, e) {
        var a = this,
            b = '',
            c = d.group;
        b += a.groupHeaderCls + ' ' + a.tabularGroupHeaderCls;
        if (!d.subtotalRow && !d.hidden) {
            if (c && c.children && c.axis.matrix.collapsibleRows) {
                b += ' ' + a.groupHeaderCollapsibleCls;
                if (!c.expanded) {
                    b += ' ' + a.groupHeaderCollapsedCls
                }
            }
        }
        a.setCellCls(b);
        a.setInnerGroupCls(a.groupCls)
    },
    recordTabularRenderer: function(b) {
        var a = this;
        if (b.hidden) {
            a.setCellCls(a.outlineCellHiddenCls);
            return
        }
        a.setCellCls(a.groupHeaderCls)
    }
}, 0, ["pivotgridgroupcell"], ["widget", "gridcellbase", "textcell", "gridcell", "pivotgridcell", "pivotgridgroupcell"], {
    "widget": !0,
    "gridcellbase": !0,
    "textcell": !0,
    "gridcell": !0,
    "pivotgridcell": !0,
    "pivotgridgroupcell": !0
}, ["widget.pivotgridgroupcell"], 0, [Ext.pivot.cell, 'Group'], 0));
(Ext.cmd.derive('Ext.pivot.Row', Ext.grid.Row, {
    config: {
        recordInfo: null,
        rowCls: null,
        eventName: null
    },
    summaryDataCls: 'x-pivot-summary-data',
    summaryRowCls: 'x-pivot-grid-group-total',
    grandSummaryRowCls: 'x-pivot-grid-grand-total',
    onRender: function() {
        (arguments.callee.$previous || Ext.grid.Row.prototype.onRender).call(this);
        var a = this.getViewModel();
        if (a) {
            a.set('columns', this.getRefOwner().getMatrix().modelInfo)
        }
    },
    destroy: function() {
        this.setRecordInfo(null);
        Ext.grid.Row.prototype.destroy.call(this)
    },
    updateRecord: function(c, d) {
        var a = this,
            b;
        if (a.destroying || a.destroyed) {
            return
        }
        b = a.getRefOwner().getRecordInfo(c);
        a.setRecordInfo(b);
        if (b) {
            a.setRowCls(b.rowClasses);
            a.setEventName(b.rowEvent)
        }
        Ext.grid.Row.prototype.updateRecord.call(this, c, d)
    }
}, 0, ["pivotgridrow"], ["widget", "component", "gridrow", "pivotgridrow"], {
    "widget": !0,
    "component": !0,
    "gridrow": !0,
    "pivotgridrow": !0
}, ["widget.pivotgridrow"], 0, [Ext.pivot, 'Row'], 0));
(Ext.cmd.derive('Ext.pivot.Grid', Ext.grid.Grid, {
    isPivotGrid: !0,
    isPivotComponent: !0,
    enableLoadMask: !0,
    enableColumnSort: !0,
    startRowGroupsCollapsed: !0,
    startColGroupsCollapsed: !0,
    cellSelector: '.x-gridcell',
    clsGroupTotal: 'x-pivot-grid-group-total',
    clsGrandTotal: 'x-pivot-grid-grand-total',
    groupHeaderCollapsedCls: 'x-pivot-grid-group-header-collapsed',
    groupHeaderCollapsibleCls: 'x-pivot-grid-group-header-collapsible',
    summaryDataCls: 'x-pivot-summary-data',
    groupCls: 'x-pivot-grid-group',
    relayedMatrixEvents: ['beforereconfigure', 'reconfigure', 'start', 'progress', 'done', 'modelbuilt', 'columnsbuilt', 'recordbuilt', 'buildtotals', 'storebuilt', 'groupexpand', 'groupcollapse', 'beforerequest', 'requestexception'],
    config: {
        matrix: {
            type: 'local'
        },
        leftAxisCellConfig: {
            xtype: 'pivotgridgroupcell'
        },
        topAxisCellConfig: {
            xtype: 'pivotgridcell'
        }
    },
    variableHeights: !0,
    itemConfig: {
        xtype: 'pivotgridrow'
    },
    classCLs: 'x-pivot-grid',
    initialize: function() {
        var a = this;
        a.setColumns([]);
        a.setStore(new Ext.data.ArrayStore({
            $internal: !0,
            fields: []
        }));
        a.pivotDataSource = new Ext.pivot.feature.PivotStore({
            store: a.getStore(),
            grid: a,
            matrix: a.getMatrix(),
            clsGrandTotal: a.clsGrandTotal,
            clsGroupTotal: a.clsGroupTotal,
            summaryDataCls: a.summaryDataCls
        });
        a.getHeaderContainer().on({
            columntap: 'handleColumnTap',
            headergrouptap: 'handleHeaderGroupTap',
            scope: a
        });
        return Ext.grid.Grid.prototype.initialize.call(this)
    },
    destroy: function() {
        var a = this;
        Ext.destroy(a.matrixRelayedListeners, a.matrixListeners, a.headerCtListeners, a.lockedHeaderCtListeners);
        Ext.destroy(a.pivotDataSource);
        a.matrixRelayedListeners = a.matrixListeners = a.headerCtListeners = a.lockedHeaderCtListeners = null;
        a.setMatrix(null);
        Ext.grid.Grid.prototype.destroy.call(this);
        a.lastColumnSorted = a.store = Ext.destroy(a.store)
    },
    applyMatrix: function(a, b) {
        Ext.destroy(b);
        if (a == null) {
            return a
        }
        if (a && a.isPivotMatrix) {
            a.cmp = this;
            return a
        }
        Ext.applyIf(a, {
            type: 'local'
        });
        a.cmp = this;
        return Ext.Factory.pivotmatrix(a)
    },
    updateMatrix: function(b, c) {
        var a = this;
        Ext.destroy(c, a.matrixListeners, a.matrixRelayedListeners);
        if (b) {
            a.matrixListeners = b.on({
                cleardata: a.onMatrixClearData,
                start: a.onMatrixProcessStart,
                progress: a.onMatrixProcessProgress,
                done: a.onMatrixDataReady,
                afterupdate: a.onMatrixAfterUpdate,
                groupexpand: a.onMatrixGroupExpandCollapse,
                groupcollapse: a.onMatrixGroupExpandCollapse,
                scope: a,
                destroyable: !0
            });
            a.matrixRelayedListeners = a.relayEvents(b, a.relayedMatrixEvents, 'pivot');
            if (a.pivotDataSource) {
                a.pivotDataSource.setMatrix(b)
            }
        }
    },
    refreshView: function() {
        var a = this;
        if (a.destroyed || a.destroying) {
            return
        }
        a.getStore().fireEvent('pivotstoreremodel', a)
    },
    updateHeaderContainerColumns: function(e) {
        var b = this,
            h = [],
            a, j, c, g, d, f, i;
        if (e) {
            d = b.getColumnForGroup(b.getHeaderContainer().innerItems, e);
            if (d.found) {
                c = d.item.getParent();
                j = d.index;
                i = c.items.length;
                for (f = 0; f < i; f++) {
                    g = c.items.getAt(f);
                    if (g.group == e) {
                        h.push(g)
                    }
                }
                c.remove(h);
                a = Ext.clone(b.pivotColumns);
                b.preparePivotColumns(a);
                a = b.getVisiblePivotColumns(b.prepareVisiblePivotColumns(a), e);
                c.insert(j, a)
            }
        } else {
            a = Ext.clone(b.pivotColumns);
            b.preparePivotColumns(a);
            a = b.prepareVisiblePivotColumns(a);
            b.setColumns(a)
        }
    },
    getColumnForGroup: function(e, d) {
        var f = e.length,
            a = {
                found: !1,
                index: -1,
                item: null
            },
            c, b;
        for (c = 0; c < f; c++) {
            b = e[c];
            if (b.group == d) {
                a.found = !0;
                a.index = c;
                a.item = b
            } else if (b.innerItems) {
                a = this.getColumnForGroup(b.innerItems, d)
            }
            if (a.found) {
                break
            }
        }
        return a
    },
    onMatrixClearData: function() {
        var a = this;
        a.getStore().removeAll(!0);
        a.setColumns([]);
        if (!a.expandedItemsState) {
            a.lastColumnsState = null
        }
        a.sortedColumn = null
    },
    onMatrixProcessStart: function() {
        if (this.enableLoadMask) {
            this.setMasked({
                xtype: 'loadmask',
                indicator: !0
            })
        }
    },
    onMatrixProcessProgress: function(d, c, b) {
        var a = ((c || 0.1) * 100) / (b || 0.1);
        if (this.enableLoadMask) {
            this.getMasked().setMessage(Ext.util.Format.number(a, '0') + '%')
        }
    },
    onMatrixDataReady: function(a) {
        this.refreshMatrixData(a, !1)
    },
    onMatrixAfterUpdate: function(b, a) {
        if (a) {
            this.refreshMatrixData(b, !0)
        } else {
            this.refreshView()
        }
    },
    onMatrixGroupExpandCollapse: function(c, b, a) {
        if (b == 'col') {
            this.updateHeaderContainerColumns(a)
        }
    },
    refreshMatrixData: function(b, k) {
        var a = this,
            c = b.getColumnHeaders(),
            g = !1,
            h = a.getTopAxisCellConfig(),
            i = b.leftAxis.items.items,
            j = b.topAxis.items.items,
            d, f, e;
        if (a.enableLoadMask) {
            a.setMasked(!1)
        }
        if (h) {
            h.zeroValue = b.showZeroAsBlank ? '' : '0'
        }
        if (!k) {
            if (a.expandedItemsState) {
                f = i.length;
                for (d = 0; d < f; d++) {
                    e = i[d];
                    if (Ext.Array.indexOf(a.expandedItemsState['rows'], e.key) >= 0) {
                        e.expanded = !0;
                        g = !0
                    }
                }
                f = j.length;
                for (d = 0; d < f; d++) {
                    e = j[d];
                    if (Ext.Array.indexOf(a.expandedItemsState['cols'], e.key) >= 0) {
                        e.expanded = !0;
                        g = !0
                    }
                }
                if (g) {
                    c = b.getColumnHeaders();
                    delete a.expandedItemsState
                }
            } else {
                if (b.collapsibleRows) {
                    a.doExpandCollapseTree(b.leftAxis.getTree(), !a.startRowGroupsCollapsed)
                }
                if (b.collapsibleColumns) {
                    a.doExpandCollapseTree(b.topAxis.getTree(), !a.startColGroupsCollapsed);
                    c = b.getColumnHeaders()
                }
            }
        }
        a.pivotColumns = Ext.clone(c);
        c = Ext.clone(a.pivotColumns);
        a.preparePivotColumns(c);
        a.restorePivotColumnsState(c);
        c = a.prepareVisiblePivotColumns(c);
        a.setColumns(c);
        if (!Ext.isEmpty(a.sortedColumn)) {
            b.leftAxis.sortTreeByField(a.sortedColumn.dataIndex, a.sortedColumn.direction)
        }
        a.getStore().fireEvent('pivotstoreremodel', a);
        if (!Ext.isEmpty(a.sortedColumn)) {
            a.updateColumnSortState(a.sortedColumn.dataIndex, a.sortedColumn.direction)
        }
        a.lastColumnSorted = null
    },
    getVisiblePivotColumns: function(d, e) {
        var b = [],
            f = d.length,
            c, a;
        for (c = 0; c < f; c++) {
            a = d[c];
            if (a.group == e) {
                b.push(a)
            }
            if (a.columns) {
                b = Ext.Array.merge(b, this.getVisiblePivotColumns(a.columns, e))
            }
        }
        return b
    },
    prepareVisiblePivotColumns: function(c) {
        var e = c.length,
            d = [],
            b, a, f;
        for (b = 0; b < e; b++) {
            a = c[b];
            if (!a.hidden) {
                d.push(a)
            }
            if (a.columns) {
                a.columns = this.prepareVisiblePivotColumns(a.columns)
            }
        }
        return d
    },
    preparePivotColumns: function(d) {
        var b = this,
            e = {
                menuDisabled: !0,
                sortable: !1,
                lockable: !1
            },
            f = d ? d.length : 0,
            c, a;
        for (c = 0; c < f; c++) {
            a = d[c];
            a.cls = a.cls || '';
            Ext.apply(a, e);
            if (a.leftAxis) {
                if (a.cell) {
                    Ext.applyIf(a.cell, Ext.clone(b.getLeftAxisCellConfig()))
                } else {
                    Ext.apply(a, {
                        cell: Ext.clone(b.getLeftAxisCellConfig())
                    })
                }
            } else if (a.topAxis) {
                if (a.cell) {
                    Ext.applyIf(a.cell, Ext.clone(b.getTopAxisCellConfig()))
                } else {
                    Ext.apply(a, {
                        cell: Ext.clone(b.getTopAxisCellConfig())
                    })
                }
            }
            if (a.subTotal) {
                a.cls = b.clsGroupTotal
            }
            if (a.group && a.xgrouped) {
                if (a.group.expanded) {
                    if (!a.subTotal && this._matrix.collapsibleColumns) {
                        a.cls += (Ext.isEmpty(a.cls) ? '' : ' ') + b.groupHeaderCollapsibleCls
                    }
                } else {
                    if (a.subTotal && this._matrix.collapsibleColumns) {
                        a.cls += (Ext.isEmpty(a.cls) ? '' : ' ') + b.groupHeaderCollapsibleCls + ' ' + b.groupHeaderCollapsedCls
                    }
                }
                if (a.subTotal) {
                    a.text = a.group.expanded ? a.group.getTextTotal() : Ext.String.format('<div class="x-pivot-grid-group-icon x-font-icon"></div><div class="' + this.groupCls + '">{0}</div>', a.group.name)
                } else if (a.group) {
                    a.text = Ext.String.format('<div class="x-pivot-grid-group-icon x-font-icon"></div><div class="' + this.groupCls + '">{0}</div>', a.group.name)
                }
                a.xexpandable = a.subTotal ? !a.group.expanded : a.group.expanded;
                if ((!a.group.expanded && !a.subTotal) || (a.group.expanded && a.subTotal && this.getMatrix().colSubTotalsPosition == 'none')) {
                    a.hidden = !0
                }
            }
            if (a.grandTotal) {
                a.cls = b.clsGrandTotal
            }
            if (Ext.isEmpty(a.columns)) {
                if (a.dimension) {
                    Ext.apply(a, {
                        renderer: a.dimension ? a.dimension.getRenderer() : !1,
                        formatter: a.dimension ? a.dimension.getFormatter() : !1,
                        scope: a.dimension ? a.dimension.scope : null,
                        align: a.dimension.align
                    });
                    if (a.dimension.flex > 0) {
                        a.flex = a.flex || a.dimension.flex
                    } else {
                        a.width = a.width || a.dimension.width
                    }
                    a.cell = Ext.merge(a.cell, a.dimension.cellConfig)
                }
                if (a.cell && a.cell.bind && !a.cell.viewModel) {
                    a.cell.bind = b.processBindKey(a.cell.bind, a.dataIndex)
                }
            } else {
                b.preparePivotColumns(a.columns)
            }
        }
    },
    processBindKey: function(a, d) {
        var b, e, f, c, g;
        if (Ext.isString(a)) {
            g = a.replace('{column', '{columns.' + d);
            return g.replace('{value', '{record.' + d)
        } else if (Ext.isObject(a)) {
            b = Ext.Object.getAllKeys(a)
        } else if (Ext.isArray(a)) {
            b = a
        }
        if (b) {
            f = b.length;
            for (c = 0; c < f; c++) {
                e = b[c];
                a[e] = this.processBindKey(a[e], d)
            }
        }
        return a
    },
    reconfigurePivot: function(a) {
        var b = this.getMatrix();
        a = a || {};
        if (b) {
            if (a.type && b.type !== a.type) {
                this.setMatrix(a)
            } else {
                b.reconfigure(a)
            }
        } else {
            this.setMatrix(a)
        }
    },
    doExpandCollapseTree: function(b, c) {
        var a;
        for (a = 0; a < b.length; a++) {
            b[a].expanded = c;
            if (b[a].children) {
                this.doExpandCollapseTree(b[a].children, c)
            }
        }
    },
    doExpandCollapse: function(f, e, b, d) {
        var c = this.getMatrix(),
            a;
        if (!c) {
            return
        }
        a = (f == 'row' ? c.leftAxis : c.topAxis)['findTreeElement']('key', e);
        if (!a) {
            return
        }
        a = a.node;
        b = Ext.isDefined(b) ? b : !a.expanded;
        if (b) {
            a.expand(d)
        } else {
            a.collapse(d)
        }
    },
    expandRow: function(b, a) {
        this.doExpandCollapse('row', b, !0, a)
    },
    collapseRow: function(b, a) {
        this.doExpandCollapse('row', b, !1, a)
    },
    expandCol: function(b, a) {
        this.doExpandCollapse('col', b, !0, a)
    },
    collapseCol: function(b, a) {
        this.doExpandCollapse('col', b, !1, a)
    },
    expandAll: function() {
        this.expandAllColumns();
        this.expandAllRows()
    },
    expandAllRows: function() {
        this.getMatrix().leftAxis.expandAll()
    },
    expandAllColumns: function() {
        this.getMatrix().topAxis.expandAll()
    },
    collapseAll: function() {
        this.collapseAllRows();
        this.collapseAllColumns()
    },
    collapseAllRows: function() {
        this.getMatrix().leftAxis.collapseAll()
    },
    collapseAllColumns: function() {
        this.getMatrix().topAxis.collapseAll()
    },
    setStore: function(a) {
        var b;
        if (!a || (a && a.$internal)) {
            this.callParent([a])
        } else {
            b = this.getMatrix();
            if (b && b instanceof Ext.pivot.matrix.Local) {
                b.reconfigure({
                    store: a
                })
            }
        }
    },
    getTopAxisKey: function(c) {
        var f = this,
            e = f.getMatrix(),
            b = e.getColumns(),
            d = null,
            a;
        if (!c) {
            return null
        }
        for (a = 0; a < b.length; a++) {
            if (b[a].name === c.getDataIndex()) {
                d = b[a].col;
                break
            }
        }
        return d
    },
    getTopAxisItem: function(a) {
        return this.getMatrix().topAxis.items.getByKey(this.getTopAxisKey(a))
    },
    getLeftAxisItem: function(b) {
        var a;
        if (!b) {
            return null
        }
        a = this.pivotDataSource.storeInfo[b.internalId];
        return a ? this.getMatrix().leftAxis.items.getByKey(a.leftKey) : null
    },
    getStateProperties: function() {
        return ['viewLayoutType', 'rowSubTotalsPosition', 'rowGrandTotalsPosition', 'colSubTotalsPosition', 'colGrandTotalsPosition', 'aggregate', 'leftAxis', 'topAxis', 'enableColumnSort', 'sortedColumn']
    },
    getPivotColumnsState: function() {
        var c = this,
            a, b;
        if (!c.lastColumnsState) {
            b = c.getDataIndexColumns(c.getMatrix().getColumnHeaders());
            c.lastColumnsState = {};
            for (a = 0; a < b.length; a++) {
                if (b[a].dataIndex) {
                    c.lastColumnsState[b[a].dataIndex] = {
                        width: b[a].width,
                        flex: b[a].flex || 0
                    }
                }
            }
        }
        b = c.getColumns();
        for (a = 0; a < b.length; a++) {
            if (b[a].dataIndex) {
                c.lastColumnsState[b[a].dataIndex] = {
                    width: b[a].rendered ? b[a].getWidth() : b[a].width,
                    flex: b[a].flex || 0
                }
            }
        }
        return c.lastColumnsState
    },
    getDataIndexColumns: function(b) {
        var c = [],
            a;
        for (a = 0; a < b.length; a++) {
            if (b[a].dataIndex) {
                c.push(b[a].dataIndex)
            } else if (Ext.isArray(b[a].columns)) {
                c = Ext.Array.merge(c, this.getDataIndexColumns(b[a].columns))
            }
        }
        return c
    },
    restorePivotColumnsState: function(a) {
        this.parsePivotColumnsState(this.getPivotColumnsState(), a)
    },
    parsePivotColumnsState: function(d, b) {
        var c, a;
        if (!b) {
            return
        }
        for (a = 0; a < b.length; a++) {
            c = d[b[a].dataIndex];
            if (c) {
                if (c.flex) {
                    b[a].flex = c.flex
                } else if (c.width) {
                    b[a].width = c.width
                }
            }
            this.parsePivotColumnsState(d, b[a].columns)
        }
    },
    onChildTap: function(a) {
        this.handleRowEvent('tap', a.event);
        Ext.grid.Grid.prototype.onChildTap.call(this, a)
    },
    onChildTapHold: function(a) {
        this.handleRowEvent('taphold', a.event);
        Ext.grid.Grid.prototype.onChildTapHold.call(this, a)
    },
    onChildSingleTap: function(a) {
        this.handleRowEvent('singletap', a.event);
        Ext.grid.Grid.prototype.onChildSingleTap.call(this, a)
    },
    onChildDoubleTap: function(a) {
        this.handleRowEvent('doubletap', a.event);
        Ext.grid.Grid.prototype.onChildDoubleTap.call(this, a)
    },
    handleRowEvent: function(e, d) {
        var a = Ext.fly(d.getTarget(this.cellSelector)),
            c = this.getNavigationModel(),
            b;
        if (a && a.component && a.component.handleEvent) {
            a.component.handleEvent(e, d);
            if (c) {
                b = c.getLocation();
                if (b) {
                    b.refresh()
                }
            }
        }
    },
    handleColumnTap: function(e, a, d) {
        var c = this,
            b;
        if (a.xexpandable) {
            this.handleHeaderGroupTap(e, a, d);
            return !1
        }
        if (!c.enableColumnSort) {
            return
        }
        b = (a.$axisSortDirection === 'ASC') ? 'DESC' : 'ASC';
        if ((a.leftAxis || a.topAxis) && !Ext.isEmpty(a.getDataIndex())) {
            if (c.getMatrix().leftAxis.sortTreeByField(a.getDataIndex(), b)) {
                a.$axisSortDirection = b;
                c.refreshView();
                c.updateSortIndicator(a, b);
                d.stopEvent()
            }
        }
        return !1
    },
    updateSortIndicator: function(b, c) {
        var a = this.lastColumnSorted;
        if (a) {
            a.setSortState(null);
            if (b.topAxis && a.leftAxis) {
                a.$axisSortDirection = null
            }
        }
        b.setSortState(c);
        this.lastColumnSorted = b
    },
    handleHeaderGroupTap: function(c, a, b) {
        if (a.xexpandable && this._matrix.collapsibleColumns) {
            b.stopEvent();
            this.doExpandCollapse('col', a.key)
        }
        return !1
    },
    isRTL: function() {
        if (Ext.isFunction(this.isLocalRtl)) {
            return this.isLocalRtl()
        }
        return !1
    },
    getRecordInfo: function(a) {
        return a ? this.pivotDataSource.storeInfo[a.internalId] : null
    },
    onColumnRemove: function(b, a) {
        if (a === this.lastColumnSorted) {
            this.lastColumnSorted = null
        }
        return Ext.grid.Grid.prototype.onColumnRemove.call(this, b, a)
    },
    privates: {
        discardMeasureRow: !0
    }
}, 0, ["pivotgrid"], ["widget", "component", "container", "componentdataview", "list", "grid", "pivotgrid"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "componentdataview": !0,
    "list": !0,
    "grid": !0,
    "pivotgrid": !0
}, ["widget.pivotgrid"], 0, [Ext.pivot, 'Grid'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.Column', Ext.dataview.ListItem, {
    userCls: 'x-pivot-grid-com.landau.core.config-column',
    filteredCls: 'x-pivot-grid-com.landau.core.config-column-filter',
    ascSortIconCls: 'x-pivot-grid-com.landau.core.config-column-btn-sort-asc',
    descSortIconCls: 'x-pivot-grid-com.landau.core.config-column-btn-sort-desc',
    config: {
        deleteCmp: {
            xtype: 'component',
            cls: ['x-pivot-grid-com.landau.core.config-column-btn', 'x-pivot-grid-com.landau.core.config-column-btn-delete'],
            docked: 'right',
            hidden: !0
        },
        moveCmp: {
            xtype: 'component',
            cls: ['x-pivot-grid-com.landau.core.config-column-btn', 'x-pivot-grid-com.landau.core.config-column-btn-move'],
            docked: 'left'
        },
        sortCmp: {
            xtype: 'component',
            cls: 'x-pivot-grid-com.landau.core.config-column-btn',
            docked: 'right',
            hidden: !0
        }
    },
    applyDeleteCmp: function(a, b) {
        if (a && !a.isComponent) {
            a = Ext.factory(a, Ext.Component, b)
        }
        return a
    },
    updateDeleteCmp: function(a, b) {
        if (a) {
            this.add(a)
        }
        Ext.destroy(b)
    },
    applyMoveCmp: function(a, b) {
        if (a && !a.isComponent) {
            a = Ext.factory(a, Ext.Component, b)
        }
        return a
    },
    updateMoveCmp: function(a, b) {
        if (a) {
            this.add(a)
        }
        Ext.destroy(b)
    },
    applySortCmp: function(a, b) {
        if (a && !a.isComponent) {
            a = Ext.factory(a, Ext.Component, b)
        }
        return a
    },
    updateSortCmp: function(a, b) {
        if (a) {
            this.add(a)
        }
        Ext.destroy(b)
    },
    getField: function() {
        return this.getRecord().get('field')
    },
    updateRecord: function(d, c) {
        var b = this,
            f = b.innerElement,
            a, e;
        Ext.dataview.ListItem.prototype.updateRecord.call(this, d, c);
        if (!d) {
            return
        }
        if (c) {
            e = c.get('field');
            if (e) {
                a = c.get('field').getSettings();
                b.resetStyle(f, a.getStyle());
                b.removeCls(a.getCls())
            }
        }
        e = d.get('field');
        if (e) {
            a = d.get('field').getSettings();
            f.setStyle(a.getStyle());
            b.addCls(a.getCls());
            b.refreshData()
        }
    },
    refreshData: function() {
        var a = this,
            i = a.getDeleteCmp(),
            e = a.getRecord(),
            f = e.get('field'),
            h = f.getSettings(),
            b = a.dataview || a.getDataview(),
            c = b.getFieldType ? b : b.up('pivotconfigcontainer'),
            g = c.getFieldType(),
            d;
        if (g !== 'all') {
            d = h.isFixed(c);
            i.setHidden(d)
        }
        e.set('text', f.getFieldText());
        a.updateSortCmpCls();
        a.updateFilterCls()
    },
    updateFilterCls: function() {
        var a = this,
            b = a.dataview || a.getDataview(),
            c = b.getFieldType ? b : b.up('pivotconfigcontainer'),
            d = c.getFieldType();
        if (d !== 'all') {
            if (a.getField().getFilter()) {
                a.addCls(a.filteredCls)
            } else {
                a.removeCls(a.filteredCls)
            }
        }
    },
    updateSortCmpCls: function() {
        var a = this,
            c = a.dataview || a.getDataview(),
            f = c.getFieldType ? c : c.up('pivotconfigcontainer'),
            d = f.getFieldType(),
            e = a.getField(),
            b = a.getSortCmp();
        if (d === 'leftAxis' || d === 'topAxis') {
            b.show();
            b.setUserCls('');
            if (e.getSortable()) {
                if (e.getDirection() === 'ASC') {
                    b.setUserCls(a.ascSortIconCls)
                } else {
                    b.setUserCls(a.descSortIconCls)
                }
            }
        } else {
            b.hide()
        }
    },
    resetStyle: function(e, d) {
        var b = {},
            c = Ext.Object.getAllKeys(d),
            f = c.length,
            a;
        for (a = 0; a < f; a++) {
            b[c[a]] = null
        }
        e.setStyle(b)
    },
    onApplyFilterSettings: function(b, a) {
        this.getField().setFilter(a);
        this.updateFilterCls();
        this.applyChanges()
    },
    onRemoveFilter: function() {
        this.getField().setFilter(null);
        this.updateFilterCls();
        this.applyChanges()
    },
    applyChanges: function() {
        var a = this.dataview || this.getDataView();
        if (a) {
            a.applyChanges()
        }
    }
}, 0, ["pivotconfigfield"], ["widget", "component", "container", "dataitem", "listitem", "pivotconfigfield"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "dataitem": !0,
    "listitem": !0,
    "pivotconfigfield": !0
}, ["widget.pivotconfigfield"], 0, [Ext.pivot.plugin.configurator, 'Column'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.DragZone', Ext.drag.Source, {
    groups: 'pivotfields',
    proxy: {
        type: 'placeholder',
        cursorOffset: [-20, 30],
        placeholderCls: 'x-pivot-drag-proxy-placeholder',
        validCls: 'x-pivot-drag-proxy-placeholder-valid',
        invalidCls: 'x-pivot-drag-proxy-placeholder-invalid'
    },
    handle: '.x-listitem',
    platformConfig: {
        phone: {
            activateOnLongPress: !0
        }
    },
    constructor: function(a) {
        var b = a.getList();
        this.panel = a;
        this.list = b;
        Ext.drag.Source.prototype.constructor.call(this, {
            element: b.getScrollable().getElement()
        })
    },
    onDragStart: function(b) {
        var c = Ext.fly(b.eventTarget).up(this.getHandle()),
            d = '<span class="x-pivot-drag-placeholder-icon">&nbsp;</span><span>{0}</span>',
            a;
        if (!c || !c.component) {
            return
        }
        a = c.component.getRecord();
        b.setData('record', a);
        b.setData('sourceList', this.list);
        this.getProxy().setHtml(Ext.String.format(d, a.get('text')))
    }
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.plugin.configurator, 'DragZone'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.DropZone', Ext.drag.Target, {
    groups: 'pivotfields',
    leftIndicatorCls: 'x-pivot-grid-left-indicator',
    rightIndicatorCls: 'x-pivot-grid-right-indicator',
    listItemSelector: '.x-listitem',
    listSelector: '.x-list',
    constructor: function(a) {
        var b = a.getList();
        this.panel = a;
        this.list = b;
        Ext.drag.Target.prototype.constructor.call(this, {
            element: b.getScrollable().getElement()
        })
    },
    getLeftIndicator: function() {
        if (!this.leftIndicator) {
            this.self.prototype.leftIndicator = Ext.getBody().createChild({
                cls: this.leftIndicatorCls,
                html: "&#160;"
            });
            this.self.prototype.indicatorWidth = this.leftIndicator.dom.offsetWidth;
            this.self.prototype.indicatorOffset = Math.round(this.indicatorWidth / 2)
        }
        return this.leftIndicator
    },
    getRightIndicator: function() {
        if (!this.rightIndicator) {
            this.self.prototype.rightIndicator = Ext.getBody().createChild({
                cls: this.rightIndicatorCls,
                html: "&#160;"
            })
        }
        return this.rightIndicator
    },
    accepts: function(f) {
        var c = f.data,
            d = c.record,
            a = c.sourceList,
            b = this.panel,
            e = b.up('pivotconfigpanel');
        if (!d) {
            return !0
        }
        if (!a.isXType('pivotconfigcontainer')) {
            a = a.up('pivotconfigcontainer')
        }
        return d.get('field').getSettings().isAllowed(b) && e.isAllowed(a, b, c.record)
    },
    onDragMove: function(a) {
        if (a.valid) {
            this.positionIndicator(a)
        } else {
            this.hideIndicators()
        }
    },
    onDragLeave: function(a) {
        this.hideIndicators()
    },
    onDrop: function(e) {
        var b = e.data,
            a = b.sourceList,
            c = this.panel,
            d = c.up('pivotconfigpanel');
        this.hideIndicators();
        if (!d) {
            return
        }
        if (!a.isXType('pivotconfigcontainer')) {
            a = a.up('pivotconfigcontainer')
        }
        d.dragDropField(a, c, b.record, b.position)
    },
    positionIndicator: function(n) {
        var b = this,
            i = -1,
            k = b.getLeftIndicator(),
            j = b.getRightIndicator(),
            m = b.indicatorWidth,
            q = b.indicatorOffset,
            a, e, d, c, f, o, g, p, h, l;
        a = b.getCursorElement(n, b.listItemSelector);
        if (a) {
            e = a.component;
            h = 'tl';
            l = 'tr';
            i = e.$dataIndex
        } else {
            h = 'bl';
            l = 'br';
            i = b.list.getViewItems().length;
            e = b.list.getItemAt(i - 1);
            if (e) {
                a = e.element
            } else {
                a = b.getCursorElement(n, b.listSelector)
            }
        }
        d = k.getAlignToXY(a, h);
        c = j.getAlignToXY(a, l);
        d[0] -= 1;
        c[0] -= m;
        if (h === 'tl') {
            d[1] -= m;
            c[1] -= m
        }
        f = g = -q;
        f += a.getX();
        o = f + a.getWidth();
        g += a.getY();
        p = g + a.getHeight();
        d[0] = Ext.Number.constrain(d[0], f, o);
        c[0] = Ext.Number.constrain(c[0], f, o);
        d[1] = Ext.Number.constrain(d[1], g, p);
        c[1] = Ext.Number.constrain(c[1], g, p);
        k.show();
        j.show();
        k.setXY(d);
        j.setXY(c);
        n.setData('position', i)
    },
    hideIndicators: function() {
        this.getLeftIndicator().hide();
        this.getRightIndicator().hide()
    },
    getCursorElement: function(f, c, e, b) {
        var a = f.cursor.current,
            d = Ext.drag.Manager.elementFromPoint(a.x, a.y);
        return Ext.fly(d).up(c, e, b)
    }
}, 1, 0, 0, 0, 0, 0, [Ext.pivot.plugin.configurator, 'DropZone'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.Container', Ext.Panel, {
    config: {
        fieldType: 'all',
        emptyText: null,
        store: {
            type: 'array',
            fields: [{
                name: 'text',
                type: 'string'
            }, {
                name: 'field',
                type: 'auto'
            }]
        },
        list: {
            xclass: 'Ext.dataview.List',
            handleSorting: !1,
            handleFiltering: !1,
            isConfiguratorContainer: !0,
            disableSelection: !0,
            itemConfig: {
                xtype: 'pivotconfigfield'
            },
            deferEmptyText: !1,
            touchAction: {
                panX: !1,
                pinchZoom: !1,
                doubleTapZoom: !1
            },
            itemRipple: !1
        }
    },
    layout: 'fit',
    initialize: function() {
        var a = this,
            b = a.getList();
        Ext.Panel.prototype.initialize.call(this);
        a.dragZone = new Ext.pivot.plugin.configurator.DragZone(a);
        a.dropZone = new Ext.pivot.plugin.configurator.DropZone(a);
        if (a.getFieldType() !== 'all') {
            b.element.on({
                delegate: '.x-pivot-grid-com.landau.core.config-column-btn',
                tap: a.handleColumnBtnTap,
                scope: a
            });
            b.element.on({
                delegate: '.x-listitem-body-el',
                tap: a.handleColumnTap,
                scope: a
            })
        }
    },
    destroy: function() {
        Ext.destroyMembers(this, 'storeListeners', 'dragZone', 'dropZone');
        Ext.Panel.prototype.destroy.call(this)
    },
    updateFieldType: function(a) {
        if (a !== 'all') {
            this.setUserCls('x-pivot-grid-com.landau.core.config-container')
        }
    },
    updateEmptyText: function(b) {
        var a = this.getList();
        if (a) {
            a.setEmptyText(b)
        }
    },
    applyList: function(a, c) {
        var b;
        if (a) {
            b = this.getStore();
            if (a.isComponent) {
                a.setStore(b)
            } else {
                a.store = b;
                a.emptyText = this.getEmptyText();
                a = Ext.factory(a, Ext.dataview.List, c)
            }
        }
        return a
    },
    updateList: function(a) {
        if (a) {
            this.add(a)
        }
    },
    applyStore: function(a) {
        return Ext.Factory.store(a)
    },
    updateStore: function(b, c) {
        var a = this;
        Ext.destroy(a.storeListeners);
        if (b) {
            a.storeListeners = b.on({
                datachanged: a.applyChanges,
                scope: a
            })
        }
    },
    applyChanges: function() {
        if (this.getFieldType() !== 'all') {
            this.fireEvent('configchange', this)
        }
    },
    addField: function(a, d) {
        var e = this,
            c = e.getStore(),
            f = e.getFieldType(),
            b = {};
        a.isAggregate = (f === 'aggregate');
        Ext.apply(b, {
            field: a,
            text: a.getFieldText()
        });
        if (d >= 0) {
            c.insert(d, b)
        } else {
            c.add(b)
        }
    },
    removeField: function(a) {
        this.getStore().remove(a);
        a.set('field', null)
    },
    moveField: function(b, c) {
        var a = this.getStore(),
            d = a.indexOf(b);
        if (c === -1 && d === a.getCount() - 1) {
            return
        }
        a.remove(b);
        if (c >= 0) {
            a.insert(c, b)
        } else {
            a.add(b)
        }
    },
    handleColumnBtnTap: function(e) {
        var a = this,
            b = Ext.fly(e.currentTarget),
            c = b.up('.' + a.getList().baseCls + 'item').component,
            d = c.getRecord();
        if (!d) {
            return
        }
        if (b.hasCls('x-pivot-grid-com.landau.core.config-column-btn-delete')) {
            a.fireEvent('removefield', a, c, d);
            return
        }
        if (b.hasCls('x-pivot-grid-com.landau.core.config-column-btn-tools')) {
            a.fireEvent('toolsbtnpressed', a, c)
        }
    },
    handleColumnTap: function(e) {
        var a = this,
            d = Ext.fly(e.currentTarget),
            b = d.up('.' + a.getList().baseCls + 'item').component,
            c = b.getRecord();
        if (!c) {
            return
        }
        a.fireEvent('toolsbtnpressed', a, b)
    }
}, 0, ["pivotconfigcontainer"], ["widget", "component", "container", "panel", "pivotconfigcontainer"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "panel": !0,
    "pivotconfigcontainer": !0
}, ["widget.pivotconfigcontainer"], 0, [Ext.pivot.plugin.configurator, 'Container'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.PanelController', Ext.app.ViewController, {
    destroy: function() {
        var a = this;
        a.pivotListeners = a.matrixProperties = Ext.destroy(a.pivotListeners);
        Ext.app.ViewController.prototype.destroy.call(this)
    },
    closeMe: function() {
        var a = this.getView();
        a.fireEvent('close', a)
    },
    cancelConfiguration: function() {
        this.refreshDimensions();
        this.matrixProperties = null;
        this.closeMe()
    },
    applyConfiguration: function() {
        this.applyChanges().then(function(a) {
            a.closeMe()
        })
    },
    showSettings: function() {
        var b = this.getView(),
            d = b.getPivot(),
            a = d.getMatrix().serialize(),
            c = b.down('#settings');
        delete(a.leftAxis);
        delete(a.topAxis);
        delete(a.aggregate);
        if (d.fireEvent('beforeshowpivotsettings', b, {
                container: c,
                settings: a
            }) !== !1) {
            b.setActiveItem(c);
            c.setMatrixProperties(a);
            d.fireEvent('showpivotsettings', b, {
                container: c,
                settings: a
            })
        }
    },
    backToMainView: function() {
        this.getView().setActiveItem('#main')
    },
    onPivotChanged: function(c, b) {
        var a = this;
        Ext.destroy(a.pivotListeners);
        if (b) {
            a.pivotListeners = b.getMatrix().on({
                done: a.onPivotDone,
                scope: a,
                destroyable: !0
            })
        }
    },
    onFieldsChanged: function(b, a) {
        if (!a) {
            return
        }
        this.refreshDimensions()
    },
    onBeforeApplyPivotSettings: function(c, b) {
        var a = this.getView();
        return a.getPivot().fireEvent('beforeapplypivotsettings', a, {
            container: c,
            settings: b
        })
    },
    onApplyPivotSettings: function(c, a) {
        var b = this.getView();
        this.matrixProperties = a;
        this.onConfigChanged();
        return b.getPivot().fireEvent('applypivotsettings', b, {
            container: c,
            settings: a
        })
    },
    onBeforeApplyConfigFieldSettings: function(c, b) {
        var a = this.getView();
        return a.getPivot().fireEvent('beforeapplyconfigfieldsettings', a, {
            container: c,
            settings: b
        })
    },
    onApplyConfigFieldSettings: function(c, b) {
        var a = this.getView();
        this.onConfigChanged();
        return a.getPivot().fireEvent('applyconfigfieldsettings', a, {
            container: c,
            settings: b
        })
    },
    onConfigChanged: function() {
        this.configurationChanged = !0
    },
    showCard: function(k, i) {
        var b = this.getView(),
            h = b.getPivot(),
            f = i.getField().getSettings(),
            a = b.down('#field'),
            g = [],
            e, d, c, j;
        if (h.fireEvent('beforeshowconfigfieldsettings', b, {
                container: a,
                settings: f
            }) !== !1) {
            b.setActiveItem(a);
            a.setFieldItem(i);
            e = this.getAggregateContainer().getStore();
            j = e.getCount();
            for (c = 0; c < j; c++) {
                d = e.getAt(c).get('field');
                g.push([d.getHeader(), d.getId()])
            }
            a.getViewModel().getStore('sDimensions').loadData(g);
            h.fireEvent('showconfigfieldsettings', b, {
                container: a,
                settings: f
            })
        }
    },
    onRemoveField: function(b, d, c) {
        var a = this.getView();
        a.dragDropField(b, a.getAllFieldsContainer(), c)
    },
    refreshDimensions: function() {
        var a = this,
            l = a.getView(),
            k = l.getPivot(),
            b = k ? k.getMatrix() : null,
            f, c, d, e, i, g, h, j;
        if (!b) {
            return
        }
        a.internalReconfiguration = !0;
        e = a.getAllFieldsContainer();
        f = a.getTopAxisContainer();
        c = a.getLeftAxisContainer();
        d = a.getAggregateContainer();
        e.getStore().removeAll();
        f.getStore().removeAll();
        c.getStore().removeAll();
        d.getStore().removeAll();
        j = l.getFields().clone();
        i = a.getConfigFields(b.topAxis.dimensions.getRange());
        g = a.getConfigFields(b.leftAxis.dimensions.getRange());
        h = a.getConfigFields(b.aggregate.getRange());
        a.addFieldsToConfigurator(j.getRange(), e);
        a.addFieldsToConfigurator(i, f);
        a.addFieldsToConfigurator(g, c);
        a.addFieldsToConfigurator(h, d);
        a.internalReconfiguration = !1
    },
    onPivotDone: function() {
        if (this.internalReconfiguration) {
            this.internalReconfiguration = !1
        } else {
            this.refreshDimensions()
        }
    },
    reconfigurePivot: function(e, f) {
        var a = this,
            d = a.getView(),
            c = d.getPivot(),
            b = {
                topAxis: a.getFieldsFromContainer(a.getTopAxisContainer(), !0),
                leftAxis: a.getFieldsFromContainer(a.getLeftAxisContainer(), !0),
                aggregate: a.getFieldsFromContainer(a.getAggregateContainer(), !0)
            };
        Ext.apply(b, a.matrixProperties);
        a.internalReconfiguration = !0;
        if (c.fireEvent('beforeconfigchange', d, b) !== !1) {
            c.getMatrix().reconfigure(b);
            c.fireEvent('configchange', d, b)
        }
        e(a)
    },
    getAllFieldsContainer: function() {
        return this.lookupReference('fieldsCt')
    },
    getLeftAxisContainer: function() {
        return this.lookupReference('fieldsLeftCt')
    },
    getTopAxisContainer: function() {
        return this.lookupReference('fieldsTopCt')
    },
    getAggregateContainer: function() {
        return this.lookupReference('fieldsAggCt')
    },
    applyChanges: function() {
        var a = this;
        return new Ext.Promise(function(b, c) {
            var d = a.getView();
            if (a.configurationChanged) {
                a.configurationChanged = !1;
                if (d.isHidden() || a.internalReconfiguration) {
                    c(a);
                    return
                }
                Ext.asap(a.reconfigurePivot, a, [b, c])
            } else {
                b(a)
            }
        })
    },
    getFieldsFromContainer: function(g, e) {
        var d = g.getStore(),
            f = d.getCount(),
            c = [],
            a, b;
        for (a = 0; a < f; a++) {
            b = d.getAt(a).get('field');
            c.push(e === !0 ? b.getConfiguration() : b)
        }
        return c
    },
    addFieldsToConfigurator: function(c, b) {
        var d = c.length,
            a;
        b.getStore().removeAll();
        for (a = 0; a < d; a++) {
            b.addField(c[a], -1)
        }
        b.getList().refresh()
    },
    getConfigFields: function(d) {
        var g = d.length,
            f = this.getView().getFields(),
            e = [],
            b, a, c;
        for (b = 0; b < g; b++) {
            c = d[b];
            a = f.byDataIndex.get(c.dataIndex);
            if (a) {
                a = a.clone();
                a.setConfig(c.getInitialConfig());
                e.push(a)
            }
        }
        return e
    }
}, 0, 0, 0, 0, ["controller.pivotconfigpanel"], 0, [Ext.pivot.plugin.configurator, 'PanelController'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.model.Select', Ext.data.Model, {
    fields: [{
        name: 'text',
        type: 'string'
    }, {
        name: 'value',
        type: 'auto'
    }, {
        name: 'type',
        type: 'integer'
    }]
}, 0, 0, 0, 0, 0, 0, [Ext.pivot.plugin.configurator.model, 'Select'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.store.Select', Ext.data.ArrayStore, {
    model: 'Ext.pivot.plugin.configurator.model.Select'
}, 0, 0, 0, 0, ["store.pivotselect"], 0, [Ext.pivot.plugin.configurator.store, 'Select'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.FormController', Ext.app.ViewController, {
    init: function(a) {
        var b = this.getViewModel();
        b.getStore('sSorters').loadData([
            [a.sortClearText, 'none'],
            [a.sortAscText, 'ASC'],
            [a.sortDescText, 'DESC']
        ]);
        b.getStore('sFilters').loadData([
            [a.clearFilterText, 'none'],
            [a.labelFiltersText, 'label'],
            [a.valueFiltersText, 'value'],
            [a.top10FiltersText, 'top10']
        ]);
        b.getStore('sTopOrder').loadData([
            [a.topOrderTopText, 'top'],
            [a.topOrderBottomText, 'bottom']
        ]);
        b.getStore('sTopType').loadData([
            [a.topTypeItemsText, 'items'],
            [a.topTypePercentText, 'percent'],
            [a.topTypeSumText, 'sum']
        ]);
        b.getStore('sAlign').loadData([
            [a.alignLeftText, 'left'],
            [a.alignCenterText, 'center'],
            [a.alignRightText, 'right']
        ]);
        b.set('labelFilterText', a.labelFilterText);
        b.set('valueFilterText', a.valueFilterText);
        b.set('requiredMessage', a.requiredFieldText)
    },
    applySettings: function() {
        var j = this.getViewModel(),
            e = this.getView(),
            g = e.getFieldItem(),
            h = g.getField(),
            b = Ext.clone(j.get('form')),
            d, i, c, a, f;
        if (!e.isValid()) {
            return
        }
        if (b.align && b.align.isModel) {
            b.align = b.align.get('value')
        }
        if (h.isAggregate) {
            if (b.formatter && b.formatter.isModel) {
                d = b.formatter
            } else {
                i = j.getStore('sFormatters');
                d = i.findRecord('value', b.formatter, 0, !1, !0, !0)
            }
            if (d) {
                if (d.get('type') == 1) {
                    b.formatter = d.get('value');
                    b.renderer = null
                } else {
                    b.renderer = d.get('value');
                    b.formatter = null
                }
            }
            if (b.aggregator && b.aggregator.isModel) {
                b.aggregator = b.aggregator.get('value')
            }
        } else {
            c = b.direction;
            if (c && c.isModel) {
                c = c.get('value')
            }
            b.sortable = (c !== 'none');
            b.direction = c || 'ASC';
            a = b.filter;
            f = a.type.isModel ? a.type.get('value') : a.type;
            if (!a || !a.type || f === 'none') {
                a = null
            } else {
                a.type = f;
                if (a.operator && a.operator.isModel) {
                    a.operator = a.operator.get('value')
                }
                if (a.dimensionId && a.dimensionId.isModel) {
                    a.dimensionId = a.dimensionId.get('value')
                }
                if (a.topType && a.topType.isModel) {
                    a.topType = a.topType.get('value')
                }
                if (a.topOrder && a.topOrder.isModel) {
                    a.topOrder = a.topOrder.get('value')
                }
                if (a.type === 'top10') {
                    a.type = 'value';
                    a.operator = 'top10'
                }
                if (a.operator === 'between' || a.operator === 'not between') {
                    a.value = [a.from, a.to]
                }
                delete(a.from);
                delete(a.to);
                if (a.type === 'label') {
                    delete(a.dimensionId);
                    delete(a.topSort);
                    delete(a.topType);
                    delete(a.topOrder)
                }
            }
            b.filter = a
        }
        if (e.fireEvent('beforeapplyconfigfieldsettings', e, b) !== !1) {
            h.setConfig(b);
            g.refreshData();
            e.fireEvent('applyconfigfieldsettings', e, b);
            this.cancelSettings()
        }
    },
    cancelSettings: function() {
        var a = this.getView();
        a.setFieldItem(null);
        a.fireEvent('close', a)
    },
    onFieldItemChanged: function(q, p) {
        var i = this.getViewModel(),
            h = {},
            l = [],
            o = [],
            g, j, m, n, k, f, b, d, c, a, e;
        if (!p) {
            i.set('form', h);
            return
        }
        g = p.getField();
        c = g.getConfig();
        Ext.apply(h, {
            dataIndex: c.dataIndex,
            header: c.header
        });
        if (g.isAggregate) {
            j = g.getSettings();
            m = j.getFormatters();
            n = j.getRenderers();
            k = j.getAggregators();
            f = k.length;
            for (b = 0; b < f; b++) {
                o.push([g.getAggText(k[b]), k[b]])
            }
            d = Ext.Object.getAllKeys(m || {});
            f = d.length;
            for (b = 0; b < f; b++) {
                l.push([d[b], m[d[b]], 1])
            }
            d = Ext.Object.getAllKeys(n || {});
            f = d.length;
            for (b = 0; b < f; b++) {
                l.push([d[b], n[d[b]], 2])
            }
            i.getStore('sFormatters').loadData(l);
            i.getStore('sAggregators').loadData(o);
            e = c.formatter;
            if (Ext.isFunction(e)) {
                e = null
            }
            if (!e && !Ext.isFunction(c.renderer)) {
                e = c.renderer
            }
            Ext.apply(h, {
                formatter: e,
                aggregator: c.aggregator,
                align: c.align
            })
        } else {
            a = c.filter;
            Ext.apply(h, {
                direction: (c.sortable ? c.direction : 'none'),
                filter: {
                    type: (a ? (a.type === 'value' && a.operator === 'top10' ? 'top10' : a.type) : 'none'),
                    operator: (a ? (a.type === 'value' && a.operator === 'top10' ? 'top10' : a.operator) : null),
                    value: (a ? a.value : null),
                    from: (a ? (Ext.isArray(a.value) ? a.value[0] : null) : null),
                    to: (a ? (Ext.isArray(a.value) ? a.value[1] : null) : null),
                    caseSensitive: (a ? a.caseSensitive : !1),
                    topSort: (a ? a.topSort : !1),
                    topOrder: (a ? a.topOrder : !1),
                    topType: (a ? a.topType : !1),
                    dimensionId: (a ? a.dimensionId : null)
                }
            })
        }
        i.set('form', h)
    },
    prepareOperators: function(d) {
        var a = this.getView(),
            c = this.getViewModel(),
            b;
        b = [
            [a.equalsLText, '='],
            [a.doesNotEqualLText, '!='],
            [a.greaterThanLText, '>'],
            [a.greaterThanOrEqualToLText, '>='],
            [a.lessThanLText, '<'],
            [a.lessThanOrEqualToLText, '<='],
            [a.betweenLText, 'between'],
            [a.notBetweenLText, 'not between']
        ];
        if (d === 'label') {
            Ext.Array.insert(b, 3, [
                [a.beginsWithLText, 'begins'],
                [a.doesNotBeginWithLText, 'not begins'],
                [a.endsWithLText, 'ends'],
                [a.doesNotEndWithLText, 'not ends'],
                [a.containsLText, 'contains'],
                [a.doesNotContainLText, 'not contains']
            ])
        }
        c.getStore('sOperators').loadData(b)
    },
    onChangeFilterType: function(c, a) {
        var b = a && a.isModel ? a.get('value') : a;
        if (b === 'label' || b === 'value') {
            this.prepareOperators(b)
        }
    }
}, 0, 0, 0, 0, ["controller.pivotconfigform"], 0, [Ext.pivot.plugin.configurator, 'FormController'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.Form', Ext.form.Panel, {
    controller: 'pivotconfigform',
    viewModel: {
        stores: {
            sFormatters: {
                type: 'pivotselect'
            },
            sAggregators: {
                type: 'pivotselect'
            },
            sSorters: {
                type: 'pivotselect'
            },
            sFilters: {
                type: 'pivotselect'
            },
            sOperators: {
                type: 'pivotselect'
            },
            sTopOrder: {
                type: 'pivotselect'
            },
            sTopType: {
                type: 'pivotselect'
            },
            sDimensions: {
                type: 'pivotselect'
            },
            sAlign: {
                type: 'pivotselect'
            }
        },
        data: {
            requiredMessage: null,
            labelFilterText: null,
            valueFilterText: null
        },
        formulas: {
            filterType: {
                bind: {
                    bindTo: '{form.filter.type}',
                    deep: !0
                },
                get: function(a) {
                    return a && a.isModel ? a.get('value') : a
                }
            },
            filterOperator: {
                bind: {
                    bindTo: '{form.filter.operator}',
                    deep: !0
                },
                get: function(a) {
                    return a && a.isModel ? a.get('value') : a
                }
            },
            filterOperatorValue: {
                bind: '{filterCommon && !(filterOperator === "between" || filterOperator === "not between")}',
                get: function(a) {
                    return a
                }
            },
            filterOperatorBetween: {
                bind: '{filterCommon && (filterOperator === "between" || filterOperator === "not between")}',
                get: function(a) {
                    return a
                }
            },
            filterCommon: {
                bind: '{filterType === "label" || filterType === "value"}',
                get: function(a) {
                    return a
                }
            },
            filterLabel: {
                bind: '{filterType === "label"}',
                get: function(a) {
                    return a
                }
            },
            filterValue: {
                bind: '{filterType === "value"}',
                get: function(a) {
                    return a
                }
            },
            filterTop10: {
                bind: '{filterType === "top10"}',
                get: function(a) {
                    return a
                }
            }
        }
    },
    eventedConfig: {
        fieldItem: null,
        title: null
    },
    listeners: {
        fielditemchange: 'onFieldItemChanged'
    },
    defaults: {
        xtype: 'fieldset',
        defaults: {
            labelAlign: 'top'
        }
    },
    showAnimation: {
        type: 'slideIn',
        duration: 250,
        easing: 'ease-out',
        direction: 'left'
    },
    hideAnimation: {
        type: 'slideOut',
        duration: 250,
        easing: 'ease-in',
        direction: 'right'
    },
    okText: 'Ok',
    cancelText: 'Cancel',
    formatText: 'Format as',
    summarizeByText: 'Summarize by',
    customNameText: 'Custom name',
    sourceNameText: 'The source name for this field is "{form.dataIndex}"',
    sortText: 'Sort',
    filterText: 'Filter',
    sortResultsText: 'Sort results',
    alignText: 'Align',
    alignLeftText: 'Left',
    alignCenterText: 'Center',
    alignRightText: 'Right',
    caseSensitiveText: 'Case sensitive',
    valueText: 'Value',
    fromText: 'From',
    toText: 'To',
    labelFilterText: 'Show items for which the label',
    valueFilterText: 'Show items for which',
    top10FilterText: 'Show',
    sortAscText: 'Sort A to Z',
    sortDescText: 'Sort Z to A',
    sortClearText: 'Disable sorting',
    clearFilterText: 'Disable filtering',
    labelFiltersText: 'Label com.landau.core.filters',
    valueFiltersText: 'Value com.landau.core.filters',
    top10FiltersText: 'Top 10 com.landau.core.filters',
    equalsLText: 'equals',
    doesNotEqualLText: 'does not equal',
    beginsWithLText: 'begins with',
    doesNotBeginWithLText: 'does not begin with',
    endsWithLText: 'ends with',
    doesNotEndWithLText: 'does not end with',
    containsLText: 'contains',
    doesNotContainLText: 'does not contain',
    greaterThanLText: 'is greater than',
    greaterThanOrEqualToLText: 'is greater than or equal to',
    lessThanLText: 'is less than',
    lessThanOrEqualToLText: 'is less than or equal to',
    betweenLText: 'is between',
    notBetweenLText: 'is not between',
    topOrderTopText: 'Top',
    topOrderBottomText: 'Bottom',
    topTypeItemsText: 'Items',
    topTypePercentText: 'Percent',
    topTypeSumText: 'Sum',
    requiredFieldText: 'This field is required',
    operatorText: 'Operator',
    dimensionText: 'Dimension',
    orderText: 'Order',
    typeText: 'Type',
    updateFieldItem: function(d) {
        var a = this,
            b, c;
        a.removeAll(!0, !0);
        if (!d) {
            return
        }
        c = d.getField();
        b = [{
            xtype: 'titlebar',
            docked: 'top',
            titleAlign: 'left',
            bind: {
                title: '{form.header}'
            },
            items: [{
                text: a.cancelText,
                align: 'right',
                ui: 'alt',
                handler: 'cancelSettings'
            }, {
                text: a.okText,
                align: 'right',
                ui: 'alt',
                handler: 'applySettings',
                margin: '0 0 0 5'
            }]
        }, {
            bind: {
                instructions: a.sourceNameText
            },
            items: [{
                label: a.customNameText,
                xtype: 'textfield',
                name: 'header',
                required: !0,
                requiredMessage: a.requiredFieldText,
                bind: '{form.header}'
            }]
        }];
        if (c.isAggregate) {
            b.push({
                items: [{
                    label: a.alignText,
                    xtype: 'selectfield',
                    autoSelect: !1,
                    useClearIcon: !0,
                    name: 'align',
                    bind: {
                        store: '{sAlign}',
                        value: '{form.align}'
                    }
                }, {
                    label: a.formatText,
                    xtype: 'selectfield',
                    autoSelect: !1,
                    useClearIcon: !0,
                    name: 'formatter',
                    bind: {
                        store: '{sFormatters}',
                        value: '{form.formatter}'
                    }
                }, {
                    label: a.summarizeByText,
                    xtype: 'selectfield',
                    autoSelect: !1,
                    useClearIcon: !0,
                    name: 'aggregator',
                    bind: {
                        store: '{sAggregators}',
                        value: '{form.aggregator}'
                    }
                }]
            })
        } else {
            b.push({
                xtype: 'fieldset',
                items: [{
                    label: a.sortText,
                    labelAlign: 'top',
                    xtype: 'selectfield',
                    autoSelect: !1,
                    useClearIcon: !0,
                    name: 'sort',
                    bind: {
                        store: '{sSorters}',
                        value: '{form.direction}'
                    }
                }, {
                    label: a.filterText,
                    labelAlign: 'top',
                    xtype: 'selectfield',
                    autoSelect: !1,
                    useClearIcon: !0,
                    name: 'filter',
                    bind: {
                        store: '{sFilters}',
                        value: '{form.filter.type}'
                    },
                    listeners: {
                        change: 'onChangeFilterType'
                    }
                }]
            }, {
                defaults: {
                    labelAlign: 'top'
                },
                bind: {
                    hidden: '{!filterCommon}',
                    title: '{filterLabel ? labelFilterText : valueFilterText}'
                },
                items: [{
                    xtype: 'selectfield',
                    autoSelect: !1,
                    placeholder: a.dimensionText,
                    name: 'dimensionId',
                    bind: {
                        store: '{sDimensions}',
                        value: '{form.filter.dimensionId}',
                        hidden: '{!filterValue}',
                        required: '{filterValue}',
                        requiredMessage: '{filterValue ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'selectfield',
                    autoSelect: !1,
                    placeholder: a.operatorText,
                    name: 'operator',
                    bind: {
                        store: '{sOperators}',
                        value: '{form.filter.operator}',
                        required: '{filterCommon}',
                        requiredMessage: '{filterCommon ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'textfield',
                    placeholder: a.valueText,
                    name: 'value',
                    bind: {
                        value: '{form.filter.value}',
                        hidden: '{filterOperatorBetween}',
                        required: '{filterOperatorValue}',
                        requiredMessage: '{filterOperatorValue ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'textfield',
                    placeholder: a.fromText,
                    name: 'from',
                    bind: {
                        value: '{form.filter.from}',
                        hidden: '{filterOperatorValue}',
                        required: '{filterOperatorBetween}',
                        requiredMessage: '{filterOperatorBetween ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'textfield',
                    placeholder: a.toText,
                    name: 'to',
                    bind: {
                        value: '{form.filter.to}',
                        hidden: '{filterOperatorValue}',
                        required: '{filterOperatorBetween}',
                        requiredMessage: '{filterOperatorBetween ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'togglefield',
                    label: a.caseSensitiveText,
                    name: 'caseSensitive',
                    bind: '{form.filter.caseSensitive}'
                }]
            }, {
                xtype: 'fieldset',
                title: a.top10FilterText,
                defaults: {
                    labelAlign: 'top'
                },
                bind: {
                    hidden: '{!filterTop10}'
                },
                items: [{
                    xtype: 'selectfield',
                    autoSelect: !1,
                    placeholder: a.orderText,
                    name: 'topOrder',
                    bind: {
                        store: '{sTopOrder}',
                        value: '{form.filter.topOrder}',
                        required: '{filterTop10}',
                        requiredMessage: '{filterTop10 ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'textfield',
                    placeholder: a.valueText,
                    name: 'topValue',
                    bind: {
                        value: '{form.filter.value}',
                        required: '{filterTop10}',
                        requiredMessage: '{filterTop10 ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'selectfield',
                    autoSelect: !1,
                    placeholder: a.typeText,
                    name: 'topType',
                    bind: {
                        store: '{sTopType}',
                        value: '{form.filter.topType}',
                        required: '{filterTop10}',
                        requiredMessage: '{filterTop10 ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'selectfield',
                    autoSelect: !1,
                    placeholder: a.dimensionText,
                    name: 'topDimensionId',
                    bind: {
                        store: '{sDimensions}',
                        value: '{form.filter.dimensionId}',
                        required: '{filterTop10}',
                        requiredMessage: '{filterTop10 ? requiredMessage : null}'
                    }
                }, {
                    xtype: 'togglefield',
                    label: a.sortResultsText,
                    name: 'topSort',
                    bind: '{form.filter.topSort}'
                }]
            })
        }
        a.add(b)
    }
}, 0, ["pivotconfigform"], ["widget", "component", "container", "panel", "fieldpanel", "formpanel", "pivotconfigform"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "panel": !0,
    "fieldpanel": !0,
    "formpanel": !0,
    "pivotconfigform": !0
}, ["widget.pivotconfigform"], 0, [Ext.pivot.plugin.configurator, 'Form'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.SettingsController', Ext.app.ViewController, {
    init: function(a) {
        var b = this.getViewModel();
        b.getStore('sLayout').loadData([
            [a.outlineLayoutText, 'outline'],
            [a.compactLayoutText, 'compact'],
            [a.tabularLayoutText, 'tabular']
        ]);
        b.getStore('sPositions').loadData([
            [a.firstPositionText, 'first'],
            [a.hidePositionText, 'none'],
            [a.lastPositionText, 'last']
        ]);
        b.getStore('sYesNo').loadData([
            [a.yesText, !0],
            [a.noText, !1]
        ])
    },
    applySettings: function() {
        var d = this.getViewModel(),
            c = this.getView(),
            a = Ext.clone(d.get('form')),
            b;
        for (b in a) {
            if (a[b] && a[b].isModel) {
                a[b] = a[b].get('value')
            }
        }
        if (c.fireEvent('beforeapplypivotsettings', c, a) !== !1) {
            c.fireEvent('applypivotsettings', c, a);
            this.cancelSettings()
        }
    },
    cancelSettings: function() {
        var a = this.getView();
        a.setMatrixProperties(null);
        a.fireEvent('close', a)
    },
    onMatrixPropertiesChanged: function(b, a) {
        this.getViewModel().set('form', a)
    }
}, 0, 0, 0, 0, ["controller.pivotsettings"], 0, [Ext.pivot.plugin.configurator, 'SettingsController'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.Settings', Ext.form.Panel, {
    controller: 'pivotsettings',
    viewModel: {
        stores: {
            sLayout: {
                type: 'pivotselect'
            },
            sPositions: {
                type: 'pivotselect'
            },
            sYesNo: {
                type: 'pivotselect'
            }
        }
    },
    eventedConfig: {
        matrixProperties: null
    },
    listeners: {
        matrixpropertieschange: 'onMatrixPropertiesChanged'
    },
    defaults: {
        xtype: 'fieldset',
        defaults: {
            labelAlign: 'top'
        }
    },
    showAnimation: {
        type: 'slideIn',
        duration: 250,
        easing: 'ease-out',
        direction: 'left'
    },
    hideAnimation: {
        type: 'slideOut',
        duration: 250,
        easing: 'ease-in',
        direction: 'right'
    },
    titleText: 'Settings',
    okText: 'Ok',
    cancelText: 'Cancel',
    layoutText: 'Layout',
    outlineLayoutText: 'Outline',
    compactLayoutText: 'Compact',
    tabularLayoutText: 'Tabular',
    firstPositionText: 'First',
    hidePositionText: 'Hide',
    lastPositionText: 'Last',
    rowSubTotalPositionText: 'Row subtotal position',
    columnSubTotalPositionText: 'Column subtotal position',
    rowTotalPositionText: 'Row total position',
    columnTotalPositionText: 'Column total position',
    showZeroAsBlankText: 'Show zero as blank',
    yesText: 'Yes',
    noText: 'No',
    updateMatrixProperties: function(c) {
        var a = this,
            b;
        a.removeAll(!0, !0);
        if (!c) {
            return
        }
        b = [{
            xtype: 'titlebar',
            docked: 'top',
            titleAlign: 'left',
            title: a.titleText,
            items: [{
                text: a.cancelText,
                align: 'right',
                ui: 'alt',
                handler: 'cancelSettings'
            }, {
                text: a.okText,
                align: 'right',
                ui: 'alt',
                handler: 'applySettings',
                margin: '0 0 0 5'
            }]
        }, {
            label: a.layoutText,
            xtype: 'selectfield',
            autoSelect: !1,
            useClearIcon: !0,
            name: 'viewLayoutType',
            bind: {
                store: '{sLayout}',
                value: '{form.viewLayoutType}'
            }
        }, {
            label: a.rowSubTotalPositionText,
            xtype: 'selectfield',
            autoSelect: !1,
            useClearIcon: !0,
            name: 'rowSubTotalsPosition',
            bind: {
                store: '{sPositions}',
                value: '{form.rowSubTotalsPosition}'
            }
        }, {
            label: a.columnSubTotalPositionText,
            xtype: 'selectfield',
            autoSelect: !1,
            useClearIcon: !0,
            name: 'colSubTotalsPosition',
            bind: {
                store: '{sPositions}',
                value: '{form.colSubTotalsPosition}'
            }
        }, {
            label: a.rowTotalPositionText,
            xtype: 'selectfield',
            autoSelect: !1,
            useClearIcon: !0,
            name: 'rowGrandTotalsPosition',
            bind: {
                store: '{sPositions}',
                value: '{form.rowGrandTotalsPosition}'
            }
        }, {
            label: a.columnTotalPositionText,
            xtype: 'selectfield',
            autoSelect: !1,
            useClearIcon: !0,
            name: 'colGrandTotalsPosition',
            bind: {
                store: '{sPositions}',
                value: '{form.colGrandTotalsPosition}'
            }
        }, {
            label: a.showZeroAsBlankText,
            xtype: 'selectfield',
            autoSelect: !1,
            useClearIcon: !0,
            name: 'showZeroAsBlank',
            bind: {
                store: '{sYesNo}',
                value: '{form.showZeroAsBlank}'
            }
        }];
        a.add(b)
    }
}, 0, ["pivotsettings"], ["widget", "component", "container", "panel", "fieldpanel", "formpanel", "pivotsettings"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "panel": !0,
    "fieldpanel": !0,
    "formpanel": !0,
    "pivotsettings": !0
}, ["widget.pivotsettings"], 0, [Ext.pivot.plugin.configurator, 'Settings'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.configurator.Panel', Ext.Panel, {
    controller: 'pivotconfigpanel',
    isPivotConfigPanel: !0,
    cls: 'x-pivot-grid-com.landau.core.config-panel',
    showAnimation: {
        type: 'slideIn',
        duration: 250,
        easing: 'ease-out',
        direction: 'left'
    },
    hideAnimation: {
        type: 'slideOut',
        duration: 250,
        easing: 'ease-in',
        direction: 'right'
    },
    panelTitle: 'Configuration',
    panelAllFieldsText: 'Drop Unused Fields Here',
    panelAllFieldsTitle: 'All fields',
    panelTopFieldsText: 'Drop Column Fields Here',
    panelTopFieldsTitle: 'Column labels',
    panelLeftFieldsText: 'Drop Row Fields Here',
    panelLeftFieldsTitle: 'Row labels',
    panelAggFieldsText: 'Drop Agg Fields Here',
    panelAggFieldsTitle: 'Values',
    cancelText: 'Cancel',
    okText: 'Done',
    eventedConfig: {
        pivot: null,
        fields: null
    },
    listeners: {
        pivotchange: 'onPivotChanged',
        fieldschange: 'onFieldsChanged'
    },
    layout: 'card',
    initialize: function() {
        this.setup();
        return Ext.Panel.prototype.initialize.call(this)
    },
    dragDropField: function(a, b, e, h) {
        var f = this,
            m = f.getPivot(),
            d = e.get('field'),
            i = a.getFieldType(),
            g = b.getFieldType(),
            j = f.getController(),
            l = j.getTopAxisContainer(),
            k = j.getLeftAxisContainer(),
            c;
        if (m.fireEvent('beforemoveconfigfield', this, {
                fromContainer: a,
                toContainer: b,
                field: d
            }) !== !1) {
            if (a != b) {
                if (g === 'all') {
                    a.removeField(e)
                } else if (g === 'aggregate') {
                    b.addField(d, h);
                    if (i !== 'all') {
                        a.removeField(e)
                    }
                } else {
                    c = f.findFieldInContainer(d, b);
                    if (c) {
                        return
                    }
                    if (g === 'leftAxis') {
                        c = f.findFieldInContainer(d, l);
                        a = c ? l : a
                    } else {
                        c = f.findFieldInContainer(d, k);
                        a = c ? k : a
                    }
                    if (c) {
                        a.removeField(c);
                        b.addField(d)
                    } else {
                        if (i === 'aggregate') {
                            a.removeField(e)
                        }
                        b.addField(d, h)
                    }
                }
            } else {
                b.moveField(e, h)
            }
        }
    },
    isAllowed: function(b, a, f) {
        var d = !0,
            g = f.get('field'),
            e = b && b.getFieldType(),
            c = a && a.getFieldType();
        if (e === 'aggregate' && (c === 'leftAxis' || c === 'topAxis')) {
            d = !this.findFieldInContainer(g, a)
        }
        return d
    },
    findFieldInContainer: function(f, d) {
        var c = d.getStore(),
            e = c.getCount(),
            a, b;
        for (a = 0; a < e; a++) {
            b = c.getAt(a);
            if (b.get('field').getDataIndex() == f.getDataIndex()) {
                return b
            }
        }
    },
    setup: function() {
        var a = this,
            b = {
                configchange: 'onConfigChanged',
                toolsbtnpressed: 'showCard',
                removefield: 'onRemoveField'
            };
        a.add([{
            itemId: 'main',
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'titlebar',
                docked: 'top',
                title: a.panelTitle,
                titleAlign: 'left',
                items: [{
                    xtype: 'tool',
                    type: 'gear',
                    align: 'right',
                    handler: 'showSettings'
                }, {
                    text: a.cancelText,
                    align: 'right',
                    ui: 'alt',
                    handler: 'cancelConfiguration'
                }, {
                    text: a.okText,
                    align: 'right',
                    ui: 'alt',
                    handler: 'applyConfiguration',
                    margin: '0 0 0 5'
                }]
            }, {
                reference: 'fieldsCt',
                xtype: 'pivotconfigcontainer',
                title: a.panelAllFieldsTitle,
                emptyText: a.panelAllFieldsText,
                fieldType: 'all',
                listeners: b
            }, {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                    xtype: 'pivotconfigcontainer',
                    flex: 1
                },
                items: [{
                    reference: 'fieldsAggCt',
                    title: a.panelAggFieldsTitle,
                    emptyText: a.panelAggFieldsText,
                    fieldType: 'aggregate',
                    listeners: b
                }, {
                    reference: 'fieldsLeftCt',
                    title: a.panelLeftFieldsTitle,
                    emptyText: a.panelLeftFieldsText,
                    fieldType: 'leftAxis',
                    listeners: b
                }, {
                    reference: 'fieldsTopCt',
                    title: a.panelTopFieldsTitle,
                    emptyText: a.panelTopFieldsText,
                    fieldType: 'topAxis',
                    listeners: b
                }]
            }]
        }, {
            itemId: 'field',
            xtype: 'pivotconfigform',
            listeners: {
                close: 'backToMainView',
                beforeapplyconfigfieldsettings: 'onBeforeApplyConfigFieldSettings',
                applyconfigfieldsettings: 'onApplyConfigFieldSettings'
            }
        }, {
            itemId: 'settings',
            xtype: 'pivotsettings',
            listeners: {
                close: 'backToMainView',
                beforeapplypivotsettings: 'onBeforeApplyPivotSettings',
                applypivotsettings: 'onApplyPivotSettings'
            }
        }])
    },
    getAllFieldsContainer: function() {
        return this.lookup('fieldsCt')
    },
    getAllFieldsHeader: function() {
        return this.getAllFieldsContainer().getHeader()
    },
    setAllFieldsContainerVisible: function(a) {
        this.getAllFieldsContainer().setHidden(!a)
    },
    getLeftAxisContainer: function() {
        return this.lookup('fieldsLeftCt')
    },
    getLeftAxisHeader: function() {
        return this.getLeftAxisContainer().getHeader()
    },
    setLeftAxisContainerVisible: function(a) {
        this.getLeftAxisContainer().setHidden(!a)
    },
    getTopAxisContainer: function() {
        return this.lookup('fieldsTopCt')
    },
    getTopAxisHeader: function() {
        return this.getTopAxisContainer().getHeader()
    },
    setTopAxisContainerVisible: function(a) {
        this.getTopAxisContainer().setHidden(!a)
    },
    getAggregateContainer: function() {
        return this.lookup('fieldsAggCt')
    },
    getAggregateHeader: function() {
        return this.getAggregateContainer().getHeader()
    },
    setAggregateContainerVisible: function(a) {
        this.getAggregateContainer().setHidden(!a)
    }
}, 0, ["pivotconfigpanel"], ["widget", "component", "container", "panel", "pivotconfigpanel"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "panel": !0,
    "pivotconfigpanel": !0
}, ["widget.pivotconfigpanel"], 0, [Ext.pivot.plugin.configurator, 'Panel'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.Configurator', Ext.plugin.Abstract, {
    config: {
        fields: [],
        width: 400,
        panel: {
            lazy: !0,
            $value: {
                xtype: 'pivotconfigpanel',
                hidden: !0,
                floated: !0,
                modal: !0,
                hideOnMaskTap: !0,
                right: 0,
                height: '100%'
            }
        },
        pivot: null
    },
    init: function(a) {
        this.setPivot(a)
    },
    destroy: function() {
        this.setConfig({
            pivot: null,
            panel: null
        });
        Ext.plugin.Abstract.prototype.destroy.call(this)
    },
    enable: function() {
        this.disabled = !1;
        this.showConfigurator()
    },
    disable: function() {
        this.disabled = !0;
        this.hideConfigurator()
    },
    applyPanel: function(a, b) {
        if (a) {
            a = a.isInstance ? a : Ext.create(a)
        }
        return a
    },
    updatePanel: function(a, d) {
        var b = this,
            c = this.getPivot();
        Ext.destroy(d, b.panelListeners);
        if (a) {
            b.panelListeners = a.on({
                hiddenchange: 'onPanelHiddenChange',
                close: 'hideConfigurator',
                scope: b,
                destroyable: !0
            });
            a.setConfig({
                pivot: c,
                fields: b.getFields()
            });
            c.add(a)
        }
    },
    onPanelHiddenChange: function(b, a) {
        this.getPivot().fireEvent(a ? 'hideconfigpanel' : 'showconfigpanel', b)
    },
    updatePivot: function(b, c) {
        var a = this;
        Ext.destroy(a.pivotListeners);
        if (c) {
            c.showConfigurator = c.hideConfigurator = null
        }
        if (b) {
            b.showConfigurator = Ext.bind(a.showConfigurator, a);
            b.hideConfigurator = Ext.bind(a.hideConfigurator, a);
            if (b.initialized) {
                a.onPivotInitialized()
            } else {
                b.on({
                    initialize: 'onPivotInitialized',
                    single: !0,
                    scope: a
                })
            }
        }
    },
    getWidth: function() {
        var c = this.getPivot(),
            b = Ext.Viewport,
            a = 100;
        if (c && c.element) {
            a = c.element.getWidth()
        }
        if (b) {
            a = Math.min(a, b.element.getHeight(), b.element.getWidth())
        }
        return Ext.Number.constrain(this._width, 100, a)
    },
    onPivotInitialized: function() {
        var h = this,
            m = h.getPivot(),
            d = h.getFields(),
            g = m.getMatrix(),
            p = m.getHeaderContainer && m.getHeaderContainer(),
            k = [],
            i = {},
            o = !1,
            n, c, f, j, e, b, a, l;
        if (d.length === 0 && g instanceof Ext.pivot.matrix.Local) {
            o = !0;
            n = g.store;
            c = n ? n.model.getFields() : [];
            e = c.length;
            for (b = 0; b < e; b++) {
                j = c[b].getName();
                if (!d.byDataIndex.get(j)) {
                    d.add({
                        header: Ext.String.capitalize(j),
                        dataIndex: j
                    })
                }
            }
        }
        c = Ext.Array.merge(g.leftAxis.dimensions.getRange(), g.topAxis.dimensions.getRange(), g.aggregate.getRange());
        e = c.length;
        for (b = 0; b < e; b++) {
            a = c[b].getConfig();
            delete(a.matrix);
            delete(a.values);
            delete(a.id);
            f = d.byDataIndex.get(a.dataIndex);
            if (!f) {
                d.add(a)
            } else if (o) {
                if (!i[a.dataIndex]) {
                    i[a.dataIndex] = 0
                }
                delete(a.header);
                i[a.dataIndex]++;
                k.push(a)
            }
        }
        e = k.length;
        for (b = 0; b < e; b++) {
            a = k[b];
            if (i[a.dataIndex] === 1) {
                f = d.byDataIndex.get(a.dataIndex);
                if (f) {
                    l = f.getConfig();
                    Ext.merge(l, a);
                    f.setConfig(l)
                }
            }
        }
        h.isReady = !0;
        h.doneSetup = !1;
        if (p) {
            h.pivotListeners = p.renderElement.on({
                longpress: 'showConfigurator',
                scope: this
            })
        }
    },
    hideConfigurator: function() {
        var a = this.getPanel();
        if (a) {
            a.hide()
        }
    },
    showConfigurator: function() {
        var a = this.getPanel();
        if (a) {
            a.setWidth(this.getWidth());
            a.show()
        }
    },
    getFields: function() {
        var a = this._fields;
        if (!a) {
            a = new Ext.util.Collection({
                extraKeys: {
                    byDataIndex: 'dataIndex'
                },
                decoder: function(a) {
                    return (a && a.isField) ? a : new Ext.pivot.plugin.configurator.Field(a || {})
                }
            });
            this.setFields(a)
        }
        return a
    },
    applyFields: function(a, b) {
        if (a == null || (a && a.isCollection)) {
            return a
        }
        if (a) {
            if (!b) {
                b = this.getFields()
            }
            b.splice(0, b.length, a)
        }
        return b
    },
    deprecated: {
        '6.5': {
            configs: {
                panelWrapper: null,
                panelWrap: null
            }
        }
    }
}, 0, 0, 0, 0, ["plugin.pivotconfigurator"], 0, [Ext.pivot.plugin, 'Configurator'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.DrillDown', Ext.plugin.Abstract, {
    config: {
        columns: null,
        width: 500,
        pageSize: 25,
        remoteStore: null,
        panel: {
            lazy: !0,
            $value: {
                xtype: 'panel',
                hidden: !0,
                floated: !0,
                modal: !0,
                hideOnMaskTap: !0,
                right: 0,
                height: '100%',
                showAnimation: {
                    type: 'slideIn',
                    duration: 250,
                    easing: 'ease-out',
                    direction: 'left'
                },
                hideAnimation: {
                    type: 'slideOut',
                    duration: 250,
                    easing: 'ease-in',
                    direction: 'right'
                },
                layout: 'fit',
                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    itemId: 'title',
                    items: {
                        xtype: 'button',
                        text: 'Done',
                        ui: 'action',
                        align: 'right',
                        itemId: 'done'
                    }
                }, {
                    xtype: 'grid',
                    itemId: 'grid',
                    plugins: {
                        gridpagingtoolbar: !0
                    }
                }]
            }
        },
        pivot: null
    },
    titleText: 'Drill down',
    doneText: 'Done',
    init: function(a) {
        this.setPivot(a);
        return Ext.plugin.Abstract.prototype.init.call(this, a)
    },
    destroy: function() {
        this.setConfig({
            pivot: null,
            panel: null
        });
        Ext.plugin.Abstract.prototype.destroy.call(this)
    },
    updatePivot: function(a) {
        Ext.destroy(this.gridListeners);
        if (a) {
            this.gridListeners = a.on({
                pivotitemcelldoubletap: 'showPanel',
                pivotgroupcelldoubletap: 'showPanel',
                pivottotalcelldoubletap: 'showPanel',
                scope: this,
                destroyable: !0
            })
        }
    },
    applyPanel: function(a, b) {
        if (a) {
            a = a.isInstance ? a : Ext.create(a)
        }
        return a
    },
    updatePanel: function(b, c) {
        var a = this,
            d = this.getPivot();
        Ext.destroy(c, a.panelListeners, a.buttonListeners);
        if (b) {
            a.panelListeners = b.on({
                hiddenchange: 'onPanelHiddenChange',
                scope: a,
                destroyable: !0
            });
            b.down('#title').setTitle(a.titleText);
            a.buttonListeners = b.down('#done').on({
                tap: 'hidePanel',
                scope: a,
                destroyable: !0
            });
            a.initializeStoreAndColumns();
            d.add(b)
        }
    },
    initializeStoreAndColumns: function() {
        var b = this,
            i = b.getPanel(),
            g = b.getPivot().getMatrix(),
            f = Ext.Array.from(b.getColumns() || []),
            e = b.getPageSize(),
            c, a, d, k, h, j;
        if (!g || !i || !(j = i.down('#grid'))) {
            return
        }
        if (g.isLocalMatrix) {
            c = g.store.model.getFields();
            a = new Ext.data.Store({
                pageSize: e,
                fields: Ext.clone(c),
                proxy: {
                    type: 'memory',
                    enablePaging: (e > 0),
                    reader: {
                        type: 'array'
                    }
                }
            });
            if (f.length === 0) {
                k = c.length;
                for (d = 0; d < k; d++) {
                    h = c[d];
                    f.push({
                        text: Ext.String.capitalize(h.name),
                        dataIndex: h.name,
                        xtype: 'column'
                    })
                }
            }
        } else {
            a = Ext.getStore(b.getRemoteStore());
            if (a && a.isStore) {
                a.setPageSize(e);
                a.setRemoteFilter(!0)
            }
        }
        j.setConfig({
            store: a,
            columns: f
        })
    },
    onPanelHiddenChange: function(b, a) {
        this.getPivot().fireEvent(a ? 'hidedrilldownpanel' : 'showdrilldownpanel', b)
    },
    getWidth: function() {
        var c = this.getPivot(),
            b = Ext.Viewport,
            a = 100;
        if (c && c.element) {
            a = c.element.getWidth()
        }
        if (b) {
            a = Math.min(a, b.element.getHeight(), b.element.getWidth())
        }
        return Ext.Number.constrain(this._width, 100, a)
    },
    showPanel: function(g) {
        var c = this,
            d = c.getPanel(),
            f = c.getPivot().getMatrix(),
            a, h, b, e;
        if (c.disabled) {
            return
        }
        a = f.results.get(g.leftKey, g.topKey);
        if (!a || !d || !(h = d.down('#grid'))) {
            return
        }
        b = h.getStore();
        if (f.isLocalMatrix) {
            b.getProxy().setData(a.records);
            b.loadPage(1)
        } else {
            e = Ext.Array.merge(c.getFiltersFromParams(a.getLeftAxisItem() ? a.getLeftAxisItem().data : {}), c.getFiltersFromParams(a.getTopAxisItem() ? a.getTopAxisItem().data : {}));
            b.clearFilter(!0);
            if (e.length > 0) {
                b.addFilter(e)
            } else {
                b.load()
            }
        }
        d.setWidth(c.getWidth());
        d.show()
    },
    hidePanel: function() {
        var a = this.getPanel();
        if (a) {
            a.hide()
        }
    },
    getFiltersFromParams: function(c) {
        var d = [],
            a, e, b;
        if (Ext.isObject(c)) {
            b = Ext.Object.getKeys(c);
            e = b.length;
            for (a = 0; a < e; a++) {
                d.push({
                    property: b[a],
                    exactMatch: !0,
                    value: c[b[a]]
                })
            }
        }
        return d
    }
}, 0, 0, 0, 0, ["plugin.pivotdrilldown"], 0, [Ext.pivot.plugin, 'DrillDown'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.rangeeditor.PanelController', Ext.app.ViewController, {
    applySettings: function() {
        var a = this.getViewModel().get('form'),
            c = Ext.bind(this.cancelSettings, this),
            b;
        if (a && a.type && a.type.isModel) {
            a.type = a.type.get('value')
        }
        b = Ext.Factory.pivotupdate(a);
        this.updaterListeners = this.getView().relayEvents(b, ['beforeupdate', 'update']);
        b.update().then(c, c)
    },
    cancelSettings: function() {
        var a = this.getView();
        this.updaterListeners = Ext.destroy(this.updaterListeners);
        a.fireEvent('close', a)
    }
}, 0, 0, 0, 0, ["controller.pivotrangeeditor"], 0, [Ext.pivot.plugin.rangeeditor, 'PanelController'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.rangeeditor.Panel', Ext.form.Panel, {
    controller: 'pivotrangeeditor',
    viewModel: {
        stores: {
            sTypes: {
                type: 'array',
                fields: ['value', 'text'],
                autoDestroy: !0
            }
        }
    },
    showAnimation: {
        type: 'slideIn',
        duration: 250,
        easing: 'ease-out',
        direction: 'left'
    },
    hideAnimation: {
        type: 'slideOut',
        duration: 250,
        easing: 'ease-in',
        direction: 'right'
    },
    titleText: 'Range editor',
    valueText: 'Value',
    fieldText: 'Source field is "{form.dataIndex}"',
    typeText: 'Type',
    okText: 'Ok',
    cancelText: 'Cancel',
    initialize: function() {
        var a = this;
        a.add([{
            xtype: 'titlebar',
            docked: 'top',
            title: a.titleText,
            titleAlign: 'left',
            items: [{
                text: a.cancelText,
                align: 'right',
                ui: 'alt',
                handler: 'cancelSettings'
            }, {
                text: a.okText,
                align: 'right',
                ui: 'alt',
                handler: 'applySettings',
                margin: '0 0 0 5'
            }]
        }, {
            xtype: 'fieldset',
            bind: {
                instructions: a.fieldText
            },
            defaults: {
                labelAlign: 'top'
            },
            items: [{
                label: a.typeText,
                xtype: 'selectfield',
                autoSelect: !1,
                useClearIcon: !0,
                bind: {
                    store: '{sTypes}',
                    value: '{form.type}'
                }
            }, {
                label: a.valueText,
                xtype: 'numberfield',
                bind: '{form.value}'
            }]
        }]);
        return Ext.form.Panel.prototype.initialize.call(this)
    }
}, 0, ["pivotrangeeditor"], ["widget", "component", "container", "panel", "fieldpanel", "formpanel", "pivotrangeeditor"], {
    "widget": !0,
    "component": !0,
    "container": !0,
    "panel": !0,
    "fieldpanel": !0,
    "formpanel": !0,
    "pivotrangeeditor": !0
}, ["widget.pivotrangeeditor"], 0, [Ext.pivot.plugin.rangeeditor, 'Panel'], 0));
(Ext.cmd.derive('Ext.pivot.plugin.RangeEditor', Ext.plugin.Abstract, {
    config: {
        updaters: [
            ['percentage', 'Percentage'],
            ['increment', 'Increment'],
            ['overwrite', 'Overwrite'],
            ['uniform', 'Uniform']
        ],
        defaultUpdater: 'uniform',
        width: 400,
        panel: {
            lazy: !0,
            $value: {
                xtype: 'pivotrangeeditor',
                hidden: !0,
                floated: !0,
                modal: !0,
                hideOnMaskTap: !0,
                right: 0,
                height: '100%'
            }
        },
        pivot: null
    },
    init: function(a) {
        this.setPivot(a);
        return Ext.plugin.Abstract.prototype.init.call(this, a)
    },
    destroy: function() {
        this.setConfig({
            pivot: null,
            panel: null
        });
        Ext.plugin.Abstract.prototype.destroy.call(this)
    },
    applyPanel: function(a, b) {
        if (a) {
            a = a.isInstance ? a : Ext.create(a)
        }
        return a
    },
    updatePanel: function(a, d) {
        var b = this,
            c = this.getPivot();
        Ext.destroy(d, b.panelListeners);
        if (a) {
            b.panelListeners = a.on({
                hiddenchange: 'onPanelHiddenChange',
                close: 'hidePanel',
                scope: b,
                destroyable: !0
            });
            a.getViewModel().getStore('sTypes').loadData(b.getUpdaters());
            c.relayEvents(a, ['beforeupdate', 'update'], 'pivot');
            c.add(a)
        }
    },
    onPanelHiddenChange: function(b, a) {
        this.getPivot().fireEvent(a ? 'hiderangeeditorpanel' : 'showrangeeditorpanel', b)
    },
    updatePivot: function(b, c) {
        var a = this;
        Ext.destroy(a.gridListeners);
        if (b) {
            a.gridListeners = b.on({
                pivotitemcelldoubletap: 'showPanel',
                pivotgroupcelldoubletap: 'showPanel',
                pivottotalcelldoubletap: 'showPanel',
                scope: a,
                destroyable: !0
            })
        }
    },
    getWidth: function() {
        var c = this.getPivot(),
            b = Ext.Viewport,
            a = 100;
        if (c && c.element) {
            a = c.element.getWidth()
        }
        if (b) {
            a = Math.min(a, b.element.getHeight(), b.element.getWidth())
        }
        return Ext.Number.constrain(this._width, 100, a)
    },
    showPanel: function(a, k, j) {
        var b = this,
            i = b.getPivot(),
            c = b.getPanel(),
            g = i.getMatrix(),
            h, d, e, f;
        if (b.disabled) {
            return
        }
        d = g.results.get(a.leftKey, a.topKey);
        if (!d || !c) {
            return
        }
        h = c.getViewModel();
        e = a.column;
        f = e.dimension.getDataIndex();
        h.set('form', {
            leftKey: a.leftKey,
            topKey: a.topKey,
            dataIndex: f,
            value: d.getValue(e.dimension.getId()),
            type: b.getDefaultUpdater(),
            matrix: g
        });
        c.setWidth(b.getWidth());
        c.show()
    },
    hidePanel: function() {
        var a = this.getPanel();
        if (a) {
            a.hide()
        }
    },
    deprecated: {
        '6.5': {
            configs: {
                panelWrapper: null,
                panelWrap: null
            }
        }
    }
}, 0, 0, 0, 0, ["plugin.pivotrangeeditor"], 0, [Ext.pivot.plugin, 'RangeEditor'], 0));

