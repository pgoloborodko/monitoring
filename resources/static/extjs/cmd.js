(function(q) {
    var e, p = ['constructor', 'toString', 'valueOf', 'toLocaleString'],
        n = {},
        m = {},
        k = 0,
        l, j, a, b, o, f, c, d, g, h, i, t = function() {
            var s, t;
            j = Ext.Base;
            a = Ext.ClassManager;
            b = Ext.Class;
            for (s = p.length; s-- > 0;) {
                t = (1 << s);
                m[n[t] = p[s]] = t
            }
            for (s in m) {
                k |= m[s]
            }
            k = ~k;
            Function.prototype.$isFunction = 1;
            i = !!a.addAlias;
            o = b.getPreprocessor('config').fn;
            f = b.getPreprocessor('cachedConfig');
            f = f && f.fn;
            h = b.getPreprocessor('privates');
            h = h && h.fn;
            d = b.getPreprocessor('platformConfig');
            d = d && d.fn;
            c = a.postprocessors.platformConfig;
            c = c && c.fn;
            g = a.postprocessors.deprecated;
            g = g && g.fn;
            e = j.$staticMembers;
            if (!e) {
                e = [];
                for (l in j) {
                    if (j.hasOwnProperty(l)) {
                        e.push(l)
                    }
                }
            }
            q.derive = r;
            return r.apply(this, arguments)
        },
        s = function(f, d, o) {
            var l = o.enumerableMembers,
                j = f.prototype,
                b, h, k, e, m;
            if (!d) {
                return
            }
            if (i) {
                f.addMembers(d)
            } else {
                for (b in d) {
                    e = d[b];
                    if (e && e.$isFunction && !e.$isClass && e !== Ext.emptyFn && e !== Ext.identityFn) {
                        m = j.hasOwnProperty(b) && j[b];
                        if (m) {
                            e.$previous = m
                        }
                        j[b] = h = e;
                        h.$owner = f;
                        h.$name = b
                    } else {
                        j[b] = e
                    }
                }
                for (k = 1; l; k <<= 1) {
                    if (l & k) {
                        l &= ~k;
                        b = n[k];
                        j[b] = h = d[b];
                        h.$owner = f;
                        h.$name = b
                    }
                }
            }
            if (d.platformConfig && c) {
                c.call(a, f.$className, f, d)
            }
            if (d.deprecated && g) {
                g.call(a, f.$className, f, d)
            }
        },
        r = function(u, Q, c, L, E, M, O, B, D, G, N) {
            var g = function ctor() {
                    return this.constructor.apply(this, arguments) || null
                },
                m = g,
                v = {
                    enumerableMembers: L & k,
                    onCreated: N,
                    onBeforeCreated: s,
                    aliases: B
                },
                p = c.alternateClassName || [],
                P = Ext.global,
                F, z, l, K, y, C, H, r, x, w, n, I, t, J = a.alternateToName || a.maps.alternateToName,
                A = a.nameToAlternates || a.maps.nameToAlternates;
            for (l = e.length; l-- > 0;) {
                H = e[l];
                g[H] = j[H]
            }
            if (c.$isFunction) {
                c = c(g)
            }
            v.data = c;
            x = c.statics;
            delete c.statics;
            c.$className = u;
            if ('$className' in c) {
                g.$className = c.$className
            }
            g.extend(Q);
            r = g.prototype;
            if (E) {
                g.xtype = c.xtype = E[0];
                r.xtypes = E
            }
            r.xtypesChain = M;
            r.xtypesMap = O;
            c.alias = B;
            m.triggerExtended(g, c, v);
            if (c.onClassExtended) {
                g.onExtended(c.onClassExtended, g);
                delete c.onClassExtended
            }
            if (c.privates && h) {
                h.call(b, g, c)
            }
            if (x) {
                if (i) {
                    g.addStatics(x)
                } else {
                    for (w in x) {
                        if (x.hasOwnProperty(w)) {
                            t = x[w];
                            if (t && t.$isFunction && !t.$isClass && t !== Ext.emptyFn && t !== Ext.identityFn) {
                                g[w] = I = t;
                                I.$owner = g;
                                I.$name = w
                            }
                            g[w] = t
                        }
                    }
                }
            }
            if (c.inheritableStatics) {
                g.addInheritableStatics(c.inheritableStatics);
                delete c.inheritableStatics
            }
            if (r.onClassExtended) {
                m.onExtended(r.onClassExtended, m);
                delete r.onClassExtended
            }
            if (c.platformConfig && d) {
                d.call(b, g, c);
                delete c.platformConfig
            }
            if (c.config) {
                o.call(b, g, c)
            }
            if (c.cachedConfig && f) {
                f.call(b, g, c);
                delete c.cachedConfig
            }
            v.onBeforeCreated(g, v.data, v);
            for (l = 0, y = D && D.length; l < y; ++l) {
                g.mixin.apply(g, D[l])
            }
            for (l = 0, y = B.length; l < y; l++) {
                F = B[l];
                a.setAlias ? a.setAlias(g, F) : a.addAlias(g, F)
            }
            if (c.singleton) {
                m = new g()
            }
            if (!(p instanceof Array)) {
                p = [p]
            }
            n = a.getName(m);
            for (l = 0, K = p.length; l < K; l++) {
                z = p[l];
                a.classes[z] = m;
                if (i) {
                    a.addAlternate(g, z)
                } else {
                    if (n) {
                        J[z] = n;
                        p = A[n] || (A[n] = []);
                        p.push(z)
                    }
                }
            }
            for (l = 0, y = G.length; l < y; l += 2) {
                C = G[l];
                if (!C) {
                    C = P
                }
                C[G[l + 1]] = m
            }
            a.classes[u] = m;
            if (!i) {
                if (n && n !== u) {
                    J[u] = n;
                    p = A[n] || (A[n] = []);
                    p.push(u)
                }
            }
            delete r.alternateClassName;
            if (v.onCreated) {
                v.onCreated.call(m, m)
            }
            if (u) {
                a.triggerCreated(u)
            }
            return m
        };
    q.derive = t
}(Ext.cmd = {}));