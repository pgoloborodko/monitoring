Ext.define('Landau.events.EventButtonStoreDay', {
    extend: 'Ext.data.Store',
    alias: 'store.event-button-store-day',

    model: 'Landau.events.EventButtonModel',

    proxy: {
        type: 'ajax',
        url: '/events/buttons/day',
        reader: {
            type: 'json'
        }
    }
});