Ext.define('Landau.events.sync.SyncModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id', mapping: 'id'},
        {name: 'deviceFull', mapping: 'device'},
        {name: 'device', mapping: 'deviceFull.name'},
        {name: 'extracted', convert: function (value, record) {
                if(value !== null)  return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }},
        {name: 'inserted', convert: function (value, record) {
            if(value !== null) return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }},
        {name: 'userModel', mapping: function (data, record) {
            console.log(data);

            if(data != null && data.user != null) {
                return Ext.create("Landau.users.UsersModel", data.user); //.get("passport");
            }
        }},
        {name: 'userCompany', convert: function (v, rec) {
            console.log(rec);

            if(rec !== undefined && rec.get('userModel') !== undefined && rec.get('userModel').get("company") !== null) {
                return rec.get('userModel').get("company").name;
            }
        }, depends: [ 'userModel' ]},

        {name: 'userPassport', convert: function (v, rec) {
                if(rec !== undefined && rec.get('userModel') !== undefined) {
                    return rec.get('userModel').get("passport");
                }
            }, depends: [ 'userModel' ]}
    ]
});