Ext.define('Landau.events.sync.SyncStore', {
    extend: 'Ext.data.Store',
    alias: 'store.events-sync-store',

    model: 'Landau.events.sync.SyncModel',

    proxy: {
        type: 'ajax',
        url: '/events/users/sync',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});