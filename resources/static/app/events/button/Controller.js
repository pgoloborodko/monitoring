Ext.define("Landau.events.button.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.events-button-controller",

    filter: function () {
        this.lookup("grid").getStore().load({
            params: {
                day: this.lookup("day").getValue(),
                type: this.lookup("type").getValue(),
                company: this.lookup("company").getValue()
            }
        });
    }
});