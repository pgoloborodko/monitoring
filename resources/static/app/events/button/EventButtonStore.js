Ext.define('Landau.events.button.EventButtonStore', {
    extend: 'Ext.data.Store',
    alias: 'store.event-button-store',

    model: 'Landau.events.button.EventButtonModel',

    proxy: {
        type: 'ajax',
        url: '/events/buttons',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});