Ext.define("Landau.events.button.Dialog", {
    extend: 'Ext.Dialog',
    title: lang("События нажатия кнопок"),

    requires: [
        'Landau.events.button.View'
    ],

    bodyPadding: 20,
    autoSize: true,
    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    height: '90%',
    width: '90%',
    margin: 0,
    padding: 0,

    items: [{
        xtype: 'landau-events-button-view',
        height: '100%',
        width: '100%',
    }]
});