Ext.define("Landau.events.button.View", {
    extend: 'Ext.Panel',
    controller: 'events-button-controller',
    xtype: 'landau-events-button-view',
    title: null,

    requires: [
        'Landau.events.button.Controller',
        'Landau.events.button.EventButtonStore',
        'Landau.companies.CompaniesSelect',
        'Landau.common.Pageable'
    ],

    height: '100%',
    width: '100%',

    items: [{
        xtype : 'toolbar',
        docked: 'top',
        items: [{
            xtype: 'selectfield',
            name: 'type',
            reference: "type",
            flex: 1,
            placeholder: lang('Фильтация по типу события'),
            margin: '0 10 0 0',
            options: [{
                text: lang('-- Без фильтра --'),
                value: ''
            }, {
                text: 'SOS',
                value: 'SOS'
            }, {
                text: lang('Патрулирование'),
                value: 'PATROL'
            }, {
                text: lang('Поиск человека'),
                value: 'ORDER'
            },{
                text: lang('Запрос поиска человека'),
                value: 'CALL'
            }, {
                text: lang('Идентификация'),
                value: 'IDENTY'
            }, {
                text: lang('Стриминг по кнопке'),
                value: 'STREAM'
            }]
        }, {
            xtype: 'datepickerfield',
            maxValue: new Date(),
            value: new Date(),
            margin: '0 10 0 0',
            reference: 'day',
            dateFormat: 'd/m/Y',
            flex: 1,
        }, {
            flex: 1,
            xtype: 'companies-select',
            reference: 'company',
            margin: '0 10 0 0'
        }, {
            xtype: 'button',
            text: lang("Найти"),
            ui: 'action',
            handler: "filter"
        }]
    }, {
        xtype: 'grid',
        reference: "grid",
        height: '100%',
        store:{
            type: 'event-button-store',
            autoLoad: true
        },

        items: [{
            xtype: 'pageable'
        }],

        columns: [{
            text: lang('ФИО'),
            dataIndex: 'fio',
            flex: 1
        }, {
            text: lang('Тип'),
            width: 150,
            dataIndex: 'type',
            renderer: function(value, record, column, cell) {
                cell.row.setCls("");

                switch (value) {
                    case "SOS":
                        cell.row.setCls("red");
                        return "SOS";
                    case "IDENTY":
                        return lang("Идентификация человека");
                    case "ORDER":
                        return lang("Задача поиска человека");
                    case "PATROL":
                        return lang("Задача патрулирования");
                    case "CALL":
                        return lang("Создание задачи поиска человека");
                    case "STREAM":
                        return lang("Стриминг по кнопке");
                }

                return value;
            },
        }, {
            text: lang('Дата'),
            dataIndex: 'created',
            width: 150
        }, {
            text: lang('Компания'),
            dataIndex: 'company',
            width: 150
        }]
    }]
});