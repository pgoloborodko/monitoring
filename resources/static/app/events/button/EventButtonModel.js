Ext.define('Landau.events.button.EventButtonModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id', mapping: 'id'},
        {name: 'type', mapping: 'type'},
        {name: 'text', mapping: 'text'},
        {name: 'fio', mapping: 'fio'},
        {name: 'deviceFull', mapping: 'device'},
        {name: 'device', mapping: 'deviceFull.name'},

        {name: 'created', convert: function (value, record) {
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }},
    ]
});