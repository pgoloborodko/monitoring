Ext.define('Landau.events.EventButtonModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id', mapping: 'id'},
        {name: 'type', mapping: 'type'},
    ]
});