Ext.define('Landau.events.streams.EventStreamsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.event-streams-store',

    model: 'Landau.events.streams.EventStreamsModel',

    proxy: {
        type: 'ajax',
        url: '/events/streams',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});