Ext.define("Landau.events.streams.View", {
    extend: 'Ext.Panel',
    controller: 'events-streams-controller',
    title: null,
    xtype: 'landau-events-streams-view',

    requires: [
        'Landau.events.streams.Controller',
        'Landau.events.streams.EventStreamsStore',
        'Landau.companies.CompaniesSelect',
        'Landau.common.Pageable'
    ],

    height: '100%',
    width: '100%',

    items: [{
        xtype : 'toolbar',
        docked: 'top',
        items: [{
            xtype: 'selectfield',
            name: 'type',
            reference: "type",
            flex: 1,
            placeholder: lang('Фильтация по типу события'),
            margin: '0 10 0 0',
            options: [{
                text: lang('-- Без фильтра --'),
                value: ''
            }, {
                text:  lang('Запуск стрима'),
                value: 'START'
            }, {
                text: lang('Остановка стрима'),
                value: 'STOP'
            }]
        }, {
            xtype: 'datepickerfield',
            maxValue: new Date(),
            value: new Date(),
            margin: '0 10 0 0',
            reference: 'day',
            dateFormat: 'd/m/Y',
            flex: 1,
        }, {
            flex: 1,
            xtype: 'companies-select',
            reference: 'company',
            margin: '0 10 0 0'
        }, {
            xtype: 'button',
            text: lang("Найти"),
            ui: 'action',
            handler: "filter"
        }]
    }, {
        xtype: 'grid',
        reference: "grid",
        height: '100%',
        store:{
            type: 'event-streams-store',
            autoLoad: true
        },

        items: [{
            xtype: 'pageable'
        }],

        columns: [{
            text: lang('ФИО'),
            dataIndex: 'fio',
            flex: 1
        }, {
            text: lang('Тип'),
            width: 150,
            dataIndex: 'type',
            renderer: function(value, record, column, cell) {
                cell.row.setCls("");

                switch (value) {
                    case "START":
                        return lang("Запуск стрима");
                    case "STOP":
                        return lang("Остановка стрима");
                }

                return value;
            },
        }, {
            text: lang('Дата'),
            dataIndex: 'created',
            width: 150
        }, {
            text: lang('Компания'),
            dataIndex: 'company',
            width: 150
        }]
    }]
});