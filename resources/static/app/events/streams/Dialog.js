Ext.define("Landau.events.streams.Dialog", {
    extend: 'Ext.Dialog',
    title: lang("События стримов устройств"),

    requires: [
        'Landau.events.streams.View'
    ],

    bodyPadding: 20,
    autoSize: true,
    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    height: '90%',
    width: '90%',
    margin: 0,
    padding: 0,

    items: [{
        xtype: 'landau-events-streams-view',
        height: '100%',
        width: '100%',
    }]
});