Ext.define('Landau.events.EventButtonStore', {
    extend: 'Ext.data.Store',
    alias: 'store.event-button-store',

    model: 'Landau.events.EventButtonModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/events/buttons',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});