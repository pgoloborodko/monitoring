Ext.define('Landau.events.system.EventSystemStore', {
    extend: 'Ext.data.Store',
    alias: 'store.event-system-store',

    model: 'Landau.events.system.EventSystemModel',

    proxy: {
        type: 'ajax',
        url: '/events/system',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});