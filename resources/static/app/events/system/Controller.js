Ext.define("Landau.events.system.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.events-system-controller",

    filter: function () {
        this.lookup("grid").getStore().load({
            params: {
                day: this.lookup("day").getValue(),
                company: this.lookup("company").getValue()
            }
        });
    }
});