Ext.define("Landau.events.system.Dialog", {
    extend: 'Ext.Dialog',
    title: lang("Системные события устройств"),

    requires: [
        'Landau.events.system.View'
    ],

    bodyPadding: 20,
    autoSize: true,
    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    height: '90%',
    width: '90%',
    margin: 0,
    padding: 0,

    items: [{
        xtype: 'landau-events-system-view',
        height: '100%',
        width: '100%',
    }]
});