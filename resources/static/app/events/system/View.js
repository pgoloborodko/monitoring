Ext.define("Landau.events.system.View", {
    extend: 'Ext.Panel',
    controller: 'events-system-controller',
    title: null,
    xtype: 'landau-events-system-view',

    requires: [
        'Landau.events.system.Controller',
        'Landau.events.system.EventSystemStore',
        'Landau.companies.CompaniesSelect',
        'Landau.common.Pageable'
    ],

    height: '100%',
    width: '100%',

    items: [{
        xtype : 'toolbar',
        docked: 'top',
        items: [{
            xtype: 'datepickerfield',
            maxValue: new Date(),
            value: new Date(),
            margin: '0 10 0 0',
            reference: 'day',
            dateFormat: 'd/m/Y',
            flex: 1,
        }, {
            flex: 1,
            xtype: 'companies-select',
            reference: 'company',
            margin: '0 10 0 0'
        }, {
            xtype: 'button',
            text: lang("Найти"),
            ui: 'action',
            handler: "filter"
        }]
    }, {
        xtype: 'grid',
        reference: "grid",
        height: '100%',
        store:{
            type: 'event-system-store',
            autoLoad: true
        },

        items: [{
            xtype: 'pageable'
        }],

        columns: [{
            text: lang('ФИО'),
            dataIndex: 'fio',
            flex: 1
        }, {
            text: lang('Тип'),
            width: 150,
            dataIndex: 'type'
        }, {
            text: lang('Дата'),
            dataIndex: 'created',
            width: 150
        }, {
            text: lang('Компания'),
            dataIndex: 'company',
            width: 150
        }]
    }]
});