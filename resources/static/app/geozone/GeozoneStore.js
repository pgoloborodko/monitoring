Ext.define('Landau.geozone.GeozoneStore', {
    extend: 'Ext.data.Store',
    alias: 'store.geozone-store',

    model: 'Landau.geozone.GeozoneModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/geofences',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});