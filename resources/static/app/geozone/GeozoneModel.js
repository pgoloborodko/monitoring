Ext.define('Landau.geozone.GeozoneModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',

    fields: [
        {name: 'company', mapping: 'company.name'}
    ]

});