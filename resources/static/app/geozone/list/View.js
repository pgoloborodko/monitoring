Ext.define("Landau.geozone.list.View", {
    extend: 'Ext.Panel',
    controller: 'geozone-controller',

    requires: [
        'Landau.geozone.list.Controller',
        'Landau.geozone.list.ViewGrid',
        'Landau.companies.CompaniesSelect'
    ],

    height: "100%",
    defaultType: 'panel',

    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'geozone-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить геозону'),
        ui: 'action',
        handler: 'addGeozone'
    }, {
        xtype: 'spacer'
    },{
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});