Ext.define("Landau.geozone.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'geozone-grid-view',

    height: '100%',

    requires: [
        'Landau.geozone.GeozoneStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'geozone-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        flex: 1
    }, {
        text: lang('Редактировать геозону'),
        flex: 1,
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: [{
                type: 'maximize',
                handler: 'editGeozone',
                tooltip: lang('Редактировать геозону')
            }]
        }
    }]
});