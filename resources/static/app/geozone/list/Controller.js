Ext.define("Landau.geozone.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.geozone-controller",

    requires: [
        'Landau.geozone.save.View'
    ],

    doFilter: function () {
        var company = this.lookup("company");

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue()
            }
        });
    },

    addGeozone: function() {
        var dialog = Ext.create("Landau.geozone.save.View", {
            parentController: this,
            title: lang('Добавить геозону')
        });

        dialog.show();
    },

    editGeozone: function(grid, info) {
        console.log(this);

        var dialog = Ext.create("Landau.geozone.save.View", {
            idGeozone: info.record.get('id')
        });

        dialog.show();
    },

    reloadGrid: function () {
        this.lookup("grid").getStore().reload();
    }
});