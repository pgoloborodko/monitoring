Ext.define("Landau.geozone.save.Controller", {
    extend: 'Ext.app.ViewController',
    alias: 'controller.geozone-save-controller',

    requires: [
        "Landau.map.MapViewYandex",
        'Landau.geozone.list.ViewGrid',
    ],

    init: function() {
        if (this.getView().getIdGeozone()) {
            var store = Ext.create('Landau.geozone.save.GeozoneAddStore');

            store.load({
                params: {
                    id: this.getView().getIdGeozone()
                },
                callback: function(records, operation, success) {
                    console.log(records[0].data);

                    if (success) {
                        var form = this.lookup("formGeozone");
                        form.setValues(records[0]);
                        this.lookup('map').createGeozone(this.prepareCords(records[0].data.points), false)
                    }
                },
                scope: this
            });
        } else {
            this.lookup('map').createGeozone([], true);
        }
    },

    prepareCords: function(val) {
        var coordArr = [];
        val.map(function(item) {
            coordArr.push([item.x, item.y])
        });
        return [coordArr, []]
    },

    doErase: function () {
        this.lookup('map').clearPoligon();
    },

    saveGeozone: function () {
        var form = this.lookup('formGeozone');
        var that = this;

        if (form.validate()) {
            var reqJson = {
                "id": this.getView().getIdGeozone(),
                "name": this.lookup("geozoneNameInput").getValue(),
                "company": this.lookup("companyNameInput").getValue(),
                "points":this.lookup('map').getPolygon()
            };

            reqJson = JSON.stringify(reqJson);
            //console.log(reqJson);

            Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});

            Ext.Ajax.request({
                url: '/geofences/save',
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                jsonData: reqJson,

                success: function(response, opts) {
                    Ext.Viewport.setMasked(false);
                    //console.log(response);

                    // делаем обновление данных в основной таблице
                    that.getView().parentController.lookup("grid").getStore().reload();

                    that.getView().destroy();
                },

                failure: function(response, opts) {
                    Ext.Viewport.setMasked(false);
                    //console.log(response);
                    var dialog = Ext.create({
                        xtype: 'dialog',
                        title: lang('Внимание!'),

                        maximizable: false,
                        html: lang('Сбой на стороне сервера! Код ошибки:') + ' ' + response.status,

                        buttons: {
                            ok: function () {  // standard button (see below)
                                dialog.destroy();
                            }
                        }
                    });

                    dialog.show();
                }
            });
        }
    }
});