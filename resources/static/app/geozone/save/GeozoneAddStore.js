Ext.define('Landau.geozone.save.GeozoneAddStore', {
    extend: 'Ext.data.Store',
    alias: 'store.geozone-save-store',

    //model: 'Landau.geozone.list.add.GeozoneAddModel',

    proxy: {
        type: 'ajax',
        url: '/geofences/save',
        reader: {
            type: 'json'
        }
    }

});