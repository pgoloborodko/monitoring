Ext.define("Landau.geozone.save.View", {
    extend: 'Ext.Dialog',
    requires: [
        //'Landau.geozone.list.add.ViewGrid',
        'Landau.geozone.save.Controller',
        "Landau.map.MapViewYandex",
        'Landau.companies.CompaniesSelect'
    ],
    config: {
        //geozoneNameInput: '',
        //companyNameInput: ''
        idGeozone: null
    },
/*
    listeners: {
        show: function() {
            console.log('1')
                this.lookup("companyNameInput").setAutoFocus(true);
        },
        beforeshow: function() {
            console.log('2')
            this.lookup("companyNameInput").setAutoFocus(true)
        }
    },
*/
    xtype: 'geozone-save',
    controller: 'geozone-save-controller',
    title: lang('Сохранение геозоны'),

    closable: true,
    maximized: true,
    padding: 0,
    height: '100%',

    items: [{
        header: false,
        //flex: 3,
        xtype: "yandex-map",
        reference: "map",
        width: '100%',
        height: '100%'
    }],

    tbar:[{
        xtype: 'formpanel',
        reference: 'formGeozone',
        autoSize: true,
        layout: 'hbox',
        width: '100%',
        //margin: 0,
        padding: 0,
        bodyStyle:'background-color:transparent;',
        style:'background-color: transparent;',
        defaults: {
            xtype: 'textfield',
            errorTarget: 'side'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        }, {
            xtype: 'textfield',
            name: 'name',
            reference: 'geozoneNameInput',
            placeholder: lang('Название геозоны'),
            required: true
        }, {
            xtype: 'container',
            html: '&nbsp;&nbsp;'
        },{
            xtype: 'companies-select',
            name: 'company',
            required: true,
            reference: 'companyNameInput',
            margin: '0 10'
        }, {
            xtype: 'container',
            html: '&nbsp;&nbsp;'
        }, {
            xtype: 'button',
            iconCls: 'x-fa fa-eraser',
            text: lang('Очистить карту'),
            handler: 'doErase'
        },{
            xtype: 'container',
            flex:3,
            html: '&nbsp;&nbsp;'
        }, {
            xtype: 'button',
            text: lang('Сохранить'),
            reference: 'saveBtn',
            ui: 'action',
            handler: 'saveGeozone'
        }]
    }]
});