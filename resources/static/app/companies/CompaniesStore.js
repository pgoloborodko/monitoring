Ext.define('Landau.companies.CompaniesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.companies-store',

    model: 'Landau.companies.CompaniesModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/companies',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});