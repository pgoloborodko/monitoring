Ext.define('Landau.companies.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'companies-save-view',
    controller: 'companies-save-controller',
    title: lang('Сохранение компании'),

    requires: [
        'Landau.companies.save.Controller',
        'Landau.companies.CompaniesStore'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [
            // {
            //     xtype: 'container',
            //     layout: 'vbox',
            //     margin: '0 10 0 0',
            //     items: [{
            //         xtype: 'containerfield',
            //         label: lang('Брендинг'),
            //         required: true,
            //         defaults: {
            //             flex: 1
            //         },
            //         items: [{
            //             name: 'color',
            //             placeholder: lang('Цвет')
            //         }]
            //     }, {
            //         xtype: 'img',
            //         height: 170,
            //         width: 170,
            //         layout: 'center',
            //         reference: 'logo'
            //     }, {
            //         xtype: 'filefield',
            //         name: 'logoFile',
            //         placeholder: lang('Логотип')
            //     }]
            // },
                {
                xtype: 'container',
                layout: 'vbox',
                width: '100%',
                items: [{
                    xtype: 'containerfield',
                    label: lang('Название компани'),
                    required: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        name: 'name',
                        placeholder: lang('Название'),
                        required: true
                    }]
                }, {
                    xtype: 'filefield',
                    name: 'licenseFile',
                    placeholder: lang('Лицензия'),
                    //required: true
                }, {
                    xtype: 'containerfield',
                    label: lang('Выбор карты'),
                    margin: '0 5 0 0',
                    required: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'selectfield',
                        name: 'map',
                        required: true,
                        placeholder: lang('Выберите тип карты') + '...',
                        options: [{
                            text: lang('Yandex'),
                            value: 'YANDEX'
                        }, {
                            text: lang('Google'),
                            value: 'GOOGLE'
                        }, {
                            text: lang('Metro'),
                            value: 'METRO'
                        }]
                    }]
                }, {
                    xtype: 'containerfield',
                    label: lang('Стриминг'),
                    required: true,
                    layout: 'vbox',
                    // align: 'left',
                    // defaults: {
                    //     flex: 1
                    // },
                    items: [
                        {
                            xtype: 'fieldcontainer',
                            defaultType: 'checkboxfield',
                            items: [
                                {
                                    boxLabel  : lang('Постоянный'),
                                    name      : 'grantStreaming',
                                    required: true,
                                    value: true,
                                    margin: '0 10 0 0'
                                }, {
                                    boxLabel  : lang('Периодический'),
                                    name      : 'periodic',
                                    required: true,
                                    value: true
                                }
                            ]
                        }
                    ],
                    // items: [{
                    //     xtype: 'checkbox',
                    //     name: 'grantStreaming',
                    //     boxLabel: lang('Постоянный'),
                    //     required: true,
                    //     // align: 'left',
                    //     // left: '100px',
                    //     value: true
                    // },{
                    //     xtype: 'checkbox',
                    //     name: 'periodic',
                    //     boxLabel: lang('Периодический'),
                    //     required: true,
                    //     // align: 'left',
                    //     // left: '10px',
                    //     value: true
                    // }]
                }]
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});