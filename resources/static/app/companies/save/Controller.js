Ext.define('Landau.companies.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.companies-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var filefield = this.lookup('photoFile');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/companies/save',
                scope: this,
                success: this.onCancel
                //upload: true
            });
        }
    },

    changeImage: function (that, newValue, oldValue, eOpts) {
        var photo = this.lookup('logo');
        var files = this.lookup('logoFile').getFiles();

        if (files && files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                photo.setSrc(e.target.result);
            };

            reader.readAsDataURL(files[0]);
        }
    }
});