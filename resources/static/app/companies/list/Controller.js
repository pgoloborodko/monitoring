Ext.define("Landau.companies.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.companies-controller",

    save: function () {
        this.openForm("Landau.companies.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.companies.save.View", '/companies/save?id=' + info.record.id);
    },

    doFilter: function () {
        var name = this.lookup("name");

        this.lookup("grid").getStore().reload({
            params: {
                name: name.getValue()
            }
        });
    }
});