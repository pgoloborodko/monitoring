Ext.define("Landau.companies.list.View", {
    extend: 'Ext.Panel',
    controller: 'companies-controller',
    xtype: 'companies-list-view',

    requires: [
        'Landau.companies.list.Controller',
        'Landau.companies.list.ViewGrid'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [{
            height: "100%",
            xtype: 'companies-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новую компанию'),
        ui: 'action',
        handler: 'save'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: 'Название',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});