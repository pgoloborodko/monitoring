Ext.define('Landau.companies.CompaniesModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'name',
        'employees',
        'devices',
        'grantStreaming',
        'logo',
        'color'
    ]
});