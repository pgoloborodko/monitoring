Ext.define('Landau.companies.CompaniesSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'companies-select',
    requires: [
        'Landau.companies.list.View'
    ],

    config: {
        title: lang('Выбор компании'),
        placeholder: lang('Компания...'),
        view: 'companies-list-view',
        displayTpl: '{name}',
    }
});