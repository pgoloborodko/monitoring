Ext.define('Landau.externaldbs.ExternaldbsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.externaldbs-store',

    model: 'Landau.externaldbs.ExternaldbsModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/externals',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});