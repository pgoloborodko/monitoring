Ext.define('Landau.externaldbs.ExternaldbsModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'company', mapping: 'company.name'}
    ]
});