Ext.define("Landau.externaldbs.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.externaldbs-controller",

    save: function () {
        this.openForm("Landau.externaldbs.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.externaldbs.save.View", '/externals/save?id=' + info.record.id);
    },

    doFilter: function () {
        var company = this.lookup("company");

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue()
            }
        });
    }
});