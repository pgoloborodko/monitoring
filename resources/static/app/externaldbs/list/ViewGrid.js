Ext.define("Landau.externaldbs.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'externaldbs-grid-view',

    height: '100%',

    requires: [
        'Landau.externaldbs.ExternaldbsStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'externaldbs-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Адрес'),
        dataIndex: 'address',
        width: 200
    }, {
        text: lang('Порт'),
        dataIndex: 'port',
        width: 200
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 200
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});