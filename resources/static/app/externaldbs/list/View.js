Ext.define("Landau.externaldbs.list.View", {
    extend: 'Ext.Panel',
    controller: 'externaldbs-controller',

    requires: [
        'Landau.externaldbs.list.Controller',
        'Landau.externaldbs.list.ViewGrid',
        'Landau.companies.CompaniesSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'externaldbs-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новую БД'),
        ui: 'action',
        handler: 'save'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'companies-select',
        margin: '0 10 0 0',
        reference: 'company'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});