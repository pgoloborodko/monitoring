Ext.define('Landau.externaldbs.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'externaldbs-save-view',
    controller: 'externaldbs-save-controller',
    title: lang('Сохранение БД'),

    requires: [
        'Landau.externaldbs.save.Controller',
        'Landau.companies.CompaniesSelect',
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Параметры сервера'),
            required: true,
            layout: 'vbox',
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                name: 'name',
                placeholder: lang('Название'),
                required: true
            }, {
                name: 'address',
                placeholder: lang('Адрес'),
                required: true
            },{
                name: 'login',
                placeholder: lang('Логин'),
                required: true
            },{
                name: 'password',
                placeholder: lang('Пароль'),
                required: true
            },{
                name: 'port',
                placeholder: lang('Порт'),
                required: true,
                validators  : /[0-9]+/
            }, {
                xtype: 'companies-select',
                required: true,
                name: 'company'
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});