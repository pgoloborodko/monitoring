Ext.define('Landau.externaldbs.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.externaldbs-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/externals/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});