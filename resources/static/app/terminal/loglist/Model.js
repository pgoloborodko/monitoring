Ext.define('Landau.terminal.loglist.Model', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'filename', mapping: 'filename'},
        {name: 'size', convert: function (value, record) {
            return (value/1024).toFixed(0) + " Кбайт"
        }},
    ]
});