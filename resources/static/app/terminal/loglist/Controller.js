Ext.define("Landau.terminal.loglist.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.terminal-loglist-controller",

    init: function() {
        this.lookup("grid").getStore().load({
            params: {
                terminal: this.getView().getTerminal().id
            }
        });
    },

    onCancel: function () {
        this.getView().destroy();
    },

    exportDocument: function (menuitem) {
        var pivotgrid = this.lookup('grid');

        pivotgrid.saveDocumentAs({
            type: 'xlsx',
            title: 'Лог файлы терминала ' + this.getViewModel().get("terminal").name + "(" + this.getViewModel().get("terminal").serial + ")",
            fileName: 'logs_terminal_' + this.getViewModel().get("terminal").name + '-' + this.getViewModel().get("terminal").serial + '.xlsx'
        });
    },

    onLogFile: function(grid, info) {
        this.openModelForm("Landau.terminal.logfile.View", {
            terminal: that.getViewModel().get("terminal"),
            filename: info.record.data.filename
        });
    }
});