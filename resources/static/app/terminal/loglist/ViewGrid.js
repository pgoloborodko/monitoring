Ext.define("Landau.terminal.loglist.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'terminal-loglist-grid-view',

    height: '100%',
    width: '100%',

    requires: [
        'Landau.terminal.loglist.Store',
        'Ext.grid.plugin.Exporter',
    ],

    plugins: [{
        type: 'gridexporter'
    }],

    store: {
        type: 'terminal-loglist-store',
        autoLoad: false
    },
/*
    items: [{
        xtype: 'pageable'
    }],
*/
    columns: [{
        text: lang('Название'),
        dataIndex: 'filename',
        flex: 2,
        exportStyle: {
            autoFitWidth: true,
            width: 400
        }
    }, {
        text: lang('Размер'),
        dataIndex: 'size',
        flex: 1,
        exportStyle: {
            autoFitWidth: true,
            width: 200
        }
    }, {
        text: lang('Действия'),
        width: 'auto',

        cell: {
            tools: [{
                type: 'maximize',
                handler: 'onLogFile',
                tooltip: lang('Просмотр')
            }]
        }
    }]
});