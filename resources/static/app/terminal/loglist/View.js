Ext.define('Landau.terminal.loglist.View', {
    extend: 'Ext.Dialog',
    xtype: 'terminal-loglist-view',
    controller: 'terminal-loglist-controller',
    title: lang('Список лог-файлов'),

    height: "90%",
    width: "50%",

    config: {
        terminal: null
    },

    requires: [
        'Landau.terminal.loglist.Controller',
        'Landau.terminal.loglist.ViewGrid'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'terminal-loglist-grid-view',
        reference: 'grid',
    }],

    buttons: [{
        iconCls: 'x-fa fa-file-excel-o',
        text: lang('XLS'),
        handler: 'exportDocument'
    }, {
        text: lang('Закрыть'),
        handler: 'onCancel'
    }]
});