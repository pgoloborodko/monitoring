Ext.define('Landau.terminal.loglist.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.terminal-loglist-store',

    model: 'Landau.terminal.loglist.Model',

    proxy: {
        type: 'ajax',
        url: '/terminals/log/list', //поменять пятерку
        reader: {
            type: 'json'
            //rootProperty: 'data.content', //.content
            //totalProperty: 'data.totalElements',
            //metaProperty: 'data.size'
        }
    }
});