Ext.define("Landau.terminal.logfile.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.terminal-logfile-controller",

    init: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        Ext.Ajax.request({
            url: '/terminals/log/file',
            params : {terminal: this.getViewModel().get("terminal").id, file: this.getViewModel().get('filename')},
            scope: this,
            failure: 'failureCallback',
            success: 'successCallback'
        });

        //console.log(this.getViewModel())
        //https://localhost/terminals/request/log/file?terminal=5&file=terminal.log.2019-06-21.0.gz
    },

    onCancel: function () {
        this.getView().destroy();
    },

    successCallback: function (response) {
        //console.log(response.responseText)
        this.getView().setItems([
            Ext.create("Ext.panel.Panel", {
                html: '<pre>' + response.responseText + '</pre>'
            })
        ])
    }
});