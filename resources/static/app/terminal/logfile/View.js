Ext.define('Landau.terminal.logfile.View', {
    extend: 'Ext.Dialog',
    xtype: 'terminal-logfile-view',
    controller: 'terminal-logfile-controller',
    title: lang('Просмотр лог-файла'),

    closable: true,
    maximized: true,
    padding: 0,
    height: '100%',

    requires: [
        'Landau.terminal.logfile.Controller'
    ],

    bodyPadding: 20,
    autoSize: true,

    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    scrollable: true,

    onCancel: function () {
        this.destroy();
    },
/*
    items: [{
        xtype: 'panel',
        centered: true,
        html: '<b>Подождите! Идет загрузка...</b>'
    }]*/
});