Ext.define("Landau.terminal.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.terminal-controller",
    selected: null,

    init: function () {
        this.lookup("grid").getStore().on('load', function(context, records) {
            for (var i = 0; i < records.length; i++) {
                if(this.selected !== null && this.selected.id === records[i].data.id) {
                    records[i].set("checked", true);
                }
            }
        }, this);
    },

    menuCollapseListener: function (panel) {
        panel.header.items.getAt(1).hide();
        panel.header.items.getAt(2).hide();
        panel.header.setTitle(lang("Список терминалов"));

        this.lookup("map").resize();
    },

    menuExpandListener: function (panel) {
        panel.header.items.getAt(1).show();

        if(this.lookup("city").getValue() != null) {
            panel.header.items.getAt(2).show();
        }

        panel.header.setTitle(lang("Поиск"));
    },

    rowclick: function (scope, row) {
        if(row.cell.dataIndex !== "checked" &&(row.record.get("vision") === undefined || row.record.get("vision") === false)) {
            row.record.set("checked", true);

            this.checkchange(scope, row, true, row.record);
        }
    },

    checkchange: function(that, rowIndex, checked, record, e, eOpts) {
        let data = record.store.getData();

        if(checked && !record.get('online')) {
            //record.set("checked", false);
            Ext.Msg.alert(lang("Внимание!"), lang("Терминал не в сети!"));
        }

        for(let i=0; i < data.length; i++) {
            if(data.getAt(i).get("id") !== record.get("id")) {
                data.getAt(i).set("checked", false);
            }
        }

        if(checked) {
            this.lookup("container").setItems([{
                xtype: "terminal-remote-view",
                terminal: record.data
            }]);

            this.selected = record.data;
        }
        else {
            this.lookup("container").setItems({
                xtype: 'panel',
                html: lang("Выберите онлайн-терминал"),
                flex: 1,
                width: '100%',
                height: '100%',
                layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                },
            });

            this.selected = null;
        }
    },

    changeCity: function (scope) {
        this.lookup("grid").getStore().load({
            params: {
                city: scope.getValue()
            }
        });

        if(scope.getValue() === null) {
            this.lookup("city").setWidth(235);
            this.lookup("district").setHidden(true);
        }
        else {
            this.lookup("city").setWidth(100);
            this.lookup("district").setHidden(false);
        }

        this.lookup("district").getStore().proxy.extraParams = {city: scope.getValue()};
    },

    changeDistrict: function (scope) {
        this.lookup("grid").getStore().load({
            params: {
                city: this.lookup("city").getValue(),
                district: scope.getValue()
            }
        });
    }
});