Ext.define("Landau.terminal.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'terminal-view',
    controller: 'terminal-controller',
    flex: 1,

    requires: [
        'Landau.terminal.Controller',
        'Landau.terminal.remote.View',
        'Ext.panel.Collapser',
        'Ext.data.TreeStore',
        "Landau.districts.DistrictsSelect",
        "Landau.cities.CitiesSelect",
        "Landau.terminals.Store",
        'Landau.common.Pageable',
    ],

    height: "100%",
    defaultType: 'panel',

    layout: {
        type: 'hbox'
    },

    items: [{
        reference: 'menu',
        border: true,
        listeners: {
            'collapse': 'menuCollapseListener',
            'expand': 'menuExpandListener'
        },
        title: lang('Теминалы'),
        flex: false,
        width: 290,
        collapsible: {
            collapsed: false,
            direction: 'left'
        },
        items: [{
            height: '100%',
            xtype: 'grid',
            scope: this,
            reference: "grid",
            hideHeaders: true,
            store: {
                type: 'terminals-store',
                autoLoad: true
            },
            listeners: {
                childtap: 'rowclick'
            },
            columns: [{
                xtype:'checkcolumn',
                width: 30,
                dataIndex: 'checked',
                sortable: false,
                menuDisabled: true,
                scope: this,
                listeners: {
                    checkchange: 'checkchange'
                }
            }, {
                dataIndex: 'state',
                width: 30,
                cell: {
                    encodeHtml: false
                }
            }, {
                text: lang('Имя'),
                dataIndex: 'name',
                flex: 1
            }],
            items: [{
                xtype: 'pageable',
                small: true
            }]
        }]
    }, {
        header: false,
        flex: 1,
        xtype: "container",
        reference: "container",
        width: '100%',
        height: '100%',
        items: [{
            xtype: 'panel',
            html: lang("Выберите онлайн-терминал"),
            flex: 1,
            width: '100%',
            height: '100%',
            layout: {
                type: 'hbox',
                align: 'center',
                pack: 'center'
            },
        }]
    }]
});