Ext.define("Landau.terminal.remote.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.terminal-remote-controller",

    init: function () {
        this.getView().add({
            xtype: 'container',
            height: '100%',
            width: '100%',
            padding: 0,
            cls: 'iframe-terminal',
            html: "<iframe src='/terminals/request/" + this.getView().getTerminal().id + "/app/index.html' style='width: 100%; height: 100%; border:0'></iframe>"
        });
    },

    onLogs: function(grid, info) {
        this.openFormData("Landau.terminal.loglist.View", {
            terminal: this.getView().getTerminal()
        });
    }
});