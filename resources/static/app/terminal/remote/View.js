Ext.define("Landau.terminal.remote.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'terminal-remote-view',
    controller: 'terminal-remote-controller',
    flex: 1,

    config: {
        terminal: null
    },

    requires: [
        'Landau.terminal.remote.Controller',
    ],

    height: "100%",
    defaultType: 'panel',

    layout: {
        type: 'hbox'
    },

    tbar: [{
        text: lang("лог-файлы"),
        handler: 'onLogs'
    }],

    items: []
});