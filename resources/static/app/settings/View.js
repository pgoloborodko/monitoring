Ext.define("Landau.settings.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'settings-view',
    controller: 'settings-controller',
    flex: 1,

    requires: [
        'Landau.settings.Controller',
        'Ext.panel.Collapser',
        'Ext.layout.HBox',
        'Ext.data.TreeStore',
        'Landau.settings.SystemStore'
    ],

    store: {
        type: 'store.system-store',
        autoLoad: true
    },
    height: "100%",
    defaultType: 'panel',

    layout: {
        type: 'hbox'
    },

    items: [{
        border: true,
        header: false,
        width: 250,
        scrollable: true,
        items: [{
            xtype: 'list',
            itemTpl: '{title}',
            reference: 'list',
            listeners: {
                itemtap: 'loadSettingPage'
            },
            data: [
                { title: lang('Пользователи системы'), view: 'Landau.users.list.View' },
                { title: lang('Профили людей'), view: 'Landau.profiles.list.View' },
                { title: lang('Транспорт'), view: 'Landau.transport.list.View' },
                //{ title: 'Автоколонны', view: 'Landau.convoy.list.View' },
                { title: lang('Устройства'), view: 'Landau.devices.list.View' },
                { title: lang('Камеры'), view: 'Landau.cameras.list.View' },
                //{ title: lang('Приложения'), view: 'Landau.apps.list.View' },
                { title: lang('Города'), view: 'Landau.cities.list.View' },
                { title: lang('Районы'), view: 'Landau.districts.list.View' },
                { title: lang('Маршруты'), view: 'Landau.routes.list.View' },
                //{ title: lang('Фильтры'), view: 'Landau.filter.list.View' },
                { title: lang('Терминалы'), view: 'Landau.terminals.list.View' },
                { title: lang('Биометрические комплексы'), view: 'Landau.biometric.list.View' },
                { title: lang('Компании'), view: 'Landau.companies.list.View' },
                { title: lang('Внешние БД'), view: 'Landau.externaldbs.list.View' },
                { title: lang('Токены API'), view: 'Landau.tokens.list.View' },
                { title: lang('Стрим-серверы'), view: 'Landau.streamservers.list.View' },
                /*{ title: lang('Сессии'), view: 'Landau.sessions.list.View' },*/
                { title: lang('Геозоны'), view: 'Landau.geozone.list.View' },
                { title: lang('Данные лицензии'), view: 'Landau.license.ViewGrid' },
                { title: lang('API документация'), view: 'Landau.api.View' }
            ]
        }]
    }, {
        header: false,
        flex: 1,
        reference: 'container'
    }]
});