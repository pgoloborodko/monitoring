Ext.define('Landau.settings.SystemStore', {
    extend: 'Ext.data.Store',
    alias: 'store.system-store',

    model: 'Landau.settings.SystemModel',

    proxy: {
        type: 'ajax',
        url: '/system',
        reader: {
            type: 'json'
        }
    }
});