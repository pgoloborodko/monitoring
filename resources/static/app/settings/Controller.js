Ext.define("Landau.settings.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.settings-controller",

    loadSettingPage: function (list, index, element, event) {
        if(event.get("view") !== null) {
            Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Загрузка...', indicator:true});
            this.lookup('list').select(index);

            setTimeout(function () {
                var newView = Ext.create(event.get("view"));
                this.lookup('container').setItems(newView);
                Ext.Viewport.setMasked(false);
            }.bind(this), 10);
        }
    },


    init: function () {
        this.lookup('list').select(0);

        Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Загрузка...', indicator:true});
        var newView = Ext.create("Landau.users.list.View");
        this.lookup('container').setItems(newView);

        Ext.Viewport.setMasked(false);
    }
});