Ext.define('Landau.common.Messages', {
    singleton: true,
    alternateClassName: 'Messages',

    requiredScripts: [
        '/js/stomp/sock.js',
        '/js/stomp/stomp.js'
    ],

    mixins: ['Ext.mixin.Mashup'],
    binds: {},
    listens: [],
    listensConnect: [],
    listensDisconnect: [],
    isConnected: false,

    constructor: function(config) {
        this._connect();
    },

    onConnect: function(cb) {
        this.listensConnect.push(cb);
    },

    onDisconnect: function(cb) {
        this.listensDisconnect.push(cb);
    },

    _connect: function() {
        this.client = Stomp.over(new SockJS('/stomp'));

        this.client.connect({}, () => {
            this.isConnected = true;
            console.log("ws connected");

            for(let i=0; i < this.listensConnect.length; i++) {
                this.listensConnect[i]();
            }

            for(let i=0; i < this.listens.length; i++) {
                this._subscribe(this.listens[i]);
            }
        }, () => {
            this.isConnected = false;
            console.log("ws disconnected");

            for(let i=0; i < this.listensDisconnect.length; i++) {
                this.listensDisconnect[i]();
            }

            setTimeout(() => {
                this._connect();
            }, 5000);
        });
    },

    bind: function(options) {
        console.log("bind", options);

        this.listens.push(options);

        if(this.isConnected) {
            this._subscribe(options);
        }
    },

    _subscribe: function(options) {
        this.binds[options.method] = this.client.subscribe(options.listen, function (message) {
            console.log("message", message, options);
            options.method(JSON.parse(message.body));
        });

        if(options.callback !== undefined) {
            options.callback(this.binds[options.method]);
        }
    },

    unbind: function(options) {
        console.log("unbind", options);

        for(let i=0; i < this.listens.length; i++) {
            if(this.listens[i].method === options.method) {
                this.listens.splice(index, 1);
            }
        }

        this.client.unsubscribe(this.binds[options.method]);
        delete this.binds[options.method];
    },

    unsubscribe: function(subscribe) {
        this.client.unsubscribe(subscribe);
    }
});