Ext.define('Landau.common.Sorter', {
    override: 'Ext.util.Sorter',

    serialize: function() {
        return this.getProperty() + "," + this.getDirection();
    }
});