let defaultXtypes = [
    'component', 'panel', 'dialog', 'toolbar', 'button', 'container',
    'gridrow', 'viewport', 'messagebox', 'loadmask', 'tooltip', 'spacer',
    'pageable', 'headercontainer', 'gridcolumn', 'title', 'titlebar', 'loadmask',
    'tool', 'textfield', 'containerfield', 'passwordfield', 'containerfield',
    'selectfield', 'containerfield', 'paneltitle', 'panelheader', 'formpanel',
    'hiddenfield', 'segmentedbutton', 'datepickerfield', 'tab', 'checkcolumn',
    'grid', 'list', 'simplelistitem', 'tabbar', 'rowheader', 'video', 'boundlist',
    'textareafield', 'checkbox', 'filebutton', 'filefield', 'img', 'combodialog', "main-view",
    "datepanel", "tool"
];

let timer;
let menus = [];
let copyText = null;

function showContextMenu(xy, field) {
    menus.push(field);

    clearTimeout(timer);
    timer = setTimeout(function () {
        menus.sort(function(a, b) {
            if (a.text < b.text) {
                return 1;
            }
            if (a.text > b.text) {
                return -1;
            }

            return 0;
        });

        if(copyText != null) {
            menus.unshift({
                xtype: 'menuseparator'
            });

            menus.unshift({
                text: lang("Скопировать") + " " + copyText,
                iconCls: 'x-fa fa-files-o',
                handler: copyToClipboard.bind(this, copyText)
            });
        }


        let menu = new Ext.menu.Menu({
            items: menus,
        });

        menu.showAt(xy);
        menus = [];
        copyText = null;
    }, 100);
}

function copyToClipboard(string) {
    const el = document.createElement('textarea');
    el.value = string;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

Ext.define('Landau.common.Component', {
    override: 'Ext.Component',

    initialize: function () {
        let xtype = this.xtype;


        if(defaultXtypes.indexOf(xtype) < 0 && this.excludeTest === undefined) {
            this.addCls("marker-" + this.xtype + (this.name != null ? '-' + this.name : ''));

            this.el.set({
                marker: this.xtype + (this.name != null ? '-' + this.name : '')
            });
        }

        this.el.on("contextmenu", function (e) {
            if(copyText == null && e.target.innerText) {
                copyText = e.target.innerText;
            }

            if(e.target.tagName.toLowerCase() === "input" || e.target.tagName.toLowerCase() === "textarea") {
                e.stopPropagation();
                return;
            }

            console.log(this.xtype);

            if(defaultXtypes.indexOf(this.xtype) < 0 && this.excludeTest === undefined) {
                e.preventDefault();

                let way = this.$className.split(".");
                way.shift();
                way.pop();

                showContextMenu(e.getXY(), {
                    text: "Тест: " + "/app/" + way.join("/") + "/" + this.xtype + ".html",
                    href: "/app/" + way.join("/") + "/" + this.xtype + ".html",
                    iconCls: 'x-fa fa-check',
                    target: '_blank'
                });

                if(this.el.getAttribute("marker") != null) {
                    showContextMenu(e.getXY(), {
                        text: "Маркер: " + this.el.getAttribute("marker"),
                        iconCls: 'x-fa fa-thumb-tack',
                        handler: copyToClipboard.bind(this, this.el.getAttribute("marker")),
                        listeners: {
                            focusenter: function () {
                                this.el.addCls("test-view");
                            }.bind(this),
                            focusleave: function () {
                                this.el.removeCls("test-view");
                            }.bind(this)
                        }
                    });
                }
            }
        }, this);
    }
});