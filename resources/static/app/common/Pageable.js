Ext.define('Landau.common.Pageable', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.pageable',
    config: {
        small: false
    },

    mixins: ['Ext.mixin.Mashup'],

    controller: {
        store: null,
        pages: 1,
        init:function() {
            if(this.getView().getSmall()) {
                this.lookup("pageString").setHidden(true);

                this.lookup("count").setHidden(true);
                this.lookup("totalElements").setHidden(true);
                this.lookup("countString").setHidden(true);
                this.lookup("countPageString").setHidden(true);
            }

            var interval = setInterval(function () {
                if(this.getView().getParent().getStore() != null) {
                    this.store = this.getView().getParent().getStore();

                    this.update();
                    clearInterval(interval)
                }
            }.bind(this), 100);
        },

        update: function() {
            this.pages = Math.ceil(this.store.getTotalCount() / this.store.getProxy().getReader().metaData);

            this.store.on("load", function (data) {
                console.log(this.store);

                this.pages = Math.ceil(this.store.getTotalCount() / this.store.getProxy().getReader().metaData);

                if(this.pages < this.store.currentPage) {
                    this.lookup("page").setHtml(this.pages);
                }
                else {
                    this.lookup("page").setHtml(this.store.currentPage);
                }

                this.lookup("totalElements").setHtml(this.store.getTotalCount());
                this.lookup("totalPages").setHtml(this.pages);
                this.lookup("count").setHtml(this.store.getCount());
            }, this);

            this.lookup("totalElements").setHtml(this.store.getTotalCount());
            this.lookup("totalPages").setHtml(this.pages);
            this.lookup("count").setHtml(this.store.getCount());
            this.lookup("page").setHtml(this.store.currentPage);
        },

        first: function() {
            this.store.load({
                page: 1,
                params: this.store.lastOptions.params
            });
        },
        last: function() {
            this.store.load({
                page: this.pages,
                params: this.store.lastOptions.params
            });
        },
        prev: function() {
            if(this.store.currentPage > 1) {
                this.store.load({
                    page: this.store.currentPage - 1,
                    params: this.store.lastOptions.params
                });
            }
        },
        next: function() {
            if(this.store.currentPage < this.pages) {
                this.store.load({
                    page: this.store.currentPage + 1,
                    params: this.store.lastOptions.params
                });
            }
        }
    },

    layout: 'vbox',
    docked: 'bottom',

    bbar:[{
        xtype: 'panel',
        html: lang('Страница'),
        margin: '0 5 0 0',
        ui: 'disabled',
        reference: 'pageString'
    },{
        xtype: 'panel',
        html: '1',
        margin: '0 5 0 0',
        reference: 'page'
    },{
        xtype: 'panel',
        html: lang('из'),
        margin: '0 5 0 0',
    }, {
        xtype: 'panel',
        margin: '0 5 0 0',
        html: '~',
        reference: 'totalPages'
    }, {
        xtype: 'button',
        text: "<<",
        width: 50,
        handler: 'first',
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        }
    }, {
        xtype: 'button',
        text: "<",
        width: 50,
        handler: 'prev'
    }, {
        xtype: 'button',
        text: ">",
        width: 50,
        handler: 'next'
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'button',
        text: ">>",
        width: 50,
        handler: 'last'
    }, {
        xtype: 'spacer',
    },{
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'panel',
        html: lang('Записей на странице'),
        margin: '0 5 0 0',
        reference: 'countPageString'
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'panel',
        margin: '0 5 0 0',
        html: '~',
        reference: 'count'
    },{
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'panel',
        html: lang('из'),
        margin: '0 5 0 0',
        reference: 'countString'
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'panel',
        margin: '0 5 0 0',
        html: '~',
        reference: 'totalElements'
    }]
});