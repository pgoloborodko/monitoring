Ext.define('Landau.common.TabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.landau-tabpanel',
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
    },
    setActiveItem: function(card) {
        return;

        var me = this,
            previous;

        if(card) {
            previous = me.getTabBar().getActiveTab();

            me.getTabBar().setActiveTab(card);
            //me.tabBar.setActiveTab(card.tab);
            // we basically remove the part that changes between cards,
            // but keep firing 'tabchange' event
            me.fireEvent('tabchange', me, card, previous);

            return false;
        }
    }
});