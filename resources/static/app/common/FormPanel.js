Ext.define("Landau.common.FormPanel", {
    override: "Ext.form.Panel",

    submit: function (options, e) {
        options = options || {};

        var me = this,
            formValues = me.getValues(me.getStandardSubmit() || !options.submitDisabled),
            form = me.element.dom || {};

        if (this.getEnableSubmissionForm()) {
            form = this.createSubmissionForm(form, formValues);
        }

        options = Ext.apply({
            url : me.getUrl() || form.action,
            submit: false,
            form: form,
            method : me.getMethod() || form.method || 'post',
            autoAbort : false,
            params : null,
            waitMsg : null,
            headers : null,
            success : null,
            failure : null
        }, options || {});

        this.setMasked(true);

        return me.fireAction('beforesubmit', [me, formValues, options, e], 'doBeforeSubmit', null, null, 'after');
    },

    privates: {
        /**
         * @private
         */
        applyExtraParams: function (options) {
            var form = options.form,
                params = Ext.merge(this.getBaseParams() || {}, options.params),
                name, input;

            for (name in params) {
                input = document.createElement('input');
                input.setAttribute('type', 'text');
                input.setAttribute('name', name);
                input.setAttribute('value', params[name]);
                form.appendChild(input);
            }
        },

        /**
         * @private
         */
        beforeAjaxSubmit: function (form, options, successFn, failureFn) {
            var me = this,
                url = options.url || me.getUrl(),
                request = Ext.merge({}, {
                    url: url,
                    timeout: me.getTimeout() * 1000,
                    form: form,
                    scope: me
                }, options);

            delete request.success;
            delete request.failure;

            request.params = Ext.merge(me.getBaseParams() || {}, options.params);
            request.header = Ext.apply({
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }, options.headers || {});

            request.callback = function (callbackOptions, success, response) {
                var responseText = response.responseText,
                    responseXML = response.responseXML,
                    statusResult = Ext.data.request.Ajax.parseStatus(response.status, response);

                if (form.$fileswap) {
                    var original, placeholder;

                    Ext.each(form.$fileswap, function (item) {
                        original = item.original;
                        placeholder = item.placeholder;

                        placeholder.parentNode.insertBefore(original, placeholder.nextSibling);
                        placeholder.parentNode.removeChild(placeholder);
                    });

                    form.$fileswap = null;
                    delete form.$fileswap;
                }

                me.setMasked(false);

                if (response.success) {
                    console.log(successFn);
                    successFn(response, responseText);
                }
                else {
                    failureFn(response, responseText);
                }
            };

            if (Ext.feature.has.XHR2 && request.xhr2) {
                delete request.form;

                var formData = request.data = new FormData(form);

                if (request.params) {
                    Ext.iterate(request.params, function (name, value) {
                        if (Ext.isArray(value)) {
                            Ext.each(value, function (v) {
                                formData.append(name, v);
                            });
                        } else {
                            formData.append(name, value);
                        }
                    });

                    delete request.params;
                }
            }

            return Ext.Ajax.request(request);
        },

        /**
         * @private
         */
        beforeDirectSubmit: function (api, form, options, successFn, failureFn) {
            var me = this,
                submit;

            me.applyExtraParams(options);

            api = Ext.direct.Manager.resolveApi(api, me);
            me.setApi(api);

            submit = api.submit;

            if (!submit) {
                Ext.raise("Cannot find Ext Direct API method for submit action");
            }

            return submit(form, function (data, response, success) {
                me.setMasked(false);

                if (success) {
                    if (data.success) {
                        successFn(response, data);
                    } else {
                        failureFn(response, data);
                    }
                } else {
                    failureFn(response, data);
                }
            }, me);
        },

        /**
         * @private
         */
        beforeStandardSubmit: function (form, options) {
            if (options.url && Ext.isEmpty(form.action)) {
                form.action = options.url;
            }

            // Spinner fields must have their components enabled *before* submitting or else the value
            // will not be posted.
            var fields = this.query('spinnerfield'),
                ln = fields.length,
                body = document.body,
                i, field;

            for (i = 0; i < ln; i++) {
                field = fields[i];

                if (!field.getDisabled()) {
                    field.setDisabled(false);
                }
            }

            body.appendChild(form);

            form.method = (options.method || form.method).toLowerCase();
            form.submit();

            // If the form is targeting a different DOM such as an iframe, then this
            // resource will remain in the current body since it does not reload upon
            // submit so we need to explicitly remove it.
            body.removeChild(form);
        },

        /**
         * @private
         */
        createSubmissionForm: function (form, values) {
            var fields = this.getFields(),
                name, input, field, fileTrigger, inputDom;

            if (form.nodeType === 1) {
                form = form.cloneNode(false);

                for (name in values) {
                    input = document.createElement("input");
                    input.setAttribute("type", "text");
                    input.setAttribute("name", name);
                    input.setAttribute("value", values[name]);
                    form.appendChild(input);
                }
            }

            for (name in fields) {
                if (fields.hasOwnProperty(name)) {
                    field = fields[name];
                    if(field.isFile) {
                        // The <input type="file"> of a FileField is its "file" trigger button.
                        fileTrigger = field.getTriggers().file;
                        inputDom = fileTrigger && fileTrigger.getComponent().buttonElement.dom;

                        if (inputDom) {
                            if(!form.$fileswap) form.$fileswap = [];
                            input = inputDom.cloneNode(true);
                            inputDom.parentNode.insertBefore(input, inputDom.nextSibling);
                            form.appendChild(inputDom);
                            form.$fileswap.push({original: inputDom, placeholder: input});
                        }
                    } else if(field.isPassword) {
                        if(field.getInputType() !== "password") {
                            field.setRevealed(false);
                        }
                    }
                }
            }

            return form;
        },

        /**
         * @private
         */
        doBeforeSubmit: function (me, formValues, options) {
            var form = options.form || {},
                multipartDetected = false,
                ret;

            if (this.getMultipartDetection() === true) {
                this.getFields(false).forEach(function (field) {
                    if (field.isFile === true) {
                        multipartDetected = true;
                        return false;
                    }
                });

                if (multipartDetected) {
                    form.setAttribute("enctype", "multipart/form-data");
                }
            }

            if (options.enctype) {
                form.setAttribute("enctype", options.enctype);
            }

            if (me.getStandardSubmit()) {
                ret = me.beforeStandardSubmit(form, options);
            }
            else {
                var api = me.getApi(),
                    scope = options.scope || me,
                    failureFn = function (response, responseText) {
                        if (Ext.isFunction(options.failure)) {
                            options.failure.call(scope, me, response, responseText);
                        }

                        me.fireEvent('exception', me, response);
                    },
                    successFn = function (response, responseText) {
                        if (Ext.isFunction(options.success)) {
                            options.success.call(options.scope || me, me, response, responseText);
                        }

                        me.fireEvent('submit', me, response);
                    },
                    waitMsg = options.waitMsg;

                if (options.waitMsg) {
                    if (typeof waitMsg === 'string') {
                        waitMsg = {
                            xtype: 'loadmask',
                            message: waitMsg
                        };
                    }

                    me.setMasked(waitMsg);
                }

                if (api) {
                    ret = me.beforeDirectSubmit(api, form, options, successFn, failureFn);
                }
                else {
                    ret = me.beforeAjaxSubmit(form, options, successFn, failureFn);
                }
            }

            return ret;
        }
    },

    load: function (options) {
        options = options || {};

        var me = this,
            api = me.getApi(),
            url = options.url || me.getUrl(),
            waitMsg = options.waitMsg,
            successFn = function (response, data) {
                console.log(data);

                me.setValues(data);

                if (Ext.isFunction(options.success)) {
                    options.success.call(options.scope || me, me, response, data);
                }

                me.fireEvent('load', me, response);
            },
            failureFn = function (response, data) {
                if (Ext.isFunction(options.failure)) {
                    options.failure.call(options.scope, me, response, data);
                }

                me.fireEvent('exception', me, response);
            },
            load, args;

        me.setMasked({
            xtype   : 'loadmask',
            message : lang("Загрузка..."),
        });

        if (api) {
            api = Ext.direct.Manager.resolveApi(api, me);
            me.setApi(api);

            load = api.load;

            if (!load) {
                Ext.raise("Cannot find Ext Direct API method for load action");
            }

            args = load.$directCfg.method.getArgs({
                params: me.getParams(options.params),
                paramOrder: me.getParamOrder(),
                paramsAsHash: me.getParamsAsHash(),
                scope: me,
                callback: function (data, response, success) {
                    me.setMasked(false);


                    if (success) {
                        successFn(response, data);
                    } else {
                        failureFn(response, data);
                    }
                }
            });

            load.apply(window, args);
        }
        else if (url) {
            return Ext.Ajax.request({
                url: url,
                timeout: (options.timeout || me.getTimeout()) * 1000,
                method: options.method || 'GET',
                autoAbort: options.autoAbort,
                headers: Ext.apply(
                    {
                        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    options.headers || {}
                ),

                success: function(response, opts) {
                    me.setMasked(false);
                    successFn(response, Ext.decode(response.responseText));
                },

                failure: function(response, opts) {
                    me.setMasked(false);
                    failureFn(response, response.responseText);
                }
            });
        }
    }
});