Ext.define('Landau.common.MediaRecorder', {
    alternateClassName: 'MRecorder',
    singleton: true,
    open: false,
    recorder: null,
    chunks: [],
    callback: function() {},

    constructor: function(config) {
        if(navigator.mediaDevices == null) {
            return;
        }

        navigator.mediaDevices.getUserMedia({audio: true}).then(function (stream) {
            this.recorder = new MediaRecorder(stream);
            this.open = true;

            this.recorder.ondataavailable = function(e) {
                this.chunks.push(e.data);
            }.bind(this);

            this.recorder.onstop = function(e) {
                let blob = new Blob(this.chunks, { 'type' : 'audio/ogg; codecs=opus' });

                let reader = new FileReader();
                reader.onload = function() {
                    let dataUrl = reader.result;
                    let base64 = dataUrl.split(',')[1];
                    this.callback(base64);
                }.bind(this);
                reader.readAsDataURL(blob);
            }.bind(this);
        }.bind(this));
    },

    isOpen: function () {
        return that.open;
    },

    start: function () {
        if(this.recorder == null) {
            return false;
        }

        this.chunks = [];
        this.recorder.start();

        return this.open;
    },

    stop: function (callback) {
        if(this.recorder == null) {
            return false;
        }

        this.callback = callback;
        this.recorder.stop();
    }
});