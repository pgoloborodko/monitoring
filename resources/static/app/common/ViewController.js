let hashObj = Ext.Object.fromQueryString(location.hash.substr(1));

for(let hashKey in hashObj) {
    if(Array.isArray(hashObj[hashKey])) {
        for(let i=0; i < hashObj[hashKey].length; i++) {
            hashObj[hashKey][i] = !isNaN(hashObj[hashKey][i]) ? Number.parseInt(hashObj[hashKey][i]) : hashObj[hashKey][i];
        }
    }
    else {
        hashObj[hashKey] =  !isNaN(hashObj[hashKey]) ? Number.parseInt(hashObj[hashKey]) : hashObj[hashKey];
    }
}

Ext.define("Landau.common.ViewController", {
    override: "Ext.app.ViewController",

    updateHashValue: function(key, value) {
        value = !isNaN(value) ? Number.parseInt(value) : value;

        if(this.hasHashValue(key)) {
            hashObj[key] = value;
        }
        else {
            this.addHashValue(key, value);
        }

        location.hash = Ext.Object.toQueryString(hashObj);
    },

    addHashValue: function(key, value) {
        value = !isNaN(value) ? Number.parseInt(value) : value;

        if(this.hasHashValue(key)) {
            let currentValues = this.getHashValue(key);

            if(Array.isArray(currentValues)) {
                if(currentValues.indexOf(value) < 0) {
                    currentValues.push(value);
                }
            }
            else {
                if(currentValues !== value) {
                    hashObj[key] = [currentValues, value];
                }
            }
        }
        else {
            hashObj[key] = value;
        }

        location.hash = Ext.Object.toQueryString(hashObj);
    },

    getHashValue: function(key) {
        return hashObj[key] === undefined ? null : hashObj[key];
    },

    hasHashValue: function(key) {
        return hashObj[key] !== undefined;
    },

    removeHashValue: function(key, value) {
        value = !isNaN(value) ? Number.parseInt(value) : value;

        if(value === undefined) {
            delete hashObj[key];
        }
        else {
            if(this.hasHashValue(key)) {
                let currentValue = this.getHashValue();
                if(Array.isArray(currentValue)) {
                    currentValue.splice(currentValue.indexOf(value), 1);
                }
                else {
                    delete hashObj[key];
                }
            }
        }

        location.hash = Ext.Object.toQueryString(hashObj);
    },

    openForm: function (xtype, url, callback) {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        let dialog = Ext.create(xtype, {
            listeners: {
                destroy: function () {
                    if(this.lookup("grid") != null) {
                        setTimeout(function () {
                            this.lookup("grid").getStore().reload();
                        }.bind(this), 10);
                    }
                }.bind(this)
            }
        }).show();

        if(url !== undefined) {
            callback = callback || function() {};
            let form = dialog.lookup("form");

            form.load({
                url: url,
                success: function(ctx, response, data) {
                    setTimeout(function () {
                        callback(dialog, data);
                        Ext.Viewport.setMasked(false);
                    }, 10);
                },
                failure: function(ctx, error) {
                    console.error("error load form data", arguments);

                    setTimeout(function () {
                        Ext.Msg.alert(lang("Внимание!"), lang("Ошибка загрузки формы!"));
                        Ext.Viewport.setMasked(false);
                    }, 10);
                }
            });
        }
        else if(callback !== undefined) {
            setTimeout(function () {
                Ext.Msg.alert(lang("Внимание!"), lang("Адрес формы не найден!"));
                Ext.Viewport.setMasked(false);
                console.error("form url not found", arguments);
            }, 10);
        }
        else {
            setTimeout(function () {
                Ext.Viewport.setMasked(false);
            }, 10);
        }
    },

    openFormData: function (xtype, data) {
        data.listeners = {
            destroy: function () {
                if(this.lookup("grid") != null) {
                    this.lookup("grid").getStore().reload();
                }
            }.bind(this)
        };

        return Ext.create(xtype, data).show();
    },

    openModelForm: function (xtype, viewModelData) {
        let dialog = Ext.create(xtype, {
            listeners: {
                destroy: function (reload) {
                    if(this.lookup("grid") != null) {
                        this.lookup("grid").getStore().reload();
                    }
                }.bind(this)
            },
            viewModel: {
                data: viewModelData
            }
        }).show();
    }
});