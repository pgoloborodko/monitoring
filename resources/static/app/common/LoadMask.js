Ext.define("Landau.common.LoadMask", {
    override: "Ext.LoadMask",
    config: {
        message: lang('Загрузка...'),
        zIndex: 100000000
    }
});