/**
 * Контроллер нативного sldp плеера
 */
Ext.define("Landau.video.sldp.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.video-sldp-controller",
    sldp: null,
    playing: false,
    eventForStartStream: null,

    /**
     * Уничтожить плеер
     */
    destroy: function () {
        console.debug("destroy");

        if (this.sldp !== null) {
            this.sldp.destroy();
            this.sldp = null;
        }

        if(this.eventForStartStream != null) {
            this.eventForStartStream.unsubscribe();
        }

        clearTimeout(this.timeout);
        clearTimeout(this.keepalive);

        this.keepAliveFunction = null;
    },

    init: function() {
        this.event = Messages.bind({
            listen: "/user/users/streams/start/" + this.getView().getUser().id,
            method: () => {
                console.log("start stream event for " + this.getView().getUser().id);
                if(!this.playing) return;
                this.initPlayer();
            },
            callback: (event) => {
                this.eventForStartStream = event;
            }
        });

        if(this.getView().getAutoplay()) {
            this.play();
        }
    },

    getVideo: function() {
        return this.lookup("sldp").bodyElement.dom.id;
    },

    /**
     * инициализация плеера
     * @returns {null}
     */
    initPlayer: function() {
        if(!window.SLDP) return null;

        Ext.Ajax.request({
            scope: this,
            url:'/streams/user/keepalive?user=' + this.getView().getUser().id,
            success: function(response) {
                if (this.sldp !== null) {
                    this.sldp.destroy();
                }

                this.lookup("notification").setHidden(false).setHtml("Запуск стриминга");
                this.lookup("sldp").setHidden(true);

                this.sldp = SLDP.init({
                    container: this.getVideo(),
                    stream_url: response.responseText,
                    height: "parent",
                    width: "parent",
                    //log_level:"debug",
                    controls: true,
                    callbacks: {
                        onConnectionStarted: this.onConnectionStarted.bind(this),
                        onConnectionEstablished: this.onConnectionEstablished.bind(this),
                        onPlay: this.onPlay.bind(this),
                        onPause: this.onPause.bind(this),
                        onVolumeSet: this.onVolumeSet.bind(this),
                        onConnectionClosed: this.onConnectionClosed.bind(this),
                        onChangeRendition: this.onChangeRendition.bind(this),
                        onChangeRenditionComplete: this.onChangeRenditionComplete.bind(this),
                        onPlayStarted: this.onPlayStarted.bind(this),
                        onPlayFailed: this.onPlayFailed.bind(this),
                        onError: this.onError.bind(this)
                    },
                    reconnects: 1000,
                    autoplay: false,
                    latency_tolerance: 2000,
                    buffering: 1000
                }, this);

                this.sldp.setVolume(this.getView().getVolume());
                this.sldp.play();

                this.getView().fireEvent('onInit', this.getView());
            },
            failure: function (error) {
                console.error(arguments);
                //Ext.Msg.alert("Внимание!", "Ошибка запроса запуска стриминга");

                this.lookup("notification").setHidden(false).setHtml("Ошибка запуска стриминга");
                this.lookup("sldp").setHidden(true);

                this.getView().fireEvent('onError', this.getView(), "user offline");
            }
        });
    },

    /**
     * Запуск проигрывания, возобновляет keep alive
     */
    play: function () {
        if(!this.playing) {
            this.playing = true;
            console.debug("play");

            clearTimeout(this.timeout);

            this.initPlayer();
            this.keepAliveFunction();

            this.lookup("notification").setHidden(false).setHtml("Запуск стрима");
            this.lookup("sldp").setHidden(true);

        }
    },

    /**
     * стоп проигрывания, прекращает keep alive
     */
    stop: function () {
        console.debug("stop");
        this.playing = false;

        clearTimeout(this.timeout);
        clearTimeout(this.keepalive);

        if (this.sldp !== null) {
            this.sldp.stop();
            this.sldp.destroy();
            this.sldp = null;
        }

        this.lookup("notification").setHidden(false).setHtml("Стрим остановлен");
        this.lookup("sldp").setHidden(true);

    },

    fullscreen: function() {
        if(this.sldp !== null) {
            this.sldp.fullscreen();
            this.fireEvent('onExpand', this);
        }
    },

    /**
     * keep alive функция
     */
    keepAliveFunction: function () {
        console.debug("keepAliveFunction");

        clearTimeout(this.keepalive);

        if(this.playing) {
            this.keepalive = setTimeout(function() {
                if(this.keepAliveFunction != null) {
                    Ext.Ajax.request({
                        scope: this,
                        url:'/streams/user/keepalive?user=' + this.getView().getUser().id,
                        success: function(data) {
                            this.keepAliveFunction();
                        },
                        failure: function () {
                            this.keepAliveFunction();
                            console.error(arguments);
                        }
                    });
                }
            }.bind(this), 9000);
        }
    },

    /**
     * События sldp библиотеки
     */
    onConnectionStarted: function() {
        console.debug("sldp.onConnectionStarted");
    },

    onPlay: function() {
        console.debug("sldp.onPlay");
        this.getView().fireEvent('onPlay', this.getView());
    },

    onPause: function() {
        console.debug("sldp.onPause");
        this.getView().fireEvent('onPause', this.getView());
    },

    onError: function(error) {
        console.debug("sldp.onError");

        this.onPause();
        this.getView().fireEvent('onError', this.getView());

        this.lookup("notification").setHidden(false).setHtml("Ошибка подключения к стриму");
        this.lookup("sldp").setHidden(true);

        clearTimeout(this.timeout);

        if(this.playing) {
            this.timeout = setTimeout(function () {
                this.sldp.play();
            }.bind(this), 5000);
        }
    },

    onConnectionEstablished: function() {
        console.debug("sldp.onConnectionEstablished");

        if(this.playing) {
            this.lookup("notification").setHidden(true);
            this.lookup("sldp").setHidden(false);

            let message = Ext.fly(this.getVideo()).query(".sldp_message_wrp")[0];

            if (message !== undefined) {
                message && Ext.fly(this.getVideo()).query(".sldp_message_wrp")[0].parentNode.removeChild(message);
            }
        }
    },

    onConnectionClosed: function() {
        console.debug("sldp.onConnectionClosed");
        //console.debug("sldp.onConnectionClosed");
    },

    onVolumeSet: function() {
        console.debug("sldp.onVolumeSet");
        //console.debug(arguments);
    },

    onLatencyAdjustSeek: function () {
        console.debug("sldp.onLatencyAdjustSeek");
        //console.debug(arguments);
    },

    onChangeRendition: function() {
        console.debug("sldp.onChangeRendition");
        //console.debug(arguments);
    },

    onChangeRenditionComplete: function() {
        console.debug("sldp.onChangeRenditionComplete");
        //console.debug(arguments);
    },

    onPlayStarted: function() {
        console.debug("sldp.onPlayStarted");
        this.onPlay();
    },

    onPlayFailed: function() {
        console.debug("sldp.onPlayFailed");
        //console.debug(arguments);

        this.lookup("notification").setHidden(false).setHtml("Ошибка воспроизведения стрима");
        this.lookup("sldp").setHidden(true);
    },

    startCall: function () {
        console.debug("startCall");

        if(!MRecorder.start()) {
            Ext.Msg.alert(lang("Внимание!"), lang("Функция голосовой связи недоступна!"));
        }
    },

    stopCall: function () {
        console.debug("stopCall");

        MRecorder.stop(function (result) {
            Ext.Ajax.request({
                scope: this,
                method: 'post',
                url: '/chat/messages/save?destination=' + this.getView().getUser().id,
                params: {
                    type: "AUDIO",
                    body64: result
                },
                success: function (response) {

                }
            });
        }.bind(this));
    },
});