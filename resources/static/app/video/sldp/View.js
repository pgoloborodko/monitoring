/**
 * Нативный sldp плеер, см static/tests/sldp/index.html
 */
Ext.define("Landau.video.sldp.View", {
    extend: "Ext.Panel",
    xtype: "sldp",
    layout: 'vbox',
    controller: "video-sldp-controller",

    requires: [
        "Landau.video.sldp.Controller",
        'Landau.tasks.list.View',
        'Landau.common.MediaRecorder'
    ],

    requiredScripts: [
        '/js/sldp/sldp-v2.11.3.js'
    ],

    mixins: ['Ext.mixin.Mashup'],

    config: {
        autoplay: false,
        user: null,
        volume: 0,
        time: new Date()
    },

    listeners: {
        resize: function (panel, w, h) {
            //panel.setHeight(w / 1.6);
        }
    },

    height: 'auto',
    maxHeight: '100%',
    minHeight: 200,

    items: [{
        xtype: 'container',
        layout: 'box',
        reference: 'sldp',
        maxHeight: '100%',
        hidden: true
    }, {
        style: 'background: black; height: 100%; color: white; text-align: center;',
        xtype: 'container',
        layout: 'box',
        html: 'Стриминг остановлен',
        reference: 'notification'
    }],

    rbar: {
        padding: 3,
        cls: 'timeline-icons',
        items: [{
            xtype: 'button',
            iconCls: 'x-fa fa-microphone',
            press: 'startCall',
            handler: 'stopCall',
            tooltip: {
                html: 'PTT',
                autoHide: true,
                anchor: true,
                align: 'r'
            }
        }]
    },

    /**
     * Запуск проигрывания, возобновляет keep alive
     */
    play: function () {
        this.getController().play();
    },

    /**
     * Пауза проигрывания, прекращает keep alive
     */
    stop: function () {
        this.getController().stop();
    },

    /**
     * Развернуть плеер на весь экран
     */
    expand: function () {
        this.getController().fullscreen();
    },

    /**
     * функция заглушка
     */
    time: function () {
        return this;
    }
});