/**
 * Нативный hls плеер, см static/tests/hls/index.html
 */
Ext.define("Landau.video.hls.View", {
    extend: "Ext.panel.Panel",
    xtype: "hls",
    layout: 'vbox',
    controller: "video-hls-controller",

    requires: [
        "Landau.video.hls.Controller",
        "Landau.video.hls.Timeline"
    ],

    requiredScripts: [
        '/js/hls/hls.0.14.0.js',
        '/js/hls/HlsBuilder.js'
    ],

    mixins: ['Ext.mixin.Mashup'],

    config: {
        autoplay: false,
        device: null,
        volume: 0,
        dateTime: new Date(),
        date: new Date(),
        play: false,
        user: null
    },

    listeners: {
        resize: function (panel, w, h) {
            this.lookup("hls").setHeight(h-135);
        }
    },

    items: [{
        xtype: 'video',
        reference: 'hls',
        controls: true,
        listeners: {
            play: 'onPlay',
            pause: 'onPause'
        },
        hidden: false
    }, {
        xtype: 'panel',
        height: 135,
        items: [{
            style:'overflow-x: scroll;',
            height: 133,
            flex: 1,
            xtype: 'timeline',
            reference: 'timeline',
            listeners: {
                onSetTime: 'onSetTime'
            }
        }],
        rbar: {
            padding: 3,
            cls: 'timeline-icons',
            items: [{
                xtype: 'button',
                iconCls: 'x-fa fa-search-plus',
                handler: 'increase',
                tooltip: {
                    html: lang('Увеличить'),
                    autoHide: true,
                    anchor: true,
                    align: 'r'
                }
            }, {
                xtype: 'button',
                iconCls: 'x-fa fa-search-minus',
                handler: 'reduce',
                tooltip: {
                    html: lang('Уменьшить'),
                    autoHide: true,
                    anchor: true,
                    align: 'r'
                }
            }/*, {
                xtype: 'button',
                padding: 0,
                iconCls: 'x-fa fa-volume-off',
                tooltip: {
                    html: 'Включить звук',
                    autoHide: true,
                    anchor: true,
                    align: 'r'
                }
            }*/]
        }
    }],

    play: function () {
        this.getController().play();
    },

    pause: function () {
        this.getController().pause();
    },

    stop: function () {
        this.getController().stop();
    },

    setDateTime: function (datetime) {
        this.callParent([datetime]);

        let startDay = new Date(datetime.getTime());
        startDay.setHours(0,0,0,0);
        this.setDate(startDay);

        this.getController().setDateTime(datetime);
    },

    /*setDate: function (date) {
        if(date === null) {
            date = new Date();
        }

        this.callParent([date]);

        if(this.getController().isInit) {
            this.getController().setDate(date);
        }

        return this;
    },

    setSeconds: function (seconds) {
        this.getController().setSeconds(seconds);
    } */
});