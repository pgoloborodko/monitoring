/**
 *  hls timeline плеер, см static/tests/hls/timeline.html
 */
Ext.define("Landau.video.hls.Timeline", {
    extend: "Ext.panel.Panel",
    xtype: 'timeline',
    stage: null,
    timeline: null,
    lineLayer: null,
    chunkLayer: null,
    eventLayer: null,
    clockLayer: null,
    trueW: 0,
    w: 0,
    h: 120,
    divider: 1,
    startDate: new Date(),
    pixelMillisecond: 0,
    hourInterval: 0,
    hourIntervalWithDivider: 0,
    lineHeight: 15,
    eventColors: {
        "SOS": "rgba(255,69,49,0.80)",
        "PTT": "rgba(104,209,158,0.80)",
        "RECOGNITION": "rgba(104,209,158,0.80)",
        "STREAM": "rgba(92,169,209,0.8)"
    },
    eventPosition: {"SOS": 3, "STREAM": 4, "LOW": 5, "PTT": 6},
    currentDate: new Date(),
    clockTimeWidth: 60,
    timezone: new Date().getTimezoneOffset() * 60000,
    height: 120,
    widthFactor: 1,
    drag: false,
    dragged: false,
    resizeInterval: null,
    resizeWidth: 0,

    requiredScripts: [
        '/js/konva.min.js'
    ],

    mixins: ['Ext.mixin.Mashup'],

    config: {
        archive: [],
        events: []
    },

    resize: function(w) {
        console.log("resize timeline " + w + "x" + this.h);

        this.trueW = w;
        this.draw(this.trueW, this.h);
    },

    setDateTime: function(datetime) {
        let currentDayOfYear = datetime.getDate();

        if(!this.dragged) {
            this.currentDate = datetime;
            let startDay = new Date(datetime.getTime());
            startDay.setHours(0,0,0,0);
            this.startDate = startDay;

            if(startDay.getDate() !== currentDayOfYear) {
                this.drawTimeline();
                this.drawArchive();
                this.drawEvents();
            }

            this.drawClock();
        }
    },

    initialize: function () {
        this.stage = new Konva.Stage({
            container: this.el.dom,
            padding: 5
        });

        this.lineLayer = new Konva.Layer();
        this.chunkLayer = new Konva.Layer();
        this.eventLayer = new Konva.Layer();
        this.clockLayer = new Konva.Layer();

        this.stage.add(this.chunkLayer, this.eventLayer, this.lineLayer, this.clockLayer);

        this.resizeInterval = setInterval(function () {
            if(this.el.dom.offsetWidth !== this.resizeWidth) {
                this.resizeWidth = this.el.dom.offsetWidth;
                this.resize(this.resizeWidth);
            }
        }.bind(this), 1000);
    },

    destroy: function() {
        console.log("timeline destroy");
        clearInterval(this.resizeInterval);
        this.stage.destroy();
    },

    setArchive: function () {
        this.callParent(arguments);

        if(this.stage != null) {
            this.drawArchive();
        }
    },

    setEvents: function () {
        this.callParent(arguments);

        if(this.stage != null) {
            this.drawEvents();
            this.stage.draw();
        }
    },

    draw: function(trueW, h) {
        w = this.trueW * this.widthFactor;

        this.stage.setWidth(w);
        this.stage.setHeight(h);

        if(w < 400) {
            this.divider = 4;
        }
        else if(w < 700) {
            this.divider = 2;
        }
        else {
            this.divider = 1;
        }

        this.w = w - 2;
        this.h = h;
        this.hourInterval = this.w/24;
        this.hourIntervalWithDivider = this.hourInterval * this.divider;
        this.pixelMillisecond = (this.w)/24/3600/1000;

        console.log('draw');

        this.drawTimeline();
        this.drawArchive();
        this.drawEvents();
        this.drawClock();
    },

    drawTimeline: function() {
        console.log('drawTimeline');
        this.lineLayer.destroyChildren();

        for(let i=0; i <= 24; i++) {
            if(i % this.divider === 0) {
                this.lineLayer.add(this.drawHour(i));
            }
        }

        this.lineLayer.draw();
    },

    drawArchive: function () {
        console.log('drawArchive');
        this.chunkLayer.destroyChildren();

        this.chunkLayer.add(this.drawText(lang("СТРИМ"), 1));
        this.chunkLayer.add(this.drawText(lang("АРХИВ"), 2));

        let archive = this.getArchive();
        for(let i=0; i < archive.length; i++) {
            this.chunkLayer.add(this.drawChunk(archive[i]));
        }

        this.chunkLayer.draw();
    },

    drawEvents: function () {
        console.log('drawEvents');
        this.eventLayer.destroyChildren();

        this.eventLayer.add(this.drawText(lang("СОС"), 3));
        this.eventLayer.add(this.drawText(lang("СТРИМ"), 4));
        this.eventLayer.add(this.drawText(lang("НИЗКИЙ ЗАРЯД"), 5));
        this.eventLayer.add(this.drawText(lang("PTT"), 6));

        let events = this.getEvents();
        for(let i=0; i < events.length; i++) {
            this.eventLayer.add(this.drawEvent(events[i]));
        }

        this.eventLayer.draw();
    },

    drawText: function(text, position) {
        let textElem = new Konva.Text({
            x: 0,
            y: 2,
            height: this.lineHeight,
            text: text,
            fontSize: 12,
            fill: '#a7a7a7',
            padding: 0,
            align: 'center'
        });

        let line = new Konva.Line({
            points: [0, 0, this.w + 40, 0],
            stroke: 'rgba(167,167,167,0.38)',
            strokeWidth: 1,
            lineJoin: 'round',
            /*
             * line segments with a length of 33px
             * with a gap of 10px
             */
            dash: [5, 2],
        });

        let group = new Konva.Group({
            x: 0,
            y: position * this.lineHeight + this.lineHeight,
            width: this.w + 40,
            height: this.lineHeight
        });

        group.add(line);
        group.add(textElem);

        return group;
    },

    drawHour: function(hour) {
        let line = new Konva.Line({
            points: [0, 25, 0, this.h],
            strokeWidth: 1,
            stroke: 'rgba(217,217,217,0.69)',
            height: this.h
        });

        let text = new Konva.Text({
            x: hour === 0 ? 0 : hour === 24 ? - this.hourIntervalWithDivider : - this.hourIntervalWithDivider/2,
            y: 10,
            text: hour,
            fontSize: 12,
            fill: '#a7a7a7',
            width: this.hourIntervalWithDivider,
            padding: 0,
            align: hour === 0 ? 'left' : hour === 24 ? 'right' : 'center'
        });

        let group = new Konva.Group({
            x: this.hourInterval * hour,
            y: 0,
            width: this.hourIntervalWithDivider,
            height: this.h,
            fill: '#a7a7a7'
        });

        group.add(line);
        group.add(text);

        return group;
    },

    drawChunk: function(chunk) {
        let differenceTime = chunk.start - this.startDate.getTime();
        let interval = this.pixelMillisecond * differenceTime;
        let rect;

        if(chunk.stream) {
            rect = new Konva.Rect({
                x: interval,
                y: this.lineHeight * 2,
                width: this.pixelMillisecond * chunk.duration, //this.pixelMillisecond * chunk.duration,
                height: this.lineHeight,
                fill: 'rgba(115,179,239,1)',
                name: 'chunk'
            });
        }
        else {
            rect = new Konva.Rect({
                x: interval,
                y: this.lineHeight * 3,
                width: this.pixelMillisecond * chunk.duration, //this.pixelMillisecond * chunk.duration,
                height: this.lineHeight,
                fill: 'rgba(255,158,79,1)',
                name: 'chunk'
            });
        }

        rect.on('mouseover', function() {
            rect.scaleY(1.5);
            this.chunkLayer.draw();
        }.bind(this));

        rect.on('mouseout', function() {
            rect.scaleY(1);
            this.chunkLayer.draw();
        }.bind(this));

        rect.setAttr('value', chunk);

        return rect;
    },

    drawEvent: function(event) {

        let differenceTime = event.created - this.startDate.getTime();
        let interval = this.pixelMillisecond * differenceTime;

        let circle = new Konva.Circle({
            x: interval,
            y: this.eventPosition[event.type] * this.lineHeight + this.lineHeight + this.lineHeight/2,
            radius: this.lineHeight / 4,
            fill: this.eventColors[event.type],
            name: 'event'
        });

        circle.on('mouseover', function() {
            circle.scale({x: 1.5, y: 1.5});
            this.eventLayer.draw();
        }.bind(this));

        circle.on('mouseout', function() {
            circle.scale({x: 1,  y: 1});
            this.eventLayer.draw();
        }.bind(this));

        circle.on('mouseup', function() {
            this.fireEvent("eventClick", event);
        }.bind(this));

        circle.setAttr('value', event);

        return circle;
    },

    drawClock: function(event) {
        console.log('drawClock');

        this.clockLayer.destroyChildren();
        this.stage.off('mousemove');
        this.stage.off('mouseup');
        this.stage.off('mousedown');

        let mouseCorrect = 0;

        let group = new Konva.Group({
            x: 0,
            y: 0,
            width: this.clockTimeWidth,
            height: this.h,
            name: 'clock'
        });

        let text = new Konva.Text({
            x: 0,
            y: 10,
            text: "00:00:00",
            fontSize: 12,
            fill: '#FAFAFA',
            width: this.clockTimeWidth,
            padding: 0,
            align: 'center',
            name: 'clock'
        });

        let line = new Konva.Line({
            points: [this.clockTimeWidth/2, 0, this.clockTimeWidth/2, this.h],
            strokeWidth: 1,
            stroke: '#00CCFF',
            height: this.h,
            name: 'clock'
        });

        let rect = new Konva.Rect({
            x: 0,
            y: 4,
            width: this.clockTimeWidth, //this.pixelMillisecond * chunk.duration,
            height: this.lineHeight + this.lineHeight/2,
            fill: '#00CCFF',
            radius: 10,
            name: 'clock'
        });

        group.add(line);
        group.add(rect);
        group.add(text);

        let linePosition = function (pixelPosition) {
            let correct = this.clockTimeWidth/2;

            pixelPosition = pixelPosition - mouseCorrect;

            if(pixelPosition <= 0) {
                correct = correct + pixelPosition;
                pixelPosition = 0;
            }
            else if(pixelPosition >= this.w - this.clockTimeWidth) {
                correct = this.clockTimeWidth - (this.w - pixelPosition) + correct;
                pixelPosition = this.w - this.clockTimeWidth;
            }

            if(correct < 0) {
                correct = 0;
            }
            else if(correct > this.clockTimeWidth) {
                correct = this.clockTimeWidth;
            }

            line.points([correct, 0, correct, this.h]);
            group.x(pixelPosition);

            text.text(this.getTimeFromMilliseconds((pixelPosition + correct)  / this.pixelMillisecond));
            this.clockLayer.draw();
        }.bind(this);

        this.stage.on('mousedown', function(e) {
            this.drag = true;
            this.dragged = false;

            if(e.target.parent !== null && e.target.parent.name() === "clock") {
                mouseCorrect = e.evt.layerX - group.attrs.x;
            }
            else if(e.target !== null && e.target.name() === "chunk") {
                mouseCorrect = 0;
                let value = (e.target.attrs.value.start - this.startDate.getTime()) * this.pixelMillisecond;
                linePosition(value - this.clockTimeWidth/2);
                mouseCorrect = e.evt.layerX - group.attrs.x;
            }
            else if(e.target !== null && e.target.name() === "event") {
                mouseCorrect = 0;
                let value = (e.target.attrs.value.created - this.startDate.getTime()) * this.pixelMillisecond;
                linePosition(value - this.clockTimeWidth/2);
                mouseCorrect = e.evt.layerX - group.attrs.x;
            }
            else {
                mouseCorrect = this.clockTimeWidth / 2;
                linePosition(e.evt.layerX);
            }
        }.bind(this));

        this.stage.on('mouseup', function() {
            this.drag = false;
            this.dragged = false;
            this.fireEvent("onSetTime", this.currentDate);
        }.bind(this));

        this.stage.on('mousemove', function(e) {
            if(this.drag) {
                this.dragged = true;
                linePosition(e.evt.layerX);
            }
        }.bind(this));

        linePosition((this.currentDate.getTime() - this.startDate.getTime()) * this.pixelMillisecond - this.clockTimeWidth/2);

        this.clockLayer.add(group);
        this.clockLayer.draw();
    },

    getTimeFromMilliseconds: function (millis) {
        if(!isNaN(millis)) {
            this.currentDate = new Date(this.startDate.getTime() + millis);
        }
        return this.toHe(this.currentDate.getHours()) + ":" + this.toHe(this.currentDate.getMinutes()) + ":" + this.toHe(this.currentDate.getSeconds());
    },

    toHe: function (time) {
        return (time < 10 ? "0" : "") + time;
    },

    increase: function () {
        //horizontal = e.currentTarget.scrollLeft;
        if(this.widthFactor < 30) {
            this.widthFactor *= 2;
            this.draw(this.trueW, this.h);

            let offset = (this.currentDate.getTime() - this.startDate.getTime()) * this.pixelMillisecond;
            this.el.dom.scrollLeft = offset - this.el.dom.offsetWidth / 2;
        }
    },

    reduce: function () {
        if(this.widthFactor > 1) {
            this.widthFactor /= 2;
            this.draw(this.trueW, this.h);

            let offset = (this.currentDate.getTime() - this.startDate.getTime()) * this.pixelMillisecond;
            this.el.dom.scrollLeft = offset - this.el.dom.offsetWidth / 2;
        }
    }
});