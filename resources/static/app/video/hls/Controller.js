Ext.define("Landau.video.hls.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.video-hls-controller",
    hls: null,
    hlsPlaylist: [],
    isInit: false,
    isPlay: false,
    timelineInterval: null,
    currentFrag: null,

    config: {
        hls: null,
        archive: [],
        date: new Date()
    },

    getVideo: function() {
        return this.lookup("hls").media.dom;
    },

    init: function () {
        if(this.hls !== null) {
            this.hls.destroy();
        }

        this.getVideo().muted = this.getView().getVolume() === 0;

        if(!window.Hls || !window.HlsBuilder) {
            return Ext.Msg.alert("Ошибка!", "Отсутствует HLS или HlsBuilder компонент!");
        }

        this.hls = new Hls({
            fragLoadingTimeOut: 30000,
            manifestLoadingTimeOut: 30000,
            levelLoadingTimeOut: 30000
        });

        this.hls.attachMedia(this.getVideo());
        this.hls.on(Hls.Events.ERROR, this.onError.bind(this));
        this.hls.on(Hls.Events.FRAG_CHANGED, function (ev, frag) {
            this.currentFrag = frag.frag;
            console.log('frag changed', frag);
        }.bind(this));

        this.fireViewEvent('onInit', this.getView());

        let timeline = this.lookup("timeline");

        this.timelineInterval = setInterval(function () {
            if(this.isPlay && this.currentFrag != null) {
                timeline.setDateTime(new Date(this.currentFrag.programDateTime + (this.getVideo().currentTime * 1000 - this.currentFrag.start * 1000)));
            }
        }.bind(this), 1000);
    },

    destroy: function () {
        console.debug("hls.destroy");

        if(this.hls != null) {
            this.hls.destroy();
        }

        if(this.timelineInterval != null) {
            clearInterval(this.timelineInterval);
        }
    },

    play: function () {
        console.debug('hls.play');
        this.isPlay = true;

        this.getVideo().play();
    },

    pause: function () {
        console.debug("hls.pause");
        this.isPlay = false;

        this.getVideo().pause();
    },

    stop: function () {
        console.debug("hls.stop");
        this.isPlay = false;

        this.lookup("hls").stop();
        this.lookup("timeline").setDateTime(this.getView().getDate());
    },

    setDateTime: function() {
        this.lookup("timeline").setDateTime(this.getView().getDateTime());

        new HlsBuilder(location.origin, this.getView().getDate(), this.getView().getUser().id).load(function(playlist, m3u8) {
            this.setArchive(playlist);
            this.lookup("timeline").setArchive(playlist);
            this.hls.loadSource(URL.createObjectURL(new Blob([new TextEncoder("utf-8").encode(m3u8)])));
        }.bind(this));

        Ext.create("Landau.events.EventButtonStoreDay").load({
            params: {
                user: this.getView().getUser().id,
                day: Ext.Date.format(this.getView().getDate(), "Y-m-d")
            },
            callback: function (data) {
                let events = [];
                for(let i=0; i < data.length; i++) {
                    data[i].data.created = new Date(data[i].data.created).getTime();
                    events.push(data[i].data);
                }

                this.lookup("timeline").setEvents(events);
            }.bind(this)
        });

        if(this.getView().getAutoplay()) {
            this.play();
        }
    },

    onError: function(event, data) {
        console.debug("hls.onError", data);

        if (data.fatal) {
            this.pause();

            switch(data.type) {
                case Hls.ErrorTypes.MEDIA_ERROR:
                    console.debug("onError", data);
                    this.getView().fireEvent('onError', this.getView(), data.type);

                    break;
                case Hls.ErrorTypes.NETWORK_ERROR:
                    console.debug("onNotFound", data);
                    this.getView().fireEvent('onNotFound', this.getView(), data.type);
                    break;
            }
        }
        else {
            if(this.isPlay) {
                this.pause();
                this.hls.recoverMediaError();
                this.play();
            }
        }
    },

    onPlay: function() {
        console.debug("hls.onPlay");
        this.getView().fireEvent('onPlay', this.getView());
    },

    onPause: function() {
        console.debug("hls.onPause");
        this.getView().fireEvent('onPause', this.getView());
    },

    onSetTime: function (datetime) {
        this.currentFrag = null;
        let archive = this.getArchive();
        let time = datetime.getTime();
        let near = null;

        this.updateHashValue("datetime", datetime.getTime());

        for(let i=0; i < archive.length; i++) {
            if(time >= archive[i].start && time - archive[i].start < archive[i].duration) {

                this.getVideo().currentTime = (archive[i].time + (time - archive[i].start))/1000;
                this.play();
                this.getView().fireEvent('onPlay', this.getView());

                console.log(this.lookup("timeline").getTimeFromMilliseconds(this.getVideo().currentTime * 1000));
                return;
            }
            else if(time < archive[i].start && near === null) {
                near = archive[i];
            }
        }

        if(near != null) {
            this.currentFrag = null;
            this.getVideo().currentTime = near.time/1000 + 1;

            console.log(this.lookup("timeline").getTimeFromMilliseconds(this.getVideo().currentTime * 1000));

            this.lookup("timeline").setDateTime(new Date(near.start));
            this.play();
            this.getView().fireEvent('onPlay', this.getView());
        }
        else {
            this.pause();
            this.currentFrag = null;
        }

        console.debug(this.getVideo().currentTime);
    },

    prevChunk: function () {

    },

    nextChunk: function () {

    },

    increase: function () {
        this.lookup("timeline").increase();
    },

    reduce: function () {
        this.lookup("timeline").reduce();
    },

    mute: function (ctx) {
        ctx.setIconCls("x-fa fa-volume-on");
        this.getVideo().muted = true;
    }
});



