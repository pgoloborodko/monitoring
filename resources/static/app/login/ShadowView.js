/**
 * Demonstrates a simple login form.
 */
Ext.define('Landau.login.ShadowView', {
    extend: 'Ext.Dialog',
    xtype: 'shadow-login-view',
    singleton: true,
    alternateClassName: 'ShadowView',

    requires: [
        'Landau.login.View'
    ],

    padding: '0',

    items: [{
        xtype: 'login-view',
        reference: "login",
        shadowLogin: true
    }]
});