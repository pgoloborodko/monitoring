Ext.define('Landau.login.register.View', {
    extend: 'Ext.Dialog',
    controller: 'login-register-controller',
    title: lang('Регистрация'),

    requires: [
        'Landau.login.register.Controller',
        'Landau.companies.CompaniesStore',
        'Landau.cities.CitiesStore',
        'Landau.districts.DistrictsStore'
    ],

    width: 320,
    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Файл ключа'),
            required: true,
            layout: 'vbox',
            defaults: {
                flex: 1
            },
            items: [{
                xtype:'filefield',
                label: '',
                name: 'key',
                required: true,
            }]
        }, {
            xtype: 'containerfield',
            label: lang('Название компании'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'textfield',
                required: true,
                name: 'company'
            }]
        },{
            xtype: 'containerfield',
            label: lang('Номер телефона'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                // xtype: 'textfield',
                // required: true,
                // name: 'phone',
                // placeholder: '+7 (xxx) xxx-xxxx',
                // inputMask: '+7 (999) 999-99-99'
                placeholder: '+7 (xxx) xxx-xxxx',
                required: true,
                inputMask: '+7 (999) 999-99-99'
            }]
        }, {
            xtype: 'containerfield',
            label: lang('Пароль'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                // xtype: 'passwordfield',
                // required: true,
                // name: 'password',
                // validators: /.{9,}$/
                xtype: 'passwordfield',
                name: 'password',
                //placeholder: lang('Пароль'),
                required: true,
                errorTarget: 'side',

                validators: function (val) {
                    var lengthOfPass = 9;
                    //var tn = val.replace(/.{9,}$/),
                    //var tn = val.replace(/[^0-9]/g,''),
                    var tn = val;
                    errMsg = lang("Длина пароля должна быть больше") + " " + lengthOfPass + " " + lang("символов") + "!";
                    return (tn.length >= lengthOfPass) ? true : errMsg;
                }
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});