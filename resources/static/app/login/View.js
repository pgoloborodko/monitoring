/**
 * Demonstrates a simple login form.
 */
Ext.define('Landau.login.View', {
    extend: 'Ext.Panel',
    xtype: 'login-view',

    requires: [
        'Landau.login.Controller'
    ],

    config: {
        shadowLogin: false,
        phoneValue: null
    },

    layout: 'center',

    controller: {
        init: function () {
            if(this.getView().getShadowLogin()) {
                this.lookup("form").lookup("phoneValue").setValue(Ext.util.Cookies.get("phone"));
                this.lookup("form").lookup("phoneValue").setDisabled(true);
                this.lookup("form").lookup("phoneValue").setName(null);

                this.lookup("form").add(Ext.create('Ext.field.Hidden', {
                    name: 'phone',
                    value: Ext.util.Cookies.get("phone")
                }));

                this.lookup("form").lookup("lang").setHidden(true);
                this.lookup("form").lookup("key").setHidden(true);
                this.lookup("form").lookup("logout").setHidden(false);
            }
        }
    },

    items: [{
        xtype: 'container',
        padding: 20,
        layout: 'center',
        reference: 'container',
        platformConfig: {
            '!desktop': {
                width: "100%"
            }
        },
        items: [{
            xtype: 'formpanel',
            reference: 'form',
            controller: 'login-controller',
            url: '/login/auth',
            title: lang('Авторизация'),
            platformConfig: {
                '!desktop': {
                    width: "90%"
                }
            },
            width: 320,
            bodyPadding: 20,
            autoSize: true,
            shadow: true,
            items: [{
                reference: 'phone',
                xtype: 'containerfield',
                label: lang('Номер телефона'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'textfield',
                    required: true,
                    name: 'phone',
                    reference: 'phoneValue',
                    placeholder: '+7 (xxx) xxx-xxxx',
                    inputMask: '+7 (999) 999-99-99'
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Пароль'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'passwordfield',
                    required: true,
                    name: 'password',
                    //validators: /.{9,}$/
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Язык'),
                required: true,
                reference: "lang",
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'selectfield',
                    name: 'language',
                    value: readCookie("lang") === null ? "ru" : readCookie("lang"),
                    options: [{
                        text: 'Русский',
                        value: 'ru',
                        selected: true,
                    },{
                        text: 'English',
                        value: 'en'
                    }],
                    listeners: {
                        change: function (ctx, value) {
                            Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});
                            createCookie("lang", value, 10000000);
                            location.reload();
                        }
                    }
                }]
            }],

            buttons: [{
                text: lang('Выйти'),
                handler: 'onLogout',
                reference: "logout",
                ui: 'decline',
                hidden: true
            }, {
                text: lang('Регистрация'),
                reference: "key",
                handler: 'onKey'
            }, {
                xtype: 'spacer'
            }, {
                text: lang('Войти'),
                handler: 'onLogin',
                ui: 'action'
            }]
        }, {
            xtype: 'container',
            disabled: true,
            html: '<a href="#" ' +
                'style="display: block; ' +
                'text-align: center; ' +
                'margin-top: 10px; ' +
                'color: black;">Список изменений</a>',
            listeners: {
                element  : 'element',
                click    : function() {
                    Ext.create("Landau.login.Release").show();
                }
            }
        }]
    }]
});