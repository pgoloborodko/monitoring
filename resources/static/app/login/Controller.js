Ext.define('Landau.login.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login-controller',

    failureCallback: function (ctx, response) {
        console.log(response);

        Ext.Msg.alert(lang('Ошибка!'), lang('Логин/пароль неверные!'));
    },

    successCallback: function (ctx, response) {
        console.log(response);

        if(!this.lookup("phoneValue").getDisabled()) {
            Ext.util.Cookies.set("phone", this.lookup("phoneValue").getValue());
            this.getView().getParent().hide();
            location.reload();
        }
        else {
            ShadowView.hide();
        }
    },

    onLogout: function() {
        location.href = "/logout";
    },

    onKey: function() {
        Ext.create("Landau.login.register.View").show();
    },

    onLogin: function () {
        var form = this.getView();

        if (form.validate()) {
            form.submit({
                waitMsg:lang('Запрос...'),
                scope: this,
                success: this.successCallback,
                failure: this.failureCallback
            });
        } else {
            Ext.Msg.alert(lang('Ошибка!'), lang('Заполните правильно все поля!'));
        }
    }
});