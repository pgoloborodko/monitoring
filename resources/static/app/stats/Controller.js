Ext.define("Landau.stats.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.stats-controller",

    loadPage: function (list, index, element, event) {
        Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Загрузка...', indicator:true});
        this.lookup('listStats').select(index);
        var that = this;
        that.lookup('container').setItems([]);

        setTimeout(function () {
            var newView = Ext.create(event.get("view"));
            that.lookup('container').setItems(newView);

            Ext.Viewport.setMasked(false);
        }, 10);
    },


    init: function () {
        this.lookup('listStats').select(0);

        Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Загрузка...', indicator:true});
        var newView = Ext.create('Landau.stats.uptime.View');

        //this.lookup('container').removeAll();
        this.lookup('container').setItems(newView);

        Ext.Viewport.setMasked(false);
    }
});