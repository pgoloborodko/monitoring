Ext.define('Landau.stats.commonstat.StreamModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'time', convert: function (value, record) {
                return value
        }},

        {name: 'devices', convert: function (value, record) {
                return value
        }},

        {name: 'streaming', convert: function (value, record) {
                return value
        }}
    ]
});