Ext.define('Landau.stats.commonstat.ServerStore', {
    alias: 'store.stats-commonstat-serverstore',
    extend: 'Ext.data.Store',

    model: 'Landau.stats.commonstat.ServerModel',

    //fields: ['time', 'cpu', 'mem'],

    data: [
        {"time": 0, "cpu": 10, 'mem': 5, 'streams': 33, 'disk': 65},
        {"time": 1, "cpu": 40, 'mem': 13, 'streams': 21, 'disk': 65},
        {"time": 2, "cpu": 50, 'mem': 13, 'streams': 34, 'disk': 64},
        {"time": 3, "cpu": 18, 'mem': 13, 'streams': 32, 'disk': 4},
        {"time": 4, "cpu": 17, 'mem': 17, 'streams': 54, 'disk': 34},
        {"time": 5, "cpu": 6, 'mem': 5, 'streams': 65, 'disk': 43},
        {"time": 6, "cpu": 4, 'mem': 3, 'streams': 12, 'disk': 65},
        {"time": 7, "cpu": 10, 'mem': 12, 'streams': 12, 'disk': 54},
        {"time": 8, "cpu": 10, 'mem': 12, 'streams': 12, 'disk': 65},
        {"time": 9, "cpu": 10, 'mem': 12, 'streams': 3, 'disk': 76},
        {"time": 10, "cpu": 1, 'mem': 40, 'streams': 3, 'disk': 65},
        {"time": 11, "cpu": 15, 'mem': 20, 'streams': 4, 'disk': 54},
        {"time": 12, "cpu": 14, 'mem': 20, 'streams': 4, 'disk': 65},
        {"time": 13, "cpu": 35, 'mem': 20, 'streams': 5, 'disk': 45},
        {"time": 14, "cpu": 22, 'mem': 2, 'streams': 6, 'disk': 34},
        {"time": 15, "cpu": 2, 'mem': 2, 'streams': 7, 'disk': 56},
        {"time": 16, "cpu": 45, 'mem': 2, 'streams': 33, 'disk': 67},
        {"time": 17, "cpu": 12, 'mem': 2, 'streams': 32, 'disk': 56},
        {"time": 18, "cpu": 16, 'mem': 2, 'streams': 31, 'disk': 45},
        {"time": 19, "cpu": 14, 'mem': 2, 'streams': 11, 'disk': 34},
        {"time": 20, "cpu": 10, 'mem': 2, 'streams': 22, 'disk': 56},
        {"time": 21, "cpu": 10, 'mem': 2, 'streams': 22, 'disk': 67},
        {"time": 22, "cpu": 88, 'mem': 2, 'streams': 33, 'disk': 65},
        {"time": 23, "cpu": 96, 'mem': 24, 'streams': 4, 'disk': 65},
        {"time": 24, "cpu": 100, 'mem': 55, 'streams': 4, 'disk': 65}
    ]
});