Ext.define('Landau.stats.commonstat.ServerModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'time', convert: function (value, record) {
                return value
        }},

        {name: 'cpu', convert: function (value, record) {
                return value
        }},

        {name: 'mem', convert: function (value, record) {
                return value
        }},

        {name: 'streams', convert: function (value, record) {
                return value
            }}
    ]
});