Ext.define('Landau.stats.commonstat.StreamStore', {
    alias: 'store.stats-commonstat-streamstore',
    extend: 'Ext.data.Store',

    model: 'Landau.stats.commonstat.StreamModel',

    //fields: ['time', 'devices', 'streaming'],

    data: [
        {"time": 0, "devices": 10, 'streaming': 5},
        {"time": 1, "devices": 4, 'streaming': 3},
        {"time": 2, "devices": 5, 'streaming': 3},
        {"time": 3, "devices": 8, 'streaming': 3},
        {"time": 4, "devices": 17, 'streaming': 17},
        {"time": 5, "devices": 60, 'streaming': 55},
        {"time": 6, "devices": 45, 'streaming': 43},
        {"time": 7, "devices": 10, 'streaming': 2},
        {"time": 8, "devices": 10, 'streaming': 2},
        {"time": 9, "devices": 10, 'streaming': 2},
        {"time": 10, "devices": 11, 'streaming': 4},
        {"time": 11, "devices": 15, 'streaming': 2},
        {"time": 12, "devices": 14, 'streaming': 2},
        {"time": 13, "devices": 35, 'streaming': 2},
        {"time": 14, "devices": 22, 'streaming': 2},
        {"time": 15, "devices": 22, 'streaming': 2},
        {"time": 16, "devices": 45, 'streaming': 2},
        {"time": 17, "devices": 10, 'streaming': 2},
        {"time": 18, "devices": 10, 'streaming': 2},
        {"time": 19, "devices": 10, 'streaming': 2},
        {"time": 20, "devices": 10, 'streaming': 2},
        {"time": 21, "devices": 10, 'streaming': 2},
        {"time": 22, "devices": 10, 'streaming': 2},
        {"time": 23, "devices": 10, 'streaming': 2},
        {"time": 24, "devices": 100, 'streaming': 55}
    ]
});