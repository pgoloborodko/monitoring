Ext.define('Landau.stats.commonstat.SyncModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'time', convert: function (value, record) {
                return value
        }},

        {name: 'stores', convert: function (value, record) {
                return value
        }},

        {name: 'streaming', convert: function (value, record) {
                return value
        }},

        {name: 'terminals', convert: function (value, record) {
                return value
        }}
    ]
});