Ext.define('Landau.stats.commonstat.ChartStream', {
    extend: 'Ext.chart.CartesianChart',
    xtype: 'stats-commonstat-chartstream',

    requires: [
        'Landau.stats.commonstat.StreamStore',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Time',
        'Ext.chart.interactions.CrossZoom'
    ],

    store: {
        type: 'stats-commonstat-streamstore'
        //fields: ['time', 'devices', 'striming']
    },

    insetPadding: {
        top: 20
    },

    engine: 'Ext.draw.engine.Svg',

    axes: [{
        type: 'numeric',
        position: 'left',
        grid: true,
        title: lang('Штук')
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['time'],
        title: lang('Время суток')
    }],

    legend: {
        type: 'sprite',
        position: 'bottom',
        marker: {
            size: 15
        }
    },

    series: [
        {
            type: 'area',
            subStyle: {
                fill: '#57bbd6'
            },
            xField: 'time',
            yField: 'devices',
            title: lang('Регистраторы в сети')
        },
        {
            type: 'area',
            subStyle: {
                fill: '#ee6e73'
            },
            xField: 'time',
            yField: 'streaming',
            title: lang('Регистраторы в режиме стриминга')
        }
    ]
});