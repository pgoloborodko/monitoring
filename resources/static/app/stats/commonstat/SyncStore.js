Ext.define('Landau.stats.commonstat.SyncStore', {
    alias: 'store.stats-commonstat-syncstore',
    extend: 'Ext.data.Store',

    model: 'Landau.stats.commonstat.SyncModel',

    data: [
        {"time": 0, "stores": 10, 'streaming': 5, 'terminals': 34},
        {"time": 1, "stores": 4, 'streaming': 3, 'terminals': 33},
        {"time": 2, "stores": 5, 'streaming': 3, 'terminals': 32},
        {"time": 3, "stores": 8, 'streaming': 3, 'terminals': 33},
        {"time": 4, "stores": 17, 'streaming': 17, 'terminals': 32},
        {"time": 5, "stores": 60, 'streaming': 55, 'terminals': 33},
        {"time": 6, "stores": 45, 'streaming': 43, 'terminals': 33},
        {"time": 7, "stores": 10, 'streaming': 2, 'terminals': 33},
        {"time": 8, "stores": 10, 'streaming': 2, 'terminals': 33},
        {"time": 9, "stores": 10, 'streaming': 2, 'terminals': 33},
        {"time": 10, "stores": 11, 'streaming': 4, 'terminals': 33},
        {"time": 11, "stores": 15, 'streaming': 2, 'terminals': 33},
        {"time": 12, "stores": 14, 'streaming': 2, 'terminals': 13},
        {"time": 13, "stores": 35, 'streaming': 2, 'terminals': 11},
        {"time": 14, "stores": 22, 'streaming': 2, 'terminals': 11},
        {"time": 15, "stores": 22, 'streaming': 2, 'terminals': 13},
        {"time": 16, "stores": 45, 'streaming': 2, 'terminals': 13},
        {"time": 17, "stores": 10, 'streaming': 2, 'terminals': 13},
        {"time": 18, "stores": 10, 'streaming': 2, 'terminals': 13},
        {"time": 19, "stores": 10, 'streaming': 2, 'terminals': 3},
        {"time": 20, "stores": 10, 'streaming': 2, 'terminals': 3},
        {"time": 21, "stores": 10, 'streaming': 2, 'terminals': 3},
        {"time": 22, "stores": 10, 'streaming': 2, 'terminals': 3},
        {"time": 23, "stores": 10, 'streaming': 2, 'terminals': 3},
        {"time": 24, "stores": 100, 'streaming': 55, 'terminals': 3}
    ]
});