Ext.define('Landau.stats.commonstat.ChartServer', {
    extend: 'Ext.chart.CartesianChart',
    xtype: 'stats-commonstat-chartserver',

    requires: [
        'Landau.stats.commonstat.ServerStore',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Time',
        'Ext.chart.interactions.CrossZoom'
    ],

    store: {
        type: 'stats-commonstat-serverstore'
    },

    insetPadding: {
        top: 20
    },

    engine: 'Ext.draw.engine.Svg',

    axes: [{
        type: 'numeric',
        position: 'left',
        grid: true,
        title: '%'
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['time'],
        title: lang('Время суток')
    }],

    legend: {
        type: 'sprite',
        position: 'bottom',
        marker: {
            size: 15
        }
    },

    series: [
        {
            type: 'line',
            style: {
                lineWidth: 2
            },
            xField: 'time',
            yField: 'cpu',
            title: 'CPU'
        },
        {
            type: 'line',
            style: {
                lineWidth: 2
            },
            xField: 'time',
            yField: 'mem',
            title: 'MEM'
        },
        {
            type: 'line',
            style: {
                lineWidth: 2
            },
            xField: 'time',
            yField: 'streams',
            title: lang('Потоки')
        },
        {
            type: 'line',
            style: {
                lineWidth: 2
            },
            xField: 'time',
            yField: 'disk',
            title: 'DISK'
        }
    ]
});