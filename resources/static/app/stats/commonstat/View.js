Ext.define("Landau.stats.commonstat.View", {
    extend: 'Ext.Panel',

    requires: [
        'Landau.stats.commonstat.ChartStream',
        'Landau.stats.commonstat.ChartServer',
        'Landau.stats.commonstat.ChartSync',
        'Landau.stats.commonstat.Controller',
        'Landau.companies.CompaniesStore'
    ],

    controller: 'stats-commonstat-controller',

    height: "100%",
    defaultType: 'panel',
    scrollable: true,

    items: [{
        xtype: 'container',
        padding: '20 20 0 20',
        style:{"background-color":"#fff"},
        html: '<h2>' + lang('Регистраторы') + '</h2>',
    }, {
        height: "40%",
        xtype: 'stats-commonstat-chartstream',
        reference: 'chartStrim'
    }, {
        xtype: 'container',
        padding: '20 20 0 20',
        style:{"background-color":"#fff"},
        html: '<h2>' + lang('Стрим-сервера') + '</h2>',
    }, {
        height: "40%",
        xtype: 'stats-commonstat-chartserver',
        reference: 'chartServer'
    }, {
        xtype: 'container',
        padding: '20 20 0 20',
        style:{"background-color":"#fff"},
        html: '<h2>' + lang('Синхронизация данных хранилищ') + '</h2>',
    }, {
        height: "40%",
        xtype: 'stats-commonstat-chartsync',
        reference: 'chartSync'
    }
    ],

    tbar: [{
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'dateCurrent',
        dateFormat: 'd/m/Y'
    },{
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});