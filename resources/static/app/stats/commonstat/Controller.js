Ext.define("Landau.stats.commonstat.Controller", {
    extend: 'Ext.app.ViewController',
    alias: 'controller.stats-commonstat-controller',

    requires: [
        'Ext.exporter.text.CSV',
        'Ext.exporter.text.TSV',
        'Ext.exporter.text.Html',
        'Ext.exporter.excel.Xml',
        'Ext.exporter.excel.Xlsx',
        'Ext.exporter.excel.PivotXlsx'
    ],

    init: function() {

        // this.lookup("chartStream").getStore().reload({
        //     params: {
        //         device: 50, //this.getView().getDevice().id,
        //         date: '2019-10-17' //Ext.Date.format(new Date(selectedDate), "Y-m-d")
        //     }
        // });


    },

    doFilter: function () {
        var dateCurrent = this.lookup("dateCurrent").getValue();
        dateCurrent.setHours(0,0,0,0);

        this.lookup("chartStrim").getStore().reload({
            params: {
                from: Ext.Date.format(dateCurrent, "Y-m-d")
            }
        });

        this.lookup("chartServer").getStore().reload({
            params: {
                from: Ext.Date.format(dateCurrent, "Y-m-d")
            }
        });

        this.lookup("chartSync").getStore().reload({
            params: {
                from: Ext.Date.format(dateCurrent, "Y-m-d")
            }
        });
    },

    exportDocument: function (menuitem) {
        var dateStart = this.lookup("dateStart").getValue();
        dateStart.setHours(0,0,0,0);

        var dateEnd = this.lookup("dateEnd").getValue();
        dateEnd.setHours(0,0,0,0);

        var pivotgrid = this.lookup('grid');

        pivotgrid.saveDocumentAs({
            type: 'xlsx',
            title: 'Отчет по времени работы с ' + Ext.Date.format(dateStart, "Y-m-d") + ' по ' + Ext.Date.format(dateEnd, "Y-m-d"),
            fileName: 'uptime_stats_' + Ext.Date.format(dateStart, "Y-m-d") + '-' + Ext.Date.format(dateEnd, "Y-m-d") + '.xlsx'
        });
    },

    onError: function (error) {
        Ext.Msg.alert('Error', typeof error === 'string' ? error : 'Unknown error');
    },

    onDetail: function(grid, info) {
        //console.log(info.record.get('device'));
        var dialog = Ext.create("Landau.stats.commonstat.details.View", {
            parentController: this,
            date: this.lookup("dateCurrent").getValue(),
            device: info.record.get('device')
        });

        dialog.show();
    }
});