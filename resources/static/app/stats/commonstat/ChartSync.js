Ext.define('Landau.stats.commonstat.ChartSync', {
    extend: 'Ext.chart.CartesianChart',
    xtype: 'stats-commonstat-chartsync',

    requires: [
        'Landau.stats.commonstat.SyncStore',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Time',
        'Ext.chart.interactions.CrossZoom'
    ],

    store: {
        type: 'stats-commonstat-syncstore'
    },

    insetPadding: {
        top: 20
    },

    engine: 'Ext.draw.engine.Svg',

    axes: [{
        type: 'numeric',
        position: 'left',
        grid: true,
        title: lang('Тбайт')
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['time'],
        title: lang('Время суток')
    }],

    legend: {
        type: 'sprite',
        position: 'bottom',
        marker: {
            size: 15
        }
    },

    series: [
        {
            type: 'line',
            xField: 'time',
            yField: 'stores',
            title: lang('Хранилищ'),
            style: {
                lineWidth: 2
            }
        },
        {
            type: 'line',
            xField: 'time',
            yField: 'streaming',
            title: lang('Стриминга'),
            style: {
                lineWidth: 2
            }
        },
        {
            type: 'line',
            xField: 'time',
            yField: 'terminals',
            title: lang('Терминала'),
            style: {
                lineWidth: 2
            }
        }
    ]
});