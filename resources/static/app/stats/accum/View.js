Ext.define("Landau.stats.accum.View", {
    extend: 'Ext.Panel',
    xtype: 'stats-accum-view',

    requires: [
        'Landau.stats.accum.Grid',
        'Landau.stats.accum.Controller',
        'Landau.companies.CompaniesSelect'
    ],

    controller: 'stats-accum-controller',

    height: "100%",
    defaultType: 'panel',

    items: [{
        height: "100%",
        xtype: 'stats-accum-grid-view',
        reference: 'grid'
    }],

    tbar: [{
        xtype: 'spacer'
    }, {
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'dateCurrent',
        dateFormat: 'd/m/Y'
    },{
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});