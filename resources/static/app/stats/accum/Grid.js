Ext.define("Landau.stats.accum.Grid", {
    extend: 'Ext.grid.Grid',
    xtype: 'stats-accum-grid-view',

    requires: [
        'Landau.stats.accum.StatsAccumStore'
    ],

    store: {
        type: 'stats-accum-store',
        autoLoad: true
    },

    columns: [{
        text: lang('Устройство'),
        dataIndex: 'deviceName',
        width: 150,
    }, {
        text: lang('Заряд/разряд, мАч'),
        dataIndex: 'capacityChargeAtHour',
        flex: 1,
        renderer: function (value, record) {
            return record.data.capacityChargeAtHour + "/" + record.data.capacityDischargeAtHour;
        }

    }, {
        text: lang('Заряд/разряд, %'),
        dataIndex: 'percentChargeAtHour',
        flex: 1,
        renderer: function (value, record) {
            return record.data.percentChargeAtHour + "/" + record.data.percentDischargeAtHour;
        }
    }, {
        text: lang('Полная зарядка/разрядка'),
        dataIndex: 'dischargeTimeLeftInSeconds',
        flex: 1,
        renderer: function (value, record) {
            return record.data.chargeTimeLeftInSeconds + "/" + record.data.dischargeTimeLeftInSeconds;
        }
    }, {
        text: lang('Остаток'),
        dataIndex: 'lastCharge',
        width: 80
    }, {
        text: lang('График'),
        dataIndex: 'chart',
        width: 150,
        cell: {
            xtype: 'widgetcell',
            forceWidth: true,
            widget: {
                xtype: 'sparklineline'
            }
        }
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 150
    }, {
        text: lang('Детальная информация'),
        width: 50,
        locked: true,
        sortable: false,
        menuDisabled: true,

        cell: {
            tools: [{
                type: 'maximize',
                handler: 'onDetail',
                tooltip: lang('Детальная информация')
            }]
        }
    }]
});