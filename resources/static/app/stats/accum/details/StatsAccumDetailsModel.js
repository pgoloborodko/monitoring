Ext.define('Landau.stats.accum.details.StatsAccumDetailsModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'from_to', convert: function (value, record) {
            //console.log(record);
            var from = record.data.from;
            var to = record.data.to;

            if ((!!from) && (!!to)) {
                if ((from === null) && (to === null)) return '~';
                return Ext.Date.format(new Date(from), "H:i") + ' - ' + Ext.Date.format(new Date(to), "H:i");
            };
        }},

        {name: 'action', convert: function (value) {
            switch (value) {
                case 'CHARGE':
                    return lang('Зарядка');
                    break;
                case 'DISCHARGE':
                    return lang('Разрядка');
                    break;
                default:
                    return '~';
            }
        }},

        {name: 'percentRaw', convert: function (value, record) {
                var percent = record.data.percent;
                //if (percent === null) return 0;
                return percent;
            }},

        {name: 'percentReady', convert: function (value, record) {
            var percent = record.data.percent;
            if (percent === null) return '~';
            return percent + ' %';
        }},

        {name: 'capacityFromPercent', convert: function (value) {
            if (value === null) return '~';
            return value + ' мАч';
        }},

        {name: 'dischargeTimeLeftInSeconds', convert: function (value) {
            if (value === null) return '~';
            var date = new Date();
            date.setTime( parseInt(value) + date.getTimezoneOffset() * 60000);
            return Ext.Date.format(date, "H:i:s");
        }},

        {name: 'chargeTimeLeftInSeconds', convert: function (value) {
            if (value === null) return '~';
            var date = new Date();
            date.setTime( parseInt(value) + date.getTimezoneOffset() * 60000);
            return Ext.Date.format(date, "H:i:s");
        }}
    ]
});