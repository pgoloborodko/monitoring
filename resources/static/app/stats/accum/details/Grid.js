Ext.define("Landau.stats.accum.details.Grid", {
    extend: 'Ext.grid.Grid',
    xtype: 'stats-accum-details-grid-view',

    plugins: [{
        type: 'gridexporter'
    }],

    requires: [
        'Landau.stats.accum.details.StatsAccumDetailsStore'
    ],

    store: {
        type: 'stats-accum-details-store',
        //autoLoad: true
    },

    columns: [{
        text: lang('Интервал'),
        dataIndex: 'from_to',
        flex: 1,
        exportStyle: {
            autoFitWidth: true
        }
    }, {
        text: lang('Тип'),
        dataIndex: 'action',
        flex: 1,
        exportStyle: {
            autoFitWidth: true
        }
    }, {
        text: lang('Зарядка/разрядка, %'),
        dataIndex: 'percentReady',
        flex: 1,
        exportStyle: {
            autoFitWidth: true
        }
    }, {
        text: lang('Зарядка/разрядка, мАч'),
        dataIndex: 'capacityFromPercent',
        flex: 1,
        exportStyle: {
            autoFitWidth: true
        }
    }, {
        text: lang('Остаток'),
        dataIndex: 'dischargeTimeLeftInSeconds',
        flex: 1,
        exportStyle: {
            autoFitWidth: true
        }
    }, {
        text: lang('Полная зарядка'),
        dataIndex: 'chargeTimeLeftInSeconds',
        flex: 1,
        exportStyle: {
            autoFitWidth: true
        }
    }]
});