Ext.define("Landau.stats.accum.details.Controller", {
    extend: 'Ext.app.ViewController',
    alias: 'controller.stats-accum-details-controller',

    requires: [
        //'Ext.exporter.text.CSV',
        //'Ext.exporter.text.TSV',
        //'Ext.exporter.text.Html',
        //'Ext.exporter.excel.Xml',
        //'Ext.exporter.excel.Xlsx',
        //'Ext.exporter.excel.PivotXlsx'
    ],

    init: function() {
        var selectedDate = this.getView().getDate();
        this.lookup("dateCurrentDetails").setValue(selectedDate);

        this.lookup("gridDetails").getStore().reload({
            params: {
                device: this.getView().getDevice().id,
                date: Ext.Date.format(new Date(selectedDate), "Y-m-d")
            }
        });

        this.lookup("gridDetails").getStore().on("load", this.storeLoad, this);
    },

    storeLoad: function(contex, records) {
        var data = [];

        records.map(function(item){
            data.push({
                "time": new Date(item.get('from')).getTime(),
                "currentAkbCharge": item.get('currentAkbCharge'),
                "percentRaw": item.get('percentRaw')
            })
        });

        this.lookup("chartDetails").getStore().setData(data)
    },

    doFilter: function () {
        var selectedDate = this.lookup("dateCurrentDetails").getValue();

        this.lookup("gridDetails").getStore().reload({
            params: {
                device: this.getView().getDevice().id,
                date: Ext.Date.format(new Date(selectedDate), "Y-m-d")
            }
        });
    },

    exportDocument: function (menuitem) {

        var currentData = this.lookup("dateCurrentDetails").getValue();
        var deviceName = this.getView().getDevice().name;
        var deviceSeril = this.getView().getDevice().serial;
        var commonCaption = lang('Статистика_аккумулятора_') + deviceName + '_(' + deviceSeril + ')_' + Ext.Date.format(currentData, "Y-m-d")

        var pivotgrid = this.lookup('gridDetails');


        pivotgrid.saveDocumentAs({
            type: 'xlsx',
            title: commonCaption.split('_').join(' '),
            fileName: commonCaption + '.xlsx'
        });
    },

    onError: function (error) {
        Ext.Msg.alert('Error', typeof error === 'string' ? error : 'Unknown error');
    }
});