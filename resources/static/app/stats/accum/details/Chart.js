Ext.define('Landau.stats.accum.details.Chart', {
    extend: 'Ext.chart.CartesianChart',
    xtype: 'stats-accum-details-chart',

    requires: [
        'Landau.stats.accum.details.StatsAccumDetailsStore',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Time',
        'Ext.chart.interactions.CrossZoom'
    ],

    insetPadding: {
        top: 20
    },
    engine: 'Ext.draw.engine.Svg',
    store: {
        fields: ['time', 'currentAkbCharge', 'percentRaw'],
    },
    axes: [{
        type: 'numeric',
        position: 'left',
        //fields: ['currentAkbCharge', 'percentRaw'],
        grid: true,
        //minimum: 0
    }, {
        type: 'time',
        dateFormat: 'H:m',
        position: 'bottom',
        fields: ['time']
    }],

    legend: {
        type: 'sprite',
        position: 'bottom',
        marker: {
            size: 15
        }
    },

    series: [
        {
            type: 'area',
            subStyle: {
                fill: '#ee6e73'
            },
            xField: 'time',
            yField: 'currentAkbCharge',
            title: lang('Расход батареи'),
        },
        {
            type: 'line',
            xField: 'time',
            yField: 'percentRaw',
            subStyle: {
                fill: '#57bbd6'
            },
            title: lang('Потребление устройства'),


            highlightCfg: {
                scale: 0.9
            }
        }
    ]
});