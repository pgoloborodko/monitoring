Ext.define("Landau.stats.accum.details.View", {
    extend: 'Ext.Dialog',
    requires: [
        'Landau.stats.accum.details.Grid',
        'Landau.stats.accum.details.Chart',
        'Landau.stats.accum.details.Controller'
    ],
    config: {
        date: new Date(),
        device: null
    },
    xtype: 'accum-details-view',
    controller: 'stats-accum-details-controller',
    title: lang('Детальная информация'),

    closable: true,
    maximized: true,
    padding: 0,
    height: '100%',

    items: [{
        height: "35%",
        xtype: 'stats-accum-details-chart',
        reference: 'chartDetails'
    }, {
        height: "65%",
        xtype: 'stats-accum-details-grid-view',
        // scrollable: true,
        reference: 'gridDetails'
    }],

    tbar: [{
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'dateCurrentDetails',
        dateFormat: 'd/m/Y',
    }, {
        xtype: 'container',
        html: '&nbsp;&nbsp;'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }, {
        iconCls: 'x-fa fa-file-excel-o',
        text: lang('XLS'),
        handler: 'exportDocument'
    }]
})