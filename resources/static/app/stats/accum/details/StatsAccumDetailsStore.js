Ext.define('Landau.stats.accum.details.StatsAccumDetailsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.stats-accum-details-store',

    model: 'Landau.stats.accum.details.StatsAccumDetailsModel',

    proxy: {
        type: 'ajax',
        url: '/stats/devices/battery/details',
        reader: {
            type: 'json'
        }
    }
});