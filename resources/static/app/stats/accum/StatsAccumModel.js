Ext.define('Landau.stats.accum.StatsAccumModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'startSession', convert: function (value, record) {
            if (value === null) return '~';
            return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        }},

        {name: 'endSession', convert: function (value, record) {
            if (value === null) return '~';
            return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        }},

        {name: 'accumValue', convert: function (value, record) {
            if (value === null) return '~';
            var date = new Date();
            date.setTime( parseInt(value) * 1000 + date.getTimezoneOffset() * 60000);
            return Ext.Date.format(date, "H:i:s");
        }},

        {name: 'chargeTimeLeftInSeconds', convert: function (value, record) {
            if (value === null) return '~';
            var date = new Date();
            date.setTime( parseInt(value) * 1000 + date.getTimezoneOffset() * 60000);
            return Ext.Date.format(date, "H:i:s");
        }},

        {name: 'capacityChargeAtHour', convert: function (value, record) {
            if (value === null) return '~';
            return value;
        }},

        {name: 'percentChargeAtHour', convert: (value) => {
                if (value === null) return '~';
                return value;
        }},

        {name: 'percentDischargeAtHour', convert: (value) => {
                if (value === null) return '~';
                return value;
        }},

        {name: 'capacityDischargeAtHour', convert: (value) => {
                if (value === null) return '~';
                return value;
        }},

        {name: 'dischargeTimeLeftInSeconds', convert: function (value, record) {
                if (value === null) return '~';
                var date = new Date();
                date.setTime( parseInt(value) * 1000 + date.getTimezoneOffset() * 60000);
                return Ext.Date.format(date, "H:i:s");
        }},

        {name: 'lastCharge', convert: function (value, record) {
                return value + "%";
        }},

        {name: 'company', mapping: 'device.company.name'},

        {name: 'deviceName', mapping: 'device.name'},
    ]
});