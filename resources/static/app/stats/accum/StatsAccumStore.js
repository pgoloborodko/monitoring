Ext.define('Landau.stats.accum.StatsAccumStore', {
    extend: 'Ext.data.Store',
    alias: 'store.stats-accum-store',

    model: 'Landau.stats.accum.StatsAccumModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/stats/devices/battery',
        reader: {
            type: 'json'
        }
    }
});