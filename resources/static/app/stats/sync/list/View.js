Ext.define("Landau.stats.sync.list.View", {
    extend: 'Ext.Panel',
    controller: 'stats-sync-list-controller',
    xtype: 'stats-sync-list-view',

    requires: [
        'Landau.stats.sync.list.Controller',
        'Landau.stats.sync.list.ViewGrid',
        'Landau.companies.CompaniesSelect',
        'Landau.users.UsersSelect',
        'Landau.devices.DevicesSelect',
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'stats-sync-grid-view',
            reference: "grid"
        }]
    }],

    tbar: [{
        text: lang('Добавить новую запись'),
        ui: 'action',
        handler: 'save',
        margin: '0 10 0 0'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0',
        width: 120,
    }, {
        margin: '0 10 0 0',
        width: 120,
        xtype: 'devices-select',
        reference: 'device',
    }, {
        xtype: 'users-select',
        reference: 'user',
        margin: '0 10 0 0',
        width: 120,
    }, {
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'extracted',
        dateFormat: 'Y-m-d',
        width: 95
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});