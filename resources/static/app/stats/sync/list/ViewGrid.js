Ext.define("Landau.stats.sync.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'stats-sync-grid-view',

    height: '100%',

    requires: [
        'Landau.events.sync.SyncStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'events-sync-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Регистратор'),
        dataIndex: 'device',
        width: 150
    }, {
        text: lang('Пользователь'),
        dataIndex: 'userPassport',
        flex: 1
    },  {
        text: lang('Дата выдачи'),
        dataIndex: 'extracted',
        width: 150
    },  {
        text: lang('Дата возврата'),
        dataIndex: 'inserted',
        width: 150
    },  {
        text: lang('Компания'),
        dataIndex: 'userCompany',
        width: 100
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit',
            }
        }
    }]
});