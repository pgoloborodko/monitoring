Ext.define("Landau.stats.sync.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.stats-sync-list-controller",

    save: function () {
        this.openForm("Landau.stats.sync.save.View", '/events/users/sync/save', function(form, response) {
            form.lookup("extracted").setValue(Ext.Date.format(new Date(), "Y-m-d H:i:s"));
        });
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.stats.sync.save.View", '/events/users/sync/save?id=' + info.record.id, function(form, response) {
            if(response.inserted !== null) {
                form.lookup("inserted").setValue(Ext.Date.format(new Date(response.inserted), "Y-m-d H:i:s"));
            }

            form.lookup("extracted").setValue(Ext.Date.format(new Date(response.extracted), "Y-m-d H:i:s"));
        });
    },

    doFilter: function () {
        let company = this.lookup("company");
        let inserted = this.lookup("inserted");
        let extracted = this.lookup("extracted");
        let device = this.lookup("device");
        let user = this.lookup("user");

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue(),
                extracted: Ext.Date.format(extracted.getValue(), "Y-m-d"),
                device: device.getValue(),
                user: user.getValue()
            }
        });
    }
});