Ext.define('Landau.stats.sync.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'stats-sync-save-view',
    controller: 'stats-sync-save-controller',
    title: lang('Сохранение выдачи/получения'),

    requires: [
        'Landau.stats.sync.save.Controller',
        'Landau.devices.DevicesSelect',
        'Landau.users.UsersSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Сохранение выдачи/получения'),
            required: true,
            layout: 'vbox',
            reference: 'cont',
            defaults: {
                flex: 1,
                errorTarget: 'side',
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                xtype: 'devices-select',
                name: 'device'
            }, {
                xtype: 'users-select',
                name: 'user'
            }, {
                name: 'extracted',
                reference: 'extracted',
                placeholder: lang('Выдан'),
                required: true,
                inputMask: '9999-99-99 99:99:99'
            }, {
                name: 'inserted',
                reference: 'inserted',
                placeholder: lang('Возвращен'),
                inputMask: '9999-99-99 99:99:99'
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});