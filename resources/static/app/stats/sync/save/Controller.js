Ext.define('Landau.stats.sync.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.stats-sync-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/events/users/sync/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});