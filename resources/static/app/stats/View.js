Ext.define("Landau.stats.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'stats-view',
    controller: 'stats-controller',
    flex: 1,

    requires: [
        'Landau.stats.Controller',
        'Ext.panel.Collapser',
        'Ext.layout.HBox',
        'Ext.data.TreeStore'
    ],

    height: "100%",
    defaultType: 'panel',

    layout: {
        type: 'hbox'
    },

    platformConfig: {
        '!desktop': {
            layout: {
                type: 'vbox'
            }
        }
    },

    items: [{
        border: true,
        header: false,

        items: [{
            platformConfig: {
                '!desktop': {
                    height: 100
                }
            },
            xtype: 'list',
            itemTpl: '{title}',
            reference: 'listStats',
            listeners: {
                itemtap: 'loadPage'
            },
            data: [
                { title: lang('Отчеты по времени работы'), view: 'Landau.stats.uptime.View' },
                { title: lang('Аккумулятор (условные данные)'), view: 'Landau.stats.accum.View' },
                /*{ title: lang('Отображение общей статистики'), view: 'Landau.stats.commonstat.View' },*/
                { title: lang('Отчеты выдачи/получения'), view: 'Landau.stats.sync.list.View' },
                { title: lang('События кнопок'), view: 'Landau.events.button.View' },
                { title: lang('События стримов'), view: 'Landau.events.streams.View' },
                { title: lang('Системные события регистратора'), view: 'Landau.events.system.View' }
            ]
        }]
    }, {
        header: false,
        flex: 4,
        reference: 'container'
    }]
});