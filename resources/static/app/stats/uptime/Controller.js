Ext.define("Landau.stats.uptime.Controller", {
    extend: 'Ext.app.ViewController',
    alias: 'controller.stats-uptime-controller',

    requires: [
        'Ext.exporter.text.CSV',
        'Ext.exporter.text.TSV',
        'Ext.exporter.text.Html',
        'Ext.exporter.excel.Xml',
        'Ext.exporter.excel.Xlsx',
        'Ext.exporter.excel.PivotXlsx'
    ],

    doFilter: function () {
        var dateStart = this.lookup("dateStart").getValue();
        dateStart.setHours(0,0,0,0);

        var dateEnd = this.lookup("dateEnd").getValue();
        dateEnd.setHours(0,0,0,0);

        var company = this.lookup("company");

        this.lookup("grid").getStore().reload({
            params: {
                startSession: Ext.Date.format(dateStart, "Y-m-d"),
                endSession: Ext.Date.format(dateEnd, "Y-m-d"),
                company: company.getValue()
            }
        });
    },

    exportDocument: function (menuitem) {
        var dateStart = this.lookup("dateStart").getValue();
        dateStart.setHours(0,0,0,0);

        var dateEnd = this.lookup("dateEnd").getValue();
        dateEnd.setHours(0,0,0,0);

        var pivotgrid = this.lookup('grid');

        pivotgrid.saveDocumentAs({
            type: 'xlsx',
            title: 'Отчет по времени работы с ' + Ext.Date.format(dateStart, "Y-m-d") + ' по ' + Ext.Date.format(dateEnd, "Y-m-d"),
            fileName: 'uptime_stats_' + Ext.Date.format(dateStart, "Y-m-d") + '-' + Ext.Date.format(dateEnd, "Y-m-d") + '.xlsx'
        });
    },

    onError: function (error) {
        Ext.Msg.alert('Error', typeof error === 'string' ? error : 'Unknown error');
    },
});