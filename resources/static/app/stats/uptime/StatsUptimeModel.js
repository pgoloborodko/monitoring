Ext.define('Landau.stats.uptime.StatsUptimeModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'startSession', convert: function (value, record) {
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        }},
        {name: 'endSession', convert: function (value, record) {
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        }},
        {name: 'uptimeValue', convert: function (value, record) {
            value = parseInt(value);

            let hours = Math.floor(value / 1000 / 3600);
            let minutes =  Math.floor((value - hours * 3600 * 1000) / 1000 / 60);
            let seconds =  Math.floor((value - hours * 3600 * 1000 - minutes * 60 * 1000) / 1000);

            return (hours < 10 ? "0" + hours: hours)
                + ":" + (minutes < 10 ? "0" + minutes : minutes)
                + ":" + (seconds < 10 ? "0" + seconds : seconds);
        }},
        {name: 'company', mapping: 'device.company.name'},
        {name: 'deviceId', mapping: 'device.id'},
        {name: 'device', mapping: 'device.name'}
    ]
});