Ext.define("Landau.stats.uptime.View", {
    extend: 'Ext.Panel',
    xtype: 'stats-uptime-view',
    //controller: 'peoples-controller',

    requires: [
        'Landau.stats.uptime.Grid',
        'Landau.stats.uptime.Controller',
        'Landau.companies.CompaniesSelect'
    ],

    controller: 'stats-uptime-controller',

    height: "100%",
    defaultType: 'panel',

    items: [{
        height: "100%",
        xtype: 'stats-uptime-grid-view',
        reference: 'grid'
    }],

    tbar: [{
        xtype: 'spacer'
    }, {
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'dateStart',
        dateFormat: 'd/m/Y'
    }, {
        xtype: 'container',
        html: '-&nbsp;&nbsp;'
    }, {
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'dateEnd',
        dateFormat: 'd/m/Y'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }, {
        iconCls: 'x-fa fa-file-excel-o',
        text: lang('XLS'),
        handler: 'exportDocument'
    }]
});