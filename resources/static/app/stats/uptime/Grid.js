Ext.define("Landau.stats.uptime.Grid", {
    extend: 'Ext.grid.Grid',
    xtype: 'stats-uptime-grid-view',

    height: '1000',

    plugins: [{
        type: 'gridexporter'
    }],

    requires: [
        'Landau.stats.uptime.StatsUptimeStore',
        'Ext.grid.plugin.Exporter'
    ],

    store: {
        type: 'stats-uptime-store',
        autoLoad: true
    },

    columns: [{
        text: lang('Номер устройства'),
        dataIndex: 'deviceId',
        width: 50,
        exportStyle: {
            autoFitWidth: true,
            width: 50
        }
    }, {
        text: lang('Устройство'),
        dataIndex: 'device',
        flex: 1,
        exportStyle: {
            autoFitWidth: true,
            width: 200
        }
    },{
        text: lang('Компания'),
        dataIndex: 'company',
        flex: 1,
        exportStyle: {
            autoFitWidth: true,
            width: 350
        }
    }, {
        text: lang('Время первого запуска'),
        dataIndex: 'startSession',
        width: 200,
        exportStyle: {
            autoFitWidth: true,
            width: 300
        }
    },  {
        text: lang('Время последнего отклика'),
        dataIndex: 'endSession',
        width: 200,
        exportStyle: {
            autoFitWidth: true,
            width: 300
        }
    }, {
        text: lang('Общее время работы'),
        dataIndex: 'uptimeValue',
        width: 200,
        exportStyle: {
            autoFitWidth: true,
            width: 250
        }
    }, {
        text: lang('Подключений'),
        dataIndex: 'connectionCount',
        width: 120,
        exportStyle: {
            autoFitWidth: true,
            width: 150
        }
    }]
});