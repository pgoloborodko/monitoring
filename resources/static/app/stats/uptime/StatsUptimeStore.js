Ext.define('Landau.stats.uptime.StatsUptimeStore', {
    extend: 'Ext.data.Store',
    alias: 'store.stats-uptime-store',

    model: 'Landau.stats.uptime.StatsUptimeModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/stats/devices/uptime',
        reader: {
            type: 'json'
        }
    }
});