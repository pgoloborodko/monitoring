Ext.define('Landau.profiles.ProfilesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.profiles-store',

    model: 'Landau.profiles.ProfilesModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/profiles',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});