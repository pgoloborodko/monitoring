Ext.define('Landau.profiles.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'profiles-save-view',
    controller: 'profiles-save-controller',
    title: lang('Сохранение биометрического профиля'),

    requires: [
        'Landau.companies.CompaniesSelect',
        'Landau.profiles.save.Controller'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'container',
            layout: 'hbox',
            flex: 3,
            items: [{
                xtype: 'container',
                layout: 'vbox',
                margin: '0 10 0 0',
                items: [{
                    html: lang('ФОТО'),
                    layout: 'center'
                }, {
                    xtype: 'container',
                    layout: 'hbox',
                    margin: '0 0 10 0',
                    items: [{
                        flex: 1,
                        xtype:'filefield',
                        label: '',
                        name: 'photoFile',
                        listeners: {
                            change: 'changeImage'
                        },
                        reference: 'photoFile'
                    },{
                        xtype:'hiddenfield',
                        name: 'photoFileHidden',
                        reference: 'photoFileHidden'
                    },{
                        margin: '0 0 0 10',
                        xtype: 'button',
                        text: 'webcam',
                        handler: 'fromWebCam',
                        ui: 'action'
                    }]
                }, {
                    xtype: 'img',
                    height: 170,
                    width: 170,
                    layout: 'center',
                    reference: 'photo'
                }]
            },{
                flex: 1,
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    xtype: 'hiddenfield',
                    name: 'id'
                }, {
                    xtype: 'containerfield',
                    label: lang('ФИО'),
                    required: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        name: 'lastName',
                        placeholder: lang('Фамилия'),
                        required: true
                    }, {
                        name: 'firstName',
                        placeholder: lang('Имя'),
                        required: true,
                        margin: '0 5'
                    }, {
                        name: 'secondName',
                        placeholder: lang('Отчество'),
                        margin: '0 5'
                    }, {
                        name: 'age',
                        xtype: 'numberfield',
                        placeholder: lang('Возраст'),
                        width: 50
                    }]
                }, {
                    xtype: 'containerfield',
                    label: lang('Паспорт'),
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        flex: null,
                        name: 'passportNum',
                        placeholder: lang('Номер'),
                        inputMask:'9999',
                        width: 100
                    }, {
                        flex: null,
                        name: 'passportSeries',
                        placeholder: lang('Серия'),
                        margin: '0 5',
                        inputMask:'999999',
                        width: 100
                    }, {
                        name: 'passportIssue',
                        placeholder: lang('Выдан'),
                    }]
                },{
                    xtype: 'container',
                    layout: 'hbox',
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'containerfield',
                        label: lang('Тип аккаунта'),
                        margin: '0 5 0 0',
                        required: true,
                        defaults: {
                            flex: 1
                        },
                        items: [{
                            xtype: 'selectfield',
                            name: 'type',
                            required: true,
                            placeholder: lang('Выберите тип человека...'),
                            options: [{
                                text: lang('Обычный'),
                                value: 'USUAL'
                            }, {
                                text: lang('Разыскиваемый'),
                                value: 'CRIMINAL'
                            }]
                        }]
                    },{
                        xtype: 'containerfield',
                        label: lang('Компания'),
                        required: true,
                        defaults: {
                            flex: 1
                        },
                        items: [{
                            xtype: 'companies-select',
                            name: 'company',
                            reference: 'company'
                        }]
                    }]
                }]
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});