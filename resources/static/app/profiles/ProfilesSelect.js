Ext.define('Landau.profiles.ProfilesSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'profiles-select',
    requires: [
        'Landau.profiles.list.View'
    ],

    config: {
        title: lang('Выбор биометрического профиля'),
        placeholder: lang('Профиль...'),
        view: 'profiles-list-view',
        displayTpl: '{fio}',
    }
});