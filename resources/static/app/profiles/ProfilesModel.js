Ext.define('Landau.profiles.ProfilesModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'phone',
        'firstName',
        'secondName',
        'lastName',
        'driverA',
        'driverB',
        'driverC',
        'driverD',
        'templateExists',
        {name: 'fio', convert: function (value, record) {
                var lastName = !record.get('lastName')? "" : record.get('lastName')
                var firstName = !record.get('firstName')? "" : record.get('firstName');
                var secondName = !record.get('secondName')? "" : record.get('secondName');

                return lastName + " " +  firstName + " " + secondName;
            },
            depends: [ 'firstName',  'secondName',  'lastName']
        },
        {
            name: 'type', convert: function (value, record) {
                switch (value) {
                    case "USUAL":
                        return lang("Обычный");
                    case "CRIMINAL":
                        return lang("Разыскиваемый");
                }
            }
        },
        {name: 'device', mapping: 'device.name'},
        {name: 'company', mapping: 'company.name'},
        {name: 'city', mapping: 'city.name'},
        {name: 'district', mapping: 'district.name'}
    ]
});