Ext.define("Landau.profiles.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'profiles-grid-view',
    excludeTest: true,

    height: '100%',

    requires: [
        'Landau.profiles.ProfilesStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'profiles-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('ФИО'),
        dataIndex: 'fio',
        flex: 1,
        sorter: {
            property: ['lastName', 'firstName', 'secondName']
        }
    }, {
        text: lang('Тип'),
        dataIndex: 'type',
        width: 100
    },  {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 100
    }, {
        text: lang('Действия'),
        width: 'auto',
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit',
            }
        }
    }]
});