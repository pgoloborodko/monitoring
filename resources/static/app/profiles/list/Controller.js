Ext.define("Landau.profiles.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.profiles-controller",

    save: function () {
        this.openForm("Landau.profiles.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.profiles.save.View", '/profiles/save?id=' + info.record.id,  function (form, response) {
            console.log(form);

            if(response.photo !== null) {
                form.lookup('photo').setSrc('/avatars/' + response.photo);
            }
        });
    },

    doFilter: function () {
        var firstName = this.lookup("firstName");
        var secondName = this.lookup("secondName");
        var lastName = this.lookup("lastName");

        var company = this.lookup("company");

        this.lookup("grid").getStore().reload({
            params: {
                firstName: firstName.getValue(),
                secondName: secondName.getValue(),
                lastName: lastName.getValue(),
                company: company.getValue()
            }
        });
    }
});