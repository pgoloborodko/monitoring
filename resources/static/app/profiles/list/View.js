Ext.define("Landau.profiles.list.View", {
    extend: 'Ext.Panel',
    controller: 'profiles-controller',
    xtype: 'profiles-list-view',

    requires: [
        'Landau.profiles.list.Controller',
        'Landau.profiles.list.ViewGrid',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'profiles-grid-view',
            reference: "grid"
        }]
    }],

    tbar: [{
        text: lang('Добавить новый профиль'),
        ui: 'action',
        handler: 'save',
        margin: '0 10 0 0'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'lastName',
        placeholder: lang('Фамилия'),
        margin: '0 10 0 0'
    }, {
        xtype: 'textfield',
        reference: 'firstName',
        placeholder: lang('Имя'),
        margin: '0 10 0 0'
    }, {
        xtype: 'textfield',
        reference: 'secondName',
        placeholder: lang('Отчество'),
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        xtype: 'districts-select',
        reference: 'district',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});