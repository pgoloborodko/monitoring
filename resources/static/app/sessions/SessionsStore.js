Ext.define('Landau.sessions.SessionsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.sessions-store',

    model: 'Landau.sessions.SessionsModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/users/sessions',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});