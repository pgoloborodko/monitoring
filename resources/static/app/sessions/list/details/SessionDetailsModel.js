Ext.define('Landau.sessions.list.details.SessionDetailsModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'description', convert: function (value, record) {
                var tempArr = [];
                var apiMethod = '';
                var apiMethodArr = [];

                if(!!value) {
                    // отбрасываем последнюю часть строки после точки и добавляем метод
                    tempArr = value.split('.');

                    apiMethod = tempArr[tempArr.length - 1];
                    apiMethodArr = apiMethod.split(': ');
                    apiMethod = apiMethodArr[apiMethodArr.length - 1];

                    tempArr.pop();
                    tempArr.shift();
                }

                return lang(tempArr.join('/') + ': ' + apiMethod)
            }
        },
        // {name: 'description', mapping: 'sessionLogs'},
/*
        {
            name: 'description', mapping: 'sessionLogs', convert: function (value, record) {
                console.log('++++++++++++++++++')
                console.log(value)
                console.log(record)
                //if ((value === null) || (value === undefined)) return '~';
                //if ((value.name === null) || (value.name === undefined)) return '~';
                return value
            }
        }

    */
        {
            name: 'date', convert: function (value) {
                if ((value === null) || (value === undefined)) return '~';
                return Ext.Date.format(new Date(value), "H:i:s");
            }
        }
    ]
});