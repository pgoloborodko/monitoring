Ext.define('Landau.sessions.list.details.SessionDetailsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.sessions-list-details-store',

    model: 'Landau.sessions.list.details.SessionDetailsModel',

    proxy: {
        type: 'ajax',
        url: '/sessions/session',
        //url: 'sessions/session_log?session=302&date=2019-09-24',
        //url: '/sessions/session_log',
        reader: {
            type: 'json',
            rootProperty: 'sessionLogs'
        }
    }

    //sessions/session_log?session=302&date=2019-09-24

});