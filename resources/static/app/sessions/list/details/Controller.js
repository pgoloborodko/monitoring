Ext.define("Landau.sessions.list.details.Controller", {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sessions-list-details-controller',

    requires: [
        //'Ext.exporter.text.CSV',
        //'Ext.exporter.text.TSV',
        //'Ext.exporter.text.Html',
        //'Ext.exporter.excel.Xml',
        //'Ext.exporter.excel.Xlsx',
        //'Ext.exporter.excel.PivotXlsx'
    ],

    init: function() {
        var selectedDate = this.getView().getDate();
        //this.lookup("dateCurrentDetails").setValue(selectedDate);
        var that = this;


        this.lookup("gridDetails").getStore().reload({
            params: {
                id: this.getView().getSessionId(), //this.getView().getId(),
                //session: '302',
                date: Ext.Date.format(new Date(selectedDate), "Y-m-d")

                //sessions/session_log?session=302&date=2019-09-24
            }
        });


        // обращаюсь к контроллеру второй раз :(
        Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});

        Ext.Ajax.request({
            url: 'system/sessions/getSession?id=' + that.getView().getSessionId() + '&date=' + Ext.Date.format(new Date(selectedDate), "Y-m-d"),

            success: function(response, opts) {
                Ext.Viewport.setMasked(false);
                //console.log(JSON.parse(response.responseText));
                that.storeLoad(null, [JSON.parse(response.responseText)])
            },

            failure: function(response, opts) {
                Ext.Viewport.setMasked(false);
                console.log(response)
            }
        });


        //console.log('+++++++++++++++++++++')
        //console.dir(Ext.user)

        //this.lookup("gridMain").getStore().on("load", this.storeLoad, this);
    },
/*
    init: function() {
        let selectedDate = this.getView().getDate();
        this.lookup("dateCurrentDetails").setValue(selectedDate);

        this.lookup("gridDetails").getStore().reload({
            params: {
                device: this.getView().getDevice().id,
                date: Ext.Date.format(new Date(selectedDate), "Y-m-d")
            }
        });

        this.lookup("gridDetails").getStore().on("load", this.storeLoad, this);
    },
*/
    storeLoad: function(contex, records) {
        //console.log('+++++++++++++++++++++');
        //console.log(records[0].data);

        var userId = records[0].data.user.id;

        var userName = records[0].data.user.username;
        if (userName === null || userName === undefined) userName = '~';

        var startSession = records[0].data.startSession;
        if (startSession === null || startSession === undefined) {startSession = '~'}
        else startSession = Ext.Date.format(new Date(startSession), "Y-m-d");

        var endSession = records[0].data.endSession;
        if (endSession === null || endSession === undefined) {
            this.lookup("closeSessionBtn").setHidden(false);
        }
        else {
            endSession = Ext.Date.format(new Date(endSession), "Y-m-d");
            this.lookup("endSession").setHtml(lang('Конец сессии') + ': ' + endSession);
            this.lookup("endSession").setHidden(false);
        }

        // если своя сессия, то дисаблим кнопку
        this.lookup("closeSessionBtn").setDisabled(records[0].data.current);
        this.lookup("userName").setHtml(lang('Пользователь') + ': ' + userName + ' (id: ' + userId + ')');
        this.lookup("startSession").setHtml(lang('Начало сессии') + ': ' + startSession);
    },

    closeSession: function() {
        var sessionId = this.getView().getSessionId();
        var that = this;

        Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});

        Ext.Ajax.request({
            url: '/sessions/close',
            params: {
                id: sessionId
            },

            success: function(response, opts) {
                Ext.Viewport.setMasked(false);

                if (JSON.parse(response.responseText) === 'Сессия не найдена') {
                    var dialog = Ext.create({
                        xtype: 'dialog',
                        title: lang('Внимание!'),

                        maximizable: false,
                        html: lang('Сессия не найдена'),

                        buttons: {
                            ok: function () {  // standard button (see below)
                                dialog.destroy();
                            }
                        }
                    });

                    dialog.show();
                } else {
                    // Перезагружаем страницу с подробной информацией
                    that.getView().destroy();

                    var dialog = Ext.create("Landau.sessions.list.details.View", {
                        parentController: "Landau.sessions.list.Controller",
                        sessionId: sessionId
                    });

                    dialog.show();
                }
            },

            failure: function(response, opts) {
                Ext.Viewport.setMasked(false);
                //console.log('server-side failure with status code ' + response.status);
                //console.log(response);

                var dialog = Ext.create({
                    xtype: 'dialog',
                    title: lang('Внимание!'),

                    maximizable: false,
                    html: lang('Сбой на стороне сервера! Код ошибки:') + ' ' + response.status,

                    buttons: {
                        ok: function () {  // standard button (see below)
                            dialog.destroy();
                        }
                    }
                });

                dialog.show();
            }
        });
    },
/*
    doFilter: function () {
        let selectedDate = this.lookup("dateCurrentDetails").getValue();

        this.lookup("gridDetails").getStore().reload({
            params: {
                device: this.getView().getDevice().id,
                date: Ext.Date.format(new Date(selectedDate), "Y-m-d")
            }
        });
    },
    */

    exportDocument: function (menuitem) {
        var pivotgrid = this.lookup('gridDetails');
        var id = this.getView().getSessionId();
        var userName = this.lookup("userName").getHtml();
        var userId = this.lookup("userName").getHtml();

        pivotgrid.saveDocumentAs({
            type: 'xlsx',
            title: lang("Сессия") + " id: " +  id + '. ' + userName,
            fileName: lang("Сессия") + "_id_" +  id + '.xlsx'
        });

    },

    onError: function (error) {
        Ext.Msg.alert('Error', typeof error === 'string' ? error : 'Unknown error');
    }
});