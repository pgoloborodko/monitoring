Ext.define("Landau.sessions.list.details.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'sessions-list-details-grid-view',

    plugins: [{
        type: 'gridexporter'
    }],

    requires: [
        'Landau.sessions.list.details.SessionDetailsStore'
    ],

    store: {
        type: 'sessions-list-details-store',
        //autoLoad: true
    },

    columns: [/*{
        text: '#',
        dataIndex: 'id',
        flex: 1
    }, */{
        text: lang('Время'),
        dataIndex: 'date',
        flex: 1
    }, {
        text: lang('Описание'),
        dataIndex: 'description',
        flex: 1
    }
    ]

});