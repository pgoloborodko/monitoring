Ext.define("Landau.sessions.list.details.View", {
    extend: 'Ext.Dialog',
    requires: [
        'Landau.sessions.list.details.ViewGrid',
        'Landau.sessions.list.details.Controller',
        'Landau.sessions.list.details.SessionDetailsStore'
    ],
    config: {
        date: new Date(),
        device: null,
        sessionId: null
    },
    xtype: 'sessions-details-view',
    controller: 'sessions-list-details-controller',
    title: lang('Детальная информация'),

    closable: true,
    maximized: true,
    padding: 0,
    height: '100%',

    store: {
        type: 'sessions-list-details-store'
    },

    items: [{
        height: "100%",
        xtype: 'sessions-list-details-grid-view',
        reference: 'gridDetails'
    }],

    tbar: [/*{
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(), //this.getDate(),
        margin: '0 10 0 0',
        reference: 'dateCurrentDetails',
        dateFormat: 'd/m/Y',
    }, {
        xtype: 'container',
        html: '&nbsp;&nbsp;'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Фильтр'),
        handler: 'doFilter'
    },*/
    {
        xtype: 'container',
        html: '',
        reference: 'userName',
        margin: '0 10 0 0'
    }, {
        xtype: 'container',
        html: '',
        reference: 'startSession',
        margin: '0 10 0 0'
    }, {
        xtype: 'container',
        html: '',
        reference: 'endSession',
        margin: '0 10 0 0',
        hidden: true
    }, {
        text: lang('Закрыть сессию'),
        handler: 'closeSession',
        reference: "closeSessionBtn",
        margin: '0 10 0 0',
        hidden: true
    }, {
        iconCls: 'x-fa fa-file-excel-o',
        text: lang('XLS'),
        handler: 'exportDocument'
    }]
});