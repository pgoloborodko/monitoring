Ext.define("Landau.sessions.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.sessions-controller",

    /*init: function() {
        var store = Ext.create('Landau.sessions.SessionsStore');

        store.load({

            params: {
                id: this.getView().getIdGeozone()
            },

            callback: function(records, operation, success) {
                success ? console.log(records) : console.log('NO Rec!!!!!!!')
            },
            scope: this
        });
    }, */



    onDetail: function(grid, info) {
        //console.log('===========================');
        //console.log(info.record.get('id'))
        var dialog = Ext.create("Landau.sessions.list.details.View", {
            parentController: this,
            //date: this.lookup("dateCurrent").getValue(),
            sessionId: info.record.get('id')
            //device: info.record.get('device')
        });

        dialog.show();
    },

    doFilter: function () {
        var company = this.lookup("company");
        var dateCurrent = this.lookup("dateCurrent").getValue();
        console.log(dateCurrent);

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue(),
                startSession: Ext.Date.format(dateCurrent, "Y-m-d")
            }
        });
    },
});