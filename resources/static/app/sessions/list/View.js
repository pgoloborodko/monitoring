Ext.define("Landau.sessions.list.View", {
    extend: 'Ext.Panel',
    controller: 'sessions-controller',

    requires: [
        'Landau.sessions.list.Controller',
        'Landau.sessions.list.ViewGrid',
        'Landau.companies.CompaniesSelect'
    ],


    height: "100%",
    defaultType: 'panel',

    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'sessions-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        xtype: 'spacer'
    }, {
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'dateCurrent',
        dateFormat: 'd/m/Y',
        width: 100,
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});