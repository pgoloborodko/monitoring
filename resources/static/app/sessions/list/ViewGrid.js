Ext.define("Landau.sessions.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'sessions-grid-view',

    height: '100%',

    requires: [
        'Landau.sessions.SessionsStore',
        'Landau.common.Pageable',
        'Landau.sessions.list.Controller'
    ],

    store: {
        type: 'sessions-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        flex: 1
    }, {
        text: lang('Пользователь'),
        dataIndex: 'user',
        flex: 1
    }, {
        text: lang('Id пользователя'),
        dataIndex: 'userId',
        flex: 1
    }, {
        text: lang('Начало сессии'),
        dataIndex: 'startSession',
        flex: 1
    }, {
        text: lang('Завершение сессии'),
        dataIndex: 'endSession',
        flex: 1
    }, {
        text: lang('Статус'),
        dataIndex: 'online',
        flex: 1
    }, {
        text: lang('Детальная информация'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: [{
                type: 'maximize',
                handler: 'onDetail',
                tooltip: lang('Детальная информация')
            }]
        }
    }]
});