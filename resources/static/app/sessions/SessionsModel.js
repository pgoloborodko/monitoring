Ext.define('Landau.sessions.SessionsModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'company', convert: function (value) {
                if ((value === null) || (value === undefined)) return '~';
                if ((value.name === null) || (value.name === undefined)) return '~';
                return value.name
            }
        }, {
            name: 'user', convert: function (value) {
                if ((value === null) || (value === undefined)) return '~';
                if ((value.username === null) || (value.username === undefined)) return '~';
                return value.username
            }
        }, {
            name: 'userId', mapping: 'user.id'
        }, {
            name: 'startSession', convert: function (value) {
                if ((value === null) || (value === undefined)) return '~';
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }
        }, {
            name: 'endSession', convert: function (value) {
                if ((value === null) || (value === undefined)) return '~';
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }
        }, {
            name: 'online', convert: function (value) {
                if ((value === null) || (value === undefined)) return '~';
                if (value) return lang('В сети');
                return lang('Нет в сети')
            }
        }
    ]
});
