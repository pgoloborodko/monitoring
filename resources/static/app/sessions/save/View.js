Ext.define('Landau.sessions.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'sessions-save-view',
    controller: 'sessions-save-controller',
    title: lang('Сохранение стрим-сервера'),

    requires: [
        'Landau.sessions.save.Controller',
        'Landau.companies.CompaniesSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Параметры сервера'),
            required: true,
            layout: 'vbox',
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                name: 'name',
                placeholder: lang('Название'),
                required: true
            }, {
                name: 'rtmpUrl',
                placeholder: lang('Адрес RTMP'),
                required: true
            },{
                name: 'httpUrl',
                placeholder: lang('Адрес HTTP'),
                required: true
            }, {
                name: 'sldpUrl',
                placeholder: lang('Адрес SLDP'),
                required: true
            }, {
                name: 'hlsUrl',
                placeholder: lang('Адрес HLS'),
                required: true
            }, {
                name: 'maxCPU',
                placeholder: lang('Max CPU'),
                required: true
            }, {
                name: 'maxIncomingStream',
                placeholder: lang('Макс входящий поток'),
                required: true
            }, {
                name: 'maxOutgoingStream',
                placeholder: lang('Макс исходящий поток'),
                required: true
            }, {
                name: 'maxDevices',
                placeholder: lang('Макс подключений'),
                required: true
            },{
                name: 'token',
                placeholder: lang('token'),
                required: true
            }, {
                xtype: 'companies-select',
                required: true,
                name: 'company',
            }, {
                name: 'state',
                boxLabel: lang('Активен'),
                required: true,
                xtype: 'checkbox',
                value: true
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});