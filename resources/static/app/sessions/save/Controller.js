Ext.define('Landau.sessions.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sessions-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/sessions/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});