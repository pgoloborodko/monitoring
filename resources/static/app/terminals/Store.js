Ext.define('Landau.terminals.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.terminals-store',

    model: 'Landau.terminals.Model',
    remoteSort: true,

    sorters: [{
        property: 'lastOnline',
        direction: 'ASC'
    }],

    proxy: {
        type: 'ajax',
        url: '/terminals',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});