Ext.define('Landau.terminals.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.terminals-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/terminals/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});