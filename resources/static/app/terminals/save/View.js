Ext.define('Landau.terminals.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'terminals-save-view',
    controller: 'terminals-save-controller',
    title: lang('Сохранение терминала'),

    requires: [
        'Landau.terminals.save.Controller',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        },{
            xtype: 'containerfield',
            label: lang('Название и компания'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'name',
                placeholder: lang('Имя'),
                required: true,
                margin: '0 10 0 0'
            }, {
                xtype: 'companies-select',
                name: 'company',
                required: true,
                reference: 'company'
            }]
        },{
            xtype: 'containerfield',
            label: lang('Город и район'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'cities-select',
                name: 'city',
                required: true,
                reference: 'city',
                margin: '0 10 0 0'
            }, {
                xtype: 'districts-select',
                name: 'district',
                required: true,
                reference: 'district'
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});