Ext.define("Landau.terminals.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.terminals-controller",

    save: function () {
        this.openForm("Landau.terminals.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.terminals.save.View", '/terminals/save?id=' + info.record.id);
    },

    onView: function (grid, info) {
        var dialog = Ext.create("Landau.terminals.view.View", {
            parentController: this,
            terminal: info.record,
            items: [{
                xtype: 'container',
                style: 'height: 100%',
                height: '100%',
                padding: 0,
                cls: 'iframe-terminal',
                html: "<iframe src='/terminals/request/" + info.record.id + "/app/index.html' style='width: 100%; height: 100%; border:0'></iframe>"
            }]
        });

        dialog.show();
    },

    doFilter: function () {
        var serial = this.lookup("serial");
        var name = this.lookup("name");
        var company = this.lookup("company");
        var city = this.lookup("city");
        var district = this.lookup("district");

        this.lookup("grid").getStore().reload({
            params: {
                name: name.getValue(),
                serial: serial.getValue(),
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue()
            }
        });
    }
});