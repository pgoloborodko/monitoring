Ext.define("Landau.terminals.list.View", {
    extend: 'Ext.Panel',
    controller: 'terminals-controller',
    xtype: 'terminals-list-view',

    requires: [
        'Landau.terminals.list.Controller',
        'Landau.terminals.list.ViewGrid',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        height: "100%",
        xtype: 'terminals-grid-view',
        reference: "grid"
    }],

    tbar: [{
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'serial',
        placeholder: 'Серийный',
        margin: '0 10 0 0'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: 'Название',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        xtype: 'districts-select',
        reference: 'district',
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});