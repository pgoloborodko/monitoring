Ext.define("Landau.terminals.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'terminals-grid-view',

    height: '100%',

    requires: [
        'Landau.terminals.Store',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'terminals-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        dataIndex: 'state',
        width: 50,
        cell: {
            encodeHtml: false
        }
    }, {
        text: lang('Серийный номер'),
        dataIndex: 'serial',
        width: 'auto'
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        width: 'auto'
    }, {
        text: lang('В сети'),
        dataIndex: 'lastOnline',
        width: 150
    }, {
        text: lang('Хранилище'),
        dataIndex: 'lastOnline',
        width: 150
    }, {
        text: lang('Город'),
        dataIndex: 'city',
        width: 150
    }, {
        text: lang('Район'),
        dataIndex: 'district',
        width: 150
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 170
    }, {
        text: lang('Всего/свободно/занято'),
        dataIndex: 'space',
        width: 'auto',
        sorter: {
            property: ['allSpace', 'freeSpace', 'holdSpace']
        }
    }, {
        text: lang('Синхронизируются/заряжаются/в работе/готовы'),
        dataIndex: 'devices',
        width: 'auto',
        sorter: {
            property: ['syncDevices', 'chargeDevices', 'extractedDevices', 'okDevices']
        }
    }, {
        text: lang('Действия'),
        width: 'auto',
        locked: true,
        sortable: false,
        menuDisabled: true,

        cell: {
            tools: [{
                type: 'gear',
                handler: 'onEdit',
                tooltip: lang('Редактировать')
            }]
        }
    }]
});