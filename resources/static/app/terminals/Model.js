Ext.define('Landau.terminals.Model', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id', mapping: 'id'},
        {name: 'serial', mapping: 'serial'},
        {name: 'city', mapping: 'city.name'},
        {name: 'district', mapping: 'district.name'},
        {name: 'lastOnline', convert: function (value, record) {
            return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        }},
        {name: 'state', convert: function (value, record) {
                if(value == undefined &&  record.data.online || value) {
                    return "<span class='devices-status' style='background:#73b51e'></span>";
                }
                return "<span class='devices-status' style='background:#cf4c35'></span>";
        }},
        {name: 'company', mapping: 'company.name'},
        {name: 'space', convert: function (value, record) {
                return (record.data.allSpace/1024/1024/1024).toFixed(1)
                    + '/'
                    + (record.data.freeSpace/1024/1024/1024).toFixed(1)
                    + '/'
                    + (record.data.holdSpace/1024/1024/1024).toFixed(1);
        }},
        {name: 'devices', convert: function (value, record) {

                let nullConvert = (val) => {
                    if (val != null) return val
                    return '~'
                }

                return nullConvert(record.data.syncDevices)
                    + '/'
                    + nullConvert(record.data.chargeDevices)
                    + '/'
                    + nullConvert(record.data.extractedDevices)
                    + '/'
                    + nullConvert(record.data.okDevices);
        }}
    ]
});