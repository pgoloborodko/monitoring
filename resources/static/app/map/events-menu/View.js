Ext.define("Landau.map.events-menu.View", {
    extend: 'Ext.Panel',
    xtype: 'map-events-menu-view',
    controller: 'map-events-menu-controller',

    config: {
        type: "EVENTS"
    },

    requires: [
        'Ext.panel.Collapser',
        'Landau.map.events-menu.Controller',
        'Landau.events.EventStore'
    ],

    height: '100%',

    border: true,

    title: 'Events',
    flex: false,

    collapsible: {
        collapsed: false,
        direction: 'left'
    },

    items: [{
        xtype: 'list',
        store:{
            autoLoad: true,
            type: 'event-store',
            proxy: {
                extraParams : {
                    created : Ext.Date.format(new Date(), "Y-m-d")
                }
            }
        },
        itemTpl: [
            '<div>{type} {device}</div>'
        ],
        height: '100%',
    }],


    bbar: {
        arrowAlign: 'top',
        xtype: 'button',
        menuAlign: 'b',
        text: 'События',
        modal: true,
        width: '100%',
        menu: [{
            text: 'События',
            handler: 'setType'
        }, {
            text: 'Плейлист',
            handler: 'setType'
        }, {
            text: 'Текст',
            handler: 'setType'
        }]
    }
});