Ext.define("Landau.map.users-menu.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.map-users-menu-controller",
    selected: [],

    listeners: {
        connectDevice: 'connectDevice',
        disconnectDevice: 'connectDevice',
        message: 'message',
    },

    init: function() {
        Messages.bind({
            listen: "/user/users/status",
            method: this.connectDisconnect.bind(this)
        });

        Messages.bind({
            listen: "/user/users/updates",
            method: this.updateUsers.bind(this)
        });

        Messages.onConnect(function () {
            this.lookup("grid").getStore().reload();
        }.bind(this));


        this.lookup("grid").getStore().on({
            single: true,
            load: function(context, records) {
                for (let i = 0; i < records.length; i++) {
                    let index = this.selected.indexOf(records[i].data.user.id);

                    if(index >= 0) {
                        this.fireViewEvent("addUser", records[i].data.type === "СТРИМ" ? "STREAM" : "ARCHIVE", records[i].data.user);
                    }
                }
            }.bind(this)
        });

        this.lookup("grid").getStore().on({
            load: function(context, records) {
                for (let i = 0; i < records.length; i++) {
                    let index = this.selected.indexOf(records[i].data.user.id);

                    if(index >= 0) {
                        records[i].set("checked", true);
                    }
                }
            }.bind(this)
        });

        if(this.hasHashValue("datetime")) {
            this.getView().setDate(new Date(parseInt(this.getHashValue("datetime"))));
        }
        else {
            this.getView().setDate(new Date());
        }
    },

    updateUsers: function (user) {
        this.lookup("grid").refresh();
    },

    connectDisconnect: function (user) {
        let items = this.lookup("grid").getStore().getData();

        for (let i = 0; i < items.length; i++) {

            if(user.device.id === items.getAt(i).data.user.device.id) {
                let userTable = items.getAt(i).get("user"); //, device.online);
                userTable.device.online = user.device.online;

                items.getAt(i).set("user", userTable);
                items.getAt(i).set("online", userTable.device.online);
            }
        }

        this.lookup("grid").refresh();
    },

    rowclick: function (scope, row) {
        if(row.cell.dataIndex !== "checked" && (row.record.get("checked") === undefined || row.record.get("checked") === false)) {
            row.record.set("checked", true);
            this.selected.push(row.record.data.user.id);

            this.fireViewEvent("addUser",  lang(row.record.data.type) ===  lang("СТРИМ") ? "STREAM" : "ARCHIVE", row.record.data.user);
        }

        this.fireViewEvent("locateUser",  lang(row.record.data.type) ===  lang("СТРИМ") ? "STREAM" : "ARCHIVE", row.record.data.user);
    },

    checkchange: function(that, rowIndex, checked, record, e, eOpts) {
        if(checked) {
            this.fireViewEvent("addUser",  lang(record.data.type) ===  lang("СТРИМ") ? "STREAM" : "ARCHIVE", record.data.user);

            this.selected.push(record.data.user.id);
        }
        else {
            this.fireViewEvent("removeUser",  lang(record.data.type) ===  lang("СТРИМ") ? "STREAM" : "ARCHIVE", record.data.user);

            for(let i=0; i < this.selected.length; i++) {
                if (this.selected[i] === record.data.user.id) {
                    this.selected.splice(i, 1);
                }
            }
        }
    }
});