Ext.define("Landau.map.users-menu.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'map-users-menu-view',
    controller: 'map-users-menu-controller',

    config: {
        date: new Date(),
        type: 'MAP',
        selected: []
    },

    requires: [
        'Ext.panel.Collapser',
        'Landau.map.users-menu.Controller',
        'Landau.map.users-menu.FilterUserStore'
    ],

    title: {
        text:lang('Пользователи')
    },
    border: true,
    flex: false,
    collapsible: {
        collapsed: false,
        direction: 'left'
    },

    platformConfig: {
        '!desktop': {
            cls: 'mobile-config',
            collapsible: {
                collapsed: true
            }
        }
    },

    items: [{
        height: '100%',
        xtype: 'grid',
        scope: this,
        reference: "grid",
        hideHeaders: true,
        store: {
            type: 'map-users-menu-filteruserstore',
            autoLoad: true
        },
        listeners: {
            childtap: 'rowclick'
        },
        columns: [{
            xtype:'checkcolumn',
            width: 30,
            dataIndex: 'checked',
            sortable: false,
            menuDisabled: true,
            scope: this,
            listeners: {
                checkchange: 'checkchange'
            }
        }, {
            text: lang('Имя'),
            dataIndex: 'userPassport',
            flex: 1,
            renderer: function(value, record, dataIndex, cell) {
                if(lang(record.data.type) === lang("СТРИМ"))  {
                    cell.row.setCls(record.data.user.device.online ? 'online' : 'offline');
                }
                else {
                    cell.row.setCls("");
                }

                return value;
            }
        }]
    }],

    setDate: function(date) {
        this.callParent([date]);

        this.lookup("grid").getStore().load({
            params: {
                date: Ext.Date.format(date, "Y-m-d")
            }
        })
    },

    setType: function (type) {
        this.callParent([type]);

        if(type === "MAP") {
            this.lookup("grid").getStore().removeFilter('type');
        }
        else if(type === "STREAM") {
            this.lookup("grid").getStore().filter('type', lang("СТРИМ"));
        }
        else if(type === "ARCHIVE") {
            this.lookup("grid").getStore().filter('type', lang("АРХИВ"));
        }
    },

    setSelected: function (selected) {
        this.getController().selected = selected;
    }
});