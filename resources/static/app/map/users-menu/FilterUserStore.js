Ext.define('Landau.map.users-menu.FilterUserStore', {
    alias: 'store.map-users-menu-filteruserstore',
    extend: 'Ext.data.Store',

    model: 'Landau.map.users-menu.FilterUserModel',
    groupField: 'type',
    groupDir: 'DESC',

    sorters: [{
        property: 'online',
        direction: 'DESC'
    }],

    // [{
    //     start: timestamp,
    //     duration: seconds,
    //     url: 'chunk url'
    // }, {
    // start: timestamp,
    //     duration: seconds,
    //     url: 'chunk url'
    // }]
    //fields: ['time', 'devices', 'streaming'],

    proxy: {
        type: 'ajax',
        url: '/filter/day',
        reader: {
            type: 'json'
        }
    }
});