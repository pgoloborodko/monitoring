Ext.define('Landau.map.users-menu.FilterUserModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'userModel', mapping: function (data, record) {
            if(data != null && data.user != null) {
                return Ext.create("Landau.users.UsersModel", data.user); //.get("passport");
            }
        }},

        {name: 'userPassport', convert: function (v, rec) {
                if(rec !== undefined && rec.get('userModel') !== undefined) {
                    return rec.get('userModel').get("passport");
                }
            }, depends: [ 'userModel' ]},


        {name: 'type', convert: function (v, rec) {
            return lang((v === "STREAM") ? "СТРИМ" : "АРХИВ");
        }},

        {name: 'online', convert: function (v, rec) {
                if(lang(rec.data.type) === lang("СТРИМ"))  {
                    return rec.data.user.device.online;
                }

                return false;
            }},
    ]
});