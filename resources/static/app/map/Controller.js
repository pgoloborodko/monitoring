Ext.define("Landau.map.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.map-controller",
    isLoaded: false,

    config: {
        eventsDate: new Date(Date.now()),
        eventsDevices: null,
        date: new Date(),
        type: "MAP",
    },

    init: function () {
        if(this.hasHashValue("user")) {
            let users = this.getHashValue("user");
            this.lookup("menu").setSelected(Array.isArray(users) ? users : [users]);
        }

        if(this.hasHashValue("datetime")) {
            this.onChangeDay(new Date(parseInt(this.getHashValue("datetime"))));
        }

        this.calendar = Ext.create("Ext.Dialog", {
            title: lang('Календарь'),
            closable: true,
            closeAction: 'hide',

            width: "90%",
            height: "90%",
            padding:0,

            platformConfig: {
                '!desktop': {
                    width: "100%",
                    height: "100%",
                }
            },
            items: [{
                xtype: 'landau-calendar',
                width: '100%',
                height: '100%',
                padding:0,
                margin: 0,
                date: this.getDate(),
                listeners: {
                    onSelectDate: this.onChangeDay.bind(this)
                }
            }]
        });

        this.lookup("hls").setDateTime(this.getDate());

/*        if(location.hash.indexOf("video") >= 0) {
            var params = Ext.urlDecode(location.hash);

            console.log(params);

            if(params["date"] != null) {
                //date.suspendEvents();
                //date.setValue(new Date(params["date"]));
                //date.resumeEvents(false);

                this.lookup("player").getController().changeDate(this, date);
            }


            if(params["#video?devices"] != null) {
                let devices = params["#video?devices"].split(",").map(function (value) {
                    return parseInt(value);
                });

                Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});

                for(let i=0; i < devices.length; i++) {
                    Ext.Ajax.request({
                        url: '/devices/save?id=' + devices[i],
                        disableCaching: false,
                        scope: this,
                        failure: function () {
                            Ext.Viewport.setMasked(false);
                        },
                        success: function (response) {
                            Ext.Viewport.setMasked(false);

                            this.addDevice(Ext.decode(response.responseText), date);
                        }
                    });
                }
            }

        }*/

        this.lookup("mapContainer").add(Ext.create("Landau.map.MapViewYandex", {
            reference: "map",
            width: '100%',
            height: '100%',
        }));

        //this.lookup("map").bindStore(this.lookup("grid").getStore());
        ///this.lookup("map").bindStore(this.lookup("gridEvents").getStore());
        //
        // console.log('==============================')

/*        Messages.bind({
            listen: "/users/events",
            method: this.messageQuery.bind(this)
        });*/

        /*this.lookup("map").setClickDevice({
            scope: this,
            callback: 'clickDevice'
        });*/
    },

    addUser: function(ctx, type, user) {
        if(!this.lookup("map").hasRoute(user)) {
            this.lookup("map").addRoute(user, this.getDate());
        }

        if(type === "STREAM") {
            this.lookup("sldp").addUser(user);
            this.lookup("map").addUser(user);
        }
        else {
            this.lookup("hls").addUser(user);
        }

        this.addHashValue("user", user.id);
    },

    removeUser: function(ctx, type, user) {
        this.lookup("map").removeRoute(user);

        if(type === "STREAM") {
            this.lookup("sldp").removeUser(user);
            this.lookup("map").removeUser(user);
        }
        else {
            this.lookup("hls").removeUser(user);
        }

        this.removeHashValue("user", user.id);
    },

    locateUser: function(ctx, user) {
        this.lookup("map").fitUser(user);
    },

    setType: function(type) {
        this.callParent([type]);

        if(this.lookup("menu") != null) {
            this.lookup("menu").setType(type);
        }

        if(this.lookup("map") != null) {
            this.lookup("map").resize();
        }

        if(type === "MAP") {
            this.lookup("sldp").setHidden(true);
            this.lookup("hlsContainer").setHidden(true);

            if(Ext.os.is.Desktop === undefined) {
                this.lookup("mapContainer").setHidden(false);
            }
        }
        else if(type === "STREAM") {
            this.lookup("sldp").setHidden(false);
            this.lookup("hlsContainer").setHidden(true);

            if(Ext.os.is.Desktop === undefined) {
                this.lookup("mapContainer").setHidden(true);
            }
        }
        else if(type === "ARCHIVE") {
            this.lookup("sldp").setHidden(true);
            this.lookup("hlsContainer").setHidden(false);

            if(Ext.os.is.Desktop === undefined) {
                this.lookup("mapContainer").setHidden(true);
            }
        }
    },

    mouseMove: function(e) {
        if (e.parentEvent.browserEvent.clientY < 40) {
            this.lookup('videoControlPanel').show();
        } else {
            this.lookup('videoControlPanel').hide()
        }
    },

    dialogDeviceClose: function () {
        if(!this.lookup("videoContainer").getHidden()) {
            this.lookup("player").play();
        }
    },

    clickDevice: function (data) {
        this.lookup("player").pause();

        new Ext.create("Landau.devices.one.ViewSldp", {
            device: data,
            play: true
        }).show();
    },

    mouseEnterDevise: function (device) {
        var store = Ext.create('Landau.map.stores.infoDeviceStore');

        store.load({
            params: {
                id: device.id
            },
            callback: function(records, operation, success) {
                success && this.lookup("map").createBalloon(records, device.location);
            },
            scope: this
        });

        //this.lookup("map").createBalloon('vvvvv', device.location)
    },

    rowclick: function (scope, row) {
        if(row.cell.dataIndex !== "checked" &&(row.record.get("checked") === undefined || row.record.get("checked") === false)) {
            row.record.set("checked", true);

            this.checkchange(scope, row, true, row.record);
        }

        this.lookup("map").fitDevice(row.record.data);
    },

    checkchange: function(that, rowIndex, checked, record, e, eOpts) {
        var date = this.lookup("date").getValue();

        if(checked) {
            this.addDevice(record.data, date);
        }
        else {
            this.removeDevice(record.data);
        }

        this.redirectTo('video?devices=' + this.selected.join(",") + "&date=" + date.toISOString());
    },

    openEvents: function (buttonValue) {
        var button = this.lookup(buttonValue.reference);

        button.setUi("raised");
        button.setBadgeText(false);

        this.eventView = new Ext.create("Landau.events.View", {
            type: (buttonValue.reference === "eventButton") ? "" : 'SOS',
            parentController: this
        });

        this.eventView.show();
    },

    openButtonEvents: function() {
        this.openForm("Landau.events.button.Dialog");
    },

    openStreamsEvents: function() {
        this.openForm("Landau.events.streams.Dialog");
    },

    openSystemEvents: function() {
        this.openForm("Landau.events.system.Dialog");
    },

    messageSos: function (data) {
        var audio = new Audio('/common/warning_sos.mp3');
        audio.play();

        if(this.eventView != undefined && !this.eventView.isDestroyed) {
            this.eventView.lookup("grid").getStore().reload();
        }
        else {
            var button = this.lookup("sosButton");
            var count = !button.getBadgeText() ? 0 : parseInt(button.getBadgeText());

            count++;

            button.setBadgeText(count);
            button.setUi("raised button-group decline alt");
        }
    },

    messageQuery: function (data) {
        console.log(data);

        if(this.eventView != undefined && !this.eventView.isDestroyed) {
            this.eventView.lookup("grid").getStore().reload();
        }
        else {
            if(data.type === "START_STREAM") {
                var audio = new Audio('/common/warning_streaming.mp3');
                audio.play();
            }

            var button = this.lookup("eventButton");
            var count = !button.getBadgeText() ? 0 : parseInt(button.getBadgeText());

            count++;

            button.setBadgeText(count);
            button.setUi("raised button-group action alt");

            let items = this.lookup("grid").getStore().getData();

            for (let i = 0; i < items.length; i++) {
                if(items.getAt(i).get("id") == data.device.id) {
                    items.getAt(i).set("checked", true);
                    this.checkchange(this, -1, true, items.getAt(i));
                }
            }
        }
    },


    onCenter: function (that, data) {
        console.log(data.cell);

        if(!this.lookup("map").fitDevice(data.record.data)) {
            return Ext.Msg.alert(lang('Внимание!'), lang('Регистратор еще не отправлял GPS данных!'), Ext.emptyFn);
        }
    },

    clickRoute: function (deviceId, date) {
        console.log("заглушка");
    },

    clickDate: function(ctx, date) {
        this.calendar.show();
    },

    onChangeDay: function(date) {
        this.setDate(date);

        if(this.calendar != null) {
            this.calendar.hide();
            this.lookup("sldp").removeAllUsers();
            this.lookup("hls").removeAllUsers();
            this.lookup("menu").setSelected([]);
            this.updateHashValue("datetime", date.getTime());
            this.removeHashValue("user");
        }

        this.lookup("hls").setDateTime(date);
        this.lookup("date").setValue(date);
        this.lookup("dateHls").setValue(date);
        this.lookup("menu").setDate(date);
    },

    onStream: function () {
        let calendar = this.lookup('date');
        calendar.suspendEvents();
        calendar.setValue(new Date());
        calendar.resumeEvents(false);
    },

    onExpand: function (ctx, expand) {
        this.lookup("menu").setHidden(expand);
        this.lookup("mapContainer").setHidden(expand);
        this.getView().up().up().getTabBar().setHidden(expand);
    }
});