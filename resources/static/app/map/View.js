Ext.define("Landau.map.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'map-view',
    controller: 'map-controller',
    flex: 1,

    requires: [
        'Landau.map.users-menu.View',
        'Landau.map.Controller',
        "Landau.map.MapViewYandex",
        "Landau.player.sldp.View",
        "Landau.player.hls.View",
        "Landau.calendar.Calendar"
    ],

    height: "100%",
    defaultType: 'panel',

    layout: {
        type: 'hbox'
    },

    items: [{
        xtype: 'map-users-menu-view',
        reference: 'menu',
        width: 250,
        platformConfig: {
            '!desktop': {
                width: "100%",
            }
        },
        listeners: {
            'addUser': 'addUser',
            'removeUser': 'removeUser',
            'locateUser': 'locateUser',
        }
    }, {
        xtype: 'player-sldp',
        reference: 'sldp',
        hidden: true,
        flex: 8,
        height: '100%',
    }, {
        xtype: 'panel',
        flex: 8,
        hidden: true,
        reference: 'hlsContainer',
        items: [{
            xtype: 'player-hls',
            reference: 'hls',
            height: '100%',
        }],
        tbar: {
            platformConfig: {
                'desktop': {
                    hidden: true
                }
            },
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'datepickerfield',
                maxValue: new Date(),
                value: new Date(),
                dateFormat: 'Y-m-d',
                reference: 'dateHls',
                readOnly: true,
                width: 150,
                listeners: {
                    click: {
                        element: 'element', //bind to the underlying el property on the panel
                        fn: 'clickDate'
                    }
                }
            }]
        }
    }, {
        header: false,
        flex: 3,
        reference: "mapContainer",
        width: '100%',
        height: '100%',
        tbar: [{
            xtype: 'spacer'
        }, {
            //text: lang('События кнопок'),
            margin: '0 5 0 0',
            ui: 'raised',
            iconCls: 'x-fa fa-bell',
            handler: 'openButtonEvents',
            platformConfig: {
                '!desktop': {
                    hidden: true
                }
            },
            tooltip: {
                html:  lang('События нажатия кнопок'),
                autoHide: true,
                anchor: true,
                align: 'br'
            }
        }, {
            //text: lang('События стримов'),
            margin: '0 5 0 0',
            ui: 'raised',
            iconCls: 'x-fa fa-film',
            handler: 'openStreamsEvents',
            platformConfig: {
                '!desktop': {
                    hidden: true
                }
            },
            tooltip: {
                html: lang('События стримов'),
                autoHide: true,
                anchor: true,
                align: 'br'
            }
        }, {
            //text: lang('Системные события'),
            margin: '0 15 0 0',
            ui: 'raised',
            iconCls: 'x-fa fa-puzzle-piece',
            handler: 'openSystemEvents',
            platformConfig: {
                '!desktop': {
                    hidden: true
                }
            },
            tooltip: {
                html: lang('Системные события устройств'),
                autoHide: true,
                anchor: true,
                align: 'br'
            }
        }, {
            xtype: 'container',
            items: [{
                xtype: 'datepickerfield',
                maxValue: new Date(),
                value: new Date(),
                reference: 'date',
                dateFormat: 'Y-m-d',
                readOnly: true,
                platformConfig: {
                    '!desktop': {
                        width: 150
                    }
                },
                width: 95,
                listeners:{
                    click: {
                        element: 'element', //bind to the underlying el property on the panel
                        fn: 'clickDate'
                    }
                }
            }],
        }]
    }]
});