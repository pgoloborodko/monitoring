Ext.define("Landau.map.MapViewMetro", {
    extend: "Ext.Panel",
    xtype: "metro-map",

    requiredScripts: [
        '/js/metro-map/metro-map.js',
    ],

    mixins: ['Ext.mixin.Mashup'],

    requires: [
        'Ext.data.StoreManager'
    ],

    getElementConfig: function() {
        return {
            reference: 'element',
            className: 'x-container',
            style: 'height: 100%; width: 100%;',
            children: [{
                reference: 'bodyElement',
                className: 'x-inner',
                style: 'height: 100%; width: 100%;',
                children: [{
                    reference: 'mapContainer',
                    style: 'height: 100%; width: 100%;',
                    className: Ext.baseCSSPrefix + 'map-container'
                }]
            }]
        };
    },

    config: {
        useCurrentLocation: false,
        map: null,
        mapListeners: null,
        markers: {},
        clickDevice: {scope: this, callback: function () {}},
        clickRoute: {scope: this, callback: function () {}},
        store: null,
        isRoute: false
    },

    initialize: function() {
        var that = this;
        this.callParent();
        this.initMap();
    },

    initMap: function() {
        var map = this.getMap();
        if(!map) {
            if(!window.MetroMap) return null;

            var element = this.mapContainer;
            //Remove the API Required div
            if (element.dom.firstChild) {
                Ext.fly(element.dom.firstChild).destroy();
            }
            var that = this;

            map = new MetroMap(element.dom);

            map.ready(function() {
                console.log();

                that.setMap(map);
                that.initEventListeners();
            });
        }
    },

    bindStore: function (store) {
        this.setStore(store);
    },

    initEventListeners: function () {
        Messages.bind({
            listen: Device,
            scope: this,
            callback: function (data) {
                if (this.getMarkers()[data.id]) {
                    this.getMarkers()[data.id].device = data;
                    this.getMarkers()[data.id].setIcon(data.online ? '/images/user.png' : '/images/off_user.png');
                    this.getMarkers()[data.id].setText(data.name + ",&nbsp;♥&nbsp;" + data.pulse);
                }

                var records = this.getStore().getData().getRange();

                for (var i = 0; i < records.length; i++) {
                    if (records[i].data.id === data.id) {
                        records[i].set('state', data.online);
                    }
                }
            }
        });

        Messages.bind({
            listen: DeviceWifiLog,
            scope: this,
            callback: function (data) {
                if(data.device != null) {
                    if(this.getMarkers()[data.device.id] && this.getMarkers()[data.device.id].isUpdate) {
                        this.getMarkers()[data.device.id].setMac(data.mac);
                    }
                }
            }
        });
    },

    addUser: function(user) {
        var that = this;

        if(!window.MetroMap) return null;

        //ymaps.ready(function() {
            that.removeDevice(device);

            Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});

            Ext.Ajax.request({
                url: '/devices/wifi/one?id=' + device.id,
                disableCaching: false,
                scope: that,
                failure: function () {
                    Ext.Viewport.setMasked(false);
                },
                success: function(response) {
                    Ext.Viewport.setMasked(false);

                    var decode = Ext.decode(response.responseText);

                    if(decode == null) {
                        return Ext.Msg.alert(lang('Внимание!'), lang('Регистратор еще не отправлял Wi-Fi данных!'), Ext.emptyFn);
                    }

                    // add marker
                    var marker = this.getMap().addMarker(decode.mac,  device.name, device.online ? '/images/user.png' : '/images/off_user.png');
                    marker.device = device;

                    this.getMarkers()[device.id] = marker;

                    marker.setClick(function () {
                        this.getClickDevice().scope[this.getClickDevice().callback].call(this.getClickDevice().scope, this.getMarkers()[device.id].device);
                    }.bind(this));

                    // update records
                    var records = this.getStore().getData().getRange();

                    for (var i = 0; i < records.length; i++) {
                        if (records[i].data.id === device.id) {
                            records[i].set('gps', true);
                        }
                    }
                }
            });
        //});
    },

    removeUser: function (device) {
        if(!window.MetroMap) return null;

        if(this.getMarkers()[device.id] != null) {
            this.getMarkers()[device.id].remove();
            this.getMarkers()[device.id] = null;
        }

        var records = this.getStore().getData().getRange();
        for (var i = 0; i < records.length; i++) {
            if (records[i].data.id === device.id) {
                records[i].set('gps', false);
            }
        }
    },

    removeDevices: function () {
        if(!window.MetroMap) return null;

        for(let device in this.getMarkers()) {
            this.removeDevice({id: device});
        }
    },

    removeRoutes: function () {
        return null;
    },

    addRoute: function(device, date) {
        return null;
    },

    removeRoute: function(device) {
        return null;
    },

    addRoutes: function (records, date) {
        return null;
    },

    fitDevice: function(device) {
        return null;
    },

    resize: function () {
        
    }
});