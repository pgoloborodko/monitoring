Ext.define('Landau.map.stores.PlayListsModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id_device', mapping: 'id_device'},
        {name: 'start', mapping: 'start'},
        {name: 'duration', mapping: 'duration'},
        {name: 'url', mapping: 'url'}
        // {name: 'id', mapping: 'id'},
        // {name: 'type', mapping: 'type'},
        // {name: 'text', mapping: 'text'},
        // {name: 'deviceFull', mapping: 'device'},
        // {name: 'device', mapping: 'deviceFull.name'},
        // {name: 'company', mapping: 'company.name'},
        // {name: 'addedTimeOnly', mapping: 'added', convert: function (value, record) {
        //         return Ext.Date.format(new Date(value), "H:i:s");
        //     }},
        // {name: 'added', convert: function (value, record) {
        //         return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        //     }},
        // {name: 'result', convert: function (value, record) {
        //         return value == 1 ? true : false;
        //     }},
    ]
});