Ext.define('Landau.map.stores.infoDeviceStore', {
    extend: 'Ext.data.Store',
    alias: 'store.info-device-store',

    //model: 'Landau.geozone.list.add.GeozoneAddModel',

    proxy: {
        type: 'ajax',
        url: '/devices/info',
        reader: {
            type: 'json',
        }
    }
});