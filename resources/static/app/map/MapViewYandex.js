Ext.define("Landau.map.MapViewYandex", {
    extend: "Ext.Panel",
    xtype: "yandex-map",

    requiredScripts: [
        '//api-maps.yandex.ru/2.1/?lang=ru_RU'
    ],

    mixins: ['Ext.mixin.Mashup'],

    requires: [
        'Ext.data.StoreManager'
    ],

    myPolygon: null,
    myBalloon: null,
    eventsForStatus: {},
    eventsForLocations: {},

    getElementConfig: function() {
        return {
            reference: 'element',
            className: 'x-container',
            style: 'height: 100%; width: 100%;',
            children: [{
                reference: 'bodyElement',
                className: 'x-inner',
                style: 'height: 100%; width: 100%;',
                children: [{
                    reference: 'mapContainer',
                    style: 'height: 100%; width: 100%;',
                    className: Ext.baseCSSPrefix + 'map-container'
                }]
            }]
        };
    },

    config: {
        useCurrentLocation: false,
        map: null,
        mapOptions: {},
        mapListeners: null,
        markers: {},
        routes: {},
        clusters: {},
        polylines: {},
        polylinePoints:{},
        clickUser: {scope: this, callback: function () {}},
        clickRoute: {scope: this, callback: function () {}},
        store: null,
        isRoute: false
    },

    initialize: function() {
        var that = this;
        this.callParent();
        this.initMap();
    },

    initMap: function() {
        var map = this.getMap();
        if(!map) {
            if(!window.ymaps) return null;

            var element = this.mapContainer,
                mapOptions = this.getMapOptions();
                me = this;

            //Remove the API Required div
            if (element.dom.firstChild) {
                Ext.fly(element.dom.firstChild).destroy();
            }

            mapOptions.center = mapOptions.center || [55.76, 37.64];
            mapOptions.zoom = mapOptions.zoom || 10;


            var that = this;
            ymaps.ready(function() {
                map = new ymaps.Map(element.dom, mapOptions, {
                    suppressMapOpenBlock: true,
                    yandexMapDisablePoiInteractivity: true
                });

                that.setMap(map);
            });
        }
    },

    bindStore: function (store) {
        this.setStore(store);
    },

    addUser: function(user) {
        if(!window.ymaps) return null;

        let statusEvents = Messages.bind({
            listen: "/user/users/status/" + user.id,
            method: (user) => {
                if(!window.ymaps) return null;

                this.getMarkers()[user.id].options.set('iconImageHref', user.device.online ? '/images/user.png' : '/images/off_user.png');
            },
            callback: (event) => {
                this.eventsForStatus[user.id] = event;
            }
        });

        let locationEvents = Messages.bind({
            listen: "/user/users/location/" + user.id,
            method:  (location)  => {
                if(!window.ymaps) return null;

                location.created = new Date(location.created);

                if(this.getMarkers()[user.id] && this.getMarkers()[user.id].isUpdate) {
                    this.getMarkers()[user.id].geometry.setCoordinates([location.latitude, location.longitude]);
                }

                if(this.getClusters()[user.id] && this.getClusters()[user.id].isUpdate) {
                    this._addLocationToRoute(user, location);
                }
            },
            callback: (event) => {
                this.eventsForLocations[user.id] = event;
            }
        });

        Ext.Ajax.request({
            url: '/locations/user/last?user=' + user.id,
            scope: this,
            failure: function () {
                Ext.Viewport.setMasked(false);
                Ext.Msg.alert(lang('Ошибка!'), lang('Регистратор еще не присылал локаций!'));
            },
            success: function (response) {
                Ext.Viewport.setMasked(false);

                var decode = Ext.decode(response.responseText);

                ymaps.ready(function() {
                    this._createMarker(user, decode, true);
                }.bind(this));

                this.fit();
            }
        });
    },

    removeUser: function (user) {
        if(!window.ymaps) return null;

        if(this.eventsForStatus[user.id] != null) {
            this.eventsForStatus[user.id].unsubscribe();
        }

        if(this.eventsForLocations[user.id] != null) {
            this.eventsForLocations[user.id].unsubscribe();
        }

        if(this.getMarkers()[user.id] != null) {
            this.getMap().geoObjects.remove(this.getMarkers()[user.id]);
            this.getMarkers()[user.id] = null;
        }
    },

    _createMarker: function (user, location, isUpdate) {
        if(!window.ymaps) return null;

        var content = ymaps.templateLayoutFactory.createClass(
            '<div class="map-marker">$[properties.iconContent]</div>'
        );

        this.getMarkers()[user.id] = new ymaps.Placemark([location.latitude, location.longitude], {
            iconCaption: user.passport,
            iconContent: user.passport,
            hintContent: lang("Идет загрузка данных...")
           }, {
            iconLayout: 'default#imageWithContent',
            iconImageHref: user.device.online ? 'images/user.png' : 'images/off_user.png',
            iconImageSize: [32, 32],
            iconImageOffset: [-16, -16],
            iconContentOffset: [35, 8],
            iconContentLayout: content,
        });

        var that = this;

        //this.getMarkers()[user.id].device = data.device;
        this.getMarkers()[user.id].isUpdate = isUpdate !== undefined ? isUpdate : true;

        this.getMarkers()[user.id].events.add('click', function () {
            that.getClickDevice().scope[that.getClickDevice().callback].call(that.getClickDevice().scope, device);
        });
/*
        this.getMarkers()[device.id].events.add('mouseenter', function () {
            that.getMarkers()[device.id].balloon.open();
        });
*/
        this.getMarkers()[user.id].events.add('hintopen', function (e) {
            //that.getMarkers()[device.id].properties.set('hintContent', lang("Идет загрузка данных..."));

            var store = Ext.create('Landau.map.stores.infoDeviceStore');
            var newContent = null;
            var getBalloonHtml = function(rec) {
                //console.log(rec);
                var checkRec = function(val) {
                    if (val) return val;
                    return lang('не задан')
                };

                return (
                    '<div style="padding: 0 10px 10px 10px">' +
                        '<h3>Cотрудник: ' + checkRec(user.fullName) + '</h3>' +
                        '<span>Транспорт: ' + checkRec(rec.transport) + '</span><br/>' +
                        '<span>Статус аккумулятора: ' + checkRec(rec.charge) + '</span>' +
                    '</div>'
                );
            };

            store.load({
                params: {
                    id: device.id
                },
                callback: function(records, operation, success) {
                    success
                        ?
                        newContent = getBalloonHtml(records[0].data)
                        :
                        newContent = '<div>' + lang("Данные не доступны") + '</div>';

                    that.getMarkers()[device.id].properties.set('hintContent', newContent);
                },
                scope: this
            });
        });

        this.getMap().geoObjects.add(this.getMarkers()[user.id]);

        this.getMap().setCenter(
            [location.latitude, location.longitude],
            15
        );
    },

    /**
     *
     * @param records
     */
    addRoutes: function (records, date) {
        if(!window.ymaps) return null;

        var devices = [];
        var that = this;

        for(var i=0; i < records.length; i++) {
            devices.push(records[i].data.id);
        }

        Ext.Viewport.setMasked({ xtype: 'loadmask', message: lang('Загрузка...'), indicator:true});

        Ext.Ajax.request({
            url: '/devices/locations/oneList?date='+Ext.Date.format(date, 'Y-m-d')+'&device=' + devices.join("&device="),
            disableCaching: false,
            scope: this,
            failure: function () {
                Ext.Viewport.setMasked(false);
            },
            success: function(response) {
                Ext.Viewport.setMasked(false);

                var decode = Ext.decode(response.responseText);

                for(var i=0; i < decode.length; i++) {
                    if(decode[i] != null) {
                        this.addRoute(decode[i].device, date);

                        for(var g=0; g < records.length; g++) {
                            if(records[g].data.id === decode[i].device.id) {
                                records[g].set("vision", true);
                            }
                        }
                    }
                }

                this.fit();
            }
        });
    },

    hasRoute: function(user) {
        return this.getClusters()[user.id] !== undefined;
    },

    addRoute: function(user, date) {
        if(!window.ymaps) return null;

        ymaps.ready(function() {
            var url = "date";

            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            //var color = getComputedStyle(document.body).getPropertyValue('--base-color').trim();

            Ext.Viewport.setMasked({xtype: 'loadmask', message: lang('Загрузка...'), indicator: true});

            this.getClusters()[user.id] = new ymaps.Clusterer({
                margin: 0,
                clusterIconContentLayout: null,
                clusterIcons: [{
                    href: '/images/baloon.png',
                    size: [10, 10],
                    offset: [-5, -5]
                }, {
                    href: '/images/baloon.png',
                    size: [10, 10],
                    offset: [-5, -5]
                }],
            });

            this.getClusters()[user.id].isUpdate = Ext.Date.format(date, 'Y-m-d') === Ext.Date.format(new Date(), 'Y-m-d');

            // polyline
            this.getPolylines()[user.id] = new ymaps.Polyline([], {}, {
                balloonCloseButton: false,
                strokeColor: [color, "#ffffff"],
                strokeWidth: [5, 4],
                strokeOpacity: [1, 0.5]
            });

            this.getMap().geoObjects.add(this.getClusters()[user.id]);
            this.getMap().geoObjects.add(this.getPolylines()[user.id]);

            Ext.Ajax.request({
                url: '/locations/user/date?user=' + user.id + "&date=" + Ext.Date.format(date, 'Y-m-d'),
                scope: this,
                failure: function () {
                    Ext.Viewport.setMasked(false);
                },
                success: function (response) {
                    Ext.Viewport.setMasked(false);

                    var decode = Ext.decode(response.responseText);

                    this.getPolylinePoints()[user.id] = [];

                    for (var i = 0; i < decode.length; i++) {
                        if (decode[i] != null) {
                            decode[i].created = new Date(decode[i].created);
                            this._addLocationToRoute(user, decode[i]);
                        }
                    }

                    this.fit();
                }
            });
        }.bind(this));
    },

    removeRoute: function(user) {
        if(!window.ymaps) return null;

        if(this.getClusters()[user.id] != null) {
            this.getMap().geoObjects.remove(this.getClusters()[user.id]);
            delete this.getClusters()[user.id];
        }

        if(this.getPolylines()[user.id] != null) {
            this.getMap().geoObjects.remove(this.getPolylines()[user.id]);
            delete this.getPolylines()[user.id];
        }

        if(this.getPolylinePoints()[user.id] != null) {
            delete this.getPolylinePoints()[user.id];
        }
    },

    removeDevices: function () {
        if(!window.ymaps) return null;

        for(var user in this.getMarkers()) {
            this.removeUser({id: user});
        }
    },

    removeRoutes: function () {
        if(!window.ymaps) return null;

        for(var user in this.getClusters()) {
            this.removeRoute({id: user});
        }
    },

    _addLocationToRoute: function (user, location) {
        if(!window.ymaps) return null;

        var that = this;

        var dateText = Ext.Date.format(location.created, 'H:i:s');

        var placemark = new ymaps.Placemark([location.latitude, location.longitude], {
            balloonContent: location.created,
            iconContent: "",
            clusterCaption: "<div class='x-map-hint'>" + dateText + "</div>"
        }, {
            iconLayout: 'default#imageWithContent',
            iconImageHref: '/images/baloon.png',
            openBalloonOnClick: false,
            iconImageSize: [10, 10],
            iconImageOffset: [-5, -5],
            iconContentOffset: [10, -2],
            iconContentLayout: ymaps.templateLayoutFactory.createClass('<div class="polylineDot">$[properties.iconContent]</div>'),
            hideIconOnBalloonOpen: false
        });

        placemark.events.add('mouseenter', function (ev) {
            ev.get('target').properties.set('iconContent', dateText);
        });

        placemark.events.add('mouseleave', function (ev) {
            ev.get('target').properties.set('iconContent', "");
        });

        placemark.events.add('click', function (ev) {
            that.getClickRoute().scope[that.getClickRoute().callback].call(that.getClickRoute().scope, user.id, date);
        });

        this.getClusters()[user.id].add(placemark);

        //
        if(this.getPolylines()[user.id]) {
            this.getPolylinePoints()[user.id].push(location.created.getTime());
            this.getPolylinePoints()[user.id].sort();

            let index = this.getPolylinePoints()[user.id].indexOf(location.created.getTime());

            //console.log("insert index " + index + " for " + location.created);

            this.getPolylines()[user.id].geometry.insert(index, [location.latitude, location.longitude]);
        }
    },

    /**
     *
     */
    resize: function () {
        if(!window.ymaps) return null;

        ymaps.ready(function() {
            this.getMap().container.fitToViewport();
        }.bind(this));
    },

    /**
     *
     */
    fitUser:function (user) {
        if(!window.ymaps) return null;

        if(this.getMarkers()[user.id] === undefined) {
            return false;
            //this.addDevice(device);
        }
        else {
            this.getMap().setCenter(
                this.getMarkers()[user.id].geometry.getCoordinates(),
                15
            );

            return true;
        }
    },

    /**
     *
     */
    fit: function() {
        if(!window.ymaps) return null;

        if(this.getMap().geoObjects.getBounds() !== null) {
            //console.log(centerAndZoom);
            var centerAndZoom = ymaps.util.bounds.getCenterAndZoom(
                this.getMap().geoObjects.getBounds(),
                this.getMap().container.getSize(),
                this.getMap().options.get('projection')
            );

            if(centerAndZoom.zoom > 15) {
                centerAndZoom.zoom = 15;
            }

            /*if(centerAndZoom.zoom < 10) {
                centerAndZoom.zoom = 10;
            }*/

            this.getMap().setCenter(
                centerAndZoom.center,
                centerAndZoom.zoom
            );
        }
    },

    getPolygon: function() {
        var resArr = [];
        this.myPolygon.geometry.getCoordinates()[0].map(function(item) {
            resArr.push({x: item[0], y: item[1]})
        });
        return resArr
    },

    clearPoligon: function() {
        ymaps.ready(function() {
            this.getMap().geoObjects.removeAll();
            this.createGeozone([], true);
        });
    },

    createGeozone: function(polygonArr, editable) {
        var that = this;
        this.myPolygon = null;

        //if(!window.ymaps) return null;

        ymaps.ready(function() {
            // Создаем многоугольник без вершин.
            that.myPolygon = new ymaps.Polygon(polygonArr, {}, {
                // Курсор в режиме добавления новых вершин.
                editorDrawingCursor: "crosshair",
                // Максимально допустимое количество вершин.
                //editorMaxPoints: 5,
                // Цвет заливки.
                //fillColor: '#00FF00',
                // Цвет обводки.
                strokeColor: '#0000FF',
                // Ширина обводки.
                strokeWidth: 1
            });

            that.getMap().geoObjects.add(that.myPolygon);


            // В режиме добавления новых вершин меняем цвет обводки многоугольника.
            /*
            var stateMonitor = new ymaps.Monitor(that.myPolygon.editor.state);
            stateMonitor.add("drawing", function (newValue) {
                that.myPolygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
            });
            */

            // Включаем режим редактирования с возможностью добавления новых вершин.
            that.myPolygon.editor.startDrawing();
            /*
            editable
                ?
                that.myPolygon.editor.startEditing()
                :
                that.myPolygon.editor.stopEditing()
                */
        });
    }
});