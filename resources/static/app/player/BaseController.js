/**
 * Базовый контейнер видеозаписей
 */
Ext.define("Landau.player.BaseController", {
    extend: "Ext.app.ViewController",
    alias: "controller.player-base-controller",

    setCol: function(control) {
        let col = control.col;
        let videos = this.lookup("videos");

        this.getView().setCol(col);

        for(let i=0; i < videos.getItems().length; i++) {
            videos.getAt(i).setCls(col);

            if(col === "col-12" && this.getView().xtype === "player-hls") {
                let videos = this.lookup("videos");

                for(let i=0; i < videos.getItems().length; i++) {
                    videos.getAt(i).setWidth(0);
                    videos.getAt(i).setWidth(this.getView().el.dom.getBoundingClientRect().width);
                }
            }
             //console.log(videos.getAt(i).el.dom.getBoundingClientRect().width);
        }
    },

    play: function () {
        this.getView().setPlay(true);

        let videos = this.lookup("videos");

        for(let i=0; i < videos.getItems().length; i++) {
            videos.getAt(i).play();
        }

        this.lookup("play").setDisabled(true);
        this.lookup("pause").setDisabled(false);
        this.lookup("stop").setDisabled(false);
    },

    stop: function () {
        this.getView().setPlay(false);

        let videos = this.lookup("videos");

        for(let i=0; i < videos.getItems().length; i++) {
            videos.getAt(i).stop();
        }

        this.lookup("play").setDisabled(false);
        this.lookup("pause").setDisabled(true);
        this.lookup("stop").setDisabled(true);
    },

    pause: function () {
        this.getView().setPlay(false);

        let videos = this.lookup("videos");

        for(let i=0; i < videos.getItems().length; i++) {
            videos.getAt(i).pause();
        }

        this.lookup("play").setDisabled(false);
        this.lookup("pause").setDisabled(true);
        this.lookup("stop").setDisabled(false);
    },

    removeUser: function (user) {
        let videos = this.lookup("videos");

        for(let i=0; i < videos.getItems().length; i++) {
            let item = videos.getAt(i);

            if(item.getUser().id === user.id) {
                item.destroy();
            }
        }

        if(videos.getItems().length === 0) {
            this.lookup("placeholder").setHidden(false);
        }
    },

    removeAllUsers: function () {
        let videos = this.lookup("videos");
        let items = videos.getItems();

        while(items.length > 0) {
            items.getAt(0).destroy();
        }

        this.lookup("placeholder").setHidden(false);
    },

    expand: function () {
        this.getView().toggleCls("expand");
    },

    setDateTime: function (datetime) {
        let videos = this.lookup("videos");
        let items = videos.getItems();

        while(items.length > 0) {
            items.getAt(0).setDateTime(datetime);
        }
    },

    copyUrl: function (ctx, ev) {
        const el = document.createElement('textarea');
        el.value = location.href;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);

        console.log(arguments);

        Ext.toast({
            message:  lang('Ссылка скопирована в буфер обмена'),
            timeout: 600,
            offset: [ev.clientX - 120, ev.clientY - 50]
        });
    },
});