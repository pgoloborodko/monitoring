/**
 * плеер Hls
 */
Ext.define("Landau.player.sldp.View", {
    // extend: "Ext.container.Container",
    extend: "Landau.player.BaseView",
    controller: "player-sldp-controller",
    xtype: "player-sldp",

    requires: [
        "Landau.video.sldp.View",
        "Landau.player.BaseView",
        "Landau.player.sldp.Controller"
    ],

    config: {
        type: 'sldp'
    },

    bbar: [{
        iconCls: 'x-fa fa-play',
        text: lang(''),
        handler: 'play',
        reference: 'play'
    }, {
        iconCls: 'x-fa fa-pause',
        text: lang(''),
        handler: 'pause',
        reference: 'pause',
        disabled: true,
        hidden: true
    }, {
        iconCls: 'x-fa fa-stop',
        text: lang(''),
        handler: 'stop',
        reference: 'stop',
        disabled: true
    }, {
        xtype: 'spacer'
    }, {
        iconCls: 'x-fa fa-copy',
        text: lang('Ссылка на видео'),
        handler: 'copyUrl',
        margin: '0 10 0 0'
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'segmentedbutton',
        cls: 'segment-select-cls',
        reference: 'segment-type',
        margin: '0 10 0 0',
        defaults: {
            handler: 'setCol'
        },
        items: [{
            text: '<img src="/images/grid-views/1.png"> 1',
            col: 'col-12',
        },{
            text: '<img src="/images/grid-views/2.png"> 2',
            pressed: true,
            col: 'col-6'
        },{
            text: '<img src="/images/grid-views/3.png"> 3',
            col: 'col-4'
        },{
            text: '<img src="/images/grid-views/4.png"> 4',
            col: 'col-3'
        }/*,{
            text: '<img src="/images/grid-views/5.png"> 5',
            col: 'col-2'
        }*/,{
            text: '<img src="/images/grid-views/6.png"> 6',
            col: 'col-2'
        }]
    }, {
        iconCls: 'x-fa fa-expand',
        set: 'col-md-12',
        handler: 'expand',
        ui: ''
    }]
});