/**
 * плеер Hls
 */
Ext.define("Landau.player.BaseView", {
    // extend: "Ext.container.Container",
    extend: "Ext.Panel",
    controller: "player-base-controller",
    autoplay: false,

    requires: [
        "Landau.common.Messages",
        "Landau.player.BaseController"
    ],

    config: {
        play: false,
        col: 'col-6',
        type: 'sldp',
        dateTime: new Date(),
    },

    items:[{
        cls:'row scroll-for-container y',
        width: '100%',
        height: '100%',
        xtype: 'container',
        reference: 'videos',
        items: []
    }, {
        reference: 'placeholder',
        xtype: 'container',
        style: 'position: absolute; width: 100%; height: 100%; left:0; top: 0; text-align: center;',
        html: "<div style='margin-top: 20%'>" +  lang("Выберите пользователя") + "</div>"
    }],

    /**
     * Запуск проигрывания
     */
    play: function () {
        this.getController().play();
    },

    /**
     * Стоп проигрывания
     */
    stop: function () {
        console.log("this.autoplay", false);
        this.autoplay = false;
        this.getController().stop();
    },

    /**
     * Пауза проигрывания
     */
    pause: function () {
        this.getController().pause();
    },

    /**
     * Добавление устройства
     * @param user
     */
    addUser: function (user) {
        this.lookup("videos").insert(0, {
            xtype: this.getType(),
            user: user,
            autoplay: this.getPlay(),
            cls: (Ext.os.is.Desktop === undefined) ? "col-12" : this.getCol(),
            dateTime: this.getDateTime(),
            listeners: {
                onPlay: 'play'
            }
        });

        this.lookup("placeholder").setHidden(true);
    },

    /**
     * удаление одного юзера из плеера
     * @param user
     */
    removeUser: function (user) {
        this.getController().removeUser(user);
    },

    /**
     * удаление всех пользователей из плеера
     */
    removeAllUsers: function () {
        this.getController().removeAllUsers();
    },

    /**
     * получение плеера юзера
     * @param user
     */
    getUser: function(user) {
        return this.getController().getUser(user);
    },

    /**
     *
     * @param datetime
     * @returns {*|void}
     */
    setDateTime: function(datetime) {
        this.callParent([datetime]);
        return this.getController().setDateTime(datetime);
    }
});