/**
 * плеер Hls
 */
Ext.define("Landau.player.hls.View", {
    // extend: "Ext.container.Container",
    extend: "Landau.player.BaseView",
    controller: "player-hls-controller",
    xtype: "player-hls",

    requires: [
        "Landau.video.hls.View",
        "Landau.player.BaseView",
        "Landau.player.hls.Controller"
    ],

    config: {
        type: 'hls',
        sync: true,
        col: 'col-12'
    },

    listeners: {
        resize: function (panel, w, h) {
            if(this.getCol() === "col-12") {
                let videos = this.lookup("videos");

                for(let i=0; i < videos.getItems().length; i++) {
                    videos.getAt(i).setWidth(w);
                }
            }
        }
    },

    items:[{
        cls:'scroll-for-container x',
        layout: 'hbox',
        width: '100%',
        height: '100%',
        xtype: 'container',
        reference: 'videos',
        items: []
    }, {
        reference: 'placeholder',
        xtype: 'container',
        style: 'position: absolute; width: 100%; height: 100%; left:0; top: 0; text-align: center;',
        html: "<div style='margin-top: 20%'>" +  lang("Выберите пользователя") + "</div>"
    }],


    // tbar: {
    //     reference: 'videoControlPanel_',
    //     hideAnimation: 'fadeOut',
    //     items: [{
    //         iconCls: 'x-fa fa-play',
    //         text: lang(''),
    //         margin: '0 10 0 0',
    //         reference: 'playButton',
    //         // handler: 'togglePlay',
    //     }]
    // },



    bbar: [{
        iconCls: 'x-fa fa-play',
        text: lang(''),
        handler: 'play',
        reference: 'play',
    }, {
        iconCls: 'x-fa fa-pause',
        text: lang(''),
        handler: 'pause',
        reference: 'pause',
        disabled: true
    }, {
        iconCls: 'x-fa fa-stop',
        text: lang(''),
        handler: 'stop',
        reference: 'stop',
        disabled: true
    }, {
        xtype: 'spacer'
    }, {
        iconCls: 'x-fa fa-copy',
        text: lang('Ссылка на видео'),
        handler: 'copyUrl',
        margin: '0 10 0 0'
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        xtype: 'segmentedbutton',
        cls: 'segment-select-cls',
        reference: 'segment-type',
        margin: '0 10 0 0',
        defaults: {
            handler: 'setCol'
        },
        items: [{
            text: '<img src="/images/grid-views/1.png"> 1',
            pressed: true,
            col: 'col-12',
        },{
            text: '<img src="/images/grid-views/2.png"> 2',
            col: 'col-6'
        },{
            text: '<img src="/images/grid-views/3.png"> 3',
            col: 'col-4'
        },{
            text: '<img src="/images/grid-views/4.png"> 4',
            col: 'col-3'
        }]
    }, {
        iconCls: 'x-fa fa-expand',
        set: 'col-md-12',
        handler: 'expand',
        ui: ''
    }],

    setSync: function(isSync) {
        this.callParent([isSync]);
        //this.getController().setSync(isSync);
    }
});