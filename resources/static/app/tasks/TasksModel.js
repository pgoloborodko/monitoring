Ext.define('Landau.tasks.TasksModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'city', mapping: 'city.name'},
        {name: 'district', mapping: 'district.name'},
        {name: 'company', mapping: 'company.name'},
        {name: 'route', mapping: 'route.name'},
        {name: 'created', convert: function (value, record) {
            return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
        }},
        {name: 'profile', convert: function (value, record) {

            if(value !== undefined && value !== null) {
                var lastName = !value.lastName ? "" : value.lastName;
                var firstName = !value.firstName? "" : value.firstName;
                var secondName = !value.secondName? "" : value.secondName;

                return lastName + " " +  firstName + " " + secondName;
            }
        }},
        {name: 'type', convert: function (value, record) {
            if(value === "FIND_PROFILE") {
                return lang('Поиск человека');
            }
            else if(value === "WALK_ROUTE") {
                return lang('Патрулирование');
            }
        }}
    ]
});