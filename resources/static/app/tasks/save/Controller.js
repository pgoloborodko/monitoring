Ext.define("Landau.tasks.save.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.tasks-save-controller",

    init: function() {
        console.log(this.getView().device);

        var device = this.getView().device;

        if(device !== undefined && device !== null) {
            this.lookup('grid').getStore().reload({
                params: {
                    id: device.id
                },
                callback: function () {
                    var record = this.lookup('grid').getStore().getAt(0);
                    record.set('checked',true);

                    var company = this.lookup("companyBox");
                    var city = this.lookup("cityBox");
                    var district = this.lookup("district");

                    if(record.get("company") != null) {
                        company.setValue(record.get("company").id);
                        company.setDisplayed(record.get("company").name);

                        company.getStore().load({
                            params : {queryString : record.get("company").name}
                        });

                        this.getViewModel().set({
                            company: record.get("company").id
                        });
                    }

                    if(record.get("city") != null) {
                        city.setValue(record.get("city").id);

                        city.setValue(record.get("city").id);
                        city.setDisplayed(record.get("city").name);

                        city.getStore().load({
                            params : {queryString : record.get("city").name}
                        });
                    }

                    if(record.get("district") != null) {
                        district.setValue(record.get("district").id);

                        district.setValue(record.get("district").id);
                        district.setDisplayed(record.get("district").name);

                        district.getStore().load({
                            params : {queryString : record.get("district").name}
                        });
                    }
                },
                scope: this
            });
        }

        console.log(device);
    },

    onChangeCity: function (scope) {
        this.lookup("district").getParams().city = scope.getValue();

        this.onChangeCombo();
    },

    onChangeType: function(thx, value) {
        this.lookup("route").setHidden((value === "FIND_PROFILE"));
        this.lookup("profile").setHidden((value !== "FIND_PROFILE"));

        this.lookup("routeCombo").setRequired((value !== "FIND_PROFILE"));
        this.lookup("profileCombo").setRequired((value === "FIND_PROFILE"));
    },

    onCancel: function () {
        this.getView().destroy();
    },

    onChangeCombo: function (thx, value) {
        this.lookup('grid').getStore().reload({
            params: {
                company: this.lookup('companyBox').getValue(),
                city: this.lookup('cityBox').getValue(),
                district: this.lookup('district').getValue()
            }
        });
    },

    onOK: function () {
        var form = this.lookup('form');
        var grid = this.lookup("grid");

        var allRecords = (grid.getStore().getData().getSource() || grid.getStore().getData()).getRange();

        var performers = {};

        for(var i=0, index=0; i < allRecords.length; i++) {
            if(allRecords[i].data.checked) {
                performers["performers[" + index++ + "].user"] = allRecords[i].data.id;
            }
        }

        if(Object.keys(performers).length === 0) {
            return Ext.Msg.alert(lang("Внимание!"), lang("Выберите хотябы одно устройство."));
        }

        if (form.validate()) {
            form.submit({
                params: performers,
                url: '/tasks/save',
                scope: this,
                success: this.onCancel,
                failure: this.onFailure
            });
        }
    },

    onFailure: function (ctx, result) {
        return Ext.Msg.alert(lang('Внимание!'), lang('Ошибка запроса: ' + result.responseText));
    }
});