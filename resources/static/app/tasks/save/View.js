Ext.define('Landau.tasks.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'tasks-save-view',
    controller: 'tasks-save-controller',
    title: lang('Сохранение задачи'),

    requires: [
        'Landau.tasks.save.Controller',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect',
        'Landau.users.UsersSelect',
        'Landau.routes.RoutesSelect',
        'Landau.profiles.ProfilesSelect',
        'Landau.users.UsersOnlineStore'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',
    width: "90%",
    height: "90%",
    scrollable: true,

    platformConfig: {
        '!desktop': {
            width: "100%",
            height: "100%",
        }
    },

    onCancel: function () {
        this.destroy();
    },

    viewModel: {
        data: {
            company: null,
            city: null,
        }
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        width: "100%",
        height: "100%",
        scrollable: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        }, {
            xtype: 'panel',
            height: "100%",
            layout: 'hbox',
            platformConfig: {
                '!desktop': {
                    layout: 'vbox',
                }
            },

            items: [{
                xtype: 'panel',
                layout: 'vbox',
                height: "100%",
                flex: 1,
                margin: '0 10 0 0',
                items: [{
                    xtype: 'containerfield',
                    label: lang('Тип задачи'),
                    required: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'selectfield',
                        name: 'type',
                        required: true,
                        placeholder: lang('Выберите тип задачи...'),
                        options: [{
                            text: lang('Поиск человека'),
                            value: 'FIND_PROFILE'
                        }, {
                            text: lang('Патрулирование'),
                            value: 'WALK_ROUTE'
                        }],
                        listeners : {
                            change : 'onChangeType'
                        }
                    }]
                }, {
                    xtype: 'containerfield',
                    label: lang('Компания'),
                    required: true,
                    reference: 'route',
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'companies-select',
                        name:'company',
                        reference: 'companyBox',
                        listeners: {
                            change: 'onChangeCombo'
                        }
                    }]
                }, {
                    xtype: 'containerfield',
                    label: lang('Маршрут'),
                    required: true,
                    reference: 'route',
                    hidden: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'routes-select',
                        name:'route',
                        reference: 'routeCombo'
                    }]
                }, {
                    xtype: 'containerfield',
                    label: lang('Человек'),
                    required: true,
                    reference: 'profile',
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'profiles-select',
                        reference: 'profileCombo',
                        name:'profile'
                    }]
                }, {
                    xtype: 'containerfield',
                    label: lang('Комментарий'),
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'textareafield',
                        name: 'comment'
                    }]
                }]
            },{
                xtype: 'panel',
                layout: 'vbox',
                height: "100%",
                flex: 1,
                items: [/*{
                    xtype: 'containerfield',
                    label: lang('Фильтр пользователей'),
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'cities-select',
                        reference: 'cityBox',
                        name:'city',
                        margin: '0 5 0 0',
                        listeners: {
                            change: 'onChangeCity'
                        }
                    }, {
                        xtype: 'districts-select',
                        reference: 'district',
                        name:'district',
                        listeners: {
                            change: 'onChangeCombo'
                        }
                    }]
                }, */{
                    xtype: 'containerfield',
                    label: lang('Пользователь'),
                    required: true,
                    flex: 1,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'grid',
                        reference: 'grid',
                        hideHeaders: true,
                        flex: 1,
                        store: {
                            type: 'users-online-store',
                            autoLoad: true
                        },
                        columns: [{
                            text: '',
                            xtype: 'checkcolumn',
                            headerCheckbox: true,
                            dataIndex: 'checked',
                            width: 30
                        }, {
                            dataIndex: 'state',
                            width: 30,
                            cell: {
                                encodeHtml: false
                            },
                            renderer: function(value, record, dataIndex, cell) {
                                if(lang(record.data.type) === lang("СТРИМ"))  {
                                    cell.row.setCls(record.data.user.device.online ? 'online' : 'offline');
                                }
                                else {
                                    cell.row.setCls("");
                                }

                                return value;
                            }
                        }, {
                            text: lang('ФИО'),
                            dataIndex: 'passport',
                            flex: 1
                        }]
                    }]
                }]
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});