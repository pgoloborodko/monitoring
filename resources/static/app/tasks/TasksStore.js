Ext.define('Landau.tasks.TasksStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tasks-store',

    model: 'Landau.tasks.TasksModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/tasks',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});