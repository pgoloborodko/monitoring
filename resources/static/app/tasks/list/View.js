Ext.define("Landau.tasks.list.View", {
    extend: 'Ext.Panel',
    controller: 'tasks-controller',
    xtype: 'tasks-list-view',

    requires: [
        'Landau.tasks.list.Controller',
        'Landau.tasks.list.ViewGrid',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        height: "100%",
        xtype: 'tasks-grid-view',
        reference: 'grid'
    }],

    tbar: {
        platformConfig: {
            '!desktop': {
                layout: 'vbox'
            }
        },
        items: [{
            platformConfig: {
                '!desktop': {
                    text: lang('Добавить'),
                    margin: '0 0 0 0',
                }
            },
            text: lang('Добавить задачу'),
            ui: 'action',
            handler: 'save',
            margin: '0 10 0 0'
        }, {
            xtype: 'spacer'
        },{
            xtype: 'datepickerfield',
            maxValue: new Date(),
            value: new Date(),
            margin: '0 10 0 0',
            reference: 'date',
            dateFormat: 'd/m/Y',
            platformConfig: {
                '!desktop': {
                    margin: '0 0 0 0',
                    flex: 1,
                }
            }
        }, {
            xtype: 'selectfield',
            reference: 'type',
            clearable: true,
            placeholder: lang('Выберите тип задачи...'),
            options: [{
                text: lang('Поиск биометрического профиля'),
                value: 'FIND_PROFILE'
            }, {
                text: lang('Патрулирование'),
                value: 'WALK_ROUTE'
            }],
            margin: '0 10 0 0',
            platformConfig: {
                '!desktop': {
                    margin: '0 0 0 0',
                    flex: 1
                }
            }
        }, {
            xtype: 'companies-select',
            reference: 'company',
            margin: '0 10 0 0',
            platformConfig: {
                '!desktop': {
                    margin: '0 0 0 0',
                }
            }
        }, {
            xtype: 'cities-select',
            reference: 'city',
            margin: '0 10 0 0',
            platformConfig: {
                '!desktop': {
                    margin: '0 0 0 0',
                }
            }
        }, {
            xtype: 'districts-select',
            reference: 'district',
            margin: '0 10 0 0',
            platformConfig: {
                '!desktop': {
                    margin: '0 0 0 0',
                }
            }
        }, {
            platformConfig: {
                '!desktop': {
                    flex: 1
                }
            },
            iconCls: 'x-fa fa-search',
            text: lang('Найти'),
            handler: 'doFilter'
        }]
    }
});