Ext.define("Landau.tasks.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'tasks-grid-view',

    height: '100%',

    requires: [
        'Landau.tasks.TasksStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'tasks-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Тип задачи'),
        dataIndex: 'type',
        width: 100,
        platformConfig: {
            '!desktop': {
                flex: 1
            }
        }
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 100,
        platformConfig: {
            '!desktop': {
               hidden: true
            }
        }
    }, {
        text: lang('Город'),
        dataIndex: 'city',
        width: 150,
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        }
    }, {
        text: lang('Район'),
        dataIndex: 'district',
        width: 150,
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        }
    }, {
        text: lang('Профиль'),
        dataIndex: 'profile',
        width: 250,
        platformConfig: {
            '!desktop': {
                flex: 1
            }
        }
    }, {
        text: lang('Маршрут'),
        dataIndex: 'route',
        width: 150,
        platformConfig: {
            '!desktop': {
                flex: 1
            }
        }
    }, {
        text: lang('Кол-во участников'),
        dataIndex: 'performers',
        width: 150,
        renderer: function (value) {
            return value.length;
        },
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        }
    }, {
        text: lang('Создан'),
        dataIndex: 'created',
        width: 150,
        platformConfig: {
            '!desktop': {
                flex: 1
            }
        }
    }/*, {
        text: lang('Действия'),
        width: 'auto',

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }*/]
});