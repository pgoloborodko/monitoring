Ext.define("Landau.tasks.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.tasks-controller",

    save: function () {
        this.openForm("Landau.tasks.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.tasks.save.View", '/tasks/save?id=' + info.record.id);
    },

    doFilter: function () {
        var date = this.lookup("date");
        var dateStart = date.getValue();
        dateStart.setHours(0,0,0,0);

        var company = this.lookup("company");
        var city = this.lookup("city");
        var district = this.lookup("district");
        var type = this.lookup("type");

        this.lookup("grid").getStore().reload({
            params: {
                created: Ext.Date.format(dateStart, "Y-m-d"),
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue(),
                type: type.getValue()
            }
        });
    }
});