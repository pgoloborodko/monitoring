Ext.define('Landau.devices.tele.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-tele-view',
    controller: 'devices-tele-controller',
    title: lang('Телеметрия'),

    requires: [
        'Landau.devices.tele.Controller',
        'Landau.devices.tele.ViewGrid'
    ],

    bodyPadding: 0,
    //autoSize: true,
    maximized: true,
    height: '100%',

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        height: '100%',
        width: '100%',
        xtype: 'devices-tele-grid-view',
        reference: 'grid',
        bind: {
            viewModel: {
                data: {
                    device: '{device}'
                }
            }
        }
    }],

/*
    buttons: [{
        text: lang('Закрыть'),
        handler: 'onCancel'
    }]
    */
});