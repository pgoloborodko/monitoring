Ext.define("Landau.devices.tele.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-tele-controller",

    onFilter: 'ffff',

    onCancel: function () {
        this.getView().destroy();
    },

    doFilter: function () {
        var date = this.lookup("date");

        //var dateStart = date.getValue();
        //dateStart.setHours(0,0,0,0);

        this.lookup("grid").getStore().reload({
            params: {
                telemetryDate: Ext.Date.format(date.getValue(), 'Y/m/d')
            }
        });

/*
        if(!this.lookup("radioColumn").getHidden()) {
            if(this.lookup("videos").getItems().length > 0) {
                this.lookup("videos").getItems().get(0).loadTime(dateStart);

                this.lookup("map").removeRoutes();
                //this.lookup("map").addRoute(that.getValue(), date.getValue());
            }


        this.lookup("map").removeRoutes();

        var records = this.lookup("grid").getStore().data.items;


        if(this.lookup("checkbox").getChecked()) {
            for(var g=0; g < records.length; g++) {
                records[g].set("vision", false);
            }

            this.lookup("map").addRoutes(records, dateStart);
        }
        else {
            for (var i = 0; i < records.length; i++) {
                if(records[i].data.vision) {
                    this.lookup("map").addRoute(records[i].data, dateStart);
                }
            }

        for (var i = 0; i < records.length; i++) {
            if(records[i].data.vision) {
                this.lookup("map").addRoute(records[i].data, dateStart);
            }
        }*/
    }


});