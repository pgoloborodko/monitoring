Ext.define('Landau.devices.tele.Model', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', mapping: 'id'},
        {name: 'gsmType', convert: (value) => {
            return nullConvert(value);
            }},
        {name: 'gsmSignal', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'faceTemplates', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'watchPulse', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'allSpace', convert: (value) => {
                return (value/1024/1024/1024).toFixed(1) + ' Гб';
            }},
        {name: 'holdSpace', convert: (value) => {
                return (value/1024/1024/1024).toFixed(1) + ' Гб';
            }},
        {name: 'freeSpace', convert: (value) => {
                return (value/1024/1024/1024).toFixed(1) + ' Гб';
            }},
        {name: 'cpuLoad', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'gpuLoad', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'akbDeviceCharge', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'akbWatchCharge', convert: (value) => {
                return nullConvert(value);
            }},
        {name: 'telemetryDate', convert: function (value) {
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }},
        {name: 'externalCameraOnline', convert: (value) => {
                return trueFalseConvert(value);
            }},
        {name: 'charging', convert: (value) => {
                return trueFalseConvert(value);
            }},
        {name: 'wifiConnected', convert: (value) => {
                return trueFalseConvert(value);
            }},
        {name: 'gsmConnected', convert: (value) => {
                return nullConvert(value);
            }}
    ]
});

function nullConvert(val) {
    if (val || val === 0) return val
    return '~'
}

function trueFalseConvert(val) {
    if (val) return lang('Да')
    return lang('Нет')
}


