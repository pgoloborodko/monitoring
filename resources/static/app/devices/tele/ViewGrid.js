Ext.define("Landau.devices.tele.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'devices-tele-grid-view',

    height: 500,

    requires: [
        'Landau.devices.tele.Store',
        'Landau.common.Pageable'
    ],

    bind: {
        store: {
            type: 'devices-tele-store',
            autoLoad: true,
            proxy: {
                extraParams : {
                    device :'{device.id}'
                }
            }
        },
    },

    items: [{
        xtype: 'pageable'
    }, {
        xtype : 'toolbar',
        docked: 'top',
        items: [{
            xtype: 'datepickerfield',
            maxValue: new Date(),
            value: new Date(),
            margin: '0',
            reference: 'date',
            dateFormat: 'd/m/Y',
            width: 100
        }, {
            iconCls: 'x-fa fa-search',
            text: lang('Найти'),
            handler: 'doFilter'
        }]
    }],


    columns: [{
        text: lang('Id'),
        dataIndex: 'id',
        width: 'auto'
    }, {
        text: lang('Тип GSM'),
        dataIndex: 'gsmType',
        width: 'auto'
    }, {
        text: lang('Сигнал GSM'),
        dataIndex: 'gsmSignal',
        width: 'auto'
    }, {
        text: lang('Шаблоны лиц'),
        dataIndex: 'faceTemplates',
        width: 'auto'
    }, {
        text: lang('Пульс с часов'),
        dataIndex: 'watchPulse',
        width: 'auto'
    }, {
        text: lang('Хранилище'),
        dataIndex: 'allSpace',
        width: 'auto'
    }, {
        text: lang('Занято'),
        dataIndex: 'holdSpace',
        width: 'auto'
    }, {
        text: lang('Свободно'),
        dataIndex: 'freeSpace',
        width: 'auto'
    }, {
        text: lang('CPU'),
        dataIndex: 'cpuLoad',
        width: 'auto'
    }, {
        text: lang('GPU'),
        dataIndex: 'gpuLoad',
        width: 'auto'
    }, {
        text: lang('Зарядка устройства'),
        dataIndex: 'akbDeviceCharge',
        width: 'auto'
    }, {
        text: lang('Зарядка часов'),
        dataIndex: 'akbWatchCharge',
        width: 'auto'
    }, {
        text: lang('Дата'),
        dataIndex: 'telemetryDate',
        width: '150'
    }/*, {
        text: lang('Время на устройстве'),
        dataIndex: 'timeOnDevice',
        width: 'auto'
    }*/, {
        text: lang('Внешняя камера'),
        dataIndex: 'externalCameraOnline',
        width: 'auto'
    }, {
        text: lang('Зарядка'),
        dataIndex: 'charging',
        width: 'auto'
    }, {
        text: lang('Подключение к WiFi'),
        dataIndex: 'wifiConnected',
        width: 'auto'
    }, {
        text: lang('Подключение к GSM'),
        dataIndex: 'gsmConnected',
        width: 'auto'
    }]
});