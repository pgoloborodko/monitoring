Ext.define('Landau.devices.tele.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.devices-tele-store',

    model: 'Landau.devices.tele.Model',

    proxy: {
        type: 'ajax',
        url: '/devices/telemetry',
        reader: {
            type: 'json',
            rootProperty: 'data.content', //.content
            totalProperty: 'data.totalElements',
            metaProperty: 'data.size'
        },
        extraParams: {
            sort : [
                'telemetryDate,desc'
            ]
        }
    }
});