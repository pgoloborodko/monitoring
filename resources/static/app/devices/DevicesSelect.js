Ext.define('Landau.devices.DevicesSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'devices-select',
    requires: [
        'Landau.devices.list.View'
    ],

    config: {
        title:  lang('Выбор устройства'),
        placeholder: lang('Устройство...'),
        view: 'devices-list-view',
        displayTpl: '{name}',
    }
});