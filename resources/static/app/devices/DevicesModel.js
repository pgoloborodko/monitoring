Ext.define('Landau.devices.DevicesModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'cityName', mapping: 'city.name'},
        {name: 'districtName', mapping: 'district.name'},
        {name: 'stateBoolean', mapping: 'state'},
        {name: 'lastOnline', convert: function (value, record) {
            return Ext.Date.format(new Date(value), "m-d H:i:s");
        }},
        {name: 'gps', convert: function (value, record) {
            return value == undefined ? false : value;
        }},
        {name: 'charge', convert: function (value, record) {
                return value + "%";
        }},
        {name: 'model', convert: function (value, record) {
            if(value != undefined) {
                return value.manufacturer + ", " + value.model + ", " + value.firmware;
            }
        }},
        {name: 'state', convert: function (value, record) {
            if(value == undefined &&  record.data.online || value) {
                return "<span class='devices-status' style='background:#73b51e'></span>";
            }
            return "<span class='devices-status' style='background:#cf4c35'></span>";
        }},
        {name: 'companyName', mapping: 'company.name'}
    ]
});