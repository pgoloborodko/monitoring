Ext.define("Landau.devices.models.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'devices-models-grid-view',

    height: '100%',

    requires: [
        'Landau.devices.models.DevicesModelsStore'
    ],

    store: {
        type: 'devices-models-store',
        autoLoad: true
    },

    columns: [{
        text: lang('Производитель'),
        dataIndex: 'manufacturer',
        flex: 1
    }, {
        text: lang('Модель'),
        dataIndex: 'model',
        flex: 1
    }, {
        text: lang('Прошивка'),
        dataIndex: 'firmware',
        flex: 1
    }, {
        text: lang('Аккумулятор, мАч'),
        dataIndex: 'batteryCapacity',
        flex: 1
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: [{
                type: 'gear',
                tooltip: lang('Редактировать'),
                handler: 'onEdit',
            }]
        }
    }]
});


