Ext.define("Landau.devices.models.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-models-controller",


    onEdit: function (grid, info) {
        var dialog = Ext.create("Landau.devices.models.save.View", {
            parentController: this
        });

        dialog.show();

        var form = dialog.lookup('form');

        form.load({
            url: '/devices/models/save?id=' + info.record.id,
            success: function (ev, response) {
                form.setValues(response.data);
            },
            failure: function () {
                console.log(arguments);
            }
        });
    },

    onOpenDevice: function () {
        //var f

    },

    save: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.devices.models.save.View", {
                parentController: that
            }).show();

            Ext.Viewport.setMasked(false);
        }, 10);
    },

    reloadStore: function () {
        this.getView().getItems().items[0].store.reload();
    },

    doFilter: function () {
        var company = this.lookup("company");
        var city = this.lookup("city");
        var district = this.lookup("district");

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue()
            }
        });
    }
});