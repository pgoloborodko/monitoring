Ext.define("Landau.devices.models.list.View", {
    extend: 'Ext.Dialog',
    controller: 'devices-models-controller',
    xtype: 'devices-models-list-view',

    requires: [
        'Landau.devices.models.list.Controller',
        'Landau.devices.models.list.ViewGrid'
    ],

    maximized: true,
    height: '100%',
    closable: true,
    padding: 0,

    defaultType: 'panel',
    title: lang("Список моделей устройств"),

    tbar: [{
        iconCls: 'x-fa fa-plus',
        text: lang('Добавить новую модель'),
        handler: 'onEdit',
        ui: 'action',
    } ],

    items: [{
        height: "100%",
        xtype: 'devices-models-grid-view',
        reference: "grid"
    }]
});