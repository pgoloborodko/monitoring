Ext.define('Landau.devices.models.DevicesModelsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.devices-models-store',

    model: 'Landau.devices.models.DevicesModelsModel',

    proxy: {
        type: 'ajax',
        url: '/devices/models',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});