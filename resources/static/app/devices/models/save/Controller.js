Ext.define('Landau.devices.models.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.devices-models-save-controller',


    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/devices/models/save',
                scope: this,
                success: this.successCallback,
                failure: this.failureCallback
            });
        }
    },

    successCallback: function () {
        this.getView().parentController.reloadStore();
        this.getView().destroy();
    },

    failureCallback: function (scope, result) {
        var form = this.lookup('form');

        console.log(result);

        for(var i=0; i < result.data.length; i++) {
            this.lookup(result.data[i].field).setError(result.data[i].code);
        }
    }
});