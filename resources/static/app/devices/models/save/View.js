Ext.define('Landau.devices.models.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-models-save-view',
    controller: 'devices-models-save-controller',
    title: lang('Сохранение модели регистратора'),

    requires: [
        'Landau.devices.models.save.Controller'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        },{
            xtype: 'containerfield',
            label: lang('Производитель'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'manufacturer',
                placeholder: lang('Производитель'),
                required: true
            }]
        },{
            xtype: 'containerfield',
            label: lang('Модель'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'model',
                placeholder: lang('Модель'),
                required: true
            }]
        },{
            xtype: 'containerfield',
            label: lang('Прошивка'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'firmware',
                placeholder: lang('Прошивка'),
                required: true
            }]
        }, {
            xtype: 'containerfield',
            label: lang('Аккумулятор, мАч'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'batteryCapacity',
                placeholder: lang('Аккумулятор, мАч'),
                required: true
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});