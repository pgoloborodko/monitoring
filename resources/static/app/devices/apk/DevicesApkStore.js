Ext.define('Landau.devices.apk.DevicesApkStore', {
    extend: 'Ext.data.Store',
    alias: 'store.devices-apk-store',

    model: 'Landau.devices.apk.DevicesApkModel',

    sorters: [{
        property: 'state',
        direction: 'ASC'
    }],

    proxy: {
        type: 'ajax',
        url: '/devices/apk',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        },
        extraParams: {
            sort : [
                'id,desc'
            ]
        }
    }
});