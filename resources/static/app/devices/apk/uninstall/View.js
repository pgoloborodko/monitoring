Ext.define('Landau.devices.apk.uninstall.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-apk-uninstall-view',
    controller: 'devices-apk-uninstall-controller',
    title: lang('Удаление APK'),

    requires: [
        'Landau.devices.apk.uninstall.Controller',
        'Landau.devices.DevicesSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'type',
            value: 'UNINSTALL'
        },{
            xtype: 'containerfield',
            label: lang('Устройство'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'devices-select',
                reference: 'device',
                margin: '0 10 0 0',
                name: 'device'
            }]
        },{
            xtype: 'containerfield',
            label: lang('APK файл'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'pack',
                placeholder: lang('Название пакета'),
                required: true
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});