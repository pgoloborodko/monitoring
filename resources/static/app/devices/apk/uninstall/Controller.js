Ext.define('Landau.devices.apk.uninstall.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.devices-apk-uninstall-controller',


    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/devices/apk/save',
                scope: this,
                success: this.successCallback,
                failure: this.failureCallback
            });
        }
    },

    successCallback: function (scope, result) {
        this.getView().destroy();

        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        console.log(result.data);

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.devices.apk.log.View", {
                viewModel: {
                    data: {
                        deviceApk: result
                    }
                }
            }).show();

            Ext.Viewport.setMasked(false);
        }, 10);
    },

    failureCallback: function (scope, result) {
        var form = this.lookup('form');

        for(var i=0; i < result.length; i++) {
            this.lookup(result[i].field).setError(result[i].code);
        }
    }
});