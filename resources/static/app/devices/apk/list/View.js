Ext.define("Landau.devices.apk.list.View", {
    extend: 'Ext.Dialog',
    controller: 'devices-apk-controller',

    requires: [
        'Landau.devices.apk.list.Controller',
        'Landau.devices.apk.list.ViewGrid',
        'Landau.devices.DevicesSelect'
    ],


    maximized: true,
    height: '100%',
    closable: true,
    title: lang("APK Приложения"),

    defaultType: 'panel',

    items: [{
        height: "100%",
        xtype: 'devices-apk-grid-view',
        reference: "grid"
    }],

    padding: 0,

    tbar: [{
        xtype: 'devices-select',
        reference: 'device',
        margin: '0 10 0 0'
    }, {
        xtype: 'selectfield',
        name: 'type',
        reference: "type",
        placeholder: lang('Фильтация по типу'),
        options: [{
            text: lang('-- Без фильтра --'),
            value: ''
        }, {
            text: lang('Установка'),
            value: 'INSTALL'
        }, {
            text: lang('Удаление'),
            value: 'UNINSTALL'
        }]
    }, {
        xtype: 'datepickerfield',
        maxValue: new Date(),
        value: new Date(),
        margin: '0 10 0 0',
        reference: 'date',
        dateFormat: 'd/m/Y',
        width: 100
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter',
        margin: '0 0 0 10'
    }, {
        xtype: 'spacer'
    }, {
        iconCls: 'x-fa fa-plus',
        text: lang('Установить приложение'),
        handler: 'doInstall',
        ui: 'confirm',
    }, {
        iconCls: 'x-fa fa-minus',
        text: lang('Удалить приложение'),
        handler: 'doUninstall',
        ui: 'decline',
        margin: '0 0 0 10'
    }]
});