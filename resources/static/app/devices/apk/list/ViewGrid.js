Ext.define("Landau.devices.apk.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'devices-apk-grid-view',

    height: '100%',

    requires: [
        'Landau.devices.apk.DevicesApkStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'devices-apk-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Устройство'),
        dataIndex: 'device',
        flex: 1
    }, {
        text: lang('Тип'),
        dataIndex: 'type',
        flex: 1
    }, {
        text: lang('Дата'),
        dataIndex: 'added',
        flex: 1
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: [{
                type: 'search',
                handler: 'onLog',
                tooltip: lang('Лог')
            }]
        }
    }]
});