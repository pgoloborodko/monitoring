Ext.define("Landau.devices.apk.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-apk-controller",

    onEdit: function (grid, info) {
        this.openForm("Landau.devices.apk.save.View", '/devices/apk/save?id=' + info.record.id);
    },

    save: function () {
        this.openForm("Landau.devices.apk.save.View");
    },

    doFilter: function () {
        var device = this.lookup("device");
        var type = this.lookup("type");
        var date = this.lookup("date");

        this.lookup("grid").getStore().reload({
            params: {
                device: device.getValue(),
                type: type.getValue(),
                added: Ext.Date.format(date.getValue(), 'Y-m-d')
            }
        });
    },

    onLog: function(grid, info) {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        console.log(info.record.data);

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.devices.apk.log.View", {
                viewModel: {
                    data: {
                        deviceApk: info.record.data
                    }
                }
            }).show();

            Ext.Viewport.setMasked(false);
        }, 10);
    },

    doInstall: function() {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.devices.apk.install.View").show();

            Ext.Viewport.setMasked(false);
        }, 10);
    },

    doUninstall: function() {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.devices.apk.uninstall.View").show();

            Ext.Viewport.setMasked(false);
        }, 10);
    }
});