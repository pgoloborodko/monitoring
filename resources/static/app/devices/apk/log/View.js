Ext.define("Landau.devices.apk.log.View", {
    extend: 'Ext.Dialog',

    controller: {
        doRedo: function () {
            console.log(this);
            this.lookup("grid").getStore().reload();
        }
    },

    requires: [
        'Landau.devices.apk.log.ViewGrid',
        'Landau.devices.apk.log.DevicesApkLogStore'
    ],

    maximized: true,
    height: '100%',
    closable: true,
    padding: 0,
    bind: {
        title: 'Лог'
    },

    defaultType: 'panel',

    tbar: [ {
        iconCls: 'x-fa fa-retweet',
        text: lang('Обновить состояние'),
        handler: 'doRedo',
        ui: 'action'
    }],

    items: [{
        height: "100%",
        xtype: 'devices-apk-log-grid-view',
        reference: "grid",
        bind: {
            viewModel: {
                data: {
                    deviceApk: '{deviceApk}'
                }
            }
        }
    }]
});