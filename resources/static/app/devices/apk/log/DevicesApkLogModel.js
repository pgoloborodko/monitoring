Ext.define('Landau.devices.apk.log.DevicesApkLogModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'added', convert: function (value, record) {
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }},
        {name: 'type', convert: function (value, record) {
                switch (value) {
                    case "installRoot":
                        return lang("Автоматическая установка начата");
                    case "installManual":
                        return lang("Ручная установка начата");
                    case "installed":
                        return lang("Установка закончена");
                    case "uninstallRoot":
                        return lang("Автоматическое удаление начато");
                    case "uninstallManual":
                        return lang("Ручное удаление начато");
                    case "uninstalled":
                        return lang("Удаление закончено");
                    case "download":
                        return lang("Загрузка начата");
                    case "downloaded":
                        return lang("Загрузка закончена");
                    case "error":
                        return lang("Ошибка");
                }
            }},
    ]
});