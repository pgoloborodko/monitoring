Ext.define('Landau.devices.apk.log.DevicesApkLogStore', {
    extend: 'Ext.data.Store',
    alias: 'store.devices-apk-log-store',

    model: 'Landau.devices.apk.log.DevicesApkLogModel',

    proxy: {
        type: 'ajax',
        url: '/devices/apk/save',
        reader: {
            type: 'json'
        }
    }
});