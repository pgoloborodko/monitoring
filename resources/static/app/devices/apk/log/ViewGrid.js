Ext.define("Landau.devices.apk.log.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'devices-apk-log-grid-view',

    height: '100%',

    requires: [
        'Landau.devices.apk.log.DevicesApkLogStore'
    ],

    bind: {
        store: {
            type: 'devices-apk-log-store',
            autoLoad: true,
            proxy: {
                extraParams : {
                    id :'{deviceApk.id}'
                }
            }
        },
    },

    columns: [{
        text: lang('Тип'),
        dataIndex: 'type',
        flex: 1
    }, {
        text: lang('Сообщение'),
        dataIndex: 'message',
        flex: 1
    }, {
        text: lang('Дата'),
        dataIndex: 'added',
        flex: 1
    }]
});