Ext.define('Landau.devices.apk.DevicesApkModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'device', mapping: 'device.name'},
        {name: 'added', convert: function (value, record) {
                return Ext.Date.format(new Date(value), "Y-m-d H:i:s");
            }},
        {name: 'type', convert: function (value, record) {
                switch (value) {
                    case "INSTALL":
                        return lang("Установка");
                    case "UNINSTALL":
                        return lang("Удаление");
                }
            }},
    ]
});