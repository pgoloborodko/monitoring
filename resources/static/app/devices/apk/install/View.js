Ext.define('Landau.devices.apk.install.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-apk-install-view',
    controller: 'devices-apk-install-controller',
    title: lang('Установка APK'),

    requires: [
        'Landau.devices.apk.install.Controller',
        'Landau.devices.DevicesStore'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'type',
            value: 'INSTALL'
        },{
            xtype: 'containerfield',
            label: lang('Устройство'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'devices-',
                valueField: 'id',
                displayField: 'name',
                forceSelection: true,
                required: true,
                queryMode: 'remote',
                placeholder: lang('Устройство...'),
                reference: 'device',
                store: {
                    type: 'devices-store'
                },
                itemTpl: '{name}',
                margin: '0 10 0 0',
                name: 'device'
            }]
        },{
            xtype: 'containerfield',
            label: lang('APK файл'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                flex: 1,
                xtype:'filefield',
                label: '',
                name: 'apkFile',
                reference: 'apkFile'
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});