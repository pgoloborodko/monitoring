Ext.define('Landau.devices.apk.install.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.devices-apk-install-controller',


    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/devices/apk/save',
                scope: this,
                success: this.successCallback
            });
        }
    },

    successCallback: function (scope, result) {
        this.onCancel();

        Ext.create("Landau.devices.apk.log.View", {
            viewModel: {
                data: {
                    deviceApk: result
                }
            }
        }).show();
    }
});