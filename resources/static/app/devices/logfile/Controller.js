Ext.define("Landau.devices.logfile.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-logfile-controller",

    init: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        Ext.Ajax.request({
            url: '/devices/log/file',
            method:'get',
            params : {device: this.getViewModel().get("device").id, file: this.getViewModel().get('filename')},
            scope: this,
            failure: 'failureCallback',
            success: 'successCallback'
        });

        //console.log(this.getViewModel())
        //https://localhost/devices/request/log/file?terminal=5&file=terminal.log.2019-06-21.0.gz
    },

    onCancel: function () {
        this.getView().destroy();
    },

    failureCallback: function () {
        //console.log('++++')
        //console.log(arguments)
    },

    successCallback: function (response) {

        Ext.Viewport.setMasked(false);
        //console.log(response.responseText)
        this.getView().setItems([
            Ext.create("Ext.panel.Panel", {
                html: '<pre>' + response.responseText + '</pre>'
            })
        ])
    }
});