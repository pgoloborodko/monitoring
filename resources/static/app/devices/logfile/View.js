Ext.define('Landau.devices.logfile.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-logfile-view',
    controller: 'devices-logfile-controller',
    title: lang('Просмотр лог-файла'),

    closable: true,
    maximized: true,
    padding: 0,
    height: '100%',

    requires: [
        'Landau.devices.logfile.Controller'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    scrollable: true,

    onCancel: function () {
        this.destroy();
    },
/*
    items: [{
        xtype: 'panel',
        centered: true,
        html: '<b>Подождите! Идет загрузка...</b>'
    }]*/
});