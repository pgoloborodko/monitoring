Ext.define("Landau.devices.list.View", {
    extend: 'Ext.Panel',
    controller: 'devices-controller',
    xtype: 'devices-list-view',

    requires: [
        'Landau.devices.list.Controller',
        'Landau.devices.list.ViewGrid',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        height: "100%",
        xtype: 'devices-grid-view',
        reference: "grid"
    }],

    tbar: [{
        iconCls: 'x-fa fa-sitemap',
        text: lang('Модели'),
        handler: 'doModels'
    }, {
        iconCls: 'x-fa fa-android',
        text:  lang('Приложения'),
        menu: [{
            text:  lang('Установить на выбранные')
        },{
            text:  lang('Удалить на выбранных')
        }]
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'serial',
        placeholder: lang('Серийный'),
        margin: '0 10 0 0'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: lang('Название'),
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        xtype: 'districts-select',
        reference: 'district',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]

});