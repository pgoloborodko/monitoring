Ext.define("Landau.devices.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'devices-grid-view',
    excludeTest: true,
    height: '100%',

    requires: [
        'Landau.devices.DevicesStore',
        'Landau.common.Pageable',
        'Ext.grid.plugin.RowOperations'
    ],

    store: {
        type: 'devices-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        dataIndex: 'state',
        width: 30,
        cell: {
            encodeHtml: false
        },
        sorter: {
            property: 'online'
        }
    }, {
        text: lang('Серийный номер'),
        dataIndex: 'serial',
        flex: 1,
        minWidth: 100
    }, {
        text: lang('Имя'),
        dataIndex: 'name',
        flex: 1,
        minWidth: 100
    }, {
        text: lang('В сети'),
        dataIndex: 'lastOnline',
        width: 120
    }, {
        text: lang('Заряд'),
        dataIndex: 'charge',
        width: 75
    }, {
        text: lang('Город'),
        dataIndex: 'cityName',
        width: 140,
        sorter: {
            property: 'city'
        }
    }, {
        text: lang('Район'),
        dataIndex: 'districtName',
        width: 140,
        sorter: {
            property: 'district'
        }
    }, {
        text: lang('Компания'),
        dataIndex: 'companyName',
        width: 140,
        sorter: {
            property: 'company'
        }
    }, {
        text: lang('Модель'),
        dataIndex: 'model',
        width: 100
    }, {
        text: lang('Версия'),
        dataIndex: 'version',
        width: 70
    }, {
        text: lang('Действия'),
        width: 135,
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,
        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: [{
                type: 'print',
                handler: 'onLogs',
                tooltip: lang('Лог файлы')
            }, {
                type: 'search',
                handler: 'onTele',
                tooltip: lang('Телеметрия')
            }, {
                type: 'plus',
                tooltip: lang('Установить APK'),
                handler: 'onInstallApk',
            }, {
                type: 'minus',
                tooltip: lang('Удалить APK'),
                handler: 'onUninstallApk',
            }, {
                type: 'gear',
                tooltip: lang('Редактировать'),
                handler: 'onEdit',
            }]
        }
    }]
});


