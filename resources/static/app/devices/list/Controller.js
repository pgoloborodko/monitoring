Ext.define("Landau.devices.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-controller",

    save: function () {
        this.openForm("Landau.devices.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.devices.save.View", '/devices/save?id=' + info.record.id);
    },

    doModels: function () {
        this.openForm("Landau.devices.models.list.View");
    },

    doApks: function () {
        this.openForm("Landau.devices.apk.list.View");
    },

    onLogs: function(grid, info) {
        this.openModelForm("Landau.devices.loglist.View", {
            device: info.record.data
        });
    },

    onTele: function(grid, info) {
        this.openModelForm("Landau.devices.tele.View", {
            device: info.record.data
        });
    },

    onInstallApk: function(grid, info) {
        this.openForm("Landau.devices.apk.install.View").setValues({
            device: info.record.data
        });
    },

    onUninstallApk: function(grid, info) {
        this.openForm("Landau.devices.apk.uninstall.View").setValues({
            device: info.record.data
        });
    },

    doFilter: function () {
        let serial = this.lookup("serial");
        let name = this.lookup("name");
        let company = this.lookup("company");
        let city = this.lookup("city");
        let district = this.lookup("district");

        this.lookup("grid").getStore().reload({
            params: {
                serial: serial.getValue(),
                name: name.getValue(),
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue()
            }
        });
    }
});