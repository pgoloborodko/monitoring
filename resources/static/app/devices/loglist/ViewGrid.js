Ext.define("Landau.devices.loglist.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'devices-loglist-grid-view',

    height: 500,

    requires: [
        'Landau.devices.loglist.Store',
        'Ext.grid.plugin.Exporter',
    ],

    plugins: [{
        type: 'gridexporter'
    }],

    bind: {
        store: {
            type: 'devices-loglist-store',
            autoLoad: true,
            proxy: {
                extraParams : {
                    device :'{device.id}'
                }
            }
        },
    },

    columns: [{
        text: lang('Название'),
        dataIndex: 'name',
        flex: 2,
        exportStyle: {
            autoFitWidth: true,
            width: 400
        }
    }, {
        text: lang('Размер'),
        dataIndex: 'size',
        flex: 1,
        exportStyle: {
            autoFitWidth: true,
            width: 200
        }
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        cell: {
            tools: [{
                type: 'maximize',
                handler: 'onLogs',
                tooltip: lang('Просмотр')
            }]
        }
    }]
});