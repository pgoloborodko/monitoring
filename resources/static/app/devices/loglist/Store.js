Ext.define('Landau.devices.loglist.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.devices-loglist-store',

    model: 'Landau.devices.loglist.Model',

    proxy: {
        type: 'ajax',
        url: '/devices/log/list', //поменять пятерку
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});