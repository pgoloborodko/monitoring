Ext.define('Landau.devices.loglist.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-loglist-view',
    controller: 'devices-loglist-controller',
    title: lang('Список лог-файлов'),

    requires: [
        'Landau.devices.loglist.Controller',
        'Landau.devices.loglist.ViewGrid'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        height: 500,
        width: 500,
        xtype: 'devices-loglist-grid-view',
        reference: 'grid',
        bind: {
            viewModel: {
                data: {
                    device: '{device}'
                }
            }
        }
    }],

    buttons: [{
        iconCls: 'x-fa fa-file-excel-o',
        text: lang('XLS'),
        handler: 'exportDocument'
    }, {
        text: lang('Закрыть'),
        handler: 'onCancel'
    }]
});