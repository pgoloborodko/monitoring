Ext.define("Landau.devices.loglist.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-loglist-controller",

    onCancel: function () {
        this.getView().destroy();
    },

    exportDocument: function (menuitem) {
        var pivotgrid = this.lookup('grid');

        pivotgrid.saveDocumentAs({
            type: 'xlsx',
            title:  lang('Лог файлы устройства') + " " + this.getViewModel().get("device").name + "(" + this.getViewModel().get("device").serial + ")",
            fileName: 'logs_device_' + this.getViewModel().get("device").name + '-' + this.getViewModel().get("device").serial + '.xlsx'
        });
    },

    onLogs: function(grid, info) {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.devices.logfile.View", {
                viewModel: {
                    data: {
                        device: that.getViewModel().get("device"),
                        filename: info.record.data.name
                    }
                }
            }).show();

            Ext.Viewport.setMasked(false);
        }, 10);
    }
});