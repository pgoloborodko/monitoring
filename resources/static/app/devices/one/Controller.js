Ext.define("Landau.devices.one.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.devices-one-controller",

    init: function () {
        this.getView().getDevice().deviceId = this.getView().getDevice().id;

        this.autoUpdate({
            data: this.getView().getDevice()
        });
        this.findPatrol();
        this.findTransport();

        var that = this;

        Messages.bind({
            action: ["message.device", "connect", "disconnect"],
            callback: this.autoUpdate,
            scope: this
        });

        this.lookup("hbox").insert(0, {
            reference: 'sldp-controls',
            xtype: 'sldp-controls',
            width: 720,
            height: 480,
            play: true,
            device: this.getView().getDevice()
        });
    },

    destroy: function () {
        Messages.unbind({
            action: ["message.device", "connect", "disconnect"],
            callback: this.autoUpdate,
        });

        Messages.trigger("info.dialogDeviceClose", {});
    },

    autoUpdate: function (data) {

        //console.log('+++++++++++++++++++++++++++++++++++')
        //console.dir(data)
        //console.log(this.getView().device.id)
        //console.log(data.deviceId)

        if(this.getView().getDevice().id != data.device.deviceId) return;

        this.lookup("status").setValue(data.device.online ? "В сети" : "Не в сети");
        this.lookup("serial").setValue(data.device.serial);
        this.lookup("charge").setValue(data.device.charge);
    },

    findPatrol: function () {
        Ext.Ajax.request({
            url: '/users/find/getOneByDevice?id=' + this.getView().getDevice().id,
            scope: this,
            failure: function () {
                Ext.Msg.alert(lang("Ошибка!"), lang("Возникла ошибка при запросе данных патрульного!"));
                this.lookup("patrol").setValue("Ошибка запроса!");
            },
            success: function(response) {
                var decode = Ext.decode(response.responseText);

                if (decode.success) {
                    this.lookup("patrol").setValue(decode.data.lastName + " " + decode.data.firstName + " " + decode.data.secondName);
                    //console.log(decode);
                }
                else {
                    this.lookup("patrol").setValue(lang("Без патрульного"));
                }
            }
        });
    },

    findTransport: function () {
        Ext.Ajax.request({
            url: '/transport/find/getOneByDevice?id=' + this.getView().getDevice().id,
            scope: this,
            failure: function () {
                Ext.Msg.alert(lang("Ошибка!"), lang("Возникла ошибка при запросе данных транспорта!"));
                this.lookup("transport").setValue(lang("Ошибка запроса!"));
            },
            success: function(response) {
                var decode = Ext.decode(response.responseText);

                if (decode.success) {
                    this.lookup("transport").setValue(decode.data.name + " " + decode.data.numbers);
                    //console.log(decode);
                }
                else {
                    this.lookup("transport").setValue(lang("Без транспорта"));
                }
            }
        });
    }
})