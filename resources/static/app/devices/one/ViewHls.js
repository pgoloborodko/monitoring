Ext.define("Landau.devices.one.ViewHls", {
    extend: 'Ext.Dialog',
    xtype: 'one-hld-view',
    controller: 'devices-one-controller',
    title: lang('Просмотр архива регистратора'),

    requires: [
        'Ext.data.StoreManager',
        'Landau.devices.one.Controller',
        'Landau.devices.DevicesStore',
        'Landau.video.hls.View'
    ],

    autoSize: true,
    closable: true,

    items: [{
        layout: 'hbox',
        items: [{
            xtype: 'hls',
            width: 720,
            height: 480,
        },{
            hidden: true,
            margin: '0 0 0 10',
            xtype: 'panel',
            defaults: {
                labelAlign: 'placeholder',
                xtype: 'textfield',
                readOnly: true
            },
            items: [{
                label: lang('Статус регистратора'),
                value: lang('Обновление...'),
                reference: 'status'
            }, {
                label: lang('Серийный номер'),
                value: lang('Обновление...'),
                reference: 'serial'
            }, {
                label: lang('Патрульный'),
                value: lang('Обновление...'),
                reference: 'patrol'
            }, {
                label: lang('Транспорт'),
                value: lang('Обновление...'),
                reference: 'transport'
            }, {
                xtype: 'containerfield',
                items: [{
                    label: lang('Заряд'),
                    value: '100%',
                    margin: '0 10 0 0',
                    readOnly: true,
                    reference: 'charge'
                },{
                    label: lang('Пульс'),
                    value: '84',
                    readOnly: true,
                    reference: 'pulse'
                }]
            },{
                xtype: 'list'
            }]
        }]
    }]
})