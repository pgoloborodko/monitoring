Ext.define("Landau.devices.one.ViewSldp", {
    extend: 'Ext.Dialog',
    xtype: 'one-sldp-view',
    controller: 'devices-one-controller',
    title: lang('Просмотр регистратора'),

    requires: [
        'Ext.data.StoreManager',
        'Landau.devices.one.Controller',
        'Landau.devices.DevicesStore',
        'Landau.video.sldp.controls.View'
    ],

    bodyPadding: 20,
    autoSize: true,
    closable: true,

    config: {
        device: null,
    },



    items: [{
        layout: 'hbox',
        reference: 'hbox',
        items: [{
            margin: '0 0 0 10',
            xtype: 'panel',
            defaults: {
                labelAlign: 'placeholder',
                xtype: 'textfield',
                readOnly: true
            },
            items: [{
                label: lang('Статус регистратора'),
                value: lang('Обновление...'),
                reference: 'status'
            }, {
                label: lang('Серийный номер'),
                value: lang('Обновление...'),
                reference: 'serial'
            }, {
                label: lang('Патрульный'),
                value: lang('Обновление...'),
                reference: 'patrol'
            }, {
                label: lang('Транспорт'),
                value: lang('Обновление...'),
                reference: 'transport'
            }, {
                xtype: 'containerfield',
                items: [{
                    label: lang('Заряд'),
                    value: '100%',
                    margin: '0 10 0 0',
                    readOnly: true,
                    reference: 'charge'
                },{
                    label: lang('Пульс'),
                    value: '~',
                    readOnly: true,
                    reference: 'pulse'
                }]
            },{
                xtype: 'list'
            }]
        }]
    }]
})