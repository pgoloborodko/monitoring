Ext.define('Landau.devices.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'devices-save-view',
    controller: 'devices-save-controller',
    title: lang('Сохранение регистратора'),

    requires: [
        'Landau.devices.save.Controller',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        },{
            xtype: 'containerfield',
            label: lang('Название и компания'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'name',
                placeholder: lang('Имя'),
                required: true,
                margin: '0 10 0 0'
            }, {
                xtype: 'companies-select',
                name: 'company',
                reference: 'company'
            }]
        },{
            xtype: 'containerfield',
            label: lang('Город и район'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'cities-select',
                name: 'city',
                reference: 'city',
                /*listeners: {
                    change: 'changeCity'
                },*/
                margin: '0 10 0 0'
            }, {
                xtype: 'districts-select',
                name: 'district',
                reference: 'district'
            }]
        },{
            xtype: 'containerfield',
            label: lang('Сим-карта'),
            defaults: {
                flex: 1
            },
            items: [{
                name: 'simSerial',
                placeholder: lang('Серийный'),
                margin: '0 10 0 0',
                readOnly: true
            }, {
                name: 'simNumber',
                placeholder: lang('Номер'),
                readOnly: true
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});