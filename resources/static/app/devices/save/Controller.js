Ext.define('Landau.devices.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.devices-save-controller',


    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/devices/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});