Ext.define('Landau.devices.DevicesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.devices-store',

    model: 'Landau.devices.DevicesModel',
    remoteSort: true,

    sorters: [{
        property: 'lastOnline',
        direction: 'ASC'
    }],

    proxy: {
        type: 'ajax',
        url: '/devices',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});