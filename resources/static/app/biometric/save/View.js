Ext.define('Landau.biometric.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'biometric-save-view',
    controller: 'biometric-save-controller',
    title: lang('Сохранение биометрического комплекса'),

    requires: [
        'Landau.biometric.save.Controller',
        'Landau.companies.CompaniesSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        },{
            xtype: 'containerfield',
            label: lang('Название биометрического комплекса'),
            required: true,
            items: [{
                flex: 1,
                name: 'name',
                placeholder: lang('Название'),
                required: true
            }]
        },{
            xtype: 'containerfield',
            label: lang('Токен'),
            required: true,
            items: [{
                flex: 1,
                name: 'token',
                placeholder: lang('Токен'),
                required: true
            }]
        },{
            xtype: 'containerfield',
            label: lang('Компания'),
            required: true,
            items: [{
                flex: 1,
                xtype: 'companies-select',
                required: true,
                name: 'company'
            }
            ]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});