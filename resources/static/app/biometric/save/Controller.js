Ext.define('Landau.biometric.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.biometric-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/biometric/save',
                scope: this,
                success: 'onCancel'
            });
        }
    }
});