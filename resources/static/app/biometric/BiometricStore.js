Ext.define('Landau.biometric.BiometricStore', {
    extend: 'Ext.data.Store',
    alias: 'store.biometric-store',

    model: 'Landau.biometric.BiometricModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/biometric',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});