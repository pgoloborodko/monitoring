Ext.define('Landau.biometric.BiometricModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'company', mapping: 'company.name'},
        {name: 'state', convert: function (value, record) {
                if(value == undefined &&  record.data.online || value) {
                    return "<span class='devices-status' style='background:#73b51e'></span>";
                }
                return "<span class='devices-status' style='background:#cf4c35'></span>";
        }},
    ]
});