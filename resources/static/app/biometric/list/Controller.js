Ext.define("Landau.biometric.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.biometric-controller",

    onEdit: function (grid, info) {
        this.openForm("Landau.biometric.save.View", '/biometric/save?id=' + info.record.id);
    },

    doFilter: function () {
        var company = this.lookup("company");
        var city = this.lookup("city");
        var district = this.lookup("district");

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue()
            }
        });
    }
});