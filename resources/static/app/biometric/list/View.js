Ext.define("Landau.biometric.list.View", {
    extend: 'Ext.Panel',
    controller: 'biometric-controller',

    requires: [
        'Landau.biometric.list.Grid',
        'Landau.biometric.list.Controller',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],


    height: "100%",
    defaultType: 'panel',

    items: [{
        height: "100%",
        xtype: 'biometric-grid-view',
        reference: 'grid'
    }],

    tbar: [{
        xtype: 'spacer'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        xtype: 'districts-select',
        reference: 'district',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});