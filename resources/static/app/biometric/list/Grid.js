Ext.define("Landau.biometric.list.Grid", {
    extend: 'Ext.grid.Grid',
    xtype: 'biometric-grid-view',

    height: '100%',

    requires: [
        'Landau.biometric.BiometricStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'biometric-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    },  {
        text: lang('Онлайн'),
        dataIndex: 'state',
        width: 100,
        cell: {
            encodeHtml: false
        },
        sorter: {
            property: 'online'
        }
    }/*, {
        text: lang('Токен'),
        dataIndex: 'token',
        flex: 1
    }*/, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 200
    },  {
        text: lang('Тип'),
        dataIndex: 'type',
        width: 100
    },  {
        text: lang('Версия'),
        dataIndex: 'version',
        width: 100
    }/*,  {
        text: lang('Режим работы'),
        dataIndex: 'mode',
        width: 200
    }*/, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit',
            }
        }
    }]
});