Ext.define('Landau.calendar.CalendarStore', {
    extend: 'Ext.data.Store',
    alias: 'store.landau-calendar-store',
    model: 'Landau.calendar.CalendarModel',

    proxy: {
        type: 'ajax',
        // url: '/events/findForMonth?date=2019-12-11&listOfUserIds=1&listOfUserIds=2&listOfUserIds=3&typesOfEvents=EXTRACTED&typesOfEvents=INSERTED',
        url: '/events/calendar',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            // totalProperty: 'totalElements',
            // metaProperty: 'size'
        }
    }
});