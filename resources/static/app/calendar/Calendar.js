Ext.define("Landau.calendar.Calendar", {
    extend: 'Ext.Panel',
    xtype: 'landau-calendar',
    controller: 'calendar-controller',

    requires: [
        'Landau.calendar.CalendarController',
        'Landau.calendar.CalendarStore'
    ],

    config: {
        date: null
    },

    height: '100%',
    width: '100%',
    days: [lang("ПН"), lang("ВТ"), lang("СР"), lang("ЧТ"), lang("ПТ"), lang("СБ"), lang("ВС")],
    months: [lang("Январь"), lang("Февраль"),lang("Март"), lang("Апрель"), lang("Май"), lang("Июнь"), lang("Июль"),
        lang("Август"), lang("Сентябрь"),lang( "Октябрь"), lang("Ноябрь"), lang("Декабрь")],

    tbar: [{
        xtype: 'button',
        text: lang('сегодня'),
        ui: 'action',
        handler: 'today',
        margin: '0 20 0 0'
    },{
        xtype: 'button',
        text: '<',
        handler: 'prev'
    }, {
        xtype: 'button',
        reference: 'month',
        text: '~',
        style: 'color: black',
        disabled: true,
    }, {
        xtype: 'button',
        reference: 'year',
        text: '~',
        style: 'color: black',
        disabled: true,
    }, {
        xtype: 'button',
        text: '>',
        handler: 'next'
    }],

    currentDate: new Date(),
    selectDate: new Date(),
    selectCell: null,

    weekCount: function (year, month_number) {
        let lastOfMonth = new Date(year, month_number + 1, 0);
        let firstDay = (new Date(year, month_number, 0)).getDay();

        return Math.ceil( (firstDay + lastOfMonth.getDate()) / 7);
    },

    changeMonth: function (year, month) {
        this.lookup("month").setText(this.months[month]);
        this.lookup("year").setText(year);

        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        this.store.load({
            scope: this,
            params: {
                startDate: new Date(year, month, 1),
                endDate: new Date(year, month + 1, 0),
            },
            callback: function(records) {
                Ext.Viewport.setMasked(false);

                let firstDay = (new Date(year, month, 0)).getDay();
                let daysInMonth = this.daysInMonth(year, month);
                let previousDaysInMonth = this.daysInMonth(year, month - 1);

                let tbl = document.createElement("div");
                tbl.className = "landau-calendar-wrapper";
                let date = 1, cell, cellText, event;

                let row = document.createElement("div");
                row.className = "landau-calendar-row days";
                for (let j = 0; j < 7; j++) {
                    cell = document.createElement("div");
                    cell.className = "landau-calendar-cell day";
                    cellText = document.createTextNode(this.days[j]);
                    cell.appendChild(cellText);
                    row.appendChild(cell);
                }
                tbl.appendChild(row);

                for (let i = 0; i < this.weekCount(year, month); i++) {
                    let row = document.createElement("div");
                    row.className = "landau-calendar-row";

                    for (let j = 0; j < 7; j++) {
                        if (i === 0 && j < firstDay) {
                            cell = document.createElement("div");
                            cell.className = "landau-calendar-cell inactive";

                            cellText = document.createTextNode(previousDaysInMonth - (firstDay - j) + 1);
                            cell.appendChild(cellText);
                            row.appendChild(cell);
                        }
                        else if (date > daysInMonth) {
                            cell = document.createElement("div");
                            cell.className = "landau-calendar-cell inactive";
                            cellText = document.createTextNode(date - daysInMonth);
                            cell.appendChild(cellText);
                            row.appendChild(cell);
                            date++;
                        }
                        else {
                            cell = document.createElement("div");
                            cell.className = "landau-calendar-cell active";

                            if(date === this.currentDate.getDate() && month === this.currentDate.getMonth()) {
                                cell.className += " today";
                            }

                            if(date === this.selectDate.getDate() && month === this.selectDate.getMonth()) {
                                cell.className += " select";
                                this.selectCell = cell;
                            }

                            cellText = document.createElement("span");
                            cellText.className += "calendar-day";
                            cellText.innerText = date;
                            cell.appendChild(cellText);

                            for(let r=0; r < records.length; r++) {
                                if(records[r].data.day === date) {
                                    for(let t=0; t < records[r].data.types.length; t++) {
                                        event = document.createElement("div");
                                        event.className += "event " + records[r].data.types[t].toLowerCase();
                                        event.innerText = records[r].data.types[t];

                                        cell.appendChild(event);
                                    }
                                }
                            }
                            cell.date = new Date(year, month, date);
                            cell.addEventListener("click", this.cellClick.bind(this), false);
                            row.appendChild(cell);
                            date++;
                        }
                    }

                    tbl.appendChild(row); // appending each row into calendar body.
                }
                this.bodyElement.el.dom.innerHTML = "";
                this.bodyElement.el.dom.appendChild(tbl);
            }
        });
    },

    daysInMonth: function(year, month) {
        if(month < 0) {
            month = 11;
            year -= 1;
        }

        return 32 - new Date(year, month, 32).getDate();
    },

    cellClick: function (e) {
        let target = e.target;

        if(target.className.indexOf("event") >= 0 || target.tagName.toLowerCase() === "span") {
            target = target.parentNode;
        }

        this.selectDate = target.date;

        if(this.selectCell) {
            this.selectCell.className = this.selectCell.className.replace("select", "");
        }

        this.selectCell = target;
        this.selectCell.className += " select";

        this.fireEvent("onSelectDate", this.selectDate);
    }
});