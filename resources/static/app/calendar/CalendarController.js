Ext.define("Landau.calendar.CalendarController", {
    extend: "Ext.app.ViewController",
    alias: "controller.calendar-controller",

    init: function() {
        this.getView().store = Ext.create("Landau.calendar.CalendarStore");

        if(this.getView().getDate() != null) {
            this.getView().selectDate = this.getView().getDate();
            this.select(this.getView().getDate());
        }
        else {

            this.today();
        }
    },

    today: function() {
        this.getView().changeMonth(this.getView().currentDate.getFullYear(), this.getView().currentDate.getMonth());
    },

    select: function(date) {
        this.getView().changeMonth(date.getFullYear(), date.getMonth());
    },

    weekCount: function (year, month_number) {
        var firstOfMonth = new Date(year, month_number-1, 1);
        var lastOfMonth = new Date(year, month_number, 0);

        var used = firstOfMonth.getDay() + 6 + lastOfMonth.getDate();

        return Math.ceil( used / 7);
    },

    next: function () {
        this.getView().selectDate = new Date(this.getView().selectDate.setMonth(this.getView().selectDate.getMonth()+1));
        this.select(this.getView().selectDate);
    },

    prev: function () {
        this.getView().selectDate = new Date(this.getView().selectDate.setMonth(this.getView().selectDate.getMonth()-1));
        this.select(this.getView().selectDate);
    }
});