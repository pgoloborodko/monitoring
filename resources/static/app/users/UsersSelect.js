Ext.define('Landau.users.UsersSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'users-select',
    requires: [
        'Landau.users.list.View',
    ],

    config: {
        title: lang('Выбор пользователя'),
        placeholder: lang('Пользователь...'),
        view: 'users-list-view',
        displayTpl: '{passport}',
    }
});