Ext.define("Landau.users.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.users-controller",

    saveUser: function () {
        this.openForm("Landau.users.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.users.save.View", '/users/save?id=' + info.record.id, function(dialog, response) {
            dialog.lookup('form').lookup('password').setRequired(false);

            if(response.photo !== null) {
                dialog.lookup('form').lookup('photo').setSrc('/avatars/' + response.photo);
            }
        });
    },

    doFilter: function () {
        var firstName = this.lookup("firstName");
        var secondName = this.lookup("secondName");
        var lastName = this.lookup("lastName");
        let phone = this.lookup("phone");

        var company = this.lookup("company");
        var city = this.lookup("city");
        var district = this.lookup("district");

        this.lookup("grid").getStore().reload({
            params: {
                firstName: firstName.getValue(),
                secondName: secondName.getValue(),
                lastName: lastName.getValue(),
                phone: phone.getValue(),
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue()
            }
        });
    }
});