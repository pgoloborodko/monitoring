Ext.define("Landau.users.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'users-grid-view',
    excludeTest: true,

    height: '100%',

    requires: [
        'Landau.users.UsersStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'users-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Паспорт'),
        dataIndex: 'passport',
        flex: 1,
        sorter: {
            property: ['lastName', 'firstName', 'secondName']
        }
    }, {
        text: lang('Тип'),
        dataIndex: 'role',
        width: 100
    }, {
        text: lang('Телефон'),
        dataIndex: 'phone',
        width: 110
    }, {
        text: lang('Категории'),
        dataIndex: 'driver',
        width: 100,
        renderer: function (value, record, index, cell) {
            let values = [];

            if(record.data.driverA) {
                values.push("A");
            }
            if(record.data.driverB) {
                values.push("B");
            }
            if(record.data.driverC) {
                values.push("C");
            }
            if(record.data.driverD) {
                values.push("D");
            }

            return values.length > 0 ? values.join(", ") : "Прав нет";
        },
        sorter: {
            property: ['driverA', 'driverB', 'driverC', 'driverD']
        }
    }, {
        text: lang('Регистратор'),
        dataIndex: 'device',
        width: 110
    }, {
        text: lang('Город'),
        dataIndex: 'city',
        width: 100
    }, {
        text: lang('Район'),
        dataIndex: 'district',
        width: 100
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 100
    }, {
        text: lang('Действия'),
        width: 'auto',
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit',
            }
        }
    }]
});