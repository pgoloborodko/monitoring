Ext.define('Landau.users.UsersOnlineStore', {
    extend: 'Ext.data.Store',
    alias: 'store.users-online-store',

    model: 'Landau.users.UsersOnlineModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/users/online',
        reader: {
            type: 'json'
        }
    }
});