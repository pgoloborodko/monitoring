Ext.define('Landau.users.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'users-save-view',
    controller: 'users-save-controller',
    title: lang('Сохранение пользователя'),
    testPage: "/tests/settings/user.html",

    requires: [
        'Landau.users.save.Controller',
        'Landau.users.save.ViewForm',
        'Landau.devices.DevicesStore',
        'Landau.users.UsersStore',
        'Landau.companies.CompaniesStore'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',
    height: '90%',


    items: [{
        xtype: 'users-save-form',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        scope: this,
        listeners: {
            onCancel: "onCancel",
            onSaved: "onSaved"
        }
    }]
});