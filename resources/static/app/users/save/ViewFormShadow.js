Ext.define('Landau.users.save.ViewFormShadow', {
    extend: 'Ext.form.Panel',
    xtype: 'users-save-form-shadow',
    controller: 'users-form-controller',

    requires: [
        'Landau.users.save.FormController'
    ],

    reference: 'form',
    width: '100%',
    height: '100%',

    autoSize: true,
    defaults: {
        xtype: 'textfield',
        errorTarget: 'under'
    },

    items: [{
        xtype: 'container',
        layout: 'hbox',
        flex: 1,
        items: [{
            xtype: 'container',
            layout: 'vbox',
            margin: '0 10 0 0',
            items: [{
                html: 'ФОТО',
                layout: 'center'
            }, {
                xtype: 'container',
                layout: 'hbox',
                margin: '0 0 10 0',
                items: [{
                    flex: 1,
                    xtype:'filefield',
                    label: '',
                    name: 'photoFile',
                    listeners: {
                        change: 'changeImage'
                    },
                    reference: 'photoFile'
                },{
                    xtype:'hiddenfield',
                    name: 'photoFileHidden',
                    reference: 'photoFileHidden'
                },{
                    margin: '0 0 0 10',
                    xtype: 'button',
                    text: 'webcam',
                    handler: 'fromWebCam',
                    ui: 'action'
                }]
            }, {
                xtype: 'img',
                height: 350,
                width: 350,
                layout: 'center',
                reference: 'photo'
            }]
        },{
            flex: 1,
            xtype: 'container',
            layout: 'vbox',
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                xtype: 'hiddenfield',
                name: 'company',
                reference: "company",
                value: 1
            }, {
                xtype: 'hiddenfield',
                name: 'role',
                value: 'SHADOW'
            }, {
                xtype: 'containerfield',
                label: lang('ФИО'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    name: 'lastName',
                    placeholder: lang('Фамилия'),
                    required: true
                }, {
                    name: 'firstName',
                    placeholder: lang('Имя'),
                    required: true,
                    margin: '0 5'
                }, {
                    name: 'secondName',
                    placeholder: 'Отчество'
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Паспорт'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    flex: null,
                    name: 'passportNum',
                    placeholder: lang('Номер'),
                    required: true,
                    inputMask:'9999',
                    width: 100
                }, {
                    flex: null,
                    name: 'passportSeries',
                    placeholder: lang('Серия'),
                    required: true,
                    margin: '0 5',
                    inputMask:'999999',
                    width: 100
                }, {
                    name: 'passportIssue',
                    placeholder: lang('Выдан'),
                    required: true
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Тип пассажира'),
                required: true,
                defaults: {
                    width: 55
                },
                items: [{
                    xtype: 'radio',
                    value: "SSN",
                    name: 'description',
                    label: 'SSN',
                    margin: '0 40 0 0',
                    labelWidth: 35,
                }, {
                    xtype: 'radio',
                    value: "VIP",
                    name: 'description',
                    label: 'VIP',
                    labelWidth: 35,
                }]
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});