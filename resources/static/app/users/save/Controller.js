Ext.define('Landau.users.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.users-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onSaved: function () {
        this.getView().destroy();
    }
});