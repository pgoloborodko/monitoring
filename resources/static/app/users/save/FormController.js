Ext.define('Landau.users.save.FormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.users-form-controller',


    changeImage: function (that, newValue, oldValue, eOpts) {
        var photo = this.lookup('photo');
        var hidden = this.lookup('photoFileHidden');
        var photoFile = this.lookup('photoFile');
        var files = photoFile.getFiles();

        hidden.setValue("");
        photoFile.setName("photoFile");

        if (files && files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                photo.setSrc(e.target.result);
            };

            reader.readAsDataURL(files[0]);
        }
    },

    fromWebCam: function () {
        var photo = this.lookup('photo');
        var file = this.lookup('photoFile');
        var hidden = this.lookup('photoFileHidden');
        var formDom = this.getView().element.dom;

        var stream, imageCapture, that = this;

        window.navigator.mediaDevices.getUserMedia({video: { width: 1280, height: 720 }}).then(function(mediaStream) {
            //hidden.setName("photoFile");

            stream = mediaStream;
            var mediaStreamTrack = mediaStream.getVideoTracks()[0];
            imageCapture = new ImageCapture(mediaStreamTrack);

            imageCapture.grabFrame().then(function (bitmap) {
                var canvas = document.createElement('canvas');
                var  context = canvas.getContext('2d');

                canvas.width = bitmap.width;
                canvas.height = bitmap.height;

                context.drawImage(bitmap, 0, 0)
                url = canvas.toDataURL("image/jpeg", 100);

                //var url = window.URL.createObjectURL(value);
                photo.setSrc(url);


                var base64 = url.split(',')[1];
                hidden.setValue(base64);

                mediaStreamTrack.stop();
           }).catch(function() {
                console.log(arguments);
                Ext.Msg.alert(lang("Ошибка!"), lang("Невозможно сделать фото!"));
            });


        }).catch(function() {
            console.log(arguments);
            Ext.Msg.alert(lang("Ошибка!"), lang("Нет доступа к камере!"));
        });
    },

    changeCity: function (scope) {
        this.lookup("district").getParams().city = scope.getValue();
    },

    onOK: function () {
        this.getView().fireEvent('onSave', this.getView());

        var form = this.getView();

        if (form.validate()) {
            form.submit({
                url: '/users/save',
                scope: this,
                method: 'post',
                success: this.successCallback,
                failure: this.failureCallback
            });
        }
    },

    successCallback: function () {
        this.getView().fireEvent('onSaved', this.getView());
    },

    failureCallback: function (scope, result) {
        this.getView().fireEvent('onError', this.getView());

        if(result.code === 400) {
            Ext.Msg.alert(lang('Ошибка!'), lang('Пользователь с таким телефоном уже существует!'));
        }
        else {
            Ext.Msg.alert(lang('Ошибка!'), lang('Заполните поля!'));
        }
    },

    onCancel: function () {
        this.getView().fireEvent('onCancel', this.getView());
    },
});