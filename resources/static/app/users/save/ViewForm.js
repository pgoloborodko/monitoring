Ext.define('Landau.users.save.ViewForm', {
    extend: 'Ext.form.Panel',
    xtype: 'users-save-form',
    controller: 'users-form-controller',
    excludeTest: true,

    requires: [
        'Landau.users.save.FormController',
        'Landau.devices.DevicesSelect',
        'Landau.districts.DistrictsSelect',
        'Landau.cities.CitiesSelect',
        'Landau.companies.CompaniesSelect'
    ],

    reference: 'form',
    width: '100%',
    height: '100%',

    autoSize: true,
    defaults: {
        xtype: 'textfield',
        errorTarget: 'under'
    },

    items: [{
        xtype: 'container',
        layout: 'hbox',
        flex: 3,
        items: [{
            xtype: 'container',
            layout: 'vbox',
            margin: '0 10 0 0',
            items: [{
                html: lang('ФОТО'),
                layout: 'center'
            }, {
                xtype: 'container',
                layout: 'hbox',
                margin: '0 0 10 0',
                items: [{
                    flex: 1,
                    xtype:'filefield',
                    label: '',
                    name: 'photoFile',
                    listeners: {
                        change: 'changeImage'
                    },
                    reference: 'photoFile'
                },{
                    xtype:'hiddenfield',
                    name: 'photoFileHidden',
                    reference: 'photoFileHidden'
                },{
                    margin: '0 0 0 10',
                    xtype: 'button',
                    text: 'webcam',
                    handler: 'fromWebCam',
                    ui: 'action'
                }]
            }, {
                xtype: 'img',
                height: 170,
                width: 170,
                layout: 'center',
                reference: 'photo'
            }]
        },{
            flex: 1,
            xtype: 'container',
            layout: 'vbox',
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                xtype: 'containerfield',
                label: lang('ФИО'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    name: 'lastName',
                    placeholder: lang('Фамилия'),
                    required: true
                }, {
                    name: 'firstName',
                    placeholder: lang('Имя'),
                    required: true,
                    margin: '0 5'
                }, {
                    name: 'secondName',
                    placeholder: 'Отчество'
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Данные для входа'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    name: 'phone',
                    placeholder: lang('Номер телефона'),
                    required: true,
                    margin: '0 5 0 0',
                    inputMask: '+7 (999) 999-99-99',
                    reference: 'phone',
                    //errorTarget: 'side',
                }, {
                    xtype: 'passwordfield',
                    name: 'password',
                    placeholder: lang('Пароль'),
                    required: true,
                    //validators: /.{9,}$/,
                    reference: 'password',
                    errorTarget: 'side',

                    validators: function (val) {
                        var lengthOfPass = 9;
                        //var tn = val.replace(/.{9,}$/),
                        //var tn = val.replace(/[^0-9]/g,''),
                        var tn = val;
                        errMsg = lang("Длина пароля должна быть больше") + " " + lengthOfPass + " " + lang("символов") + "!";
                        return (tn.length >= lengthOfPass) ? true : errMsg;
                    }
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Паспорт'),
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    flex: null,
                    name: 'passportNum',
                    placeholder: lang('Номер'),
                    required: true,
                    inputMask:'9999',
                    width: 100
                }, {
                    flex: null,
                    name: 'passportSeries',
                    placeholder: lang('Серия'),
                    required: true,
                    margin: '0 5',
                    inputMask:'999999',
                    width: 100
                }, {
                    name: 'passportIssue',
                    placeholder: lang('Выдан')
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Водительские права'),
                defaults: {
                    flex: 1,
                    allowBlank: false
                },
                items: [{
                    flex: null,
                    name: 'driverNum',
                    placeholder: lang('Номер'),
                    inputMask:'9999',
                    width: 100
                }, {
                    flex: null,
                    name: 'driverSeries',
                    placeholder: lang('Серия'),
                    margin: '0 5',
                    inputMask:'999999',
                    width: 100
                },  {
                    xtype: 'checkbox',
                    boxLabel: 'A',
                    name: 'driverA',
                    value: true
                },  {
                    xtype: 'checkbox',
                    boxLabel: 'B',
                    name: 'driverB',
                    value: true
                },  {
                    xtype: 'checkbox',
                    boxLabel: 'C',
                    name: 'driverC',
                    value: true
                },  {
                    xtype: 'checkbox',
                    boxLabel: 'D',
                    name: 'driverD',
                    value: true
                }]
            }, {
                xtype: 'container',
                layout: 'hbox',
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'containerfield',
                    label: lang('Тип аккаунта'),
                    margin: '0 5 0 0',
                    required: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'selectfield',
                        name: 'role',
                        required: true,
                        placeholder: lang('Выберите тип аккаунта...'),
                        options: [{
                            text: lang('Администратор'),
                            value: 'ADMIN'
                        }, {
                            text: lang('Оператор'),
                            value: 'OPERATOR'
                        }, {
                            text: lang('Наблюдающий'),
                            value: 'VIEWER'
                        }, {
                            text: lang('КПП'),
                            value: 'CHECKPOINT'
                        }, {
                            text: lang('Патрульный'),
                            value: 'PATROL'
                        }/*, {
                            text: lang('Пассажир'),
                            value: 'SHADOW'
                        }*/]
                    }]
                },{
                    xtype: 'containerfield',
                    label: lang('Компания'),
                    required: true,
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'companies-select',
                        name: 'company',
                        required: true
                    }]
                }/*,{
                    xtype: 'containerfield',
                    label: lang('Регистратор'),
                    defaults: {
                        flex: 1
                    },
                    items: [{
                        xtype: 'devices-select',
                        name: 'device',
                        reference: 'device'
                    }]
                }*/]
            },{
                xtype: 'containerfield',
                label: lang('Город и район'),
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'cities-select',
                    name: 'city',
                    reference: 'city',
                    listeners: {
                        change: 'changeCity'
                    },
                    margin: '0 5 0 0',
                },{
                    xtype: 'districts-select',
                    name: 'district',
                    reference: 'district'
                }]
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});