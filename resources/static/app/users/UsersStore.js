Ext.define('Landau.users.UsersStore', {
    extend: 'Ext.data.Store',
    alias: 'store.users-store',

    model: 'Landau.users.UsersModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/users',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});