Ext.define('Landau.users.UsersModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'phone',
        'firstName',
        'secondName',
        'lastName',
        'driverA',
        'driverB',
        'driverC',
        'driverD',
        'templateExists',
        {name: 'passport', convert: function (value, record) {
            let lastName = (record.get('lastName') !== undefined ? record.get('lastName'): "");
            let firstName =  (record.get('firstName') !== undefined ? record.get('firstName'): "");
            let secondName =  (record.get('secondName') !== undefined ? record.get('secondName'): "");

            return lastName + " " +  firstName+ " " + secondName;
        },  depends: [ 'firstName',  'secondName',  'lastName']},
        {
            name: 'role', convert: function (value, record) {
                switch (value) {
                    case "ADMIN":
                        return lang("Администратор");
                    case "OPERATOR":
                        return lang("Оператор");
                    case "VIEWER":
                        return lang("Наблюдающий");
                    case "PATROL":
                        return lang("Патрульный");
                    case "CHECKPOINT":
                        return lang("КПП");
                    case "SHADOW":
                        return lang("Пассажир");
                }
            }
        },
        {name: 'device', mapping: 'device.name'},
        {name: 'company', mapping: 'company.name'},
        {name: 'city', mapping: 'city.name'},
        {name: 'district', mapping: 'district.name'}
    ]
});