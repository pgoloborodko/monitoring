Ext.define('Landau.combolist.DialogController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.combolist-dialog-controller',
    wrapper: null,
    dataIndex: null,

    onCancel: function () {
        this.getView().hide();
    },

    init: function () {
        this.wrapper = this.lookup("wrapper").add({
            xtype: this.getView().getView(),
            width: '100%',
            height: '100%'
        });

        this.grid = this.wrapper.lookup("grid");

        this.grid.setListeners({
            childtap: this.rowclick.bind(this)
        });

        this.grid.getStore().proxy.extraParams = this.getView().getParams();

        for(let i=0; i < this.grid.getColumns().length; i++) {
            if(this.grid.getColumns()[i].getDataIndex() === "settings") {
                this.grid.getColumns()[i].setHidden(true);
            }

            if((this.grid.getColumns()[i].getDataIndex() === "boxcheck" && this.getView().getMultiselect()) || (this.grid.getColumns()[i].getDataIndex() === "radiocheck" && !this.getView().getMultiselect())) {
                this.grid.getColumns()[i].setHidden(false);
                this.dataIndex = this.grid.getColumns()[i].getDataIndex();
                this.grid.getColumns()[i].setListeners({
                    checkchange: this.checkchange.bind(this)
                })
            }
        }

        //
        this.grid.getStore().on("load", function (ctx, records) {
            for(let i=0; i < records.length; i++) {

                if(this.getView().getSelected()[records[i].get("id")] !== undefined) {
                    records[i].set(this.dataIndex, true);
                }
            }
        }, this);

        if(this.grid.getStore().getAutoLoad()) {
            this.grid.getStore().load();
        }

        //
        this.updateButtonState();
    },

    destroy: function () {
        this.wrapper.lookup("grid").getStore().off("load");
    },

    onOK: function() {
        this.fireViewEvent("onSelect", this.getView().getSelected());
        this.getView().hide();
    },

    checkchange: function(that, rowIndex, checked, record, e, eOpts) {
        this.updateSelected(checked, record);
        record.set(this.dataIndex, checked);
    },

    rowclick: function (scope, row) {
        if(row.cell.dataIndex !== this.dataIndex) {
            if(row.record.get(this.dataIndex) === undefined || row.record.get(this.dataIndex) === false) {
                this.updateSelected(true, row.record);
                row.record.set(this.dataIndex, true);
            }
            else {
                this.updateSelected(false, row.record);
                row.record.set(this.dataIndex, false);
            }
        }
    },

    updateSelected: function(checked, record) {
        // grid
        if(!this.getView().getMultiselect()) {
            let items = this.grid.getStore();

            for (let i = 0; i < items.count(); i++) {
                items.getAt(i).set(this.dataIndex, false);
            }

            this.getView().setSelected({});
        }

        // checked
        if(checked) {
            this.getView().getSelected()[record.get("id")] = record;
        }
        else {
            delete this.getView().getSelected()[record.get("id")];
        }

        this.updateButtonState();
    },

    updateButtonState: function() {
        // selected
        let len = Object.keys(this.getView().getSelected()).length;

        if(len > 0) {
            this.lookup("ok").setDisabled(false);
            this.lookup("ok").setText(lang("Применить") + " " + len + " " + this.declOfNum(len, [lang("элемент"), lang("элемента"), lang("элементов")]));
        }
        else {
            this.lookup("ok").setDisabled(true);
            this.lookup("ok").setText(lang("Применить"));
        }
    },

    declOfNum: function (n, titles) {
        return titles[n%10===1 && n%100!==11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2];
    }
});