Ext.define('Landau.combolist.View', {
    extend: 'Ext.field.ComboBox',
    xtype: 'combolist',
    editable: false,
    controller: 'combolist-controller',
    requires: [
        "Landau.combolist.ViewController"
    ],

    config: {
        multiselect: false,
        view: null,
        filters: [],
        valueField: "id",
        title: lang('Выбор элемента'),
        clearable: true,
        params: {}
    },

    initialize: function () {
        this.bodyElement.on('click', (e) => {
            if(e.target.parentNode.className.indexOf("x-cleartrigger") < 0) {
                this.getController().openDialog.call(this.getController());
            }
        }, this);

        this.addListener("change", function (ctx, newVal, oldVal) {
            if(newVal == null || !newVal) {
                this.setInputValue("");
            }
            else {
                this.setInputValue( this.getDisplayTpl().apply(newVal));
            }
        });
    },

    onExpandTap: function() {

    },

    getRawValue: function() {
        if(this._value === undefined || this._value === null) {
            return null;
        }
        else {
            return this._value;
        }
    },

    getValue: function () {
        if(this._value === undefined || this._value === null) {
            return null;
        }
        else {
            return this._value[this.getValueField()];
        }
    }
});