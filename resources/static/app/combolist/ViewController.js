Ext.define('Landau.combolist.ViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.combolist-controller',

    openDialog: function () {
        let selected = {};

        if(this.getView().getValue() != null) {
            selected[this.getView().getValue()] = this.getView().getRawValue();
        }

        this.dialog = Ext.create("Landau.combolist.Dialog", {
            selected: selected,
            title: this.getView().getTitle(),
            view: this.getView().getView(),
            params: this.getView().getParams(),
            multiselect: this.getView().getMultiselect(),
            listeners: {
                onSelect: function(ctx, values) {
                    let vals = Object.values(values);

                    if(vals.length > 0) {
                        this.getView().setValue(vals[0].data);
                    }
                    else {
                        this.getView().setValue(null);
                    }
                }.bind(this)
            }
        });

        this.dialog.show();
    }
});