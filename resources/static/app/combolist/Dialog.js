Ext.define('Landau.combolist.Dialog', {
    extend: 'Ext.Dialog',
    xtype: 'combodialog',
    closable: true,
    title: 'Выбор элементов',
    width: '90%',
    height: '90%',
    controller: 'combolist-dialog-controller',
    padding: '0 10',

    config: {
        multiselect: false,
        view: null,
        params: {},
        selected:{}
    },

    requires: [
        "Landau.combolist.DialogController",
        'Landau.common.Pageable',
        'Landau.common.Radiocolumn'
    ],

    onCancel: function () {
        this.hide();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        width: '100%',
        height: '100%',
        padding: '0 10',
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'container',
            reference: 'wrapper',
            width: '100%',
            height: '100%'
        }]
    }],

    buttons: [{
        text: lang('Применить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0',
        reference: 'ok',
        disabled: true
    }/*, {
        text: lang('Сбросить'),
        handler: 'onClear'
    }*/, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});