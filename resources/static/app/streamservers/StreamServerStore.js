Ext.define('Landau.streamservers.StreamServerStore', {
    extend: 'Ext.data.Store',
    alias: 'store.streamservers-store',

    model: 'Landau.streamservers.StreamServerModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/streams/servers',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});