Ext.define('Landau.streamservers.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.streamservers-save-controller',


    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/streams/servers/save',
                scope: this,
                success: this.successCallback,
                failure: this.failureCallback
            });
        }
    },

    successCallback: function () {
        this.getView().parentController.reloadStore();
        this.getView().destroy();
    },

    failureCallback: function (scope, result) {
        if(result.data.length == 0) {
            Ext.Msg.alert(lang('Ошибка!'), lang('У вас нет прав на добавление стрим-сервера'));

            this.getView().parentController.reloadStore();
            this.getView().destroy();
        }
    }
});