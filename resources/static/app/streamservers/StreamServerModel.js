Ext.define('Landau.streamservers.StreamServerModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'company', mapping: 'company.name'}
    ]
});