Ext.define("Landau.streamservers.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'streamservers-grid-view',

    height: '100%',

    requires: [
        'Landau.streamservers.StreamServerStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'streamservers-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Адрес RTMP'),
        dataIndex: 'rtmpUrl',
        width: 150
    }, {
        text: lang('Адрес HTTP'),
        dataIndex: 'httpUrl',
        width: 150
    }, {
        text: lang('Адрес SLDP'),
        dataIndex: 'sldpUrl',
        width: 150
    }, {
        text: lang('Адрес HLS stream'),
        dataIndex: 'hlsStreamUrl',
        width: 150
    }, {
        text: lang('Адрес HLS raw'),
        dataIndex: 'hlsArchiveUrl',
        width: 150
    }, {
        text: lang('Токен'),
        dataIndex: 'token',
        width: 200
    }, {
        text: lang('Активен'),
        dataIndex: 'state',
        width: 70
    }, {
        text: lang('Статус'),
        dataIndex: 'status',
        width: 70
    }/*, {
        text: lang('Устройства'),
        dataIndex: 'onlineDevices',
        width: 100
    }*/, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 200
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});