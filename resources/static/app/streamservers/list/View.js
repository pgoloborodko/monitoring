Ext.define("Landau.streamservers.list.View", {
    extend: 'Ext.Panel',
    controller: 'streamservers-controller',

    requires: [
        'Landau.streamservers.list.Controller',
        'Landau.streamservers.list.ViewGrid',
        'Landau.companies.CompaniesSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'streamservers-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новый стрим-сервер'),
        ui: 'action',
        handler: 'save'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});