Ext.define("Landau.streamservers.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.streamservers-controller",


    onEdit: function (grid, info) {
        var dialog = Ext.create("Landau.streamservers.save.View", {
            parentController: this
        });

        dialog.show();

        var form = dialog.lookup('form');

        form.load({
            waitMsg: lang("Загрузка..."),
            url: '/streams/servers/save?id=' + info.record.id
        });
    },

    onOpenDevice: function () {
        //var f

    },

    save: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.streamservers.save.View", {
                parentController: that
            }).show();

            Ext.Viewport.setMasked(false);
        }, 10);
    },

    reloadStore: function () {
        this.getView().getItems().items[0].items.items[0].store.reload();
    },


    doFilter: function () {
        var company = this.lookup("company");

        this.lookup("grid").getStore().reload({
            params: {
                company: company.getValue()
            }
        });
    }
});