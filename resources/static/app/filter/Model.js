Ext.define('Landau.filter.Model', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'name'
    ]
});