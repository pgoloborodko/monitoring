Ext.define('Landau.filter.StoreValue', {
    extend: 'Ext.data.Store',
    alias: 'store.filter-store-value',

    model: 'Landau.filter.ModelValue',

    proxy: {
        type: 'ajax',
        url: '/filter/values',
        reader: {
            type: 'json',
            rootProperty: 'content',
        }
    }
});