Ext.define('Landau.filter.ModelValue', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'name'
    ]
});