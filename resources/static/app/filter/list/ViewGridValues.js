Ext.define("Landau.filter.list.ViewGridValues", {
    extend: 'Ext.grid.Grid',
    xtype: 'filter-grid-values-view',

    height: '100%',

    requires: [
        'Landau.filter.StoreValue'
    ],

    store: {
        type: 'filter-store-value',
        autoLoad: true
    },

    columns: [{
        text: lang('Название'),
        dataIndex: 'name',
    }, {
        text: lang('Действия'),
        width: 'auto',

        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});