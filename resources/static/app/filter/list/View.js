Ext.define("Landau.filter.list.View", {
    extend: 'Ext.Panel',
    controller: 'filter-list-controller',

    requires: [
        'Landau.filter.list.Controller',
        'Landau.filter.list.ViewGrid',
        'Landau.filter.list.ViewGridValues'
    ],


    height: "100%",
    defaultType: 'panel',
    layout: "hbox",
    flex: 1,

    items: [{
        border: true,
        xtype: "panel",
        width: "50%",
        items: [{
            height: "100%",
            xtype: 'filter-grid-view'
        }],
        tbar: [{
            text: lang('Добавить новый фильтр'),
            ui: 'action',
            handler: 'saveFilter'
        }]
    },{
        xtype: "panel",
        width: "50%",
        items: [{
            height: "100%",
            xtype: 'filter-grid-view'
        }],
        tbar: [{
            text: lang('Добавить новое значение фильтра'),
            ui: 'action',
            handler: 'saveFilter'
        }]
    }]
});