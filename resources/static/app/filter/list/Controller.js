Ext.define("Landau.filter.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.filter-list-controller",

    onEdit: function (grid, info) {
        var dialog = Ext.create("Landau.filter.save.View", {
            parentController: this
        });

        dialog.show();

        var form = dialog.lookup('form');

        form.load({
            waitMsg: lang("Загрузка..."),
            url: '/cities/save?id=' + info.record.id,
            success: function (ev, response) {
                form.setValues(response);
            },
            failure: function () {
                console.log(arguments);
            }
        });
    },


    saveFilter: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.filter.saveFilter.View", {
                parentController: that
            }).show();
        }, 10);
    },

    saveFilterValue: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            Ext.create("Landau.filter.saveFilterValue.View", {
                parentController: that
            }).show();
        }, 10);
    },

    reloadStore: function () {
        this.getView().getItems().items[0].items.items[0].store.reload();
    }
});