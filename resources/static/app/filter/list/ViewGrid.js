Ext.define("Landau.filter.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'filter-grid-view',

    height: '100%',

    requires: [
        'Landau.filter.Store'
    ],

    store: {
        type: 'filter-store',
        autoLoad: true
    },

    columns: [{
        text: lang('Название'),
        dataIndex: 'name',
    }, {
        text: lang('Действия'),
        width: 'auto',

        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});