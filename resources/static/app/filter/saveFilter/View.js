Ext.define('Landau.cities.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'cities-save-view',
    controller: 'cities-save-controller',
    title: lang('Сохранение города'),

    requires: [
        'Landau.cities.save.Controller'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Название города'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                name: 'name',
                placeholder: lang('Город'),
                required: true,
                margin: '0 5 0 0'
            }, {
                name: 'region',
                placeholder: lang('Регион'),
                required: true,
                width: 100,
                validators: /^[0-9]+$/,
                flex: null
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});