Ext.define('Landau.cities.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cities-save-controller',


    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/cities/save',
                scope: this,
                success: this.successCallback,
                failure: this.failureCallback
            });
        }
    },

    successCallback: function () {
        this.getView().parentController.reloadStore();
        this.getView().destroy();
    },

    failureCallback: function (scope, result) {
        if(result.length == 0) {
            Ext.Msg.alert(lang('Ошибка!'), lang('У вас нет прав на добавление городов'));

            this.getView().parentController.reloadStore();
            this.getView().destroy();
        }
    }
});