Ext.define('Landau.filter.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.filter-store',

    model: 'Landau.filter.Model',

    proxy: {
        type: 'ajax',
        url: '/filter',
        reader: {
            type: 'json',
            rootProperty: 'content',
        }
    }
});