Ext.define("Landau.transport.list.View", {
    extend: 'Ext.Panel',
    controller: 'transport-controller',
    xtype: 'transport-list-view',

    requires: [
        'Landau.transport.list.Controller',
        'Landau.transport.list.ViewGrid'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'transport-grid-view',
            reference: "grid"
        }]
    }],

    tbar: [{
        text: lang('Добавить новый транспорт'),
        ui: 'action',
        handler: 'save'
    }]
});