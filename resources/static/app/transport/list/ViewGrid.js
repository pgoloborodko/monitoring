Ext.define("Landau.transport.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'transport-grid-view',
    excludeTest: true,

    height: '100%',

    requires: [
        'Landau.transport.list.Store',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'transport-list-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Номер'),
        dataIndex: 'numbers',
        width: 150
    }, {
        text: lang('Водитель'),
        dataIndex: 'driverFull',
        width: 200,
        sorter: {
            property: 'driver'
        }
    }, {
        text: lang('Регистратор'),
        dataIndex: 'device',
        width: 200
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 150
    }, {
        text: lang('Действия'),
        width: 'auto',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit',
            }
        }
    }]
});