Ext.define('Landau.transport.list.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.transport-list-store',

    model: 'Landau.transport.list.Model',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/transport',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});