Ext.define("Landau.transport.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.transport-controller",

    save: function () {
        this.openForm("Landau.transport.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.transport.save.View", '/transport/save?id=' + info.record.id);
    }
});