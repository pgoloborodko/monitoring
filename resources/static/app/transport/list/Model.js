Ext.define('Landau.transport.list.Model', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'name',
        'numbers',
        {name: 'device', mapping: 'device.serial'},
        {
            name: 'driverFull', convert: function (value, record) {
                if (record.get('driver') != null) {
                    return record.get('driver').lastName + " " + record.get('driver').firstName + " " + record.get('driver').secondName;
                }

                return lang("Без водителя");
            }
        },
        {name: 'company', mapping: 'company.name'}
    ]
});