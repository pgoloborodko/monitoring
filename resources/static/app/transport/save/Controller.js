Ext.define('Landau.transport.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.transport-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');
        var that = this;

        if (form.validate()) {
            form.submit({
                url: '/transport/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});