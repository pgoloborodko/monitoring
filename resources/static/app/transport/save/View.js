Ext.define('Landau.transport.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'transport-save-view',
    controller: 'transport-save-controller',
    title: lang('Сохранение транспорта'),

    requires: [
        'Landau.transport.save.Controller',
        'Landau.devices.list.ViewGrid',
        'Landau.users.UsersSelect',
        'Landau.devices.DevicesSelect',
        'Landau.companies.CompaniesSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'id'
        },{
            xtype: 'containerfield',
            label: lang('Регистрационные данные'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'name',
                placeholder: lang('Марка, модель'),
                required: true,
                errorTarget: 'side',
                margin: '0 5 0 0'
            },{
                flex: null,
                name: 'numbers',
                placeholder: lang('Номерной знак'),
                required: true,
                errorTarget: 'side',
                width: 100
            }]
        }, {
            xtype: 'containerfield',
            label: lang('Категория'),
            defaults: {
                flex: 1,
                allowBlank: false
            },
            items: [{
                xtype: 'radio',
                boxLabel: 'A',
                name: 'category',
                layout: 'left',
                value: 'A'
            },  {
                xtype: 'radio',
                boxLabel: 'B',
                name: 'category',
                value: 'B'
            },  {
                xtype: 'radio',
                boxLabel: 'C',
                name: 'category',
                value: 'C'
            },  {
                xtype: 'radio',
                boxLabel: 'D',
                name: 'category',
                value: 'D'
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'containerfield',
                label: lang('Тип транспорта'),
                margin: '0 5 0 0',
                required: true,
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'selectfield',
                    name: 'type',
                    required: true,
                    placeholder: lang('Выберите тип транспорта...'),
                    options: [{
                        text: lang('Автомобиль'),
                        value: 'CAR'
                    }, {
                        text: lang('Автобус'),
                        value: 'BUS'
                    }, {
                        text: lang('Мотоцикл'),
                        value: 'MOTO'
                    }, {
                        text: lang('Грузовик'),
                        value: 'TRUCK'
                    }, {
                        text: lang('Фургон'),
                        value: 'WAGON'
                    }, {
                        text: lang('Другое'),
                        value: 'OTHER'
                    }]
                }]
            }, {
                xtype: 'containerfield',
                label: lang('Компания'),
                required: true,
                margin: '0 5 0 0',
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'companies-select',
                    name: 'company'
                }]
            },{
                xtype: 'containerfield',
                label: lang('Регистратор'),
                margin: '0 5 0 0',
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'devices-select',
                    name: 'device'
                }]
            },{
                xtype: 'containerfield',
                label: lang('Водитель'),
                defaults: {
                    flex: 1
                },
                items: [{
                    xtype: 'users-select',
                    name: 'driver'
                }]
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});