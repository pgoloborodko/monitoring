Ext.define("Landau.api.View", {
    extend: 'Ext.panel.Panel',
    xtype: 'api-view',
    flex: 1,
    controller: {
        refresh: function () {
            this.lookup("iframe").el.dom.querySelector("iframe").src = this.lookup("iframe").el.dom.querySelector("iframe").src;

            /// not work wtf
            // this.lookup("iframe").setHtml("<iframe src='/swagger-ui.html' style='width: 100%; height: 100%; border:0'></iframe>");
        }
    },

    height: "100%",
    defaultType: 'panel',

    layout: {
        type: 'hbox'
    },

    items: [{
        xtype: 'container',
        height: '100%',
        width: '100%',
        padding: 0,
        cls: 'iframe-terminal',
        reference: 'iframe',
        html: "<iframe src='/swagger-ui.html' style='width: 100%; height: 100%; border:0'></iframe>"
    }],

    tbar: [{
        iconCls: 'x-fa fa-refresh',
        text: lang('Обновить'),
        handler: 'refresh'
    }]
});