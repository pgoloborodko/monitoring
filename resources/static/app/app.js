//Ext.Loader.setConfig({disableCaching: false});

window.appVersion = "~";

Ext.application({
    appFolder: '/app',
    name: 'Landau',

    requires: [
        'Landau.common.Component',
        'Landau.loader.View',
        'Landau.common.LoadMask',
        'Landau.common.FormPanel',
        'Landau.common.ViewController',
        'Landau.locale.Locale_ru',
        'Landau.login.ShadowView',
        'Landau.common.Sorter',
        'Landau.common.ProxyServer',
        'Landau.common.Radiocolumn'
        //'Landau.viewer.View',
        //'Landau.checkpoint.View',
        //'Landau.login.View',
        //'Landau.main.View',
    ],

    mainView: 'Landau.loader.View'
});