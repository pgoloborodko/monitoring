Ext.define("Landau.main.Controller", {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main-controller',

    init: function () {
        let that = this;

        Ext.core.DomHelper.append(
            that.getView().el.dom,
            `<span style="position: absolute;
                    z-index: 100000000000000000;
                    top: 5px;
                    right: 5px;
                    color: rgba(255, 255, 255, 1);
                    font-weight: bold;
                    text-shadow: 0 0 3px black;">${window.appVersion}</span>`
        );


        if(this.hasHashValue("tab")) {
            let value = this.getHashValue("tab");

            if(value.indexOf("stream") >= 0) {
                this.getView().getAt(1).tab.fireEvent("tap", this.getView().getAt(1).tab);
            }
            else if(value.indexOf("video") >= 0) {
                this.getView().getAt(2).tab.fireEvent("tap", this.getView().getAt(2).tab);
            }
            else if(value.indexOf("terminals") >= 0) {
                this.getView().getAt(3).tab.fireEvent("tap", this.getView().getAt(3).tab);
            }
            else if(value.indexOf("tasks") >= 0) {
                this.getView().setActiveItem(4);
            }
            else if(value.indexOf("stats") >= 0) {
                this.getView().setActiveItem(5);
            }
            else if(value.indexOf("settings") >= 0) {
                this.getView().setActiveItem(6);
            }
        }

        //this.lookup("logo").tab.setTitle('<img src="/images/favicon-white.png" style="position: absolute;height: 50px;top: 10px;">');

/*        if(Ext.user.company && Ext.user.company.logo != null) {
            this.lookup("logo").tab.setTitle('<img src="/avatars/' + Ext.user.company.logo + '" style="position: absolute;height: 50px;top: 10px;">');
        }
        else {
            this.lookup("logo").tab.setTitle('<img src="/images/favicon-white.png" style="position: absolute;height: 50px;top: 10px;">');
        }



        if(location.hash.indexOf("video") >= 0) {
            this.getView().getAt(1).tab.fireEvent("tap", this.getView().getAt(1).tab);
        }
        else if(location.hash.indexOf("stream") >= 0) {
            this.getView().getAt(2).tab.fireEvent("tap", this.getView().getAt(2).tab);
        }
        else if(location.hash.indexOf("terminals") >= 0) {
            this.getView().setActiveItem(3);
        }
        else if(location.hash.indexOf("tasks") >= 0) {
            this.getView().setActiveItem(4);
        }
        else if(location.hash.indexOf("stats") >= 0) {
            this.getView().setActiveItem(5);
        }
        else if(location.hash.indexOf("settings") >= 0) {
            this.getView().setActiveItem(6);
        }*/
    }
});