/**
 *
 */
Ext.define('Landau.main.View', {
    extend: 'Ext.tab.Panel',
    xtype: 'main-view',
    controller: 'main-controller',

    requires: [
        "Landau.main.Controller",
        "Landau.map.View",
        "Landau.terminal.View",
        "Landau.settings.View",
        "Landau.stats.View",
        "Landau.tasks.list.View",
        "Landau.common.MediaRecorder",
        'Landau.stats.accum.details.Chart'
    ],

    layout: {
        animation: false
    },

    config: {
        logo: null
    },
    onChangeTab: function(tab) {
        console.log(tab);
    },

    listeners: {
        beforeshow: function(tab) {
            //Ext.user.role !== "ADMIN" && this.child('#control').tab.hide()
        },

        beforeactiveItemchange : function(tab, value, oldValue) {
            if(value.view === "video") {
                tab.setActiveItem(0);
                return false;
            }
            else if(value.view === "stream") {
                tab.setActiveItem(0);
                return false;
            }
            else if(value.view === "exit") {
                Ext.apply(Ext.MessageBox, {
                    YES: { text: lang('Да'), itemId: 'yes', ui: 'action', margin: '0 5 0 0' },
                    NO: { text: lang('Нет'), itemId: 'no'}
                });

                Ext.apply(Ext.MessageBox, {
                    YESNO: [Ext.MessageBox.YES, Ext.MessageBox.NO]
                });

                Ext.Msg.confirm(lang("Выход"), lang("Вы точно хотите выйти?"), function (buttonValue) {
                    if(buttonValue === "yes") {
                        location.href = "/logout";
                    }
                });
                return false;
            }
            else {
                tab.getTabBar().getAt(1).removeCls('x-active');
            }
        }
    },

    platformConfig: {
        '!desktop': {
            tabBar: {
                docked: 'bottom'
            },
            cls: 'mobile-config'
        }
    },

    tabBar: {
        docked: 'top'
    },

    defaults: {
        scrollable: true
    },

    // bbar: {
    //     width: '100%',
    //     docked: 'bottom',
    //     items: [{
    //         text: lang('Статус бар'),
    //         width: '100%'
    //     }]
    // },

    items: [{
        platformConfig: {
            '!desktop': {
                title: ''
            }
        },
        iconCls: 'x-fa fa-map-marker',
        title: lang('Карта'),
        view: 'map',
        cls: 'map',
        active: true,
        items : [{
            xtype : 'map-view'
        }],

        tab: {
            listeners: {
                tap: function(tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "map");

                    tab.setActive(true);
                    tab.tabBar.animateTabIndicator(tab.tabBar.getAt(0), tab.tabBar.getAt(1));

                    tab.tabBar.getAt(0).addCls('x-active');
                    tab.tabBar.getAt(1).removeCls('x-active');
                    tab.tabBar.getAt(2).removeCls('x-active');

                    // pause stream videos
                    var streams = tab.tabBar.getAt(0).card.getAt(0);
                    streams.getController().setType("MAP");

                    setTimeout(function () {
                        tab.activeIndicatorElement.show();
                    }, 50);
                }
            }
        }
    }, {
        platformConfig: {
            '!desktop': {
                title: ''
            }
        },
        iconCls: 'x-fa fa-play-circle',
        title: lang('СТРИМ'),
        cls: 'stream',
        view: 'stream',
        html: '<span class="action">Streams</span>',
        tab: {
            listeners: {
                tap: function(tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "stream");

                    tab.tabBar.getAt(0).removeCls('x-active');
                    tab.tabBar.getAt(1).addCls('x-active');
                    tab.tabBar.getAt(2).removeCls('x-active');

                    // play videos
                    let streams = tab.tabBar.getAt(0).card.getAt(0);
                    streams.getController().setType("STREAM");

                    setTimeout(function () {
                        //streams.lookup("videoContainer").setHidden(false);
                        //streams.setEventsHidden(false);
                        //streams.lookup("map").resize();
                        // streams.lookup("player").play();
                        //streams.lookup("map").getController().clear();
                    }, 50);
                }
            }
        }
    }, {
        platformConfig: {
            '!desktop': {
                title: ''
            }
        },
        iconCls: 'x-fa fa-film',
        title: lang('Расследования'),
        cls: 'video',
        view: 'video',
        html: '<span class="action">Videos</span>',
        tab: {
            listeners: {
                tap: function(tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "video");

                    tab.tabBar.getAt(0).removeCls('x-active');
                    tab.tabBar.getAt(1).removeCls('x-active');
                    tab.tabBar.getAt(2).addCls('x-active');

                    // play videos
                    var streams = tab.tabBar.getAt(0).card.getAt(0);
                    streams.getController().setType("ARCHIVE");

                    setTimeout(function () {
                        //streams.lookup("videoContainer").setHidden(false);
                        //streams.setEventsHidden(false);
                        //streams.lookup("map").resize();
                        // streams.lookup("player").play();
                        //streams.lookup("map").getController().clear();
                    }, 50);
                }
            }
        }
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true,
                title: ''
            }
        },
        iconCls: 'x-fa fa-hdd-o',
        title: lang('Терминалы'),
        cls: 'terminals',
        view: 'terminals',
        items : [{
            xtype : 'terminal-view'
        }],
        tab: {
            listeners: {
                tap: function (tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "terminals");

                    tab.tabBar.getAt(0).removeCls('x-active');
                    tab.tabBar.getAt(1).removeCls('x-active');
                    tab.tabBar.getAt(2).removeCls('x-active');

                    //var streams = tab.tabBar.getAt(0).card.getAt(0);
                    //streams.getController().setType("TERMINAL");
                    // pause videos
                    // tab.tabBar.getItems().items[0].card.items.items[0].lookup("player").pause();
                    //tab.tabBar.getAt(0).card.getAt(0).lookup("player").tabTapRefresh();
                }
            }
        }
    }, {
        platformConfig: {
            '!desktop': {
                title: ''
            }
        },
        iconCls: 'x-fa fa-tasks',
        title: lang('Задачи'),
        cls: 'tasks',
        view: 'tasks',
        items : [{
            xtype : 'tasks-list-view'
        }],
        tab: {
            listeners: {
                tap: function (tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "tasks");

                    tab.card.getAt(0).lookup("grid").getStore().load();
                    // pause stream videos
                    // tab.tabBar.getItems().items[0].card.items.items[0].lookup("player").pause();
                    //tab.tabBar.getAt(0).card.getAt(0).lookup("player").tabTapRefresh()
                }
            }
        }
    },  {
        platformConfig: {
            '!desktop': {
                title: ''
            }
        },
        iconCls: 'x-fa fa-bar-chart',
        title: lang('Статистика'),
        cls: 'stats',
        view: 'stats',
        items : [{
            xtype : 'stats-view'
        }],
        tab: {
            listeners: {
                tap: function (tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "stats");

                    // pause stream videos
                    // tab.tabBar.getItems().items[0].card.items.items[0].lookup("player").pause();
                    //tab.tabBar.getAt(0).card.getAt(0).lookup("player").tabTapRefresh()
                }
            }
        }
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true,
                title: ''
            }
        },
        iconCls: 'x-fa fa-cog',
        title: lang('Управление'),
        cls: 'settings',
        view: 'settings',
        items : [{
            xtype : 'settings-view'
        }],
        tab: {
            listeners: {
                tap: function (tab) {
                    tab.getParent().getParent().getController().updateHashValue("tab", "settings");

                    // pause stream videos
                    // tab.tabBar.getItems().items[0].card.items.items[0].lookup("player").pause();
                    //tab.tabBar.getAt(0).card.getAt(0).lookup("player").tabTapRefresh()
                }
            }
        }
    }, {
        platformConfig: {
            '!desktop': {
                title: ''
            }
        },
        iconCls: 'x-fa fa-sign-out',
        title: lang('Выход'),
        cls: 'exit',
        view: 'exit',
    }, {
        platformConfig: {
            '!desktop': {
                hidden: true
            }
        },
        title: '&nbsp;',
        cls: 'exit2',
        reference: 'logo',
        disabled: true
    }/*, {
        xtype: 'toolbar',
        docked: 'bottom',
        padding:0,
        style: 'position: absolute; bottom:20px; right: 20px; padding:0; background: transparent;',
        // height: '25px',
        // padding: 0,
        items: [{
            iconCls: 'x-fa fa-pencil-square-o',
            title: lang('Чат'),
            text: 'Чат',
            ui: 'action',
            height: '48px',
            style: {
                fontSize: '16px',
                lineHeight: '20px'
            }
        }]
    }*/]

        /*{
        xtype: 'toolbar',
        docked: 'bottom',
        // height: '25px',
        // padding: 0,
        items: [{
            iconCls: 'x-fa fa-sign-out',
            title: lang('Статус бар'),
        }]
    }*/

});