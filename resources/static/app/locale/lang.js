var arrayff = [];

// get some kind of XMLHttpRequest
var xhrObj = new XMLHttpRequest();

// open and send a synchronous request
xhrObj.open('GET', "/app/locale/"+(readCookie("lang") === null ? "ru" : readCookie("lang"))+".js", false);
xhrObj.send('');

// add the returned content to a newly created script tag
var se = document.createElement('script');
se.type = "text/javascript";
se.text = xhrObj.responseText;
document.getElementsByTagName('head')[0].appendChild(se);

// HSS
function lang(value) {
    if(locale[value] != null){
        return locale[value]
    }
    // console.log('+++++++++++++++++++++++')
    //console.warn(value);
    return value;
}

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}