
Ext.define('Landau.locale.Locale_ru', {
    override: 'Ext.Date',
    monthNames: [
        lang('Янв'),
        lang('Фев'),
        lang('Мар'),
        lang('Апр'),
        lang('Май'),
        lang('Июн'),
        lang('Июл'),
        lang('Авг'),
        lang('Сен'),
        lang('Окт'),
        lang('Ноя'),
        lang('Дек')
    ],
    dayNames: [
        lang("Вс"),
        lang("Пн"),
        lang("Вт"),
        lang("Ср"),
        lang("Чт"),
        lang("Пт"),
        lang("Сб")
    ],
    firstDayOfWeek: 4,
    MONDAY: 0
});
/*
Ext.Date.dayNames= [
    "аыв",
    "авы",
    "куц",
    "куц",
    "куц",
    "авы",
    "авы"
];

Ext.override(Ext.DatePicker, {
    todayText : 'Bug�n',
    minText : 'Minimum Tarih',
    maxText : 'Maksimum Tarih',
    disabledDaysText : '',
    disabledDatesText : '',
    //monthNames : localMonthNames,
    //dayNames : localDayNames,
    nextText : 'Sonraki Ay',
    prevText : '�nceki Ay',
    monthYearText : 'MonthYearText',
    todayTip : '{0} (spacebar)',
    okText : 'Tamam',
    cancelText : '?ptal',
    format : 'd.m.Y',
    startDay : 1
});
*/