Ext.define('Landau.license.LicenseModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',

    fields: [
        {name: 'state', convert: function (value, record) {
                if(value == undefined &&  record.data.online || value) {
                    return "<span class='devices-status' style='background:#73b51e; float: left; margin: 0 5px 0 0'></span> " + lang("Активно");
                }
                return "<span class='devices-status' style='background:#cf4c35; float: left; margin: 0 5px 0 0'></span>" + lang("Деактивно");
            }}
    ]
});