Ext.define("Landau.license.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'license-grid-view',

    height: '100%',

    requires: [
        'Landau.license.LicenseStore'
    ],

    store: {
        type: 'license-store',
        autoLoad: true
    },

    columns: [{
        text: lang('Наименование'),
        dataIndex: 'name',
        flex: 1,
        renderer: function (value) {
            return lang(value);
        }
    }, {
        text: lang('Артикул'),
        dataIndex: 'article',
        width: 100,
    }, {
        text: lang('Количество лицензий'),
        dataIndex: 'count',
        width: 160,
    }, {
        text: lang('Статус'),
        dataIndex: 'state',
        width: 100,
        cell: {
            encodeHtml: false
        }
    }]
});