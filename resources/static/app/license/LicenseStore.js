Ext.define('Landau.license.LicenseStore', {
    extend: 'Ext.data.Store',
    alias: 'store.license-store',

    model: 'Landau.license.LicenseModel',

    proxy: {
        type: 'ajax',
        url: '/license/list',
        reader: {
            type: 'json'
        }
    }
});