Ext.define('Landau.tokens.TokenModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'company', mapping: 'company.name'},
        {name: 'status',  convert: function (value) {
            return value ? "Активен" : "Выключен"
        }}
    ]
});