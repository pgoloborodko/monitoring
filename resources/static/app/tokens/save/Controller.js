Ext.define('Landau.tokens.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.tokens-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/users/tokens/save',
                scope: this,
                success: this.onCancel
            });
        }
    }
});