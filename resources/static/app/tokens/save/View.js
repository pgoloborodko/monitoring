Ext.define('Landau.tokens.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'tokens-save-view',
    controller: 'tokens-save-controller',
    title: lang('Сохранение токена'),

    requires: [
        'Landau.tokens.save.Controller'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Параметры токена'),
            required: true,
            layout: 'vbox',
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                name: 'name',
                placeholder: lang('Название'),
                required: true,
            }, {
                name: 'value',
                placeholder: lang('Токен (автоматически)'),
                readOnly: true
            },{
                xtype: 'checkbox',
                name: 'status',
                boxLabel: lang('Активен'),
                required: true,
                value: true
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});