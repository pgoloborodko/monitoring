Ext.define("Landau.tokens.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.tokens-controller",

    onEdit: function (grid, info) {
        this.openForm("Landau.tokens.save.View", '/users/tokens/save?id=' + info.record.id);
    },

    save: function () {
        this.openForm("Landau.tokens.save.View",);
    }
});