Ext.define("Landau.tokens.list.View", {
    extend: 'Ext.Panel',
    controller: 'tokens-controller',
    xtype: 'tokens-list-view',

    requires: [
        'Landau.tokens.list.Controller',
        'Landau.tokens.list.ViewGrid'
    ],

    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'tokens-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новый токен'),
        ui: 'action',
        handler: 'save'
    }]
});