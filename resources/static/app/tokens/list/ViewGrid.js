Ext.define("Landau.tokens.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'tokens-grid-view',

    height: '100%',

    requires: [
        'Landau.tokens.TokenStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'tokens-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Токен'),
        dataIndex: 'value',
        width: 300
    }, {
        text: lang('Статус'),
        dataIndex: 'status',
        width: 200
    }, {
        text: lang('Действия'),
        width: 'auto',

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});