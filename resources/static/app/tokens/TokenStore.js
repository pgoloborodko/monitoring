Ext.define('Landau.tokens.TokenStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tokens-store',

    model: 'Landau.tokens.TokenModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/users/tokens',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});