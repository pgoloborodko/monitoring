/**
 * init form
 */
Ext.define('Landau.loader.View', {
    extend: 'Ext.LoadMask',
    controller: 'loader-controller',
    title: 'Login form',

    requires: [
        'Landau.loader.Controller'
    ],

    message: lang('Инициализация...')
});