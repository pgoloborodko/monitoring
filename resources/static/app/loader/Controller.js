Ext.define('Landau.loader.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.loader-controller',

    init: function (data, format) {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        Ext.Ajax.request({
            url: '/login/status',
            disableCaching: false,
            scope: this,
            failure: this.failureCallback,
            success: this.successCallback
        });
    },

    // 403
    failureCallback: function (response) {
        this.getView().destroy();
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        Ext.require("Landau.login.View", function() {
            Ext.Viewport.add(Ext.create("Landau.login.View"));
            Ext.Viewport.setMasked(false);
        }, this);

        window.appVersion = response.getResponseHeader("App-Version");
        console.info("App version: " + response.getResponseHeader("App-Version"));
    },

    // 200
    successCallback: function (response) {
        this.getView().destroy();

        var decode = Ext.decode(response.responseText);

        if(decode.company != null && decode.company.color != null) {
            document.documentElement.style.setProperty('--base-color', decode.company.color);
        }

        let map = "MapViewYandex";

        if(decode.company != null && decode.company.map === "METRO") {
            map = "MapViewMetro";
        }
        if(decode.company != null && decode.company.map === "GOOGLE") {
            map = "MapViewYandex";
        }

        Ext.user = decode;

        var xtype = "main";

        switch(decode.role) {
            case "VIEWER":
                xtype = "viewer";
                break;
            case "CHECKPOINT":
                xtype = "checkpoint";
                break;
        }


        Ext.require(["Landau." + xtype + ".View", 'Landau.common.Messages', "Landau.map." + map], function() {
            Ext.Viewport.add(Ext.create("Landau." + xtype + ".View", {
                viewModel: {
                    data: {
                        map: "Landau.map." + map
                    }
                }
            }));

            this.loginChecker();

            Ext.Viewport.setMasked(false);
        }, this);


        window.appVersion = response.getResponseHeader("App-Version");
        console.info("App version: " + response.getResponseHeader("App-Version"));
    },

    loginChecker: function () {
        setTimeout(function () {
            Ext.Ajax.request({
                url: '/login/status',
                disableCaching: false,
                scope: this,
                json: true,
                failure: function () {
                    if(ShadowView.isHidden()) {
                        ShadowView.show();
                    }

                    this.loginChecker();
                },
                success: function (response) {
                    if(!ShadowView.isHidden()) {
                        ShadowView.hide();
                    }

                    this.loginChecker();
                }
            });

        }.bind(this), 20000);
    }
});
