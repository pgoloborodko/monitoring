Ext.define('Landau.cities.CitiesModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'name',
        'region'
    ]
});