Ext.define('Landau.cities.CitiesSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'cities-select',
    requires: [
        'Landau.cities.list.View'
    ],

    config: {
        title: lang('Выбор города'),
        placeholder: lang('Город...'),
        view: 'cities-list-view',
        displayTpl: '{name}',
    }
});