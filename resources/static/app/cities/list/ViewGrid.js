Ext.define("Landau.cities.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'cities-grid-view',

    height: '100%',

    requires: [
        'Landau.cities.CitiesStore',
        'Landau.common.Pageable',
        'Landau.common.Radiocolumn'
    ],

    store: {
        type: 'cities-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Регион'),
        dataIndex: 'region',
        width: 100
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Действия'),
        width: 'auto',
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});