Ext.define("Landau.cities.list.View", {
    extend: 'Ext.Panel',
    controller: 'cities-controller',
    xtype: 'cities-list-view',

    requires: [
        'Landau.cities.list.Controller',
        'Landau.cities.list.ViewGrid',
        'Landau.companies.CompaniesSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'cities-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новый город'),
        ui: 'action',
        handler: 'save'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: 'Название',
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'concreteCompany',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});