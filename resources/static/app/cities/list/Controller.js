Ext.define("Landau.cities.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.cities-controller",

    save: function () {
        this.openForm("Landau.cities.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.cities.save.View", '/cities/save?id=' + info.record.id);
    },

    doFilter: function () {
        var concreteCompany = this.lookup("concreteCompany");
        var name = this.lookup("name");

        this.lookup("grid").getStore().reload({
            params: {
                concreteCompany: concreteCompany.getValue(),
                name: name.getValue()
            }
        });
    }
});