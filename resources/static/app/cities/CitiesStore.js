Ext.define('Landau.cities.CitiesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cities-store',

    model: 'Landau.cities.CitiesModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/cities',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});