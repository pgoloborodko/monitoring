Ext.define('Landau.routes.RoutesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.routes-store',

    model: 'Landau.routes.RoutesModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/routes',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});