Ext.define('Landau.routes.RoutesSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'routes-select',
    requires: [
        'Landau.routes.list.View'
    ],

    config: {
        title: lang('Выбор маршрута'),
        placeholder: lang('Маршрут...'),
        view: 'routes-list-view',
        displayTpl: '{name}',
    }
});