Ext.define('Landau.routes.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'routes-save-view',
    controller: 'routes-save-controller',
    title: lang('Сохранение маршрута'),

    requires: [
        'Landau.routes.save.Controller',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Название маршрута'),
            required: true,
            layout: 'vbox',
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                name: 'name',
                placeholder: lang('Название маршрута'),
                required: true
            }, {
                xtype: 'companies-select',
                name: 'company'
            }, {
                xtype: 'cities-select',
                name: 'city',
                listeners: {
                    change: 'changeCity'
                },
            }, {
                xtype: 'districts-select',
                name: 'district',
                reference: 'district'
            }]
        }],

        buttons: [{
            text: lang('Сохранить'),
            ui: 'action',
            handler: 'onOK',
            margin: '0 5 0 0'
        }, {
            text: lang('Отменить'),
            handler: 'onCancel'
        }]
    }]
});