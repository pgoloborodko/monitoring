Ext.define('Landau.routes.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.routes-save-controller',

    changeCity: function (scope) {
        this.lookup("district").getParams().city = scope.getValue();
    },

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/routes/save',
                scope: this,
                success: this.onCancel,
            });
        }
    }
});