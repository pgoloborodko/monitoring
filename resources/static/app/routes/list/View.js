Ext.define("Landau.routes.list.View", {
    extend: 'Ext.Panel',
    controller: 'routes-controller',
    xtype: 'routes-list-view',

    requires: [
        'Landau.routes.list.Controller',
        'Landau.routes.list.ViewGrid',
        'Landau.companies.CompaniesSelect',
        'Landau.cities.CitiesSelect',
        'Landau.districts.DistrictsSelect'
    ],


    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'routes-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новый маршрут'),
        ui: 'action',
        handler: 'save',
        margin: '0 10 0 0'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: 'Название',
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        xtype: 'districts-select',
        reference: 'district',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});