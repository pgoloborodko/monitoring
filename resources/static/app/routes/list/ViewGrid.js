Ext.define("Landau.routes.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'routes-grid-view',
    excludeTest: true,

    height: '100%',

    requires: [
        'Landau.routes.RoutesStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'routes-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Компания'),
        dataIndex: 'company',
        width: 200
    }, {
        text: lang('Город'),
        dataIndex: 'city',
        width: 200
    }, {
        text: lang('Район'),
        dataIndex: 'district',
        width: 200
    }, {
        text: lang('Действия'),
        width: 'auto',
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});