Ext.define("Landau.routes.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.routes-controller",

    onEdit: function (grid, info) {
        this.openForm("Landau.routes.save.View", '/routes/save?id=' + info.record.id);
    },

    save: function () {
        this.openForm("Landau.routes.save.View");
    },

    doFilter: function () {
        let name = this.lookup("name");
        let company = this.lookup("company");
        let city = this.lookup("city");
        let district = this.lookup("district");

        this.lookup("grid").getStore().reload({
            params: {
                name: name.getValue(),
                company: company.getValue(),
                city: city.getValue(),
                district: district.getValue()
            }
        });
    }
});