Ext.define('Landau.routes.RoutesModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'company', mapping: 'company.name'},
        {name: 'city', mapping: 'city.name'},
        {name: 'district', mapping: 'district.name'}
    ]
});