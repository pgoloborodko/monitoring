Ext.define("Landau.checkpoint.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.checkpoint-controller",

    init: function () {
        this.lookup("grid").getStore().load({
            params: {
                role: 'SHADOW'
            }
        });

        Messages.bind({
            action: ["message.template"],
            callback: function (data) {
                this.lookup("grid").getStore().reload();
            },
            scope: this
        });


        Messages.bind({
            action: ["message.identy"],
            callback: this.messageIdenty,
            scope: this
        });
    },

    messageIdenty: function (data) {
        if(this.eventView != undefined && !this.eventView.isDestroyed) {
            data.deviceFull = data.device;
            data.device = data.device.serial;
            data.company = data.company.name;

            this.eventView.lookup("grid").getStore().insert(0, data);
            this.eventView.lookup("grid").refresh();
        }
        else {
            var button = this.lookup("eventButton");
            var count = !button.getBadgeText() ? 0 : parseInt(button.getBadgeText());

            count++;

            button.setBadgeText(count);
            button.setUi("raised button-group action alt");
        }
    },

    onExit: function () {
        Ext.apply(Ext.MessageBox, {
            YES: { text: 'Да', itemId: 'yes', ui: 'action'},
            NO: { text: 'Нет', itemId: 'no', margin: '0 5 0 0' }
        });

        Ext.apply(Ext.MessageBox, {
            YESNO: [Ext.MessageBox.NO, Ext.MessageBox.YES]
        });

        Ext.Msg.confirm("Выход", "Вы точно хотите выйти?", function (buttonValue) {
            if(buttonValue === "yes") {
                location.href = "/logout";
            }
        })
        return false;
    },

    newUser: function () {
        Ext.Viewport.setMasked({ xtype: 'loadmask'});

        var that = this;
        setTimeout(function () {
            that.lookup("container").setItems({
                xtype: 'users-save-form-shadow',
                title: "Сохранение пассажира",
                listeners: {
                    onCancel: "onCancelForm",
                    onSaved: "onSavedForm",
                },
                scope: this
            });
        }, 10);
    },

    onCancelForm: function () {
        this.lookup("container").setItems({
            xtype: 'container',
            layout: 'center',
            cls:'row',
            flex: 1,
            html: 'Выберите пассажира',
            height: '100%',
        });
    },

    onSavedForm: function () {
        this.lookup("grid").getStore().reload();

        this.onCancelForm();

        Ext.Msg.alert("Внимание!", "Пассажир сохранен!");
    },

    onEdit: function (grid, info) {
        this.lookup("container").setItems({
            xtype: 'users-save-form-shadow',
            title: "Сохранение пассажира",
            listeners: {
                onCancel: "onCancelForm",
                onSaved: "onSavedForm",
            },
            scope: this
        });

        var form = this.lookup('form');

        form.load({
            waitMsg: "Загрузка...",
            url: 'users/save?id=' + info.record.id,
            success: function (ev, response) {
                form.setValues(response);

                if(response.company != null && form.lookup('company') != null) {
                    form.lookup('company').setValue(response.company.id);
                }

                if(response.photo !== null) {
                    form.lookup('photo').setSrc('/avatars/' + response.photo);
                }
            },
            failure: function () {
                console.log(arguments);
            }
        });
    },

    openEvents: function (buttonValue) {
        var button = this.lookup("eventButton");
        button.setUi("raised");
        button.setBadgeText(false);

        this.eventView = new Ext.create("Landau.events.View", {
            type: 'IDENTY',
            parentController: this,
            closable: true,
            maskTapHandler: 'onCancel',
        });

        this.eventView.show();
    },
});