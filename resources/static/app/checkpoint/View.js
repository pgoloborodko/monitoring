Ext.define("Landau.checkpoint.View", {
    extend: 'Ext.panel.Panel',
    controller: "checkpoint-controller",

    extend: 'Ext.panel.Panel',
    flex: 1,
    defaultType: 'panel',
    layout: {
        type: 'hbox'
    },

    requires: [
        'Ext.layout.HBox',
        'Landau.users.UsersStore',
        "Landau.events.View",
        "Landau.users.save.ViewFormShadow",
        'Landau.checkpoint.Controller',
    ],

    items: [{
        reference: 'menu',
        title: "Пассажиры",
        width: 300,
        border: true,
        items: [{
            defaultType: 'panel',
            flex: 1,
            height: '100%',
            xtype: 'grid',
            layout: 'fit',
            autoLoad: true,
            scope: this,
            reference: "grid",
            store: {
                type: 'users-store',
                scope: this
            },
            listeners: {
                childtap: "onEdit"
            },
            columns: [{
                sortable: false,
                menuDisabled: true,
                text: 'ФИО',
                dataIndex: 'passport',
                flex: 1
            }, {
                sortable: false,
                menuDisabled: true,
                text: 'Шаблон',
                dataIndex: 'templateExists',
                width: 80,
                renderer: function (value, record, index, cell) {
                    cell.setTools([{
                        xtype: 'checkbox',
                        checked: record.data.templateExists,
                        width: 30,
                        labelWidth: 10,
                        margin: '0 15 0 0',
                        disabled: true
                    }]);
                }
            }]
        }]
    }, {
        layout: 'vbox',
        flex: 8,
        height: '100%',
        autoScroll: true,
        reference: 'container',
        items: [{
            xtype: 'container',
            layout: 'center',
            cls:'row',
            flex: 1,
            html: 'Выберите пассажира',
            height: '100%',
        }]
    },{
        xtype : 'toolbar',
        docked: 'bottom',
        reference: 'toolbar-bottom',
        items: [{
            text: 'Новый пассажир',
            margin: '0 20 0 0',
            ui:'action',
            handler: 'newUser'
        }, {
            text: 'Уведомления',
            margin: '0 20 0 0',
            ui:'raised',
            reference: 'eventButton',
            handler: 'openEvents'
        }, {
            xtype: 'spacer'
        },{
            text: 'Выход',
            margin: '0 34 0 0',
            handler: 'onExit',
        }]
    }]
});