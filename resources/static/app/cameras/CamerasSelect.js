Ext.define('Landau.cameras.CamerasSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'cameras-select',
    requires: [
        'Landau.cameras.list.View'
    ],

    config: {
        title:  lang('Выбор камеры'),
        placeholder: lang('Камера...'),
        view: 'cameras-list-view',
        displayTpl: '{name}',
    }
});