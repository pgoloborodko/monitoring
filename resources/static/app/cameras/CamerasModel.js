Ext.define('Landau.cameras.CamerasModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'name',
        {name: 'city', mapping: 'city.name'},
    ]
});