Ext.define("Landau.cameras.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.cameras-controller",

    save: function () {
        this.openForm("Landau.cameras.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.cameras.save.View", '/cameras/save?id=' + info.record.id);
    }
});