Ext.define("Landau.cameras.list.View", {
    extend: 'Ext.Panel',
    controller: 'cameras-controller',
    xtype: 'cameras-list-view',

    requires: [
        'Landau.cameras.list.Controller',
        'Landau.cameras.list.ViewGrid'
    ],

    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'cameras-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новую камеру'),
        ui: 'action',
        handler: 'save'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: 'Название',
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});