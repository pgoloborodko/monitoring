Ext.define("Landau.cameras.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'cameras-grid-view',

    height: '100%',

    requires: [
        'Landau.cameras.CamerasStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'cameras-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Район'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Город'),
        dataIndex: 'city',
        flex: 1
    }, {
        text: lang('Действия'),
        width: 'auto',
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});