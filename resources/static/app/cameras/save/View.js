Ext.define('Landau.cameras.save.View', {
    extend: 'Ext.Dialog',
    xtype: 'cameras-save-view',
    controller: 'cameras-save-controller',
    title: 'Сохранение камеры',

    requires: [
        'Landau.cameras.save.Controller',
        'Landau.cities.CitiesSelect'
    ],

    bodyPadding: 20,
    autoSize: true,

    closable: true,
    defaultFocus: 'textfield',
    maskTapHandler: 'onCancel',

    onCancel: function () {
        this.destroy();
    },

    items: [{
        xtype: 'formpanel',
        reference: 'form',
        autoSize: true,
        defaults: {
            xtype: 'textfield',
            errorTarget: 'under'
        },
        items: [{
            xtype: 'containerfield',
            label: lang('Название камеры'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                name: 'name',
                placeholder: lang('Район'),
                required: true
            }]
        },{
            xtype: 'containerfield',
            label: lang('Пренадлежность городу'),
            required: true,
            defaults: {
                flex: 1
            },
            items: [{
                name: 'city',
                xtype: 'cities-select',
                reference: 'city',
                required: true,
                width: 100,
                itemTpl: '{name}'
            }]
        }]
    }],

    buttons: [{
        text: lang('Сохранить'),
        ui: 'action',
        handler: 'onOK',
        margin: '0 5 0 0'
    }, {
        text: lang('Отменить'),
        handler: 'onCancel'
    }]
});