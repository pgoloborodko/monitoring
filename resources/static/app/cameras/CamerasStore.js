Ext.define('Landau.cameras.CamerasStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cameras-store',

    model: 'Landau.cameras.CamerasModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/cameras',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});