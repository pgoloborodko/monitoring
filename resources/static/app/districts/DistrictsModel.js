Ext.define('Landau.districts.DistrictsModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        'id',
        'name',
        {name: 'city', mapping: 'city.name'},
    ]
});