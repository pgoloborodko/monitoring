Ext.define('Landau.districts.DistrictsSelect', {
    extend: 'Landau.combolist.View',
    xtype: 'districts-select',
    requires: [
        'Landau.districts.list.View'
    ],

    config: {
        title:  lang('Выбор района'),
        placeholder: lang('Район...'),
        view: 'districts-list-view',
        displayTpl: '{name}',
    }
});