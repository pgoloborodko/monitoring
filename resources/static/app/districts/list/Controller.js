Ext.define("Landau.districts.list.Controller", {
    extend: "Ext.app.ViewController",
    alias: "controller.districts-controller",

    save: function () {
        this.openForm("Landau.districts.save.View");
    },

    onEdit: function (grid, info) {
        this.openForm("Landau.districts.save.View", '/districts/save?id=' + info.record.id);
    },

    doFilter: function () {
        let name = this.lookup("name");
        let company = this.lookup("company");
        let city = this.lookup("city");

        this.lookup("grid").getStore().reload({
            params: {
                name: name.getValue(),
                company: company.getValue(),
                city: city.getValue()
            }
        });
    }
});