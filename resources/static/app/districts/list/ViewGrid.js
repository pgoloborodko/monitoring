Ext.define("Landau.districts.list.ViewGrid", {
    extend: 'Ext.grid.Grid',
    xtype: 'districts-grid-view',

    height: '100%',

    requires: [
        'Landau.districts.DistrictsStore',
        'Landau.common.Pageable'
    ],

    store: {
        type: 'districts-store',
        autoLoad: true
    },

    items: [{
        xtype: 'pageable'
    }],

    columns: [{
        text: '#',
        dataIndex: 'id',
        width: 50
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'boxcheck',
        width: 50,
        cell: {
            xtype: 'checkcell'
        },
    }, {
        hidden: true,
        xtype: 'checkcolumn',
        dataIndex: 'radiocheck',
        width: 50,
        cell: {
            xtype: 'radiocell'
        },
    }, {
        text: lang('Название'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: lang('Город'),
        dataIndex: 'city',
        flex: 1
    }, {
        text: lang('Действия'),
        width: 'auto',
        dataIndex: 'settings',
        sortable: false,
        menuDisabled: true,

        // Cells can contain only tools (no dataIndex)
        cell: {
            tools: {
                gear: 'onEdit'
            }
        }
    }]
});