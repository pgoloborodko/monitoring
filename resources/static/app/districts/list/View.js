Ext.define("Landau.districts.list.View", {
    extend: 'Ext.Panel',
    controller: 'districts-controller',
    xtype: 'districts-list-view',

    requires: [
        'Landau.districts.list.Controller',
        'Landau.districts.list.ViewGrid'
    ],

    height: "100%",
    defaultType: 'panel',


    items: [{
        xtype: 'container',
        height: "100%",
        items: [ {
            height: "100%",
            xtype: 'districts-grid-view',
            reference: 'grid'
        }]
    }],

    tbar: [{
        text: lang('Добавить новый район'),
        ui: 'action',
        handler: 'save'
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'textfield',
        reference: 'name',
        placeholder: 'Название',
        margin: '0 10 0 0'
    }, {
        xtype: 'cities-select',
        reference: 'city',
        margin: '0 10 0 0'
    }, {
        xtype: 'companies-select',
        reference: 'company',
        margin: '0 10 0 0'
    }, {
        iconCls: 'x-fa fa-search',
        text: lang('Найти'),
        handler: 'doFilter'
    }]
});