Ext.define('Landau.districts.DistrictsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.districts-store',

    model: 'Landau.districts.DistrictsModel',
    remoteSort: true,

    proxy: {
        type: 'ajax',
        url: '/districts',
        reader: {
            type: 'json',
            rootProperty: 'content', //.content
            totalProperty: 'totalElements',
            metaProperty: 'size'
        }
    }
});