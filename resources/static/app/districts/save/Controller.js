Ext.define('Landau.districts.save.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.districts-save-controller',

    onCancel: function () {
        this.getView().destroy();
    },

    onOK: function () {
        var form = this.lookup('form');

        if (form.validate()) {
            form.submit({
                url: '/districts/save',
                scope: this,
                success: this.onCancel,
                failure: this.onFailure
            });
        }
    },

    onFailure: function (ctx, result) {
        return Ext.Msg.alert(lang('Внимание!'), lang('Ошибка запроса: ' + result.responseText));
    }
});