FROM openjdk:11.0.5-jdk
COPY resources /var/www/resources
COPY target/core-2.4.4.3.jar /var/www/
COPY wait-for-it.sh /var/www/
WORKDIR /var/www

EXPOSE 443
EXPOSE 80